<?php

/* default/template/checkout/cart.twig */
class __TwigTemplate_d9012f0167b696ca1b6a686a37ce19229f3afeec6efcf01fbe343dbe86690e05 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo "
<style>
\t.cart-page{
\t\tmargin-top: 120px !important;
\t}
\t#button-voucher{
\t\tborder: none;
\t\tpadding: 0;
\t}
</style>
<div id=\"checkout-cart\" class=\"container\">
  <ul class=\"breadcrumb\">
    ";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 14
            echo "    <li><a href=\"";
            echo $this->getAttribute($context["breadcrumb"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array());
            echo "</a></li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 16
        echo "  </ul>
  ";
        // line 17
        if ((isset($context["attention"]) ? $context["attention"] : null)) {
            // line 18
            echo "  <div class=\"alert alert-info\"><i class=\"fa fa-info-circle\"></i> ";
            echo (isset($context["attention"]) ? $context["attention"] : null);
            echo "
    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
  </div>
  ";
        }
        // line 22
        echo "  ";
        if ((isset($context["success"]) ? $context["success"] : null)) {
            // line 23
            echo "  <div class=\"alert alert-success alert-dismissible\"><i class=\"fa fa-check-circle\"></i> ";
            echo (isset($context["success"]) ? $context["success"] : null);
            echo "
    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
  </div>
  ";
        }
        // line 27
        echo "  ";
        if ((isset($context["error_warning"]) ? $context["error_warning"] : null)) {
            // line 28
            echo "  <div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo (isset($context["error_warning"]) ? $context["error_warning"] : null);
            echo "
    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
  </div>
  ";
        }
        // line 32
        echo "  <div class=\"row\">";
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "
    ";
        // line 33
        if (((isset($context["column_left"]) ? $context["column_left"] : null) && (isset($context["column_right"]) ? $context["column_right"] : null))) {
            // line 34
            echo "    ";
            $context["class"] = "col-sm-6";
            // line 35
            echo "    ";
        } elseif (((isset($context["column_left"]) ? $context["column_left"] : null) || (isset($context["column_right"]) ? $context["column_right"] : null))) {
            // line 36
            echo "    ";
            $context["class"] = "col-sm-9";
            // line 37
            echo "    ";
        } else {
            // line 38
            echo "    ";
            $context["class"] = "col-sm-12";
            // line 39
            echo "    ";
        }
        // line 40
        echo "    <div id=\"content\" class=\"";
        echo (isset($context["class"]) ? $context["class"] : null);
        echo "\">";
        echo (isset($context["content_top"]) ? $context["content_top"] : null);
        echo "
      <h1>";
        // line 41
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "
        ";
        // line 42
        if ((isset($context["weight"]) ? $context["weight"] : null)) {
            // line 43
            echo "        &nbsp;(";
            echo (isset($context["weight"]) ? $context["weight"] : null);
            echo ")
        ";
        }
        // line 44
        echo " </h1>
      <form action=\"";
        // line 45
        echo (isset($context["action"]) ? $context["action"] : null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\">
        <div class=\"table-responsive\">
          <table class=\"table table-bordered\">
            <thead>
              <tr>
                <td class=\"text-center\">";
        // line 50
        echo (isset($context["column_image"]) ? $context["column_image"] : null);
        echo "</td>
                <td class=\"text-left\">";
        // line 51
        echo (isset($context["column_name"]) ? $context["column_name"] : null);
        echo "</td>
                <td class=\"text-left\">";
        // line 52
        echo (isset($context["column_model"]) ? $context["column_model"] : null);
        echo "</td>
                <td class=\"text-left\">";
        // line 53
        echo (isset($context["column_quantity"]) ? $context["column_quantity"] : null);
        echo "</td>
                <td class=\"text-right\">";
        // line 54
        echo (isset($context["column_price"]) ? $context["column_price"] : null);
        echo "</td>
                <td class=\"text-right\">";
        // line 55
        echo (isset($context["column_total"]) ? $context["column_total"] : null);
        echo "</td>
              </tr>
            </thead>
            <tbody>
            
            ";
        // line 60
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 61
            echo "            <tr>
              <td class=\"text-center\">";
            // line 62
            if ($this->getAttribute($context["product"], "thumb", array())) {
                echo " <a href=\"";
                echo $this->getAttribute($context["product"], "href", array());
                echo "\"><img src=\"";
                echo $this->getAttribute($context["product"], "thumb", array());
                echo "\" alt=\"";
                echo $this->getAttribute($context["product"], "name", array());
                echo "\" title=\"";
                echo $this->getAttribute($context["product"], "name", array());
                echo "\" class=\"img-thumbnail\" /></a> ";
            }
            echo "</td>
              <td class=\"text-left\"><a href=\"";
            // line 63
            echo $this->getAttribute($context["product"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["product"], "name", array());
            echo "</a> ";
            if ( !$this->getAttribute($context["product"], "stock", array())) {
                echo " <span class=\"text-danger\">***</span> ";
            }
            // line 64
            echo "                ";
            if ($this->getAttribute($context["product"], "option", array())) {
                // line 65
                echo "                ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["product"], "option", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["option"]) {
                    echo " <br />
                <small>";
                    // line 66
                    echo $this->getAttribute($context["option"], "name", array());
                    echo ": ";
                    echo $this->getAttribute($context["option"], "value", array());
                    echo "</small> ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 67
                echo "                ";
            }
            // line 68
            echo "                ";
            if ($this->getAttribute($context["product"], "reward", array())) {
                echo " <br />
                <small>";
                // line 69
                echo $this->getAttribute($context["product"], "reward", array());
                echo "</small> ";
            }
            // line 70
            echo "                ";
            if ($this->getAttribute($context["product"], "recurring", array())) {
                echo " <br />
                <span class=\"label label-info\">";
                // line 71
                echo (isset($context["text_recurring_item"]) ? $context["text_recurring_item"] : null);
                echo "</span> <small>";
                echo $this->getAttribute($context["product"], "recurring", array());
                echo "</small> ";
            }
            echo "</td>
              <td class=\"text-left\">";
            // line 72
            echo $this->getAttribute($context["product"], "model", array());
            echo "</td>
              <td class=\"text-left\"><div class=\"input-group btn-block\" style=\"max-width: 200px;\">
                  
                  ";
            // line 75
            if ((array_key_exists("quantity_status", $context) && (isset($context["quantity_status"]) ? $context["quantity_status"] : null))) {
                echo " 
                    <input type=\"text\" name=\"quantity[";
                // line 76
                echo $this->getAttribute($context["product"], "cart_id", array(), "array");
                echo "]\" value=\"";
                echo $this->getAttribute($context["product"], "quantity", array(), "array");
                echo "\" size=\"1\" class=\"form-control\" readonly/>
                  ";
            } else {
                // line 77
                echo " 
                      <input type=\"text\" name=\"quantity[";
                // line 78
                echo $this->getAttribute($context["product"], "cart_id", array(), "array");
                echo "]\" value=\"";
                echo $this->getAttribute($context["product"], "quantity", array(), "array");
                echo "\" size=\"1\" class=\"form-control\" />
                  ";
            }
            // line 80
            echo "                
                  <span class=\"input-group-btn\">
                  <button type=\"submit\" data-toggle=\"tooltip\" title=\"";
            // line 82
            echo (isset($context["button_update"]) ? $context["button_update"] : null);
            echo "\" class=\"btn btn-primary\"><i class=\"fa fa-refresh\"></i></button>
                  <button type=\"button\" data-toggle=\"tooltip\" title=\"";
            // line 83
            echo (isset($context["button_remove"]) ? $context["button_remove"] : null);
            echo "\" class=\"btn btn-danger\" onclick=\"cart.remove('";
            echo $this->getAttribute($context["product"], "cart_id", array());
            echo "');\"><i class=\"fa fa-times-circle\"></i></button>
                  </span></div></td>
              <td class=\"text-right\">";
            // line 85
            echo $this->getAttribute($context["product"], "price", array());
            echo "</td>
              <td class=\"text-right\">";
            // line 86
            echo $this->getAttribute($context["product"], "total", array());
            echo "</td>
            </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 89
        echo "            ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["vouchers"]) ? $context["vouchers"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["voucher"]) {
            // line 90
            echo "            <tr>
              <td></td>
              <td class=\"text-left\">";
            // line 92
            echo $this->getAttribute($context["voucher"], "description", array());
            echo "</td>
              <td class=\"text-left\"></td>
              <td class=\"text-left\"><div class=\"input-group btn-block\" style=\"max-width: 200px;\">
                  <input type=\"text\" name=\"\" value=\"1\" size=\"1\" disabled=\"disabled\" class=\"form-control\" />
                  <span class=\"input-group-btn\">
                  <button type=\"button\" data-toggle=\"tooltip\" title=\"";
            // line 97
            echo (isset($context["button_remove"]) ? $context["button_remove"] : null);
            echo "\" class=\"btn btn-danger\" onclick=\"voucher.remove('";
            echo $this->getAttribute($context["voucher"], "key", array());
            echo "');\"><i class=\"fa fa-times-circle\"></i></button>
                  </span></div></td>
              <td class=\"text-right\">";
            // line 99
            echo $this->getAttribute($context["voucher"], "amount", array());
            echo "</td>
              <td class=\"text-right\">";
            // line 100
            echo $this->getAttribute($context["voucher"], "amount", array());
            echo "</td>
            </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['voucher'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 103
        echo "              </tbody>
            
          </table>
        </div>
      </form>
      ";
        // line 108
        if ((isset($context["modules"]) ? $context["modules"] : null)) {
            // line 109
            echo "      <h2>";
            echo (isset($context["text_next"]) ? $context["text_next"] : null);
            echo "</h2>
      <p>";
            // line 110
            echo (isset($context["text_next_choice"]) ? $context["text_next_choice"] : null);
            echo "</p>
      <div class=\"panel-group\" id=\"accordion\"> ";
            // line 111
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["modules"]) ? $context["modules"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                // line 112
                echo "        ";
                echo $context["module"];
                echo "
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 113
            echo " </div>
      ";
        }
        // line 114
        echo " <br />
      <div class=\"row\">
        <div class=\"col-sm-4 col-sm-offset-8\">
          <table class=\"table table-bordered\">
            ";
        // line 118
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["totals"]) ? $context["totals"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["total"]) {
            // line 119
            echo "            <tr>
              <td class=\"text-right\"><strong>";
            // line 120
            echo $this->getAttribute($context["total"], "title", array());
            echo ":</strong></td>
              <td class=\"text-right\">";
            // line 121
            echo $this->getAttribute($context["total"], "text", array());
            echo "</td>
            </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['total'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 124
        echo "          </table>
        </div>
      </div>
      <div class=\"buttons clearfix\">
        <div class=\"pull-left\"><a href=\"";
        // line 128
        echo (isset($context["continue"]) ? $context["continue"] : null);
        echo "\" class=\"btn btn-default\">";
        echo (isset($context["button_shopping"]) ? $context["button_shopping"] : null);
        echo "</a></div>
        <div class=\"pull-right\"><a href=\"index.php?route=checkout/checkout_address\" class=\"btn btn-primary\">";
        // line 129
        echo (isset($context["button_checkout"]) ? $context["button_checkout"] : null);
        echo "</a></div>
      </div>
      ";
        // line 131
        echo (isset($context["content_bottom"]) ? $context["content_bottom"] : null);
        echo "</div>
    ";
        // line 132
        echo (isset($context["column_right"]) ? $context["column_right"] : null);
        echo "</div>
   
   
</div>
<div class=\"cart-page\">
<div class=\"container\">
 <div class=\"row\">
         
        <div class=\"col-md-8\">
            <div class=\"cart-block\">
                <h3><i class=\"fa fa-cart-arrow-down\"></i>My cart</h3>
                <ul>
\t\t\t\t";
        // line 144
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 145
            echo "                    <li>
                        <div class=\"cart-sec\">
                        <div class=\"cart-img\">
                           <a href=\"\"><img src=\"";
            // line 148
            echo $this->getAttribute($context["product"], "thumb", array());
            echo "\"></a>
                        </div>
                        <div class=\"cart-cont\">
                            <div class=\"cart-det\">
                                <div class=\"cart-info\">
                                    <h6>";
            // line 153
            echo $this->getAttribute($context["product"], "name", array());
            echo "</h6>
\t\t\t\t\t\t\t\t\t";
            // line 154
            if ($this->getAttribute($context["product"], "option", array())) {
                // line 155
                echo "\t\t\t\t\t\t\t\t\t\t";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["product"], "option", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["option"]) {
                    // line 156
                    echo "\t\t\t\t\t\t\t\t\t\t<small>";
                    echo $this->getAttribute($context["option"], "name", array());
                    echo ": ";
                    echo $this->getAttribute($context["option"], "value", array());
                    echo "</small> ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 157
                echo "\t\t\t\t\t\t\t\t\t";
            }
            // line 158
            echo "                                    <small>8Gb RAM</small>
                                    <strong>";
            // line 159
            echo $this->getAttribute($context["product"], "price", array());
            echo "</strong>
                                    <small class=\"available-off-cart\"><strike>&#8377;2,06,000</strike>(4% OFF) <span>9 offer available<i class=\"fa fa-info\"></i></span></small>
                                    
                                </div>
                                <div class=\"cart-delivery\">
                                    <select name=\"type\" class=\"custom-select\">
                                      <option value=\"\">Regulary Delivery</option>
                                      <option value=\"\">Fast Delivery</option>
                                     </select>
                                     <span><small>Free</small>delivery by Aug 15</span>
                                </div>
                            </div>
                             <div class=\"cart-edit\">
                                <div class=\"cart-quantity\">
                                  <a href=\"\"><i class=\"fa fa-minus-square-o\"></i></a> <span>";
            // line 173
            echo $this->getAttribute($context["product"], "quantity", array());
            echo "</span>  <a href=\"\"><i class=\"fa fa-plus-square-o\"></i></a>
                                </div>
                                <div class=\"cart-remove\">
                                   <span><a href=\"\">Move to whitelist</a></span>
                                    <strong><a onclick=\"cart.remove('";
            // line 177
            echo $this->getAttribute($context["product"], "cart_id", array());
            echo "');\">Remove<i class=\"fa fa-trash\"></i></a></strong>
                                </div>
                            </div>
                        </div>
                        </div>
                        <div class=\"cart-protection\">
                            <div class=\"protection-img\">
                                <img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/protection-img.png\">
                            </div>
                            <div class=\"protection-cont\">
                                <h6>complete mobile protection</h6>
                                <strong>&#8377;1,06,000</strong>
                                    <small class=\"available-off-cart\"><strike>&#8377;2,06,000</strike> <span>(4% OFF)</span></small>
                                    <p>Brand authorised repair / replacement guarantee for 1 year.Special offer\"Get 6 month Google One trial with complete mobile protection\"<a href=\"\">Know More</a></p>
                                    <a href=\"\" class=\"btn btn-protect\">Add Protection</a>
                            </div>
                        </div>
                    </li>
\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 196
        echo "                </ul>
            </div>
            <div class=\"cart-block whitelist-block\">
                <h3><i class=\"fa fa-heart\"></i>My whitelist</h3>
                <ul>
                    <li>
                        <div class=\"cart-sec\">
                        <div class=\"cart-img\">
                           <a href=\"\"><img src=\"http://poorvikabeta.webindia.com/image/cache/catalog/demo/imac_1-90x120.jpg\"></a>
                        </div>
                        <div class=\"cart-cont\">
                            <div class=\"cart-det whitelist-border\">
                                <div class=\"cart-info\">
                                    <h6>Dell Laptop</h6>
                                    \t<div class=\"rating\">
\t\t\t\t\t\t    <div class=\"rating-show whitelist-size\">
\t\t\t\t\t\t        <h6>4.5</h6>
\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t<div class=\"rating-box whitelist-rating\">
\t\t\t\t\t\t\t";
        // line 215
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 5));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 216
            echo "\t\t\t\t\t\t\t\t";
            if (((isset($context["rating"]) ? $context["rating"] : null) < $context["i"])) {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            } else {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            }
            // line 217
            echo "\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 218
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                                    <strong>&#8377;1,06,000</strong>
                                   
                                    
                                </div>
                                <div class=\"cart-delivery move-cart\">
                                   <a href=\"\" class=\"btn btn-move-cart\">Move to Cart</a>
                                   <span><a href=\"\">Remove<i class=\"fa fa-trash\"></i></a></span>
                                </div>
                            </div>
                             
                        </div>
                        </div>
                    </li>
                     <li>
                        <div class=\"cart-sec\">
                        <div class=\"cart-img\">
                           <a href=\"\"><img src=\"http://poorvikabeta.webindia.com/image/cache/catalog/demo/imac_1-90x120.jpg\"></a>
                        </div>
                        <div class=\"cart-cont\">
                            <div class=\"cart-det whitelist-border\">
                                <div class=\"cart-info\">
                                    <h6>Dell Laptop</h6>
                                    \t<div class=\"rating\">
\t\t\t\t\t\t    <div class=\"rating-show whitelist-size\">
\t\t\t\t\t\t        <h6>4.5</h6>
\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t<div class=\"rating-box whitelist-rating\">
\t\t\t\t\t\t\t";
        // line 247
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 5));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 248
            echo "\t\t\t\t\t\t\t\t";
            if (((isset($context["rating"]) ? $context["rating"] : null) < $context["i"])) {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            } else {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            }
            // line 249
            echo "\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 250
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                                    <strong>&#8377;1,06,000</strong>
                                   
                                    
                                </div>
                                <div class=\"cart-delivery move-cart\">
                                   <a href=\"\" class=\"btn btn-move-cart\">Move to Cart</a>
                                   <span><a href=\"\">Remove<i class=\"fa fa-trash\"></i></a></span>
                                </div>
                            </div>
                             
                        </div>
                        </div>
                    </li>
                   
                </ul>
            </div>
        </div>
        <div class=\"col-md-4\">
            <div class=\"cart-block summary\">
            <h3>Payment Summary</h3>
            <ul>
\t\t\t\t";
        // line 273
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["totals"]) ? $context["totals"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["total"]) {
            // line 274
            echo "\t\t\t\t\t<li>
\t\t\t\t\t";
            // line 275
            if (($this->getAttribute($context["total"], "title", array()) == "Total")) {
                // line 276
                echo "\t\t\t\t\t\tPrice-2 items
\t\t\t\t\t";
            } else {
                // line 278
                echo "\t\t\t\t\t\t";
                echo $this->getAttribute($context["total"], "title", array());
                echo "
\t\t\t\t\t";
            }
            // line 280
            echo "\t\t\t\t\t  <span>";
            echo $this->getAttribute($context["total"], "text", array());
            echo "</span>
\t\t\t\t\t</li>
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['total'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 283
        echo "                 <li>Delivery Fee<span class=\"green-color\">Free</span></li>
                  <li>Coupon Discount<span><a href=\"\">Apply Coupon</a></span></li>
                  <li class=\"total-amount\">Total Amount<span>";
        // line 285
        echo (isset($context["total_amount"]) ? $context["total_amount"] : null);
        echo "</span></li>
                  <li class=\"save-offer\"><i class=\"fa fa-percent\"></i>You will save &#8377;6,000 on this Order</li>
            </ul>
            </div>
            <div class=\"gift-certify\">
                <h3><i class=\"fa fa-gift\"></i>Use Gift Certificate</h3>
                <input type=\"text\" name=\"voucher\" value=\"\" placeholder=\"Enter Gift Code\" id=\"input-voucher\">
\t\t\t\t<a class=\"btn btn-protect btn-gift\"><input type=\"submit\" value=\"Apply Gift Certificate\" id=\"button-voucher\" data-loading-text=\"Loading...\"></a>
            </div>
            <div class=\"checkout-button\">
                <a href=\"index.php?route=checkout/checkout_address\" class=\"btn btn-checkout\">Proceed To Checkout</a>
            </div>
        </div>
    </div>
</div>
</div>

<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName(\"tabcontent\");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = \"none\";
  }
  tablinks = document.getElementsByClassName(\"tablinks\");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(\" active\", \"\");
  }
  document.getElementById(cityName).style.display = \"block\";
  evt.currentTarget.className += \" active\";
}
</script>
";
        // line 317
        echo (isset($context["footer"]) ? $context["footer"] : null);
        echo " ";
    }

    public function getTemplateName()
    {
        return "default/template/checkout/cart.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  701 => 317,  666 => 285,  662 => 283,  652 => 280,  646 => 278,  642 => 276,  640 => 275,  637 => 274,  633 => 273,  608 => 250,  602 => 249,  595 => 248,  591 => 247,  560 => 218,  554 => 217,  547 => 216,  543 => 215,  522 => 196,  497 => 177,  490 => 173,  473 => 159,  470 => 158,  467 => 157,  457 => 156,  452 => 155,  450 => 154,  446 => 153,  438 => 148,  433 => 145,  429 => 144,  414 => 132,  410 => 131,  405 => 129,  399 => 128,  393 => 124,  384 => 121,  380 => 120,  377 => 119,  373 => 118,  367 => 114,  363 => 113,  354 => 112,  350 => 111,  346 => 110,  341 => 109,  339 => 108,  332 => 103,  323 => 100,  319 => 99,  312 => 97,  304 => 92,  300 => 90,  295 => 89,  286 => 86,  282 => 85,  275 => 83,  271 => 82,  267 => 80,  260 => 78,  257 => 77,  250 => 76,  246 => 75,  240 => 72,  232 => 71,  227 => 70,  223 => 69,  218 => 68,  215 => 67,  206 => 66,  199 => 65,  196 => 64,  188 => 63,  174 => 62,  171 => 61,  167 => 60,  159 => 55,  155 => 54,  151 => 53,  147 => 52,  143 => 51,  139 => 50,  131 => 45,  128 => 44,  122 => 43,  120 => 42,  116 => 41,  109 => 40,  106 => 39,  103 => 38,  100 => 37,  97 => 36,  94 => 35,  91 => 34,  89 => 33,  84 => 32,  76 => 28,  73 => 27,  65 => 23,  62 => 22,  54 => 18,  52 => 17,  49 => 16,  38 => 14,  34 => 13,  19 => 1,);
    }
}
/* {{ header }}*/
/* <style>*/
/* 	.cart-page{*/
/* 		margin-top: 120px !important;*/
/* 	}*/
/* 	#button-voucher{*/
/* 		border: none;*/
/* 		padding: 0;*/
/* 	}*/
/* </style>*/
/* <div id="checkout-cart" class="container">*/
/*   <ul class="breadcrumb">*/
/*     {% for breadcrumb in breadcrumbs %}*/
/*     <li><a href="{{ breadcrumb.href }}">{{ breadcrumb.text }}</a></li>*/
/*     {% endfor %}*/
/*   </ul>*/
/*   {% if attention %}*/
/*   <div class="alert alert-info"><i class="fa fa-info-circle"></i> {{ attention }}*/
/*     <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*   </div>*/
/*   {% endif %}*/
/*   {% if success %}*/
/*   <div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> {{ success }}*/
/*     <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*   </div>*/
/*   {% endif %}*/
/*   {% if error_warning %}*/
/*   <div class="alert alert-danger alert-dismissible"><i class="fa fa-exclamation-circle"></i> {{ error_warning }}*/
/*     <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*   </div>*/
/*   {% endif %}*/
/*   <div class="row">{{ column_left }}*/
/*     {% if column_left and column_right %}*/
/*     {% set class = 'col-sm-6' %}*/
/*     {% elseif column_left or column_right %}*/
/*     {% set class = 'col-sm-9' %}*/
/*     {% else %}*/
/*     {% set class = 'col-sm-12' %}*/
/*     {% endif %}*/
/*     <div id="content" class="{{ class }}">{{ content_top }}*/
/*       <h1>{{ heading_title }}*/
/*         {% if weight %}*/
/*         &nbsp;({{ weight }})*/
/*         {% endif %} </h1>*/
/*       <form action="{{ action }}" method="post" enctype="multipart/form-data">*/
/*         <div class="table-responsive">*/
/*           <table class="table table-bordered">*/
/*             <thead>*/
/*               <tr>*/
/*                 <td class="text-center">{{ column_image }}</td>*/
/*                 <td class="text-left">{{ column_name }}</td>*/
/*                 <td class="text-left">{{ column_model }}</td>*/
/*                 <td class="text-left">{{ column_quantity }}</td>*/
/*                 <td class="text-right">{{ column_price }}</td>*/
/*                 <td class="text-right">{{ column_total }}</td>*/
/*               </tr>*/
/*             </thead>*/
/*             <tbody>*/
/*             */
/*             {% for product in products %}*/
/*             <tr>*/
/*               <td class="text-center">{% if product.thumb %} <a href="{{ product.href }}"><img src="{{ product.thumb }}" alt="{{ product.name }}" title="{{ product.name }}" class="img-thumbnail" /></a> {% endif %}</td>*/
/*               <td class="text-left"><a href="{{ product.href }}">{{ product.name }}</a> {% if not product.stock %} <span class="text-danger">***</span> {% endif %}*/
/*                 {% if product.option %}*/
/*                 {% for option in product.option %} <br />*/
/*                 <small>{{ option.name }}: {{ option.value }}</small> {% endfor %}*/
/*                 {% endif %}*/
/*                 {% if product.reward %} <br />*/
/*                 <small>{{ product.reward }}</small> {% endif %}*/
/*                 {% if product.recurring %} <br />*/
/*                 <span class="label label-info">{{ text_recurring_item }}</span> <small>{{ product.recurring }}</small> {% endif %}</td>*/
/*               <td class="text-left">{{ product.model }}</td>*/
/*               <td class="text-left"><div class="input-group btn-block" style="max-width: 200px;">*/
/*                   */
/*                   {% if (quantity_status is defined and quantity_status) %} */
/*                     <input type="text" name="quantity[{{ product['cart_id'] }}]" value="{{ product['quantity'] }}" size="1" class="form-control" readonly/>*/
/*                   {% else %} */
/*                       <input type="text" name="quantity[{{ product['cart_id'] }}]" value="{{ product['quantity'] }}" size="1" class="form-control" />*/
/*                   {% endif %}*/
/*                 */
/*                   <span class="input-group-btn">*/
/*                   <button type="submit" data-toggle="tooltip" title="{{ button_update }}" class="btn btn-primary"><i class="fa fa-refresh"></i></button>*/
/*                   <button type="button" data-toggle="tooltip" title="{{ button_remove }}" class="btn btn-danger" onclick="cart.remove('{{ product.cart_id }}');"><i class="fa fa-times-circle"></i></button>*/
/*                   </span></div></td>*/
/*               <td class="text-right">{{ product.price }}</td>*/
/*               <td class="text-right">{{ product.total }}</td>*/
/*             </tr>*/
/*             {% endfor %}*/
/*             {% for voucher in vouchers %}*/
/*             <tr>*/
/*               <td></td>*/
/*               <td class="text-left">{{ voucher.description }}</td>*/
/*               <td class="text-left"></td>*/
/*               <td class="text-left"><div class="input-group btn-block" style="max-width: 200px;">*/
/*                   <input type="text" name="" value="1" size="1" disabled="disabled" class="form-control" />*/
/*                   <span class="input-group-btn">*/
/*                   <button type="button" data-toggle="tooltip" title="{{ button_remove }}" class="btn btn-danger" onclick="voucher.remove('{{ voucher.key }}');"><i class="fa fa-times-circle"></i></button>*/
/*                   </span></div></td>*/
/*               <td class="text-right">{{ voucher.amount }}</td>*/
/*               <td class="text-right">{{ voucher.amount }}</td>*/
/*             </tr>*/
/*             {% endfor %}*/
/*               </tbody>*/
/*             */
/*           </table>*/
/*         </div>*/
/*       </form>*/
/*       {% if modules %}*/
/*       <h2>{{ text_next }}</h2>*/
/*       <p>{{ text_next_choice }}</p>*/
/*       <div class="panel-group" id="accordion"> {% for module in modules %}*/
/*         {{ module }}*/
/*         {% endfor %} </div>*/
/*       {% endif %} <br />*/
/*       <div class="row">*/
/*         <div class="col-sm-4 col-sm-offset-8">*/
/*           <table class="table table-bordered">*/
/*             {% for total in totals %}*/
/*             <tr>*/
/*               <td class="text-right"><strong>{{ total.title }}:</strong></td>*/
/*               <td class="text-right">{{ total.text }}</td>*/
/*             </tr>*/
/*             {% endfor %}*/
/*           </table>*/
/*         </div>*/
/*       </div>*/
/*       <div class="buttons clearfix">*/
/*         <div class="pull-left"><a href="{{ continue }}" class="btn btn-default">{{ button_shopping }}</a></div>*/
/*         <div class="pull-right"><a href="index.php?route=checkout/checkout_address" class="btn btn-primary">{{ button_checkout }}</a></div>*/
/*       </div>*/
/*       {{ content_bottom }}</div>*/
/*     {{ column_right }}</div>*/
/*    */
/*    */
/* </div>*/
/* <div class="cart-page">*/
/* <div class="container">*/
/*  <div class="row">*/
/*          */
/*         <div class="col-md-8">*/
/*             <div class="cart-block">*/
/*                 <h3><i class="fa fa-cart-arrow-down"></i>My cart</h3>*/
/*                 <ul>*/
/* 				{% for product in products %}*/
/*                     <li>*/
/*                         <div class="cart-sec">*/
/*                         <div class="cart-img">*/
/*                            <a href=""><img src="{{ product.thumb }}"></a>*/
/*                         </div>*/
/*                         <div class="cart-cont">*/
/*                             <div class="cart-det">*/
/*                                 <div class="cart-info">*/
/*                                     <h6>{{ product.name }}</h6>*/
/* 									{% if product.option %}*/
/* 										{% for option in product.option %}*/
/* 										<small>{{ option.name }}: {{ option.value }}</small> {% endfor %}*/
/* 									{% endif %}*/
/*                                     <small>8Gb RAM</small>*/
/*                                     <strong>{{ product.price }}</strong>*/
/*                                     <small class="available-off-cart"><strike>&#8377;2,06,000</strike>(4% OFF) <span>9 offer available<i class="fa fa-info"></i></span></small>*/
/*                                     */
/*                                 </div>*/
/*                                 <div class="cart-delivery">*/
/*                                     <select name="type" class="custom-select">*/
/*                                       <option value="">Regulary Delivery</option>*/
/*                                       <option value="">Fast Delivery</option>*/
/*                                      </select>*/
/*                                      <span><small>Free</small>delivery by Aug 15</span>*/
/*                                 </div>*/
/*                             </div>*/
/*                              <div class="cart-edit">*/
/*                                 <div class="cart-quantity">*/
/*                                   <a href=""><i class="fa fa-minus-square-o"></i></a> <span>{{ product.quantity }}</span>  <a href=""><i class="fa fa-plus-square-o"></i></a>*/
/*                                 </div>*/
/*                                 <div class="cart-remove">*/
/*                                    <span><a href="">Move to whitelist</a></span>*/
/*                                     <strong><a onclick="cart.remove('{{ product.cart_id }}');">Remove<i class="fa fa-trash"></i></a></strong>*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                         </div>*/
/*                         <div class="cart-protection">*/
/*                             <div class="protection-img">*/
/*                                 <img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/protection-img.png">*/
/*                             </div>*/
/*                             <div class="protection-cont">*/
/*                                 <h6>complete mobile protection</h6>*/
/*                                 <strong>&#8377;1,06,000</strong>*/
/*                                     <small class="available-off-cart"><strike>&#8377;2,06,000</strike> <span>(4% OFF)</span></small>*/
/*                                     <p>Brand authorised repair / replacement guarantee for 1 year.Special offer"Get 6 month Google One trial with complete mobile protection"<a href="">Know More</a></p>*/
/*                                     <a href="" class="btn btn-protect">Add Protection</a>*/
/*                             </div>*/
/*                         </div>*/
/*                     </li>*/
/* 					{% endfor %}*/
/*                 </ul>*/
/*             </div>*/
/*             <div class="cart-block whitelist-block">*/
/*                 <h3><i class="fa fa-heart"></i>My whitelist</h3>*/
/*                 <ul>*/
/*                     <li>*/
/*                         <div class="cart-sec">*/
/*                         <div class="cart-img">*/
/*                            <a href=""><img src="http://poorvikabeta.webindia.com/image/cache/catalog/demo/imac_1-90x120.jpg"></a>*/
/*                         </div>*/
/*                         <div class="cart-cont">*/
/*                             <div class="cart-det whitelist-border">*/
/*                                 <div class="cart-info">*/
/*                                     <h6>Dell Laptop</h6>*/
/*                                     	<div class="rating">*/
/* 						    <div class="rating-show whitelist-size">*/
/* 						        <h6>4.5</h6>*/
/* 						    </div>*/
/* 							<div class="rating-box whitelist-rating">*/
/* 							{% for i in 1..5 %}*/
/* 								{% if rating < i %}<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>{% else %}<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>{% endif %}*/
/* 							{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                                     <strong>&#8377;1,06,000</strong>*/
/*                                    */
/*                                     */
/*                                 </div>*/
/*                                 <div class="cart-delivery move-cart">*/
/*                                    <a href="" class="btn btn-move-cart">Move to Cart</a>*/
/*                                    <span><a href="">Remove<i class="fa fa-trash"></i></a></span>*/
/*                                 </div>*/
/*                             </div>*/
/*                              */
/*                         </div>*/
/*                         </div>*/
/*                     </li>*/
/*                      <li>*/
/*                         <div class="cart-sec">*/
/*                         <div class="cart-img">*/
/*                            <a href=""><img src="http://poorvikabeta.webindia.com/image/cache/catalog/demo/imac_1-90x120.jpg"></a>*/
/*                         </div>*/
/*                         <div class="cart-cont">*/
/*                             <div class="cart-det whitelist-border">*/
/*                                 <div class="cart-info">*/
/*                                     <h6>Dell Laptop</h6>*/
/*                                     	<div class="rating">*/
/* 						    <div class="rating-show whitelist-size">*/
/* 						        <h6>4.5</h6>*/
/* 						    </div>*/
/* 							<div class="rating-box whitelist-rating">*/
/* 							{% for i in 1..5 %}*/
/* 								{% if rating < i %}<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>{% else %}<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>{% endif %}*/
/* 							{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                                     <strong>&#8377;1,06,000</strong>*/
/*                                    */
/*                                     */
/*                                 </div>*/
/*                                 <div class="cart-delivery move-cart">*/
/*                                    <a href="" class="btn btn-move-cart">Move to Cart</a>*/
/*                                    <span><a href="">Remove<i class="fa fa-trash"></i></a></span>*/
/*                                 </div>*/
/*                             </div>*/
/*                              */
/*                         </div>*/
/*                         </div>*/
/*                     </li>*/
/*                    */
/*                 </ul>*/
/*             </div>*/
/*         </div>*/
/*         <div class="col-md-4">*/
/*             <div class="cart-block summary">*/
/*             <h3>Payment Summary</h3>*/
/*             <ul>*/
/* 				{% for total in totals %}*/
/* 					<li>*/
/* 					{% if total.title == "Total" %}*/
/* 						Price-2 items*/
/* 					{% else %}*/
/* 						{{ total.title }}*/
/* 					{% endif %}*/
/* 					  <span>{{ total.text }}</span>*/
/* 					</li>*/
/* 				{% endfor %}*/
/*                  <li>Delivery Fee<span class="green-color">Free</span></li>*/
/*                   <li>Coupon Discount<span><a href="">Apply Coupon</a></span></li>*/
/*                   <li class="total-amount">Total Amount<span>{{ total_amount }}</span></li>*/
/*                   <li class="save-offer"><i class="fa fa-percent"></i>You will save &#8377;6,000 on this Order</li>*/
/*             </ul>*/
/*             </div>*/
/*             <div class="gift-certify">*/
/*                 <h3><i class="fa fa-gift"></i>Use Gift Certificate</h3>*/
/*                 <input type="text" name="voucher" value="" placeholder="Enter Gift Code" id="input-voucher">*/
/* 				<a class="btn btn-protect btn-gift"><input type="submit" value="Apply Gift Certificate" id="button-voucher" data-loading-text="Loading..."></a>*/
/*             </div>*/
/*             <div class="checkout-button">*/
/*                 <a href="index.php?route=checkout/checkout_address" class="btn btn-checkout">Proceed To Checkout</a>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* </div>*/
/* */
/* <script>*/
/* function openCity(evt, cityName) {*/
/*   var i, tabcontent, tablinks;*/
/*   tabcontent = document.getElementsByClassName("tabcontent");*/
/*   for (i = 0; i < tabcontent.length; i++) {*/
/*     tabcontent[i].style.display = "none";*/
/*   }*/
/*   tablinks = document.getElementsByClassName("tablinks");*/
/*   for (i = 0; i < tablinks.length; i++) {*/
/*     tablinks[i].className = tablinks[i].className.replace(" active", "");*/
/*   }*/
/*   document.getElementById(cityName).style.display = "block";*/
/*   evt.currentTarget.className += " active";*/
/* }*/
/* </script>*/
/* {{ footer }} */
