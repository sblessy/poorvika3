<?php

/* default/template/checkout/confirm.twig */
class __TwigTemplate_8b266f6f24d38f802088f4d3569bc56c4bbdaf434583e4eff5a1070c5f4b8b5c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ( !(isset($context["redirect"]) ? $context["redirect"] : null)) {
            // line 2
            echo "\t<ul class=\"none-boxshadow\">
        ";
            // line 3
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                echo "  
\t\t\t<li>
\t\t\t\t<div class=\"payment-img\">
\t\t\t\t\t<img src=\"";
                // line 6
                echo $this->getAttribute($context["product"], "thumb", array());
                echo "\">
\t\t\t\t</div>
\t\t\t\t<div class=\"payment-cont\">
\t\t\t\t\t<span>";
                // line 9
                echo $this->getAttribute($context["product"], "name", array());
                echo "</span>
\t\t\t\t\t<strong>";
                // line 10
                echo $this->getAttribute($context["product"], "price", array());
                echo "</strong>
\t\t\t\t</div>
\t\t\t</li>
\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 13
            echo "\t
\t</ul>
\t";
            // line 15
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["vouchers"]) ? $context["vouchers"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["voucher"]) {
                // line 16
                echo "    <ul>
      <li class=\"text-left\">";
                // line 17
                echo $this->getAttribute($context["voucher"], "description", array());
                echo "</li>
      <li class=\"text-left\"></li>
      <li class=\"text-right\">1</li>
      <li class=\"text-right\">";
                // line 20
                echo $this->getAttribute($context["voucher"], "amount", array());
                echo "</li>
      <li class=\"text-right\">";
                // line 21
                echo $this->getAttribute($context["voucher"], "amount", array());
                echo "</li>
    </ul>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['voucher'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 24
            echo "\t<ul>     
\t\t<li>Delivery Fee<span class=\"green-color\">Free</span></li>
\t\t<li>Coupon Discount<span><a href=\"\">Apply Coupon</a></span></li>
\t\t";
            // line 27
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["totals"]) ? $context["totals"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["total"]) {
                // line 28
                echo "\t\t\t<li class=\"total-amount\">";
                echo $this->getAttribute($context["total"], "title", array());
                echo "<span>";
                echo $this->getAttribute($context["total"], "text", array());
                echo "</span></li>
\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['total'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 30
            echo "\t\t<li class=\"save-offer\"><i class=\"fa fa-percent\"></i>You will save &#8377;6,000 on this Order</li>
\t</ul>
\t\t\t
\t
";
            // line 34
            echo (isset($context["payment"]) ? $context["payment"] : null);
            echo "
";
        } else {
            // line 35
            echo " 
<script type=\"text/javascript\"><!--
location = '";
            // line 37
            echo (isset($context["redirect"]) ? $context["redirect"] : null);
            echo "';
//--></script> 
";
        }
        // line 39
        echo " ";
    }

    public function getTemplateName()
    {
        return "default/template/checkout/confirm.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  123 => 39,  117 => 37,  113 => 35,  108 => 34,  102 => 30,  91 => 28,  87 => 27,  82 => 24,  73 => 21,  69 => 20,  63 => 17,  60 => 16,  56 => 15,  52 => 13,  42 => 10,  38 => 9,  32 => 6,  24 => 3,  21 => 2,  19 => 1,);
    }
}
/* {% if not redirect %}*/
/* 	<ul class="none-boxshadow">*/
/*         {% for product in products %}  */
/* 			<li>*/
/* 				<div class="payment-img">*/
/* 					<img src="{{ product.thumb }}">*/
/* 				</div>*/
/* 				<div class="payment-cont">*/
/* 					<span>{{ product.name }}</span>*/
/* 					<strong>{{ product.price }}</strong>*/
/* 				</div>*/
/* 			</li>*/
/* 	{% endfor %}	*/
/* 	</ul>*/
/* 	{% for voucher in vouchers %}*/
/*     <ul>*/
/*       <li class="text-left">{{ voucher.description }}</li>*/
/*       <li class="text-left"></li>*/
/*       <li class="text-right">1</li>*/
/*       <li class="text-right">{{ voucher.amount }}</li>*/
/*       <li class="text-right">{{ voucher.amount }}</li>*/
/*     </ul>*/
/*     {% endfor %}*/
/* 	<ul>     */
/* 		<li>Delivery Fee<span class="green-color">Free</span></li>*/
/* 		<li>Coupon Discount<span><a href="">Apply Coupon</a></span></li>*/
/* 		{% for total in totals %}*/
/* 			<li class="total-amount">{{ total.title }}<span>{{ total.text }}</span></li>*/
/* 		{% endfor %}*/
/* 		<li class="save-offer"><i class="fa fa-percent"></i>You will save &#8377;6,000 on this Order</li>*/
/* 	</ul>*/
/* 			*/
/* 	*/
/* {{ payment }}*/
/* {% else %} */
/* <script type="text/javascript"><!--*/
/* location = '{{ redirect }}';*/
/* //--></script> */
/* {% endif %} */
