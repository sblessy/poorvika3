<?php

/* so-destino/template/product/manufacturer_info.twig */
class __TwigTemplate_61db0f013eab62b226a462192209d99f15e95a25f632a94a99b63fae9342453c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo "


<div class=\"container\">
  <ul class=\"breadcrumb\">
    ";
        // line 6
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 7
            echo "    <li><a href=\"";
            echo $this->getAttribute($context["breadcrumb"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array());
            echo "</a></li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 9
        echo "  </ul>
  <div class=\"row\">";
        // line 10
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "
    ";
        // line 11
        if (((isset($context["column_left"]) ? $context["column_left"] : null) && (isset($context["column_right"]) ? $context["column_right"] : null))) {
            // line 12
            echo "    ";
            $context["class"] = "col-sm-6";
            // line 13
            echo "    ";
        } elseif (((isset($context["column_left"]) ? $context["column_left"] : null) || (isset($context["column_right"]) ? $context["column_right"] : null))) {
            // line 14
            echo "    ";
            $context["class"] = "col-sm-9";
            // line 15
            echo "    ";
        } else {
            // line 16
            echo "    ";
            $context["class"] = "col-sm-12";
            // line 17
            echo "    ";
        }
        // line 18
        echo "    <div id=\"content\" class=\"";
        echo (isset($context["class"]) ? $context["class"] : null);
        echo "\">";
        echo (isset($context["content_top"]) ? $context["content_top"] : null);
        echo "
\t\t<h2>";
        // line 19
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h2>
\t\t";
        // line 20
        if ((isset($context["products"]) ? $context["products"] : null)) {
            // line 21
            echo "\t\t\t";
            // line 22
            echo "            <div class=\"products-category\">
                ";
            // line 23
            $this->loadTemplate(((isset($context["theme_directory"]) ? $context["theme_directory"] : null) . "/template/soconfig/listing.twig"), "so-destino/template/product/manufacturer_info.twig", 23)->display(array_merge($context, array("listingType" => (isset($context["listingType"]) ? $context["listingType"] : null))));
            // line 24
            echo "            </div>
\t\t";
        } else {
            // line 26
            echo "\t\t\t<p>";
            echo (isset($context["text_empty"]) ? $context["text_empty"] : null);
            echo "</p>
\t\t\t<div class=\"buttons\">
\t\t\t\t<div class=\"pull-right\"><a href=\"";
            // line 28
            echo (isset($context["continue"]) ? $context["continue"] : null);
            echo "\" class=\"btn btn-primary\">";
            echo (isset($context["button_continue"]) ? $context["button_continue"] : null);
            echo "</a></div>
\t\t\t</div>
\t\t";
        }
        // line 31
        echo "\t\t";
        echo (isset($context["content_bottom"]) ? $context["content_bottom"] : null);
        echo "</div>
    ";
        // line 32
        echo (isset($context["column_right"]) ? $context["column_right"] : null);
        echo "</div>
</div>

                      ";
        // line 35
        if ((isset($context["wk_preorder_pro_status"]) ? $context["wk_preorder_pro_status"] : null)) {
            // line 36
            echo "                         <script>
                             function addToCart(id) {
                               \$('.preorder-alert').remove();
                               \$.ajax({
                                   url:'index.php?route=checkout/precart/customer_preordered',
                                   data: {
                                       product_id: id,
                                   },
                                   type:'post',
                                   dataType: 'json',
                                   beforeSend: function() {

                                   },
                                   success:function(response) {
                                       if(response.success) {
                                           cart.add(id);
                                       } else {
                                           \$('.breadcrumb').after('<div class=\"alert alert-danger preorder-alert\"><i class=\"fa fa-exclamation-circle\"></i>'+response.msg+'<button class=\"close\" data-dismiss=\"alert\" type=\"button\">&times;</button></div>');
                                           \$('html, body').animate({ scrollTop: 0 }, 'slow');
                                       }
                                   }
                               });
                             }
                         </script>
                   ";
        }
        // line 61
        echo "                  
";
        // line 62
        echo (isset($context["footer"]) ? $context["footer"] : null);
    }

    public function getTemplateName()
    {
        return "so-destino/template/product/manufacturer_info.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  150 => 62,  147 => 61,  120 => 36,  118 => 35,  112 => 32,  107 => 31,  99 => 28,  93 => 26,  89 => 24,  87 => 23,  84 => 22,  82 => 21,  80 => 20,  76 => 19,  69 => 18,  66 => 17,  63 => 16,  60 => 15,  57 => 14,  54 => 13,  51 => 12,  49 => 11,  45 => 10,  42 => 9,  31 => 7,  27 => 6,  19 => 1,);
    }
}
/* {{ header }}*/
/* */
/* */
/* <div class="container">*/
/*   <ul class="breadcrumb">*/
/*     {% for breadcrumb in breadcrumbs %}*/
/*     <li><a href="{{ breadcrumb.href }}">{{ breadcrumb.text }}</a></li>*/
/*     {% endfor %}*/
/*   </ul>*/
/*   <div class="row">{{ column_left }}*/
/*     {% if column_left and column_right %}*/
/*     {% set class = 'col-sm-6' %}*/
/*     {% elseif column_left or column_right %}*/
/*     {% set class = 'col-sm-9' %}*/
/*     {% else %}*/
/*     {% set class = 'col-sm-12' %}*/
/*     {% endif %}*/
/*     <div id="content" class="{{ class }}">{{ content_top }}*/
/* 		<h2>{{ heading_title }}</h2>*/
/* 		{% if products %}*/
/* 			{#==== Product Listing ==== #}*/
/*             <div class="products-category">*/
/*                 {% include theme_directory~'/template/soconfig/listing.twig' with {listingType: listingType} %}*/
/*             </div>*/
/* 		{% else %}*/
/* 			<p>{{ text_empty }}</p>*/
/* 			<div class="buttons">*/
/* 				<div class="pull-right"><a href="{{ continue }}" class="btn btn-primary">{{ button_continue }}</a></div>*/
/* 			</div>*/
/* 		{% endif %}*/
/* 		{{ content_bottom }}</div>*/
/*     {{ column_right }}</div>*/
/* </div>*/
/* */
/*                       {% if wk_preorder_pro_status %}*/
/*                          <script>*/
/*                              function addToCart(id) {*/
/*                                $('.preorder-alert').remove();*/
/*                                $.ajax({*/
/*                                    url:'index.php?route=checkout/precart/customer_preordered',*/
/*                                    data: {*/
/*                                        product_id: id,*/
/*                                    },*/
/*                                    type:'post',*/
/*                                    dataType: 'json',*/
/*                                    beforeSend: function() {*/
/* */
/*                                    },*/
/*                                    success:function(response) {*/
/*                                        if(response.success) {*/
/*                                            cart.add(id);*/
/*                                        } else {*/
/*                                            $('.breadcrumb').after('<div class="alert alert-danger preorder-alert"><i class="fa fa-exclamation-circle"></i>'+response.msg+'<button class="close" data-dismiss="alert" type="button">&times;</button></div>');*/
/*                                            $('html, body').animate({ scrollTop: 0 }, 'slow');*/
/*                                        }*/
/*                                    }*/
/*                                });*/
/*                              }*/
/*                          </script>*/
/*                    {% endif %}*/
/*                   */
/* {{ footer }}*/
