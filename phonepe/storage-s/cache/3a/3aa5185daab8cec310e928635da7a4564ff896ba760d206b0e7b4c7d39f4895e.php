<?php

/* catalog/oaplan_form.twig */
class __TwigTemplate_d63d8b443d34348595f602fac213d4425a2e832ded5e31059b4cb85b821b1eae extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo "

";
        // line 3
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\">
        <button type=\"submit\" form=\"form-category\" data-toggle=\"tooltip\" title=\"";
        // line 8
        echo (isset($context["button_save"]) ? $context["button_save"] : null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i></button>
        <a href=\"";
        // line 9
        echo (isset($context["cancel"]) ? $context["cancel"] : null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo (isset($context["button_cancel"]) ? $context["button_cancel"] : null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
      <h1>";
        // line 10
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
      <ul class=\"breadcrumb\">
        ";
        // line 12
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 13
            echo "          <li><a href=\"";
            echo $this->getAttribute($context["breadcrumb"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array());
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 15
        echo "      </ul>
    </div>
  </div>
  <div class=\"container-fluid\">
    ";
        // line 19
        if ((isset($context["error_warning"]) ? $context["error_warning"] : null)) {
            // line 20
            echo "      <div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo (isset($context["error_warning"]) ? $context["error_warning"] : null);
            echo "
        <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
      </div>
    ";
        }
        // line 24
        echo "    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <h3 class=\"panel-title\"><i class=\"fa fa-pencil\"></i> ";
        // line 26
        echo (isset($context["text_form"]) ? $context["text_form"] : null);
        echo "</h3>
      </div>
      <div class=\"panel-body\">
        <form action=\"";
        // line 29
        echo (isset($context["action"]) ? $context["action"] : null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-category\" class=\"form-horizontal\">
          <ul class=\"nav nav-tabs\">
            <li class=\"active\"><a href=\"#tab-general\" data-toggle=\"tab\">";
        // line 31
        echo (isset($context["tab_general"]) ? $context["tab_general"] : null);
        echo "</a></li>
          </ul>
          <div class=\"tab-content\">
            <div class=\"tab-pane active\" id=\"tab-general\">
              <ul class=\"nav nav-tabs\" id=\"language\">
\t\t\t  ";
        // line 36
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 37
            echo "                <li><a href=\"#language";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" data-toggle=\"tab\"><img src=\"language/";
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /> ";
            echo $this->getAttribute($context["language"], "name", array());
            echo "</a></li>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 39
        echo "              </ul>
              <div class=\"tab-content\">
                ";
        // line 41
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 42
            echo "                <div class=\"tab-pane\" id=\"language";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\">
                  <div class=\"form-group required\">
                    <label class=\"col-sm-2 control-label\" for=\"input-plancode";
            // line 44
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\">Plan Code</label>
                    <div class=\"col-sm-10\">
                      <input type=\"text\" name=\"plan_code\" value=\"";
            // line 46
            if ($this->getAttribute((isset($context["oneassist_plans"]) ? $context["oneassist_plans"] : null), "plan_code", array())) {
                echo " ";
                echo $this->getAttribute((isset($context["oneassist_plans"]) ? $context["oneassist_plans"] : null), "plan_code", array());
                echo " ";
            }
            echo "\" placeholder=\"Plan Code\" id=\"plancode\" class=\"form-control\">
\t\t\t\t\t  ";
            // line 47
            if ((isset($context["error_plancode"]) ? $context["error_plancode"] : null)) {
                // line 48
                echo "                      <div class=\"text-danger\">";
                echo (isset($context["error_plancode"]) ? $context["error_plancode"] : null);
                echo "</div>
                      ";
            }
            // line 50
            echo "                    </div>
                  </div>
                  <div class=\"form-group required\">
                    <label class=\"col-sm-2 control-label\" for=\"input-apx_itemcode";
            // line 53
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\">APX Item Code</label>
                    <div class=\"col-sm-10\">
                      <input type=\"text\" name=\"apx_itemcode\" value=\"";
            // line 55
            if ($this->getAttribute((isset($context["oneassist_plans"]) ? $context["oneassist_plans"] : null), "apx_itemcode", array())) {
                echo " ";
                echo $this->getAttribute((isset($context["oneassist_plans"]) ? $context["oneassist_plans"] : null), "apx_itemcode", array());
                echo " ";
            }
            echo "\" placeholder=\"APX Item Code\" id=\"apx_itemcode\" class=\"form-control\">
\t\t\t\t\t  ";
            // line 56
            if ((isset($context["error_apx_itemcode"]) ? $context["error_apx_itemcode"] : null)) {
                // line 57
                echo "                      <div class=\"text-danger\">";
                echo (isset($context["error_apx_itemcode"]) ? $context["error_apx_itemcode"] : null);
                echo "</div>
                      ";
            }
            // line 59
            echo "                    </div>
                  </div>
                  <div class=\"form-group required\">
                    <label class=\"col-sm-2 control-label\" for=\"input-category";
            // line 62
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\">Category</label>
                    <div class=\"col-sm-10\">
                      <select name=\"category_id\" class=\"form-control\">
                          <option value=\"\">Select Category</option>
\t\t\t\t\t\t  ";
            // line 66
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 67
                echo "                          
                          <option value=\"";
                // line 68
                echo $this->getAttribute($context["category"], "category_id", array());
                echo "\" ";
                if (($this->getAttribute((isset($context["oneassist_plans"]) ? $context["oneassist_plans"] : null), "category_id", array()) == $this->getAttribute($context["category"], "category_id", array()))) {
                    echo " ";
                    echo "selected";
                    echo " ";
                }
                echo ">";
                echo $this->getAttribute($context["category"], "name", array());
                echo "</option>
                          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 70
            echo "                      </select>
\t\t\t\t\t  ";
            // line 71
            if ((isset($context["error_category"]) ? $context["error_category"] : null)) {
                // line 72
                echo "                      <div class=\"text-danger\">";
                echo (isset($context["error_category"]) ? $context["error_category"] : null);
                echo "</div>
                      ";
            }
            // line 74
            echo "                    </div>
                  </div>
                  <div class=\"form-group required\">
                    <label class=\"col-sm-2 control-label\" for=\"input-plantype";
            // line 77
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\">Type</label>
                    <div class=\"col-sm-10\">
                      <select name=\"type\" class=\"form-control\">
                          <option value=\"\">Select Type</option>
                          <option value=\"ADLD\" ";
            // line 81
            if (($this->getAttribute((isset($context["oneassist_plans"]) ? $context["oneassist_plans"] : null), "type", array()) == "ADLD")) {
                echo " ";
                echo "selected";
                echo " ";
            }
            echo ">Accidental and Liquid Damage Protection Plan</option>
                          <option value=\"Extended Warranty\" ";
            // line 82
            if (($this->getAttribute((isset($context["oneassist_plans"]) ? $context["oneassist_plans"] : null), "type", array()) == "Extended Warranty")) {
                echo " ";
                echo "selected";
                echo " ";
            }
            echo ">Extended Warranty</option>
                      </select>
\t\t\t\t\t  ";
            // line 84
            if ((isset($context["error_plantype"]) ? $context["error_plantype"] : null)) {
                // line 85
                echo "                      <div class=\"text-danger\">";
                echo (isset($context["error_plantype"]) ? $context["error_plantype"] : null);
                echo "</div>
                      ";
            }
            // line 87
            echo "                    </div>
                  </div>
                  <div class=\"form-group required\">
                    <label class=\"col-sm-2 control-label\" for=\"input-brand";
            // line 90
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\">Brand</label>
                    <div class=\"col-sm-10\">
                      <select name=\"brand\" class=\"form-control\">
                          <option value=\"\">Select Brand</option>
                          <option value=\"Non Apple\" ";
            // line 94
            if (($this->getAttribute((isset($context["oneassist_plans"]) ? $context["oneassist_plans"] : null), "brand", array()) == "Non Apple")) {
                echo " ";
                echo "selected";
                echo " ";
            }
            echo ">Non Apple</option>
                          <option value=\"Apple\" ";
            // line 95
            if (($this->getAttribute((isset($context["oneassist_plans"]) ? $context["oneassist_plans"] : null), "brand", array()) == "Apple")) {
                echo " ";
                echo "selected";
                echo " ";
            }
            echo ">Apple</option>
                      </select>
\t\t\t\t\t  ";
            // line 97
            if ((isset($context["error_brand"]) ? $context["error_brand"] : null)) {
                // line 98
                echo "                      <div class=\"text-danger\">";
                echo (isset($context["error_brand"]) ? $context["error_brand"] : null);
                echo "</div>
                      ";
            }
            // line 100
            echo "
                    </div>
                  </div>
                  <div class=\"form-group required\">
                    <label class=\"col-sm-2 control-label\" for=\"input-planname";
            // line 104
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\">Plan Name</label>
                    <div class=\"col-sm-10\">
                      <input type=\"text\" name=\"plan_name\" value=\"";
            // line 106
            if ($this->getAttribute((isset($context["oneassist_plans"]) ? $context["oneassist_plans"] : null), "plan_name", array())) {
                echo " ";
                echo $this->getAttribute((isset($context["oneassist_plans"]) ? $context["oneassist_plans"] : null), "plan_name", array());
                echo " ";
            }
            echo "\" placeholder=\"Plan Name\" id=\"plan_name\" class=\"form-control\">
                      ";
            // line 107
            if ((isset($context["error_plan_name"]) ? $context["error_plan_name"] : null)) {
                // line 108
                echo "                      <div class=\"text-danger\">";
                echo (isset($context["error_plan_name"]) ? $context["error_plan_name"] : null);
                echo "</div>
                      ";
            }
            // line 110
            echo "                    </div>
                  </div>
                  <div class=\"form-group required\">
                    <label class=\"col-sm-2 control-label\" for=\"input-fromprice<?php echo \$language['language_id']; ?>\">From Price</label>
                    <div class=\"col-sm-10\">
                      <input type=\"text\" name=\"from_price\" value=\"";
            // line 115
            if ($this->getAttribute((isset($context["oneassist_plans"]) ? $context["oneassist_plans"] : null), "from_price", array())) {
                echo " ";
                echo $this->getAttribute((isset($context["oneassist_plans"]) ? $context["oneassist_plans"] : null), "from_price", array());
                echo " ";
            }
            echo "\" placeholder=\"From Price\" id=\"fromprice\" class=\"form-control\">
                      ";
            // line 116
            if ((isset($context["error_fromprice"]) ? $context["error_fromprice"] : null)) {
                // line 117
                echo "                      <div class=\"text-danger\">";
                echo (isset($context["error_fromprice"]) ? $context["error_fromprice"] : null);
                echo "</div>
                      ";
            }
            // line 119
            echo "                    </div>
                  </div>
                  <div class=\"form-group required\">
                    <label class=\"col-sm-2 control-label\" for=\"input-toprice<?php echo \$language['language_id']; ?>\">To Price</label>
                    <div class=\"col-sm-10\">
                      <input type=\"text\" name=\"to_price\" value=\"";
            // line 124
            if ($this->getAttribute((isset($context["oneassist_plans"]) ? $context["oneassist_plans"] : null), "to_price", array())) {
                echo " ";
                echo $this->getAttribute((isset($context["oneassist_plans"]) ? $context["oneassist_plans"] : null), "to_price", array());
                echo " ";
            }
            echo "\" placeholder=\"To Price\" id=\"toprice\" class=\"form-control\">
                      ";
            // line 125
            if ((isset($context["error_toprice"]) ? $context["error_toprice"] : null)) {
                // line 126
                echo "                      <div class=\"text-danger\">";
                echo (isset($context["error_toprice"]) ? $context["error_toprice"] : null);
                echo "</div>
                      ";
            }
            // line 128
            echo "                    </div>
                  </div>
                  <div class=\"form-group required\">
                    <label class=\"col-sm-2 control-label\" for=\"input-mrp";
            // line 131
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\">Poorvika MRP</label>
                    <div class=\"col-sm-10\">
                      <input type=\"text\" name=\"poorvika_mrp\" value=\"";
            // line 133
            if ($this->getAttribute((isset($context["oneassist_plans"]) ? $context["oneassist_plans"] : null), "poorvika_mrp", array())) {
                echo " ";
                echo $this->getAttribute((isset($context["oneassist_plans"]) ? $context["oneassist_plans"] : null), "poorvika_mrp", array());
                echo " ";
            }
            echo "\" placeholder=\"Poorvika MRP\" id=\"mrp\" class=\"form-control\">
                      ";
            // line 134
            if ((isset($context["error_mrp"]) ? $context["error_mrp"] : null)) {
                // line 135
                echo "                      <div class=\"text-danger\">";
                echo (isset($context["error_mrp"]) ? $context["error_mrp"] : null);
                echo "</div>
                      ";
            }
            // line 137
            echo "                    </div>
                  </div>

                </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 142
        echo "              </div>
            </div>

            
          </div>
        </form>
      </div>
    </div>
  </div>
  <script type=\"text/javascript\" src=\"view/javascript/summernote/summernote.js\"></script>
  <link href=\"view/javascript/summernote/summernote.css\" rel=\"stylesheet\" />
  <script type=\"text/javascript\" src=\"view/javascript/summernote/opencart.js\"></script> 
  <script type=\"text/javascript\"><!--
\$('#language a:first').tab('show');
//--></script></div>
";
        // line 157
        echo (isset($context["footer"]) ? $context["footer"] : null);
    }

    public function getTemplateName()
    {
        return "catalog/oaplan_form.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  429 => 157,  412 => 142,  402 => 137,  396 => 135,  394 => 134,  386 => 133,  381 => 131,  376 => 128,  370 => 126,  368 => 125,  360 => 124,  353 => 119,  347 => 117,  345 => 116,  337 => 115,  330 => 110,  324 => 108,  322 => 107,  314 => 106,  309 => 104,  303 => 100,  297 => 98,  295 => 97,  286 => 95,  278 => 94,  271 => 90,  266 => 87,  260 => 85,  258 => 84,  249 => 82,  241 => 81,  234 => 77,  229 => 74,  223 => 72,  221 => 71,  218 => 70,  202 => 68,  199 => 67,  195 => 66,  188 => 62,  183 => 59,  177 => 57,  175 => 56,  167 => 55,  162 => 53,  157 => 50,  151 => 48,  149 => 47,  141 => 46,  136 => 44,  130 => 42,  126 => 41,  122 => 39,  105 => 37,  101 => 36,  93 => 31,  88 => 29,  82 => 26,  78 => 24,  70 => 20,  68 => 19,  62 => 15,  51 => 13,  47 => 12,  42 => 10,  36 => 9,  32 => 8,  24 => 3,  19 => 1,);
    }
}
/* {{ header }}*/
/* */
/* {{ column_left }}*/
/* <div id="content">*/
/*   <div class="page-header">*/
/*     <div class="container-fluid">*/
/*       <div class="pull-right">*/
/*         <button type="submit" form="form-category" data-toggle="tooltip" title="{{ button_save }}" class="btn btn-primary"><i class="fa fa-save"></i></button>*/
/*         <a href="{{ cancel }}" data-toggle="tooltip" title="{{ button_cancel }}" class="btn btn-default"><i class="fa fa-reply"></i></a></div>*/
/*       <h1>{{ heading_title }}</h1>*/
/*       <ul class="breadcrumb">*/
/*         {% for breadcrumb in breadcrumbs %}*/
/*           <li><a href="{{ breadcrumb.href }}">{{ breadcrumb.text }}</a></li>*/
/*         {% endfor %}*/
/*       </ul>*/
/*     </div>*/
/*   </div>*/
/*   <div class="container-fluid">*/
/*     {% if error_warning %}*/
/*       <div class="alert alert-danger alert-dismissible"><i class="fa fa-exclamation-circle"></i> {{ error_warning }}*/
/*         <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*       </div>*/
/*     {% endif %}*/
/*     <div class="panel panel-default">*/
/*       <div class="panel-heading">*/
/*         <h3 class="panel-title"><i class="fa fa-pencil"></i> {{ text_form }}</h3>*/
/*       </div>*/
/*       <div class="panel-body">*/
/*         <form action="{{ action }}" method="post" enctype="multipart/form-data" id="form-category" class="form-horizontal">*/
/*           <ul class="nav nav-tabs">*/
/*             <li class="active"><a href="#tab-general" data-toggle="tab">{{ tab_general }}</a></li>*/
/*           </ul>*/
/*           <div class="tab-content">*/
/*             <div class="tab-pane active" id="tab-general">*/
/*               <ul class="nav nav-tabs" id="language">*/
/* 			  {% for language in languages %}*/
/*                 <li><a href="#language{{ language.language_id }}" data-toggle="tab"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /> {{ language.name }}</a></li>*/
/*                 {% endfor %}*/
/*               </ul>*/
/*               <div class="tab-content">*/
/*                 {% for language in languages %}*/
/*                 <div class="tab-pane" id="language{{ language.language_id }}">*/
/*                   <div class="form-group required">*/
/*                     <label class="col-sm-2 control-label" for="input-plancode{{ language.language_id }}">Plan Code</label>*/
/*                     <div class="col-sm-10">*/
/*                       <input type="text" name="plan_code" value="{% if oneassist_plans.plan_code %} {{ oneassist_plans.plan_code }} {% endif %}" placeholder="Plan Code" id="plancode" class="form-control">*/
/* 					  {% if error_plancode %}*/
/*                       <div class="text-danger">{{ error_plancode }}</div>*/
/*                       {% endif %}*/
/*                     </div>*/
/*                   </div>*/
/*                   <div class="form-group required">*/
/*                     <label class="col-sm-2 control-label" for="input-apx_itemcode{{ language.language_id }}">APX Item Code</label>*/
/*                     <div class="col-sm-10">*/
/*                       <input type="text" name="apx_itemcode" value="{% if oneassist_plans.apx_itemcode %} {{ oneassist_plans.apx_itemcode }} {% endif %}" placeholder="APX Item Code" id="apx_itemcode" class="form-control">*/
/* 					  {% if error_apx_itemcode %}*/
/*                       <div class="text-danger">{{ error_apx_itemcode }}</div>*/
/*                       {% endif %}*/
/*                     </div>*/
/*                   </div>*/
/*                   <div class="form-group required">*/
/*                     <label class="col-sm-2 control-label" for="input-category{{ language.language_id }}">Category</label>*/
/*                     <div class="col-sm-10">*/
/*                       <select name="category_id" class="form-control">*/
/*                           <option value="">Select Category</option>*/
/* 						  {% for category in categories %}*/
/*                           */
/*                           <option value="{{ category.category_id }}" {% if oneassist_plans.category_id == category.category_id %} {{ "selected" }} {% endif %}>{{ category.name }}</option>*/
/*                           {% endfor %}*/
/*                       </select>*/
/* 					  {% if error_category %}*/
/*                       <div class="text-danger">{{ error_category }}</div>*/
/*                       {% endif %}*/
/*                     </div>*/
/*                   </div>*/
/*                   <div class="form-group required">*/
/*                     <label class="col-sm-2 control-label" for="input-plantype{{ language.language_id }}">Type</label>*/
/*                     <div class="col-sm-10">*/
/*                       <select name="type" class="form-control">*/
/*                           <option value="">Select Type</option>*/
/*                           <option value="ADLD" {% if oneassist_plans.type == "ADLD" %} {{ "selected" }} {% endif %}>Accidental and Liquid Damage Protection Plan</option>*/
/*                           <option value="Extended Warranty" {% if oneassist_plans.type == "Extended Warranty" %} {{ "selected" }} {% endif %}>Extended Warranty</option>*/
/*                       </select>*/
/* 					  {% if error_plantype %}*/
/*                       <div class="text-danger">{{ error_plantype }}</div>*/
/*                       {% endif %}*/
/*                     </div>*/
/*                   </div>*/
/*                   <div class="form-group required">*/
/*                     <label class="col-sm-2 control-label" for="input-brand{{ language.language_id }}">Brand</label>*/
/*                     <div class="col-sm-10">*/
/*                       <select name="brand" class="form-control">*/
/*                           <option value="">Select Brand</option>*/
/*                           <option value="Non Apple" {% if oneassist_plans.brand == "Non Apple" %} {{ "selected" }} {% endif %}>Non Apple</option>*/
/*                           <option value="Apple" {% if oneassist_plans.brand == "Apple" %} {{ "selected" }} {% endif %}>Apple</option>*/
/*                       </select>*/
/* 					  {% if error_brand %}*/
/*                       <div class="text-danger">{{ error_brand }}</div>*/
/*                       {% endif %}*/
/* */
/*                     </div>*/
/*                   </div>*/
/*                   <div class="form-group required">*/
/*                     <label class="col-sm-2 control-label" for="input-planname{{ language.language_id }}">Plan Name</label>*/
/*                     <div class="col-sm-10">*/
/*                       <input type="text" name="plan_name" value="{% if oneassist_plans.plan_name %} {{ oneassist_plans.plan_name }} {% endif %}" placeholder="Plan Name" id="plan_name" class="form-control">*/
/*                       {% if error_plan_name %}*/
/*                       <div class="text-danger">{{ error_plan_name }}</div>*/
/*                       {% endif %}*/
/*                     </div>*/
/*                   </div>*/
/*                   <div class="form-group required">*/
/*                     <label class="col-sm-2 control-label" for="input-fromprice<?php echo $language['language_id']; ?>">From Price</label>*/
/*                     <div class="col-sm-10">*/
/*                       <input type="text" name="from_price" value="{% if oneassist_plans.from_price %} {{ oneassist_plans.from_price }} {% endif %}" placeholder="From Price" id="fromprice" class="form-control">*/
/*                       {% if error_fromprice %}*/
/*                       <div class="text-danger">{{ error_fromprice }}</div>*/
/*                       {% endif %}*/
/*                     </div>*/
/*                   </div>*/
/*                   <div class="form-group required">*/
/*                     <label class="col-sm-2 control-label" for="input-toprice<?php echo $language['language_id']; ?>">To Price</label>*/
/*                     <div class="col-sm-10">*/
/*                       <input type="text" name="to_price" value="{% if oneassist_plans.to_price %} {{ oneassist_plans.to_price }} {% endif %}" placeholder="To Price" id="toprice" class="form-control">*/
/*                       {% if error_toprice %}*/
/*                       <div class="text-danger">{{ error_toprice }}</div>*/
/*                       {% endif %}*/
/*                     </div>*/
/*                   </div>*/
/*                   <div class="form-group required">*/
/*                     <label class="col-sm-2 control-label" for="input-mrp{{ language.language_id }}">Poorvika MRP</label>*/
/*                     <div class="col-sm-10">*/
/*                       <input type="text" name="poorvika_mrp" value="{% if oneassist_plans.poorvika_mrp %} {{ oneassist_plans.poorvika_mrp }} {% endif %}" placeholder="Poorvika MRP" id="mrp" class="form-control">*/
/*                       {% if error_mrp %}*/
/*                       <div class="text-danger">{{ error_mrp }}</div>*/
/*                       {% endif %}*/
/*                     </div>*/
/*                   </div>*/
/* */
/*                 </div>*/
/*                 {% endfor %}*/
/*               </div>*/
/*             </div>*/
/* */
/*             */
/*           </div>*/
/*         </form>*/
/*       </div>*/
/*     </div>*/
/*   </div>*/
/*   <script type="text/javascript" src="view/javascript/summernote/summernote.js"></script>*/
/*   <link href="view/javascript/summernote/summernote.css" rel="stylesheet" />*/
/*   <script type="text/javascript" src="view/javascript/summernote/opencart.js"></script> */
/*   <script type="text/javascript"><!--*/
/* $('#language a:first').tab('show');*/
/* //--></script></div>*/
/* {{ footer }}*/
