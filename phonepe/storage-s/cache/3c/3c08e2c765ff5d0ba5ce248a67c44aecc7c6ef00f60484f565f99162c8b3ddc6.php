<?php

/* so-destino/template/extension/module/so_onepagecheckout/checkout/payment_methods.twig */
class __TwigTemplate_57af685f45048082974d2d9f5e23babb486f739f255f5ec55d798ce146e1cd14 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (($this->getAttribute((isset($context["setting_so_onepagecheckout_layout_setting"]) ? $context["setting_so_onepagecheckout_layout_setting"] : null), "payment_method_status", array()) == 1)) {
            // line 2
            echo "<div class=\"checkout-content checkout-payment-methods\">
    ";
            // line 3
            if ((isset($context["error_warning"]) ? $context["error_warning"] : null)) {
                // line 4
                echo "        <div class=\"alert alert-warning warning\"><i class=\"fa fa-exclamation-circle\"></i> ";
                echo (isset($context["error_warning"]) ? $context["error_warning"] : null);
                echo "
        </div>
    ";
            }
            // line 7
            echo "    ";
            if ((isset($context["payment_methods"]) ? $context["payment_methods"] : null)) {
                // line 8
                echo "        <h2 class=\"secondary-title\"><i class=\"fa fa-credit-card\"></i>";
                echo (isset($context["text_title_payment_method"]) ? $context["text_title_payment_method"] : null);
                echo "</h2>
        <div class=\"box-inner\">
            ";
                // line 10
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["payment_methods"]) ? $context["payment_methods"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["payment_method"]) {
                    // line 11
                    echo "                ";
                    if (($this->getAttribute((isset($context["setting_so_onepagecheckout_layout_setting"]) ? $context["setting_so_onepagecheckout_layout_setting"] : null), ($this->getAttribute($context["payment_method"], "code", array()) . "_status"), array(), "array", true, true) && ($this->getAttribute((isset($context["setting_so_onepagecheckout_layout_setting"]) ? $context["setting_so_onepagecheckout_layout_setting"] : null), ($this->getAttribute($context["payment_method"], "code", array()) . "_status"), array(), "array") == 1))) {
                        // line 12
                        echo "                <div class=\"radio\">
                    <label>
                        ";
                        // line 14
                        if (((($this->getAttribute($context["payment_method"], "code", array()) == (isset($context["code"]) ? $context["code"] : null)) ||  !(isset($context["code"]) ? $context["code"] : null)) || ($this->getAttribute((isset($context["setting_so_onepagecheckout_layout_setting"]) ? $context["setting_so_onepagecheckout_layout_setting"] : null), "so_onepagecheckout_default_payment", array()) == $this->getAttribute($context["payment_method"], "code", array())))) {
                            // line 15
                            echo "                            ";
                            $context["code"] = $this->getAttribute($context["payment_method"], "code", array());
                            // line 16
                            echo "                            <input type=\"radio\" name=\"payment_method\" value=\"";
                            echo $this->getAttribute($context["payment_method"], "code", array());
                            echo "\" checked=\"checked\"/>
                        ";
                        } else {
                            // line 18
                            echo "                            <input type=\"radio\" name=\"payment_method\" value=\"";
                            echo $this->getAttribute($context["payment_method"], "code", array());
                            echo "\"/>
                        ";
                        }
                        // line 20
                        echo "                        ";
                        echo $this->getAttribute($context["payment_method"], "title", array());
                        echo "
                        
                        ";
                        // line 22
                        if (($this->getAttribute($context["payment_method"], "terms", array(), "any", true, true) && $this->getAttribute($context["payment_method"], "terms", array()))) {
                            // line 23
                            echo "                            (";
                            echo $this->getAttribute($context["payment_method"], "terms", array());
                            echo ")
                        ";
                        }
                        // line 25
                        echo "                    </label>
                </div>
                ";
                    }
                    // line 28
                    echo "            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['payment_method'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 29
                echo "        </div>
    ";
            }
            // line 31
            echo "</div>
";
        }
    }

    public function getTemplateName()
    {
        return "so-destino/template/extension/module/so_onepagecheckout/checkout/payment_methods.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 31,  95 => 29,  89 => 28,  84 => 25,  78 => 23,  76 => 22,  70 => 20,  64 => 18,  58 => 16,  55 => 15,  53 => 14,  49 => 12,  46 => 11,  42 => 10,  36 => 8,  33 => 7,  26 => 4,  24 => 3,  21 => 2,  19 => 1,);
    }
}
/* {% if setting_so_onepagecheckout_layout_setting.payment_method_status == 1 %}*/
/* <div class="checkout-content checkout-payment-methods">*/
/*     {% if error_warning %}*/
/*         <div class="alert alert-warning warning"><i class="fa fa-exclamation-circle"></i> {{ error_warning }}*/
/*         </div>*/
/*     {% endif %}*/
/*     {% if payment_methods %}*/
/*         <h2 class="secondary-title"><i class="fa fa-credit-card"></i>{{ text_title_payment_method }}</h2>*/
/*         <div class="box-inner">*/
/*             {% for payment_method in payment_methods %}*/
/*                 {% if setting_so_onepagecheckout_layout_setting[payment_method.code~'_status'] is defined and setting_so_onepagecheckout_layout_setting[payment_method.code~'_status'] == 1 %}*/
/*                 <div class="radio">*/
/*                     <label>*/
/*                         {% if payment_method.code == code or not code or setting_so_onepagecheckout_layout_setting.so_onepagecheckout_default_payment == payment_method.code %}*/
/*                             {% set code = payment_method.code %}*/
/*                             <input type="radio" name="payment_method" value="{{ payment_method.code }}" checked="checked"/>*/
/*                         {% else %}*/
/*                             <input type="radio" name="payment_method" value="{{ payment_method.code }}"/>*/
/*                         {% endif %}*/
/*                         {{ payment_method.title }}*/
/*                         */
/*                         {% if payment_method.terms is defined and payment_method.terms %}*/
/*                             ({{ payment_method.terms }})*/
/*                         {% endif %}*/
/*                     </label>*/
/*                 </div>*/
/*                 {% endif %}*/
/*             {% endfor %}*/
/*         </div>*/
/*     {% endif %}*/
/* </div>*/
/* {% endif %}*/
