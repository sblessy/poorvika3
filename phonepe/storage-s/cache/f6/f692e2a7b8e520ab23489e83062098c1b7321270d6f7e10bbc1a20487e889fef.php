<?php

/* default/template/account/wk_enquiry_email.twig */
class __TwigTemplate_d8d2806ad586073b2471613f59055914a840767361764f17a8f3f0f67cc16cc7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd\">
<html>
    <head>
        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">
        <title>";
        // line 5
        echo (isset($context["store_name"]) ? $context["store_name"] : null);
        echo "</title>
    </head>
    <body style=\"font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;\">
        ";
        // line 8
        if ((array_key_exists("mail_type", $context) && ((isset($context["mail_type"]) ? $context["mail_type"] : null) == "enquiry"))) {
            echo " 
            <div style=\"width: 680px;\">
                <a href=\"";
            // line 10
            echo (isset($context["store_url"]) ? $context["store_url"] : null);
            echo "\" title=\"";
            echo (isset($context["store_name"]) ? $context["store_name"] : null);
            echo "\">
                    <img src=\"";
            // line 11
            echo (isset($context["logo"]) ? $context["logo"] : null);
            echo "\" alt=\"";
            echo (isset($context["store_name"]) ? $context["store_name"] : null);
            echo "\" style=\"margin-bottom: 20px; border: none;\" />
                </a>
                <p style=\"margin-top: 0px; margin-bottom: 20px;\">";
            // line 13
            echo (isset($context["heading"]) ? $context["heading"] : null);
            echo "</p>
                <p>";
            // line 14
            echo (isset($context["mail_text_salutation"]) ? $context["mail_text_salutation"] : null);
            echo "</p>
                <p>";
            // line 15
            echo (isset($context["query_id"]) ? $context["query_id"] : null);
            echo "</p>
                <p>";
            // line 16
            echo ((isset($context["mail_text_subject"]) ? $context["mail_text_subject"] : null) . (isset($context["enquiry_subject"]) ? $context["enquiry_subject"] : null));
            echo "</p>
                <p>";
            // line 17
            echo ((isset($context["mail_text_query"]) ? $context["mail_text_query"] : null) . (isset($context["enquiry_query"]) ? $context["enquiry_query"] : null));
            echo "</p>
                </br>
                ";
            // line 19
            if ((array_key_exists("enquiry_mail", $context) && ((isset($context["enquiry_mail"]) ? $context["enquiry_mail"] : null) == "customer_enquiry"))) {
                echo " 
                    <p>
                        ";
                // line 21
                echo (isset($context["greeting_to_customer"]) ? $context["greeting_to_customer"] : null);
                echo "</br>
                        <a href=\"";
                // line 22
                echo (isset($context["enquiry_link"]) ? $context["enquiry_link"] : null);
                echo "\" >";
                echo (isset($context["mail_text_check_ur_enq"]) ? $context["mail_text_check_ur_enq"] : null);
                echo "</a></br>
                    </p>
                ";
            }
            // line 24
            echo " 
                <p>";
            // line 25
            echo (isset($context["mail_text_thank_you"]) ? $context["mail_text_thank_you"] : null);
            echo "</p>
                <p>";
            // line 26
            echo (isset($context["store_name"]) ? $context["store_name"] : null);
            echo "</p>
            </div>
        ";
        } elseif ((        // line 28
array_key_exists("mail_type", $context) && ((isset($context["mail_type"]) ? $context["mail_type"] : null) == "enquiry"))) {
            echo " 
            <div style=\"width: 680px;\">
                <a href=\"";
            // line 30
            echo (isset($context["store_url"]) ? $context["store_url"] : null);
            echo "\" title=\"";
            echo (isset($context["store_name"]) ? $context["store_name"] : null);
            echo "\">
                    <img src=\"";
            // line 31
            echo (isset($context["logo"]) ? $context["logo"] : null);
            echo "\" alt=\"";
            echo (isset($context["store_name"]) ? $context["store_name"] : null);
            echo "\" style=\"margin-bottom: 20px; border: none;\" />
                </a>
                <p style=\"margin-top: 0px; margin-bottom: 20px;\">";
            // line 33
            echo (isset($context["heading"]) ? $context["heading"] : null);
            echo "</p>
                <p>";
            // line 34
            echo (isset($context["mail_text_salutation"]) ? $context["mail_text_salutation"] : null);
            echo "</p>
                <p>";
            // line 35
            echo (isset($context["query_id"]) ? $context["query_id"] : null);
            echo "</p>
                <p>";
            // line 36
            echo ((isset($context["mail_text_subject"]) ? $context["mail_text_subject"] : null) . (isset($context["enquiry_subject"]) ? $context["enquiry_subject"] : null));
            echo "</p>
                <p>";
            // line 37
            echo ((isset($context["mail_text_query"]) ? $context["mail_text_query"] : null) . (isset($context["enquiry_query"]) ? $context["enquiry_query"] : null));
            echo "</p>
                </br>
                ";
            // line 39
            if ((array_key_exists("enquiry_mail", $context) && ((isset($context["enquiry_mail"]) ? $context["enquiry_mail"] : null) == "customer_enquiry"))) {
                echo " 
                    <p>
                        ";
                // line 41
                echo (isset($context["greeting_to_customer"]) ? $context["greeting_to_customer"] : null);
                echo "</br>
                        <a href=\"";
                // line 42
                echo (isset($context["enquiry_link"]) ? $context["enquiry_link"] : null);
                echo "\" >";
                echo (isset($context["mail_text_check_ur_enq"]) ? $context["mail_text_check_ur_enq"] : null);
                echo "</a></br>
                    </p>
                ";
            }
            // line 44
            echo " 
                <p>";
            // line 45
            echo (isset($context["mail_text_thank_you"]) ? $context["mail_text_thank_you"] : null);
            echo "</p>
                <p>";
            // line 46
            echo (isset($context["store_name"]) ? $context["store_name"] : null);
            echo "</p>
            </div>
        ";
        }
        // line 48
        echo " 
    </body>
</html>
";
    }

    public function getTemplateName()
    {
        return "default/template/account/wk_enquiry_email.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  168 => 48,  162 => 46,  158 => 45,  155 => 44,  147 => 42,  143 => 41,  138 => 39,  133 => 37,  129 => 36,  125 => 35,  121 => 34,  117 => 33,  110 => 31,  104 => 30,  99 => 28,  94 => 26,  90 => 25,  87 => 24,  79 => 22,  75 => 21,  70 => 19,  65 => 17,  61 => 16,  57 => 15,  53 => 14,  49 => 13,  42 => 11,  36 => 10,  31 => 8,  25 => 5,  19 => 1,);
    }
}
/* <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd">*/
/* <html>*/
/*     <head>*/
/*         <meta http-equiv="Content-Type" content="text/html; charset=utf-8">*/
/*         <title>{{ store_name }}</title>*/
/*     </head>*/
/*     <body style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;">*/
/*         {% if (mail_type is defined and mail_type == 'enquiry') %} */
/*             <div style="width: 680px;">*/
/*                 <a href="{{ store_url }}" title="{{ store_name }}">*/
/*                     <img src="{{ logo }}" alt="{{ store_name }}" style="margin-bottom: 20px; border: none;" />*/
/*                 </a>*/
/*                 <p style="margin-top: 0px; margin-bottom: 20px;">{{ heading }}</p>*/
/*                 <p>{{ mail_text_salutation }}</p>*/
/*                 <p>{{ query_id }}</p>*/
/*                 <p>{{ mail_text_subject~enquiry_subject }}</p>*/
/*                 <p>{{ mail_text_query~enquiry_query }}</p>*/
/*                 </br>*/
/*                 {% if (enquiry_mail is defined and enquiry_mail == 'customer_enquiry') %} */
/*                     <p>*/
/*                         {{ greeting_to_customer }}</br>*/
/*                         <a href="{{ enquiry_link }}" >{{ mail_text_check_ur_enq }}</a></br>*/
/*                     </p>*/
/*                 {% endif %} */
/*                 <p>{{ mail_text_thank_you }}</p>*/
/*                 <p>{{ store_name }}</p>*/
/*             </div>*/
/*         {% elseif (mail_type is defined and mail_type == 'enquiry') %} */
/*             <div style="width: 680px;">*/
/*                 <a href="{{ store_url }}" title="{{ store_name }}">*/
/*                     <img src="{{ logo }}" alt="{{ store_name }}" style="margin-bottom: 20px; border: none;" />*/
/*                 </a>*/
/*                 <p style="margin-top: 0px; margin-bottom: 20px;">{{ heading }}</p>*/
/*                 <p>{{ mail_text_salutation }}</p>*/
/*                 <p>{{ query_id }}</p>*/
/*                 <p>{{ mail_text_subject~enquiry_subject }}</p>*/
/*                 <p>{{ mail_text_query~enquiry_query }}</p>*/
/*                 </br>*/
/*                 {% if (enquiry_mail is defined and enquiry_mail == 'customer_enquiry') %} */
/*                     <p>*/
/*                         {{ greeting_to_customer }}</br>*/
/*                         <a href="{{ enquiry_link }}" >{{ mail_text_check_ur_enq }}</a></br>*/
/*                     </p>*/
/*                 {% endif %} */
/*                 <p>{{ mail_text_thank_you }}</p>*/
/*                 <p>{{ store_name }}</p>*/
/*             </div>*/
/*         {% endif %} */
/*     </body>*/
/* </html>*/
/* */
