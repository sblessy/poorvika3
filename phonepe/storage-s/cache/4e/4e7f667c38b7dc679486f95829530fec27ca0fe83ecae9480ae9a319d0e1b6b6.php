<?php

/* so-destino/template/extension/module/so_onepagecheckout/checkout/coupon_voucher_reward.twig */
class __TwigTemplate_b61c22bbcef32e0e15df4db982ad46cbe30b3d95607bde3248cd8d03c7b4d2ea extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (($this->getAttribute((isset($context["setting_so_onepagecheckout_general"]) ? $context["setting_so_onepagecheckout_general"] : null), "so_onepagecheckout_layout", array()) == 2)) {
            // line 2
            echo "    <div class=\"panel-group\" id=\"accordion\">
        ";
            // line 3
            if ((isset($context["coupon_status"]) ? $context["coupon_status"] : null)) {
                // line 4
                echo "        <div class=\"panel panel-default\">
            <div class=\"panel-heading\">
                <h4 class=\"panel-title\">
                    <a href=\"#collapse-coupon\" class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion\"><i class=\"fa fa-qrcode\"></i> ";
                // line 7
                echo (isset($context["text_enter_coupon_code"]) ? $context["text_enter_coupon_code"] : null);
                echo " <i class=\"fa fa-caret-down\"></i></a>
                </h4>
            </div>
            <div id=\"collapse-coupon\" class=\"panel-collapse collapse\">
                <div class=\"panel-body\">
                    <label class=\"control-label\" for=\"input-coupon\">";
                // line 12
                echo (isset($context["text_enter_coupon_code"]) ? $context["text_enter_coupon_code"] : null);
                echo "</label>
                    <div class=\"input-group\">
                        <input type=\"text\" name=\"coupon\" value=\"";
                // line 14
                echo (isset($context["coupon"]) ? $context["coupon"] : null);
                echo "\" placeholder=\"";
                echo (isset($context["text_enter_coupon_code"]) ? $context["text_enter_coupon_code"] : null);
                echo "\" id=\"input-coupon\" class=\"form-control\">
                        <span class=\"input-group-btn\">
                            <input type=\"button\" value=\"Apply Coupon\" id=\"button-coupon\" data-loading-text=\"";
                // line 16
                echo (isset($context["text_loading"]) ? $context["text_loading"] : null);
                echo "\" class=\"btn btn-primary\">
                        </span>
                    </div>                    
                </div>
              </div>
        </div>
        ";
            }
            // line 23
            echo "
        ";
            // line 24
            if ((isset($context["voucher_status"]) ? $context["voucher_status"] : null)) {
                // line 25
                echo "        <div class=\"panel panel-default\">
            <div class=\"panel-heading\">
                <h4 class=\"panel-title\">
                    <a href=\"#collapse-voucher\" class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion\"><i class=\"fa fa-gift\"></i> ";
                // line 28
                echo (isset($context["text_enter_voucher_code"]) ? $context["text_enter_voucher_code"] : null);
                echo " <i class=\"fa fa-caret-down\"></i></a>
                </h4>
            </div>
            <div id=\"collapse-voucher\" class=\"panel-collapse collapse\">
                <div class=\"panel-body\">
                    <label class=\"control-label\" for=\"input-voucher\">";
                // line 33
                echo (isset($context["text_enter_voucher_code"]) ? $context["text_enter_voucher_code"] : null);
                echo "</label>
                    <div class=\"input-group\">
                        <input type=\"text\" name=\"voucher\" value=\"";
                // line 35
                echo (isset($context["voucher"]) ? $context["voucher"] : null);
                echo "\" placeholder=\"";
                echo (isset($context["text_enter_voucher_code"]) ? $context["text_enter_voucher_code"] : null);
                echo "\" id=\"input-coupon\" class=\"form-control\">
                        <span class=\"input-group-btn\">
                            <input type=\"button\" value=\"";
                // line 37
                echo (isset($context["text_apply_voucher"]) ? $context["text_apply_voucher"] : null);
                echo "\" id=\"button-voucher\" data-loading-text=\"";
                echo (isset($context["text_loading"]) ? $context["text_loading"] : null);
                echo "\" class=\"btn btn-primary\">
                        </span>
                    </div>                    
                </div>
              </div>
        </div>
        ";
            }
            // line 44
            echo "
        ";
            // line 45
            if ((isset($context["reward_status"]) ? $context["reward_status"] : null)) {
                // line 46
                echo "        <div class=\"panel panel-default\">
            <div class=\"panel-heading\">
                <h4 class=\"panel-title\">
                    <a href=\"#collapse-reward\" class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion\"><i class=\"fa fa-gift\"></i> ";
                // line 49
                echo (isset($context["text_enter_reward_points"]) ? $context["text_enter_reward_points"] : null);
                echo " <i class=\"fa fa-caret-down\"></i></a>
                </h4>
            </div>
            <div id=\"collapse-reward\" class=\"panel-collapse collapse\">
                <div class=\"panel-body\">
                    <label class=\"control-label\" for=\"input-reward\">";
                // line 54
                echo (isset($context["text_enter_reward_points"]) ? $context["text_enter_reward_points"] : null);
                echo "</label>
                    <div class=\"input-group\">
                        <input type=\"text\" name=\"reward\" value=\"";
                // line 56
                echo (isset($context["reward"]) ? $context["reward"] : null);
                echo "\" placeholder=\"";
                echo (isset($context["text_enter_reward_points"]) ? $context["text_enter_reward_points"] : null);
                echo "\" id=\"input-coupon\" class=\"form-control\">
                        <span class=\"input-group-btn\">
                            <input type=\"button\" value=\"";
                // line 58
                echo (isset($context["text_apply_points"]) ? $context["text_apply_points"] : null);
                echo "\" id=\"button-reward\" data-loading-text=\"";
                echo (isset($context["text_loading"]) ? $context["text_loading"] : null);
                echo "\" class=\"btn btn-primary\">
                        </span>
                    </div>                    
                </div>
              </div>
        </div>
        ";
            }
            // line 65
            echo "    </div>
";
        } else {
            // line 67
            echo "    <div class=\"checkout-content coupon-voucher\">
        <h2 class=\"secondary-title\"><i class=\"fa fa-gift\"></i>";
            // line 68
            echo (isset($context["text_coupon_voucher"]) ? $context["text_coupon_voucher"] : null);
            echo "</h2>
        <div class=\"box-inner\">
            ";
            // line 70
            if ((isset($context["coupon_status"]) ? $context["coupon_status"] : null)) {
                // line 71
                echo "            <div class=\"panel-body checkout-coupon\">
                <label class=\"col-sm-2 control-label\" for=\"input-coupon\">";
                // line 72
                echo (isset($context["text_enter_coupon_code"]) ? $context["text_enter_coupon_code"] : null);
                echo "</label>
                <div class=\"input-group\">
                    <input type=\"text\" name=\"coupon\" value=\"";
                // line 74
                echo (isset($context["coupon"]) ? $context["coupon"] : null);
                echo "\" placeholder=\"";
                echo (isset($context["text_enter_coupon_code"]) ? $context["text_enter_coupon_code"] : null);
                echo "\" id=\"input-coupon\" class=\"form-control\" />
                    <span class=\"input-group-btn\">
                        <input type=\"button\" value=\"Apply Coupon\" id=\"button-coupon\" data-loading-text=\"";
                // line 76
                echo (isset($context["text_loading"]) ? $context["text_loading"] : null);
                echo "\"  class=\"btn-primary button\" />
                    </span>
                </div>
            </div>
            ";
            }
            // line 81
            echo "
            ";
            // line 82
            if ((isset($context["voucher_status"]) ? $context["voucher_status"] : null)) {
                // line 83
                echo "            <div class=\"panel-body checkout-voucher\">
                <label class=\"col-sm-2 control-label\" for=\"input-voucher\">";
                // line 84
                echo (isset($context["text_enter_voucher_code"]) ? $context["text_enter_voucher_code"] : null);
                echo "</label>
                <div class=\"input-group\">
                    <input type=\"text\" name=\"voucher\" value=\"";
                // line 86
                echo (isset($context["voucher"]) ? $context["voucher"] : null);
                echo "\" placeholder=\"";
                echo (isset($context["text_enter_voucher_code"]) ? $context["text_enter_voucher_code"] : null);
                echo "\" id=\"input-voucher\" class=\"form-control\" />
                    <span class=\"input-group-btn\">
                        <input type=\"button\" value=\"";
                // line 88
                echo (isset($context["text_apply_voucher"]) ? $context["text_apply_voucher"] : null);
                echo "\" id=\"button-voucher\" data-loading-text=\"";
                echo (isset($context["text_loading"]) ? $context["text_loading"] : null);
                echo "\"  class=\"btn-primary button\" />
                    </span>
                </div>
            </div>
            ";
            }
            // line 93
            echo "
            ";
            // line 94
            if ((isset($context["reward_status"]) ? $context["reward_status"] : null)) {
                // line 95
                echo "                <div class=\"panel-body checkout-reward\">
                    <label class=\"col-sm-2 control-label\" for=\"input-reward\">";
                // line 96
                echo (isset($context["text_enter_reward_points"]) ? $context["text_enter_reward_points"] : null);
                echo "</label>
                    <div class=\"input-group\">
                        <input type=\"text\" name=\"reward\" value=\"";
                // line 98
                echo (isset($context["reward"]) ? $context["reward"] : null);
                echo "\" placeholder=\"";
                echo (isset($context["text_enter_reward_points"]) ? $context["text_enter_reward_points"] : null);
                echo "\" id=\"input-reward\" class=\"form-control\" />
                        <span class=\"input-group-btn\">
                            <input type=\"button\" value=\"";
                // line 100
                echo (isset($context["text_apply_points"]) ? $context["text_apply_points"] : null);
                echo "\" id=\"button-reward\" data-loading-text=\"";
                echo (isset($context["text_loading"]) ? $context["text_loading"] : null);
                echo "\"  class=\"btn-primary button\" />
                        </span>
                    </div>
                </div>
            ";
            }
            // line 105
            echo "        </div>
    </div>
";
        }
    }

    public function getTemplateName()
    {
        return "so-destino/template/extension/module/so_onepagecheckout/checkout/coupon_voucher_reward.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  242 => 105,  232 => 100,  225 => 98,  220 => 96,  217 => 95,  215 => 94,  212 => 93,  202 => 88,  195 => 86,  190 => 84,  187 => 83,  185 => 82,  182 => 81,  174 => 76,  167 => 74,  162 => 72,  159 => 71,  157 => 70,  152 => 68,  149 => 67,  145 => 65,  133 => 58,  126 => 56,  121 => 54,  113 => 49,  108 => 46,  106 => 45,  103 => 44,  91 => 37,  84 => 35,  79 => 33,  71 => 28,  66 => 25,  64 => 24,  61 => 23,  51 => 16,  44 => 14,  39 => 12,  31 => 7,  26 => 4,  24 => 3,  21 => 2,  19 => 1,);
    }
}
/* {% if setting_so_onepagecheckout_general.so_onepagecheckout_layout == 2 %}*/
/*     <div class="panel-group" id="accordion">*/
/*         {% if coupon_status %}*/
/*         <div class="panel panel-default">*/
/*             <div class="panel-heading">*/
/*                 <h4 class="panel-title">*/
/*                     <a href="#collapse-coupon" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"><i class="fa fa-qrcode"></i> {{ text_enter_coupon_code }} <i class="fa fa-caret-down"></i></a>*/
/*                 </h4>*/
/*             </div>*/
/*             <div id="collapse-coupon" class="panel-collapse collapse">*/
/*                 <div class="panel-body">*/
/*                     <label class="control-label" for="input-coupon">{{ text_enter_coupon_code }}</label>*/
/*                     <div class="input-group">*/
/*                         <input type="text" name="coupon" value="{{ coupon }}" placeholder="{{ text_enter_coupon_code }}" id="input-coupon" class="form-control">*/
/*                         <span class="input-group-btn">*/
/*                             <input type="button" value="Apply Coupon" id="button-coupon" data-loading-text="{{ text_loading }}" class="btn btn-primary">*/
/*                         </span>*/
/*                     </div>                    */
/*                 </div>*/
/*               </div>*/
/*         </div>*/
/*         {% endif %}*/
/* */
/*         {% if voucher_status %}*/
/*         <div class="panel panel-default">*/
/*             <div class="panel-heading">*/
/*                 <h4 class="panel-title">*/
/*                     <a href="#collapse-voucher" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"><i class="fa fa-gift"></i> {{ text_enter_voucher_code }} <i class="fa fa-caret-down"></i></a>*/
/*                 </h4>*/
/*             </div>*/
/*             <div id="collapse-voucher" class="panel-collapse collapse">*/
/*                 <div class="panel-body">*/
/*                     <label class="control-label" for="input-voucher">{{ text_enter_voucher_code }}</label>*/
/*                     <div class="input-group">*/
/*                         <input type="text" name="voucher" value="{{ voucher }}" placeholder="{{ text_enter_voucher_code }}" id="input-coupon" class="form-control">*/
/*                         <span class="input-group-btn">*/
/*                             <input type="button" value="{{ text_apply_voucher }}" id="button-voucher" data-loading-text="{{ text_loading }}" class="btn btn-primary">*/
/*                         </span>*/
/*                     </div>                    */
/*                 </div>*/
/*               </div>*/
/*         </div>*/
/*         {% endif %}*/
/* */
/*         {% if reward_status %}*/
/*         <div class="panel panel-default">*/
/*             <div class="panel-heading">*/
/*                 <h4 class="panel-title">*/
/*                     <a href="#collapse-reward" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"><i class="fa fa-gift"></i> {{ text_enter_reward_points }} <i class="fa fa-caret-down"></i></a>*/
/*                 </h4>*/
/*             </div>*/
/*             <div id="collapse-reward" class="panel-collapse collapse">*/
/*                 <div class="panel-body">*/
/*                     <label class="control-label" for="input-reward">{{ text_enter_reward_points }}</label>*/
/*                     <div class="input-group">*/
/*                         <input type="text" name="reward" value="{{ reward }}" placeholder="{{ text_enter_reward_points }}" id="input-coupon" class="form-control">*/
/*                         <span class="input-group-btn">*/
/*                             <input type="button" value="{{ text_apply_points }}" id="button-reward" data-loading-text="{{ text_loading }}" class="btn btn-primary">*/
/*                         </span>*/
/*                     </div>                    */
/*                 </div>*/
/*               </div>*/
/*         </div>*/
/*         {% endif %}*/
/*     </div>*/
/* {% else %}*/
/*     <div class="checkout-content coupon-voucher">*/
/*         <h2 class="secondary-title"><i class="fa fa-gift"></i>{{ text_coupon_voucher }}</h2>*/
/*         <div class="box-inner">*/
/*             {% if coupon_status %}*/
/*             <div class="panel-body checkout-coupon">*/
/*                 <label class="col-sm-2 control-label" for="input-coupon">{{ text_enter_coupon_code }}</label>*/
/*                 <div class="input-group">*/
/*                     <input type="text" name="coupon" value="{{ coupon }}" placeholder="{{ text_enter_coupon_code }}" id="input-coupon" class="form-control" />*/
/*                     <span class="input-group-btn">*/
/*                         <input type="button" value="Apply Coupon" id="button-coupon" data-loading-text="{{ text_loading }}"  class="btn-primary button" />*/
/*                     </span>*/
/*                 </div>*/
/*             </div>*/
/*             {% endif %}*/
/* */
/*             {% if voucher_status %}*/
/*             <div class="panel-body checkout-voucher">*/
/*                 <label class="col-sm-2 control-label" for="input-voucher">{{ text_enter_voucher_code }}</label>*/
/*                 <div class="input-group">*/
/*                     <input type="text" name="voucher" value="{{ voucher }}" placeholder="{{ text_enter_voucher_code }}" id="input-voucher" class="form-control" />*/
/*                     <span class="input-group-btn">*/
/*                         <input type="button" value="{{ text_apply_voucher }}" id="button-voucher" data-loading-text="{{ text_loading }}"  class="btn-primary button" />*/
/*                     </span>*/
/*                 </div>*/
/*             </div>*/
/*             {% endif %}*/
/* */
/*             {% if reward_status %}*/
/*                 <div class="panel-body checkout-reward">*/
/*                     <label class="col-sm-2 control-label" for="input-reward">{{ text_enter_reward_points }}</label>*/
/*                     <div class="input-group">*/
/*                         <input type="text" name="reward" value="{{ reward }}" placeholder="{{ text_enter_reward_points }}" id="input-reward" class="form-control" />*/
/*                         <span class="input-group-btn">*/
/*                             <input type="button" value="{{ text_apply_points }}" id="button-reward" data-loading-text="{{ text_loading }}"  class="btn-primary button" />*/
/*                         </span>*/
/*                     </div>*/
/*                 </div>*/
/*             {% endif %}*/
/*         </div>*/
/*     </div>*/
/* {% endif %}*/
