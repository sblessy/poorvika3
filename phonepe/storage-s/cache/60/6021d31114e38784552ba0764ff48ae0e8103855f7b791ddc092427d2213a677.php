<?php

/* catalog/cashify_report.twig */
class __TwigTemplate_60ef63bf7031863cd6219a180f0f33abdde5d389f44cbb13ecd5c840217495b7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo "
<link href=\"https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css\" type=\"text/css\" rel=\"stylesheet\" />
<link href=\"https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css\" type=\"text/css\" rel=\"stylesheet\" />
";
        // line 4
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "

<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <h1>";
        // line 9
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
      <ul class=\"breadcrumb\">
        ";
        // line 11
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 12
            echo "          <li><a href=\"";
            echo $this->getAttribute($context["breadcrumb"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array());
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 14
        echo "      </ul>
    </div>
  </div>
  <div class=\"container-fluid\"> ";
        // line 17
        if ((isset($context["error_warning"]) ? $context["error_warning"] : null)) {
            // line 18
            echo "      <div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo (isset($context["error_warning"]) ? $context["error_warning"] : null);
            echo "
        <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
      </div>
    ";
        }
        // line 22
        echo "    
    <div class=\"panel panel-default\">
\t   <div class=\"panel-heading\" style=\"padding-bottom: 20px;\">
\t\t  <h3>";
        // line 25
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h3>
\t\t\t
\t\t</div>
\t   <div class=\"panel-body\">
      
      \t<table id=\"csh_tbl\" class=\"display\" style=\"width:100%\"> 
\t\t\t<thead>
\t \t\t\t<tr> 
          <th>SL NO</th> 
\t\t\t\t \t<th>Product ID</th> 
          <th>Product Name</th> 
\t\t\t\t \t<th>Reference ID</th> 
\t\t\t\t \t<th>Estimated Amount</th> 
\t\t\t\t \t<th>Final Amount</th> 
\t\t\t\t \t<th>Status</th>
\t\t\t\t \t<th>Created Date</th>
\t\t\t\t \t<th class=\"not-export-col\">View</th>
\t \t\t\t</tr> 
\t \t\t</thead> 
\t \t\t<tbody> 
\t \t\t\t";
        // line 45
        $context["i"] = 1;
        // line 46
        echo "\t \t\t\t";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["csh_lists"]) ? $context["csh_lists"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["csh_list"]) {
            // line 47
            echo "\t \t\t\t<tr> 
          <td>";
            // line 48
            echo (isset($context["i"]) ? $context["i"] : null);
            echo "</td> 
\t \t\t\t\t<td>";
            // line 49
            echo $this->getAttribute($context["csh_list"], "csh_pid", array());
            echo "</td> 
\t \t\t\t\t<td>";
            // line 50
            echo $this->getAttribute($context["csh_list"], "csh_pn", array());
            echo "</td> 
          <td>";
            // line 51
            echo $this->getAttribute($context["csh_list"], "ref_id", array());
            echo " </td>
\t \t\t\t\t<td>";
            // line 52
            echo $this->getAttribute($context["csh_list"], "quote_amnt", array());
            echo "</td>
\t \t\t\t\t<td>";
            // line 53
            echo $this->getAttribute($context["csh_list"], "final_amt", array());
            echo "</td>
\t \t\t\t\t<td>";
            // line 54
            echo $this->getAttribute($context["csh_list"], "status", array());
            echo "</td>
\t \t\t\t\t<td>";
            // line 55
            echo $this->getAttribute($context["csh_list"], "created_date", array());
            echo "</td>
\t \t\t\t\t<td class=\"not-export-col\"><button type=\"button\" class=\"btn btn-info btn-lg opencshmodal fa fa-eye\" data-id=\"";
            // line 56
            echo $this->getAttribute($context["csh_list"], "csh_id", array());
            echo "\" data-toggle=\"modal\" data-target=\"#cshModal\"></button></td>
\t \t\t\t</tr>
\t \t\t\t";
            // line 58
            $context["i"] = ((isset($context["i"]) ? $context["i"] : null) + 1);
            // line 59
            echo "\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['csh_list'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 60
        echo "   \t\t\t
   \t\t\t</tbody>
   \t\t</table>

   \t\t<!-- Trigger the modal with a button -->
   \t\t
   \t\t<!-- Modal -->
<div id=\"cshModal\" class=\"modal fade\" role=\"dialog\">
  <div class=\"modal-dialog\">

    <!-- Modal content-->
    <div class=\"modal-content\">
      <div class=\"modal-header\">
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>
        <h3 class=\"modal-title\" style=\"font-weight: bold;\">Cashify Details</h3>
      </div>
      <div class=\"modal-body\">
      \t<input type=\"hidden\" id=\"cashify_id\" name=\"cashify_id\" value=\"\">
      \t<div class=\"form-group\">
\t        <label class=\"control-label\" for=\"input-id\">Product ID</label>
\t        <input type=\"text\" name=\"pid\" value=\"\" id=\"pid\" class=\"form-control\" readonly>
\t     </div>
\t     <div class=\"form-group\">
\t        <label class=\"control-label\" for=\"input-pname\">Product Name</label>
\t        <input type=\"text\" name=\"pname\" value=\"\" id=\"pname\" class=\"form-control\" readonly>
\t     </div>
      \t<div class=\"form-group\">
\t        <label class=\"control-label\" for=\"input-prefid\">Reference ID</label>
\t        <input type=\"text\" name=\"prefid\" value=\"\" id=\"prefid\" class=\"form-control\" readonly>
\t     </div>
       <div class=\"form-group\">
          <label class=\"control-label\" for=\"input-pestamt\">Estimated Amount</label>
          <input type=\"text\" name=\"pestamt\" value=\"\" id=\"pestamt\" class=\"form-control\" readonly>
       </div>
\t     <div class=\"form-group\">
\t        <label class=\"control-label\" for=\"input-pfinalamt\">Final Amount</label>
\t        <input type=\"text\" name=\"pfinalamt\" value=\"\" id=\"pfinalamt\" class=\"form-control\" readonly>
\t     </div>
      \t<div class=\"form-group\">
\t        <label class=\"control-label\" for=\"input-pst\">Status</label>
\t        <input type=\"text\" name=\"pst\" value=\"\" id=\"pst\" class=\"form-control\" readonly>
\t     </div>
\t     <div class=\"form-group\">
\t        <label class=\"control-label\" for=\"input-created_dt\">Created Date</label>
\t        <input type=\"text\" name=\"created_dt\" value=\"\" id=\"created_dt\" class=\"form-control\" readonly>
\t     </div>
      \t
      </div>
      <div class=\"modal-footer\">
        <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>
      </div>
    </div>
   \t\t
   \t\t</div>
   \t\t</div>
   \t\t</div>

 </div>
  </div>
  
  <script type=\"text/javascript\" src=\"https://code.jquery.com/jquery-3.5.1.js\"></script> 
<script type=\"text/javascript\" src=\"https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js\"></script> 
<script type=\"text/javascript\" src=\"https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js\"></script> 
<script type=\"text/javascript\" src=\"https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js\"></script> 
<script type=\"text/javascript\" src=\"https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js\"></script> 
<script type=\"text/javascript\" src=\"https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js\"></script> 
<script type=\"text/javascript\" src=\"https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js\"></script> 

<script>
/*\$(document).ready(function() {
    \$('#csh_tbl').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ]
    } );
} );*/
/*\$(document).ready(function() {
    \$('#csh_tbl').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5'
        ]
    } );
} );*/
\$(document).ready(function() {
    \$('#csh_tbl').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'copyHtml5',
                exportOptions: {
                     columns: ':not(.not-export-col)'
                }
            },
            {
                extend: 'excelHtml5',
                exportOptions: {
                     columns: ':not(.not-export-col)'
                }
            },
            {
                extend: 'csvHtml5',
                exportOptions: {
                     columns: ':not(.not-export-col)'
                }
            }
        ]
    } );
} );
\$(document).ready(function() {
  \$(\".opencshmodal\").click(function(){
     \$('#cashify_id').val(\$(this).data('id'));
     var cashify_id = \$('#cashify_id').val();
     \$.ajax({
            url: 'index.php?route=catalog/cashifyreport/getCashifyData&user_token=";
        // line 180
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "',
            type: 'POST',
            dataType : 'json',
            data: {
            cashify_id: cashify_id
        },
        success: function(response) {
        \tvar obj = JSON.stringify(response)
        \tvar stringify = JSON.parse(obj);
        \t\$('#pid').val(stringify['csh_pid']);
        \t\$('#pname').val(stringify['csh_pn']);
          \$('#pestamt').val(stringify['quote_amnt']);
          \$('#pfinalamt').val(stringify['final_amt']);
          \$('#pst').val(stringify['status']);
        \t\$('#prefid').val(stringify['ref_id']);
        \t\$('#created_dt').val(stringify['created_date']);

        \t//\$(\"#cshModal\").modal(\"show\");
\t\t\t
        }
      });
  });
});
</script>
";
        // line 204
        echo (isset($context["footer"]) ? $context["footer"] : null);
        echo " ";
    }

    public function getTemplateName()
    {
        return "catalog/cashify_report.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  300 => 204,  273 => 180,  151 => 60,  145 => 59,  143 => 58,  138 => 56,  134 => 55,  130 => 54,  126 => 53,  122 => 52,  118 => 51,  114 => 50,  110 => 49,  106 => 48,  103 => 47,  98 => 46,  96 => 45,  73 => 25,  68 => 22,  60 => 18,  58 => 17,  53 => 14,  42 => 12,  38 => 11,  33 => 9,  25 => 4,  19 => 1,);
    }
}
/* {{ header }}*/
/* <link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet" />*/
/* <link href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css" type="text/css" rel="stylesheet" />*/
/* {{ column_left }}*/
/* */
/* <div id="content">*/
/*   <div class="page-header">*/
/*     <div class="container-fluid">*/
/*       <h1>{{ heading_title }}</h1>*/
/*       <ul class="breadcrumb">*/
/*         {% for breadcrumb in breadcrumbs %}*/
/*           <li><a href="{{ breadcrumb.href }}">{{ breadcrumb.text }}</a></li>*/
/*         {% endfor %}*/
/*       </ul>*/
/*     </div>*/
/*   </div>*/
/*   <div class="container-fluid"> {% if error_warning %}*/
/*       <div class="alert alert-danger alert-dismissible"><i class="fa fa-exclamation-circle"></i> {{ error_warning }}*/
/*         <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*       </div>*/
/*     {% endif %}*/
/*     */
/*     <div class="panel panel-default">*/
/* 	   <div class="panel-heading" style="padding-bottom: 20px;">*/
/* 		  <h3>{{ heading_title }}</h3>*/
/* 			*/
/* 		</div>*/
/* 	   <div class="panel-body">*/
/*       */
/*       	<table id="csh_tbl" class="display" style="width:100%"> */
/* 			<thead>*/
/* 	 			<tr> */
/*           <th>SL NO</th> */
/* 				 	<th>Product ID</th> */
/*           <th>Product Name</th> */
/* 				 	<th>Reference ID</th> */
/* 				 	<th>Estimated Amount</th> */
/* 				 	<th>Final Amount</th> */
/* 				 	<th>Status</th>*/
/* 				 	<th>Created Date</th>*/
/* 				 	<th class="not-export-col">View</th>*/
/* 	 			</tr> */
/* 	 		</thead> */
/* 	 		<tbody> */
/* 	 			{% set i = 1 %}*/
/* 	 			{% for csh_list in csh_lists %}*/
/* 	 			<tr> */
/*           <td>{{ i }}</td> */
/* 	 				<td>{{ csh_list.csh_pid }}</td> */
/* 	 				<td>{{ csh_list.csh_pn }}</td> */
/*           <td>{{ csh_list.ref_id }} </td>*/
/* 	 				<td>{{ csh_list.quote_amnt }}</td>*/
/* 	 				<td>{{ csh_list.final_amt }}</td>*/
/* 	 				<td>{{ csh_list.status }}</td>*/
/* 	 				<td>{{ csh_list.created_date }}</td>*/
/* 	 				<td class="not-export-col"><button type="button" class="btn btn-info btn-lg opencshmodal fa fa-eye" data-id="{{ csh_list.csh_id }}" data-toggle="modal" data-target="#cshModal"></button></td>*/
/* 	 			</tr>*/
/* 	 			{% set i = i + 1 %}*/
/* 				{% endfor %}*/
/*    			*/
/*    			</tbody>*/
/*    		</table>*/
/* */
/*    		<!-- Trigger the modal with a button -->*/
/*    		*/
/*    		<!-- Modal -->*/
/* <div id="cshModal" class="modal fade" role="dialog">*/
/*   <div class="modal-dialog">*/
/* */
/*     <!-- Modal content-->*/
/*     <div class="modal-content">*/
/*       <div class="modal-header">*/
/*         <button type="button" class="close" data-dismiss="modal">&times;</button>*/
/*         <h3 class="modal-title" style="font-weight: bold;">Cashify Details</h3>*/
/*       </div>*/
/*       <div class="modal-body">*/
/*       	<input type="hidden" id="cashify_id" name="cashify_id" value="">*/
/*       	<div class="form-group">*/
/* 	        <label class="control-label" for="input-id">Product ID</label>*/
/* 	        <input type="text" name="pid" value="" id="pid" class="form-control" readonly>*/
/* 	     </div>*/
/* 	     <div class="form-group">*/
/* 	        <label class="control-label" for="input-pname">Product Name</label>*/
/* 	        <input type="text" name="pname" value="" id="pname" class="form-control" readonly>*/
/* 	     </div>*/
/*       	<div class="form-group">*/
/* 	        <label class="control-label" for="input-prefid">Reference ID</label>*/
/* 	        <input type="text" name="prefid" value="" id="prefid" class="form-control" readonly>*/
/* 	     </div>*/
/*        <div class="form-group">*/
/*           <label class="control-label" for="input-pestamt">Estimated Amount</label>*/
/*           <input type="text" name="pestamt" value="" id="pestamt" class="form-control" readonly>*/
/*        </div>*/
/* 	     <div class="form-group">*/
/* 	        <label class="control-label" for="input-pfinalamt">Final Amount</label>*/
/* 	        <input type="text" name="pfinalamt" value="" id="pfinalamt" class="form-control" readonly>*/
/* 	     </div>*/
/*       	<div class="form-group">*/
/* 	        <label class="control-label" for="input-pst">Status</label>*/
/* 	        <input type="text" name="pst" value="" id="pst" class="form-control" readonly>*/
/* 	     </div>*/
/* 	     <div class="form-group">*/
/* 	        <label class="control-label" for="input-created_dt">Created Date</label>*/
/* 	        <input type="text" name="created_dt" value="" id="created_dt" class="form-control" readonly>*/
/* 	     </div>*/
/*       	*/
/*       </div>*/
/*       <div class="modal-footer">*/
/*         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>*/
/*       </div>*/
/*     </div>*/
/*    		*/
/*    		</div>*/
/*    		</div>*/
/*    		</div>*/
/* */
/*  </div>*/
/*   </div>*/
/*   */
/*   <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script> */
/* <script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script> */
/* <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script> */
/* <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script> */
/* <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script> */
/* <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script> */
/* <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script> */
/* */
/* <script>*/
/* /*$(document).ready(function() {*/
/*     $('#csh_tbl').DataTable( {*/
/*         dom: 'Bfrtip',*/
/*         buttons: [*/
/*             'copyHtml5',*/
/*             'excelHtml5',*/
/*             'csvHtml5',*/
/*             'pdfHtml5'*/
/*         ]*/
/*     } );*/
/* } );*//* */
/* /*$(document).ready(function() {*/
/*     $('#csh_tbl').DataTable( {*/
/*         dom: 'Bfrtip',*/
/*         buttons: [*/
/*             'copyHtml5',*/
/*             'excelHtml5',*/
/*             'csvHtml5'*/
/*         ]*/
/*     } );*/
/* } );*//* */
/* $(document).ready(function() {*/
/*     $('#csh_tbl').DataTable( {*/
/*         dom: 'Bfrtip',*/
/*         buttons: [*/
/*             {*/
/*                 extend: 'copyHtml5',*/
/*                 exportOptions: {*/
/*                      columns: ':not(.not-export-col)'*/
/*                 }*/
/*             },*/
/*             {*/
/*                 extend: 'excelHtml5',*/
/*                 exportOptions: {*/
/*                      columns: ':not(.not-export-col)'*/
/*                 }*/
/*             },*/
/*             {*/
/*                 extend: 'csvHtml5',*/
/*                 exportOptions: {*/
/*                      columns: ':not(.not-export-col)'*/
/*                 }*/
/*             }*/
/*         ]*/
/*     } );*/
/* } );*/
/* $(document).ready(function() {*/
/*   $(".opencshmodal").click(function(){*/
/*      $('#cashify_id').val($(this).data('id'));*/
/*      var cashify_id = $('#cashify_id').val();*/
/*      $.ajax({*/
/*             url: 'index.php?route=catalog/cashifyreport/getCashifyData&user_token={{ user_token }}',*/
/*             type: 'POST',*/
/*             dataType : 'json',*/
/*             data: {*/
/*             cashify_id: cashify_id*/
/*         },*/
/*         success: function(response) {*/
/*         	var obj = JSON.stringify(response)*/
/*         	var stringify = JSON.parse(obj);*/
/*         	$('#pid').val(stringify['csh_pid']);*/
/*         	$('#pname').val(stringify['csh_pn']);*/
/*           $('#pestamt').val(stringify['quote_amnt']);*/
/*           $('#pfinalamt').val(stringify['final_amt']);*/
/*           $('#pst').val(stringify['status']);*/
/*         	$('#prefid').val(stringify['ref_id']);*/
/*         	$('#created_dt').val(stringify['created_date']);*/
/* */
/*         	//$("#cshModal").modal("show");*/
/* 			*/
/*         }*/
/*       });*/
/*   });*/
/* });*/
/* </script>*/
/* {{ footer }} */
