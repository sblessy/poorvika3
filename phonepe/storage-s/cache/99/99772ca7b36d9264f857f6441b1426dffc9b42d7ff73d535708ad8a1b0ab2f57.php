<?php

/* catalog/mail_form.twig */
class __TwigTemplate_f27608c0e45a25f493e529658cad1be52257052c5a7640d8c501f08651f921dd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\">
        <button type=\"submit\" form=\"form-transaction\" data-toggle=\"tooltip\" title=\"";
        // line 6
        echo (isset($context["button_save"]) ? $context["button_save"] : null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i></button>
        <a href=\"";
        // line 7
        echo (isset($context["cancel"]) ? $context["cancel"] : null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo (isset($context["button_cancel"]) ? $context["button_cancel"] : null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
      <h1>";
        // line 8
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
      <ul class=\"breadcrumb\">
          ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 11
            echo "        <li><a href=\"";
            echo $this->getAttribute($context["breadcrumb"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array());
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "      </ul>
    </div>
  </div>

  <div class=\"container-fluid\">
    ";
        // line 18
        if ((isset($context["error_warning"]) ? $context["error_warning"] : null)) {
            // line 19
            echo "    <div class=\"alert alert-danger\"><i class=\"fa fa-exclamation-circle\"></i>";
            echo (isset($context["error_warning"]) ? $context["error_warning"] : null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 23
        echo "
    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <h3 class=\"panel-title\"><i class=\"fa fa-pencil\"></i>";
        // line 26
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h3>
      </div>

      <div class=\"panel-body\">

        <div class=\"alert alert-info\"><i class=\"fa fa-exclamation-circle\"></i>";
        // line 31
        echo (isset($context["info_mail"]) ? $context["info_mail"] : null);
        echo "
          <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
        </div>

        <ul class=\"nav nav-tabs\">
          <li class=\"active\"><a href=\"#tab-general\" data-toggle=\"tab\">";
        // line 36
        echo (isset($context["tab_general"]) ? $context["tab_general"] : null);
        echo "</a></li>
          <li><a href=\"#tab-info\" data-toggle=\"tab\">";
        // line 37
        echo (isset($context["tab_info"]) ? $context["tab_info"] : null);
        echo "</a></li>
        </ul>

        <div class=\"tab-content\">
          <div class=\"tab-pane active\" id=\"tab-general\">
            <form action=\"";
        // line 42
        echo (isset($context["save"]) ? $context["save"] : null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-transaction\" class=\"form-horizontal\">

              <div class=\"form-group required\">
                <label class=\"col-sm-2 control-label\" for=\"input-name\"><span data-toggle=\"tooltip\" title=\"";
        // line 45
        echo (isset($context["entry_name_info"]) ? $context["entry_name_info"] : null);
        echo "\">";
        echo (isset($context["entry_name"]) ? $context["entry_name"] : null);
        echo "</span></label>
                <div class=\"col-sm-10\">
                  <input type=\"text\" id=\"input-name\" class=\"form-control\" name=\"name\" value=\"";
        // line 47
        echo (isset($context["name"]) ? $context["name"] : null);
        echo "\" />
                  <input type=\"hidden\" name=\"mail_id\" value=\"";
        // line 48
        echo (isset($context["mail_id"]) ? $context["mail_id"] : null);
        echo "\" />
                </div>
              </div>

              <div class=\"form-group required\">
                <label class=\"col-sm-2 control-label\" for=\"input-subject\"><span data-toggle=\"tooltip\" title=\"";
        // line 53
        echo (isset($context["entry_subject_info"]) ? $context["entry_subject_info"] : null);
        echo "\">";
        echo (isset($context["entry_subject"]) ? $context["entry_subject"] : null);
        echo "</span></label>
                <div class=\"col-sm-10\">
                  <input type=\"text\" id=\"input-subject\" class=\"form-control\" name=\"subject\" value=\"";
        // line 55
        echo (isset($context["subject"]) ? $context["subject"] : null);
        echo "\" />
                </div>
              </div>

              <div class=\"form-group required\">
                <label class=\"col-sm-2 control-label\" for=\"input-message\"><span data-toggle=\"tooltip\" title=\"";
        // line 60
        echo (isset($context["entry_message_info"]) ? $context["entry_message_info"] : null);
        echo "\">";
        echo (isset($context["entry_message"]) ? $context["entry_message"] : null);
        echo "</span></label>
                <div class=\"col-sm-10\">
                  <textarea id=\"input-message\" class=\"form-control summernote\" name=\"message\" rows=\"3\">";
        // line 62
        echo (isset($context["message"]) ? $context["message"] : null);
        echo "</textarea>
                </div>
              </div>
            </form>
          </div>
          <div class=\"tab-pane\" id=\"tab-info\">
            <p class=\"text-info\">";
        // line 68
        echo (isset($context["info_mail_add"]) ? $context["info_mail_add"] : null);
        echo "</p>
            <table class=\"table table-bordered table-hover\">
              <thead>
              <tr>
                <td>";
        // line 72
        echo (isset($context["entry_for"]) ? $context["entry_for"] : null);
        echo "</td>
                <td>";
        // line 73
        echo (isset($context["entry_code"]) ? $context["entry_code"] : null);
        echo "</td>
              </tr>
              </thead>
              ";
        // line 76
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["mail_help"]) ? $context["mail_help"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["help"]) {
            // line 77
            echo "                <tr>
                  <td>";
            // line 78
            echo twig_capitalize_string_filter($this->env, twig_replace_filter($context["help"], array("}" => "", "{" => "", "_" => " ", "config" => "Store")));
            echo "</td>
                  <td>";
            // line 79
            echo $context["help"];
            echo "</td>
                </tr>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['help'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 82
        echo "            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
  <script type=\"text/javascript\" src=\"view/javascript/summernote/summernote.js\"></script>
  <link href=\"view/javascript/summernote/summernote.css\" rel=\"stylesheet\" />
  <script type=\"text/javascript\" src=\"view/javascript/summernote/opencart.js\"></script>
  <script src=\"view/javascript/summernote/webkul_summernote.js\"></script>
";
        // line 93
        echo (isset($context["footer"]) ? $context["footer"] : null);
        echo "
";
    }

    public function getTemplateName()
    {
        return "catalog/mail_form.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  214 => 93,  201 => 82,  192 => 79,  188 => 78,  185 => 77,  181 => 76,  175 => 73,  171 => 72,  164 => 68,  155 => 62,  148 => 60,  140 => 55,  133 => 53,  125 => 48,  121 => 47,  114 => 45,  108 => 42,  100 => 37,  96 => 36,  88 => 31,  80 => 26,  75 => 23,  67 => 19,  65 => 18,  58 => 13,  47 => 11,  43 => 10,  38 => 8,  32 => 7,  28 => 6,  19 => 1,);
    }
}
/* {{ header }}{{ column_left }}*/
/* <div id="content">*/
/*   <div class="page-header">*/
/*     <div class="container-fluid">*/
/*       <div class="pull-right">*/
/*         <button type="submit" form="form-transaction" data-toggle="tooltip" title="{{ button_save }}" class="btn btn-primary"><i class="fa fa-save"></i></button>*/
/*         <a href="{{ cancel }}" data-toggle="tooltip" title="{{ button_cancel }}" class="btn btn-default"><i class="fa fa-reply"></i></a></div>*/
/*       <h1>{{ heading_title }}</h1>*/
/*       <ul class="breadcrumb">*/
/*           {% for breadcrumb in breadcrumbs %}*/
/*         <li><a href="{{ breadcrumb.href }}">{{ breadcrumb.text }}</a></li>*/
/*         {% endfor %}*/
/*       </ul>*/
/*     </div>*/
/*   </div>*/
/* */
/*   <div class="container-fluid">*/
/*     {% if error_warning %}*/
/*     <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i>{{ error_warning }}*/
/*       <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*     </div>*/
/*     {% endif %}*/
/* */
/*     <div class="panel panel-default">*/
/*       <div class="panel-heading">*/
/*         <h3 class="panel-title"><i class="fa fa-pencil"></i>{{ heading_title }}</h3>*/
/*       </div>*/
/* */
/*       <div class="panel-body">*/
/* */
/*         <div class="alert alert-info"><i class="fa fa-exclamation-circle"></i>{{ info_mail }}*/
/*           <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*         </div>*/
/* */
/*         <ul class="nav nav-tabs">*/
/*           <li class="active"><a href="#tab-general" data-toggle="tab">{{ tab_general }}</a></li>*/
/*           <li><a href="#tab-info" data-toggle="tab">{{ tab_info }}</a></li>*/
/*         </ul>*/
/* */
/*         <div class="tab-content">*/
/*           <div class="tab-pane active" id="tab-general">*/
/*             <form action="{{ save }}" method="post" enctype="multipart/form-data" id="form-transaction" class="form-horizontal">*/
/* */
/*               <div class="form-group required">*/
/*                 <label class="col-sm-2 control-label" for="input-name"><span data-toggle="tooltip" title="{{ entry_name_info }}">{{ entry_name }}</span></label>*/
/*                 <div class="col-sm-10">*/
/*                   <input type="text" id="input-name" class="form-control" name="name" value="{{ name }}" />*/
/*                   <input type="hidden" name="mail_id" value="{{ mail_id }}" />*/
/*                 </div>*/
/*               </div>*/
/* */
/*               <div class="form-group required">*/
/*                 <label class="col-sm-2 control-label" for="input-subject"><span data-toggle="tooltip" title="{{ entry_subject_info }}">{{ entry_subject }}</span></label>*/
/*                 <div class="col-sm-10">*/
/*                   <input type="text" id="input-subject" class="form-control" name="subject" value="{{ subject }}" />*/
/*                 </div>*/
/*               </div>*/
/* */
/*               <div class="form-group required">*/
/*                 <label class="col-sm-2 control-label" for="input-message"><span data-toggle="tooltip" title="{{ entry_message_info }}">{{ entry_message }}</span></label>*/
/*                 <div class="col-sm-10">*/
/*                   <textarea id="input-message" class="form-control summernote" name="message" rows="3">{{ message }}</textarea>*/
/*                 </div>*/
/*               </div>*/
/*             </form>*/
/*           </div>*/
/*           <div class="tab-pane" id="tab-info">*/
/*             <p class="text-info">{{ info_mail_add }}</p>*/
/*             <table class="table table-bordered table-hover">*/
/*               <thead>*/
/*               <tr>*/
/*                 <td>{{ entry_for }}</td>*/
/*                 <td>{{ entry_code }}</td>*/
/*               </tr>*/
/*               </thead>*/
/*               {% for help in mail_help %}*/
/*                 <tr>*/
/*                   <td>{{ (help|replace({'}': '', '{' : '', '_' : ' ', 'config' : 'Store'}))|capitalize }}</td>*/
/*                   <td>{{ help}}</td>*/
/*                 </tr>*/
/*               {% endfor %}*/
/*             </table>*/
/*           </div>*/
/*         </div>*/
/*       </div>*/
/*     </div>*/
/*   </div>*/
/* </div>*/
/*   <script type="text/javascript" src="view/javascript/summernote/summernote.js"></script>*/
/*   <link href="view/javascript/summernote/summernote.css" rel="stylesheet" />*/
/*   <script type="text/javascript" src="view/javascript/summernote/opencart.js"></script>*/
/*   <script src="view/javascript/summernote/webkul_summernote.js"></script>*/
/* {{ footer}}*/
/* */
