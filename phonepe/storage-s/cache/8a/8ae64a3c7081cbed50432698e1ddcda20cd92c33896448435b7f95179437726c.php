<?php

/* default/template/checkout/checkout_address.twig */
class __TwigTemplate_47169b3e9fe9200e0eaa118d8b60393cb8a2bb2088f0bffc10b90b172e08d32c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo "
<style>
.address-page{
\tmargin-top: 130px;
}
</style>
<div class=\"address-page\">
<div class=\"container\">
  
  <div class=\"row\">
    <div id=\"content\" class=\"";
        // line 11
        echo (isset($context["class"]) ? $context["class"] : null);
        echo "\">";
        echo (isset($context["content_top"]) ? $context["content_top"] : null);
        echo "
     
      
\t  
\t<div class=\"col-md-8\">
\t\t<div class=\"cart-block address-block\">
\t\t\t<h3><i class=\"fa fa-home\"></i>My Addresses<a id=\"new-shipping-addr\"><i class=\"fa fa-plus\"></i>Add New Address</a></h3>
\t\t\t<div id=\"shipping-address-details\">
\t\t\t</div>
\t\t</div>
\t</div>
        
        
    <div class=\"col-md-4\">
        <div class=\"cart-block summary\">
            <h3>Payment Summary</h3>
            <ul>
\t\t\t\t";
        // line 28
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["totals"]) ? $context["totals"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["total"]) {
            // line 29
            echo "\t\t\t\t\t<li>
\t\t\t\t\t";
            // line 30
            if (($this->getAttribute($context["total"], "title", array()) == "Total")) {
                // line 31
                echo "\t\t\t\t\t\tPrice-2 items
\t\t\t\t\t";
            } else {
                // line 33
                echo "\t\t\t\t\t\t";
                echo $this->getAttribute($context["total"], "title", array());
                echo "
\t\t\t\t\t";
            }
            // line 35
            echo "\t\t\t\t\t  <span>";
            echo $this->getAttribute($context["total"], "text", array());
            echo "</span>
\t\t\t\t\t</li>
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['total'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 38
        echo "\t\t\t\t<li class=\"\">Price - 2 Items<span>";
        echo (isset($context["total_amount"]) ? $context["total_amount"] : null);
        echo "</span></li>
\t\t\t\t<li>Delivery Fee<span class=\"green-color\">Free</span></li>
\t\t\t\t<li class=\"total-amount\">Total Amount<span>";
        // line 40
        echo (isset($context["total_amount"]) ? $context["total_amount"] : null);
        echo "</span></li>
                
                  
                  <li class=\"save-offer\"><i class=\"fa fa-percent\"></i>You will save &#8377;6,000 on this Order</li>
            </ul>
        </div>
        
        <div id=\"checkout-proceed\" class=\"checkout-button\">
                <a class=\"btn btn-checkout\">Proceed To Checkout</a>
        </div>
    </div>
\t
\t\t
\t\t
        
        
      ";
        // line 56
        echo (isset($context["content_bottom"]) ? $context["content_bottom"] : null);
        echo "</div>
    ";
        // line 57
        echo (isset($context["column_right"]) ? $context["column_right"] : null);
        echo "</div>
</div>
</div>
<script type=\"text/javascript\">
";
        // line 61
        if ( !(isset($context["logged"]) ? $context["logged"] : null)) {
            // line 62
            echo "\$(document).ready(function() {
\t\$(\"#modalLoginForm\").modal('show');
});
\$('.close').click(function (e) {
\t\$('#modalLoginForm').modal('hide');
\twindow.location.href = 'index.php?route=checkout/cart';
});
";
        } else {
            // line 70
            echo "\$(document).ready(function() {
\t
\t\$.ajax({
\t\turl: 'index.php?route=checkout/payment_address',
\t\tdataType: 'html',
\t\tsuccess: function(html) {
\t\t\t\$('#shipping-address-details').html(html);
\t\t\t
\t\t},
\t\terror: function(xhr, ajaxOptions, thrownError) {
\t\t\talert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
\t\t}
\t});
});
\$('#checkout-proceed').click(function () {
\t\t\$('#button-payment-method').trigger('click');
\t});
\$(document).delegate('#button-payment-address', 'click', function() {
    \$.ajax({
        url: 'index.php?route=checkout/payment_address/save',
        type: 'post',
        data: \$('#addr_details input[type=\\'text\\'], #addr_details input[type=\\'date\\'], #addr_details input[type=\\'datetime-local\\'], #addr_details input[type=\\'time\\'], #addr_details input[type=\\'password\\'], #addr_details input[type=\\'checkbox\\']:checked, #addr_details input[type=\\'radio\\']:checked, #addr_details input[type=\\'hidden\\'], #addr_details textarea, #addr_details radio'),
        dataType: 'json',
        beforeSend: function() {
        \t\$('#button-payment-address').button('loading');
\t\t},
        complete: function() {alert(\"hh\");
\t\t\t\$('#button-payment-address').button('reset');
        },
        success: function(json) {
            \$('.alert-dismissible, .text-danger').remove();
\t\t\t\$('.form-group').removeClass('has-error');
            
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
        }
    });
});
";
        }
        // line 110
        echo "</script>
";
        // line 111
        echo (isset($context["footer"]) ? $context["footer"] : null);
    }

    public function getTemplateName()
    {
        return "default/template/checkout/checkout_address.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  176 => 111,  173 => 110,  131 => 70,  121 => 62,  119 => 61,  112 => 57,  108 => 56,  89 => 40,  83 => 38,  73 => 35,  67 => 33,  63 => 31,  61 => 30,  58 => 29,  54 => 28,  32 => 11,  19 => 1,);
    }
}
/* {{ header }}*/
/* <style>*/
/* .address-page{*/
/* 	margin-top: 130px;*/
/* }*/
/* </style>*/
/* <div class="address-page">*/
/* <div class="container">*/
/*   */
/*   <div class="row">*/
/*     <div id="content" class="{{ class }}">{{ content_top }}*/
/*      */
/*       */
/* 	  */
/* 	<div class="col-md-8">*/
/* 		<div class="cart-block address-block">*/
/* 			<h3><i class="fa fa-home"></i>My Addresses<a id="new-shipping-addr"><i class="fa fa-plus"></i>Add New Address</a></h3>*/
/* 			<div id="shipping-address-details">*/
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/*         */
/*         */
/*     <div class="col-md-4">*/
/*         <div class="cart-block summary">*/
/*             <h3>Payment Summary</h3>*/
/*             <ul>*/
/* 				{% for total in totals %}*/
/* 					<li>*/
/* 					{% if total.title == "Total" %}*/
/* 						Price-2 items*/
/* 					{% else %}*/
/* 						{{ total.title }}*/
/* 					{% endif %}*/
/* 					  <span>{{ total.text }}</span>*/
/* 					</li>*/
/* 				{% endfor %}*/
/* 				<li class="">Price - 2 Items<span>{{ total_amount }}</span></li>*/
/* 				<li>Delivery Fee<span class="green-color">Free</span></li>*/
/* 				<li class="total-amount">Total Amount<span>{{ total_amount }}</span></li>*/
/*                 */
/*                   */
/*                   <li class="save-offer"><i class="fa fa-percent"></i>You will save &#8377;6,000 on this Order</li>*/
/*             </ul>*/
/*         </div>*/
/*         */
/*         <div id="checkout-proceed" class="checkout-button">*/
/*                 <a class="btn btn-checkout">Proceed To Checkout</a>*/
/*         </div>*/
/*     </div>*/
/* 	*/
/* 		*/
/* 		*/
/*         */
/*         */
/*       {{ content_bottom }}</div>*/
/*     {{ column_right }}</div>*/
/* </div>*/
/* </div>*/
/* <script type="text/javascript">*/
/* {% if not logged %}*/
/* $(document).ready(function() {*/
/* 	$("#modalLoginForm").modal('show');*/
/* });*/
/* $('.close').click(function (e) {*/
/* 	$('#modalLoginForm').modal('hide');*/
/* 	window.location.href = 'index.php?route=checkout/cart';*/
/* });*/
/* {% else %}*/
/* $(document).ready(function() {*/
/* 	*/
/* 	$.ajax({*/
/* 		url: 'index.php?route=checkout/payment_address',*/
/* 		dataType: 'html',*/
/* 		success: function(html) {*/
/* 			$('#shipping-address-details').html(html);*/
/* 			*/
/* 		},*/
/* 		error: function(xhr, ajaxOptions, thrownError) {*/
/* 			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/* 		}*/
/* 	});*/
/* });*/
/* $('#checkout-proceed').click(function () {*/
/* 		$('#button-payment-method').trigger('click');*/
/* 	});*/
/* $(document).delegate('#button-payment-address', 'click', function() {*/
/*     $.ajax({*/
/*         url: 'index.php?route=checkout/payment_address/save',*/
/*         type: 'post',*/
/*         data: $('#addr_details input[type=\'text\'], #addr_details input[type=\'date\'], #addr_details input[type=\'datetime-local\'], #addr_details input[type=\'time\'], #addr_details input[type=\'password\'], #addr_details input[type=\'checkbox\']:checked, #addr_details input[type=\'radio\']:checked, #addr_details input[type=\'hidden\'], #addr_details textarea, #addr_details radio'),*/
/*         dataType: 'json',*/
/*         beforeSend: function() {*/
/*         	$('#button-payment-address').button('loading');*/
/* 		},*/
/*         complete: function() {alert("hh");*/
/* 			$('#button-payment-address').button('reset');*/
/*         },*/
/*         success: function(json) {*/
/*             $('.alert-dismissible, .text-danger').remove();*/
/* 			$('.form-group').removeClass('has-error');*/
/*             */
/*         },*/
/*         error: function(xhr, ajaxOptions, thrownError) {*/
/*             alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*         }*/
/*     });*/
/* });*/
/* {% endif %}*/
/* </script>*/
/* {{ footer }}*/
