<?php

/* extension/module/so_countdown/form.twig */
class __TwigTemplate_aeca519639c5ea182a1dd918037145465ebb4480e0a044e721fb4e512c4cf7e1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo "
";
        // line 2
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "
<div id=\"content\">
  \t<div class=\"page-header\">
    \t<div class=\"container-fluid\">
      \t\t<div class=\"pull-right\">
        \t\t<button type=\"submit\" form=\"form-so-countdown\" data-toggle=\"tooltip\" title=\"";
        // line 7
        echo (isset($context["button_save"]) ? $context["button_save"] : null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i> ";
        echo (isset($context["button_save"]) ? $context["button_save"] : null);
        echo "</button>
                <a class=\"btn btn-success\" onclick=\"\$('#action').val('save_edit');\$('#form-so-countdown').submit();\" data-toggle=\"tooltip\" title=\"";
        // line 8
        echo (isset($context["button_savestay"]) ? $context["button_savestay"] : null);
        echo "\" ><i class=\"fa fa-pencil-square-o\"></i> ";
        echo (isset($context["button_savestay"]) ? $context["button_savestay"] : null);
        echo "</a>
        \t\t<a href=\"";
        // line 9
        echo (isset($context["cancel"]) ? $context["cancel"] : null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo (isset($context["button_cancel"]) ? $context["button_cancel"] : null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i> ";
        echo (isset($context["button_cancel"]) ? $context["button_cancel"] : null);
        echo "</a>
        \t</div>
\t\t\t<h1>";
        // line 11
        echo (isset($context["heading_title_so"]) ? $context["heading_title_so"] : null);
        echo "</h1>
      \t\t<ul class=\"breadcrumb\">
        \t\t";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 14
            echo "                <li><a href=\"";
            echo $this->getAttribute($context["breadcrumb"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array());
            echo "</a></li>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 16
        echo "      \t\t</ul>
    \t</div>
  \t</div>
  \t<div class=\"container-fluid\">
  \t\t";
        // line 20
        if ((isset($context["error_warning"]) ? $context["error_warning"] : null)) {
            // line 21
            echo "    \t\t<div class=\"alert alert-danger\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo (isset($context["error_warning"]) ? $context["error_warning"] : null);
            echo "
      \t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    \t\t</div>
    \t";
        }
        // line 25
        echo "    \t";
        if ((isset($context["success"]) ? $context["success"] : null)) {
            // line 26
            echo "    \t\t<div class=\"alert alert-success\"><i class=\"fa fa-check-circle\"></i> ";
            echo (isset($context["success"]) ? $context["success"] : null);
            echo "
      \t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    \t\t</div>
    \t";
        }
        // line 30
        echo "    \t<div class=\"panel panel-default\">
      \t\t<div class=\"panel-heading\">
        \t\t<h3 class=\"panel-title\"><i class=\"fa fa-pencil\"></i> ";
        // line 32
        echo (isset($context["text_edit"]) ? $context["text_edit"] : null);
        echo "</h3>
      \t\t</div>
      \t\t<div class=\"panel-body\">
\t   \t\t\t<form action=\"";
        // line 35
        echo (isset($context["action"]) ? $context["action"] : null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-so-countdown\" class=\"form-horizontal\">
\t   \t\t\t\t<input type=\"hidden\" name=\"action\" id=\"action\" value=\"\"/>
\t   \t\t\t\t<ul class=\"nav nav-tabs\" id=\"CheckoutTab\">
\t\t\t\t\t\t<li class=\"active\"><a href=\"#tab-module-setting\" data-toggle=\"tab\"><i class=\"fa fa-cog fa-fw\"></i> ";
        // line 38
        echo (isset($context["tab_module_setting"]) ? $context["tab_module_setting"] : null);
        echo "</a></li>
\t\t\t\t\t</ul>
\t\t\t\t\t<div class=\"tab-content\">
\t\t\t\t\t\t<div class=\"tab-pane active in\" id=\"tab-module-setting\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"input-status\"><span data-toggle=\"tooltip\" title=\"\" data-original-title=\"";
        // line 43
        echo (isset($context["text_status_help"]) ? $context["text_status_help"] : null);
        echo "\">";
        echo (isset($context["text_status"]) ? $context["text_status"] : null);
        echo "</span></label>
\t\t\t\t\t\t\t\t<div class=\"col-sm-3\">
\t\t\t\t\t\t\t\t\t<select name=\"status\" id=\"input-status\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t\t";
        // line 46
        if ((array_key_exists("status", $context) && ((isset($context["status"]) ? $context["status"] : null) == 1))) {
            // line 47
            echo "\t\t\t                \t\t\t\t<option value=\"1\" selected=\"selected\">";
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "</option>
\t\t\t                \t\t\t\t<option value=\"0\">";
            // line 48
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "</option>
\t\t\t                \t\t\t";
        } else {
            // line 50
            echo "\t\t\t                \t\t\t\t<option value=\"1\">";
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "</option>
\t\t\t                \t\t\t\t<option value=\"0\" selected=\"selected\">";
            // line 51
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "</option>
\t\t\t                \t\t\t";
        }
        // line 53
        echo "\t\t\t                \t\t</select>
\t\t\t\t  \t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"form-group required\">
\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"input-name\"><span data-toggle=\"tooltip\" title=\"\" data-original-title=\"";
        // line 57
        echo (isset($context["text_name_help"]) ? $context["text_name_help"] : null);
        echo "\">";
        echo (isset($context["text_name"]) ? $context["text_name"] : null);
        echo "</span></label>
\t\t\t\t\t\t\t\t<div class=\"col-sm-3\">
\t\t\t\t\t\t\t\t\t<input type=\"text\" id=\"input-name\" name=\"name\" value=\"";
        // line 59
        echo (isset($context["name"]) ? $context["name"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_name"]) ? $context["entry_name"] : null);
        echo "\" id=\"input-name\" class=\"form-control\" />
                                    ";
        // line 60
        if ((isset($context["error_name"]) ? $context["error_name"] : null)) {
            // line 61
            echo "                                        <div class=\"text-danger\">";
            echo (isset($context["error_name"]) ? $context["error_name"] : null);
            echo "</div>
                                    ";
        }
        // line 63
        echo "\t\t\t\t  \t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"input-priority\"><span data-toggle=\"tooltip\" title=\"\" data-original-title=\"";
        // line 66
        echo (isset($context["text_priority_help"]) ? $context["text_priority_help"] : null);
        echo "\">";
        echo (isset($context["text_priority"]) ? $context["text_priority"] : null);
        echo "</span></label>
\t\t\t\t\t\t\t\t<div class=\"col-sm-3\">
\t\t\t\t\t\t\t\t\t<input type=\"text\" id=\"input-priority\" name=\"priority\" value=\"";
        // line 68
        echo (isset($context["priority"]) ? $context["priority"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_priority"]) ? $context["entry_priority"] : null);
        echo "\" id=\"input-name\" class=\"form-control\" />
\t\t\t\t  \t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"form-group required\">
\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"input-width\"><span data-toggle=\"tooltip\" title=\"\" data-original-title=\"";
        // line 72
        echo (isset($context["text_width_help"]) ? $context["text_width_help"] : null);
        echo "\">";
        echo (isset($context["text_width"]) ? $context["text_width"] : null);
        echo "</span></label>
\t\t\t\t\t\t\t\t<div class=\"col-sm-3\">
\t\t\t\t\t\t\t\t\t<input type=\"text\" id=\"input-width\" name=\"width\" value=\"";
        // line 74
        echo (isset($context["width"]) ? $context["width"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_width"]) ? $context["entry_width"] : null);
        echo "\" id=\"input-name\" class=\"form-control\" />
                                    ";
        // line 75
        if ((isset($context["error_width"]) ? $context["error_width"] : null)) {
            // line 76
            echo "                                        <div class=\"text-danger\">";
            echo (isset($context["error_width"]) ? $context["error_width"] : null);
            echo "</div>
                                    ";
        }
        // line 78
        echo "\t\t\t\t  \t\t\t\t</div>
\t\t\t\t\t\t\t</div>
                            <div class=\"form-group\">
\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"input-opacity\"><span data-toggle=\"tooltip\" title=\"\" data-original-title=\"";
        // line 81
        echo (isset($context["text_opacity_help"]) ? $context["text_opacity_help"] : null);
        echo "\">";
        echo (isset($context["text_opacity"]) ? $context["text_opacity"] : null);
        echo "</span></label>
\t\t\t\t\t\t\t\t<div class=\"col-sm-3\">
\t\t\t\t\t\t\t\t\t<input type=\"text\" id=\"input-opacity\" name=\"opacity\" value=\"";
        // line 83
        echo (isset($context["opacity"]) ? $context["opacity"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_opacity"]) ? $context["entry_opacity"] : null);
        echo "\" id=\"input-name\" class=\"form-control\" />
\t\t\t\t  \t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"input-display-countdown\"><span data-toggle=\"tooltip\" title=\"\" data-original-title=\"";
        // line 87
        echo (isset($context["text_display_countdown_help"]) ? $context["text_display_countdown_help"] : null);
        echo "\">";
        echo (isset($context["text_display_countdown"]) ? $context["text_display_countdown"] : null);
        echo "</span></label>
\t\t\t\t\t\t\t\t<div class=\"col-sm-3\">
\t\t\t\t\t\t\t\t\t";
        // line 89
        if ((array_key_exists("display_countdown", $context) && ((isset($context["display_countdown"]) ? $context["display_countdown"] : null) == 1))) {
            // line 90
            echo "\t\t\t\t\t\t\t\t\t\t<label class=\"radio-inline\"><input type=\"radio\" name=\"display_countdown\" value=\"1\" checked />";
            echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
            echo "</label>
\t                                    <label class=\"radio-inline\"><input type=\"radio\" name=\"display_countdown\" value=\"0\" />";
            // line 91
            echo (isset($context["text_no"]) ? $context["text_no"] : null);
            echo "</label>
\t                               \t";
        } else {
            // line 93
            echo "\t                               \t\t<label class=\"radio-inline\"><input type=\"radio\" name=\"display_countdown\" value=\"1\"  />";
            echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
            echo "</label>
\t                                    <label class=\"radio-inline\"><input type=\"radio\" name=\"display_countdown\" value=\"0\" checked />";
            // line 94
            echo (isset($context["text_no"]) ? $context["text_no"] : null);
            echo "</label>
\t                               \t";
        }
        // line 96
        echo "\t\t\t\t  \t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"input-date-start\"><span data-toggle=\"tooltip\" title=\"\" data-original-title=\"";
        // line 99
        echo (isset($context["text_date_start_help"]) ? $context["text_date_start_help"] : null);
        echo "\">";
        echo (isset($context["text_date_start"]) ? $context["text_date_start"] : null);
        echo "</span></label>
\t\t\t\t\t\t\t\t<div class=\"col-sm-3\">
\t\t\t\t\t\t\t\t\t<div class=\"input-group datetime\">
                                        <input type=\"text\" name=\"date_start\" value=\"";
        // line 102
        echo (isset($context["date_start"]) ? $context["date_start"] : null);
        echo "\" placeholder=\"\" data-date-format=\"YYYY-MM-DD HH:mm:ss\" class=\"form-control\" />
                                        <span class=\"input-group-btn\">
                                            <button class=\"btn btn-default\" type=\"button\"><i class=\"fa fa-calendar\"></i></button>
                                        </span>
                                    </div>
\t\t\t\t  \t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"input-date-expire\"><span data-toggle=\"tooltip\" title=\"\" data-original-title=\"";
        // line 110
        echo (isset($context["text_date_expire_help"]) ? $context["text_date_expire_help"] : null);
        echo "\">";
        echo (isset($context["text_date_expire"]) ? $context["text_date_expire"] : null);
        echo "</span></label>
\t\t\t\t\t\t\t\t<div class=\"col-sm-3\">
\t\t\t\t\t\t\t\t\t<div class=\"input-group datetime\">
                                        <input type=\"text\" name=\"date_expire\" value=\"";
        // line 113
        echo (isset($context["date_expire"]) ? $context["date_expire"] : null);
        echo "\" placeholder=\"\" data-date-format=\"YYYY-MM-DD HH:mm:ss\" class=\"form-control\" />
                                        <span class=\"input-group-btn\">
                                            <button class=\"btn btn-default\" type=\"button\"><i class=\"fa fa-calendar\"></i></button>
                                        </span>
                                    </div>
                                    ";
        // line 118
        if ((isset($context["error_date"]) ? $context["error_date"] : null)) {
            // line 119
            echo "                                        <div class=\"text-danger clear clr clearfix\">";
            echo (isset($context["error_date"]) ? $context["error_date"] : null);
            echo "</div>
                                    ";
        }
        // line 121
        echo "\t\t\t\t  \t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"form-group \">                                
                                <div class=\"col-sm-10\">
                                    <ul class=\"nav nav-tabs\" id=\"language\">
                                    \t";
        // line 126
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 127
            echo "\t\t\t\t\t\t\t                <li><a href=\"#language";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" data-toggle=\"tab\"><img src=\"language/";
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /> ";
            echo $this->getAttribute($context["language"], "name", array());
            echo "</a></li>
\t\t\t\t\t\t                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 129
        echo "                                    </ul>
                                    <div class=\"tab-content\">
                                    \t";
        // line 131
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 132
            echo "\t                                        <div class=\"tab-pane\" id=\"language";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\">
                                                <div class=\"form-group \">
                                                    <label class=\"col-sm-2 control-label\" for=\"input-heading-title\"><span data-toggle=\"tooltip\" title=\"\" data-original-title=\"";
            // line 134
            echo (isset($context["text_heading_title_help"]) ? $context["text_heading_title_help"] : null);
            echo "\">";
            echo (isset($context["text_heading_title"]) ? $context["text_heading_title"] : null);
            echo "</span></label>
                                                    <div class=\"col-sm-10\">
                                                        <input type=\"text\" name=\"popup_description[";
            // line 136
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "][heading_title]\" value=\"";
            echo (($this->getAttribute($this->getAttribute((isset($context["popup_description"]) ? $context["popup_description"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array", false, true), "heading_title", array(), "any", true, true)) ? ($this->getAttribute($this->getAttribute((isset($context["popup_description"]) ? $context["popup_description"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "heading_title", array())) : (""));
            echo "\" placeholder=\"\" class=\"form-control\" />
                                                    </div>
                                                </div>
                                                <div class=\"form-group \">
                                                    <label class=\"col-sm-2 control-label\" for=\"input-description";
            // line 140
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\"><span data-toggle=\"tooltip\" title=\"\" data-original-title=\"";
            echo (isset($context["text_content_help"]) ? $context["text_content_help"] : null);
            echo "\">";
            echo (isset($context["text_content"]) ? $context["text_content"] : null);
            echo "</span></label>
    \t                                            <div class=\"col-sm-10\">
                                                        <textarea name=\"popup_description[";
            // line 142
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "][description]\" id=\"input-description";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" data-toggle=\"summernote\" class=\"form-control\">";
            echo (($this->getAttribute((isset($context["popup_description"]) ? $context["popup_description"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array", true, true)) ? ($this->getAttribute($this->getAttribute((isset($context["popup_description"]) ? $context["popup_description"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "description", array())) : (""));
            echo "</textarea>
                                                    </div>
                                                </div>
\t                                        </div>
                                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 147
        echo "                                    </div>
                                </div>
                            </div>
                            <div class=\"form-group\">
\t\t\t\t                <label class=\"col-sm-2 control-label\" for=\"input-content\"><span data-toggle=\"tooltip\" title=\"\" data-original-title=\"";
        // line 151
        echo (isset($context["text_image_help"]) ? $context["text_image_help"] : null);
        echo "\">";
        echo (isset($context["text_image"]) ? $context["text_image"] : null);
        echo "</span></label>
\t\t\t\t                <div class=\"col-sm-3\"><a href=\"\" id=\"thumb-image\" data-toggle=\"image\" class=\"img-thumbnail\"><img src=\"";
        // line 152
        echo (isset($context["thumb"]) ? $context["thumb"] : null);
        echo "\" alt=\"\" title=\"\" data-placeholder=\"";
        echo (isset($context["placeholder"]) ? $context["placeholder"] : null);
        echo "\" /></a>
\t\t\t\t                  \t<input type=\"hidden\" name=\"image\" value=\"";
        // line 153
        echo (isset($context["image"]) ? $context["image"] : null);
        echo "\" id=\"input-image\" />
\t\t\t\t                </div>
\t\t\t              \t</div>
\t\t\t              \t<div class=\"form-group \">
                                <label class=\"col-sm-2 control-label\">";
        // line 157
        echo (isset($context["entry_store"]) ? $context["entry_store"] : null);
        echo "</label>
                                <div class=\"col-sm-10\">
                                    <div class=\"table-responsive\">
                                        <table id=\"layouts_table\" class=\"table table-bordered table-hover\">
                                            <thead>
                                                <tr>
                                                    <td style=\"width: 1px;\"><input type=\"checkbox\" onclick=\"\$('input[name*=\\'popup_store\\']').prop('checked', this.checked);\" /></td>
                                                    <td class=\"text-left\">";
        // line 164
        echo (isset($context["text_name"]) ? $context["text_name"] : null);
        echo "</td>
                                                    <td class=\"text-left\">";
        // line 165
        echo (isset($context["text_link"]) ? $context["text_link"] : null);
        echo "</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            \t";
        // line 169
        if (twig_in_filter(0, (isset($context["popup_store_id"]) ? $context["popup_store_id"] : null))) {
            // line 170
            echo "                                            \t\t<tr>
                                            \t\t\t<td><input type=\"checkbox\" name=\"popup_store[]\" value=\"0\" checked=\"checked\" /></td>
                                            \t\t\t<td>";
            // line 172
            echo (isset($context["text_default"]) ? $context["text_default"] : null);
            echo "</td>
                                            \t\t\t<td><input type=\"text\" name=\"popup_link[0]\" class=\"form-control\" value=\"";
            // line 173
            echo (($this->getAttribute($this->getAttribute((isset($context["popup_link"]) ? $context["popup_link"] : null), 0, array(), "array", false, true), "link", array(), "any", true, true)) ? ($this->getAttribute($this->getAttribute((isset($context["popup_link"]) ? $context["popup_link"] : null), 0, array(), "array"), "link", array())) : (""));
            echo "\" /></td>
                                            \t\t</tr>
                                            \t";
        } else {
            // line 176
            echo "                                            \t\t<tr>
                                            \t\t\t<td><input type=\"checkbox\" name=\"popup_store[]\" value=\"0\" /></td>
                                            \t\t\t<td>";
            // line 178
            echo (isset($context["text_default"]) ? $context["text_default"] : null);
            echo "</td>
                                            \t\t\t<td><input type=\"text\" name=\"popup_link[0]\" class=\"form-control\" value=\"";
            // line 179
            echo (($this->getAttribute($this->getAttribute((isset($context["popup_link"]) ? $context["popup_link"] : null), 0, array(), "array", false, true), "link", array(), "any", true, true)) ? ($this->getAttribute($this->getAttribute((isset($context["popup_link"]) ? $context["popup_link"] : null), 0, array(), "array"), "link", array())) : (""));
            echo "\" /></td>
                                            \t\t</tr>
                                            \t";
        }
        // line 182
        echo "                                            \t";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["stores"]) ? $context["stores"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["store"]) {
            // line 183
            echo "                                                <tr>
                                                    <td>
                                                    \t";
            // line 185
            if (twig_in_filter($this->getAttribute($context["store"], "store_id", array()), (isset($context["popup_store_id"]) ? $context["popup_store_id"] : null))) {
                // line 186
                echo "                                                    \t\t<input type=\"checkbox\" name=\"popup_store[]\" value=\"";
                echo $this->getAttribute($context["store"], "store_id", array());
                echo "\" checked=\"checked\" />
                                                    \t";
            } else {
                // line 188
                echo "                                                    \t\t<input type=\"checkbox\" name=\"popup_store[]\" value=\"";
                echo $this->getAttribute($context["store"], "store_id", array());
                echo "\" />
                                                    \t";
            }
            // line 190
            echo "                                                    </td>
                                                    <td>";
            // line 191
            echo $this->getAttribute($context["store"], "name", array());
            echo "</td>
                                                    <td><input type=\"text\" name=\"popup_link[";
            // line 192
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "]\" class=\"form-control\" value=\"";
            echo (($this->getAttribute($this->getAttribute((isset($context["popup_link"]) ? $context["popup_link"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array", false, true), "link", array(), "any", true, true)) ? ($this->getAttribute($this->getAttribute((isset($context["popup_link"]) ? $context["popup_link"] : null), $this->getAttribute($context["store"], "store_id", array()), array(), "array"), "link", array())) : (""));
            echo "\" /></td>
                                                </tr>
                                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['store'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 195
        echo "                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t   \t\t\t</form>
\t   \t\t</div>
\t   \t</div>
\t</div>
</div>
<script type=\"text/javascript\" src=\"view/javascript/summernote/summernote.js\"></script>
<link href=\"view/javascript/summernote/summernote.css\" rel=\"stylesheet\" />
<script type=\"text/javascript\" src=\"view/javascript/summernote/opencart.js\"></script> 

<script type=\"text/javascript\">
\t\$('.datetime').datetimepicker({
    \tpickDate: true,
        pickTime: false
    });
</script> 
<script type=\"text/javascript\"><!--
\t\$('#language a:first').tab('show');
//--></script>
";
        // line 220
        echo (isset($context["footer"]) ? $context["footer"] : null);
    }

    public function getTemplateName()
    {
        return "extension/module/so_countdown/form.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  546 => 220,  519 => 195,  508 => 192,  504 => 191,  501 => 190,  495 => 188,  489 => 186,  487 => 185,  483 => 183,  478 => 182,  472 => 179,  468 => 178,  464 => 176,  458 => 173,  454 => 172,  450 => 170,  448 => 169,  441 => 165,  437 => 164,  427 => 157,  420 => 153,  414 => 152,  408 => 151,  402 => 147,  387 => 142,  378 => 140,  369 => 136,  362 => 134,  356 => 132,  352 => 131,  348 => 129,  331 => 127,  327 => 126,  320 => 121,  314 => 119,  312 => 118,  304 => 113,  296 => 110,  285 => 102,  277 => 99,  272 => 96,  267 => 94,  262 => 93,  257 => 91,  252 => 90,  250 => 89,  243 => 87,  234 => 83,  227 => 81,  222 => 78,  216 => 76,  214 => 75,  208 => 74,  201 => 72,  192 => 68,  185 => 66,  180 => 63,  174 => 61,  172 => 60,  166 => 59,  159 => 57,  153 => 53,  148 => 51,  143 => 50,  138 => 48,  133 => 47,  131 => 46,  123 => 43,  115 => 38,  109 => 35,  103 => 32,  99 => 30,  91 => 26,  88 => 25,  80 => 21,  78 => 20,  72 => 16,  61 => 14,  57 => 13,  52 => 11,  43 => 9,  37 => 8,  31 => 7,  23 => 2,  19 => 1,);
    }
}
/* {{ header }}*/
/* {{ column_left }}*/
/* <div id="content">*/
/*   	<div class="page-header">*/
/*     	<div class="container-fluid">*/
/*       		<div class="pull-right">*/
/*         		<button type="submit" form="form-so-countdown" data-toggle="tooltip" title="{{ button_save }}" class="btn btn-primary"><i class="fa fa-save"></i> {{ button_save }}</button>*/
/*                 <a class="btn btn-success" onclick="$('#action').val('save_edit');$('#form-so-countdown').submit();" data-toggle="tooltip" title="{{ button_savestay }}" ><i class="fa fa-pencil-square-o"></i> {{ button_savestay }}</a>*/
/*         		<a href="{{ cancel }}" data-toggle="tooltip" title="{{ button_cancel }}" class="btn btn-default"><i class="fa fa-reply"></i> {{ button_cancel }}</a>*/
/*         	</div>*/
/* 			<h1>{{ heading_title_so }}</h1>*/
/*       		<ul class="breadcrumb">*/
/*         		{% for breadcrumb in breadcrumbs %}*/
/*                 <li><a href="{{ breadcrumb.href }}">{{ breadcrumb.text }}</a></li>*/
/*                 {% endfor %}*/
/*       		</ul>*/
/*     	</div>*/
/*   	</div>*/
/*   	<div class="container-fluid">*/
/*   		{% if error_warning %}*/
/*     		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> {{ error_warning }}*/
/*       			<button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*     		</div>*/
/*     	{% endif %}*/
/*     	{% if success %}*/
/*     		<div class="alert alert-success"><i class="fa fa-check-circle"></i> {{ success }}*/
/*       			<button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*     		</div>*/
/*     	{% endif %}*/
/*     	<div class="panel panel-default">*/
/*       		<div class="panel-heading">*/
/*         		<h3 class="panel-title"><i class="fa fa-pencil"></i> {{ text_edit }}</h3>*/
/*       		</div>*/
/*       		<div class="panel-body">*/
/* 	   			<form action="{{ action }}" method="post" enctype="multipart/form-data" id="form-so-countdown" class="form-horizontal">*/
/* 	   				<input type="hidden" name="action" id="action" value=""/>*/
/* 	   				<ul class="nav nav-tabs" id="CheckoutTab">*/
/* 						<li class="active"><a href="#tab-module-setting" data-toggle="tab"><i class="fa fa-cog fa-fw"></i> {{ tab_module_setting }}</a></li>*/
/* 					</ul>*/
/* 					<div class="tab-content">*/
/* 						<div class="tab-pane active in" id="tab-module-setting">*/
/* 							<div class="form-group">*/
/* 								<label class="col-sm-2 control-label" for="input-status"><span data-toggle="tooltip" title="" data-original-title="{{ text_status_help }}">{{ text_status }}</span></label>*/
/* 								<div class="col-sm-3">*/
/* 									<select name="status" id="input-status" class="form-control">*/
/* 										{% if status is defined and status == 1 %}*/
/* 			                				<option value="1" selected="selected">{{ text_enabled }}</option>*/
/* 			                				<option value="0">{{ text_disabled }}</option>*/
/* 			                			{% else %}*/
/* 			                				<option value="1">{{ text_enabled }}</option>*/
/* 			                				<option value="0" selected="selected">{{ text_disabled }}</option>*/
/* 			                			{% endif %}*/
/* 			                		</select>*/
/* 				  				</div>*/
/* 							</div>*/
/* 							<div class="form-group required">*/
/* 								<label class="col-sm-2 control-label" for="input-name"><span data-toggle="tooltip" title="" data-original-title="{{ text_name_help }}">{{ text_name }}</span></label>*/
/* 								<div class="col-sm-3">*/
/* 									<input type="text" id="input-name" name="name" value="{{ name }}" placeholder="{{ entry_name }}" id="input-name" class="form-control" />*/
/*                                     {% if error_name %}*/
/*                                         <div class="text-danger">{{ error_name }}</div>*/
/*                                     {% endif %}*/
/* 				  				</div>*/
/* 							</div>*/
/* 							<div class="form-group">*/
/* 								<label class="col-sm-2 control-label" for="input-priority"><span data-toggle="tooltip" title="" data-original-title="{{ text_priority_help }}">{{ text_priority }}</span></label>*/
/* 								<div class="col-sm-3">*/
/* 									<input type="text" id="input-priority" name="priority" value="{{ priority }}" placeholder="{{ entry_priority }}" id="input-name" class="form-control" />*/
/* 				  				</div>*/
/* 							</div>*/
/* 							<div class="form-group required">*/
/* 								<label class="col-sm-2 control-label" for="input-width"><span data-toggle="tooltip" title="" data-original-title="{{ text_width_help }}">{{ text_width }}</span></label>*/
/* 								<div class="col-sm-3">*/
/* 									<input type="text" id="input-width" name="width" value="{{ width }}" placeholder="{{ entry_width }}" id="input-name" class="form-control" />*/
/*                                     {% if error_width %}*/
/*                                         <div class="text-danger">{{ error_width }}</div>*/
/*                                     {% endif %}*/
/* 				  				</div>*/
/* 							</div>*/
/*                             <div class="form-group">*/
/* 								<label class="col-sm-2 control-label" for="input-opacity"><span data-toggle="tooltip" title="" data-original-title="{{ text_opacity_help }}">{{ text_opacity }}</span></label>*/
/* 								<div class="col-sm-3">*/
/* 									<input type="text" id="input-opacity" name="opacity" value="{{ opacity }}" placeholder="{{ entry_opacity }}" id="input-name" class="form-control" />*/
/* 				  				</div>*/
/* 							</div>*/
/* 							<div class="form-group">*/
/* 								<label class="col-sm-2 control-label" for="input-display-countdown"><span data-toggle="tooltip" title="" data-original-title="{{ text_display_countdown_help }}">{{ text_display_countdown }}</span></label>*/
/* 								<div class="col-sm-3">*/
/* 									{% if display_countdown is defined and display_countdown == 1 %}*/
/* 										<label class="radio-inline"><input type="radio" name="display_countdown" value="1" checked />{{ text_yes }}</label>*/
/* 	                                    <label class="radio-inline"><input type="radio" name="display_countdown" value="0" />{{ text_no }}</label>*/
/* 	                               	{% else %}*/
/* 	                               		<label class="radio-inline"><input type="radio" name="display_countdown" value="1"  />{{ text_yes }}</label>*/
/* 	                                    <label class="radio-inline"><input type="radio" name="display_countdown" value="0" checked />{{ text_no }}</label>*/
/* 	                               	{% endif %}*/
/* 				  				</div>*/
/* 							</div>*/
/* 							<div class="form-group">*/
/* 								<label class="col-sm-2 control-label" for="input-date-start"><span data-toggle="tooltip" title="" data-original-title="{{ text_date_start_help }}">{{ text_date_start }}</span></label>*/
/* 								<div class="col-sm-3">*/
/* 									<div class="input-group datetime">*/
/*                                         <input type="text" name="date_start" value="{{ date_start }}" placeholder="" data-date-format="YYYY-MM-DD HH:mm:ss" class="form-control" />*/
/*                                         <span class="input-group-btn">*/
/*                                             <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>*/
/*                                         </span>*/
/*                                     </div>*/
/* 				  				</div>*/
/* 							</div>*/
/* 							<div class="form-group">*/
/* 								<label class="col-sm-2 control-label" for="input-date-expire"><span data-toggle="tooltip" title="" data-original-title="{{ text_date_expire_help }}">{{ text_date_expire }}</span></label>*/
/* 								<div class="col-sm-3">*/
/* 									<div class="input-group datetime">*/
/*                                         <input type="text" name="date_expire" value="{{ date_expire }}" placeholder="" data-date-format="YYYY-MM-DD HH:mm:ss" class="form-control" />*/
/*                                         <span class="input-group-btn">*/
/*                                             <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>*/
/*                                         </span>*/
/*                                     </div>*/
/*                                     {% if error_date %}*/
/*                                         <div class="text-danger clear clr clearfix">{{ error_date }}</div>*/
/*                                     {% endif %}*/
/* 				  				</div>*/
/* 							</div>*/
/* 							<div class="form-group ">                                */
/*                                 <div class="col-sm-10">*/
/*                                     <ul class="nav nav-tabs" id="language">*/
/*                                     	{% for language in languages %}*/
/* 							                <li><a href="#language{{ language.language_id }}" data-toggle="tab"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /> {{ language.name }}</a></li>*/
/* 						                {% endfor %}*/
/*                                     </ul>*/
/*                                     <div class="tab-content">*/
/*                                     	{% for language in languages %}*/
/* 	                                        <div class="tab-pane" id="language{{ language.language_id }}">*/
/*                                                 <div class="form-group ">*/
/*                                                     <label class="col-sm-2 control-label" for="input-heading-title"><span data-toggle="tooltip" title="" data-original-title="{{ text_heading_title_help }}">{{ text_heading_title }}</span></label>*/
/*                                                     <div class="col-sm-10">*/
/*                                                         <input type="text" name="popup_description[{{ language.language_id }}][heading_title]" value="{{ popup_description[language.language_id].heading_title is defined ? popup_description[language.language_id].heading_title : '' }}" placeholder="" class="form-control" />*/
/*                                                     </div>*/
/*                                                 </div>*/
/*                                                 <div class="form-group ">*/
/*                                                     <label class="col-sm-2 control-label" for="input-description{{ language.language_id }}"><span data-toggle="tooltip" title="" data-original-title="{{ text_content_help }}">{{ text_content }}</span></label>*/
/*     	                                            <div class="col-sm-10">*/
/*                                                         <textarea name="popup_description[{{ language.language_id }}][description]" id="input-description{{ language.language_id }}" data-toggle="summernote" class="form-control">{{ popup_description[language.language_id] is defined ? popup_description[language.language_id].description : '' }}</textarea>*/
/*                                                     </div>*/
/*                                                 </div>*/
/* 	                                        </div>*/
/*                                         {% endfor %}*/
/*                                     </div>*/
/*                                 </div>*/
/*                             </div>*/
/*                             <div class="form-group">*/
/* 				                <label class="col-sm-2 control-label" for="input-content"><span data-toggle="tooltip" title="" data-original-title="{{ text_image_help }}">{{ text_image }}</span></label>*/
/* 				                <div class="col-sm-3"><a href="" id="thumb-image" data-toggle="image" class="img-thumbnail"><img src="{{ thumb }}" alt="" title="" data-placeholder="{{ placeholder }}" /></a>*/
/* 				                  	<input type="hidden" name="image" value="{{ image }}" id="input-image" />*/
/* 				                </div>*/
/* 			              	</div>*/
/* 			              	<div class="form-group ">*/
/*                                 <label class="col-sm-2 control-label">{{ entry_store }}</label>*/
/*                                 <div class="col-sm-10">*/
/*                                     <div class="table-responsive">*/
/*                                         <table id="layouts_table" class="table table-bordered table-hover">*/
/*                                             <thead>*/
/*                                                 <tr>*/
/*                                                     <td style="width: 1px;"><input type="checkbox" onclick="$('input[name*=\'popup_store\']').prop('checked', this.checked);" /></td>*/
/*                                                     <td class="text-left">{{ text_name }}</td>*/
/*                                                     <td class="text-left">{{ text_link }}</td>*/
/*                                                 </tr>*/
/*                                             </thead>*/
/*                                             <tbody>*/
/*                                             	{% if 0 in popup_store_id %}*/
/*                                             		<tr>*/
/*                                             			<td><input type="checkbox" name="popup_store[]" value="0" checked="checked" /></td>*/
/*                                             			<td>{{ text_default }}</td>*/
/*                                             			<td><input type="text" name="popup_link[0]" class="form-control" value="{{ popup_link[0].link is defined ? popup_link[0].link : '' }}" /></td>*/
/*                                             		</tr>*/
/*                                             	{% else %}*/
/*                                             		<tr>*/
/*                                             			<td><input type="checkbox" name="popup_store[]" value="0" /></td>*/
/*                                             			<td>{{ text_default }}</td>*/
/*                                             			<td><input type="text" name="popup_link[0]" class="form-control" value="{{ popup_link[0].link is defined ? popup_link[0].link : '' }}" /></td>*/
/*                                             		</tr>*/
/*                                             	{% endif %}*/
/*                                             	{% for store in stores %}*/
/*                                                 <tr>*/
/*                                                     <td>*/
/*                                                     	{% if store.store_id in popup_store_id %}*/
/*                                                     		<input type="checkbox" name="popup_store[]" value="{{ store.store_id }}" checked="checked" />*/
/*                                                     	{% else %}*/
/*                                                     		<input type="checkbox" name="popup_store[]" value="{{ store.store_id }}" />*/
/*                                                     	{% endif %}*/
/*                                                     </td>*/
/*                                                     <td>{{ store.name }}</td>*/
/*                                                     <td><input type="text" name="popup_link[{{ store.store_id }}]" class="form-control" value="{{ popup_link[store.store_id].link is defined ? popup_link[store.store_id].link : '' }}" /></td>*/
/*                                                 </tr>*/
/*                                                 {% endfor %}*/
/*                                             </tbody>*/
/*                                         </table>*/
/*                                     </div>*/
/*                                 </div>*/
/*                             </div>*/
/* 						</div>*/
/* 					</div>*/
/* 	   			</form>*/
/* 	   		</div>*/
/* 	   	</div>*/
/* 	</div>*/
/* </div>*/
/* <script type="text/javascript" src="view/javascript/summernote/summernote.js"></script>*/
/* <link href="view/javascript/summernote/summernote.css" rel="stylesheet" />*/
/* <script type="text/javascript" src="view/javascript/summernote/opencart.js"></script> */
/* */
/* <script type="text/javascript">*/
/* 	$('.datetime').datetimepicker({*/
/*     	pickDate: true,*/
/*         pickTime: false*/
/*     });*/
/* </script> */
/* <script type="text/javascript"><!--*/
/* 	$('#language a:first').tab('show');*/
/* //--></script>*/
/* {{ footer }}*/
