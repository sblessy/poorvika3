<?php

/* so-mobile/template/mobile/phonepehome.twig */
class __TwigTemplate_01e18ac56450503bb4d289ad547f7124f58d22896957761225e66fd5339e15d6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        echo (isset($context["header"]) ? $context["header"] : null);
        echo "

    
    <div id=\"content\" class=\"";
        // line 5
        echo (isset($context["class"]) ? $context["class"] : null);
        echo "\">
    ";
        // line 6
        echo (isset($context["content_home"]) ? $context["content_home"] : null);
        echo "
   
    </div>
   

";
        // line 11
        echo (isset($context["footer"]) ? $context["footer"] : null);
    }

    public function getTemplateName()
    {
        return "so-mobile/template/mobile/phonepehome.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 11,  32 => 6,  28 => 5,  22 => 2,  19 => 1,);
    }
}
/* */
/* {{ header }}*/
/* */
/*     */
/*     <div id="content" class="{{ class }}">*/
/*     {{ content_home }}*/
/*    */
/*     </div>*/
/*    */
/* */
/* {{ footer }}*/
