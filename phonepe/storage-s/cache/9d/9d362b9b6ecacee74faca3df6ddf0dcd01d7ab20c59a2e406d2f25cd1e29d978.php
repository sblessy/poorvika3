<?php

/* so-destino/template/extension/module/so_deals/default_carousel.twig */
class __TwigTemplate_4518013e56bbf93695f8980cf6d8e75682b062edb2a9d2b6e9dbf59fba24d340 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"";
        echo (isset($context["tag_id"]) ? $context["tag_id"] : null);
        echo "\" class=\"so-deal clearfix ";
        echo (isset($context["class_respl"]) ? $context["class_respl"] : null);
        echo " ";
        if (((isset($context["button_page"]) ? $context["button_page"] : null) == "top")) {
            echo " ";
            echo "button-type1";
            echo " ";
        } else {
            echo " ";
            echo "button-type2";
            echo " ";
        }
        echo " style2\">
\t";
        // line 2
        if ((isset($context["display_feature"]) ? $context["display_feature"] : null)) {
            // line 3
            echo "\t\t<div class=\"product-feature\">
\t\t";
            // line 4
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["product_features"]) ? $context["product_features"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 5
                echo "\t\t\t<div class=\"item product-layout product-grid\">
\t\t\t\t<div class=\"product-thumb transition\">
\t\t\t\t\t<div class=\"image\">
\t\t\t\t\t\t";
                // line 8
                if (($this->getAttribute($context["product"], "special", array()) && (isset($context["display_sale"]) ? $context["display_sale"] : null))) {
                    // line 9
                    echo "\t\t\t\t\t\t\t<span class=\"label label-sale\">";
                    echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "text_sale"), "method");
                    echo "</span>
\t\t\t\t\t\t";
                }
                // line 11
                echo "\t\t\t\t\t\t";
                if (($this->getAttribute($context["product"], "productNew", array()) && (isset($context["display_new"]) ? $context["display_new"] : null))) {
                    // line 12
                    echo "\t\t\t\t\t\t\t<span class=\"label label-new\">";
                    echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "text_new"), "method");
                    echo "</span>
\t\t\t\t\t\t";
                }
                // line 14
                echo "\t\t\t\t\t\t";
                if ((isset($context["product_image"]) ? $context["product_image"] : null)) {
                    // line 15
                    echo "\t\t\t\t\t\t\t<a class=\"img-link\" href=\"";
                    echo $this->getAttribute($context["product"], "href", array());
                    echo "\" target=\"";
                    echo (isset($context["item_link_target"]) ? $context["item_link_target"] : null);
                    echo "\">
\t\t\t\t\t\t\t\t";
                    // line 16
                    if (((isset($context["product_image_num"]) ? $context["product_image_num"] : null) == 2)) {
                        // line 17
                        echo "\t\t\t\t\t\t\t\t\t<img data-sizes=\"auto\" src=\"data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==\" data-src=\"";
                        echo $this->getAttribute($context["product"], "thumb", array());
                        echo "\" class=\"img-thumb1 lazyload\" alt=\"";
                        echo $this->getAttribute($context["product"], "name", array());
                        echo "\">
\t\t\t\t\t\t\t\t\t<img data-sizes=\"auto\" src=\"data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==\" data-src=\"";
                        // line 18
                        echo $this->getAttribute($context["product"], "thumb2", array());
                        echo "\" class=\"img-thumb2 lazyload\" alt=\"";
                        echo $this->getAttribute($context["product"], "name", array());
                        echo "\">
\t\t\t\t\t\t\t\t";
                    } else {
                        // line 20
                        echo "\t\t\t\t\t\t\t\t\t<img data-sizes=\"auto\" src=\"data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==\" data-src=\"";
                        echo $this->getAttribute($context["product"], "thumb", array());
                        echo "\" alt=\"";
                        echo $this->getAttribute($context["product"], "name", array());
                        echo "\" class=\"lazyload\">
\t\t\t\t\t\t\t\t";
                    }
                    // line 22
                    echo "\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t";
                }
                // line 24
                echo "\t\t\t\t\t\t
\t\t\t\t\t\t";
                // line 25
                if (((isset($context["display_wishlist"]) ? $context["display_wishlist"] : null) || (isset($context["display_compare"]) ? $context["display_compare"] : null))) {
                    // line 26
                    echo "\t\t\t\t\t\t\t<div class=\"button-group so-quickview\">
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t";
                    // line 28
                    if ((isset($context["display_wishlist"]) ? $context["display_wishlist"] : null)) {
                        // line 29
                        echo "\t\t\t\t\t\t\t\t\t<button type=\"button\" title=\"";
                        echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "button_wishlist"), "method");
                        echo "\" onclick=\"wishlist.add('";
                        echo $this->getAttribute($context["product"], "product_id", array());
                        echo "');\"><i class=\"fa fa-heart\"></i></button>
\t\t\t\t\t\t\t\t";
                    }
                    // line 31
                    echo "
\t\t\t\t\t\t\t\t";
                    // line 32
                    if ((isset($context["display_compare"]) ? $context["display_compare"] : null)) {
                        // line 33
                        echo "\t\t\t\t\t\t\t\t\t<button type=\"button\" title=\"";
                        echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "button_compare"), "method");
                        echo "\" onclick=\"compare.add('";
                        echo $this->getAttribute($context["product"], "product_id", array());
                        echo "');\"><i class=\"fa fa-exchange\"></i></button>
\t\t\t\t\t\t\t\t";
                    }
                    // line 35
                    echo "\t\t\t\t\t\t\t\t<a class=\"hidden\" data-product='";
                    echo $this->getAttribute($context["product"], "product_id", array());
                    echo "' href=\"";
                    echo $this->getAttribute($context["product"], "href", array());
                    echo "\" target=\"";
                    echo (isset($context["item_link_target"]) ? $context["item_link_target"] : null);
                    echo "\" title=\"";
                    echo $this->getAttribute($context["product"], "nameFull", array());
                    echo " \" ></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
                }
                // line 38
                echo "\t\t\t\t\t\t<div class=\"item-time\">
\t\t\t\t\t\t\t<div class=\"item-timer product_time_";
                // line 39
                echo $this->getAttribute($context["product"], "product_id", array());
                echo "\"></div>
\t\t\t\t\t\t\t<script type=\"text/javascript\">
\t\t\t\t\t\t\t\t//<![CDATA[
\t\t\t\t\t\t\t\tlistdeal";
                // line 42
                echo (isset($context["module"]) ? $context["module"] : null);
                echo ".push('product_time_";
                echo $this->getAttribute($context["product"], "product_id", array());
                echo "|";
                echo $this->getAttribute($context["product"], "specialPriceToDate", array());
                echo "')
\t\t\t\t\t\t\t\t//]]>
\t\t\t\t\t\t\t</script>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"caption\">
\t\t\t\t\t\t";
                // line 49
                if (((isset($context["display_title"]) ? $context["display_title"] : null) == 1)) {
                    // line 50
                    echo "\t\t\t\t\t\t\t<h4><a href=\"";
                    echo $this->getAttribute($context["product"], "href", array());
                    echo "\" target=\"";
                    echo (isset($context["item_link_target"]) ? $context["item_link_target"] : null);
                    echo "\" title=\"";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "\" >";
                    echo $this->getAttribute($context["product"], "name_maxlength", array());
                    echo "</a></h4>
\t\t\t\t\t\t";
                }
                // line 52
                echo "\t\t\t\t\t\t
\t\t\t\t\t\t";
                // line 53
                if ((isset($context["display_description"]) ? $context["display_description"] : null)) {
                    // line 54
                    echo "\t\t\t\t\t\t\t<p>";
                    echo $this->getAttribute($context["product"], "description_maxlength", array());
                    echo "</p>
\t\t\t\t\t\t";
                }
                // line 56
                echo "\t\t\t\t\t\t
\t\t\t\t\t\t";
                // line 57
                if ((isset($context["display_rating"]) ? $context["display_rating"] : null)) {
                    // line 58
                    echo "\t\t\t\t\t\t\t<div class=\"rating\">
\t\t\t\t\t\t\t\t";
                    // line 59
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(range(1, 5));
                    foreach ($context['_seq'] as $context["_key"] => $context["j"]) {
                        // line 60
                        echo "\t\t\t\t\t\t\t\t\t";
                        if (($this->getAttribute($context["product"], "rating", array()) < $context["j"])) {
                            // line 61
                            echo "\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-2x\"></i></span>
\t\t\t\t\t\t\t\t\t";
                        } else {
                            // line 63
                            echo "\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-2x\"></i><i class=\"fa fa-star-o fa-stack-2x\"></i></span>
\t\t\t\t\t\t\t\t\t";
                        }
                        // line 65
                        echo "\t\t\t\t\t\t\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['j'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 66
                    echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
                }
                // line 68
                echo "
\t\t\t\t\t\t";
                // line 69
                if (($this->getAttribute($context["product"], "price", array()) && (isset($context["display_price"]) ? $context["display_price"] : null))) {
                    // line 70
                    echo "\t\t\t\t\t\t\t<p class=\"price\">
\t\t\t\t\t\t\t\t";
                    // line 71
                    if ( !$this->getAttribute($context["product"], "special", array())) {
                        // line 72
                        echo "\t\t\t\t\t\t\t\t\t";
                        echo $this->getAttribute($context["product"], "price", array());
                        echo "
\t\t\t\t\t\t\t\t";
                    } else {
                        // line 74
                        echo "\t\t\t\t\t\t\t\t\t<span class=\"price-new\">";
                        echo $this->getAttribute($context["product"], "special", array());
                        echo "</span>
\t\t\t\t\t\t\t\t\t<span class=\"price-old\">";
                        // line 75
                        echo $this->getAttribute($context["product"], "price", array());
                        echo "</span>
\t\t\t\t\t\t\t\t";
                    }
                    // line 77
                    echo "
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t";
                }
                // line 81
                echo "\t\t\t\t\t\t";
                if ((isset($context["display_addtocart"]) ? $context["display_addtocart"] : null)) {
                    // line 82
                    echo "\t\t\t\t\t\t\t<button type=\"button\" onclick=\"cart.add('";
                    echo $this->getAttribute($context["product"], "product_id", array());
                    echo "');\"> <span class=\"hidden-xs hidden-sm hidden-md\">";
                    echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "button_cart"), "method");
                    echo "</span></button>
\t\t\t\t\t\t";
                }
                // line 84
                echo "\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 88
            echo "\t\t</div>
\t";
        }
        // line 90
        echo "\t<div class=\"products-list  extraslider-inner\" data-effect=\"";
        echo (isset($context["effect"]) ? $context["effect"] : null);
        echo "\">
\t\t";
        // line 91
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["list"]) ? $context["list"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            if (!twig_in_filter((isset($context["product_feature_ids"]) ? $context["product_feature_ids"] : null), $this->getAttribute($context["product"], "product_id", array()))) {
                // line 92
                echo "\t\t\t";
                $context["i"] = ((isset($context["i"]) ? $context["i"] : null) + 1);
                // line 93
                echo "\t\t\t";
                if (((((isset($context["i"]) ? $context["i"] : null) % (isset($context["nb_rows"]) ? $context["nb_rows"] : null)) == 1) || ((isset($context["nb_rows"]) ? $context["nb_rows"] : null) == 1))) {
                    // line 94
                    echo "\t\t\t<div class=\"item product-layout product-grid\">
\t\t\t\t";
                }
                // line 96
                echo "\t\t\t\t<div class=\"product-thumb transition product-item-container\">\t\t\t\t\t
\t\t\t\t\t<div class=\"left-block\">
\t\t\t\t\t\t<div class=\"product-image-container\">
\t\t\t\t\t\t\t<div class=\"image\">
\t\t\t\t\t\t\t\t";
                // line 100
                if ((isset($context["product_image"]) ? $context["product_image"] : null)) {
                    // line 101
                    echo "\t\t\t\t\t\t\t\t\t<a class=\"img-link\" href=\"";
                    echo $this->getAttribute($context["product"], "href", array());
                    echo "\" target=\"";
                    echo (isset($context["item_link_target"]) ? $context["item_link_target"] : null);
                    echo "\">
\t\t\t\t\t\t\t\t\t\t";
                    // line 102
                    if (((isset($context["product_image_num"]) ? $context["product_image_num"] : null) == 2)) {
                        // line 103
                        echo "\t\t\t\t\t\t\t\t\t\t\t<img src=\"";
                        echo $this->getAttribute($context["product"], "thumb", array());
                        echo "\" class=\"img-thumb1 img-responsive\" alt=\"";
                        echo $this->getAttribute($context["product"], "name", array());
                        echo "\">
\t\t\t\t\t\t\t\t\t\t\t<img src=\"";
                        // line 104
                        echo $this->getAttribute($context["product"], "thumb2", array());
                        echo "\" class=\"img-thumb2 img-responsive\" alt=\"";
                        echo $this->getAttribute($context["product"], "name", array());
                        echo "\">
\t\t\t\t\t\t\t\t\t\t";
                    } else {
                        // line 106
                        echo "\t\t\t\t\t\t\t\t\t\t\t<img src=\"";
                        echo $this->getAttribute($context["product"], "thumb", array());
                        echo "\" alt=\"";
                        echo $this->getAttribute($context["product"], "name", array());
                        echo "\" class=\"img-responsive\">
\t\t\t\t\t\t\t\t\t\t";
                    }
                    // line 108
                    echo "\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t";
                }
                // line 109
                echo "\t\t
\t\t\t\t\t\t\t\t<div class=\"button-group so-quickview\">
\t\t\t\t\t\t\t\t

\t\t\t\t\t\t\t\t\t";
                // line 113
                if ((isset($context["display_wishlist"]) ? $context["display_wishlist"] : null)) {
                    // line 114
                    echo "\t\t\t\t\t\t\t\t\t\t<button class=\"btn-button wishlist\" type=\"button\" title=\"";
                    echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "button_wishlist"), "method");
                    echo "\" onclick=\"wishlist.add('";
                    echo $this->getAttribute($context["product"], "product_id", array());
                    echo "');\"><i class=\"fa fa-heart\"></i></button>
\t\t\t\t\t\t\t\t\t";
                }
                // line 116
                echo "
\t\t\t\t\t\t\t\t\t";
                // line 117
                if ((isset($context["display_compare"]) ? $context["display_compare"] : null)) {
                    // line 118
                    echo "\t\t\t\t\t\t\t\t\t\t<button class=\"btn-button compare\" type=\"button\" title=\"";
                    echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "button_compare"), "method");
                    echo "\" onclick=\"compare.add('";
                    echo $this->getAttribute($context["product"], "product_id", array());
                    echo "');\"><i class=\"fa fa-refresh\"></i></button>
\t\t\t\t\t\t\t\t\t";
                }
                // line 120
                echo "\t\t\t\t\t\t\t\t\t<a class=\"hidden\" data-product='";
                echo $this->getAttribute($context["product"], "product_id", array());
                echo "' href=\"";
                echo $this->getAttribute($context["product"], "href", array());
                echo "\" target=\"";
                echo (isset($context["item_link_target"]) ? $context["item_link_target"] : null);
                echo "\" ></a>
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</div>\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"box-label\">
\t\t\t\t\t\t\t";
                // line 126
                if (($this->getAttribute($context["product"], "special", array()) && (isset($context["display_sale"]) ? $context["display_sale"] : null))) {
                    // line 127
                    echo "\t\t\t\t\t\t\t\t<span class=\"label label-sale\">
\t\t\t\t\t\t\t\t\t";
                    // line 129
                    echo "\t\t\t\t\t\t\t\t\t";
                    echo $this->getAttribute($context["product"], "discount", array());
                    echo "
\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t";
                }
                // line 132
                echo "\t\t\t\t\t\t\t";
                if (($this->getAttribute($context["product"], "productNew", array()) && (isset($context["display_new"]) ? $context["display_new"] : null))) {
                    // line 133
                    echo "\t\t\t\t\t\t\t\t<span class=\"label label-new\">";
                    echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "text_new"), "method");
                    echo "</span>
\t\t\t\t\t\t\t";
                }
                // line 135
                echo "\t\t\t\t\t\t</div>
\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"item-time-w\">
\t\t\t\t\t\t\t<div class=\"item-time\">
\t\t\t\t\t\t\t\t<div class=\"item-timer product_time_";
                // line 140
                echo $this->getAttribute($context["product"], "product_id", array());
                echo "\"></div>
\t\t\t\t\t\t\t\t<script type=\"text/javascript\">
\t\t\t\t\t\t\t\t\t//<![CDATA[
\t\t\t\t\t\t\t\t\tlistdeal";
                // line 143
                echo (isset($context["module"]) ? $context["module"] : null);
                echo ".push('product_time_";
                echo $this->getAttribute($context["product"], "product_id", array());
                echo "|";
                echo $this->getAttribute($context["product"], "specialPriceToDate", array());
                echo "')
\t\t\t\t\t\t\t\t\t//]]>
\t\t\t\t\t\t\t\t</script>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"right-block\">
\t\t\t\t\t\t<div class=\"caption\">
\t\t\t\t\t\t\t";
                // line 151
                if (((isset($context["display_title"]) ? $context["display_title"] : null) == 1)) {
                    // line 152
                    echo "\t\t\t\t\t\t\t\t<h4><a href=\"";
                    echo $this->getAttribute($context["product"], "href", array());
                    echo "\" target=\"";
                    echo (isset($context["item_link_target"]) ? $context["item_link_target"] : null);
                    echo "\" title=\"";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "\">";
                    echo $this->getAttribute($context["product"], "name_maxlength", array());
                    echo "</a></h4>
\t\t\t\t\t\t\t";
                }
                // line 154
                echo "\t\t\t\t\t\t
\t\t\t\t\t\t\t";
                // line 155
                if ((isset($context["display_rating"]) ? $context["display_rating"] : null)) {
                    // line 156
                    echo "\t\t\t\t\t\t\t\t<div class=\"rating\">
\t\t\t\t\t\t\t\t\t";
                    // line 157
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(range(1, 5));
                    foreach ($context['_seq'] as $context["_key"] => $context["j"]) {
                        // line 158
                        echo "\t\t\t\t\t\t\t\t\t\t";
                        if (($this->getAttribute($context["product"], "rating", array()) < $context["j"])) {
                            // line 159
                            echo "\t\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-2x\"></i></span>
\t\t\t\t\t\t\t\t\t\t";
                        } else {
                            // line 161
                            echo "\t\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-2x\"></i><i class=\"fa fa-star-o fa-stack-2x\"></i></span>
\t\t\t\t\t\t\t\t\t\t";
                        }
                        // line 163
                        echo "\t\t\t\t\t\t\t\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['j'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 164
                    echo "\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t";
                }
                // line 166
                echo "
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t";
                // line 168
                if ((isset($context["display_description"]) ? $context["display_description"] : null)) {
                    // line 169
                    echo "\t\t\t\t\t\t\t\t<p>";
                    echo $this->getAttribute($context["product"], "description_maxlength", array());
                    echo "</p>
\t\t\t\t\t\t\t";
                }
                // line 171
                echo "\t\t\t\t\t\t\t";
                if (($this->getAttribute($context["product"], "price", array()) && (isset($context["display_price"]) ? $context["display_price"] : null))) {
                    // line 172
                    echo "\t\t\t\t\t\t\t<p class=\"price\">
\t\t\t\t\t\t\t\t";
                    // line 173
                    if ( !$this->getAttribute($context["product"], "special", array())) {
                        // line 174
                        echo "\t\t\t\t\t\t\t\t\t";
                        echo $this->getAttribute($context["product"], "price", array());
                        echo "
\t\t\t\t\t\t\t\t";
                    } else {
                        // line 176
                        echo "\t\t\t\t\t\t\t\t\t<span class=\"price-new\">";
                        echo $this->getAttribute($context["product"], "special", array());
                        echo "</span>
\t\t\t\t\t\t\t\t\t<span class=\"price-old\">";
                        // line 177
                        echo $this->getAttribute($context["product"], "price", array());
                        echo "</span>
\t\t\t\t\t\t\t\t";
                    }
                    // line 179
                    echo "
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t";
                }
                // line 183
                echo "\t\t\t\t\t\t\t";
                if ((isset($context["display_addtocart"]) ? $context["display_addtocart"] : null)) {
                    // line 184
                    echo "\t\t\t\t\t\t\t\t<button class=\"addToCart\" title=\"";
                    echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "button_cart"), "method");
                    echo "\" type=\"button\" onclick=\"cart.add('";
                    echo $this->getAttribute($context["product"], "product_id", array());
                    echo "');\"> <span>";
                    echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "button_cart"), "method");
                    echo "</span></button>
\t\t\t\t\t\t\t";
                }
                // line 186
                echo "\t\t\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t\t
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t";
                // line 191
                if (((((isset($context["i"]) ? $context["i"] : null) % (isset($context["nb_rows"]) ? $context["nb_rows"] : null)) == 0) || ((isset($context["i"]) ? $context["i"] : null) == (isset($context["count_item"]) ? $context["count_item"] : null)))) {
                    // line 192
                    echo "\t\t\t</div>
\t\t\t";
                }
                // line 194
                echo "\t\t";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 195
        echo "\t</div>
</div>
<script type=\"text/javascript\">
//<![CDATA[
jQuery(document).ready(function (\$) {  ;
(function (element) {
\tvar \$element = \$(element),
\t\t\t\$extraslider = \$('.extraslider-inner', \$element),
\t\t\t\$featureslider = \$('.product-feature', \$element),
\t\t\t_delay = ";
        // line 204
        echo (isset($context["delay"]) ? $context["delay"] : null);
        echo ",
\t\t\t_duration = ";
        // line 205
        echo (isset($context["duration"]) ? $context["duration"] : null);
        echo ",
\t\t\t_effect = '";
        // line 206
        echo (isset($context["effect"]) ? $context["effect"] : null);
        echo "';

\t\$extraslider.on('initialized.owl.carousel2', function () {
\t\tvar \$item_active = \$('.extraslider-inner .owl2-item.active', \$element);
\t\tif (\$item_active.length > 1 && _effect != 'none') {
\t\t\t_getAnimate(\$item_active);
\t\t}
\t\telse {
\t\t\tvar \$item = \$('.extraslider-inner .owl2-item', \$element);
\t\t\t\$item.css({'opacity': 1, 'filter': 'alpha(opacity = 100)'});
\t\t}
\t\t";
        // line 217
        if (((isset($context["button_page"]) ? $context["button_page"] : null) == "top")) {
            // line 218
            echo "\t\t\t\$('.extraslider-inner .owl2-dots', \$element).insertAfter(\$('.extraslider-inner .owl2-prev', \$element));
\t\t\t\$('.extraslider-inner .owl2-controls', \$element).insertBefore(\$extraslider).addClass('extraslider');
\t\t";
        } else {
            // line 221
            echo "\t\t\t\$('.extraslider-inner .owl2-nav', \$element).insertBefore(\$extraslider);
\t\t\t\$('.extraslider-inner .owl2-controls', \$element).insertAfter(\$extraslider).addClass('extraslider');
\t\t";
        }
        // line 224
        echo "\t});

\t\$extraslider.owlCarousel2({
\t\trtl: ";
        // line 227
        echo (isset($context["direction"]) ? $context["direction"] : null);
        echo ",
\t\tmargin: ";
        // line 228
        echo (isset($context["margin"]) ? $context["margin"] : null);
        echo ",
\t\tslideBy: ";
        // line 229
        echo (isset($context["slideBy"]) ? $context["slideBy"] : null);
        echo ",
\t\tautoplay: ";
        // line 230
        echo (isset($context["autoplay"]) ? $context["autoplay"] : null);
        echo ",
\t\tautoplayHoverPause: ";
        // line 231
        echo (isset($context["autoplayHoverPause"]) ? $context["autoplayHoverPause"] : null);
        echo ",
\t\tautoplayTimeout: ";
        // line 232
        echo (isset($context["autoplayTimeout"]) ? $context["autoplayTimeout"] : null);
        echo ",
\t\tautoplaySpeed: ";
        // line 233
        echo (isset($context["autoplaySpeed"]) ? $context["autoplaySpeed"] : null);
        echo ",
\t\tstartPosition: ";
        // line 234
        echo (isset($context["startPosition"]) ? $context["startPosition"] : null);
        echo ",
\t\tmouseDrag: ";
        // line 235
        echo (isset($context["mouseDrag"]) ? $context["mouseDrag"] : null);
        echo ",
\t\ttouchDrag: ";
        // line 236
        echo (isset($context["touchDrag"]) ? $context["touchDrag"] : null);
        echo ",
\t\tautoWidth: false,
\t\tresponsive: {
\t\t\t0: \t{ items: ";
        // line 239
        echo (isset($context["nb_column4"]) ? $context["nb_column4"] : null);
        echo " } ,
\t\t\t480: { items: ";
        // line 240
        echo (isset($context["nb_column3"]) ? $context["nb_column3"] : null);
        echo " },
\t\t\t768: { items: ";
        // line 241
        echo (isset($context["nb_column2"]) ? $context["nb_column2"] : null);
        echo " },
\t\t\t992: { items: ";
        // line 242
        echo (isset($context["nb_column1"]) ? $context["nb_column1"] : null);
        echo " },
\t\t\t1200: {items: ";
        // line 243
        echo (isset($context["nb_column0"]) ? $context["nb_column0"] : null);
        echo " }
\t\t},
\t\tdotClass: 'owl2-dot',
\t\tdotsClass: 'owl2-dots',
\t\tdots: ";
        // line 247
        echo (isset($context["dots"]) ? $context["dots"] : null);
        echo ",
\t\tdotsSpeed: ";
        // line 248
        echo (isset($context["dotsSpeed"]) ? $context["dotsSpeed"] : null);
        echo ",
\t\tnav: ";
        // line 249
        echo (isset($context["navs"]) ? $context["navs"] : null);
        echo ",
\t\tloop: ";
        // line 250
        echo (isset($context["loop"]) ? $context["loop"] : null);
        echo ",
\t\tnavSpeed: ";
        // line 251
        echo (isset($context["navSpeed"]) ? $context["navSpeed"] : null);
        echo ",
\t\tnavText: ['&#171;', '&#187;'],
\t\tnavClass: ['owl2-prev', 'owl2-next']
\t});

\t\$extraslider.on('translated.owl.carousel2', function (e) {
\t\tvar \$item_active = \$('.extraslider-inner .owl2-item.active', \$element);
\t\tvar \$item = \$('.extraslider-inner .owl2-item', \$element);

\t\t_UngetAnimate(\$item);

\t\tif (\$item_active.length > 1 && _effect != 'none') {
\t\t\t_getAnimate(\$item_active);
\t\t} else {
\t\t\t\$item.css({'opacity': 1, 'filter': 'alpha(opacity = 100)'});
\t\t}
\t});
\t/*feature product*/
\t\$featureslider.on('initialized.owl.carousel2', function () {
\t\tvar \$item_active = \$('.product-feature .owl2-item.active', \$element);
\t\tif (\$item_active.length > 1 && _effect != 'none') {
\t\t\t_getAnimate(\$item_active);
\t\t}
\t\telse {
\t\t\tvar \$item = \$('.owl2-item', \$element);
\t\t\t\$item.css({'opacity': 1, 'filter': 'alpha(opacity = 100)'});
\t\t}
\t\t";
        // line 278
        if (((isset($context["button_page"]) ? $context["button_page"] : null) == "top")) {
            // line 279
            echo "\t\t\t\$('.product-feature .owl2-dots', \$element).insertAfter(\$('.product-feature .owl2-prev', \$element));
\t\t\t\$('.product-feature .owl2-controls', \$element).insertBefore(\$featureslider).addClass('featureslider');\t
\t\t";
        } else {
            // line 282
            echo "\t\t\t\$('.product-feature .owl2-nav', \$element).insertBefore(\$featureslider);
\t\t\t\$('.product-feature .owl2-controls', \$element).insertAfter(\$featureslider).addClass('featureslider');;
\t\t";
        }
        // line 285
        echo "\t});

\t\$featureslider.owlCarousel2({
\t\trtl: ";
        // line 288
        echo (isset($context["direction"]) ? $context["direction"] : null);
        echo ",
\t\tmargin: ";
        // line 289
        echo (isset($context["margin"]) ? $context["margin"] : null);
        echo ",
\t\tslideBy: ";
        // line 290
        echo (isset($context["slideBy"]) ? $context["slideBy"] : null);
        echo ",
\t\tautoplay: ";
        // line 291
        echo (isset($context["autoplay"]) ? $context["autoplay"] : null);
        echo ",
\t\tautoplayHoverPause: ";
        // line 292
        echo (isset($context["autoplayHoverPause"]) ? $context["autoplayHoverPause"] : null);
        echo ",
\t\tautoplayTimeout: ";
        // line 293
        echo (isset($context["autoplayTimeout"]) ? $context["autoplayTimeout"] : null);
        echo ",
\t\tautoplaySpeed: ";
        // line 294
        echo (isset($context["autoplaySpeed"]) ? $context["autoplaySpeed"] : null);
        echo ",
\t\tstartPosition: ";
        // line 295
        echo (isset($context["startPosition"]) ? $context["startPosition"] : null);
        echo ",
\t\tmouseDrag: ";
        // line 296
        echo (isset($context["mouseDrag"]) ? $context["mouseDrag"] : null);
        echo ",
\t\ttouchDrag: ";
        // line 297
        echo (isset($context["touchDrag"]) ? $context["touchDrag"] : null);
        echo ",
\t\tautoWidth: false,
\t\tresponsive: {
\t\t\t0: \t{ items: 1 } ,
\t\t\t480: { items: 1 },
\t\t\t768: { items: 1 },
\t\t\t992: { items: 1 },
\t\t\t1200: {items: 1}
\t\t},
\t\tdotClass: 'owl2-dot',
\t\t\tdotsClass: 'owl2-dots',
\t\tdots: ";
        // line 308
        echo (isset($context["dots"]) ? $context["dots"] : null);
        echo ",
\t\tdotsSpeed: ";
        // line 309
        echo (isset($context["dotsSpeed"]) ? $context["dotsSpeed"] : null);
        echo ",
\t\tnav: ";
        // line 310
        echo (isset($context["navs"]) ? $context["navs"] : null);
        echo ",
\t\tloop: ";
        // line 311
        echo (isset($context["loop"]) ? $context["loop"] : null);
        echo ",
\t\tnavSpeed: ";
        // line 312
        echo (isset($context["navSpeed"]) ? $context["navSpeed"] : null);
        echo ",
\t\tnavText: ['&#171;', '&#187;'],
\t\tnavClass: ['owl2-prev', 'owl2-next']
\t});

\t\$featureslider.on('translated.owl.carousel2', function (e) {
\t\tvar \$item_active = \$('.product-feature .owl2-item.active', \$element);
\t\tvar \$item = \$('.product-feature .owl2-item', \$element);

\t\t_UngetAnimate(\$item);

\t\tif (\$item_active.length > 1 && _effect != 'none') {
\t\t\t_getAnimate(\$item_active);
\t\t} else {
\t\t\t\$item.css({'opacity': 1, 'filter': 'alpha(opacity = 100)'});
\t\t}
\t});
\t
\tfunction _getAnimate(\$el) {
\t\tif (_effect == 'none') return;
\t\t\$extraslider.removeClass('extra-animate');
\t\t\$el.each(function (i) {
\t\t\tvar \$_el = \$(this);
\t\t\t\$(this).css({
\t\t\t\t'-webkit-animation': _effect + ' ' + _duration + \"ms ease both\",
\t\t\t\t'-moz-animation': _effect + ' ' + _duration + \"ms ease both\",
\t\t\t\t'-o-animation': _effect + ' ' + _duration + \"ms ease both\",
\t\t\t\t'animation': _effect + ' ' + _duration + \"ms ease both\",
\t\t\t\t'-webkit-animation-delay': +i * _delay + 'ms',
\t\t\t\t'-moz-animation-delay': +i * _delay + 'ms',
\t\t\t\t'-o-animation-delay': +i * _delay + 'ms',
\t\t\t\t'animation-delay': +i * _delay + 'ms',
\t\t\t\t'opacity': 1
\t\t\t}).animate({
\t\t\t\topacity: 1
\t\t\t});

\t\t\tif (i == \$el.size() - 1) {
\t\t\t\t\$extraslider.addClass(\"extra-animate\");
\t\t\t}
\t\t});
\t}

\tfunction _UngetAnimate(\$el) {
\t\t\$el.each(function (i) {
\t\t\t\$(this).css({
\t\t\t\t'animation': '',
\t\t\t\t'-webkit-animation': '',
\t\t\t\t'-moz-animation': '',
\t\t\t\t'-o-animation': '',
\t\t\t\t'opacity': 1
\t\t\t});
\t\t});
\t}
\tdata = new Date(2013, 10, 26, 12, 00, 00);
\tfunction CountDown(date, id) {
\t\tdateNow = new Date();
\t\tamount = date.getTime() - dateNow.getTime();
\t\tif (amount < 0 && \$('#' + id).length) {
\t\t\t\$('.' + id).html(\"Now!\");
\t\t} else {
\t\t\tdays = 0;
\t\t\thours = 0;
\t\t\tmins = 0;
\t\t\tsecs = 0;
\t\t\tout = \"\";
\t\t\tamount = Math.floor(amount / 1000);
\t\t\tdays = Math.floor(amount / 86400);
\t\t\tamount = amount % 86400;
\t\t\thours = Math.floor(amount / 3600);
\t\t\tamount = amount % 3600;
\t\t\tmins = Math.floor(amount / 60);
\t\t\tamount = amount % 60;
\t\t\tsecs = Math.floor(amount);

\t\t\tout += \"<div class='time-item time-day'>\" + \"<div class='num-time'>\" + days + \"</div>\" + \" <div class='name-time'>\" + ((days == 1) ? \"";
        // line 387
        echo (isset($context["text_Day"]) ? $context["text_Day"] : null);
        echo "\" : \"";
        echo (isset($context["text_Days"]) ? $context["text_Days"] : null);
        echo "\") + \"</div>\" + \"</div> \";
\t\t\tout += \"<div class='time-item time-hour'>\" + \"<div class='num-time'>\" + hours + \"</div>\" + \" <div class='name-time'>\" + ((hours == 1) ? \"";
        // line 388
        echo (isset($context["text_Hour"]) ? $context["text_Hour"] : null);
        echo "\" : \"";
        echo (isset($context["text_Hours"]) ? $context["text_Hours"] : null);
        echo "\") + \"</div>\" + \"</div> \";
\t\t\tout += \"<div class='time-item time-min' >\" + \"<div class='num-time'>\" + mins + \"</div>\" + \" <div class='name-time'>\" + ((mins == 1) ? \"";
        // line 389
        echo (isset($context["text_Min"]) ? $context["text_Min"] : null);
        echo "\" : \"";
        echo (isset($context["text_Mins"]) ? $context["text_Mins"] : null);
        echo "\") + \"</div>\" + \"</div> \";
\t\t\tout += \"<div class='time-item time-sec' >\" + \"<div class='num-time'>\" + secs + \"</div>\" + \" <div class='name-time'>\" + ((secs == 1) ? \"";
        // line 390
        echo (isset($context["text_Sec"]) ? $context["text_Sec"] : null);
        echo "\" : \"";
        echo (isset($context["text_Secs"]) ? $context["text_Secs"] : null);
        echo "\") + \"</div>\" + \"</div> \";
\t\t\tout = out.substr(0, out.length - 2);
\t\t\t

\t\t\t\$('.' + id).html(out);

\t\t\tsetTimeout(function () {
\t\t\t\tCountDown(date, id);
\t\t\t}, 1000);
\t\t}
\t}
\tif (listdeal";
        // line 401
        echo (isset($context["module"]) ? $context["module"] : null);
        echo ".length > 0) {
\t\tfor (var i = 0; i < listdeal";
        // line 402
        echo (isset($context["module"]) ? $context["module"] : null);
        echo ".length; i++) {
\t\t\tvar arr = listdeal";
        // line 403
        echo (isset($context["module"]) ? $context["module"] : null);
        echo "[i].split(\"|\");
\t\t\tif (arr[1].length) {
\t\t\t\tvar data = new Date(arr[1]);
\t\t\t\tCountDown(data, arr[0]);
\t\t\t}
\t\t}
\t}
\t})('#";
        // line 410
        echo (isset($context["tag_id"]) ? $context["tag_id"] : null);
        echo "');
});
//]]>
</script>";
    }

    public function getTemplateName()
    {
        return "so-destino/template/extension/module/so_deals/default_carousel.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  938 => 410,  928 => 403,  924 => 402,  920 => 401,  904 => 390,  898 => 389,  892 => 388,  886 => 387,  808 => 312,  804 => 311,  800 => 310,  796 => 309,  792 => 308,  778 => 297,  774 => 296,  770 => 295,  766 => 294,  762 => 293,  758 => 292,  754 => 291,  750 => 290,  746 => 289,  742 => 288,  737 => 285,  732 => 282,  727 => 279,  725 => 278,  695 => 251,  691 => 250,  687 => 249,  683 => 248,  679 => 247,  672 => 243,  668 => 242,  664 => 241,  660 => 240,  656 => 239,  650 => 236,  646 => 235,  642 => 234,  638 => 233,  634 => 232,  630 => 231,  626 => 230,  622 => 229,  618 => 228,  614 => 227,  609 => 224,  604 => 221,  599 => 218,  597 => 217,  583 => 206,  579 => 205,  575 => 204,  564 => 195,  557 => 194,  553 => 192,  551 => 191,  544 => 186,  534 => 184,  531 => 183,  525 => 179,  520 => 177,  515 => 176,  509 => 174,  507 => 173,  504 => 172,  501 => 171,  495 => 169,  493 => 168,  489 => 166,  485 => 164,  479 => 163,  475 => 161,  471 => 159,  468 => 158,  464 => 157,  461 => 156,  459 => 155,  456 => 154,  444 => 152,  442 => 151,  427 => 143,  421 => 140,  414 => 135,  408 => 133,  405 => 132,  398 => 129,  395 => 127,  393 => 126,  379 => 120,  371 => 118,  369 => 117,  366 => 116,  358 => 114,  356 => 113,  350 => 109,  346 => 108,  338 => 106,  331 => 104,  324 => 103,  322 => 102,  315 => 101,  313 => 100,  307 => 96,  303 => 94,  300 => 93,  297 => 92,  292 => 91,  287 => 90,  283 => 88,  274 => 84,  266 => 82,  263 => 81,  257 => 77,  252 => 75,  247 => 74,  241 => 72,  239 => 71,  236 => 70,  234 => 69,  231 => 68,  227 => 66,  221 => 65,  217 => 63,  213 => 61,  210 => 60,  206 => 59,  203 => 58,  201 => 57,  198 => 56,  192 => 54,  190 => 53,  187 => 52,  175 => 50,  173 => 49,  159 => 42,  153 => 39,  150 => 38,  137 => 35,  129 => 33,  127 => 32,  124 => 31,  116 => 29,  114 => 28,  110 => 26,  108 => 25,  105 => 24,  101 => 22,  93 => 20,  86 => 18,  79 => 17,  77 => 16,  70 => 15,  67 => 14,  61 => 12,  58 => 11,  52 => 9,  50 => 8,  45 => 5,  41 => 4,  38 => 3,  36 => 2,  19 => 1,);
    }
}
/* <div id="{{ tag_id }}" class="so-deal clearfix {{ class_respl }} {% if button_page == 'top' %} {{ 'button-type1' }} {% else %} {{ 'button-type2' }} {% endif %} style2">*/
/* 	{% if display_feature %}*/
/* 		<div class="product-feature">*/
/* 		{% for product in product_features %}*/
/* 			<div class="item product-layout product-grid">*/
/* 				<div class="product-thumb transition">*/
/* 					<div class="image">*/
/* 						{% if product.special and display_sale %}*/
/* 							<span class="label label-sale">{{ objlang.get('text_sale') }}</span>*/
/* 						{% endif %}*/
/* 						{% if product.productNew and display_new %}*/
/* 							<span class="label label-new">{{ objlang.get('text_new') }}</span>*/
/* 						{% endif %}*/
/* 						{% if product_image %}*/
/* 							<a class="img-link" href="{{ product.href }}" target="{{ item_link_target }}">*/
/* 								{% if product_image_num ==2 %}*/
/* 									<img data-sizes="auto" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{ product.thumb }}" class="img-thumb1 lazyload" alt="{{ product.name }}">*/
/* 									<img data-sizes="auto" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{ product.thumb2 }}" class="img-thumb2 lazyload" alt="{{ product.name }}">*/
/* 								{% else %}*/
/* 									<img data-sizes="auto" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{ product.thumb }}" alt="{{ product.name }}" class="lazyload">*/
/* 								{% endif %}*/
/* 							</a>*/
/* 						{% endif %}*/
/* 						*/
/* 						{% if display_wishlist or display_compare %}*/
/* 							<div class="button-group so-quickview">*/
/* 								*/
/* 								{% if display_wishlist %}*/
/* 									<button type="button" title="{{ objlang.get('button_wishlist') }}" onclick="wishlist.add('{{ product.product_id }}');"><i class="fa fa-heart"></i></button>*/
/* 								{% endif %}*/
/* */
/* 								{% if display_compare %}*/
/* 									<button type="button" title="{{ objlang.get('button_compare') }}" onclick="compare.add('{{ product.product_id }}');"><i class="fa fa-exchange"></i></button>*/
/* 								{% endif %}*/
/* 								<a class="hidden" data-product='{{ product.product_id }}' href="{{ product.href }}" target="{{ item_link_target }}" title="{{ product.nameFull }} " ></a>*/
/* 							</div>*/
/* 						{% endif %}*/
/* 						<div class="item-time">*/
/* 							<div class="item-timer product_time_{{ product.product_id }}"></div>*/
/* 							<script type="text/javascript">*/
/* 								//<![CDATA[*/
/* 								listdeal{{ module }}.push('product_time_{{ product.product_id }}|{{ product.specialPriceToDate }}')*/
/* 								//]]>*/
/* 							</script>*/
/* 						</div>*/
/* 					</div>*/
/* */
/* 					<div class="caption">*/
/* 						{% if display_title == 1 %}*/
/* 							<h4><a href="{{ product.href }}" target="{{ item_link_target }}" title="{{ product.name }}" >{{ product.name_maxlength }}</a></h4>*/
/* 						{% endif %}*/
/* 						*/
/* 						{% if display_description %}*/
/* 							<p>{{ product.description_maxlength|raw }}</p>*/
/* 						{% endif %}*/
/* 						*/
/* 						{% if display_rating %}*/
/* 							<div class="rating">*/
/* 								{% for j in 1..5 %}*/
/* 									{% if product.rating < j %}*/
/* 										<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>*/
/* 									{% else %}*/
/* 										<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>*/
/* 									{% endif %}*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						{% endif %}*/
/* */
/* 						{% if product.price and display_price %}*/
/* 							<p class="price">*/
/* 								{% if not product.special %}*/
/* 									{{ product.price }}*/
/* 								{% else %}*/
/* 									<span class="price-new">{{ product.special }}</span>*/
/* 									<span class="price-old">{{ product.price }}</span>*/
/* 								{% endif %}*/
/* */
/* 								*/
/* 							</p>*/
/* 						{% endif %}*/
/* 						{% if display_addtocart %}*/
/* 							<button type="button" onclick="cart.add('{{ product.product_id }}');"> <span class="hidden-xs hidden-sm hidden-md">{{ objlang.get('button_cart') }}</span></button>*/
/* 						{% endif %}*/
/* 					</div>*/
/* 				</div>*/
/* 			</div>*/
/* 		{% endfor %}*/
/* 		</div>*/
/* 	{% endif %}*/
/* 	<div class="products-list  extraslider-inner" data-effect="{{ effect }}">*/
/* 		{% for product in list if product_feature_ids not in product.product_id %}*/
/* 			{% set i = i + 1 %}*/
/* 			{% if i % nb_rows == 1 or nb_rows == 1 %}*/
/* 			<div class="item product-layout product-grid">*/
/* 				{% endif %}*/
/* 				<div class="product-thumb transition product-item-container">					*/
/* 					<div class="left-block">*/
/* 						<div class="product-image-container">*/
/* 							<div class="image">*/
/* 								{% if product_image %}*/
/* 									<a class="img-link" href="{{ product.href }}" target="{{ item_link_target }}">*/
/* 										{% if product_image_num ==2 %}*/
/* 											<img src="{{ product.thumb }}" class="img-thumb1 img-responsive" alt="{{ product.name }}">*/
/* 											<img src="{{ product.thumb2 }}" class="img-thumb2 img-responsive" alt="{{ product.name }}">*/
/* 										{% else %}*/
/* 											<img src="{{ product.thumb }}" alt="{{ product.name }}" class="img-responsive">*/
/* 										{% endif %}*/
/* 									</a>*/
/* 								{% endif %}		*/
/* 								<div class="button-group so-quickview">*/
/* 								*/
/* */
/* 									{% if display_wishlist %}*/
/* 										<button class="btn-button wishlist" type="button" title="{{ objlang.get('button_wishlist') }}" onclick="wishlist.add('{{ product.product_id }}');"><i class="fa fa-heart"></i></button>*/
/* 									{% endif %}*/
/* */
/* 									{% if display_compare %}*/
/* 										<button class="btn-button compare" type="button" title="{{ objlang.get('button_compare') }}" onclick="compare.add('{{ product.product_id }}');"><i class="fa fa-refresh"></i></button>*/
/* 									{% endif %}*/
/* 									<a class="hidden" data-product='{{ product.product_id }}' href="{{ product.href }}" target="{{ item_link_target }}" ></a>*/
/* 								*/
/* 								</div>					*/
/* 							</div>*/
/* 						</div>*/
/* 						<div class="box-label">*/
/* 							{% if product.special and display_sale %}*/
/* 								<span class="label label-sale">*/
/* 									{# {{ objlang.get('text_sale') }} #}*/
/* 									{{ product.discount}}*/
/* 								</span>*/
/* 							{% endif %}*/
/* 							{% if product.productNew and display_new %}*/
/* 								<span class="label label-new">{{ objlang.get('text_new') }}</span>*/
/* 							{% endif %}*/
/* 						</div>*/
/* 						*/
/* 						*/
/* 						<div class="item-time-w">*/
/* 							<div class="item-time">*/
/* 								<div class="item-timer product_time_{{ product.product_id }}"></div>*/
/* 								<script type="text/javascript">*/
/* 									//<![CDATA[*/
/* 									listdeal{{ module }}.push('product_time_{{ product.product_id }}|{{ product.specialPriceToDate }}')*/
/* 									//]]>*/
/* 								</script>*/
/* 							</div>*/
/* 						</div>*/
/* 					</div>*/
/* 					<div class="right-block">*/
/* 						<div class="caption">*/
/* 							{% if display_title == 1 %}*/
/* 								<h4><a href="{{ product.href }}" target="{{ item_link_target }}" title="{{ product.name }}">{{ product.name_maxlength }}</a></h4>*/
/* 							{% endif %}*/
/* 						*/
/* 							{% if display_rating %}*/
/* 								<div class="rating">*/
/* 									{% for j in 1..5 %}*/
/* 										{% if product.rating < j %}*/
/* 											<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>*/
/* 										{% else %}*/
/* 											<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>*/
/* 										{% endif %}*/
/* 									{% endfor %}*/
/* 								</div>*/
/* 							{% endif %}*/
/* */
/* 							*/
/* 							{% if display_description %}*/
/* 								<p>{{ product.description_maxlength|raw }}</p>*/
/* 							{% endif %}*/
/* 							{% if product.price and display_price %}*/
/* 							<p class="price">*/
/* 								{% if not product.special %}*/
/* 									{{ product.price }}*/
/* 								{% else %}*/
/* 									<span class="price-new">{{ product.special }}</span>*/
/* 									<span class="price-old">{{ product.price }}</span>*/
/* 								{% endif %}*/
/* */
/* 								*/
/* 								</p>*/
/* 							{% endif %}*/
/* 							{% if display_addtocart %}*/
/* 								<button class="addToCart" title="{{ objlang.get('button_cart') }}" type="button" onclick="cart.add('{{ product.product_id }}');"> <span>{{ objlang.get('button_cart') }}</span></button>*/
/* 							{% endif %}*/
/* 							*/
/* 						</div>*/
/* 						*/
/* 					</div>*/
/* 				</div>*/
/* 				{% if i % nb_rows == 0 or i == count_item %}*/
/* 			</div>*/
/* 			{% endif %}*/
/* 		{% endfor %}*/
/* 	</div>*/
/* </div>*/
/* <script type="text/javascript">*/
/* //<![CDATA[*/
/* jQuery(document).ready(function ($) {  ;*/
/* (function (element) {*/
/* 	var $element = $(element),*/
/* 			$extraslider = $('.extraslider-inner', $element),*/
/* 			$featureslider = $('.product-feature', $element),*/
/* 			_delay = {{ delay }},*/
/* 			_duration = {{ duration }},*/
/* 			_effect = '{{ effect }}';*/
/* */
/* 	$extraslider.on('initialized.owl.carousel2', function () {*/
/* 		var $item_active = $('.extraslider-inner .owl2-item.active', $element);*/
/* 		if ($item_active.length > 1 && _effect != 'none') {*/
/* 			_getAnimate($item_active);*/
/* 		}*/
/* 		else {*/
/* 			var $item = $('.extraslider-inner .owl2-item', $element);*/
/* 			$item.css({'opacity': 1, 'filter': 'alpha(opacity = 100)'});*/
/* 		}*/
/* 		{% if button_page == "top" %}*/
/* 			$('.extraslider-inner .owl2-dots', $element).insertAfter($('.extraslider-inner .owl2-prev', $element));*/
/* 			$('.extraslider-inner .owl2-controls', $element).insertBefore($extraslider).addClass('extraslider');*/
/* 		{% else %}*/
/* 			$('.extraslider-inner .owl2-nav', $element).insertBefore($extraslider);*/
/* 			$('.extraslider-inner .owl2-controls', $element).insertAfter($extraslider).addClass('extraslider');*/
/* 		{% endif %}*/
/* 	});*/
/* */
/* 	$extraslider.owlCarousel2({*/
/* 		rtl: {{ direction }},*/
/* 		margin: {{ margin }},*/
/* 		slideBy: {{ slideBy }},*/
/* 		autoplay: {{ autoplay }},*/
/* 		autoplayHoverPause: {{ autoplayHoverPause }},*/
/* 		autoplayTimeout: {{ autoplayTimeout }},*/
/* 		autoplaySpeed: {{ autoplaySpeed }},*/
/* 		startPosition: {{ startPosition }},*/
/* 		mouseDrag: {{ mouseDrag }},*/
/* 		touchDrag: {{ touchDrag }},*/
/* 		autoWidth: false,*/
/* 		responsive: {*/
/* 			0: 	{ items: {{ nb_column4 }} } ,*/
/* 			480: { items: {{ nb_column3 }} },*/
/* 			768: { items: {{ nb_column2 }} },*/
/* 			992: { items: {{ nb_column1 }} },*/
/* 			1200: {items: {{ nb_column0 }} }*/
/* 		},*/
/* 		dotClass: 'owl2-dot',*/
/* 		dotsClass: 'owl2-dots',*/
/* 		dots: {{ dots }},*/
/* 		dotsSpeed: {{ dotsSpeed }},*/
/* 		nav: {{ navs }},*/
/* 		loop: {{ loop }},*/
/* 		navSpeed: {{ navSpeed }},*/
/* 		navText: ['&#171;', '&#187;'],*/
/* 		navClass: ['owl2-prev', 'owl2-next']*/
/* 	});*/
/* */
/* 	$extraslider.on('translated.owl.carousel2', function (e) {*/
/* 		var $item_active = $('.extraslider-inner .owl2-item.active', $element);*/
/* 		var $item = $('.extraslider-inner .owl2-item', $element);*/
/* */
/* 		_UngetAnimate($item);*/
/* */
/* 		if ($item_active.length > 1 && _effect != 'none') {*/
/* 			_getAnimate($item_active);*/
/* 		} else {*/
/* 			$item.css({'opacity': 1, 'filter': 'alpha(opacity = 100)'});*/
/* 		}*/
/* 	});*/
/* 	/*feature product*//* */
/* 	$featureslider.on('initialized.owl.carousel2', function () {*/
/* 		var $item_active = $('.product-feature .owl2-item.active', $element);*/
/* 		if ($item_active.length > 1 && _effect != 'none') {*/
/* 			_getAnimate($item_active);*/
/* 		}*/
/* 		else {*/
/* 			var $item = $('.owl2-item', $element);*/
/* 			$item.css({'opacity': 1, 'filter': 'alpha(opacity = 100)'});*/
/* 		}*/
/* 		{% if button_page == "top" %}*/
/* 			$('.product-feature .owl2-dots', $element).insertAfter($('.product-feature .owl2-prev', $element));*/
/* 			$('.product-feature .owl2-controls', $element).insertBefore($featureslider).addClass('featureslider');	*/
/* 		{% else %}*/
/* 			$('.product-feature .owl2-nav', $element).insertBefore($featureslider);*/
/* 			$('.product-feature .owl2-controls', $element).insertAfter($featureslider).addClass('featureslider');;*/
/* 		{% endif %}*/
/* 	});*/
/* */
/* 	$featureslider.owlCarousel2({*/
/* 		rtl: {{ direction }},*/
/* 		margin: {{ margin }},*/
/* 		slideBy: {{ slideBy }},*/
/* 		autoplay: {{ autoplay }},*/
/* 		autoplayHoverPause: {{ autoplayHoverPause }},*/
/* 		autoplayTimeout: {{ autoplayTimeout }},*/
/* 		autoplaySpeed: {{ autoplaySpeed }},*/
/* 		startPosition: {{ startPosition }},*/
/* 		mouseDrag: {{ mouseDrag }},*/
/* 		touchDrag: {{ touchDrag }},*/
/* 		autoWidth: false,*/
/* 		responsive: {*/
/* 			0: 	{ items: 1 } ,*/
/* 			480: { items: 1 },*/
/* 			768: { items: 1 },*/
/* 			992: { items: 1 },*/
/* 			1200: {items: 1}*/
/* 		},*/
/* 		dotClass: 'owl2-dot',*/
/* 			dotsClass: 'owl2-dots',*/
/* 		dots: {{ dots }},*/
/* 		dotsSpeed: {{ dotsSpeed }},*/
/* 		nav: {{ navs }},*/
/* 		loop: {{ loop }},*/
/* 		navSpeed: {{ navSpeed }},*/
/* 		navText: ['&#171;', '&#187;'],*/
/* 		navClass: ['owl2-prev', 'owl2-next']*/
/* 	});*/
/* */
/* 	$featureslider.on('translated.owl.carousel2', function (e) {*/
/* 		var $item_active = $('.product-feature .owl2-item.active', $element);*/
/* 		var $item = $('.product-feature .owl2-item', $element);*/
/* */
/* 		_UngetAnimate($item);*/
/* */
/* 		if ($item_active.length > 1 && _effect != 'none') {*/
/* 			_getAnimate($item_active);*/
/* 		} else {*/
/* 			$item.css({'opacity': 1, 'filter': 'alpha(opacity = 100)'});*/
/* 		}*/
/* 	});*/
/* 	*/
/* 	function _getAnimate($el) {*/
/* 		if (_effect == 'none') return;*/
/* 		$extraslider.removeClass('extra-animate');*/
/* 		$el.each(function (i) {*/
/* 			var $_el = $(this);*/
/* 			$(this).css({*/
/* 				'-webkit-animation': _effect + ' ' + _duration + "ms ease both",*/
/* 				'-moz-animation': _effect + ' ' + _duration + "ms ease both",*/
/* 				'-o-animation': _effect + ' ' + _duration + "ms ease both",*/
/* 				'animation': _effect + ' ' + _duration + "ms ease both",*/
/* 				'-webkit-animation-delay': +i * _delay + 'ms',*/
/* 				'-moz-animation-delay': +i * _delay + 'ms',*/
/* 				'-o-animation-delay': +i * _delay + 'ms',*/
/* 				'animation-delay': +i * _delay + 'ms',*/
/* 				'opacity': 1*/
/* 			}).animate({*/
/* 				opacity: 1*/
/* 			});*/
/* */
/* 			if (i == $el.size() - 1) {*/
/* 				$extraslider.addClass("extra-animate");*/
/* 			}*/
/* 		});*/
/* 	}*/
/* */
/* 	function _UngetAnimate($el) {*/
/* 		$el.each(function (i) {*/
/* 			$(this).css({*/
/* 				'animation': '',*/
/* 				'-webkit-animation': '',*/
/* 				'-moz-animation': '',*/
/* 				'-o-animation': '',*/
/* 				'opacity': 1*/
/* 			});*/
/* 		});*/
/* 	}*/
/* 	data = new Date(2013, 10, 26, 12, 00, 00);*/
/* 	function CountDown(date, id) {*/
/* 		dateNow = new Date();*/
/* 		amount = date.getTime() - dateNow.getTime();*/
/* 		if (amount < 0 && $('#' + id).length) {*/
/* 			$('.' + id).html("Now!");*/
/* 		} else {*/
/* 			days = 0;*/
/* 			hours = 0;*/
/* 			mins = 0;*/
/* 			secs = 0;*/
/* 			out = "";*/
/* 			amount = Math.floor(amount / 1000);*/
/* 			days = Math.floor(amount / 86400);*/
/* 			amount = amount % 86400;*/
/* 			hours = Math.floor(amount / 3600);*/
/* 			amount = amount % 3600;*/
/* 			mins = Math.floor(amount / 60);*/
/* 			amount = amount % 60;*/
/* 			secs = Math.floor(amount);*/
/* */
/* 			out += "<div class='time-item time-day'>" + "<div class='num-time'>" + days + "</div>" + " <div class='name-time'>" + ((days == 1) ? "{{text_Day}}" : "{{text_Days}}") + "</div>" + "</div> ";*/
/* 			out += "<div class='time-item time-hour'>" + "<div class='num-time'>" + hours + "</div>" + " <div class='name-time'>" + ((hours == 1) ? "{{text_Hour}}" : "{{text_Hours}}") + "</div>" + "</div> ";*/
/* 			out += "<div class='time-item time-min' >" + "<div class='num-time'>" + mins + "</div>" + " <div class='name-time'>" + ((mins == 1) ? "{{text_Min}}" : "{{text_Mins}}") + "</div>" + "</div> ";*/
/* 			out += "<div class='time-item time-sec' >" + "<div class='num-time'>" + secs + "</div>" + " <div class='name-time'>" + ((secs == 1) ? "{{text_Sec}}" : "{{text_Secs}}") + "</div>" + "</div> ";*/
/* 			out = out.substr(0, out.length - 2);*/
/* 			*/
/* */
/* 			$('.' + id).html(out);*/
/* */
/* 			setTimeout(function () {*/
/* 				CountDown(date, id);*/
/* 			}, 1000);*/
/* 		}*/
/* 	}*/
/* 	if (listdeal{{ module }}.length > 0) {*/
/* 		for (var i = 0; i < listdeal{{ module }}.length; i++) {*/
/* 			var arr = listdeal{{ module }}[i].split("|");*/
/* 			if (arr[1].length) {*/
/* 				var data = new Date(arr[1]);*/
/* 				CountDown(data, arr[0]);*/
/* 			}*/
/* 		}*/
/* 	}*/
/* 	})('#{{ tag_id }}');*/
/* });*/
/* //]]>*/
/* </script>*/
