<?php

/* so-destino/template/soconfig/countdown.twig */
class __TwigTemplate_624781cdea75c29b0213bedab23112202a365530c315ba8bce9c7b3e690dfe3a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 9
        echo "
";
        // line 10
        $context["full_date"] = twig_split_filter($this->env, (isset($context["special_end_date"]) ? $context["special_end_date"] : null), "-");
        // line 11
        $context["year_end"] = $this->getAttribute((isset($context["full_date"]) ? $context["full_date"] : null), 0, array(), "array");
        // line 12
        $context["month_end"] = $this->getAttribute((isset($context["full_date"]) ? $context["full_date"] : null), 1, array(), "array");
        // line 13
        $context["day_end"] = $this->getAttribute((isset($context["full_date"]) ? $context["full_date"] : null), 2, array(), "array");
        // line 14
        echo "
";
        // line 15
        if ((((isset($context["day_end"]) ? $context["day_end"] : null) && (isset($context["year_end"]) ? $context["year_end"] : null)) && (isset($context["month_end"]) ? $context["month_end"] : null))) {
            // line 16
            echo "\t <div class=\"countdown_box hidden-xs\">
\t\t<div class=\"countdown_inner\">
\t\t\t <script type=\"text/javascript\">
\t\t\t \$(function () {
\t\t\t\tvar austDay = new Date(";
            // line 20
            echo (isset($context["year_end"]) ? $context["year_end"] : null);
            echo ", ";
            echo (isset($context["month_end"]) ? $context["month_end"] : null);
            echo "-1 , ";
            echo (isset($context["day_end"]) ? $context["day_end"] : null);
            echo ");
\t\t\t\t\$('.defaultCountdown-";
            // line 21
            echo $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "product_id", array());
            echo "').countdown(austDay, function(event) {
\t\t\t\t\tvar \$this = \$(this).html(event.strftime(''
\t\t\t\t\t   + '<div class=\"time-item time-day\"><div class=\"num-time\">%D</div><div class=\"name-time\">";
            // line 23
            echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "text_labelDay"), "method");
            echo " </div></div>'
\t\t\t\t\t   + '<div class=\"time-item time-hour\"><div class=\"num-time\">%H</div><div class=\"name-time\">";
            // line 24
            echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "text_labelHour"), "method");
            echo "</div></div>'
\t\t\t\t\t   + '<div class=\"time-item time-min\"><div class=\"num-time\">%M</div><div class=\"name-time\">";
            // line 25
            echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "text_labelMin"), "method");
            echo " </div></div>'
\t\t\t\t\t   + '<div class=\"time-item time-sec\"><div class=\"num-time\">%S</div><div class=\"name-time\">";
            // line 26
            echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "text_labelSec"), "method");
            echo "</div></div>'));
\t\t\t\t});
\t\t\t\t
\t\t\t});
\t\t\t</script>
\t\t\t<div class=\"defaultCountdown-";
            // line 31
            echo $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "product_id", array());
            echo "\"></div>
\t\t</div>
\t</div>
";
        }
    }

    public function getTemplateName()
    {
        return "so-destino/template/soconfig/countdown.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 31,  66 => 26,  62 => 25,  58 => 24,  54 => 23,  49 => 21,  41 => 20,  35 => 16,  33 => 15,  30 => 14,  28 => 13,  26 => 12,  24 => 11,  22 => 10,  19 => 9,);
    }
}
/* {#*/
/* ****************************************************** */
/*  * @package	SO Framework for Opencart 3.x*/
/*  * @author	http://www.opencartworks.com*/
/*  * @license	GNU General Public License*/
/*  * @copyright(C) 2008-2017 opencartworks.com. All rights reserved.*/
/*  *******************************************************/
/* #}*/
/* */
/* {% set full_date 	= special_end_date|split('-') %}*/
/* {% set year_end  	= full_date[0]%}*/
/* {% set month_end 	= full_date[1]%}*/
/* {% set day_end   	= full_date[2]%}*/
/* */
/* {% if day_end and year_end and month_end %}*/
/* 	 <div class="countdown_box hidden-xs">*/
/* 		<div class="countdown_inner">*/
/* 			 <script type="text/javascript">*/
/* 			 $(function () {*/
/* 				var austDay = new Date({{year_end}}, {{month_end}}-1 , {{day_end}});*/
/* 				$('.defaultCountdown-{{product.product_id}}').countdown(austDay, function(event) {*/
/* 					var $this = $(this).html(event.strftime(''*/
/* 					   + '<div class="time-item time-day"><div class="num-time">%D</div><div class="name-time">{{objlang.get('text_labelDay')}} </div></div>'*/
/* 					   + '<div class="time-item time-hour"><div class="num-time">%H</div><div class="name-time">{{objlang.get('text_labelHour')}}</div></div>'*/
/* 					   + '<div class="time-item time-min"><div class="num-time">%M</div><div class="name-time">{{objlang.get('text_labelMin')}} </div></div>'*/
/* 					   + '<div class="time-item time-sec"><div class="num-time">%S</div><div class="name-time">{{objlang.get('text_labelSec')}}</div></div>'));*/
/* 				});*/
/* 				*/
/* 			});*/
/* 			</script>*/
/* 			<div class="defaultCountdown-{{product.product_id}}"></div>*/
/* 		</div>*/
/* 	</div>*/
/* {% endif %}*/
/* */
