<?php

/* so-destino/template/header/header2.twig */
class __TwigTemplate_320e2e1ce9d6f08c443ee6dcc02a66d0f5c13d317ddde18562338e9e8fce053f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $context["hidden_headercenter"] = ((($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "toppanel_type"), "method") == "2")) ? ("hidden-compact") : (""));
        // line 3
        $context["hidden_headerbottom"] = ((($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "toppanel_type"), "method") == "1")) ? ("hidden-compact") : (""));
        // line 4
        echo "
<header id=\"header\" class=\" variant typeheader-";
        // line 5
        echo (((isset($context["typeheader"]) ? $context["typeheader"] : null)) ? ((isset($context["typeheader"]) ? $context["typeheader"] : null)) : ("1"));
        echo "\">
\t<!-- HEADER TOP -->
\t<div class=\"header-top compact-hidden\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"header-top-left  col-lg-6 col-md-5 col-sm-6 col-xs-6\">
\t\t\t\t\t<!-- LANGUAGE CURENCY -->
\t\t\t\t\t";
        // line 12
        if ($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "lang_status"), "method")) {
            // line 13
            echo "\t\t\t\t\t<ul class=\"top-link list-inline lang-curr\">
\t\t\t\t\t\t";
            // line 14
            if ((isset($context["currency"]) ? $context["currency"] : null)) {
                echo "<li class=\"currency\"> ";
                echo (isset($context["currency"]) ? $context["currency"] : null);
                echo "  </li> ";
            }
            // line 15
            echo "\t\t\t\t\t\t";
            if ((isset($context["language"]) ? $context["language"] : null)) {
                echo " <li class=\"language\">";
                echo (isset($context["language"]) ? $context["language"] : null);
                echo " </li>\t";
            }
            echo "\t\t\t
\t\t\t\t\t</ul>\t\t\t\t
\t\t\t\t\t";
        }
        // line 17
        echo " 
\t\t\t\t</div>
\t\t\t\t<div class=\"header-top-right collapsed-block col-lg-6 col-md-7 col-sm-6 col-xs-6\">
\t\t\t\t\t
\t\t\t\t\t<ul class=\"top-link list-inline\">
\t\t\t\t\t\t";
        // line 22
        if ($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "welcome_message_status"), "method")) {
            // line 23
            echo "\t\t\t\t\t\t<li class=\"hidden-sm hidden-xs welcome-msg\">
\t\t\t\t\t\t\t";
            // line 24
            if ( !twig_test_empty($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "welcome_message"), "method"))) {
                // line 25
                echo "\t\t\t\t\t\t\t\t";
                echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "decode_entities", array(0 => $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "welcome_message"), "method")), "method");
                echo "
\t\t\t\t\t\t\t";
            }
            // line 26
            echo " 
\t\t\t\t\t\t</li>
\t\t\t\t\t";
        }
        // line 29
        echo "\t\t\t\t\t\t";
        if ((isset($context["logged"]) ? $context["logged"] : null)) {
            echo " 
\t\t\t\t\t\t\t<li><a href=\"";
            // line 30
            echo (isset($context["logout"]) ? $context["logout"] : null);
            echo "\">";
            echo (isset($context["text_logout"]) ? $context["text_logout"] : null);
            echo "</a></li>\t\t\t\t\t\t\t
\t\t\t\t\t\t\t ";
        } else {
            // line 32
            echo "
\t\t\t\t\t\t\t<li><a href=\"";
            // line 33
            echo (isset($context["register"]) ? $context["register"] : null);
            echo "\">";
            echo (isset($context["text_register"]) ? $context["text_register"] : null);
            echo "</a></li>
\t\t\t\t            <li><a href=\"";
            // line 34
            echo (isset($context["login"]) ? $context["login"] : null);
            echo "\">";
            echo (isset($context["text_login"]) ? $context["text_login"] : null);
            echo "</a></li>\t\t\t\t\t\t
\t\t\t\t\t\t";
        }
        // line 36
        echo "
\t\t\t\t\t\t<!-- WISHLIST  -->
\t\t\t\t\t\t";
        // line 38
        if ((isset($context["wishlist_status"]) ? $context["wishlist_status"] : null)) {
            // line 39
            echo "\t\t\t\t\t\t\t<li class=\"wishlist\"><a id=\"wishlist-total\" class=\"btn-link\" href=\"";
            echo (isset($context["wishlist"]) ? $context["wishlist"] : null);
            echo "\"  title=\"";
            echo (isset($context["text_wishlist"]) ? $context["text_wishlist"] : null);
            echo "\">";
            echo (isset($context["text_wishlist"]) ? $context["text_wishlist"] : null);
            echo "</a></li>
\t\t\t\t\t\t";
        }
        // line 40
        echo "\t
\t\t\t\t\t\t<!-- checkout -->
\t\t\t\t\t\t";
        // line 42
        if ($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "checkout_status"), "method")) {
            // line 43
            echo "\t\t\t\t\t\t\t<li class=\"checkout\"><a href=\"";
            echo (isset($context["checkout"]) ? $context["checkout"] : null);
            echo " \" class=\"btn-link\" title=\"";
            echo (isset($context["text_checkout"]) ? $context["text_checkout"] : null);
            echo " \"><span >";
            echo (isset($context["text_checkout"]) ? $context["text_checkout"] : null);
            echo " </span></a></li>
\t\t\t\t\t\t";
        }
        // line 44
        echo " \t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t</ul>
\t\t\t\t
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
\t
\t<!-- HEADER CENTER -->
\t<div class=\"header-center ";
        // line 54
        echo (isset($context["hidden_headercenter"]) ? $context["hidden_headercenter"] : null);
        echo "\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<!-- LOGO -->
\t\t\t\t<div class=\"header-bottom-left menu-vertical compact-hidden col-md-3 col-sm-4 col-xs-6\">
\t\t\t\t\t\t ";
        // line 59
        echo (isset($context["content_menu2"]) ? $context["content_menu2"] : null);
        echo "
\t\t\t\t\t</div>
\t\t\t\t<div class=\"navbar-logo col-lg-3 col-md-3 col-sm-12 col-xs-12\">
\t\t\t\t\t<div class=\"logo\">
\t\t\t\t   \t\t";
        // line 63
        echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_logo", array(), "method");
        echo "
\t\t\t\t   \t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"header-center-right col-lg-9 col-md-9 col-sm-12 col-xs-12\">\t
\t\t\t\t\t<div class=\"search-header-w\">
\t\t\t\t\t\t<div class=\"icon-search hidden-lg hidden-md hidden-sm\"><i class=\"fa fa-search\"></i></div>\t\t\t\t
\t\t\t\t\t\t";
        // line 69
        echo (isset($context["search_block"]) ? $context["search_block"] : null);
        echo "
\t\t\t\t\t</div>
\t\t\t\t\t";
        // line 71
        if (($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "phone_status"), "method") && $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "contact_number"), "method"))) {
            // line 72
            echo "\t\t\t\t\t<div class=\"telephone hidden-xs hidden-sm hidden-md\" >
\t\t\t\t\t\t";
            // line 73
            echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "decode_entities", array(0 => $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "contact_number"), "method")), "method");
            echo "
\t\t\t\t\t</div>
\t\t\t\t\t";
        }
        // line 76
        echo "\t\t\t\t\t
\t\t\t\t\t<div class=\"shopping_cart\">\t\t\t\t\t\t\t
\t\t\t\t\t \t";
        // line 78
        echo (isset($context["cart"]) ? $context["cart"] : null);
        echo "
\t\t\t\t\t</div>
\t\t\t\t
\t\t\t\t</div>

\t\t\t</div>
\t\t</div>
\t</div>
\t

\t<!-- HEADER BOTTOM -->
\t<div class=\"header-bottom ";
        // line 89
        echo (isset($context["hidden_headerbottom"]) ? $context["hidden_headerbottom"] : null);
        echo "\">
\t\t<div class=\"container\">
\t\t\t<div class=\"header-bottom-inner\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t
\t\t\t\t\t
\t\t\t\t\t<!--<div class=\"header-bottom-left menu-vertical compact-hidden col-md-3 col-sm-4 col-xs-6\">
\t\t\t\t\t\t ";
        // line 96
        echo (isset($context["content_menu2"]) ? $context["content_menu2"] : null);
        echo "
\t\t\t\t\t</div>-->
\t\t\t\t\t
\t\t\t\t\t<div class=\"header-bottom-right col-md-9 col-sm-8 col-xs-6\">\t\t\t\t\t
\t\t\t\t\t\t<!-- BOX CONTENT MENU -->
\t\t\t\t\t\t ";
        // line 101
        echo (isset($context["content_menu1"]) ? $context["content_menu1"] : null);
        echo "\t\t\t\t\t\t
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t  
\t</div>
\t\t
</header>";
    }

    public function getTemplateName()
    {
        return "so-destino/template/header/header2.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  231 => 101,  223 => 96,  213 => 89,  199 => 78,  195 => 76,  189 => 73,  186 => 72,  184 => 71,  179 => 69,  170 => 63,  163 => 59,  155 => 54,  143 => 44,  133 => 43,  131 => 42,  127 => 40,  117 => 39,  115 => 38,  111 => 36,  104 => 34,  98 => 33,  95 => 32,  88 => 30,  83 => 29,  78 => 26,  72 => 25,  70 => 24,  67 => 23,  65 => 22,  58 => 17,  47 => 15,  41 => 14,  38 => 13,  36 => 12,  26 => 5,  23 => 4,  21 => 3,  19 => 2,);
    }
}
/* {#=====Get variable : Config Select Block on header=====#}*/
/* {% set hidden_headercenter = soconfig.get_settings('toppanel_type') =='2'? 'hidden-compact' : '' %}*/
/* {% set hidden_headerbottom = soconfig.get_settings('toppanel_type') =='1'? 'hidden-compact' : '' %}*/
/* */
/* <header id="header" class=" variant typeheader-{{ typeheader ? typeheader : '1'}}">*/
/* 	<!-- HEADER TOP -->*/
/* 	<div class="header-top compact-hidden">*/
/* 		<div class="container">*/
/* 			<div class="row">*/
/* 				<div class="header-top-left  col-lg-6 col-md-5 col-sm-6 col-xs-6">*/
/* 					<!-- LANGUAGE CURENCY -->*/
/* 					{% if soconfig.get_settings('lang_status') %}*/
/* 					<ul class="top-link list-inline lang-curr">*/
/* 						{% if currency %}<li class="currency"> {{ currency }}  </li> {% endif %}*/
/* 						{% if language %} <li class="language">{{ language }} </li>	{% endif %}			*/
/* 					</ul>				*/
/* 					{% endif %} */
/* 				</div>*/
/* 				<div class="header-top-right collapsed-block col-lg-6 col-md-7 col-sm-6 col-xs-6">*/
/* 					*/
/* 					<ul class="top-link list-inline">*/
/* 						{% if soconfig.get_settings('welcome_message_status') %}*/
/* 						<li class="hidden-sm hidden-xs welcome-msg">*/
/* 							{% if soconfig.get_settings('welcome_message') is not empty %}*/
/* 								{{ soconfig.decode_entities( soconfig.get_settings('welcome_message') ) }}*/
/* 							{% endif %} */
/* 						</li>*/
/* 					{% endif %}*/
/* 						{% if logged %} */
/* 							<li><a href="{{ logout }}">{{ text_logout }}</a></li>							*/
/* 							 {% else %}*/
/* */
/* 							<li><a href="{{ register }}">{{ text_register }}</a></li>*/
/* 				            <li><a href="{{ login }}">{{ text_login }}</a></li>						*/
/* 						{% endif %}*/
/* */
/* 						<!-- WISHLIST  -->*/
/* 						{% if wishlist_status %}*/
/* 							<li class="wishlist"><a id="wishlist-total" class="btn-link" href="{{ wishlist }}"  title="{{ text_wishlist }}">{{ text_wishlist }}</a></li>*/
/* 						{% endif %}	*/
/* 						<!-- checkout -->*/
/* 						{% if soconfig.get_settings('checkout_status') %}*/
/* 							<li class="checkout"><a href="{{ checkout }} " class="btn-link" title="{{ text_checkout }} "><span >{{ text_checkout }} </span></a></li>*/
/* 						{% endif %} 	*/
/* 																		*/
/* 					</ul>*/
/* 				*/
/* 				</div>*/
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/* 	*/
/* 	<!-- HEADER CENTER -->*/
/* 	<div class="header-center {{hidden_headercenter}}">*/
/* 		<div class="container">*/
/* 			<div class="row">*/
/* 				<!-- LOGO -->*/
/* 				<div class="header-bottom-left menu-vertical compact-hidden col-md-3 col-sm-4 col-xs-6">*/
/* 						 {{ content_menu2 }}*/
/* 					</div>*/
/* 				<div class="navbar-logo col-lg-3 col-md-3 col-sm-12 col-xs-12">*/
/* 					<div class="logo">*/
/* 				   		{{soconfig.get_logo()}}*/
/* 				   	</div>*/
/* 				</div>*/
/* 				<div class="header-center-right col-lg-9 col-md-9 col-sm-12 col-xs-12">	*/
/* 					<div class="search-header-w">*/
/* 						<div class="icon-search hidden-lg hidden-md hidden-sm"><i class="fa fa-search"></i></div>				*/
/* 						{{ search_block }}*/
/* 					</div>*/
/* 					{% if soconfig.get_settings('phone_status') and soconfig.get_settings('contact_number') %}*/
/* 					<div class="telephone hidden-xs hidden-sm hidden-md" >*/
/* 						{{ soconfig.decode_entities( soconfig.get_settings('contact_number') ) }}*/
/* 					</div>*/
/* 					{% endif %}*/
/* 					*/
/* 					<div class="shopping_cart">							*/
/* 					 	{{ cart }}*/
/* 					</div>*/
/* 				*/
/* 				</div>*/
/* */
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/* 	*/
/* */
/* 	<!-- HEADER BOTTOM -->*/
/* 	<div class="header-bottom {{hidden_headerbottom}}">*/
/* 		<div class="container">*/
/* 			<div class="header-bottom-inner">*/
/* 				<div class="row">*/
/* 					*/
/* 					*/
/* 					<!--<div class="header-bottom-left menu-vertical compact-hidden col-md-3 col-sm-4 col-xs-6">*/
/* 						 {{ content_menu2 }}*/
/* 					</div>-->*/
/* 					*/
/* 					<div class="header-bottom-right col-md-9 col-sm-8 col-xs-6">					*/
/* 						<!-- BOX CONTENT MENU -->*/
/* 						 {{ content_menu1 }}						*/
/* 					</div>*/
/* 				</div>*/
/* 			</div>*/
/* 		</div>*/
/* 	  */
/* 	</div>*/
/* 		*/
/* </header>*/
