<?php

/* so-destino/template/extension/module/so_onepagecheckout/checkout/register.twig */
class __TwigTemplate_e030e6ea9024663bd341880129eac328e257d2d3a2de9671fb98797cb18788b5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"checkout-content checkout-register\">
    <fieldset id=\"account\">
        <h2 class=\"secondary-title\"><i class=\"fa fa-user-plus\"></i>";
        // line 3
        echo (isset($context["text_your_details"]) ? $context["text_your_details"] : null);
        echo "</h2>
        <div class=\"payment-new box-inner\">
            <div class=\"form-group customer-group\" style=\"display: ";
        // line 5
        echo (((twig_length_filter($this->env, (isset($context["customer_groups"]) ? $context["customer_groups"] : null)) > 1)) ? ("block") : ("none"));
        echo "\">
                <label class=\"control-label\">";
        // line 6
        echo (isset($context["entry_customer_group"]) ? $context["entry_customer_group"] : null);
        echo "</label>
                ";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["customer_groups"]) ? $context["customer_groups"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["customer_group"]) {
            // line 8
            echo "                    ";
            if (($this->getAttribute($context["customer_group"], "customer_group_id", array()) == (isset($context["customer_group_id"]) ? $context["customer_group_id"] : null))) {
                // line 9
                echo "                        <div class=\"radio\">
                            <label>
                                <input type=\"radio\" name=\"customer_group_id\" value=\"";
                // line 11
                echo $this->getAttribute($context["customer_group"], "customer_group_id", array());
                echo "\" checked=\"checked\"/>
                                ";
                // line 12
                echo $this->getAttribute($context["customer_group"], "name", array());
                echo "
                            </label>
                        </div>
                    ";
            } else {
                // line 16
                echo "                        <div class=\"radio\">
                            <label>
                                <input type=\"radio\" name=\"customer_group_id\" value=\"";
                // line 18
                echo $this->getAttribute($context["customer_group"], "customer_group_id", array());
                echo "\"/>
                                ";
                // line 19
                echo $this->getAttribute($context["customer_group"], "name", array());
                echo "
                            </label>
                        </div>
                    ";
            }
            // line 23
            echo "                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['customer_group'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 24
        echo "            </div>
            <div class=\"form-group input-firstname required\" style=\"width: 49%; float: left;\">
                <input type=\"text\" name=\"firstname\" value=\"";
        // line 26
        echo $this->getAttribute((isset($context["SoUtils"]) ? $context["SoUtils"] : null), "getProperty", array(0 => (isset($context["order_data"]) ? $context["order_data"] : null), 1 => "firstname"), "method");
        echo "\" placeholder=\"";
        echo (isset($context["entry_firstname"]) ? $context["entry_firstname"] : null);
        echo " *\" id=\"input-payment-firstname\" class=\"form-control\"/>
            </div>
            <div class=\"form-group input-lastname required\" style=\"width: 49%; float: right;\">
                <input type=\"text\" name=\"lastname\" value=\"";
        // line 29
        echo $this->getAttribute((isset($context["SoUtils"]) ? $context["SoUtils"] : null), "getProperty", array(0 => (isset($context["order_data"]) ? $context["order_data"] : null), 1 => "lastname"), "method");
        echo "\" placeholder=\"";
        echo (isset($context["entry_lastname"]) ? $context["entry_lastname"] : null);
        echo " *\" id=\"input-payment-lastname\" class=\"form-control\"/>
            </div>
            <div class=\"form-group required\">
                <input type=\"text\" name=\"email\" value=\"";
        // line 32
        echo $this->getAttribute((isset($context["SoUtils"]) ? $context["SoUtils"] : null), "getProperty", array(0 => (isset($context["order_data"]) ? $context["order_data"] : null), 1 => "email"), "method");
        echo "\" placeholder=\"";
        echo (isset($context["entry_email"]) ? $context["entry_email"] : null);
        echo " *\" id=\"input-payment-email\" class=\"form-control\"/>
            </div>
            <div class=\"form-group required\">
                <input type=\"text\" name=\"telephone\" value=\"";
        // line 35
        echo $this->getAttribute((isset($context["SoUtils"]) ? $context["SoUtils"] : null), "getProperty", array(0 => (isset($context["order_data"]) ? $context["order_data"] : null), 1 => "telephone"), "method");
        echo "\" placeholder=\"";
        echo (isset($context["entry_telephone"]) ? $context["entry_telephone"] : null);
        echo " *\" id=\"input-payment-telephone\" class=\"form-control\"/>
            </div>
            <div class=\"form-group fax-input\">
                <input type=\"text\" name=\"fax\" value=\"";
        // line 38
        echo $this->getAttribute((isset($context["SoUtils"]) ? $context["SoUtils"] : null), "getProperty", array(0 => (isset($context["order_data"]) ? $context["order_data"] : null), 1 => "fax"), "method");
        echo "\" placeholder=\"";
        echo (isset($context["entry_fax"]) ? $context["entry_fax"] : null);
        echo "\" id=\"input-payment-fax\" class=\"form-control\"/>
            </div>
            ";
        // line 40
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["custom_fields"]) ? $context["custom_fields"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["custom_field"]) {
            // line 41
            echo "                ";
            if (($this->getAttribute($context["custom_field"], "location", array()) == "account")) {
                // line 42
                echo "                    ";
                if (($this->getAttribute($context["custom_field"], "type", array()) == "select")) {
                    // line 43
                    echo "                        <div id=\"payment-custom-field";
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "\" class=\"form-group ";
                    echo (($this->getAttribute($context["custom_field"], "required", array())) ? (" required") : (""));
                    echo " custom-field\" data-sort=\"";
                    echo $this->getAttribute($context["custom_field"], "sort_order", array());
                    echo "\">
                            <label class=\"control-label\" for=\"input-payment-custom-field";
                    // line 44
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "\">";
                    echo $this->getAttribute($context["custom_field"], "name", array());
                    echo "</label>
                            <select
                                name=\"custom_field[";
                    // line 46
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "]\"
                                id=\"input-payment-custom-field";
                    // line 47
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "\"
                                class=\"form-control\">
                                <option value=\"\">";
                    // line 49
                    echo (isset($context["text_select"]) ? $context["text_select"] : null);
                    echo "</option>
                                ";
                    // line 50
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["custom_field"], "custom_field_value", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["custom_field_value"]) {
                        // line 51
                        echo "                                    <option value=\"";
                        echo $this->getAttribute($context["custom_field_value"], "custom_field_value_id", array());
                        echo "\">";
                        echo $this->getAttribute($context["custom_field_value"], "name", array());
                        echo "</option>
                                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['custom_field_value'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 53
                    echo "                            </select>
                        </div>
                    ";
                }
                // line 56
                echo "
                    ";
                // line 57
                if (($this->getAttribute($context["custom_field"], "type", array()) == "radio")) {
                    // line 58
                    echo "                        <div id=\"payment-custom-field";
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "\" class=\"form-group ";
                    echo (($this->getAttribute($context["custom_field"], "required", array())) ? (" required") : (""));
                    echo " custom-field\" data-sort=\"";
                    echo $this->getAttribute($context["custom_field"], "sort_order", array());
                    echo "\">
                            <label class=\"control-label\">";
                    // line 59
                    echo $this->getAttribute($context["custom_field"], "name", array());
                    echo "</label>
                            <div id=\"input-payment-custom-field";
                    // line 60
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "\">
                                ";
                    // line 61
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["custom_field"], "custom_field_value", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["custom_field_value"]) {
                        // line 62
                        echo "                                    <div class=\"radio\">
                                        <label>
                                            <input type=\"radio\" name=\"custom_field[";
                        // line 64
                        echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                        echo "]\" value=\"";
                        echo $this->getAttribute($context["custom_field_value"], "custom_field_value_id", array());
                        echo "\"/>
                                            ";
                        // line 65
                        echo $this->getAttribute($context["custom_field_value"], "name", array());
                        echo "
                                        </label>
                                    </div>
                                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['custom_field_value'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 69
                    echo "                            </div>
                        </div>
                    ";
                }
                // line 72
                echo "
                    ";
                // line 73
                if (($this->getAttribute($context["custom_field"], "type", array()) == "checkbox")) {
                    // line 74
                    echo "                        <div id=\"payment-custom-field";
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "\" class=\"form-group ";
                    echo (($this->getAttribute($context["custom_field"], "required", array())) ? (" required") : (""));
                    echo " custom-field\" data-sort=\"";
                    echo $this->getAttribute($context["custom_field"], "sort_order", array());
                    echo "\">
                            <label class=\"control-label\">";
                    // line 75
                    echo $this->getAttribute($context["custom_field"], "name", array());
                    echo "</label>
                            <div id=\"input-payment-custom-field";
                    // line 76
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "\">
                                ";
                    // line 77
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["custom_field"], "custom_field_value", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["custom_field_value"]) {
                        // line 78
                        echo "                                    <div class=\"checkbox\">
                                        <label>
                                            <input type=\"checkbox\" name=\"custom_field[";
                        // line 80
                        echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                        echo "][]\" value=\"";
                        echo $this->getAttribute($context["custom_field_value"], "custom_field_value_id", array());
                        echo "\"/>
                                            ";
                        // line 81
                        echo $this->getAttribute($context["custom_field_value"], "name", array());
                        echo "
                                        </label>
                                    </div>
                                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['custom_field_value'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 85
                    echo "                            </div>
                        </div>
                    ";
                }
                // line 88
                echo "
                    ";
                // line 89
                if (($this->getAttribute($context["custom_field"], "type", array()) == "text")) {
                    // line 90
                    echo "                        <div id=\"payment-custom-field";
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "\" class=\"form-group ";
                    echo (($this->getAttribute($context["custom_field"], "required", array())) ? (" required") : (""));
                    echo " custom-field\" data-sort=\"";
                    echo $this->getAttribute($context["custom_field"], "sort_order", array());
                    echo "\">
                            <label class=\"control-label\" for=\"input-payment-custom-field";
                    // line 91
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "\">";
                    echo $this->getAttribute($context["custom_field"], "name", array());
                    echo "</label>
                            <input type=\"text\"
                                name=\"custom_field[";
                    // line 93
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "]\"
                                value=\"";
                    // line 94
                    echo $this->getAttribute($context["custom_field"], "value", array());
                    echo "\"
                                placeholder=\"";
                    // line 95
                    echo $this->getAttribute($context["custom_field"], "name", array());
                    echo "\"
                                id=\"input-payment-custom-field";
                    // line 96
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "\"
                                class=\"form-control\"/>
                        </div>
                    ";
                }
                // line 100
                echo "
                    ";
                // line 101
                if (($this->getAttribute($context["custom_field"], "type", array()) == "textarea")) {
                    // line 102
                    echo "                        <div id=\"payment-custom-field";
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "\" class=\"form-group ";
                    echo (($this->getAttribute($context["custom_field"], "required", array())) ? (" required") : (""));
                    echo " custom-field\" data-sort=\"";
                    echo $this->getAttribute($context["custom_field"], "sort_order", array());
                    echo "\">
                            <label class=\"control-label\" for=\"input-payment-custom-field";
                    // line 103
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "\">";
                    echo $this->getAttribute($context["custom_field"], "name", array());
                    echo "</label>
                            <textarea
                                name=\"custom_field[";
                    // line 105
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "]\"
                                rows=\"5\" placeholder=\"";
                    // line 106
                    echo $this->getAttribute($context["custom_field"], "name", array());
                    echo "\"
                                id=\"input-payment-custom-field";
                    // line 107
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "\"
                                class=\"form-control\">";
                    // line 108
                    echo $this->getAttribute($context["custom_field"], "value", array());
                    echo "</textarea>
                        </div>
                    ";
                }
                // line 111
                echo "
                    ";
                // line 112
                if (($this->getAttribute($context["custom_field"], "type", array()) == "file")) {
                    // line 113
                    echo "                        <div id=\"payment-custom-field";
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "\" class=\"form-group ";
                    echo (($this->getAttribute($context["custom_field"], "required", array())) ? (" required") : (""));
                    echo " custom-field\" data-sort=\"";
                    echo $this->getAttribute($context["custom_field"], "sort_order", array());
                    echo "\">
                            <label class=\"control-label\">";
                    // line 114
                    echo $this->getAttribute($context["custom_field"], "name", array());
                    echo "</label>
                            <br/>
                            <button type=\"button\"
                                id=\"button-payment-custom-field";
                    // line 117
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "\"
                                data-loading-text=\"";
                    // line 118
                    echo (isset($context["text_loading"]) ? $context["text_loading"] : null);
                    echo "\" class=\"btn btn-default\"><i
                                class=\"fa fa-upload\"></i> ";
                    // line 119
                    echo (isset($context["button_upload"]) ? $context["button_upload"] : null);
                    echo "</button>
                            <input type=\"hidden\"
                                name=\"custom_field[";
                    // line 121
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "]\"
                                value=\"\"
                                id=\"input-payment-custom-field";
                    // line 123
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "\"/>
                        </div>
                    ";
                }
                // line 126
                echo "
                    ";
                // line 127
                if (($this->getAttribute($context["custom_field"], "type", array()) == "date")) {
                    // line 128
                    echo "                        <div id=\"payment-custom-field";
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "\" class=\"form-group ";
                    echo (($this->getAttribute($context["custom_field"], "required", array())) ? (" required") : (""));
                    echo " custom-field\" data-sort=\"";
                    echo $this->getAttribute($context["custom_field"], "sort_order", array());
                    echo "\">
                            <label class=\"control-label\" for=\"input-payment-custom-field";
                    // line 129
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "\">";
                    echo $this->getAttribute($context["custom_field"], "name", array());
                    echo "</label>
                            <div class=\"input-group date\">
                                <input type=\"text\"
                                    name=\"custom_field[";
                    // line 132
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "]\"
                                    value=\"";
                    // line 133
                    echo $this->getAttribute($context["custom_field"], "value", array());
                    echo "\"
                                    placeholder=\"";
                    // line 134
                    echo $this->getAttribute($context["custom_field"], "name", array());
                    echo "\" data-date-format=\"YYYY-MM-DD\"
                                    id=\"input-payment-custom-field";
                    // line 135
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "\"
                                    class=\"form-control\"/>
                                <span class=\"input-group-btn\">
                                    <button type=\"button\" class=\"btn btn-default\"><i class=\"fa fa-calendar\"></i></button>
                                </span>
                            </div>
                        </div>
                    ";
                }
                // line 143
                echo "
                    ";
                // line 144
                if (($this->getAttribute($context["custom_field"], "type", array()) == "time")) {
                    // line 145
                    echo "                        <div id=\"payment-custom-field";
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "\" class=\"form-group ";
                    echo (($this->getAttribute($context["custom_field"], "required", array())) ? (" required") : (""));
                    echo " custom-field\" data-sort=\"";
                    echo $this->getAttribute($context["custom_field"], "sort_order", array());
                    echo "\">
                            <label class=\"control-label\" for=\"input-payment-custom-field";
                    // line 146
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "\">";
                    echo $this->getAttribute($context["custom_field"], "name", array());
                    echo "</label>
                            <div class=\"input-group time\">
                                <input type=\"text\"
                                    name=\"custom_field[";
                    // line 149
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "]\"
                                    value=\"";
                    // line 150
                    echo $this->getAttribute($context["custom_field"], "value", array());
                    echo "\"
                                    placeholder=\"";
                    // line 151
                    echo $this->getAttribute($context["custom_field"], "name", array());
                    echo "\" data-date-format=\"HH:mm\"
                                    id=\"input-payment-custom-field";
                    // line 152
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "\"
                                    class=\"form-control\"/>
                                <span class=\"input-group-btn\">
                                    <button type=\"button\" class=\"btn btn-default\"><i class=\"fa fa-calendar\"></i></button>
                                </span>
                            </div>
                        </div>
                    ";
                }
                // line 160
                echo "
                    ";
                // line 161
                if (($this->getAttribute($context["custom_field"], "type", array()) == "datetime")) {
                    // line 162
                    echo "                        <div id=\"payment-custom-field";
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "\" class=\"form-group ";
                    echo (($this->getAttribute($context["custom_field"], "required", array())) ? (" required") : (""));
                    echo " custom-field\" data-sort=\"";
                    echo $this->getAttribute($context["custom_field"], "sort_order", array());
                    echo "\">
                            <label class=\"control-label\" for=\"input-payment-custom-field";
                    // line 163
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "\">";
                    echo $this->getAttribute($context["custom_field"], "name", array());
                    echo "</label>
                            <div class=\"input-group datetime\">
                                <input type=\"text\"
                                    name=\"custom_field[";
                    // line 166
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "]\"
                                    value=\"";
                    // line 167
                    echo $this->getAttribute($context["custom_field"], "value", array());
                    echo "\"
                                    placeholder=\"";
                    // line 168
                    echo $this->getAttribute($context["custom_field"], "name", array());
                    echo "\"
                                    data-date-format=\"YYYY-MM-DD HH:mm\"
                                    id=\"input-payment-custom-field";
                    // line 170
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "\"
                                    class=\"form-control\"/>
                                <span class=\"input-group-btn\">
                                    <button type=\"button\" class=\"btn btn-default\"><i class=\"fa fa-calendar\"></i></button>
                                </span>
                            </div>
                        </div>
                    ";
                }
                // line 178
                echo "                ";
            }
            // line 179
            echo "            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['custom_field'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 180
        echo "        </div>
    </fieldset>
    <fieldset id=\"password\" style=\"display: ";
        // line 182
        echo ((($this->getAttribute((isset($context["setting_so_onepagecheckout_layout_setting"]) ? $context["setting_so_onepagecheckout_layout_setting"] : null), "so_onepagecheckout_account_open", array()) == "register")) ? ("block") : ("none"));
        echo "\">
        ";
        // line 183
        if (($this->getAttribute((isset($context["setting_so_onepagecheckout_general"]) ? $context["setting_so_onepagecheckout_general"] : null), "so_onepagecheckout_layout", array()) != 2)) {
            // line 184
            echo "            <h2 class=\"secondary-title\"><i class=\"fa fa-lock\"></i>";
            echo (isset($context["text_your_password"]) ? $context["text_your_password"] : null);
            echo "</h2>
        ";
        }
        // line 186
        echo "        <div class=\"box-inner\">
            <div class=\"form-group required\">
                <input type=\"password\" name=\"password\" value=\"";
        // line 188
        echo $this->getAttribute((isset($context["SoUtils"]) ? $context["SoUtils"] : null), "getProperty", array(0 => (isset($context["order_data"]) ? $context["order_data"] : null), 1 => "password"), "method");
        echo "\" placeholder=\"";
        echo (isset($context["entry_password"]) ? $context["entry_password"] : null);
        echo " *\" id=\"input-payment-password\" class=\"form-control\"/>
            </div>
            <div class=\"form-group required\">
                <input type=\"password\" name=\"confirm\" value=\"";
        // line 191
        echo $this->getAttribute((isset($context["SoUtils"]) ? $context["SoUtils"] : null), "getProperty", array(0 => (isset($context["order_data"]) ? $context["order_data"] : null), 1 => "confirm"), "method");
        echo "\" placeholder=\"";
        echo (isset($context["entry_confirm"]) ? $context["entry_confirm"] : null);
        echo " *\" id=\"input-payment-confirm\" class=\"form-control\"/>
            </div>
        </div>
    </fieldset>
    <fieldset id=\"address\">
        <h2 class=\"secondary-title\"><i class=\"fa fa-map-marker\"></i>";
        // line 196
        echo (isset($context["text_your_address"]) ? $context["text_your_address"] : null);
        echo "</h2>
        ";
        // line 197
        echo (isset($context["payment_address_form"]) ? $context["payment_address_form"] : null);
        echo "
    </fieldset>
    
    ";
        // line 200
        if ((isset($context["is_shipping_required"]) ? $context["is_shipping_required"] : null)) {
            // line 201
            echo "        <div class=\"checkbox\">
            <label>
                ";
            // line 203
            if (((isset($context["shipping_address"]) ? $context["shipping_address"] : null) == "1")) {
                // line 204
                echo "                    <input type=\"checkbox\" name=\"shipping_address\" value=\"1\" checked=\"checked\" />
                ";
            } else {
                // line 206
                echo "                    <input type=\"checkbox\" name=\"shipping_address\" value=\"0\" />
                ";
            }
            // line 208
            echo "                ";
            echo (isset($context["entry_shipping"]) ? $context["entry_shipping"] : null);
            echo "
            </label>
        </div>
        <fieldset id=\"shipping-address\" style=\"display: ";
            // line 211
            echo ((((isset($context["shipping_address"]) ? $context["shipping_address"] : null) == "1")) ? ("none") : ("block"));
            echo "\">
            <h2 class=\"secondary-title\"><i class=\"fa fa-map-marker\"></i>";
            // line 212
            echo (isset($context["text_shipping_address"]) ? $context["text_shipping_address"] : null);
            echo "</h2>
            ";
            // line 213
            echo (isset($context["shipping_address_form"]) ? $context["shipping_address_form"] : null);
            echo "
        </fieldset>
    ";
        }
        // line 216
        echo "
    <script type=\"text/javascript\">
        if (\$('.so-onepagecheckout .date').length) {
            \$('.so-onepagecheckout .date').datetimepicker({pickTime: false});
        }
        if (\$('.so-onepagecheckout .time').length) {
            \$('.so-onepagecheckout .time').datetimepicker({pickDate: false});
        }
        if (\$('.so-onepagecheckout .datetime').length) {
            \$('.so-onepagecheckout .datetime').datetimepicker({pickDate: true, pickTime: true});
        }

        ";
        // line 228
        if ((twig_length_filter($this->env, (isset($context["customer_groups"]) ? $context["customer_groups"] : null)) > 1)) {
            // line 229
            echo "            \$(document).delegate('input[name=\"customer_group_id\"]', 'change', function () {
                \$(document).trigger('so_checkout_customer_group_changed', this.value);
            });
        ";
        }
        // line 233
        echo "
        \$('#account .form-group[data-sort]').detach().each(function() {
            if (\$(this).attr('data-sort') >= 0 && \$(this).attr('data-sort') <= \$('#account .form-group').length) {
                \$('#account .form-group').eq(\$(this).attr('data-sort')).before(this);
            }

            if (\$(this).attr('data-sort') > \$('#account .form-group').length) {
                \$('#account .form-group:last').after(this);
            }

            if (\$(this).attr('data-sort') < -\$('#account .form-group').length) {
                \$('#account .form-group:first').before(this);
            }
        });
    </script>
</div>";
    }

    public function getTemplateName()
    {
        return "so-destino/template/extension/module/so_onepagecheckout/checkout/register.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  658 => 233,  652 => 229,  650 => 228,  636 => 216,  630 => 213,  626 => 212,  622 => 211,  615 => 208,  611 => 206,  607 => 204,  605 => 203,  601 => 201,  599 => 200,  593 => 197,  589 => 196,  579 => 191,  571 => 188,  567 => 186,  561 => 184,  559 => 183,  555 => 182,  551 => 180,  545 => 179,  542 => 178,  531 => 170,  526 => 168,  522 => 167,  518 => 166,  510 => 163,  501 => 162,  499 => 161,  496 => 160,  485 => 152,  481 => 151,  477 => 150,  473 => 149,  465 => 146,  456 => 145,  454 => 144,  451 => 143,  440 => 135,  436 => 134,  432 => 133,  428 => 132,  420 => 129,  411 => 128,  409 => 127,  406 => 126,  400 => 123,  395 => 121,  390 => 119,  386 => 118,  382 => 117,  376 => 114,  367 => 113,  365 => 112,  362 => 111,  356 => 108,  352 => 107,  348 => 106,  344 => 105,  337 => 103,  328 => 102,  326 => 101,  323 => 100,  316 => 96,  312 => 95,  308 => 94,  304 => 93,  297 => 91,  288 => 90,  286 => 89,  283 => 88,  278 => 85,  268 => 81,  262 => 80,  258 => 78,  254 => 77,  250 => 76,  246 => 75,  237 => 74,  235 => 73,  232 => 72,  227 => 69,  217 => 65,  211 => 64,  207 => 62,  203 => 61,  199 => 60,  195 => 59,  186 => 58,  184 => 57,  181 => 56,  176 => 53,  165 => 51,  161 => 50,  157 => 49,  152 => 47,  148 => 46,  141 => 44,  132 => 43,  129 => 42,  126 => 41,  122 => 40,  115 => 38,  107 => 35,  99 => 32,  91 => 29,  83 => 26,  79 => 24,  73 => 23,  66 => 19,  62 => 18,  58 => 16,  51 => 12,  47 => 11,  43 => 9,  40 => 8,  36 => 7,  32 => 6,  28 => 5,  23 => 3,  19 => 1,);
    }
}
/* <div class="checkout-content checkout-register">*/
/*     <fieldset id="account">*/
/*         <h2 class="secondary-title"><i class="fa fa-user-plus"></i>{{ text_your_details }}</h2>*/
/*         <div class="payment-new box-inner">*/
/*             <div class="form-group customer-group" style="display: {{ customer_groups|length > 1 ? 'block' : 'none' }}">*/
/*                 <label class="control-label">{{ entry_customer_group }}</label>*/
/*                 {% for customer_group in customer_groups %}*/
/*                     {% if customer_group.customer_group_id == customer_group_id %}*/
/*                         <div class="radio">*/
/*                             <label>*/
/*                                 <input type="radio" name="customer_group_id" value="{{ customer_group.customer_group_id }}" checked="checked"/>*/
/*                                 {{ customer_group.name }}*/
/*                             </label>*/
/*                         </div>*/
/*                     {% else %}*/
/*                         <div class="radio">*/
/*                             <label>*/
/*                                 <input type="radio" name="customer_group_id" value="{{ customer_group.customer_group_id }}"/>*/
/*                                 {{ customer_group.name }}*/
/*                             </label>*/
/*                         </div>*/
/*                     {% endif %}*/
/*                 {% endfor %}*/
/*             </div>*/
/*             <div class="form-group input-firstname required" style="width: 49%; float: left;">*/
/*                 <input type="text" name="firstname" value="{{ SoUtils.getProperty(order_data, 'firstname') }}" placeholder="{{ entry_firstname }} *" id="input-payment-firstname" class="form-control"/>*/
/*             </div>*/
/*             <div class="form-group input-lastname required" style="width: 49%; float: right;">*/
/*                 <input type="text" name="lastname" value="{{ SoUtils.getProperty(order_data, 'lastname') }}" placeholder="{{ entry_lastname }} *" id="input-payment-lastname" class="form-control"/>*/
/*             </div>*/
/*             <div class="form-group required">*/
/*                 <input type="text" name="email" value="{{ SoUtils.getProperty(order_data, 'email') }}" placeholder="{{ entry_email }} *" id="input-payment-email" class="form-control"/>*/
/*             </div>*/
/*             <div class="form-group required">*/
/*                 <input type="text" name="telephone" value="{{ SoUtils.getProperty(order_data, 'telephone') }}" placeholder="{{ entry_telephone }} *" id="input-payment-telephone" class="form-control"/>*/
/*             </div>*/
/*             <div class="form-group fax-input">*/
/*                 <input type="text" name="fax" value="{{ SoUtils.getProperty(order_data, 'fax') }}" placeholder="{{ entry_fax }}" id="input-payment-fax" class="form-control"/>*/
/*             </div>*/
/*             {% for custom_field in custom_fields %}*/
/*                 {% if custom_field.location == 'account' %}*/
/*                     {% if custom_field.type == 'select' %}*/
/*                         <div id="payment-custom-field{{ custom_field.custom_field_id }}" class="form-group {{ custom_field.required ? ' required' : '' }} custom-field" data-sort="{{ custom_field.sort_order }}">*/
/*                             <label class="control-label" for="input-payment-custom-field{{ custom_field.custom_field_id }}">{{ custom_field.name }}</label>*/
/*                             <select*/
/*                                 name="custom_field[{{ custom_field.custom_field_id }}]"*/
/*                                 id="input-payment-custom-field{{ custom_field.custom_field_id }}"*/
/*                                 class="form-control">*/
/*                                 <option value="">{{ text_select }}</option>*/
/*                                 {% for custom_field_value in custom_field.custom_field_value %}*/
/*                                     <option value="{{ custom_field_value.custom_field_value_id }}">{{ custom_field_value.name }}</option>*/
/*                                 {% endfor %}*/
/*                             </select>*/
/*                         </div>*/
/*                     {% endif %}*/
/* */
/*                     {% if custom_field.type == 'radio' %}*/
/*                         <div id="payment-custom-field{{ custom_field.custom_field_id }}" class="form-group {{ custom_field.required ? ' required' : '' }} custom-field" data-sort="{{ custom_field.sort_order }}">*/
/*                             <label class="control-label">{{ custom_field.name }}</label>*/
/*                             <div id="input-payment-custom-field{{ custom_field.custom_field_id }}">*/
/*                                 {% for custom_field_value in custom_field.custom_field_value %}*/
/*                                     <div class="radio">*/
/*                                         <label>*/
/*                                             <input type="radio" name="custom_field[{{ custom_field.custom_field_id }}]" value="{{ custom_field_value.custom_field_value_id }}"/>*/
/*                                             {{ custom_field_value.name }}*/
/*                                         </label>*/
/*                                     </div>*/
/*                                 {% endfor %}*/
/*                             </div>*/
/*                         </div>*/
/*                     {% endif %}*/
/* */
/*                     {% if custom_field.type == 'checkbox' %}*/
/*                         <div id="payment-custom-field{{ custom_field.custom_field_id }}" class="form-group {{ custom_field.required ? ' required' : '' }} custom-field" data-sort="{{ custom_field.sort_order }}">*/
/*                             <label class="control-label">{{ custom_field.name }}</label>*/
/*                             <div id="input-payment-custom-field{{ custom_field.custom_field_id }}">*/
/*                                 {% for custom_field_value in custom_field.custom_field_value %}*/
/*                                     <div class="checkbox">*/
/*                                         <label>*/
/*                                             <input type="checkbox" name="custom_field[{{ custom_field.custom_field_id }}][]" value="{{ custom_field_value.custom_field_value_id }}"/>*/
/*                                             {{ custom_field_value.name }}*/
/*                                         </label>*/
/*                                     </div>*/
/*                                 {% endfor %}*/
/*                             </div>*/
/*                         </div>*/
/*                     {% endif %}*/
/* */
/*                     {% if custom_field.type == 'text' %}*/
/*                         <div id="payment-custom-field{{ custom_field.custom_field_id }}" class="form-group {{ custom_field.required ? ' required' : '' }} custom-field" data-sort="{{ custom_field.sort_order }}">*/
/*                             <label class="control-label" for="input-payment-custom-field{{ custom_field.custom_field_id }}">{{ custom_field.name }}</label>*/
/*                             <input type="text"*/
/*                                 name="custom_field[{{ custom_field.custom_field_id }}]"*/
/*                                 value="{{ custom_field.value }}"*/
/*                                 placeholder="{{ custom_field.name }}"*/
/*                                 id="input-payment-custom-field{{ custom_field.custom_field_id }}"*/
/*                                 class="form-control"/>*/
/*                         </div>*/
/*                     {% endif %}*/
/* */
/*                     {% if custom_field.type == 'textarea' %}*/
/*                         <div id="payment-custom-field{{ custom_field.custom_field_id }}" class="form-group {{ custom_field.required ? ' required' : '' }} custom-field" data-sort="{{ custom_field.sort_order }}">*/
/*                             <label class="control-label" for="input-payment-custom-field{{ custom_field.custom_field_id }}">{{ custom_field.name }}</label>*/
/*                             <textarea*/
/*                                 name="custom_field[{{ custom_field.custom_field_id }}]"*/
/*                                 rows="5" placeholder="{{ custom_field.name }}"*/
/*                                 id="input-payment-custom-field{{ custom_field.custom_field_id }}"*/
/*                                 class="form-control">{{ custom_field.value }}</textarea>*/
/*                         </div>*/
/*                     {% endif %}*/
/* */
/*                     {% if custom_field.type == 'file' %}*/
/*                         <div id="payment-custom-field{{ custom_field.custom_field_id }}" class="form-group {{ custom_field.required ? ' required' : '' }} custom-field" data-sort="{{ custom_field.sort_order }}">*/
/*                             <label class="control-label">{{ custom_field.name }}</label>*/
/*                             <br/>*/
/*                             <button type="button"*/
/*                                 id="button-payment-custom-field{{ custom_field.custom_field_id }}"*/
/*                                 data-loading-text="{{ text_loading }}" class="btn btn-default"><i*/
/*                                 class="fa fa-upload"></i> {{ button_upload }}</button>*/
/*                             <input type="hidden"*/
/*                                 name="custom_field[{{ custom_field.custom_field_id }}]"*/
/*                                 value=""*/
/*                                 id="input-payment-custom-field{{ custom_field.custom_field_id }}"/>*/
/*                         </div>*/
/*                     {% endif %}*/
/* */
/*                     {% if custom_field.type == 'date' %}*/
/*                         <div id="payment-custom-field{{ custom_field.custom_field_id }}" class="form-group {{ custom_field.required ? ' required' : '' }} custom-field" data-sort="{{ custom_field.sort_order }}">*/
/*                             <label class="control-label" for="input-payment-custom-field{{ custom_field.custom_field_id }}">{{ custom_field.name }}</label>*/
/*                             <div class="input-group date">*/
/*                                 <input type="text"*/
/*                                     name="custom_field[{{ custom_field.custom_field_id }}]"*/
/*                                     value="{{ custom_field.value }}"*/
/*                                     placeholder="{{ custom_field.name }}" data-date-format="YYYY-MM-DD"*/
/*                                     id="input-payment-custom-field{{ custom_field.custom_field_id }}"*/
/*                                     class="form-control"/>*/
/*                                 <span class="input-group-btn">*/
/*                                     <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>*/
/*                                 </span>*/
/*                             </div>*/
/*                         </div>*/
/*                     {% endif %}*/
/* */
/*                     {% if custom_field.type == 'time' %}*/
/*                         <div id="payment-custom-field{{ custom_field.custom_field_id }}" class="form-group {{ custom_field.required ? ' required' : '' }} custom-field" data-sort="{{ custom_field.sort_order }}">*/
/*                             <label class="control-label" for="input-payment-custom-field{{ custom_field.custom_field_id }}">{{ custom_field.name }}</label>*/
/*                             <div class="input-group time">*/
/*                                 <input type="text"*/
/*                                     name="custom_field[{{ custom_field.custom_field_id }}]"*/
/*                                     value="{{ custom_field.value }}"*/
/*                                     placeholder="{{ custom_field.name }}" data-date-format="HH:mm"*/
/*                                     id="input-payment-custom-field{{ custom_field.custom_field_id }}"*/
/*                                     class="form-control"/>*/
/*                                 <span class="input-group-btn">*/
/*                                     <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>*/
/*                                 </span>*/
/*                             </div>*/
/*                         </div>*/
/*                     {% endif %}*/
/* */
/*                     {% if custom_field.type == 'datetime' %}*/
/*                         <div id="payment-custom-field{{ custom_field.custom_field_id }}" class="form-group {{ custom_field.required ? ' required' : '' }} custom-field" data-sort="{{ custom_field.sort_order }}">*/
/*                             <label class="control-label" for="input-payment-custom-field{{ custom_field.custom_field_id }}">{{ custom_field.name }}</label>*/
/*                             <div class="input-group datetime">*/
/*                                 <input type="text"*/
/*                                     name="custom_field[{{ custom_field.custom_field_id }}]"*/
/*                                     value="{{ custom_field.value }}"*/
/*                                     placeholder="{{ custom_field.name }}"*/
/*                                     data-date-format="YYYY-MM-DD HH:mm"*/
/*                                     id="input-payment-custom-field{{ custom_field.custom_field_id }}"*/
/*                                     class="form-control"/>*/
/*                                 <span class="input-group-btn">*/
/*                                     <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>*/
/*                                 </span>*/
/*                             </div>*/
/*                         </div>*/
/*                     {% endif %}*/
/*                 {% endif %}*/
/*             {% endfor %}*/
/*         </div>*/
/*     </fieldset>*/
/*     <fieldset id="password" style="display: {{ setting_so_onepagecheckout_layout_setting.so_onepagecheckout_account_open == 'register' ? 'block' : 'none' }}">*/
/*         {% if setting_so_onepagecheckout_general.so_onepagecheckout_layout != 2 %}*/
/*             <h2 class="secondary-title"><i class="fa fa-lock"></i>{{ text_your_password }}</h2>*/
/*         {% endif %}*/
/*         <div class="box-inner">*/
/*             <div class="form-group required">*/
/*                 <input type="password" name="password" value="{{ SoUtils.getProperty(order_data, 'password') }}" placeholder="{{ entry_password }} *" id="input-payment-password" class="form-control"/>*/
/*             </div>*/
/*             <div class="form-group required">*/
/*                 <input type="password" name="confirm" value="{{ SoUtils.getProperty(order_data, 'confirm') }}" placeholder="{{ entry_confirm }} *" id="input-payment-confirm" class="form-control"/>*/
/*             </div>*/
/*         </div>*/
/*     </fieldset>*/
/*     <fieldset id="address">*/
/*         <h2 class="secondary-title"><i class="fa fa-map-marker"></i>{{ text_your_address }}</h2>*/
/*         {{ payment_address_form }}*/
/*     </fieldset>*/
/*     */
/*     {% if is_shipping_required %}*/
/*         <div class="checkbox">*/
/*             <label>*/
/*                 {% if shipping_address == '1' %}*/
/*                     <input type="checkbox" name="shipping_address" value="1" checked="checked" />*/
/*                 {% else %}*/
/*                     <input type="checkbox" name="shipping_address" value="0" />*/
/*                 {% endif %}*/
/*                 {{ entry_shipping }}*/
/*             </label>*/
/*         </div>*/
/*         <fieldset id="shipping-address" style="display: {{ shipping_address == '1' ? 'none' : 'block' }}">*/
/*             <h2 class="secondary-title"><i class="fa fa-map-marker"></i>{{ text_shipping_address }}</h2>*/
/*             {{ shipping_address_form }}*/
/*         </fieldset>*/
/*     {% endif %}*/
/* */
/*     <script type="text/javascript">*/
/*         if ($('.so-onepagecheckout .date').length) {*/
/*             $('.so-onepagecheckout .date').datetimepicker({pickTime: false});*/
/*         }*/
/*         if ($('.so-onepagecheckout .time').length) {*/
/*             $('.so-onepagecheckout .time').datetimepicker({pickDate: false});*/
/*         }*/
/*         if ($('.so-onepagecheckout .datetime').length) {*/
/*             $('.so-onepagecheckout .datetime').datetimepicker({pickDate: true, pickTime: true});*/
/*         }*/
/* */
/*         {% if customer_groups|length > 1 %}*/
/*             $(document).delegate('input[name="customer_group_id"]', 'change', function () {*/
/*                 $(document).trigger('so_checkout_customer_group_changed', this.value);*/
/*             });*/
/*         {% endif %}*/
/* */
/*         $('#account .form-group[data-sort]').detach().each(function() {*/
/*             if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#account .form-group').length) {*/
/*                 $('#account .form-group').eq($(this).attr('data-sort')).before(this);*/
/*             }*/
/* */
/*             if ($(this).attr('data-sort') > $('#account .form-group').length) {*/
/*                 $('#account .form-group:last').after(this);*/
/*             }*/
/* */
/*             if ($(this).attr('data-sort') < -$('#account .form-group').length) {*/
/*                 $('#account .form-group:first').before(this);*/
/*             }*/
/*         });*/
/*     </script>*/
/* </div>*/
