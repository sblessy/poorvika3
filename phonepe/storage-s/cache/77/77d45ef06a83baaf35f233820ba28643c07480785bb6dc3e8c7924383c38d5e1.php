<?php

/* so-destino/template/extension/module/so_onepagecheckout/checkout/shipping_methods.twig */
class __TwigTemplate_5fc97bccfae9ebeb5268d195a152ff22371749ae631cf0166c636b7de105d42d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (($this->getAttribute((isset($context["setting_so_onepagecheckout_layout_setting"]) ? $context["setting_so_onepagecheckout_layout_setting"] : null), "delivery_method_status", array()) == 1)) {
            // line 2
            echo "<div class=\"checkout-content checkout-shipping-methods\">
    ";
            // line 3
            if ((isset($context["error_warning"]) ? $context["error_warning"] : null)) {
                // line 4
                echo "        <div class=\"alert alert-warning\"><i class=\"fa fa-exclamation-circle\"></i> ";
                echo (isset($context["error_warning"]) ? $context["error_warning"] : null);
                echo "</div>
    ";
            }
            // line 6
            echo "    ";
            if ((isset($context["shipping_methods"]) ? $context["shipping_methods"] : null)) {
                // line 7
                echo "        <h2 class=\"secondary-title\"><i class=\"fa fa-location-arrow\"></i>";
                echo (isset($context["text_title_shipping_method"]) ? $context["text_title_shipping_method"] : null);
                echo "</h2>
        <div class=\"box-inner\">
            ";
                // line 9
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["shipping_methods"]) ? $context["shipping_methods"] : null));
                foreach ($context['_seq'] as $context["key"] => $context["shipping_method"]) {
                    // line 10
                    echo "                <p><strong>";
                    echo $this->getAttribute($context["shipping_method"], "title", array());
                    echo "</strong></p>
                ";
                    // line 11
                    if ( !$this->getAttribute($context["shipping_method"], "error", array())) {
                        // line 12
                        echo "                    ";
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["shipping_method"], "quote", array()));
                        foreach ($context['_seq'] as $context["_key"] => $context["quote"]) {
                            // line 13
                            echo "                        ";
                            $context["shipping_status"] = twig_split_filter($this->env, $this->getAttribute($context["quote"], "code", array()), ".");
                            // line 14
                            echo "                        ";
                            if (($this->getAttribute((isset($context["setting_so_onepagecheckout_layout_setting"]) ? $context["setting_so_onepagecheckout_layout_setting"] : null), ($this->getAttribute((isset($context["shipping_status"]) ? $context["shipping_status"] : null), 0, array(), "array") . "_status"), array(), "array", true, true) && ($this->getAttribute((isset($context["setting_so_onepagecheckout_layout_setting"]) ? $context["setting_so_onepagecheckout_layout_setting"] : null), ($this->getAttribute((isset($context["shipping_status"]) ? $context["shipping_status"] : null), 0, array(), "array") . "_status"), array(), "array") == 1))) {
                                // line 15
                                echo "                            <div class=\"radio\">
                                <label>
                                    ";
                                // line 17
                                if (((($this->getAttribute($context["quote"], "code", array()) == (isset($context["code"]) ? $context["code"] : null)) ||  !(isset($context["code"]) ? $context["code"] : null)) || ($this->getAttribute((isset($context["setting_so_onepagecheckout_layout_setting"]) ? $context["setting_so_onepagecheckout_layout_setting"] : null), "so_onepagecheckout_default_shipping", array()) == $this->getAttribute((isset($context["shipping_status"]) ? $context["shipping_status"] : null), 0, array(), "array")))) {
                                    // line 18
                                    echo "                                        ";
                                    $context["code"] = $this->getAttribute($context["quote"], "code", array());
                                    // line 19
                                    echo "                                        <input type=\"radio\" name=\"shipping_method\" value=\"";
                                    echo $this->getAttribute($context["quote"], "code", array());
                                    echo "\" checked=\"checked\"/>
                                    ";
                                } else {
                                    // line 21
                                    echo "                                        <input type=\"radio\" name=\"shipping_method\" value=\"";
                                    echo $this->getAttribute($context["quote"], "code", array());
                                    echo "\"/>
                                    ";
                                }
                                // line 23
                                echo "                                    ";
                                echo $this->getAttribute($context["quote"], "title", array());
                                echo " - ";
                                echo $this->getAttribute($context["quote"], "text", array());
                                echo "
                                </label>
                            </div>
                        ";
                            }
                            // line 27
                            echo "                    ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['quote'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 28
                        echo "                ";
                    } else {
                        // line 29
                        echo "                    <div class=\"alert alert-danger\">";
                        echo $this->getAttribute($context["shipping_method"], "error", array());
                        echo "</div>
                ";
                    }
                    // line 31
                    echo "            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['key'], $context['shipping_method'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 32
                echo "        </div>
    ";
            }
            // line 34
            echo "</div>
";
        }
    }

    public function getTemplateName()
    {
        return "so-destino/template/extension/module/so_onepagecheckout/checkout/shipping_methods.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  119 => 34,  115 => 32,  109 => 31,  103 => 29,  100 => 28,  94 => 27,  84 => 23,  78 => 21,  72 => 19,  69 => 18,  67 => 17,  63 => 15,  60 => 14,  57 => 13,  52 => 12,  50 => 11,  45 => 10,  41 => 9,  35 => 7,  32 => 6,  26 => 4,  24 => 3,  21 => 2,  19 => 1,);
    }
}
/* {% if setting_so_onepagecheckout_layout_setting.delivery_method_status == 1 %}*/
/* <div class="checkout-content checkout-shipping-methods">*/
/*     {% if error_warning %}*/
/*         <div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> {{ error_warning }}</div>*/
/*     {% endif %}*/
/*     {% if shipping_methods %}*/
/*         <h2 class="secondary-title"><i class="fa fa-location-arrow"></i>{{ text_title_shipping_method }}</h2>*/
/*         <div class="box-inner">*/
/*             {% for key, shipping_method in shipping_methods %}*/
/*                 <p><strong>{{ shipping_method.title }}</strong></p>*/
/*                 {% if not shipping_method.error %}*/
/*                     {% for quote in shipping_method.quote %}*/
/*                         {% set shipping_status = quote.code|split('.') %}*/
/*                         {% if setting_so_onepagecheckout_layout_setting[shipping_status[0]~'_status'] is defined and setting_so_onepagecheckout_layout_setting[shipping_status[0]~'_status'] == 1 %}*/
/*                             <div class="radio">*/
/*                                 <label>*/
/*                                     {% if quote.code == code or not code or setting_so_onepagecheckout_layout_setting.so_onepagecheckout_default_shipping == shipping_status[0] %}*/
/*                                         {% set code = quote.code %}*/
/*                                         <input type="radio" name="shipping_method" value="{{ quote.code }}" checked="checked"/>*/
/*                                     {% else %}*/
/*                                         <input type="radio" name="shipping_method" value="{{ quote.code }}"/>*/
/*                                     {% endif %}*/
/*                                     {{ quote.title }} - {{ quote.text }}*/
/*                                 </label>*/
/*                             </div>*/
/*                         {% endif %}*/
/*                     {% endfor %}*/
/*                 {% else %}*/
/*                     <div class="alert alert-danger">{{ shipping_method.error }}</div>*/
/*                 {% endif %}*/
/*             {% endfor %}*/
/*         </div>*/
/*     {% endif %}*/
/* </div>*/
/* {% endif %}*/
