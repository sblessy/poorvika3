<?php

/* catalog/wk_preordered_list.twig */
class __TwigTemplate_6ac3ef3e34a351f74e39e4f0a04dcc523c1f5e807018ab304ee3336b5fede6fc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo " 
";
        // line 2
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo " 
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
        <div class=\"pull-right\">
          <button type=\"button\" data-toggle=\"tooltip\" title=\"Delete\" id=\"delete_order\" class=\"btn btn-danger\"><i class=\"fa fa-trash-o\"></i></button>
          ";
        // line 8
        if ((isset($context["notification_status"]) ? $context["notification_status"] : null)) {
            echo " 
            <button id=\"notify\"  class=\"btn btn-primary\" data-toggle=\"tooltip\" data-original-title=\"";
            // line 9
            echo (isset($context["text_notify_help"]) ? $context["text_notify_help"] : null);
            echo "\" >
              <i class=\"fa fa-envelope\"></i>
              &nbsp;
              ";
            // line 12
            echo (isset($context["text_notify"]) ? $context["text_notify"] : null);
            echo " 
            </button>
          ";
        }
        // line 14
        echo " 
        </div>
        <h1>";
        // line 16
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
        <ul class=\"breadcrumb\">
          ";
        // line 18
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            echo " 
          <li><a href=\"";
            // line 19
            echo $this->getAttribute($context["breadcrumb"], "href", array(), "array");
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array(), "array");
            echo "</a></li>
          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 20
        echo " 
        </ul>
    </div>
  </div>

  <div class=\"container-fluid\">
    ";
        // line 26
        if ((isset($context["error_warning"]) ? $context["error_warning"] : null)) {
            echo " 
    <div class=\"alert alert-danger\">
    \t<i class=\"fa fa-exclamation-circle\"></i>
    \t";
            // line 29
            echo (isset($context["error_warning"]) ? $context["error_warning"] : null);
            echo " 
    </div>
    ";
        }
        // line 31
        echo " 
    ";
        // line 32
        if ((isset($context["success"]) ? $context["success"] : null)) {
            echo " 
    <div class=\"alert alert-success\">
    \t<i class=\"fa fa-check-circle\"></i>
    \t";
            // line 35
            echo (isset($context["success"]) ? $context["success"] : null);
            echo " 
    </div>
    ";
        }
        // line 37
        echo " 
    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <h3 class=\"panel-title\"><i class=\"fa fa-list\"></i>";
        // line 40
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h3>
      </div>
      <div class=\"panel-body\">
        <div class=\"well\">
          <div class=\"row\">
            <div class=\"col-sm-4\">
              <div class=\"form-group\">
                <label class=\"control-label\">
                  ";
        // line 48
        echo (isset($context["text_pname"]) ? $context["text_pname"] : null);
        echo " 
                </label>
                <input type=\"text\" class=\"form-control\" name=\"filter_pname\" value=\"";
        // line 50
        echo (isset($context["pname"]) ? $context["pname"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["text_pname"]) ? $context["text_pname"] : null);
        echo "\" />
              </div>
              <div class=\"form-group\">
                <label class=\"control-label\">
                  ";
        // line 54
        echo (isset($context["text_notification_status"]) ? $context["text_notification_status"] : null);
        echo " 
                </label>
                <select name=\"filter_notified\" class=\"form-control\">
                  <option value=\"\"></option>
                  <option value=\"1\" ";
        // line 58
        if ((array_key_exists("notified", $context) && ((isset($context["notified"]) ? $context["notified"] : null) == "yes"))) {
            echo " ";
            echo "selected";
        }
        echo " >";
        echo (isset($context["text_notified"]) ? $context["text_notified"] : null);
        echo "</option>
                  <option value=\"0\" ";
        // line 59
        if ((array_key_exists("notified", $context) && ((isset($context["notified"]) ? $context["notified"] : null) == "no"))) {
            echo " ";
            echo "selected";
        }
        echo " >";
        echo (isset($context["text_not_notified"]) ? $context["text_not_notified"] : null);
        echo "</option>
                </select>
              </div>
            </div>
            <div class=\"col-sm-4\">
              <div class=\"form-group\">
                <label class=\"control-label\">
                  ";
        // line 66
        echo (isset($context["text_name"]) ? $context["text_name"] : null);
        echo " 
                </label>
                <input type=\"text\" class=\"form-control date\" name=\"filter_name\" value=\"";
        // line 68
        echo (isset($context["name"]) ? $context["name"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["text_name"]) ? $context["text_name"] : null);
        echo "\" />
              </div>
              <div class=\"form-group\">
                <label class=\"control-label\">
                  ";
        // line 72
        echo (isset($context["text_status"]) ? $context["text_status"] : null);
        echo " 
                </label>
                <select class=\"form-control\" name=\"filter_status\">
                  <option value=\"\"></option>
                  <option value=\"0\" ";
        // line 76
        if ((array_key_exists("status", $context) && ((isset($context["status"]) ? $context["status"] : null) == "no"))) {
            echo " ";
            echo "selected";
        }
        echo " >";
        echo (isset($context["text_pre_ordered"]) ? $context["text_pre_ordered"] : null);
        echo "</option>
                  <option value=\"1\" ";
        // line 77
        if ((array_key_exists("status", $context) && ((isset($context["status"]) ? $context["status"] : null) == "yes"))) {
            echo " ";
            echo "selected";
        }
        echo " >";
        echo (isset($context["text_full_paid"]) ? $context["text_full_paid"] : null);
        echo "</option>
                </select>
              </div>
            </div>
            <div class=\"col-sm-4\">
              <div class=\"form-group\">
                <label class=\"control-label\">
                  ";
        // line 84
        echo (isset($context["text_cus_email"]) ? $context["text_cus_email"] : null);
        echo " 
                </label>
                <input type=\"text\" class=\"form-control\" name=\"filter_customer_mail\" value=\"";
        // line 86
        echo (isset($context["customer_mail"]) ? $context["customer_mail"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["text_cus_email"]) ? $context["text_cus_email"] : null);
        echo "\" />
              </div>
              <div class=\"form-group\">
                <div class=\"btn-group pull-right\">
                  <button class=\"btn btn-primary\" type=\"button\" onclick=\"filter();\" data-toggle=\"tooltip\" title=\"";
        // line 90
        echo (isset($context["button_filter"]) ? $context["button_filter"] : null);
        echo "\" >
                    <i class=\"fa fa-filter\"></i>
                  </button>
                  <a href=\"";
        // line 93
        echo (isset($context["clear_filter"]) ? $context["clear_filter"] : null);
        echo "\" class=\"btn btn-danger\" data-toggle=\"tooltip\" title=\"";
        echo (isset($context["button_clear_filter"]) ? $context["button_clear_filter"] : null);
        echo "\" >
                    <i class=\"fa fa-eraser\"></i>
                  </a>
                </div>
            </div>
          </div>
        </div>
        <form  action=\"";
        // line 100
        echo (isset($context["delete"]) ? $context["delete"] : null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form\" class=\"form-horizontal\">
          <table class=\"table table-bordered table-hover\">
            <thead>
              <tr>
              <td class=\"text-center\" style=\"width:1px\"><input type=\"checkbox\" onclick=\"\$('input[name*=\\'selected\\']').prop('checked', this.checked);\" /></td>
              <td class=\"text-left\">";
        // line 105
        if (((isset($context["sort"]) ? $context["sort"] : null) == "ppd.order_id")) {
            echo " 
                  <a href=\"";
            // line 106
            echo (isset($context["sort_orderid"]) ? $context["sort_orderid"] : null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, (isset($context["order"]) ? $context["order"] : null));
            echo "\">";
            echo (isset($context["text_order_id"]) ? $context["text_order_id"] : null);
            echo "</a>
                  ";
        } else {
            // line 107
            echo " 
                  <a href=\"";
            // line 108
            echo (isset($context["sort_orderid"]) ? $context["sort_orderid"] : null);
            echo "\">";
            echo (isset($context["text_order_id"]) ? $context["text_order_id"] : null);
            echo "</a>
                  ";
        }
        // line 109
        echo " 
                </td>

                <td class=\"text-left\">";
        // line 112
        if (((isset($context["sort"]) ? $context["sort"] : null) == "pd.name")) {
            echo " 
                  <a href=\"";
            // line 113
            echo (isset($context["sort_pname"]) ? $context["sort_pname"] : null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, (isset($context["order"]) ? $context["order"] : null));
            echo "\">";
            echo (isset($context["text_pname"]) ? $context["text_pname"] : null);
            echo "</a>
                  ";
        } else {
            // line 114
            echo " 
                  <a href=\"";
            // line 115
            echo (isset($context["sort_pname"]) ? $context["sort_pname"] : null);
            echo "\">";
            echo (isset($context["text_pname"]) ? $context["text_pname"] : null);
            echo "</a>
                  ";
        }
        // line 116
        echo " 
                </td>

                <td class=\"text-left\">";
        // line 119
        if (((isset($context["sort"]) ? $context["sort"] : null) == "c.firstname")) {
            echo " 
                  <a href=\"";
            // line 120
            echo (isset($context["sort_cname"]) ? $context["sort_cname"] : null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, (isset($context["order"]) ? $context["order"] : null));
            echo "\">";
            echo (isset($context["text_name"]) ? $context["text_name"] : null);
            echo "</a>
                  ";
        } else {
            // line 121
            echo " 
                  <a href=\"";
            // line 122
            echo (isset($context["sort_cname"]) ? $context["sort_cname"] : null);
            echo "\">";
            echo (isset($context["text_name"]) ? $context["text_name"] : null);
            echo "</a>
                  ";
        }
        // line 123
        echo " 
                </td>

                <td class=\"text-left\">";
        // line 126
        if (((isset($context["sort"]) ? $context["sort"] : null) == "c.email")) {
            echo " 
                  <a href=\"";
            // line 127
            echo (isset($context["sort_cus_mail"]) ? $context["sort_cus_mail"] : null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, (isset($context["order"]) ? $context["order"] : null));
            echo "\">";
            echo (isset($context["text_cus_email"]) ? $context["text_cus_email"] : null);
            echo "</a>
                  ";
        } else {
            // line 128
            echo " 
                  <a href=\"";
            // line 129
            echo (isset($context["sort_cus_mail"]) ? $context["sort_cus_mail"] : null);
            echo "\">";
            echo (isset($context["text_cus_email"]) ? $context["text_cus_email"] : null);
            echo "</a>
                  ";
        }
        // line 130
        echo " 
                </td>

                <td class=\"text-left\">";
        // line 133
        if (((isset($context["sort"]) ? $context["sort"] : null) == "oo.total")) {
            echo " 
                  <a href=\"";
            // line 134
            echo (isset($context["sort_paid_amount"]) ? $context["sort_paid_amount"] : null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, (isset($context["order"]) ? $context["order"] : null));
            echo "\">";
            echo (isset($context["text_paid_amount"]) ? $context["text_paid_amount"] : null);
            echo "</a>
                  ";
        } else {
            // line 135
            echo " 
                  <a href=\"";
            // line 136
            echo (isset($context["sort_paid_amount"]) ? $context["sort_paid_amount"] : null);
            echo "\">";
            echo (isset($context["text_paid_amount"]) ? $context["text_paid_amount"] : null);
            echo "</a>
                  ";
        }
        // line 137
        echo " 
                </td>

                <td class=\"text-center\">";
        // line 140
        if (((isset($context["sort"]) ? $context["sort"] : null) == "ppd.notified")) {
            echo " 
                  <a href=\"";
            // line 141
            echo (isset($context["sort_notify"]) ? $context["sort_notify"] : null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, (isset($context["order"]) ? $context["order"] : null));
            echo "\">";
            echo (isset($context["text_notification_status"]) ? $context["text_notification_status"] : null);
            echo "</a>
                  ";
        } else {
            // line 142
            echo " 
                  <a href=\"";
            // line 143
            echo (isset($context["sort_notify"]) ? $context["sort_notify"] : null);
            echo "\">";
            echo (isset($context["text_notification_status"]) ? $context["text_notification_status"] : null);
            echo "</a>
                  ";
        }
        // line 144
        echo " 
                </td>

                <td class=\"text-center\">";
        // line 147
        if (((isset($context["sort"]) ? $context["sort"] : null) == "ppd.status")) {
            echo " 
                  <a href=\"";
            // line 148
            echo (isset($context["sort_status"]) ? $context["sort_status"] : null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, (isset($context["order"]) ? $context["order"] : null));
            echo "\">";
            echo (isset($context["text_status"]) ? $context["text_status"] : null);
            echo "</a>
                  ";
        } else {
            // line 149
            echo " 
                  <a href=\"";
            // line 150
            echo (isset($context["sort_status"]) ? $context["sort_status"] : null);
            echo "\">";
            echo (isset($context["text_status"]) ? $context["text_status"] : null);
            echo "</a>
                  ";
        }
        // line 151
        echo " 
                </td>
                <td class=\"text-center\"><a>";
        // line 153
        echo (isset($context["text_action"]) ? $context["text_action"] : null);
        echo "</a></td>
              </tr>
            </thead>
            <tbody>

              ";
        // line 158
        if (array_key_exists("orders", $context)) {
            echo " 
              ";
            // line 159
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["orders"]) ? $context["orders"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["order"]) {
                echo " 
              <tr>

                <td class=\"text-center\">
                  <!-- <input type=\"checkbox\" name=\"selected[]\" value=\"";
                // line 163
                echo $this->getAttribute($context["order"], "id", array(), "array");
                echo "\" ";
                if ($this->getAttribute($context["order"], "notified", array(), "array")) {
                    echo " ";
                    echo "disabled";
                    echo " ";
                }
                echo " /> -->
                  <input type=\"checkbox\" name=\"selected[]\" value=\"";
                // line 164
                echo $this->getAttribute($context["order"], "id", array(), "array");
                echo "\"  />
                </td>
                <td class=\"text-left\">";
                // line 166
                echo ("#" . $this->getAttribute($context["order"], "order_id", array(), "array"));
                echo "</td>
                <td class=\"text-left\">";
                // line 167
                echo $this->getAttribute($context["order"], "product_name", array(), "array");
                echo "</td>
                <td class=\"text-left\">";
                // line 168
                echo $this->getAttribute($context["order"], "customer_name", array(), "array");
                echo "</td>
                <td class=\"text-left\">";
                // line 169
                echo $this->getAttribute($context["order"], "email", array(), "array");
                echo "</td>
                <td class=\"text-left\">";
                // line 170
                echo $this->getAttribute($context["order"], "preorder_price", array(), "array");
                echo "</td>
                <td class=\"text-center\">
                  ";
                // line 172
                if ($this->getAttribute($context["order"], "notified", array(), "array")) {
                    echo " ";
                    echo (("<i class=\"fa fa-check text-success\">&nbsp;" . (isset($context["text_notified"]) ? $context["text_notified"] : null)) . "</i>");
                    echo " ";
                } else {
                    echo " ";
                    echo (("<i class=\"fa fa-close text-warning\">&nbsp;" . (isset($context["text_not_notified"]) ? $context["text_not_notified"] : null)) . "</i>");
                    echo " ";
                }
                echo " 
                </td>
                <td class=\"text-center\">
                  ";
                // line 175
                if ($this->getAttribute($context["order"], "status", array(), "array")) {
                    echo " ";
                    echo (("<i class=\"fa fa-check text-success\">&nbsp;" . (isset($context["text_full_paid"]) ? $context["text_full_paid"] : null)) . "</i>");
                    echo " ";
                } else {
                    echo " ";
                    echo (("<i class=\"fa fa-clock-o text-warning\">&nbsp;" . (isset($context["text_pre_ordered"]) ? $context["text_pre_ordered"] : null)) . "</i>");
                    echo " ";
                }
                echo " 
                </td>
                <td class=\"text-center\">
                  <a href=\"index.php?route=sale/order/info";
                // line 178
                echo ((("&user_token=" . (isset($context["user_token"]) ? $context["user_token"] : null)) . "&order_id=") . $this->getAttribute($context["order"], "order_id", array(), "array"));
                echo "\" data-toggle=\"tooltip\" title=\"";
                echo (isset($context["button_view"]) ? $context["button_view"] : null);
                echo "\" class=\"btn btn-info\" target=\"_blank\"><i class=\"fa fa-eye\"></i></a>
                </td>
              </tr>
              ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['order'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 181
            echo " 
              ";
        } else {
            // line 182
            echo " 
              <tr>
                <td class=\"text-center\" colspan=\"12\">";
            // line 184
            echo (isset($context["text_no_record"]) ? $context["text_no_record"] : null);
            echo "</td>
              </tr>
              ";
        }
        // line 186
        echo " 
            </tbody>
          </table>
    </form>
    <div class=\"row\">
      <div class=\"col-sm-6 text-left\">";
        // line 191
        echo (isset($context["pagination"]) ? $context["pagination"] : null);
        echo "</div>
      <div class=\"col-sm-6 text-right\">";
        // line 192
        echo (isset($context["results"]) ? $context["results"] : null);
        echo "</div>
    </div>
  </div>
  </div>
</div>
</div>
<script type=\"text/javascript\">

\$('body').on('click','#delete_order',function(){
  var flag = false;
  \$('input[type=\\'checkbox\\']').each(function(){
    if (\$(this).is(':checked')) {
      flag = true;
    }
  });
  if (flag) {
      confirm('";
        // line 208
        echo (isset($context["text_confirmation"]) ? $context["text_confirmation"] : null);
        echo "') ? \$('form').submit() : false;
  } else {
     alert('Please select first');
  }
});

  notify_url = '";
        // line 214
        echo (isset($context["notify"]) ? $context["notify"] : null);
        echo "';
  delete_url = '";
        // line 215
        echo (isset($context["delete"]) ? $context["delete"] : null);
        echo "';

  \$(\"#notify\").on('click',function() {
    var confirmation = confirm(\"";
        // line 218
        echo (isset($context["text_confirmation"]) ? $context["text_confirmation"] : null);
        echo "\");
    if (confirmation) {
      \$(\"form\").prop('action',notify_url.replace(/&amp;/g, '&'));
      \$('form').submit();
    }
  });

  \$(\"#delete\").on('click',function(){
    var confirmation = confirm(\"";
        // line 226
        echo (isset($context["text_confirmation"]) ? $context["text_confirmation"] : null);
        echo "\");
    if (confirmation) {
      \$(\"form\").prop('action',notify_url.replace(/&amp;/g, '&'));
      \$('form').submit();
    }
  });

  function filter() {
    url = 'index.php?route=catalog/wk_preordered_list&user_token=";
        // line 234
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "';

    var filter_name = \$('input[name=\\'filter_name\\']').val();
    if (filter_name) {
      url += '&name=' + encodeURIComponent(filter_name);
    }

    var filter_pname = \$('input[name=\\'filter_pname\\']').val();
    if (filter_pname) {
      url += '&pname=' + encodeURIComponent(filter_pname);
    }

    var customer_mail = \$('input[name=\\'filter_customer_mail\\']').val();
    if (customer_mail) {
      url += '&customer_mail=' + encodeURIComponent(customer_mail);
    }

    var filter_notified = \$('select[name=\\'filter_notified\\']').val();
    if (filter_notified) {
      url += '&notified=' + encodeURIComponent(filter_notified);
    }

    var filter_status = \$('select[name=\\'filter_status\\']').val();
    if (filter_status) {
      url += '&status=' + encodeURIComponent(filter_status);
    }

    ";
        // line 261
        if ((array_key_exists("sort", $context) && (isset($context["sort"]) ? $context["sort"] : null))) {
            // line 262
            echo "      url += '&sort=";
            echo (isset($context["sort"]) ? $context["sort"] : null);
            echo "';
    ";
        }
        // line 263
        echo " 

    ";
        // line 265
        if (((array_key_exists("order", $context) &&  !twig_test_iterable((isset($context["order"]) ? $context["order"] : null))) && (isset($context["order"]) ? $context["order"] : null))) {
            // line 266
            echo "    url += '&order=";
            echo (isset($context["order"]) ? $context["order"] : null);
            echo "';
    ";
        }
        // line 267
        echo " 
  
    ";
        // line 269
        if ((array_key_exists("page", $context) && (isset($context["page"]) ? $context["page"] : null))) {
            // line 270
            echo "    url += '&page=";
            echo (isset($context["page"]) ? $context["page"] : null);
            echo "';
    ";
        }
        // line 271
        echo " 
    location = url;
  }

  \$('input[name=\\'filter_name\\']').autocomplete({

\t'source': function(request, response) {
\t\t\$.ajax({
\t\t\turl: 'index.php?route=catalog/wk_preordered_list/autocompleteFilter&user_token=";
        // line 279
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "&filter_name=' +  encodeURIComponent(request),
\t\t\tdataType: 'json',
\t\t\tsuccess: function(json) {
\t\t\t\tresponse(\$.map(json, function(item) {
\t\t\t\t\treturn {
\t\t\t\t\t\tlabel: item['name'],
\t\t\t\t\t\tvalue: item['product_id']
\t\t\t\t\t}
\t\t\t\t}));
\t\t\t}
\t\t});
\t},
\t'select': function(item) {
\t\t\$('input[name=\\'filter_name\\']').val(item['label']);
\t}
});

\$('input[name=\\'filter_pname\\']').autocomplete({

\t'source': function(request, response) {
\t\t\$.ajax({
\t\t\turl: 'index.php?route=catalog/wk_preordered_list/autocompleteFilter&user_token=";
        // line 300
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "&filter_pname=' +  encodeURIComponent(request),
\t\t\tdataType: 'json',
\t\t\tsuccess: function(json) {
\t\t\t\tresponse(\$.map(json, function(item) {
\t\t\t\t\treturn {
\t\t\t\t\t\tlabel: item['pname'],
\t\t\t\t\t\tvalue: item['product_id']
\t\t\t\t\t}
\t\t\t\t}));
\t\t\t}
\t\t});
\t},
\t'select': function(item) {
\t\t\$('input[name=\\'filter_pname\\']').val(item['label']);
\t}
});

\$('input[name=\\'filter_customer_mail\\']').autocomplete({

\t'source': function(request, response) {
\t\t\$.ajax({
\t\t\turl: 'index.php?route=catalog/wk_preordered_list/autocompleteFilter&user_token=";
        // line 321
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "&filter_customer_mail=' +  encodeURIComponent(request),
\t\t\tdataType: 'json',
\t\t\tsuccess: function(json) {
\t\t\t\tresponse(\$.map(json, function(item) {
\t\t\t\t\treturn {
\t\t\t\t\t\tlabel: item['email'],
\t\t\t\t\t\tvalue: item['product_id']
\t\t\t\t\t}
\t\t\t\t}));
\t\t\t}
\t\t});
\t},
\t'select': function(item) {
\t\t\$('input[name=\\'filter_customer_mail\\']').val(item['label']);
\t}
});
</script>

";
        // line 339
        echo (isset($context["footer"]) ? $context["footer"] : null);
        echo " 
";
    }

    public function getTemplateName()
    {
        return "catalog/wk_preordered_list.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  772 => 339,  751 => 321,  727 => 300,  703 => 279,  693 => 271,  687 => 270,  685 => 269,  681 => 267,  675 => 266,  673 => 265,  669 => 263,  663 => 262,  661 => 261,  631 => 234,  620 => 226,  609 => 218,  603 => 215,  599 => 214,  590 => 208,  571 => 192,  567 => 191,  560 => 186,  554 => 184,  550 => 182,  546 => 181,  534 => 178,  520 => 175,  506 => 172,  501 => 170,  497 => 169,  493 => 168,  489 => 167,  485 => 166,  480 => 164,  470 => 163,  461 => 159,  457 => 158,  449 => 153,  445 => 151,  438 => 150,  435 => 149,  426 => 148,  422 => 147,  417 => 144,  410 => 143,  407 => 142,  398 => 141,  394 => 140,  389 => 137,  382 => 136,  379 => 135,  370 => 134,  366 => 133,  361 => 130,  354 => 129,  351 => 128,  342 => 127,  338 => 126,  333 => 123,  326 => 122,  323 => 121,  314 => 120,  310 => 119,  305 => 116,  298 => 115,  295 => 114,  286 => 113,  282 => 112,  277 => 109,  270 => 108,  267 => 107,  258 => 106,  254 => 105,  246 => 100,  234 => 93,  228 => 90,  219 => 86,  214 => 84,  199 => 77,  190 => 76,  183 => 72,  174 => 68,  169 => 66,  154 => 59,  145 => 58,  138 => 54,  129 => 50,  124 => 48,  113 => 40,  108 => 37,  102 => 35,  96 => 32,  93 => 31,  87 => 29,  81 => 26,  73 => 20,  63 => 19,  57 => 18,  52 => 16,  48 => 14,  42 => 12,  36 => 9,  32 => 8,  23 => 2,  19 => 1,);
    }
}
/* {{ header }} */
/* {{ column_left }} */
/* <div id="content">*/
/*   <div class="page-header">*/
/*     <div class="container-fluid">*/
/*         <div class="pull-right">*/
/*           <button type="button" data-toggle="tooltip" title="Delete" id="delete_order" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>*/
/*           {% if (notification_status) %} */
/*             <button id="notify"  class="btn btn-primary" data-toggle="tooltip" data-original-title="{{ text_notify_help }}" >*/
/*               <i class="fa fa-envelope"></i>*/
/*               &nbsp;*/
/*               {{ text_notify }} */
/*             </button>*/
/*           {% endif %} */
/*         </div>*/
/*         <h1>{{ heading_title }}</h1>*/
/*         <ul class="breadcrumb">*/
/*           {% for breadcrumb in breadcrumbs %} */
/*           <li><a href="{{ breadcrumb['href'] }}">{{ breadcrumb['text'] }}</a></li>*/
/*           {% endfor %} */
/*         </ul>*/
/*     </div>*/
/*   </div>*/
/* */
/*   <div class="container-fluid">*/
/*     {% if (error_warning) %} */
/*     <div class="alert alert-danger">*/
/*     	<i class="fa fa-exclamation-circle"></i>*/
/*     	{{ error_warning }} */
/*     </div>*/
/*     {% endif %} */
/*     {% if (success) %} */
/*     <div class="alert alert-success">*/
/*     	<i class="fa fa-check-circle"></i>*/
/*     	{{ success }} */
/*     </div>*/
/*     {% endif %} */
/*     <div class="panel panel-default">*/
/*       <div class="panel-heading">*/
/*         <h3 class="panel-title"><i class="fa fa-list"></i>{{ heading_title }}</h3>*/
/*       </div>*/
/*       <div class="panel-body">*/
/*         <div class="well">*/
/*           <div class="row">*/
/*             <div class="col-sm-4">*/
/*               <div class="form-group">*/
/*                 <label class="control-label">*/
/*                   {{ text_pname }} */
/*                 </label>*/
/*                 <input type="text" class="form-control" name="filter_pname" value="{{ pname }}" placeholder="{{ text_pname }}" />*/
/*               </div>*/
/*               <div class="form-group">*/
/*                 <label class="control-label">*/
/*                   {{ text_notification_status }} */
/*                 </label>*/
/*                 <select name="filter_notified" class="form-control">*/
/*                   <option value=""></option>*/
/*                   <option value="1" {% if (notified is defined and notified == 'yes') %} {{ "selected" }}{% endif %} >{{ text_notified }}</option>*/
/*                   <option value="0" {% if (notified is defined and notified == 'no') %} {{ "selected" }}{% endif %} >{{ text_not_notified }}</option>*/
/*                 </select>*/
/*               </div>*/
/*             </div>*/
/*             <div class="col-sm-4">*/
/*               <div class="form-group">*/
/*                 <label class="control-label">*/
/*                   {{ text_name }} */
/*                 </label>*/
/*                 <input type="text" class="form-control date" name="filter_name" value="{{ name }}" placeholder="{{ text_name }}" />*/
/*               </div>*/
/*               <div class="form-group">*/
/*                 <label class="control-label">*/
/*                   {{ text_status }} */
/*                 </label>*/
/*                 <select class="form-control" name="filter_status">*/
/*                   <option value=""></option>*/
/*                   <option value="0" {% if (status is defined and status == 'no') %} {{ "selected" }}{% endif %} >{{ text_pre_ordered }}</option>*/
/*                   <option value="1" {% if (status is defined and status == 'yes') %} {{ "selected" }}{% endif %} >{{ text_full_paid }}</option>*/
/*                 </select>*/
/*               </div>*/
/*             </div>*/
/*             <div class="col-sm-4">*/
/*               <div class="form-group">*/
/*                 <label class="control-label">*/
/*                   {{ text_cus_email }} */
/*                 </label>*/
/*                 <input type="text" class="form-control" name="filter_customer_mail" value="{{ customer_mail }}" placeholder="{{ text_cus_email }}" />*/
/*               </div>*/
/*               <div class="form-group">*/
/*                 <div class="btn-group pull-right">*/
/*                   <button class="btn btn-primary" type="button" onclick="filter();" data-toggle="tooltip" title="{{ button_filter }}" >*/
/*                     <i class="fa fa-filter"></i>*/
/*                   </button>*/
/*                   <a href="{{ clear_filter }}" class="btn btn-danger" data-toggle="tooltip" title="{{ button_clear_filter }}" >*/
/*                     <i class="fa fa-eraser"></i>*/
/*                   </a>*/
/*                 </div>*/
/*             </div>*/
/*           </div>*/
/*         </div>*/
/*         <form  action="{{ delete }}" method="post" enctype="multipart/form-data" id="form" class="form-horizontal">*/
/*           <table class="table table-bordered table-hover">*/
/*             <thead>*/
/*               <tr>*/
/*               <td class="text-center" style="width:1px"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>*/
/*               <td class="text-left">{% if (sort == 'ppd.order_id') %} */
/*                   <a href="{{ sort_orderid }}" class="{{ order | lower }}">{{ text_order_id }}</a>*/
/*                   {% else %} */
/*                   <a href="{{ sort_orderid }}">{{ text_order_id }}</a>*/
/*                   {% endif %} */
/*                 </td>*/
/* */
/*                 <td class="text-left">{% if (sort == 'pd.name') %} */
/*                   <a href="{{ sort_pname }}" class="{{ order | lower }}">{{ text_pname }}</a>*/
/*                   {% else %} */
/*                   <a href="{{ sort_pname }}">{{ text_pname }}</a>*/
/*                   {% endif %} */
/*                 </td>*/
/* */
/*                 <td class="text-left">{% if (sort == 'c.firstname') %} */
/*                   <a href="{{ sort_cname }}" class="{{ order | lower }}">{{ text_name }}</a>*/
/*                   {% else %} */
/*                   <a href="{{ sort_cname }}">{{ text_name }}</a>*/
/*                   {% endif %} */
/*                 </td>*/
/* */
/*                 <td class="text-left">{% if (sort == 'c.email') %} */
/*                   <a href="{{ sort_cus_mail }}" class="{{ order | lower }}">{{ text_cus_email }}</a>*/
/*                   {% else %} */
/*                   <a href="{{ sort_cus_mail }}">{{ text_cus_email }}</a>*/
/*                   {% endif %} */
/*                 </td>*/
/* */
/*                 <td class="text-left">{% if (sort == 'oo.total') %} */
/*                   <a href="{{ sort_paid_amount }}" class="{{ order | lower }}">{{ text_paid_amount }}</a>*/
/*                   {% else %} */
/*                   <a href="{{ sort_paid_amount }}">{{ text_paid_amount }}</a>*/
/*                   {% endif %} */
/*                 </td>*/
/* */
/*                 <td class="text-center">{% if (sort == 'ppd.notified') %} */
/*                   <a href="{{ sort_notify }}" class="{{ order | lower }}">{{ text_notification_status }}</a>*/
/*                   {% else %} */
/*                   <a href="{{ sort_notify }}">{{ text_notification_status }}</a>*/
/*                   {% endif %} */
/*                 </td>*/
/* */
/*                 <td class="text-center">{% if (sort == 'ppd.status') %} */
/*                   <a href="{{ sort_status }}" class="{{ order | lower }}">{{ text_status }}</a>*/
/*                   {% else %} */
/*                   <a href="{{ sort_status }}">{{ text_status }}</a>*/
/*                   {% endif %} */
/*                 </td>*/
/*                 <td class="text-center"><a>{{ text_action }}</a></td>*/
/*               </tr>*/
/*             </thead>*/
/*             <tbody>*/
/* */
/*               {% if (orders is defined) %} */
/*               {% for order in orders %} */
/*               <tr>*/
/* */
/*                 <td class="text-center">*/
/*                   <!-- <input type="checkbox" name="selected[]" value="{{ order['id'] }}" {% if (order['notified']) %} {{ "disabled" }} {% endif %} /> -->*/
/*                   <input type="checkbox" name="selected[]" value="{{ order['id'] }}"  />*/
/*                 </td>*/
/*                 <td class="text-left">{{ "#"~order['order_id'] }}</td>*/
/*                 <td class="text-left">{{ order['product_name'] }}</td>*/
/*                 <td class="text-left">{{ order['customer_name'] }}</td>*/
/*                 <td class="text-left">{{ order['email'] }}</td>*/
/*                 <td class="text-left">{{ order['preorder_price'] }}</td>*/
/*                 <td class="text-center">*/
/*                   {% if (order['notified']) %} {{ '<i class="fa fa-check text-success">&nbsp;'~text_notified~'</i>' }} {% else %} {{ '<i class="fa fa-close text-warning">&nbsp;'~text_not_notified~'</i>' }} {% endif %} */
/*                 </td>*/
/*                 <td class="text-center">*/
/*                   {% if (order['status']) %} {{ '<i class="fa fa-check text-success">&nbsp;'~text_full_paid~'</i>' }} {% else %} {{ '<i class="fa fa-clock-o text-warning">&nbsp;'~text_pre_ordered~'</i>' }} {% endif %} */
/*                 </td>*/
/*                 <td class="text-center">*/
/*                   <a href="index.php?route=sale/order/info{{ "&user_token="~user_token~"&order_id="~order['order_id'] }}" data-toggle="tooltip" title="{{ button_view }}" class="btn btn-info" target="_blank"><i class="fa fa-eye"></i></a>*/
/*                 </td>*/
/*               </tr>*/
/*               {% endfor %} */
/*               {% else %} */
/*               <tr>*/
/*                 <td class="text-center" colspan="12">{{ text_no_record }}</td>*/
/*               </tr>*/
/*               {% endif %} */
/*             </tbody>*/
/*           </table>*/
/*     </form>*/
/*     <div class="row">*/
/*       <div class="col-sm-6 text-left">{{ pagination }}</div>*/
/*       <div class="col-sm-6 text-right">{{ results }}</div>*/
/*     </div>*/
/*   </div>*/
/*   </div>*/
/* </div>*/
/* </div>*/
/* <script type="text/javascript">*/
/* */
/* $('body').on('click','#delete_order',function(){*/
/*   var flag = false;*/
/*   $('input[type=\'checkbox\']').each(function(){*/
/*     if ($(this).is(':checked')) {*/
/*       flag = true;*/
/*     }*/
/*   });*/
/*   if (flag) {*/
/*       confirm('{{ text_confirmation }}') ? $('form').submit() : false;*/
/*   } else {*/
/*      alert('Please select first');*/
/*   }*/
/* });*/
/* */
/*   notify_url = '{{ notify }}';*/
/*   delete_url = '{{ delete }}';*/
/* */
/*   $("#notify").on('click',function() {*/
/*     var confirmation = confirm("{{ text_confirmation }}");*/
/*     if (confirmation) {*/
/*       $("form").prop('action',notify_url.replace(/&amp;/g, '&'));*/
/*       $('form').submit();*/
/*     }*/
/*   });*/
/* */
/*   $("#delete").on('click',function(){*/
/*     var confirmation = confirm("{{ text_confirmation }}");*/
/*     if (confirmation) {*/
/*       $("form").prop('action',notify_url.replace(/&amp;/g, '&'));*/
/*       $('form').submit();*/
/*     }*/
/*   });*/
/* */
/*   function filter() {*/
/*     url = 'index.php?route=catalog/wk_preordered_list&user_token={{ user_token }}';*/
/* */
/*     var filter_name = $('input[name=\'filter_name\']').val();*/
/*     if (filter_name) {*/
/*       url += '&name=' + encodeURIComponent(filter_name);*/
/*     }*/
/* */
/*     var filter_pname = $('input[name=\'filter_pname\']').val();*/
/*     if (filter_pname) {*/
/*       url += '&pname=' + encodeURIComponent(filter_pname);*/
/*     }*/
/* */
/*     var customer_mail = $('input[name=\'filter_customer_mail\']').val();*/
/*     if (customer_mail) {*/
/*       url += '&customer_mail=' + encodeURIComponent(customer_mail);*/
/*     }*/
/* */
/*     var filter_notified = $('select[name=\'filter_notified\']').val();*/
/*     if (filter_notified) {*/
/*       url += '&notified=' + encodeURIComponent(filter_notified);*/
/*     }*/
/* */
/*     var filter_status = $('select[name=\'filter_status\']').val();*/
/*     if (filter_status) {*/
/*       url += '&status=' + encodeURIComponent(filter_status);*/
/*     }*/
/* */
/*     {% if (sort is defined and sort) %}*/
/*       url += '&sort={{ sort }}';*/
/*     {% endif %} */
/* */
/*     {% if (order is defined and not order is iterable and order) %}*/
/*     url += '&order={{ order }}';*/
/*     {% endif %} */
/*   */
/*     {% if (page is defined and page) %}*/
/*     url += '&page={{ page }}';*/
/*     {% endif %} */
/*     location = url;*/
/*   }*/
/* */
/*   $('input[name=\'filter_name\']').autocomplete({*/
/* */
/* 	'source': function(request, response) {*/
/* 		$.ajax({*/
/* 			url: 'index.php?route=catalog/wk_preordered_list/autocompleteFilter&user_token={{ user_token }}&filter_name=' +  encodeURIComponent(request),*/
/* 			dataType: 'json',*/
/* 			success: function(json) {*/
/* 				response($.map(json, function(item) {*/
/* 					return {*/
/* 						label: item['name'],*/
/* 						value: item['product_id']*/
/* 					}*/
/* 				}));*/
/* 			}*/
/* 		});*/
/* 	},*/
/* 	'select': function(item) {*/
/* 		$('input[name=\'filter_name\']').val(item['label']);*/
/* 	}*/
/* });*/
/* */
/* $('input[name=\'filter_pname\']').autocomplete({*/
/* */
/* 	'source': function(request, response) {*/
/* 		$.ajax({*/
/* 			url: 'index.php?route=catalog/wk_preordered_list/autocompleteFilter&user_token={{ user_token }}&filter_pname=' +  encodeURIComponent(request),*/
/* 			dataType: 'json',*/
/* 			success: function(json) {*/
/* 				response($.map(json, function(item) {*/
/* 					return {*/
/* 						label: item['pname'],*/
/* 						value: item['product_id']*/
/* 					}*/
/* 				}));*/
/* 			}*/
/* 		});*/
/* 	},*/
/* 	'select': function(item) {*/
/* 		$('input[name=\'filter_pname\']').val(item['label']);*/
/* 	}*/
/* });*/
/* */
/* $('input[name=\'filter_customer_mail\']').autocomplete({*/
/* */
/* 	'source': function(request, response) {*/
/* 		$.ajax({*/
/* 			url: 'index.php?route=catalog/wk_preordered_list/autocompleteFilter&user_token={{ user_token }}&filter_customer_mail=' +  encodeURIComponent(request),*/
/* 			dataType: 'json',*/
/* 			success: function(json) {*/
/* 				response($.map(json, function(item) {*/
/* 					return {*/
/* 						label: item['email'],*/
/* 						value: item['product_id']*/
/* 					}*/
/* 				}));*/
/* 			}*/
/* 		});*/
/* 	},*/
/* 	'select': function(item) {*/
/* 		$('input[name=\'filter_customer_mail\']').val(item['label']);*/
/* 	}*/
/* });*/
/* </script>*/
/* */
/* {{ footer }} */
/* */
