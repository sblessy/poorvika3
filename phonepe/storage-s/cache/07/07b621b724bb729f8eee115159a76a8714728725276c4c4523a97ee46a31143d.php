<?php

/* so-destino/template/extension/module/so_onepagecheckout/checkout/cart.twig */
class __TwigTemplate_610b6bc1ff7ad3c855fc1196fdc6ee63afff3d725866780d7265babdbbd65b06 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"checkout-content checkout-cart\">
    <h2 class=\"secondary-title\"><i class=\"fa fa-shopping-cart\"></i>";
        // line 2
        echo (isset($context["text_shopping_cart"]) ? $context["text_shopping_cart"] : null);
        echo " ";
        if ((isset($context["weight"]) ? $context["weight"] : null)) {
            echo " ";
            echo (("(" . (isset($context["weight"]) ? $context["weight"] : null)) . ")");
            echo " ";
        }
        echo "</h2>
    <div class=\"box-inner\">
        <div class=\"table-responsive checkout-product\">
            <table class=\"table table-bordered table-hover\">
                <thead>
                    <tr>
                        <th class=\"text-left name\" colspan=\"2\">";
        // line 8
        echo (isset($context["column_name"]) ? $context["column_name"] : null);
        echo "</th>
                        <th class=\"text-center quantity\">";
        // line 9
        echo (isset($context["column_quantity"]) ? $context["column_quantity"] : null);
        echo "</th>
                        <th class=\"text-center price\">";
        // line 10
        echo (isset($context["column_price"]) ? $context["column_price"] : null);
        echo "</th>
                        <th class=\"text-right total\">";
        // line 11
        echo (isset($context["column_total"]) ? $context["column_total"] : null);
        echo "</th>
                    </tr>
                </thead>
                <tbody>
                ";
        // line 15
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 16
            echo "                    <tr>
                        <td class=\"text-left name\" colspan=\"2\">
                            ";
            // line 18
            if ($this->getAttribute($context["product"], "thumb", array())) {
                // line 19
                echo "                                <a href=\"";
                echo $this->getAttribute($context["product"], "href", array());
                echo "\"><img src=\"";
                echo $this->getAttribute($context["product"], "thumb", array());
                echo "\" alt=\"";
                echo $this->getAttribute($context["product"], "name", array());
                echo "\" title=\"";
                echo $this->getAttribute($context["product"], "name", array());
                echo "\" class=\"img-thumbnail\" /></a>
                            ";
            }
            // line 21
            echo "                            <a href=\"";
            echo $this->getAttribute($context["product"], "href", array());
            echo "\" class=\"product-name\">";
            echo $this->getAttribute($context["product"], "name", array());
            echo "</a>
                            ";
            // line 22
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["product"], "option", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["option"]) {
                // line 23
                echo "                                <br/>
                                &nbsp;
                                <small> - ";
                // line 25
                echo $this->getAttribute($context["option"], "name", array());
                echo ": ";
                echo $this->getAttribute($context["option"], "value", array());
                echo "</small>
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 27
            echo "                            ";
            if ($this->getAttribute($context["product"], "recurring", array())) {
                // line 28
                echo "                                <br/>
                                <span class=\"label label-info\">";
                // line 29
                echo (isset($context["text_recurring_item"]) ? $context["text_recurring_item"] : null);
                echo "</span>
                                <small>";
                // line 30
                echo $this->getAttribute($context["product"], "recurring", array());
                echo "</small>
                            ";
            }
            // line 32
            echo "                        </td>
                        <td class=\"text-left quantity\">
                            <div class=\"input-group\">
                                <input type=\"text\" name=\"quantity[";
            // line 35
            echo $this->getAttribute($context["product"], "cart_id", array());
            echo "]\" value=\"";
            echo $this->getAttribute($context["product"], "quantity", array());
            echo "\" size=\"1\" class=\"form-control\" />
                                <span class=\"input-group-btn\">
                                    ";
            // line 37
            if ($this->getAttribute((isset($context["setting_so_onepagecheckout_layout_setting"]) ? $context["setting_so_onepagecheckout_layout_setting"] : null), "show_product_removecart", array())) {
                // line 38
                echo "                                        <span data-toggle=\"tooltip\" title=\"";
                echo (isset($context["button_remove"]) ? $context["button_remove"] : null);
                echo "\" data-product-key=\"";
                echo $this->getAttribute($context["product"], "cart_id", array());
                echo "\" class=\"btn-delete\"><i class=\"fa fa-trash-o\"></i></span>
                                    ";
            }
            // line 40
            echo "                                    ";
            if ($this->getAttribute((isset($context["setting_so_onepagecheckout_layout_setting"]) ? $context["setting_so_onepagecheckout_layout_setting"] : null), "show_product_qnty_update", array())) {
                // line 41
                echo "                                        <span data-toggle=\"tooltip\" title=\"";
                echo (isset($context["button_update"]) ? $context["button_update"] : null);
                echo "\" data-product-key=\"";
                echo $this->getAttribute($context["product"], "cart_id", array());
                echo "\" class=\"btn-update\"><i class=\"fa fa-refresh\"></i></span>
                                    ";
            }
            // line 43
            echo "                                </span>
                            </div>
                        </td>
                        <td class=\"text-right price\">";
            // line 46
            echo $this->getAttribute($context["product"], "price", array());
            echo "</td>
                        <td class=\"text-right total\">";
            // line 47
            echo $this->getAttribute($context["product"], "total", array());
            echo "</td>
                    </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 50
        echo "                ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["vouchers"]) ? $context["vouchers"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["voucher"]) {
            // line 51
            echo "                    <tr>
                        <td class=\"text-left\">";
            // line 52
            echo $this->getAttribute($context["voucher"], "description", array());
            echo "</td>
                        <td class=\"text-left\"></td>
                        <td class=\"text-right\">1</td>
                        <td class=\"text-right\">";
            // line 55
            echo $this->getAttribute($context["voucher"], "amount", array());
            echo "</td>
                        <td class=\"text-right\">";
            // line 56
            echo $this->getAttribute($context["voucher"], "amount", array());
            echo "</td>
                    </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['voucher'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 59
        echo "                </tbody>
                <tfoot>
                ";
        // line 61
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["totals"]) ? $context["totals"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["total"]) {
            // line 62
            echo "                    <tr>
                        <td colspan=\"4\" class=\"text-left\">";
            // line 63
            echo $this->getAttribute($context["total"], "title", array());
            echo ":</td>
                        <td class=\"text-right\">";
            // line 64
            echo $this->getAttribute($context["total"], "text", array());
            echo "</td>
                    </tr>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['total'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 67
        echo "                </tfoot>
            </table>
        </div>
        <div id=\"payment-confirm-button\" class=\"payment-";
        // line 70
        echo $this->getAttribute((isset($context["SoUtils"]) ? $context["SoUtils"] : null), "getProperty", array(0 => (isset($context["session_data"]) ? $context["session_data"] : null), 1 => "payment_method.code"), "method");
        echo "\">
            <h2 class=\"secondary-title\"><i class=\"fa fa-credit-card\"></i>";
        // line 71
        echo (isset($context["text_payment_detail"]) ? $context["text_payment_detail"] : null);
        echo "</h2>
            ";
        // line 72
        echo (isset($context["payment"]) ? $context["payment"] : null);
        echo "
        </div>
    </div>
</div>

";
    }

    public function getTemplateName()
    {
        return "so-destino/template/extension/module/so_onepagecheckout/checkout/cart.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  233 => 72,  229 => 71,  225 => 70,  220 => 67,  211 => 64,  207 => 63,  204 => 62,  200 => 61,  196 => 59,  187 => 56,  183 => 55,  177 => 52,  174 => 51,  169 => 50,  160 => 47,  156 => 46,  151 => 43,  143 => 41,  140 => 40,  132 => 38,  130 => 37,  123 => 35,  118 => 32,  113 => 30,  109 => 29,  106 => 28,  103 => 27,  93 => 25,  89 => 23,  85 => 22,  78 => 21,  66 => 19,  64 => 18,  60 => 16,  56 => 15,  49 => 11,  45 => 10,  41 => 9,  37 => 8,  22 => 2,  19 => 1,);
    }
}
/* <div class="checkout-content checkout-cart">*/
/*     <h2 class="secondary-title"><i class="fa fa-shopping-cart"></i>{{ text_shopping_cart }} {% if weight %} {{ '('~weight~')' }} {% endif %}</h2>*/
/*     <div class="box-inner">*/
/*         <div class="table-responsive checkout-product">*/
/*             <table class="table table-bordered table-hover">*/
/*                 <thead>*/
/*                     <tr>*/
/*                         <th class="text-left name" colspan="2">{{ column_name }}</th>*/
/*                         <th class="text-center quantity">{{ column_quantity }}</th>*/
/*                         <th class="text-center price">{{ column_price }}</th>*/
/*                         <th class="text-right total">{{ column_total }}</th>*/
/*                     </tr>*/
/*                 </thead>*/
/*                 <tbody>*/
/*                 {% for product in products %}*/
/*                     <tr>*/
/*                         <td class="text-left name" colspan="2">*/
/*                             {% if product.thumb %}*/
/*                                 <a href="{{ product.href }}"><img src="{{ product.thumb }}" alt="{{ product.name }}" title="{{ product.name }}" class="img-thumbnail" /></a>*/
/*                             {% endif %}*/
/*                             <a href="{{ product.href }}" class="product-name">{{ product.name }}</a>*/
/*                             {% for option in product.option %}*/
/*                                 <br/>*/
/*                                 &nbsp;*/
/*                                 <small> - {{ option.name }}: {{ option.value }}</small>*/
/*                             {% endfor %}*/
/*                             {% if product.recurring %}*/
/*                                 <br/>*/
/*                                 <span class="label label-info">{{ text_recurring_item }}</span>*/
/*                                 <small>{{ product.recurring }}</small>*/
/*                             {% endif %}*/
/*                         </td>*/
/*                         <td class="text-left quantity">*/
/*                             <div class="input-group">*/
/*                                 <input type="text" name="quantity[{{ product.cart_id }}]" value="{{ product.quantity }}" size="1" class="form-control" />*/
/*                                 <span class="input-group-btn">*/
/*                                     {% if setting_so_onepagecheckout_layout_setting.show_product_removecart %}*/
/*                                         <span data-toggle="tooltip" title="{{ button_remove }}" data-product-key="{{ product.cart_id }}" class="btn-delete"><i class="fa fa-trash-o"></i></span>*/
/*                                     {% endif %}*/
/*                                     {% if setting_so_onepagecheckout_layout_setting.show_product_qnty_update %}*/
/*                                         <span data-toggle="tooltip" title="{{ button_update }}" data-product-key="{{ product.cart_id }}" class="btn-update"><i class="fa fa-refresh"></i></span>*/
/*                                     {% endif %}*/
/*                                 </span>*/
/*                             </div>*/
/*                         </td>*/
/*                         <td class="text-right price">{{ product.price }}</td>*/
/*                         <td class="text-right total">{{ product.total }}</td>*/
/*                     </tr>*/
/*                 {% endfor %}*/
/*                 {% for voucher in vouchers %}*/
/*                     <tr>*/
/*                         <td class="text-left">{{ voucher.description }}</td>*/
/*                         <td class="text-left"></td>*/
/*                         <td class="text-right">1</td>*/
/*                         <td class="text-right">{{ voucher.amount }}</td>*/
/*                         <td class="text-right">{{ voucher.amount }}</td>*/
/*                     </tr>*/
/*                 {% endfor %}*/
/*                 </tbody>*/
/*                 <tfoot>*/
/*                 {% for total in totals %}*/
/*                     <tr>*/
/*                         <td colspan="4" class="text-left">{{ total.title }}:</td>*/
/*                         <td class="text-right">{{ total.text }}</td>*/
/*                     </tr>*/
/*                 {% endfor %}*/
/*                 </tfoot>*/
/*             </table>*/
/*         </div>*/
/*         <div id="payment-confirm-button" class="payment-{{ SoUtils.getProperty(session_data, 'payment_method.code') }}">*/
/*             <h2 class="secondary-title"><i class="fa fa-credit-card"></i>{{ text_payment_detail }}</h2>*/
/*             {{ payment }}*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* */
/* */
