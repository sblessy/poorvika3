<?php

/* so-destino/template/extension/module/so_onepagecheckout/checkout/address_form.twig */
class __TwigTemplate_71ec78c2d7ec2f3d75f78b9a76066db7919430334a53acbd662142b2f42fd467 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"";
        echo (((isset($context["is_logged_in"]) ? $context["is_logged_in"] : null)) ? ("checkout-content") : (""));
        echo " checkout-";
        echo (isset($context["type"]) ? $context["type"] : null);
        echo "-form\">
    ";
        // line 2
        if ((isset($context["is_logged_in"]) ? $context["is_logged_in"] : null)) {
            // line 3
            echo "        <h2 class=\"secondary-title\"><i class=\"fa fa-user\"></i>";
            echo ((((isset($context["type"]) ? $context["type"] : null) == "payment")) ? ("Billing Address") : ("Delivery Address"));
            echo " </h2>
    ";
        }
        // line 5
        echo "    <div class=\"box-inner\">
        <form class=\"form-horizontal form-";
        // line 6
        echo (isset($context["type"]) ? $context["type"] : null);
        echo "\">
            ";
        // line 7
        if ((isset($context["addresses"]) ? $context["addresses"] : null)) {
            // line 8
            echo "                <div class=\"radio\">
                    <label>
                        <input type=\"radio\" name=\"";
            // line 10
            echo (isset($context["type"]) ? $context["type"] : null);
            echo "_address\" value=\"existing\" checked=\"checked\" />
                        ";
            // line 11
            echo (isset($context["text_address_existing"]) ? $context["text_address_existing"] : null);
            echo "
                    </label>
                </div>
                <div id=\"";
            // line 14
            echo (isset($context["type"]) ? $context["type"] : null);
            echo "-existing\">
                    <select name=\"";
            // line 15
            echo (isset($context["type"]) ? $context["type"] : null);
            echo "_address_id\" class=\"form-control\">
                        ";
            // line 16
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["addresses"]) ? $context["addresses"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["address"]) {
                // line 17
                echo "                            ";
                if (($this->getAttribute($context["address"], "address_id", array()) == (isset($context["address_id"]) ? $context["address_id"] : null))) {
                    // line 18
                    echo "                                <option value=\"";
                    echo $this->getAttribute($context["address"], "address_id", array());
                    echo "\" selected=\"selected\">";
                    echo $this->getAttribute($context["address"], "firstname", array());
                    echo " ";
                    echo $this->getAttribute($context["address"], "lastname", array());
                    echo ", ";
                    echo $this->getAttribute($context["address"], "address_1", array());
                    echo ", ";
                    echo $this->getAttribute($context["address"], "city", array());
                    echo ", ";
                    echo $this->getAttribute($context["address"], "zone", array());
                    echo ", ";
                    echo $this->getAttribute($context["address"], "country", array());
                    echo "</option>
                            ";
                } else {
                    // line 20
                    echo "                                <option value=\"";
                    echo $this->getAttribute($context["address"], "address_id", array());
                    echo "\">";
                    echo $this->getAttribute($context["address"], "firstname", array());
                    echo " ";
                    echo $this->getAttribute($context["address"], "lastname", array());
                    echo ", ";
                    echo $this->getAttribute($context["address"], "address_1", array());
                    echo ", ";
                    echo $this->getAttribute($context["address"], "city", array());
                    echo ", ";
                    echo $this->getAttribute($context["address"], "zone", array());
                    echo ", ";
                    echo $this->getAttribute($context["address"], "country", array());
                    echo "</option>
                            ";
                }
                // line 22
                echo "                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['address'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 23
            echo "                    </select>
                </div>
                <div class=\"radio\">
                    <label>
                        <input type=\"radio\" name=\"";
            // line 27
            echo (isset($context["type"]) ? $context["type"] : null);
            echo "_address\" value=\"new\"/>
                        ";
            // line 28
            echo (isset($context["text_address_new"]) ? $context["text_address_new"] : null);
            echo "
                    </label>
                </div>
            ";
        }
        // line 32
        echo "            <div id=\"";
        echo (isset($context["type"]) ? $context["type"] : null);
        echo "-new\" style=\"display: ";
        echo (((isset($context["addresses"]) ? $context["addresses"] : null)) ? ("none") : ("block"));
        echo "\">
                ";
        // line 33
        if ((isset($context["name"]) ? $context["name"] : null)) {
            // line 34
            echo "                    <div class=\"form-group required\">
                        <input type=\"text\" name=\"";
            // line 35
            echo (isset($context["type"]) ? $context["type"] : null);
            echo "_firstname\" value=\"";
            echo $this->getAttribute((isset($context["SoUtils"]) ? $context["SoUtils"] : null), "getProperty", array(0 => (isset($context["order_data"]) ? $context["order_data"] : null), 1 => ((isset($context["type"]) ? $context["type"] : null) . "_firstname")), "method");
            echo " *\" placeholder=\"";
            echo (isset($context["entry_firstname"]) ? $context["entry_firstname"] : null);
            echo "\" id=\"input-";
            echo (isset($context["type"]) ? $context["type"] : null);
            echo "-firstname\" class=\"form-control\" />
                    </div>
                    <div class=\"form-group required\">
                        <input type=\"text\" name=\"";
            // line 38
            echo (isset($context["type"]) ? $context["type"] : null);
            echo "_lastname\" value=\"";
            echo $this->getAttribute((isset($context["SoUtils"]) ? $context["SoUtils"] : null), "getProperty", array(0 => (isset($context["order_data"]) ? $context["order_data"] : null), 1 => ((isset($context["type"]) ? $context["type"] : null) . "_lastname")), "method");
            echo "\" placeholder=\"";
            echo (isset($context["entry_lastname"]) ? $context["entry_lastname"] : null);
            echo " *\" id=\"input-";
            echo (isset($context["type"]) ? $context["type"] : null);
            echo "-lastname\" class=\"form-control\"/>
                    </div>
                ";
        }
        // line 41
        echo "                <div class=\"form-group company-input\">
                    <input type=\"text\" name=\"";
        // line 42
        echo (isset($context["type"]) ? $context["type"] : null);
        echo "_company\" value=\"";
        echo $this->getAttribute((isset($context["SoUtils"]) ? $context["SoUtils"] : null), "getProperty", array(0 => (isset($context["order_data"]) ? $context["order_data"] : null), 1 => ((isset($context["type"]) ? $context["type"] : null) . "_company")), "method");
        echo "\" placeholder=\"";
        echo (isset($context["entry_company"]) ? $context["entry_company"] : null);
        echo "\" id=\"input-";
        echo (isset($context["type"]) ? $context["type"] : null);
        echo "-company\" class=\"form-control\"/>
                </div>
                <div class=\"form-group required\">
                    <input type=\"text\" name=\"";
        // line 45
        echo (isset($context["type"]) ? $context["type"] : null);
        echo "_address_1\" value=\"";
        echo $this->getAttribute((isset($context["SoUtils"]) ? $context["SoUtils"] : null), "getProperty", array(0 => (isset($context["order_data"]) ? $context["order_data"] : null), 1 => ((isset($context["type"]) ? $context["type"] : null) . "_address_1")), "method");
        echo "\" placeholder=\"";
        echo (isset($context["entry_address_1"]) ? $context["entry_address_1"] : null);
        echo " *\" id=\"input-";
        echo (isset($context["type"]) ? $context["type"] : null);
        echo "-address-1\" class=\"form-control\"/>
                </div>
                <div class=\"form-group address-2-input\">
                    <input type=\"text\" name=\"";
        // line 48
        echo (isset($context["type"]) ? $context["type"] : null);
        echo "_address_2\" value=\"";
        echo $this->getAttribute((isset($context["SoUtils"]) ? $context["SoUtils"] : null), "getProperty", array(0 => (isset($context["order_data"]) ? $context["order_data"] : null), 1 => ((isset($context["type"]) ? $context["type"] : null) . "_address_2")), "method");
        echo "\" placeholder=\"";
        echo (isset($context["entry_address_2"]) ? $context["entry_address_2"] : null);
        echo "\" id=\"input-";
        echo (isset($context["type"]) ? $context["type"] : null);
        echo "-address-2\" class=\"form-control\"/>
                </div>
                <div class=\"form-group required\">
                    <input type=\"text\" name=\"";
        // line 51
        echo (isset($context["type"]) ? $context["type"] : null);
        echo "_city\" value=\"";
        echo $this->getAttribute((isset($context["SoUtils"]) ? $context["SoUtils"] : null), "getProperty", array(0 => (isset($context["order_data"]) ? $context["order_data"] : null), 1 => ((isset($context["type"]) ? $context["type"] : null) . "_city")), "method");
        echo "\" placeholder=\"";
        echo (isset($context["entry_city"]) ? $context["entry_city"] : null);
        echo " *\" id=\"input-";
        echo (isset($context["type"]) ? $context["type"] : null);
        echo "-city\" class=\"form-control\"/>
                </div>
                <div class=\"form-group required\">
                    <input type=\"text\" name=\"";
        // line 54
        echo (isset($context["type"]) ? $context["type"] : null);
        echo "_postcode\" value=\"";
        echo (isset($context["postcode"]) ? $context["postcode"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_postcode"]) ? $context["entry_postcode"] : null);
        echo " *\" id=\"input-";
        echo (isset($context["type"]) ? $context["type"] : null);
        echo "-postcode\" class=\"form-control\"/>
                </div>
                <div class=\"form-group required\">
                    <select name=\"";
        // line 57
        echo (isset($context["type"]) ? $context["type"] : null);
        echo "_country_id\" id=\"input-";
        echo (isset($context["type"]) ? $context["type"] : null);
        echo "-country\" class=\"form-control\">
                        <option value=\"\">";
        // line 58
        echo (isset($context["text_select"]) ? $context["text_select"] : null);
        echo "</option>
                        ";
        // line 59
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["countries"]) ? $context["countries"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["country"]) {
            // line 60
            echo "                            ";
            if (($this->getAttribute($context["country"], "country_id", array()) == (isset($context["country_id"]) ? $context["country_id"] : null))) {
                // line 61
                echo "                                <option value=\"";
                echo $this->getAttribute($context["country"], "country_id", array());
                echo "\" selected=\"selected\">";
                echo $this->getAttribute($context["country"], "name", array());
                echo "</option>
                            ";
            } else {
                // line 63
                echo "                                <option value=\"";
                echo $this->getAttribute($context["country"], "country_id", array());
                echo "\">";
                echo $this->getAttribute($context["country"], "name", array());
                echo "</option>
                            ";
            }
            // line 65
            echo "                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['country'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 66
        echo "                    </select>
                </div>
                <div class=\"form-group required\">
                    <select name=\"";
        // line 69
        echo (isset($context["type"]) ? $context["type"] : null);
        echo "_zone_id\" id=\"input-";
        echo (isset($context["type"]) ? $context["type"] : null);
        echo "-zone\" class=\"form-control\">
                    </select>
                </div>
                ";
        // line 72
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["custom_fields"]) ? $context["custom_fields"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["custom_field"]) {
            // line 73
            echo "                    ";
            if (($this->getAttribute($context["custom_field"], "location", array()) == "address")) {
                // line 74
                echo "                        ";
                if (($this->getAttribute($context["custom_field"], "type", array()) == "select")) {
                    // line 75
                    echo "                            <div class=\"form-group ";
                    echo (($this->getAttribute($context["custom_field"], "required", array())) ? (" required") : (""));
                    echo " custom-field\" data-sort=\"";
                    echo $this->getAttribute($context["custom_field"], "sort_order", array());
                    echo "\">
                                <label class=\"col-sm-2 control-label\" for=\"input-";
                    // line 76
                    echo (isset($context["type"]) ? $context["type"] : null);
                    echo "-custom-field";
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "\">";
                    echo $this->getAttribute($context["custom_field"], "name", array());
                    echo "</label>
                                <select name=\"";
                    // line 77
                    echo (isset($context["type"]) ? $context["type"] : null);
                    echo "_custom_field[";
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "]\"
                                        id=\"input-";
                    // line 78
                    echo (isset($context["type"]) ? $context["type"] : null);
                    echo "-custom-field";
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "\"
                                        class=\"form-control\">
                                    <option value=\"\">";
                    // line 80
                    echo (isset($context["text_select"]) ? $context["text_select"] : null);
                    echo "</option>
                                    ";
                    // line 81
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["custom_field"], "custom_field_value", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["custom_field_value"]) {
                        // line 82
                        echo "                                        <option value=\"";
                        echo $this->getAttribute($context["custom_field_value"], "custom_field_value_id", array());
                        echo "\">";
                        echo $this->getAttribute($context["custom_field_value"], "name", array());
                        echo "</option>
                                    ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['custom_field_value'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 84
                    echo "                                </select>
                            </div>
                        ";
                }
                // line 87
                echo "                        
                        ";
                // line 88
                if (($this->getAttribute($context["custom_field"], "type", array()) == "radio")) {
                    // line 89
                    echo "                            <div class=\"form-group ";
                    echo (($this->getAttribute($context["custom_field"], "required", array())) ? (" required") : (""));
                    echo " custom-field\" data-sort=\"";
                    echo $this->getAttribute($context["custom_field"], "sort_order", array());
                    echo "\">
                                <label class=\"col-sm-2 control-label\">";
                    // line 90
                    echo $this->getAttribute($context["custom_field"], "name", array());
                    echo "</label>
                                <div id=\"input-";
                    // line 91
                    echo (isset($context["type"]) ? $context["type"] : null);
                    echo "-custom-field";
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "\">
                                    ";
                    // line 92
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["custom_field"], "custom_field_value", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["custom_field_value"]) {
                        // line 93
                        echo "                                        <div class=\"radio\">
                                            <label>
                                                <input type=\"radio\"
                                                       name=\"";
                        // line 96
                        echo (isset($context["type"]) ? $context["type"] : null);
                        echo "_custom_field[";
                        echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                        echo "]\"
                                                       value=\"";
                        // line 97
                        echo $this->getAttribute($context["custom_field_value"], "custom_field_value_id", array());
                        echo "\"/>
                                                ";
                        // line 98
                        echo $this->getAttribute($context["custom_field_value"], "name", array());
                        echo "
                                            </label>
                                        </div>
                                    ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['custom_field_value'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 102
                    echo "                                </div>
                            </div>
                        ";
                }
                // line 105
                echo "
                        ";
                // line 106
                if (($this->getAttribute($context["custom_field"], "type", array()) == "checkbox")) {
                    // line 107
                    echo "                            <div class=\"form-group ";
                    echo (($this->getAttribute($context["custom_field"], "required", array())) ? (" required") : (""));
                    echo " custom-field\" data-sort=\"";
                    echo $this->getAttribute($context["custom_field"], "sort_order", array());
                    echo "\">
                                <label class=\"col-sm-2 control-label\">";
                    // line 108
                    echo $this->getAttribute($context["custom_field"], "name", array());
                    echo "</label>
                                <div id=\"input-";
                    // line 109
                    echo (isset($context["type"]) ? $context["type"] : null);
                    echo "-custom-field";
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "\">
                                    ";
                    // line 110
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["custom_field"], "custom_field_value", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["custom_field_value"]) {
                        // line 111
                        echo "                                        <div class=\"checkbox\">
                                            <label>
                                                <input type=\"checkbox\"
                                                       name=\"";
                        // line 114
                        echo (isset($context["type"]) ? $context["type"] : null);
                        echo "_custom_field[";
                        echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                        echo "][]\"
                                                       value=\"";
                        // line 115
                        echo $this->getAttribute($context["custom_field_value"], "custom_field_value_id", array());
                        echo "\"/>
                                                ";
                        // line 116
                        echo $this->getAttribute($context["custom_field_value"], "name", array());
                        echo "
                                            </label>
                                        </div>
                                    ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['custom_field_value'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 120
                    echo "                                </div>
                            </div>
                        ";
                }
                // line 123
                echo "
                        ";
                // line 124
                if (($this->getAttribute($context["custom_field"], "type", array()) == "text")) {
                    // line 125
                    echo "                            <div class=\"form-group ";
                    echo (($this->getAttribute($context["custom_field"], "required", array())) ? (" required") : (""));
                    echo " custom-field\" data-sort=\"";
                    echo $this->getAttribute($context["custom_field"], "sort_order", array());
                    echo "\">
                                <label class=\"col-sm-2 control-label\" for=\"input-";
                    // line 126
                    echo (isset($context["type"]) ? $context["type"] : null);
                    echo "-custom-field";
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "\">";
                    echo $this->getAttribute($context["custom_field"], "name", array());
                    echo "</label>
                                <input type=\"text\" name=\"";
                    // line 127
                    echo (isset($context["type"]) ? $context["type"] : null);
                    echo "_custom_field[";
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "]\"
                                       value=\"";
                    // line 128
                    echo $this->getAttribute($context["custom_field"], "value", array());
                    echo "\"
                                       placeholder=\"";
                    // line 129
                    echo $this->getAttribute($context["custom_field"], "name", array());
                    echo "\"
                                       id=\"input-";
                    // line 130
                    echo (isset($context["type"]) ? $context["type"] : null);
                    echo "-custom-field";
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "\"
                                       class=\"form-control\"/>
                            </div>
                        ";
                }
                // line 134
                echo "
                        ";
                // line 135
                if (($this->getAttribute($context["custom_field"], "type", array()) == "textarea")) {
                    // line 136
                    echo "                            <div class=\"form-group ";
                    echo (($this->getAttribute($context["custom_field"], "required", array())) ? (" required") : (""));
                    echo " custom-field\" data-sort=\"";
                    echo $this->getAttribute($context["custom_field"], "sort_order", array());
                    echo "\">
                                <label class=\"col-sm-2 control-label\" for=\"input-";
                    // line 137
                    echo (isset($context["type"]) ? $context["type"] : null);
                    echo "-custom-field";
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "\">";
                    echo $this->getAttribute($context["custom_field"], "name", array());
                    echo "</label>
                                <textarea name=\"";
                    // line 138
                    echo (isset($context["type"]) ? $context["type"] : null);
                    echo "_custom_field[";
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "]\" rows=\"5\"
                                          placeholder=\"";
                    // line 139
                    echo $this->getAttribute($context["custom_field"], "name", array());
                    echo "\"
                                          id=\"input-";
                    // line 140
                    echo (isset($context["type"]) ? $context["type"] : null);
                    echo "-custom-field";
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "\"
                                          class=\"form-control\">";
                    // line 141
                    echo $this->getAttribute($context["custom_field"], "value", array());
                    echo "</textarea>
                            </div>
                        ";
                }
                // line 144
                echo "
                        ";
                // line 145
                if (($this->getAttribute($context["custom_field"], "type", array()) == "file")) {
                    // line 146
                    echo "                            <div class=\"form-group ";
                    echo (($this->getAttribute($context["custom_field"], "required", array())) ? (" required") : (""));
                    echo " custom-field\" data-sort=\"";
                    echo $this->getAttribute($context["custom_field"], "sort_order", array());
                    echo "\">
                                <label class=\"col-sm-2 control-label\">";
                    // line 147
                    echo $this->getAttribute($context["custom_field"], "name", array());
                    echo "</label>
                                <button type=\"button\"
                                        id=\"button-";
                    // line 149
                    echo (isset($context["type"]) ? $context["type"] : null);
                    echo "-custom-field";
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "\"
                                        data-loading-text=\"";
                    // line 150
                    echo (isset($context["text_loading"]) ? $context["text_loading"] : null);
                    echo "\" class=\"btn btn-default\"><i
                                        class=\"fa fa-upload\"></i> ";
                    // line 151
                    echo (isset($context["button_upload"]) ? $context["button_upload"] : null);
                    echo "</button>
                                <input type=\"hidden\" name=\"";
                    // line 152
                    echo (isset($context["type"]) ? $context["type"] : null);
                    echo "_custom_field[";
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "]\"
                                       value=\"\"
                                       id=\"input-";
                    // line 154
                    echo (isset($context["type"]) ? $context["type"] : null);
                    echo "-custom-field";
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "\"/>
                            </div>
                        ";
                }
                // line 157
                echo "
                        ";
                // line 158
                if (($this->getAttribute($context["custom_field"], "type", array()) == "date")) {
                    // line 159
                    echo "                            <div class=\"form-group ";
                    echo (($this->getAttribute($context["custom_field"], "required", array())) ? (" required") : (""));
                    echo " custom-field\" data-sort=\"";
                    echo $this->getAttribute($context["custom_field"], "sort_order", array());
                    echo "\">
                                <label class=\"col-sm-2 control-label\" for=\"input-";
                    // line 160
                    echo (isset($context["type"]) ? $context["type"] : null);
                    echo "-custom-field";
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "\">";
                    echo $this->getAttribute($context["custom_field"], "name", array());
                    echo "</label>
                                <div class=\"input-group date\">
                                    <input type=\"text\" name=\"";
                    // line 162
                    echo (isset($context["type"]) ? $context["type"] : null);
                    echo "_custom_field[";
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "]\"
                                        value=\"";
                    // line 163
                    echo $this->getAttribute($context["custom_field"], "value", array());
                    echo "\"
                                        placeholder=\"";
                    // line 164
                    echo $this->getAttribute($context["custom_field"], "name", array());
                    echo "\" data-date-format=\"YYYY-MM-DD\"
                                        id=\"input-";
                    // line 165
                    echo (isset($context["type"]) ? $context["type"] : null);
                    echo "-custom-field";
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "\"
                                        class=\"form-control\"/>
                                    <span class=\"input-group-btn\">
                                        <button type=\"button\" class=\"btn btn-default\"><i class=\"fa fa-calendar\"></i></button>
                                    </span>
                                </div>
                            </div>
                        ";
                }
                // line 173
                echo "
                        ";
                // line 174
                if (($this->getAttribute($context["custom_field"], "type", array()) == "time")) {
                    // line 175
                    echo "                            <div class=\"form-group ";
                    echo (($this->getAttribute($context["custom_field"], "required", array())) ? (" required") : (""));
                    echo " custom-field\" data-sort=\"";
                    echo $this->getAttribute($context["custom_field"], "sort_order", array());
                    echo "\">
                                <label class=\"col-sm-2 control-label\" for=\"input-";
                    // line 176
                    echo (isset($context["type"]) ? $context["type"] : null);
                    echo "-custom-field";
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "\">";
                    echo $this->getAttribute($context["custom_field"], "name", array());
                    echo "</label>
                                <div class=\"input-group time\">
                                    <input type=\"text\" name=\"";
                    // line 178
                    echo (isset($context["type"]) ? $context["type"] : null);
                    echo "_custom_field[";
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "]\"
                                        value=\"";
                    // line 179
                    echo $this->getAttribute($context["custom_field"], "value", array());
                    echo "\"
                                        placeholder=\"";
                    // line 180
                    echo $this->getAttribute($context["custom_field"], "name", array());
                    echo "\" data-date-format=\"HH:mm\"
                                        id=\"input-";
                    // line 181
                    echo (isset($context["type"]) ? $context["type"] : null);
                    echo "-custom-field";
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "\"
                                        class=\"form-control\"/>
                                    <span class=\"input-group-btn\">
                                        <button type=\"button\" class=\"btn btn-default\"><i class=\"fa fa-calendar\"></i></button>
                                    </span>
                                </div>
                            </div>
                        ";
                }
                // line 189
                echo "
                        ";
                // line 190
                if (($this->getAttribute($context["custom_field"], "type", array()) == "datetime")) {
                    // line 191
                    echo "                            <div class=\"form-group ";
                    echo (($this->getAttribute($context["custom_field"], "required", array())) ? (" required") : (""));
                    echo " custom-field\" data-sort=\"";
                    echo $this->getAttribute($context["custom_field"], "sort_order", array());
                    echo "\">
                                <label class=\"col-sm-2 control-label\" for=\"input-";
                    // line 192
                    echo (isset($context["type"]) ? $context["type"] : null);
                    echo "-custom-field";
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "\">";
                    echo $this->getAttribute($context["custom_field"], "name", array());
                    echo "</label>
                                <div class=\"input-group datetime\">
                                    <input type=\"text\" name=\"";
                    // line 194
                    echo (isset($context["type"]) ? $context["type"] : null);
                    echo "_custom_field[";
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "]\"
                                        value=\"";
                    // line 195
                    echo $this->getAttribute($context["custom_field"], "value", array());
                    echo "\"
                                        placeholder=\"";
                    // line 196
                    echo $this->getAttribute($context["custom_field"], "name", array());
                    echo "\"
                                        data-date-format=\"YYYY-MM-DD HH:mm\"
                                        id=\"input-";
                    // line 198
                    echo (isset($context["type"]) ? $context["type"] : null);
                    echo "-custom-field";
                    echo $this->getAttribute($context["custom_field"], "custom_field_id", array());
                    echo "\"
                                        class=\"form-control\"/>
                                    <span class=\"input-group-btn\">
                                        <button type=\"button\" class=\"btn btn-default\"><i class=\"fa fa-calendar\"></i></button>
                                    </span>
                                </div>
                            </div>
                        ";
                }
                // line 206
                echo "                    ";
            }
            // line 207
            echo "                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['custom_field'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 208
        echo "            </div>
        </form>
        <script type=\"text/javascript\">
            \$('.so-onepagecheckout form.form-";
        // line 211
        echo (isset($context["type"]) ? $context["type"] : null);
        echo " input[name=\"";
        echo (isset($context["type"]) ? $context["type"] : null);
        echo "_address\"]').change(function(){
                if (this.value == 'new') {
                    \$('#";
        // line 213
        echo (isset($context["type"]) ? $context["type"] : null);
        echo "-existing').hide();
                    \$('#";
        // line 214
        echo (isset($context["type"]) ? $context["type"] : null);
        echo "-new').show().find('input[type=\"text\"]').val('');
                } else {
                    \$('#";
        // line 216
        echo (isset($context["type"]) ? $context["type"] : null);
        echo "-existing').show();
                    \$('#";
        // line 217
        echo (isset($context["type"]) ? $context["type"] : null);
        echo "-new').hide();
                }
                \$(document).trigger('so_checkout_address_changed', '";
        // line 219
        echo (isset($context["type"]) ? $context["type"] : null);
        echo "');
            });

            \$('.so-onepagecheckout form.form-";
        // line 222
        echo (isset($context["type"]) ? $context["type"] : null);
        echo " select[name=\\'";
        echo (isset($context["type"]) ? $context["type"] : null);
        echo "_country_id\\']').on('change', function(e, first) {
                if (!this.value) return;
                \$.ajax({
                    url: 'index.php?route=checkout/checkout/country&country_id=' + this.value,
                    dataType: 'json',
                    beforeSend: function() {
                        \$('.so-onepagecheckout form.form-";
        // line 228
        echo (isset($context["type"]) ? $context["type"] : null);
        echo " select[name=\\'";
        echo (isset($context["type"]) ? $context["type"] : null);
        echo "_country_id\\']').after(' <i class=\"fa fa-circle-o-notch fa-spin\"></i>');
                    },
                    complete: function() {
                        \$('.fa-spin').remove();
                    },
                    success: function(json) {
                        \$('.fa-spin').remove();

                        if (json['postcode_required'] == '1') {
                            \$('.so-onepagecheckout form.form-";
        // line 237
        echo (isset($context["type"]) ? $context["type"] : null);
        echo " input[name=\\'";
        echo (isset($context["type"]) ? $context["type"] : null);
        echo "_postcode\\']').parent().addClass('required');
                        } else {
                            \$('.so-onepagecheckout form.form-";
        // line 239
        echo (isset($context["type"]) ? $context["type"] : null);
        echo " input[name=\\'";
        echo (isset($context["type"]) ? $context["type"] : null);
        echo "_postcode\\']').parent().removeClass('required');
                        }

                        html = '<option value=\"\"> --- ";
        // line 242
        echo (isset($context["text_select"]) ? $context["text_select"] : null);
        echo " --- </option>';

                        if (json['zone'] != '') {
                            for (i = 0; i < json['zone'].length; i++) {
                                html += '<option value=\"' + json['zone'][i]['zone_id'] + '\"';

                                if (json['zone'][i]['zone_id'] == '";
        // line 248
        echo (isset($context["zone_id"]) ? $context["zone_id"] : null);
        echo "') {
                                    html += ' selected=\"selected\"';
                                }

                                html += '>' + json['zone'][i]['name'] + '</option>';
                            }
                        } else {
                            html += '<option value=\"0\" selected=\"selected\">";
        // line 255
        echo (isset($context["text_none"]) ? $context["text_none"] : null);
        echo "</option>';
                        }

                        \$('.so-onepagecheckout form.form-";
        // line 258
        echo (isset($context["type"]) ? $context["type"] : null);
        echo " select[name=\\'";
        echo (isset($context["type"]) ? $context["type"] : null);
        echo "_zone_id\\']').html(html);

                        if (!first) {
                            \$(document).trigger('so_checkout_address_changed', '";
        // line 261
        echo (isset($context["type"]) ? $context["type"] : null);
        echo "');
                        }
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
                    }
                });
            }).trigger('change', true);

            \$('.so-onepagecheckout form.form-";
        // line 270
        echo (isset($context["type"]) ? $context["type"] : null);
        echo " select[name=\\'";
        echo (isset($context["type"]) ? $context["type"] : null);
        echo "_zone_id\\']').on('change', function() {
                \$(document).trigger('so_checkout_address_changed', '";
        // line 271
        echo (isset($context["type"]) ? $context["type"] : null);
        echo "');
            });

            \$('.so-onepagecheckout form.form-";
        // line 274
        echo (isset($context["type"]) ? $context["type"] : null);
        echo " select[name=\\'";
        echo (isset($context["type"]) ? $context["type"] : null);
        echo "_address_id\\']').on('change', function() {
                \$(document).trigger('so_checkout_address_changed', '";
        // line 275
        echo (isset($context["type"]) ? $context["type"] : null);
        echo "');
            });

            var timeout_";
        // line 278
        echo (isset($context["type"]) ? $context["type"] : null);
        echo " = null;
            \$('.so-onepagecheckout form.form-";
        // line 279
        echo (isset($context["type"]) ? $context["type"] : null);
        echo " input[name=\\'";
        echo (isset($context["type"]) ? $context["type"] : null);
        echo "_postcode\\']').on('keydown', function() {
                if (timeout_";
        // line 280
        echo (isset($context["type"]) ? $context["type"] : null);
        echo ") {
                    clearTimeout(timeout_";
        // line 281
        echo (isset($context["type"]) ? $context["type"] : null);
        echo ");
                }
                timeout_";
        // line 283
        echo (isset($context["type"]) ? $context["type"] : null);
        echo " = setTimeout(function () {
                    \$(document).trigger('so_checkout_address_changed', '";
        // line 284
        echo (isset($context["type"]) ? $context["type"] : null);
        echo "');
                }, 500);
            });
        </script> 
    </div>
</div>
<input type=\"hidden\" name=\"default_zone_id\" id=\"default_zone_id\" value=\"";
        // line 290
        echo (isset($context["zone_id"]) ? $context["zone_id"] : null);
        echo "\" />";
    }

    public function getTemplateName()
    {
        return "so-destino/template/extension/module/so_onepagecheckout/checkout/address_form.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  918 => 290,  909 => 284,  905 => 283,  900 => 281,  896 => 280,  890 => 279,  886 => 278,  880 => 275,  874 => 274,  868 => 271,  862 => 270,  850 => 261,  842 => 258,  836 => 255,  826 => 248,  817 => 242,  809 => 239,  802 => 237,  788 => 228,  777 => 222,  771 => 219,  766 => 217,  762 => 216,  757 => 214,  753 => 213,  746 => 211,  741 => 208,  735 => 207,  732 => 206,  719 => 198,  714 => 196,  710 => 195,  704 => 194,  695 => 192,  688 => 191,  686 => 190,  683 => 189,  670 => 181,  666 => 180,  662 => 179,  656 => 178,  647 => 176,  640 => 175,  638 => 174,  635 => 173,  622 => 165,  618 => 164,  614 => 163,  608 => 162,  599 => 160,  592 => 159,  590 => 158,  587 => 157,  579 => 154,  572 => 152,  568 => 151,  564 => 150,  558 => 149,  553 => 147,  546 => 146,  544 => 145,  541 => 144,  535 => 141,  529 => 140,  525 => 139,  519 => 138,  511 => 137,  504 => 136,  502 => 135,  499 => 134,  490 => 130,  486 => 129,  482 => 128,  476 => 127,  468 => 126,  461 => 125,  459 => 124,  456 => 123,  451 => 120,  441 => 116,  437 => 115,  431 => 114,  426 => 111,  422 => 110,  416 => 109,  412 => 108,  405 => 107,  403 => 106,  400 => 105,  395 => 102,  385 => 98,  381 => 97,  375 => 96,  370 => 93,  366 => 92,  360 => 91,  356 => 90,  349 => 89,  347 => 88,  344 => 87,  339 => 84,  328 => 82,  324 => 81,  320 => 80,  313 => 78,  307 => 77,  299 => 76,  292 => 75,  289 => 74,  286 => 73,  282 => 72,  274 => 69,  269 => 66,  263 => 65,  255 => 63,  247 => 61,  244 => 60,  240 => 59,  236 => 58,  230 => 57,  218 => 54,  206 => 51,  194 => 48,  182 => 45,  170 => 42,  167 => 41,  155 => 38,  143 => 35,  140 => 34,  138 => 33,  131 => 32,  124 => 28,  120 => 27,  114 => 23,  108 => 22,  90 => 20,  72 => 18,  69 => 17,  65 => 16,  61 => 15,  57 => 14,  51 => 11,  47 => 10,  43 => 8,  41 => 7,  37 => 6,  34 => 5,  28 => 3,  26 => 2,  19 => 1,);
    }
}
/* <div class="{{ is_logged_in ? 'checkout-content' : '' }} checkout-{{ type }}-form">*/
/*     {% if is_logged_in %}*/
/*         <h2 class="secondary-title"><i class="fa fa-user"></i>{{ type == 'payment' ? 'Billing Address' : 'Delivery Address' }} </h2>*/
/*     {% endif %}*/
/*     <div class="box-inner">*/
/*         <form class="form-horizontal form-{{ type }}">*/
/*             {% if addresses %}*/
/*                 <div class="radio">*/
/*                     <label>*/
/*                         <input type="radio" name="{{ type }}_address" value="existing" checked="checked" />*/
/*                         {{ text_address_existing }}*/
/*                     </label>*/
/*                 </div>*/
/*                 <div id="{{ type }}-existing">*/
/*                     <select name="{{ type }}_address_id" class="form-control">*/
/*                         {% for address in addresses %}*/
/*                             {% if address.address_id == address_id %}*/
/*                                 <option value="{{ address.address_id }}" selected="selected">{{ address.firstname }} {{ address.lastname }}, {{ address.address_1 }}, {{ address.city }}, {{ address.zone }}, {{ address.country }}</option>*/
/*                             {% else %}*/
/*                                 <option value="{{ address.address_id }}">{{ address.firstname }} {{ address.lastname }}, {{ address.address_1 }}, {{ address.city }}, {{ address.zone }}, {{ address.country }}</option>*/
/*                             {% endif %}*/
/*                         {% endfor %}*/
/*                     </select>*/
/*                 </div>*/
/*                 <div class="radio">*/
/*                     <label>*/
/*                         <input type="radio" name="{{ type }}_address" value="new"/>*/
/*                         {{ text_address_new }}*/
/*                     </label>*/
/*                 </div>*/
/*             {% endif %}*/
/*             <div id="{{ type }}-new" style="display: {{ addresses ? 'none' : 'block' }}">*/
/*                 {% if name %}*/
/*                     <div class="form-group required">*/
/*                         <input type="text" name="{{ type }}_firstname" value="{{ SoUtils.getProperty(order_data, type ~ '_firstname') }} *" placeholder="{{ entry_firstname }}" id="input-{{ type }}-firstname" class="form-control" />*/
/*                     </div>*/
/*                     <div class="form-group required">*/
/*                         <input type="text" name="{{ type }}_lastname" value="{{ SoUtils.getProperty(order_data, type ~ '_lastname') }}" placeholder="{{ entry_lastname }} *" id="input-{{ type }}-lastname" class="form-control"/>*/
/*                     </div>*/
/*                 {% endif %}*/
/*                 <div class="form-group company-input">*/
/*                     <input type="text" name="{{ type }}_company" value="{{ SoUtils.getProperty(order_data, type ~ '_company') }}" placeholder="{{ entry_company }}" id="input-{{ type }}-company" class="form-control"/>*/
/*                 </div>*/
/*                 <div class="form-group required">*/
/*                     <input type="text" name="{{ type }}_address_1" value="{{ SoUtils.getProperty(order_data, type ~ '_address_1') }}" placeholder="{{ entry_address_1 }} *" id="input-{{ type }}-address-1" class="form-control"/>*/
/*                 </div>*/
/*                 <div class="form-group address-2-input">*/
/*                     <input type="text" name="{{ type }}_address_2" value="{{ SoUtils.getProperty(order_data, type ~ '_address_2') }}" placeholder="{{ entry_address_2 }}" id="input-{{ type }}-address-2" class="form-control"/>*/
/*                 </div>*/
/*                 <div class="form-group required">*/
/*                     <input type="text" name="{{ type }}_city" value="{{ SoUtils.getProperty(order_data, type ~ '_city') }}" placeholder="{{ entry_city }} *" id="input-{{ type }}-city" class="form-control"/>*/
/*                 </div>*/
/*                 <div class="form-group required">*/
/*                     <input type="text" name="{{ type }}_postcode" value="{{ postcode }}" placeholder="{{ entry_postcode }} *" id="input-{{ type }}-postcode" class="form-control"/>*/
/*                 </div>*/
/*                 <div class="form-group required">*/
/*                     <select name="{{ type }}_country_id" id="input-{{ type }}-country" class="form-control">*/
/*                         <option value="">{{ text_select }}</option>*/
/*                         {% for country in countries %}*/
/*                             {% if country.country_id == country_id %}*/
/*                                 <option value="{{ country.country_id }}" selected="selected">{{ country.name }}</option>*/
/*                             {% else %}*/
/*                                 <option value="{{ country.country_id }}">{{ country.name }}</option>*/
/*                             {% endif %}*/
/*                         {% endfor %}*/
/*                     </select>*/
/*                 </div>*/
/*                 <div class="form-group required">*/
/*                     <select name="{{ type }}_zone_id" id="input-{{ type }}-zone" class="form-control">*/
/*                     </select>*/
/*                 </div>*/
/*                 {% for custom_field in custom_fields %}*/
/*                     {% if custom_field.location == 'address' %}*/
/*                         {% if custom_field.type == 'select' %}*/
/*                             <div class="form-group {{custom_field.required ? ' required' : '' }} custom-field" data-sort="{{ custom_field.sort_order }}">*/
/*                                 <label class="col-sm-2 control-label" for="input-{{ type }}-custom-field{{ custom_field.custom_field_id }}">{{ custom_field.name }}</label>*/
/*                                 <select name="{{ type }}_custom_field[{{ custom_field.custom_field_id }}]"*/
/*                                         id="input-{{ type }}-custom-field{{ custom_field.custom_field_id }}"*/
/*                                         class="form-control">*/
/*                                     <option value="">{{ text_select }}</option>*/
/*                                     {% for custom_field_value in custom_field.custom_field_value %}*/
/*                                         <option value="{{ custom_field_value.custom_field_value_id }}">{{ custom_field_value.name }}</option>*/
/*                                     {% endfor %}*/
/*                                 </select>*/
/*                             </div>*/
/*                         {% endif %}*/
/*                         */
/*                         {% if custom_field.type == 'radio' %}*/
/*                             <div class="form-group {{ custom_field.required ? ' required' : '' }} custom-field" data-sort="{{ custom_field.sort_order }}">*/
/*                                 <label class="col-sm-2 control-label">{{ custom_field.name }}</label>*/
/*                                 <div id="input-{{ type }}-custom-field{{ custom_field.custom_field_id }}">*/
/*                                     {% for custom_field_value in custom_field.custom_field_value %}*/
/*                                         <div class="radio">*/
/*                                             <label>*/
/*                                                 <input type="radio"*/
/*                                                        name="{{ type }}_custom_field[{{ custom_field.custom_field_id }}]"*/
/*                                                        value="{{ custom_field_value.custom_field_value_id }}"/>*/
/*                                                 {{ custom_field_value.name }}*/
/*                                             </label>*/
/*                                         </div>*/
/*                                     {% endfor %}*/
/*                                 </div>*/
/*                             </div>*/
/*                         {% endif %}*/
/* */
/*                         {% if custom_field.type == 'checkbox' %}*/
/*                             <div class="form-group {{ custom_field.required ? ' required' : '' }} custom-field" data-sort="{{ custom_field.sort_order }}">*/
/*                                 <label class="col-sm-2 control-label">{{ custom_field.name }}</label>*/
/*                                 <div id="input-{{ type }}-custom-field{{ custom_field.custom_field_id }}">*/
/*                                     {% for custom_field_value in custom_field.custom_field_value %}*/
/*                                         <div class="checkbox">*/
/*                                             <label>*/
/*                                                 <input type="checkbox"*/
/*                                                        name="{{ type }}_custom_field[{{ custom_field.custom_field_id }}][]"*/
/*                                                        value="{{ custom_field_value.custom_field_value_id }}"/>*/
/*                                                 {{ custom_field_value.name }}*/
/*                                             </label>*/
/*                                         </div>*/
/*                                     {% endfor %}*/
/*                                 </div>*/
/*                             </div>*/
/*                         {% endif %}*/
/* */
/*                         {% if custom_field.type == 'text' %}*/
/*                             <div class="form-group {{ custom_field.required ? ' required' : '' }} custom-field" data-sort="{{ custom_field.sort_order }}">*/
/*                                 <label class="col-sm-2 control-label" for="input-{{ type }}-custom-field{{ custom_field.custom_field_id }}">{{ custom_field.name }}</label>*/
/*                                 <input type="text" name="{{ type }}_custom_field[{{ custom_field.custom_field_id }}]"*/
/*                                        value="{{ custom_field.value }}"*/
/*                                        placeholder="{{ custom_field.name }}"*/
/*                                        id="input-{{ type }}-custom-field{{ custom_field.custom_field_id }}"*/
/*                                        class="form-control"/>*/
/*                             </div>*/
/*                         {% endif %}*/
/* */
/*                         {% if custom_field.type == 'textarea' %}*/
/*                             <div class="form-group {{ custom_field.required ? ' required' : '' }} custom-field" data-sort="{{ custom_field.sort_order }}">*/
/*                                 <label class="col-sm-2 control-label" for="input-{{ type }}-custom-field{{ custom_field.custom_field_id }}">{{ custom_field.name }}</label>*/
/*                                 <textarea name="{{ type }}_custom_field[{{ custom_field.custom_field_id }}]" rows="5"*/
/*                                           placeholder="{{ custom_field.name }}"*/
/*                                           id="input-{{ type }}-custom-field{{ custom_field.custom_field_id }}"*/
/*                                           class="form-control">{{ custom_field.value }}</textarea>*/
/*                             </div>*/
/*                         {% endif %}*/
/* */
/*                         {% if custom_field.type == 'file' %}*/
/*                             <div class="form-group {{ custom_field.required ? ' required' : '' }} custom-field" data-sort="{{ custom_field.sort_order }}">*/
/*                                 <label class="col-sm-2 control-label">{{ custom_field.name }}</label>*/
/*                                 <button type="button"*/
/*                                         id="button-{{ type }}-custom-field{{ custom_field.custom_field_id }}"*/
/*                                         data-loading-text="{{ text_loading }}" class="btn btn-default"><i*/
/*                                         class="fa fa-upload"></i> {{ button_upload }}</button>*/
/*                                 <input type="hidden" name="{{ type }}_custom_field[{{ custom_field.custom_field_id }}]"*/
/*                                        value=""*/
/*                                        id="input-{{ type }}-custom-field{{ custom_field.custom_field_id }}"/>*/
/*                             </div>*/
/*                         {% endif %}*/
/* */
/*                         {% if custom_field.type == 'date' %}*/
/*                             <div class="form-group {{ custom_field.required ? ' required' : '' }} custom-field" data-sort="{{ custom_field.sort_order }}">*/
/*                                 <label class="col-sm-2 control-label" for="input-{{ type }}-custom-field{{ custom_field.custom_field_id }}">{{ custom_field.name }}</label>*/
/*                                 <div class="input-group date">*/
/*                                     <input type="text" name="{{ type }}_custom_field[{{ custom_field.custom_field_id }}]"*/
/*                                         value="{{ custom_field.value }}"*/
/*                                         placeholder="{{ custom_field.name }}" data-date-format="YYYY-MM-DD"*/
/*                                         id="input-{{ type }}-custom-field{{ custom_field.custom_field_id }}"*/
/*                                         class="form-control"/>*/
/*                                     <span class="input-group-btn">*/
/*                                         <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>*/
/*                                     </span>*/
/*                                 </div>*/
/*                             </div>*/
/*                         {% endif %}*/
/* */
/*                         {% if custom_field.type == 'time' %}*/
/*                             <div class="form-group {{ custom_field.required ? ' required' : '' }} custom-field" data-sort="{{ custom_field.sort_order }}">*/
/*                                 <label class="col-sm-2 control-label" for="input-{{ type }}-custom-field{{ custom_field.custom_field_id }}">{{ custom_field.name }}</label>*/
/*                                 <div class="input-group time">*/
/*                                     <input type="text" name="{{ type }}_custom_field[{{ custom_field.custom_field_id }}]"*/
/*                                         value="{{ custom_field.value }}"*/
/*                                         placeholder="{{ custom_field.name }}" data-date-format="HH:mm"*/
/*                                         id="input-{{ type }}-custom-field{{ custom_field.custom_field_id }}"*/
/*                                         class="form-control"/>*/
/*                                     <span class="input-group-btn">*/
/*                                         <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>*/
/*                                     </span>*/
/*                                 </div>*/
/*                             </div>*/
/*                         {% endif %}*/
/* */
/*                         {% if custom_field.type == 'datetime' %}*/
/*                             <div class="form-group {{ custom_field.required ? ' required' : '' }} custom-field" data-sort="{{ custom_field.sort_order }}">*/
/*                                 <label class="col-sm-2 control-label" for="input-{{ type }}-custom-field{{ custom_field.custom_field_id }}">{{ custom_field.name }}</label>*/
/*                                 <div class="input-group datetime">*/
/*                                     <input type="text" name="{{ type }}_custom_field[{{ custom_field.custom_field_id }}]"*/
/*                                         value="{{ custom_field.value }}"*/
/*                                         placeholder="{{ custom_field.name }}"*/
/*                                         data-date-format="YYYY-MM-DD HH:mm"*/
/*                                         id="input-{{ type }}-custom-field{{ custom_field.custom_field_id }}"*/
/*                                         class="form-control"/>*/
/*                                     <span class="input-group-btn">*/
/*                                         <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>*/
/*                                     </span>*/
/*                                 </div>*/
/*                             </div>*/
/*                         {% endif %}*/
/*                     {% endif %}*/
/*                 {% endfor %}*/
/*             </div>*/
/*         </form>*/
/*         <script type="text/javascript">*/
/*             $('.so-onepagecheckout form.form-{{ type }} input[name="{{ type }}_address"]').change(function(){*/
/*                 if (this.value == 'new') {*/
/*                     $('#{{ type }}-existing').hide();*/
/*                     $('#{{ type }}-new').show().find('input[type="text"]').val('');*/
/*                 } else {*/
/*                     $('#{{ type }}-existing').show();*/
/*                     $('#{{ type }}-new').hide();*/
/*                 }*/
/*                 $(document).trigger('so_checkout_address_changed', '{{ type }}');*/
/*             });*/
/* */
/*             $('.so-onepagecheckout form.form-{{ type }} select[name=\'{{ type }}_country_id\']').on('change', function(e, first) {*/
/*                 if (!this.value) return;*/
/*                 $.ajax({*/
/*                     url: 'index.php?route=checkout/checkout/country&country_id=' + this.value,*/
/*                     dataType: 'json',*/
/*                     beforeSend: function() {*/
/*                         $('.so-onepagecheckout form.form-{{ type }} select[name=\'{{ type }}_country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');*/
/*                     },*/
/*                     complete: function() {*/
/*                         $('.fa-spin').remove();*/
/*                     },*/
/*                     success: function(json) {*/
/*                         $('.fa-spin').remove();*/
/* */
/*                         if (json['postcode_required'] == '1') {*/
/*                             $('.so-onepagecheckout form.form-{{ type }} input[name=\'{{ type }}_postcode\']').parent().addClass('required');*/
/*                         } else {*/
/*                             $('.so-onepagecheckout form.form-{{ type }} input[name=\'{{ type }}_postcode\']').parent().removeClass('required');*/
/*                         }*/
/* */
/*                         html = '<option value=""> --- {{ text_select }} --- </option>';*/
/* */
/*                         if (json['zone'] != '') {*/
/*                             for (i = 0; i < json['zone'].length; i++) {*/
/*                                 html += '<option value="' + json['zone'][i]['zone_id'] + '"';*/
/* */
/*                                 if (json['zone'][i]['zone_id'] == '{{ zone_id }}') {*/
/*                                     html += ' selected="selected"';*/
/*                                 }*/
/* */
/*                                 html += '>' + json['zone'][i]['name'] + '</option>';*/
/*                             }*/
/*                         } else {*/
/*                             html += '<option value="0" selected="selected">{{ text_none }}</option>';*/
/*                         }*/
/* */
/*                         $('.so-onepagecheckout form.form-{{ type }} select[name=\'{{ type }}_zone_id\']').html(html);*/
/* */
/*                         if (!first) {*/
/*                             $(document).trigger('so_checkout_address_changed', '{{ type }}');*/
/*                         }*/
/*                     },*/
/*                     error: function(xhr, ajaxOptions, thrownError) {*/
/*                         alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*                     }*/
/*                 });*/
/*             }).trigger('change', true);*/
/* */
/*             $('.so-onepagecheckout form.form-{{ type }} select[name=\'{{ type }}_zone_id\']').on('change', function() {*/
/*                 $(document).trigger('so_checkout_address_changed', '{{ type }}');*/
/*             });*/
/* */
/*             $('.so-onepagecheckout form.form-{{ type }} select[name=\'{{ type }}_address_id\']').on('change', function() {*/
/*                 $(document).trigger('so_checkout_address_changed', '{{ type }}');*/
/*             });*/
/* */
/*             var timeout_{{ type }} = null;*/
/*             $('.so-onepagecheckout form.form-{{ type }} input[name=\'{{ type }}_postcode\']').on('keydown', function() {*/
/*                 if (timeout_{{ type }}) {*/
/*                     clearTimeout(timeout_{{ type }});*/
/*                 }*/
/*                 timeout_{{ type }} = setTimeout(function () {*/
/*                     $(document).trigger('so_checkout_address_changed', '{{ type }}');*/
/*                 }, 500);*/
/*             });*/
/*         </script> */
/*     </div>*/
/* </div>*/
/* <input type="hidden" name="default_zone_id" id="default_zone_id" value="{{ zone_id }}" />*/
