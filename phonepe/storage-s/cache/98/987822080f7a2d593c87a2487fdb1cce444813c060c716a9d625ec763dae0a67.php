<?php

/* extension/module/account_picture.twig */
class __TwigTemplate_6982a1796ca7f7ffebd80b1c1b67d7c53005c5bd0a14ec6b148dee61de491441 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo "
<style>
#ah-logo {
    max-height:40px;
}

#ah-module-footer {
    background-color:#00428a; 
    color:#fff; 
    font-size:13px; 
    padding: 20px;
}
</style>
";
        // line 14
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\">
        <button type=\"submit\" form=\"form-module\" data-toggle=\"tooltip\" title=\"";
        // line 19
        echo (isset($context["button_save"]) ? $context["button_save"] : null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i></button>
        <a href=\"";
        // line 20
        echo (isset($context["cancel"]) ? $context["cancel"] : null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo (isset($context["button_cancel"]) ? $context["button_cancel"] : null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
      <h1>";
        // line 21
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
      <ul class=\"breadcrumb\">
        ";
        // line 23
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 24
            echo "        <li><a href=\"";
            echo $this->getAttribute($context["breadcrumb"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array());
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 26
        echo "      </ul>
    </div>
  </div>
  <div class=\"container-fluid\">
    ";
        // line 30
        if ((isset($context["error_warning"]) ? $context["error_warning"] : null)) {
            // line 31
            echo "    <div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo (isset($context["error_warning"]) ? $context["error_warning"] : null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 35
        echo "    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <a href=\"http://www.anyhowinfo.com\" target=\"_blank\"><img id=\"ah-logo\" class=\"img-responsive\" src=\"";
        // line 37
        echo (isset($context["anyhow_logo"]) ? $context["anyhow_logo"] : null);
        echo "\" alt=\"Anyhow Infosystems\"></a>
      </div>
      <div class=\"panel-body\">
        <form action=\"";
        // line 40
        echo (isset($context["action"]) ? $context["action"] : null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-module\" class=\"form-horizontal\">
          <div class=\"form-group\">
              <label class=\"col-sm-2 control-label\" for=\"input-status\">";
        // line 42
        echo (isset($context["entry_status"]) ? $context["entry_status"] : null);
        echo "</label>
              <div class=\"col-sm-10\">
              <select name=\"module_account_picture_status\" id=\"input-status\" class=\"form-control\">
                  ";
        // line 45
        if ((isset($context["module_account_picture_status"]) ? $context["module_account_picture_status"] : null)) {
            // line 46
            echo "                  <option value=\"1\" selected=\"selected\">";
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "</option>
                  <option value=\"0\">";
            // line 47
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "</option>
                  ";
        } else {
            // line 49
            echo "                  <option value=\"1\">";
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "</option>
                  <option value=\"0\" selected=\"selected\">";
            // line 50
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "</option>
                  ";
        }
        // line 52
        echo "              </select>
              </div>
          </div>
          <div class=\"form-group\">
              <label class=\"col-sm-2 control-label\" for=\"input-height\">";
        // line 56
        echo (isset($context["entry_height"]) ? $context["entry_height"] : null);
        echo "</label>
              <div class=\"col-sm-10\">
              <input name=\"module_account_picture_height\" id=\"input-height\" class=\"form-control\" placeholder=\"";
        // line 58
        echo (isset($context["entry_height"]) ? $context["entry_height"] : null);
        echo "\" value=\"";
        echo (isset($context["module_account_picture_height"]) ? $context["module_account_picture_height"] : null);
        echo "\">
              </div>
          </div>
          <div class=\"form-group\">
              <label class=\"col-sm-2 control-label\" for=\"input-width\">";
        // line 62
        echo (isset($context["entry_width"]) ? $context["entry_width"] : null);
        echo "</label>
              <div class=\"col-sm-10\">
              <input name=\"module_account_picture_width\" id=\"input-width\" class=\"form-control\" placeholder=\"";
        // line 64
        echo (isset($context["entry_width"]) ? $context["entry_width"] : null);
        echo "\" value=\"";
        echo (isset($context["module_account_picture_width"]) ? $context["module_account_picture_width"] : null);
        echo "\">
              </div>
          </div>
        </form>
      </div>
      <div id=\"ah-module-footer\" class=\"panel-footer text-center\">
        ";
        // line 70
        echo (isset($context["text_ah_footer"]) ? $context["text_ah_footer"] : null);
        echo "<br />";
        echo (isset($context["text_ah_version"]) ? $context["text_ah_version"] : null);
        echo "
      </div>
    </div>
  </div>
</div>
";
        // line 75
        echo (isset($context["footer"]) ? $context["footer"] : null);
    }

    public function getTemplateName()
    {
        return "extension/module/account_picture.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  178 => 75,  168 => 70,  157 => 64,  152 => 62,  143 => 58,  138 => 56,  132 => 52,  127 => 50,  122 => 49,  117 => 47,  112 => 46,  110 => 45,  104 => 42,  99 => 40,  93 => 37,  89 => 35,  81 => 31,  79 => 30,  73 => 26,  62 => 24,  58 => 23,  53 => 21,  47 => 20,  43 => 19,  35 => 14,  19 => 1,);
    }
}
/* {{ header }}*/
/* <style>*/
/* #ah-logo {*/
/*     max-height:40px;*/
/* }*/
/* */
/* #ah-module-footer {*/
/*     background-color:#00428a; */
/*     color:#fff; */
/*     font-size:13px; */
/*     padding: 20px;*/
/* }*/
/* </style>*/
/* {{ column_left }}*/
/* <div id="content">*/
/*   <div class="page-header">*/
/*     <div class="container-fluid">*/
/*       <div class="pull-right">*/
/*         <button type="submit" form="form-module" data-toggle="tooltip" title="{{ button_save }}" class="btn btn-primary"><i class="fa fa-save"></i></button>*/
/*         <a href="{{ cancel }}" data-toggle="tooltip" title="{{ button_cancel }}" class="btn btn-default"><i class="fa fa-reply"></i></a></div>*/
/*       <h1>{{ heading_title }}</h1>*/
/*       <ul class="breadcrumb">*/
/*         {% for breadcrumb in breadcrumbs %}*/
/*         <li><a href="{{ breadcrumb.href }}">{{ breadcrumb.text }}</a></li>*/
/*         {% endfor %}*/
/*       </ul>*/
/*     </div>*/
/*   </div>*/
/*   <div class="container-fluid">*/
/*     {% if error_warning %}*/
/*     <div class="alert alert-danger alert-dismissible"><i class="fa fa-exclamation-circle"></i> {{ error_warning }}*/
/*       <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*     </div>*/
/*     {% endif %}*/
/*     <div class="panel panel-default">*/
/*       <div class="panel-heading">*/
/*         <a href="http://www.anyhowinfo.com" target="_blank"><img id="ah-logo" class="img-responsive" src="{{ anyhow_logo }}" alt="Anyhow Infosystems"></a>*/
/*       </div>*/
/*       <div class="panel-body">*/
/*         <form action="{{ action }}" method="post" enctype="multipart/form-data" id="form-module" class="form-horizontal">*/
/*           <div class="form-group">*/
/*               <label class="col-sm-2 control-label" for="input-status">{{ entry_status }}</label>*/
/*               <div class="col-sm-10">*/
/*               <select name="module_account_picture_status" id="input-status" class="form-control">*/
/*                   {% if module_account_picture_status %}*/
/*                   <option value="1" selected="selected">{{ text_enabled }}</option>*/
/*                   <option value="0">{{ text_disabled }}</option>*/
/*                   {% else %}*/
/*                   <option value="1">{{ text_enabled }}</option>*/
/*                   <option value="0" selected="selected">{{ text_disabled }}</option>*/
/*                   {% endif %}*/
/*               </select>*/
/*               </div>*/
/*           </div>*/
/*           <div class="form-group">*/
/*               <label class="col-sm-2 control-label" for="input-height">{{ entry_height }}</label>*/
/*               <div class="col-sm-10">*/
/*               <input name="module_account_picture_height" id="input-height" class="form-control" placeholder="{{ entry_height }}" value="{{ module_account_picture_height }}">*/
/*               </div>*/
/*           </div>*/
/*           <div class="form-group">*/
/*               <label class="col-sm-2 control-label" for="input-width">{{ entry_width }}</label>*/
/*               <div class="col-sm-10">*/
/*               <input name="module_account_picture_width" id="input-width" class="form-control" placeholder="{{ entry_width }}" value="{{ module_account_picture_width }}">*/
/*               </div>*/
/*           </div>*/
/*         </form>*/
/*       </div>*/
/*       <div id="ah-module-footer" class="panel-footer text-center">*/
/*         {{ text_ah_footer }}<br />{{ text_ah_version }}*/
/*       </div>*/
/*     </div>*/
/*   </div>*/
/* </div>*/
/* {{ footer }}*/
