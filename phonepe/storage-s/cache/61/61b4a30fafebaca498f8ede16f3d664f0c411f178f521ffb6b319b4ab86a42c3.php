<?php

/* extension/soconfig/layout_form.twig */
class __TwigTemplate_f9fee28e3661d9804fa11fbbb545cc1f1376b642eccecd4b891334314d63fd7c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["homeLayout"] = array("1" => "Home Layout1", "2" => "Home Layout2", "3" => "Home Layout3", "4" => "Home Layout4", "5" => "Home Layout5", "6" => "Home Layout6", "7" => "Home Layout7", "8" => "Home Layout8");
        // line 11
        echo "
";
        // line 12
        echo (isset($context["header"]) ? $context["header"] : null);
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\">
        <button type=\"submit\" form=\"form-layout\" data-toggle=\"tooltip\" title=\"";
        // line 17
        echo (isset($context["button_save"]) ? $context["button_save"] : null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i></button>
        <a href=\"";
        // line 18
        echo (isset($context["cancel"]) ? $context["cancel"] : null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo (isset($context["button_cancel"]) ? $context["button_cancel"] : null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
      <h1>";
        // line 19
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
      <ul class=\"breadcrumb\">
        ";
        // line 21
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 22
            echo "        <li><a href=\"";
            echo $this->getAttribute($context["breadcrumb"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array());
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 24
        echo "      </ul>
    </div>
  </div>
  <div class=\"container-fluid\">
    ";
        // line 28
        if ((isset($context["error_warning"]) ? $context["error_warning"] : null)) {
            // line 29
            echo "    <div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo (isset($context["error_warning"]) ? $context["error_warning"] : null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 33
        echo "    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <h3 class=\"panel-title\"><i class=\"fa fa-pencil\"></i> ";
        // line 35
        echo (isset($context["text_form"]) ? $context["text_form"] : null);
        echo "</h3>
      </div>
      <div class=\"panel-body\">
        <form action=\"";
        // line 38
        echo (isset($context["action"]) ? $context["action"] : null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-layout\" class=\"form-horizontal\">
          <fieldset>
            <legend>";
        // line 40
        echo (isset($context["text_route"]) ? $context["text_route"] : null);
        echo "</legend>
            <div class=\"form-group required\">
              <label class=\"col-sm-2 control-label\" for=\"input-name\">";
        // line 42
        echo (isset($context["entry_name"]) ? $context["entry_name"] : null);
        echo "</label>
              <div class=\"col-sm-10\">
                <input type=\"text\" name=\"name\" value=\"";
        // line 44
        echo (isset($context["name"]) ? $context["name"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_name"]) ? $context["entry_name"] : null);
        echo "\" id=\"input-name\" class=\"form-control\" />
                ";
        // line 45
        if ((isset($context["error_name"]) ? $context["error_name"] : null)) {
            // line 46
            echo "                <div class=\"text-danger\">";
            echo (isset($context["error_name"]) ? $context["error_name"] : null);
            echo "</div>
                ";
        }
        // line 48
        echo "              </div>
            </div>
            <table id=\"route\" class=\"table table-striped table-bordered table-hover\">
              <thead>
                <tr>
                  <td class=\"text-left\">";
        // line 53
        echo (isset($context["entry_store"]) ? $context["entry_store"] : null);
        echo "</td>
                  <td class=\"text-left\">";
        // line 54
        echo (isset($context["entry_route"]) ? $context["entry_route"] : null);
        echo "</td>
                  <td></td>
                </tr>
              </thead>
              <tbody>

                ";
        // line 60
        $context["route_row"] = 0;
        // line 61
        echo "\t\t\t\t        
                ";
        // line 62
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["layout_routes"]) ? $context["layout_routes"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["layout_route"]) {
            // line 63
            echo "                <tr id=\"route-row";
            echo (isset($context["route_row"]) ? $context["route_row"] : null);
            echo "\">
                  <td class=\"text-left\"><select name=\"layout_route[";
            // line 64
            echo (isset($context["route_row"]) ? $context["route_row"] : null);
            echo "][store_id]\" class=\"form-control\">
                      <option value=\"0\">";
            // line 65
            echo (isset($context["text_default"]) ? $context["text_default"] : null);
            echo "</option>
                      ";
            // line 66
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["stores"]) ? $context["stores"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["store"]) {
                // line 67
                echo "                      ";
                if (($this->getAttribute($context["store"], "store_id", array()) == $this->getAttribute($context["layout_route"], "store_id", array()))) {
                    // line 68
                    echo "                      <option value=\"";
                    echo $this->getAttribute($context["store"], "store_id", array());
                    echo "\" selected=\"selected\">";
                    echo $this->getAttribute($context["store"], "name", array());
                    echo "</option>
                      ";
                } else {
                    // line 70
                    echo "                      <option value=\"";
                    echo $this->getAttribute($context["store"], "store_id", array());
                    echo "\">";
                    echo $this->getAttribute($context["store"], "name", array());
                    echo "</option>
                      ";
                }
                // line 72
                echo "                      ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['store'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 73
            echo "                    </select></td>
                  <td class=\"text-left\"><input type=\"text\" name=\"layout_route[";
            // line 74
            echo (isset($context["route_row"]) ? $context["route_row"] : null);
            echo "][route]\" value=\"";
            echo $this->getAttribute($context["layout_route"], "route", array());
            echo "\" placeholder=\"";
            echo (isset($context["entry_route"]) ? $context["entry_route"] : null);
            echo "\" class=\"form-control\" /></td>
                  <td class=\"text-left\"><button type=\"button\" onclick=\"\$('#route-row";
            // line 75
            echo (isset($context["route_row"]) ? $context["route_row"] : null);
            echo "').remove();\" data-toggle=\"tooltip\" title=\"";
            echo (isset($context["button_remove"]) ? $context["button_remove"] : null);
            echo "\" class=\"btn btn-danger\"><i class=\"fa fa-minus-circle\"></i></button></td>
                </tr>
                ";
            // line 77
            $context["route_row"] = ((isset($context["route_row"]) ? $context["route_row"] : null) + 1);
            // line 78
            echo "                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['layout_route'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 79
        echo "
              </tbody>
              <tfoot>
                <tr>
                  <td colspan=\"2\"></td>
                  <td class=\"text-left\"><button type=\"button\" onclick=\"addRoute();\" data-toggle=\"tooltip\" title=\"";
        // line 84
        echo (isset($context["button_route_add"]) ? $context["button_route_add"] : null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-plus-circle\"></i></button></td>
                </tr>
              </tfoot>
            </table>
          </fieldset>

          <fieldset>
            <legend>";
        // line 91
        echo (isset($context["text_module"]) ? $context["text_module"] : null);
        echo "</legend>
            
            ";
        // line 93
        $context["module_row"] = 0;
        // line 94
        echo "            
            <div class=\"bs-callout bs-callout-info\" id=\"callout-alerts-no-default\"> <h4>Content Header</h4> </div>
            <div class=\"well\"> 
                <div class=\"row\">
                    ";
        // line 99
        echo "                    <div class=\"col-lg-4 col-md-4 col-sm-12\"> 
                        <pre><h2 class=\"text-center\">Logo demo</h2></pre>
                    </div>

                    <div class=\"col-lg-4 col-md-4 col-sm-12\"> 
                        ";
        // line 104
        echo $this->getAttribute((isset($context["fields"]) ? $context["fields"] : null), "create_position", array(0 => "Mega menu horizontal", 1 => "content_menu1", 2 => (isset($context["layout_modules"]) ? $context["layout_modules"] : null), 3 => (isset($context["extensions"]) ? $context["extensions"] : null), 4 => (isset($context["module_row"]) ? $context["module_row"] : null)), "method");
        echo "                 
                    </div>
                    <div class=\"col-lg-4 col-md-4 col-sm-12\"> 
                        ";
        // line 107
        echo $this->getAttribute((isset($context["fields"]) ? $context["fields"] : null), "create_position", array(0 => "Mega menu vertical", 1 => "content_menu2", 2 => (isset($context["layout_modules"]) ? $context["layout_modules"] : null), 3 => (isset($context["extensions"]) ? $context["extensions"] : null), 4 => (isset($context["module_row"]) ? $context["module_row"] : null)), "method");
        echo "                 
                    </div>
                    <div class=\"col-lg-4 col-md-4 col-sm-12\"> 
                        ";
        // line 110
        echo $this->getAttribute((isset($context["fields"]) ? $context["fields"] : null), "create_position", array(0 => "Header Block", 1 => "header_block", 2 => (isset($context["layout_modules"]) ? $context["layout_modules"] : null), 3 => (isset($context["extensions"]) ? $context["extensions"] : null), 4 => (isset($context["module_row"]) ? $context["module_row"] : null)), "method");
        echo "                 
                    </div>
                    <div class=\"col-lg-4 col-md-4 col-sm-12\">
                        ";
        // line 113
        echo $this->getAttribute((isset($context["fields"]) ? $context["fields"] : null), "create_position", array(0 => "Search Block", 1 => "search_block", 2 => (isset($context["layout_modules"]) ? $context["layout_modules"] : null), 3 => (isset($context["extensions"]) ? $context["extensions"] : null), 4 => (isset($context["module_row"]) ? $context["module_row"] : null)), "method");
        echo "
                    </div>
                   
                 </div>
            </div>

            <div class=\"bs-callout bs-callout-info\" id=\"callout-alerts-no-default\"> <h4>Content Main</h4> </div>
            <div class=\"well\"> 
                <div class=\"row\">
                    <div class=\"col-sm-12\">
                        ";
        // line 123
        echo $this->getAttribute((isset($context["fields"]) ? $context["fields"] : null), "home_position", array(0 => "Content Home (Only show home page) ", 1 => "content_home", 2 => (isset($context["layout_modules"]) ? $context["layout_modules"] : null), 3 => (isset($context["extensions"]) ? $context["extensions"] : null), 4 => (isset($context["module_row"]) ? $context["module_row"] : null), 5 => (isset($context["homeLayout"]) ? $context["homeLayout"] : null)), "method");
        echo "
                    </div>
                    
                    <div class=\"col-lg-3 col-md-4 col-sm-12\">
                        ";
        // line 128
        echo "                        ";
        echo $this->getAttribute((isset($context["fields"]) ? $context["fields"] : null), "create_position", array(0 => "Column Left", 1 => "column_left", 2 => (isset($context["layout_modules"]) ? $context["layout_modules"] : null), 3 => (isset($context["extensions"]) ? $context["extensions"] : null), 4 => (isset($context["module_row"]) ? $context["module_row"] : null)), "method");
        echo "
                    </div>

                    <div class=\"col-lg-6 col-md-4 col-sm-12\">
                     ";
        // line 133
        echo "                        

                        ";
        // line 135
        echo $this->getAttribute((isset($context["fields"]) ? $context["fields"] : null), "create_position", array(0 => "Content Top", 1 => "content_top", 2 => (isset($context["layout_modules"]) ? $context["layout_modules"] : null), 3 => (isset($context["extensions"]) ? $context["extensions"] : null), 4 => (isset($context["module_row"]) ? $context["module_row"] : null)), "method");
        echo "
                       
                        ";
        // line 137
        echo $this->getAttribute((isset($context["fields"]) ? $context["fields"] : null), "create_position", array(0 => "Content Bottom", 1 => "content_bottom", 2 => (isset($context["layout_modules"]) ? $context["layout_modules"] : null), 3 => (isset($context["extensions"]) ? $context["extensions"] : null), 4 => (isset($context["module_row"]) ? $context["module_row"] : null)), "method");
        echo "

                    </div>

                    <div class=\"col-lg-3 col-md-4 col-sm-12\">
                    ";
        // line 142
        echo " 
                        ";
        // line 143
        echo $this->getAttribute((isset($context["fields"]) ? $context["fields"] : null), "create_position", array(0 => "Column Right", 1 => "column_right", 2 => (isset($context["layout_modules"]) ? $context["layout_modules"] : null), 3 => (isset($context["extensions"]) ? $context["extensions"] : null), 4 => (isset($context["module_row"]) ? $context["module_row"] : null)), "method");
        echo "
                        
                     </div>

                 
                </div>
            </div>

            <div class=\"bs-callout bs-callout-info\" id=\"callout-alerts-no-default\"> <h4>Content Footer</h4> </div>
            <div class=\"well\"> 
                <div class=\"row\">
                    ";
        // line 155
        echo "                     <div class=\"col-lg-4 col-md-4 col-sm-12\"> 
                        ";
        // line 156
        echo $this->getAttribute((isset($context["fields"]) ? $context["fields"] : null), "create_position", array(0 => "Footer Block", 1 => "footer_block", 2 => (isset($context["layout_modules"]) ? $context["layout_modules"] : null), 3 => (isset($context["extensions"]) ? $context["extensions"] : null), 4 => (isset($context["module_row"]) ? $context["module_row"] : null)), "method");
        echo "
                    </div>
                    <div class=\"col-lg-4 col-md-4 col-sm-12\"> 
                        ";
        // line 159
        echo $this->getAttribute((isset($context["fields"]) ? $context["fields"] : null), "create_position", array(0 => "Footer Block 1", 1 => "footer_block1", 2 => (isset($context["layout_modules"]) ? $context["layout_modules"] : null), 3 => (isset($context["extensions"]) ? $context["extensions"] : null), 4 => (isset($context["module_row"]) ? $context["module_row"] : null)), "method");
        echo "
                    </div>
                    <div class=\"col-lg-4 col-md-4 col-sm-12\">
                        ";
        // line 162
        echo $this->getAttribute((isset($context["fields"]) ? $context["fields"] : null), "create_position", array(0 => "Footer Block 2", 1 => "footer_block2", 2 => (isset($context["layout_modules"]) ? $context["layout_modules"] : null), 3 => (isset($context["extensions"]) ? $context["extensions"] : null), 4 => (isset($context["module_row"]) ? $context["module_row"] : null)), "method");
        echo "
                    </div>
                    <div class=\"col-lg-4 col-md-4 col-sm-12\">
                        ";
        // line 165
        echo $this->getAttribute((isset($context["fields"]) ? $context["fields"] : null), "create_position", array(0 => "Footer Block 3", 1 => "footer_block3", 2 => (isset($context["layout_modules"]) ? $context["layout_modules"] : null), 3 => (isset($context["extensions"]) ? $context["extensions"] : null), 4 => (isset($context["module_row"]) ? $context["module_row"] : null)), "method");
        echo "
                    </div>
                    <div class=\"col-lg-4 col-md-4 col-sm-12\">
                        ";
        // line 168
        echo $this->getAttribute((isset($context["fields"]) ? $context["fields"] : null), "create_position", array(0 => "Footer Block 4", 1 => "footer_block4", 2 => (isset($context["layout_modules"]) ? $context["layout_modules"] : null), 3 => (isset($context["extensions"]) ? $context["extensions"] : null), 4 => (isset($context["module_row"]) ? $context["module_row"] : null)), "method");
        echo "
                    </div>
                    <div class=\"col-lg-4 col-md-4 col-sm-12\">
                        ";
        // line 171
        echo $this->getAttribute((isset($context["fields"]) ? $context["fields"] : null), "create_position", array(0 => "Footer Block 5", 1 => "footer_block5", 2 => (isset($context["layout_modules"]) ? $context["layout_modules"] : null), 3 => (isset($context["extensions"]) ? $context["extensions"] : null), 4 => (isset($context["module_row"]) ? $context["module_row"] : null)), "method");
        echo "
                    </div>
                    <div class=\"col-lg-4 col-md-4 col-sm-12\">
                        ";
        // line 174
        echo $this->getAttribute((isset($context["fields"]) ? $context["fields"] : null), "create_position", array(0 => "Footer Block 6", 1 => "footer_block6", 2 => (isset($context["layout_modules"]) ? $context["layout_modules"] : null), 3 => (isset($context["extensions"]) ? $context["extensions"] : null), 4 => (isset($context["module_row"]) ? $context["module_row"] : null)), "method");
        echo "
                    </div>
                    <div class=\"col-lg-4 col-md-4 col-sm-12\">
                        ";
        // line 177
        echo $this->getAttribute((isset($context["fields"]) ? $context["fields"] : null), "create_position", array(0 => "Footer Block 7", 1 => "footer_block7", 2 => (isset($context["layout_modules"]) ? $context["layout_modules"] : null), 3 => (isset($context["extensions"]) ? $context["extensions"] : null), 4 => (isset($context["module_row"]) ? $context["module_row"] : null)), "method");
        echo "
                    </div>
                    <div class=\"col-lg-4 col-md-4 col-sm-12\">
                        ";
        // line 180
        echo $this->getAttribute((isset($context["fields"]) ? $context["fields"] : null), "create_position", array(0 => "Footer Block 8", 1 => "footer_block8", 2 => (isset($context["layout_modules"]) ? $context["layout_modules"] : null), 3 => (isset($context["extensions"]) ? $context["extensions"] : null), 4 => (isset($context["module_row"]) ? $context["module_row"] : null)), "method");
        echo "
                    </div>
                    
                 </div>
            </div>

          </fieldset>
        </form>
      </div>
    </div>
  </div>

  <style>
    .input-group .form-control{width:90%;}
    .form-order{border: 1px solid #ccc;    vertical-align: top;margin-left: -1px;}
    .bs-callout h4 { margin: 0;}
    .bs-callout {padding: 10px;margin: 20px 0 0;border: 1px solid #ddd;border-left-width: 5px;background: #f4f4f4;}
    .bs-callout+.well {background: none;margin-top: -1px;border-radius: 0px;}
    .bs-callout-info {border-left-color: #1b809e;}
    .common-home .column_left,.common-home .column_right  {display: none;}
    .extension-mobile-home #module-content_home{display: table;}
    .common-home #module-content_soon{display: table;}
    .common-home #module-content_home{display: table;}
    .common-home .column_content{width: 100%; display: flex; padding: 0;}
    .common-home .column_content > .table{max-width: calc(33.33% - 30px);margin: 0 15px;}
  
  </style>


  <script type=\"text/javascript\"><!--

\$(\".select2-input\").select2({});

var route_row = ";
        // line 213
        echo (isset($context["route_row"]) ? $context["route_row"] : null);
        echo ";

function addRoute() {
     html  = '<tr id=\"route-row' + route_row + '\">';
     html += '  <td class=\"text-left\"><select name=\"layout_route[' + route_row + '][store_id]\" class=\"form-control select2-input\">';
     html += '  <option value=\"0\">";
        // line 218
        echo (isset($context["text_default"]) ? $context["text_default"] : null);
        echo "</option>';
     ";
        // line 219
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["stores"]) ? $context["stores"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["store"]) {
            // line 220
            echo "     html += '<option value=\"";
            echo $this->getAttribute($context["store"], "store_id", array());
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["store"], "name", array()), "js");
            echo "</option>';
     ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['store'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 222
        echo "     html += '  </select></td>';
     html += '  <td class=\"text-left\"><input type=\"text\" name=\"layout_route[' + route_row + '][route]\" value=\"\" placeholder=\"";
        // line 223
        echo (isset($context["entry_route"]) ? $context["entry_route"] : null);
        echo "\" class=\"form-control\" /></td>';
     html += '  <td class=\"text-left\"><button type=\"button\" onclick=\"\$(\\'#route-row' + route_row + '\\').remove();\" data-toggle=\"tooltip\" title=\"";
        // line 224
        echo (isset($context["button_remove"]) ? $context["button_remove"] : null);
        echo "\" class=\"btn btn-danger\"><i class=\"fa fa-minus-circle\"></i></button></td>';
     html += '</tr>';
     
     \$('#route tbody').append(html);
     
     route_row++;
}

";
        // line 232
        echo $this->getAttribute((isset($context["fields"]) ? $context["fields"] : null), "var_module_row", array(), "method");
        echo "
function addModule(type) {
    
     html  = '<tr id=\"module-row' + module_row + '\">';
    html += '  <td class=\"text-left\"><div class=\"input-group\"><select name=\"layout_module[' + module_row + '][code]\" class=\"form-control input-sm select2-input\">';
     ";
        // line 237
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["extensions"]) ? $context["extensions"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["extension"]) {
            // line 238
            echo "     html += '    <optgroup label=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["extension"], "name", array()), "js");
            echo "\">';
     ";
            // line 239
            if ( !$this->getAttribute($context["extension"], "module", array())) {
                // line 240
                echo "     html += '      <option value=\"";
                echo $this->getAttribute($context["extension"], "code", array());
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["extension"], "name", array()), "js");
                echo "</option>';
     ";
            } else {
                // line 242
                echo "     ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["extension"], "module", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                    // line 243
                    echo "     html += '      <option value=\"";
                    echo $this->getAttribute($context["module"], "code", array());
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["module"], "name", array()), "js");
                    echo "</option>';
     ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 245
                echo "     ";
            }
            // line 246
            echo "     html += '    </optgroup>';
     ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['extension'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 248
        echo "     html += '  </select>';
    html += '  <input type=\"hidden\" name=\"layout_module[' + module_row + '][position]\" value=\"' + type.replace('-', '_') + '\" />';
    html += '  <input type=\"hidden\" name=\"layout_module[' + module_row + '][sort_order]\" value=\"\" />';
     html += '  <div class=\"input-group-btn\"><a href=\"\" target=\"_blank\" type=\"button\" data-toggle=\"tooltip\" title=\"";
        // line 251
        echo (isset($context["button_edit"]) ? $context["button_edit"] : null);
        echo "\" class=\"btn btn-primary btn-sm\"><i class=\"fa fa-pencil\"></i></a><button type=\"button\" onclick=\"\$(\\'#module-row' + module_row + '\\').remove();\" data-toggle=\"tooltip\" title=\"";
        echo (isset($context["button_remove"]) ? $context["button_remove"] : null);
        echo "\" class=\"btn btn-danger btn-sm\"><i class=\"fa fa fa-minus-circle\"></i></button></div></div></td>';
     html += '</tr>';
     \$(\".select2-input\").select2({});
     \$('#module-' + type + ' tbody').append(html);
     
     \$('#module-' + type + ' tbody select[name=\\'layout_module[' + module_row + '][code]\\']').val(\$('#module-' + type + ' tfoot select').val());
     
     \$('#module-' + type + ' select[name*=\\'code\\']').trigger('change');
          
     \$('#module-' + type + ' tbody input[name*=\\'sort_order\\']').each(function(i, element) {
          \$(element).val(i);
     });
     
     module_row++;
}


\$('#module-column-left, #module-column-right, #module-content-top, #module-content-bottom').delegate('select[name*=\\'code\\']', 'change', function() {
    var part = this.value.split('.');
    
    if (!part[1]) {
        \$(this).parent().find('a').attr('href', 'index.php?route=extension/module/' + part[0] + '&user_token=";
        // line 272
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "');
    } else {
        \$(this).parent().find('a').attr('href', 'index.php?route=extension/module/' + part[0] + '&user_token=";
        // line 274
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "&module_id=' + part[1]);
    }
});

\$('#module-column-left, #module-column-right, #module-content-top, #module-content-bottom').trigger('change');

//--></script> 
</div>
";
        // line 282
        echo (isset($context["footer"]) ? $context["footer"] : null);
    }

    public function getTemplateName()
    {
        return "extension/soconfig/layout_form.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  564 => 282,  553 => 274,  548 => 272,  522 => 251,  517 => 248,  510 => 246,  507 => 245,  496 => 243,  491 => 242,  483 => 240,  481 => 239,  476 => 238,  472 => 237,  464 => 232,  453 => 224,  449 => 223,  446 => 222,  435 => 220,  431 => 219,  427 => 218,  419 => 213,  383 => 180,  377 => 177,  371 => 174,  365 => 171,  359 => 168,  353 => 165,  347 => 162,  341 => 159,  335 => 156,  332 => 155,  318 => 143,  315 => 142,  307 => 137,  302 => 135,  298 => 133,  290 => 128,  283 => 123,  270 => 113,  264 => 110,  258 => 107,  252 => 104,  245 => 99,  239 => 94,  237 => 93,  232 => 91,  222 => 84,  215 => 79,  209 => 78,  207 => 77,  200 => 75,  192 => 74,  189 => 73,  183 => 72,  175 => 70,  167 => 68,  164 => 67,  160 => 66,  156 => 65,  152 => 64,  147 => 63,  143 => 62,  140 => 61,  138 => 60,  129 => 54,  125 => 53,  118 => 48,  112 => 46,  110 => 45,  104 => 44,  99 => 42,  94 => 40,  89 => 38,  83 => 35,  79 => 33,  71 => 29,  69 => 28,  63 => 24,  52 => 22,  48 => 21,  43 => 19,  37 => 18,  33 => 17,  24 => 12,  21 => 11,  19 => 1,);
    }
}
/* {% set homeLayout = { */
/*    '1' : 'Home Layout1',*/
/*    '2' : 'Home Layout2',*/
/*    '3' : 'Home Layout3',*/
/*    '4' : 'Home Layout4',*/
/*    '5' : 'Home Layout5',*/
/*    '6' : 'Home Layout6',*/
/*    '7' : 'Home Layout7',*/
/*    '8' : 'Home Layout8',*/
/* } %}*/
/* */
/* {{ header }}{{ column_left }}*/
/* <div id="content">*/
/*   <div class="page-header">*/
/*     <div class="container-fluid">*/
/*       <div class="pull-right">*/
/*         <button type="submit" form="form-layout" data-toggle="tooltip" title="{{ button_save }}" class="btn btn-primary"><i class="fa fa-save"></i></button>*/
/*         <a href="{{ cancel }}" data-toggle="tooltip" title="{{ button_cancel }}" class="btn btn-default"><i class="fa fa-reply"></i></a></div>*/
/*       <h1>{{ heading_title }}</h1>*/
/*       <ul class="breadcrumb">*/
/*         {% for breadcrumb in breadcrumbs %}*/
/*         <li><a href="{{ breadcrumb.href }}">{{ breadcrumb.text }}</a></li>*/
/*         {% endfor %}*/
/*       </ul>*/
/*     </div>*/
/*   </div>*/
/*   <div class="container-fluid">*/
/*     {% if error_warning %}*/
/*     <div class="alert alert-danger alert-dismissible"><i class="fa fa-exclamation-circle"></i> {{ error_warning }}*/
/*       <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*     </div>*/
/*     {% endif %}*/
/*     <div class="panel panel-default">*/
/*       <div class="panel-heading">*/
/*         <h3 class="panel-title"><i class="fa fa-pencil"></i> {{ text_form }}</h3>*/
/*       </div>*/
/*       <div class="panel-body">*/
/*         <form action="{{ action }}" method="post" enctype="multipart/form-data" id="form-layout" class="form-horizontal">*/
/*           <fieldset>*/
/*             <legend>{{ text_route }}</legend>*/
/*             <div class="form-group required">*/
/*               <label class="col-sm-2 control-label" for="input-name">{{ entry_name }}</label>*/
/*               <div class="col-sm-10">*/
/*                 <input type="text" name="name" value="{{ name }}" placeholder="{{ entry_name }}" id="input-name" class="form-control" />*/
/*                 {% if error_name %}*/
/*                 <div class="text-danger">{{ error_name }}</div>*/
/*                 {% endif %}*/
/*               </div>*/
/*             </div>*/
/*             <table id="route" class="table table-striped table-bordered table-hover">*/
/*               <thead>*/
/*                 <tr>*/
/*                   <td class="text-left">{{ entry_store }}</td>*/
/*                   <td class="text-left">{{ entry_route }}</td>*/
/*                   <td></td>*/
/*                 </tr>*/
/*               </thead>*/
/*               <tbody>*/
/* */
/*                 {% set route_row = 0 %}*/
/* 				        */
/*                 {% for layout_route in layout_routes %}*/
/*                 <tr id="route-row{{ route_row }}">*/
/*                   <td class="text-left"><select name="layout_route[{{ route_row }}][store_id]" class="form-control">*/
/*                       <option value="0">{{ text_default }}</option>*/
/*                       {% for store in stores %}*/
/*                       {% if store.store_id == layout_route.store_id %}*/
/*                       <option value="{{ store.store_id }}" selected="selected">{{ store.name }}</option>*/
/*                       {% else %}*/
/*                       <option value="{{ store.store_id }}">{{ store.name }}</option>*/
/*                       {% endif %}*/
/*                       {% endfor %}*/
/*                     </select></td>*/
/*                   <td class="text-left"><input type="text" name="layout_route[{{ route_row }}][route]" value="{{ layout_route.route }}" placeholder="{{ entry_route }}" class="form-control" /></td>*/
/*                   <td class="text-left"><button type="button" onclick="$('#route-row{{ route_row }}').remove();" data-toggle="tooltip" title="{{ button_remove }}" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>*/
/*                 </tr>*/
/*                 {% set route_row = route_row + 1 %}*/
/*                 {% endfor %}*/
/* */
/*               </tbody>*/
/*               <tfoot>*/
/*                 <tr>*/
/*                   <td colspan="2"></td>*/
/*                   <td class="text-left"><button type="button" onclick="addRoute();" data-toggle="tooltip" title="{{ button_route_add }}" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>*/
/*                 </tr>*/
/*               </tfoot>*/
/*             </table>*/
/*           </fieldset>*/
/* */
/*           <fieldset>*/
/*             <legend>{{ text_module }}</legend>*/
/*             */
/*             {% set module_row = 0 %}*/
/*             */
/*             <div class="bs-callout bs-callout-info" id="callout-alerts-no-default"> <h4>Content Header</h4> </div>*/
/*             <div class="well"> */
/*                 <div class="row">*/
/*                     {# Notice: Name position Giới hạn 14 kí tự - Setting Database  varchar(14) #}*/
/*                     <div class="col-lg-4 col-md-4 col-sm-12"> */
/*                         <pre><h2 class="text-center">Logo demo</h2></pre>*/
/*                     </div>*/
/* */
/*                     <div class="col-lg-4 col-md-4 col-sm-12"> */
/*                         {{fields.create_position('Mega menu horizontal','content_menu1',layout_modules,extensions,module_row)}}                 */
/*                     </div>*/
/*                     <div class="col-lg-4 col-md-4 col-sm-12"> */
/*                         {{fields.create_position('Mega menu vertical','content_menu2',layout_modules,extensions,module_row)}}                 */
/*                     </div>*/
/*                     <div class="col-lg-4 col-md-4 col-sm-12"> */
/*                         {{fields.create_position('Header Block','header_block',layout_modules,extensions,module_row)}}                 */
/*                     </div>*/
/*                     <div class="col-lg-4 col-md-4 col-sm-12">*/
/*                         {{fields.create_position('Search Block','search_block',layout_modules,extensions,module_row)}}*/
/*                     </div>*/
/*                    */
/*                  </div>*/
/*             </div>*/
/* */
/*             <div class="bs-callout bs-callout-info" id="callout-alerts-no-default"> <h4>Content Main</h4> </div>*/
/*             <div class="well"> */
/*                 <div class="row">*/
/*                     <div class="col-sm-12">*/
/*                         {{fields.home_position('Content Home (Only show home page) ','content_home',layout_modules,extensions,module_row,homeLayout)}}*/
/*                     </div>*/
/*                     */
/*                     <div class="col-lg-3 col-md-4 col-sm-12">*/
/*                         {#======= Position: Column Left ======#}*/
/*                         {{fields.create_position('Column Left','column_left',layout_modules,extensions,module_row)}}*/
/*                     </div>*/
/* */
/*                     <div class="col-lg-6 col-md-4 col-sm-12">*/
/*                      {#======= Position: Content Top ======#}*/
/*                         */
/* */
/*                         {{fields.create_position('Content Top','content_top',layout_modules,extensions,module_row)}}*/
/*                        */
/*                         {{fields.create_position('Content Bottom','content_bottom',layout_modules,extensions,module_row)}}*/
/* */
/*                     </div>*/
/* */
/*                     <div class="col-lg-3 col-md-4 col-sm-12">*/
/*                     {#======= Position: Column Right ======#} */
/*                         {{fields.create_position('Column Right','column_right',layout_modules,extensions,module_row)}}*/
/*                         */
/*                      </div>*/
/* */
/*                  */
/*                 </div>*/
/*             </div>*/
/* */
/*             <div class="bs-callout bs-callout-info" id="callout-alerts-no-default"> <h4>Content Footer</h4> </div>*/
/*             <div class="well"> */
/*                 <div class="row">*/
/*                     {# Notice: Name position  - Setting Database  varchar(14) #}*/
/*                      <div class="col-lg-4 col-md-4 col-sm-12"> */
/*                         {{fields.create_position('Footer Block','footer_block',layout_modules,extensions,module_row)}}*/
/*                     </div>*/
/*                     <div class="col-lg-4 col-md-4 col-sm-12"> */
/*                         {{fields.create_position('Footer Block 1','footer_block1',layout_modules,extensions,module_row)}}*/
/*                     </div>*/
/*                     <div class="col-lg-4 col-md-4 col-sm-12">*/
/*                         {{fields.create_position('Footer Block 2','footer_block2',layout_modules,extensions,module_row)}}*/
/*                     </div>*/
/*                     <div class="col-lg-4 col-md-4 col-sm-12">*/
/*                         {{fields.create_position('Footer Block 3','footer_block3',layout_modules,extensions,module_row)}}*/
/*                     </div>*/
/*                     <div class="col-lg-4 col-md-4 col-sm-12">*/
/*                         {{fields.create_position('Footer Block 4','footer_block4',layout_modules,extensions,module_row)}}*/
/*                     </div>*/
/*                     <div class="col-lg-4 col-md-4 col-sm-12">*/
/*                         {{fields.create_position('Footer Block 5','footer_block5',layout_modules,extensions,module_row)}}*/
/*                     </div>*/
/*                     <div class="col-lg-4 col-md-4 col-sm-12">*/
/*                         {{fields.create_position('Footer Block 6','footer_block6',layout_modules,extensions,module_row)}}*/
/*                     </div>*/
/*                     <div class="col-lg-4 col-md-4 col-sm-12">*/
/*                         {{fields.create_position('Footer Block 7','footer_block7',layout_modules,extensions,module_row)}}*/
/*                     </div>*/
/*                     <div class="col-lg-4 col-md-4 col-sm-12">*/
/*                         {{fields.create_position('Footer Block 8','footer_block8',layout_modules,extensions,module_row)}}*/
/*                     </div>*/
/*                     */
/*                  </div>*/
/*             </div>*/
/* */
/*           </fieldset>*/
/*         </form>*/
/*       </div>*/
/*     </div>*/
/*   </div>*/
/* */
/*   <style>*/
/*     .input-group .form-control{width:90%;}*/
/*     .form-order{border: 1px solid #ccc;    vertical-align: top;margin-left: -1px;}*/
/*     .bs-callout h4 { margin: 0;}*/
/*     .bs-callout {padding: 10px;margin: 20px 0 0;border: 1px solid #ddd;border-left-width: 5px;background: #f4f4f4;}*/
/*     .bs-callout+.well {background: none;margin-top: -1px;border-radius: 0px;}*/
/*     .bs-callout-info {border-left-color: #1b809e;}*/
/*     .common-home .column_left,.common-home .column_right  {display: none;}*/
/*     .extension-mobile-home #module-content_home{display: table;}*/
/*     .common-home #module-content_soon{display: table;}*/
/*     .common-home #module-content_home{display: table;}*/
/*     .common-home .column_content{width: 100%; display: flex; padding: 0;}*/
/*     .common-home .column_content > .table{max-width: calc(33.33% - 30px);margin: 0 15px;}*/
/*   */
/*   </style>*/
/* */
/* */
/*   <script type="text/javascript"><!--*/
/* */
/* $(".select2-input").select2({});*/
/* */
/* var route_row = {{ route_row }};*/
/* */
/* function addRoute() {*/
/*      html  = '<tr id="route-row' + route_row + '">';*/
/*      html += '  <td class="text-left"><select name="layout_route[' + route_row + '][store_id]" class="form-control select2-input">';*/
/*      html += '  <option value="0">{{ text_default }}</option>';*/
/*      {% for store in stores %}*/
/*      html += '<option value="{{ store.store_id }}">{{ store.name|escape('js') }}</option>';*/
/*      {% endfor %}*/
/*      html += '  </select></td>';*/
/*      html += '  <td class="text-left"><input type="text" name="layout_route[' + route_row + '][route]" value="" placeholder="{{ entry_route }}" class="form-control" /></td>';*/
/*      html += '  <td class="text-left"><button type="button" onclick="$(\'#route-row' + route_row + '\').remove();" data-toggle="tooltip" title="{{ button_remove }}" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';*/
/*      html += '</tr>';*/
/*      */
/*      $('#route tbody').append(html);*/
/*      */
/*      route_row++;*/
/* }*/
/* */
/* {{fields.var_module_row()}}*/
/* function addModule(type) {*/
/*     */
/*      html  = '<tr id="module-row' + module_row + '">';*/
/*     html += '  <td class="text-left"><div class="input-group"><select name="layout_module[' + module_row + '][code]" class="form-control input-sm select2-input">';*/
/*      {% for extension in extensions %}*/
/*      html += '    <optgroup label="{{ extension.name|escape('js') }}">';*/
/*      {% if not extension.module %}*/
/*      html += '      <option value="{{ extension.code }}">{{ extension.name|escape('js') }}</option>';*/
/*      {% else %}*/
/*      {% for module in extension.module %}*/
/*      html += '      <option value="{{ module.code }}">{{ module.name|escape('js') }}</option>';*/
/*      {% endfor %}*/
/*      {% endif %}*/
/*      html += '    </optgroup>';*/
/*      {% endfor %}*/
/*      html += '  </select>';*/
/*     html += '  <input type="hidden" name="layout_module[' + module_row + '][position]" value="' + type.replace('-', '_') + '" />';*/
/*     html += '  <input type="hidden" name="layout_module[' + module_row + '][sort_order]" value="" />';*/
/*      html += '  <div class="input-group-btn"><a href="" target="_blank" type="button" data-toggle="tooltip" title="{{ button_edit }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a><button type="button" onclick="$(\'#module-row' + module_row + '\').remove();" data-toggle="tooltip" title="{{ button_remove }}" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button></div></div></td>';*/
/*      html += '</tr>';*/
/*      $(".select2-input").select2({});*/
/*      $('#module-' + type + ' tbody').append(html);*/
/*      */
/*      $('#module-' + type + ' tbody select[name=\'layout_module[' + module_row + '][code]\']').val($('#module-' + type + ' tfoot select').val());*/
/*      */
/*      $('#module-' + type + ' select[name*=\'code\']').trigger('change');*/
/*           */
/*      $('#module-' + type + ' tbody input[name*=\'sort_order\']').each(function(i, element) {*/
/*           $(element).val(i);*/
/*      });*/
/*      */
/*      module_row++;*/
/* }*/
/* */
/* */
/* $('#module-column-left, #module-column-right, #module-content-top, #module-content-bottom').delegate('select[name*=\'code\']', 'change', function() {*/
/*     var part = this.value.split('.');*/
/*     */
/*     if (!part[1]) {*/
/*         $(this).parent().find('a').attr('href', 'index.php?route=extension/module/' + part[0] + '&user_token={{ user_token }}');*/
/*     } else {*/
/*         $(this).parent().find('a').attr('href', 'index.php?route=extension/module/' + part[0] + '&user_token={{ user_token }}&module_id=' + part[1]);*/
/*     }*/
/* });*/
/* */
/* $('#module-column-left, #module-column-right, #module-content-top, #module-content-bottom').trigger('change');*/
/* */
/* //--></script> */
/* </div>*/
/* {{ footer }}*/
