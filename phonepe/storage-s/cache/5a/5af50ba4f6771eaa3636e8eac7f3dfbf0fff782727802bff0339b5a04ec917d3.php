<?php

/* default/template/checkout/payment_method.twig */
class __TwigTemplate_5b5d0b03c1ecffd1f8768f9aefeba4bc4ebac4094f374a154622865ecb59236f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
                        ";
        // line 2
        if ((array_key_exists("preOrderQuantity", $context) && (isset($context["preOrderQuantity"]) ? $context["preOrderQuantity"] : null))) {
            echo " 
                            <div class=\"alert alert-danger\">
                            <i class=\"fa fa-exclamation-circle\"></i>
                            ";
            // line 5
            echo (isset($context["error_preorder_quantity"]) ? $context["error_preorder_quantity"] : null);
            echo " 
                            </div>
                        ";
        }
        // line 8
        echo "                            
";
        // line 9
        if ((isset($context["error_warning"]) ? $context["error_warning"] : null)) {
            // line 10
            echo "<div class=\"alert alert-warning alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo (isset($context["error_warning"]) ? $context["error_warning"] : null);
            echo "</div>
";
        }
        // line 12
        if ((isset($context["payment_methods"]) ? $context["payment_methods"] : null)) {
            // line 13
            echo "<p>";
            echo (isset($context["text_payment_method"]) ? $context["text_payment_method"] : null);
            echo "</p>
";
            // line 14
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["payment_methods"]) ? $context["payment_methods"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["payment_method"]) {
                // line 15
                echo "<div class=\"radio\">
  <label>";
                // line 16
                if ((($this->getAttribute($context["payment_method"], "code", array()) == (isset($context["code"]) ? $context["code"] : null)) ||  !(isset($context["code"]) ? $context["code"] : null))) {
                    // line 17
                    echo "    ";
                    $context["code"] = $this->getAttribute($context["payment_method"], "code", array());
                    // line 18
                    echo "    <input type=\"radio\" name=\"payment_method\" value=\"";
                    echo $this->getAttribute($context["payment_method"], "code", array());
                    echo "\" checked=\"checked\" />
    ";
                } else {
                    // line 20
                    echo "    <input type=\"radio\" name=\"payment_method\" value=\"";
                    echo $this->getAttribute($context["payment_method"], "code", array());
                    echo "\" />
    ";
                }
                // line 22
                echo "    ";
                echo $this->getAttribute($context["payment_method"], "title", array());
                echo "
    ";
                // line 23
                if ($this->getAttribute($context["payment_method"], "terms", array())) {
                    // line 24
                    echo "    (";
                    echo $this->getAttribute($context["payment_method"], "terms", array());
                    echo ")
    ";
                }
                // line 25
                echo " </label>
</div>
";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['payment_method'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        // line 29
        echo "<p><strong>";
        echo (isset($context["text_comments"]) ? $context["text_comments"] : null);
        echo "</strong></p>
<p>
  <textarea name=\"comment\" rows=\"8\" class=\"form-control\">";
        // line 31
        echo (isset($context["comment"]) ? $context["comment"] : null);
        echo "</textarea>
</p>
";
        // line 33
        if ((isset($context["text_agree"]) ? $context["text_agree"] : null)) {
            // line 34
            echo "<div class=\"buttons\">
  <div class=\"pull-right\">";
            // line 35
            echo (isset($context["text_agree"]) ? $context["text_agree"] : null);
            echo "
    ";
            // line 36
            if ((isset($context["agree"]) ? $context["agree"] : null)) {
                // line 37
                echo "    <input type=\"checkbox\" name=\"agree\" value=\"1\" checked=\"checked\" />
    ";
            } else {
                // line 39
                echo "    <input type=\"checkbox\" name=\"agree\" value=\"1\" />
    ";
            }
            // line 41
            echo "    &nbsp;
    
            ";
            // line 43
            if ((array_key_exists("preOrderQuantity", $context) && (isset($context["preOrderQuantity"]) ? $context["preOrderQuantity"] : null))) {
                echo " 
              <input type=\"button\" value=\"";
                // line 44
                echo (isset($context["text_error"]) ? $context["text_error"] : null);
                echo "\" id=\"\" class=\"btn btn-danger\" />
             ";
            } else {
                // line 45
                echo " 
              <input type=\"button\" value=\"";
                // line 46
                echo (isset($context["button_continue"]) ? $context["button_continue"] : null);
                echo "\" id=\"button-payment-method\" data-loading-text=\"";
                echo (isset($context["text_loading"]) ? $context["text_loading"] : null);
                echo "\" class=\"btn btn-primary\" />
             ";
            }
            // line 48
            echo "                            
  </div>
</div>
";
        } else {
            // line 52
            echo "<div class=\"buttons\">
  <div class=\"pull-right\">
    
            ";
            // line 55
            if ((array_key_exists("preOrderQuantity", $context) && (isset($context["preOrderQuantity"]) ? $context["preOrderQuantity"] : null))) {
                echo " 
              <input type=\"button\" value=\"";
                // line 56
                echo (isset($context["text_error"]) ? $context["text_error"] : null);
                echo "\" id=\"\" class=\"btn btn-danger\" />
             ";
            } else {
                // line 57
                echo " 
              <input type=\"button\" value=\"";
                // line 58
                echo (isset($context["button_continue"]) ? $context["button_continue"] : null);
                echo "\" id=\"button-payment-method\" data-loading-text=\"";
                echo (isset($context["text_loading"]) ? $context["text_loading"] : null);
                echo "\" class=\"btn btn-primary\" />
             ";
            }
            // line 60
            echo "                            
  </div>
</div>
";
        }
        // line 63
        echo " ";
    }

    public function getTemplateName()
    {
        return "default/template/checkout/payment_method.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  187 => 63,  181 => 60,  174 => 58,  171 => 57,  166 => 56,  162 => 55,  157 => 52,  151 => 48,  144 => 46,  141 => 45,  136 => 44,  132 => 43,  128 => 41,  124 => 39,  120 => 37,  118 => 36,  114 => 35,  111 => 34,  109 => 33,  104 => 31,  98 => 29,  89 => 25,  83 => 24,  81 => 23,  76 => 22,  70 => 20,  64 => 18,  61 => 17,  59 => 16,  56 => 15,  52 => 14,  47 => 13,  45 => 12,  39 => 10,  37 => 9,  34 => 8,  28 => 5,  22 => 2,  19 => 1,);
    }
}
/* */
/*                         {% if (preOrderQuantity is defined and preOrderQuantity) %} */
/*                             <div class="alert alert-danger">*/
/*                             <i class="fa fa-exclamation-circle"></i>*/
/*                             {{ error_preorder_quantity }} */
/*                             </div>*/
/*                         {% endif %}*/
/*                             */
/* {% if error_warning %}*/
/* <div class="alert alert-warning alert-dismissible"><i class="fa fa-exclamation-circle"></i> {{ error_warning }}</div>*/
/* {% endif %}*/
/* {% if payment_methods %}*/
/* <p>{{ text_payment_method }}</p>*/
/* {% for payment_method in payment_methods %}*/
/* <div class="radio">*/
/*   <label>{% if payment_method.code == code or not code %}*/
/*     {% set code = payment_method.code %}*/
/*     <input type="radio" name="payment_method" value="{{ payment_method.code }}" checked="checked" />*/
/*     {% else %}*/
/*     <input type="radio" name="payment_method" value="{{ payment_method.code }}" />*/
/*     {% endif %}*/
/*     {{ payment_method.title }}*/
/*     {% if payment_method.terms %}*/
/*     ({{ payment_method.terms }})*/
/*     {% endif %} </label>*/
/* </div>*/
/* {% endfor %}*/
/* {% endif %}*/
/* <p><strong>{{ text_comments }}</strong></p>*/
/* <p>*/
/*   <textarea name="comment" rows="8" class="form-control">{{ comment }}</textarea>*/
/* </p>*/
/* {% if text_agree %}*/
/* <div class="buttons">*/
/*   <div class="pull-right">{{ text_agree }}*/
/*     {% if agree %}*/
/*     <input type="checkbox" name="agree" value="1" checked="checked" />*/
/*     {% else %}*/
/*     <input type="checkbox" name="agree" value="1" />*/
/*     {% endif %}*/
/*     &nbsp;*/
/*     */
/*             {% if (preOrderQuantity is defined and preOrderQuantity) %} */
/*               <input type="button" value="{{ text_error }}" id="" class="btn btn-danger" />*/
/*              {% else %} */
/*               <input type="button" value="{{ button_continue }}" id="button-payment-method" data-loading-text="{{ text_loading }}" class="btn btn-primary" />*/
/*              {% endif %}*/
/*                             */
/*   </div>*/
/* </div>*/
/* {% else %}*/
/* <div class="buttons">*/
/*   <div class="pull-right">*/
/*     */
/*             {% if (preOrderQuantity is defined and preOrderQuantity) %} */
/*               <input type="button" value="{{ text_error }}" id="" class="btn btn-danger" />*/
/*              {% else %} */
/*               <input type="button" value="{{ button_continue }}" id="button-payment-method" data-loading-text="{{ text_loading }}" class="btn btn-primary" />*/
/*              {% endif %}*/
/*                             */
/*   </div>*/
/* </div>*/
/* {% endif %} */
