<?php

/* default/template/account/wk_enquiry_list.twig */
class __TwigTemplate_a0ec31fbd47776234aae9ff470a7d4fbe4712da340ea66bd61c2455d0e943dbf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo " 
<div class=\"container\">
  <ul class=\"breadcrumb\">
    ";
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            echo " 
    <li><a href=\"";
            // line 5
            echo $this->getAttribute($context["breadcrumb"], "href", array(), "array");
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array(), "array");
            echo "</a></li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 6
        echo " 
  </ul>
  ";
        // line 8
        if ((isset($context["success"]) ? $context["success"] : null)) {
            echo " 
  <div class=\"alert alert-success\"><i class=\"fa fa-check-circle\"></i>";
            // line 9
            echo (isset($context["success"]) ? $context["success"] : null);
            echo "</div>
  ";
        }
        // line 10
        echo " 
  <div class=\"row\">";
        // line 11
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo " 
    ";
        // line 12
        if (((isset($context["column_left"]) ? $context["column_left"] : null) && (isset($context["column_right"]) ? $context["column_right"] : null))) {
            echo " 
    ";
            // line 13
            $context["class"] = "col-sm-6";
            echo " 
    ";
        } elseif ((        // line 14
(isset($context["column_left"]) ? $context["column_left"] : null) || (isset($context["column_right"]) ? $context["column_right"] : null))) {
            echo " 
    ";
            // line 15
            $context["class"] = "col-sm-9";
            echo " 
    ";
        } else {
            // line 16
            echo " 
    ";
            // line 17
            $context["class"] = "col-sm-12";
            echo " 
    ";
        }
        // line 18
        echo " 
    <style>
        .table a.desc:after, .table a.DESC:after {
            content: \" \\f106\";
            font-family: FontAwesome;
            font-size: 14px;
        }
        .table a.asc:after, .table a.ASC:after {
            content: \" \\f107\";
            font-family: FontAwesome;
            font-size: 14px;
        }
    </style>
    <div id=\"content\" class=\"";
        // line 31
        echo (isset($context["class"]) ? $context["class"] : null);
        echo "\">";
        echo (isset($context["content_top"]) ? $context["content_top"] : null);
        echo " 
      <h1>
     ";
        // line 33
        echo (isset($context["heading_title_enquiry"]) ? $context["heading_title_enquiry"] : null);
        echo " 
        ";
        // line 34
        if ((array_key_exists("preorder_products", $context) && (isset($context["preorder_products"]) ? $context["preorder_products"] : null))) {
            echo " 
            <button class=\"btn btn-primary pull-right\" type=\"button\" data-toggle=\"modal\" data-target=\"#preorder-enquiry-modal\">
             ";
            // line 36
            echo (isset($context["text_on_enquiry"]) ? $context["text_on_enquiry"] : null);
            echo " 
            </button>
        ";
        } else {
            // line 38
            echo " 
            <button class=\"btn btn-danger pull-right\" type=\"button\" disabled >
                <i class=\"fa fa-exclamation\"></i>
              ";
            // line 41
            echo (isset($context["text_no_product_to_enquiry"]) ? $context["text_no_product_to_enquiry"] : null);
            echo " 
            </button>
        ";
        }
        // line 43
        echo " 
      </h1>
      <fieldset>
        <div class=\"well\">
            <form id=\"filter-form\">
                <div class=\"row\">
                    <div class=\"col-sm-4\">
                        <div class=\"form-group\">
                            <label class=\"control-label\"><b>";
        // line 51
        echo (isset($context["column_enquiry_id"]) ? $context["column_enquiry_id"] : null);
        echo "</b></label>
                            <input type=\"text\" class=\"form-control\" name=\"filter_enquiry_id\" value=\"";
        // line 52
        if (array_key_exists("filter_enquiry_id", $context)) {
            echo (isset($context["filter_enquiry_id"]) ? $context["filter_enquiry_id"] : null);
        }
        echo "\" />
                        </div>
                        <div class=\"form-group\">
                            <label class=\"control-label\"><b>";
        // line 55
        echo (isset($context["column_customer_name"]) ? $context["column_customer_name"] : null);
        echo "</b></label>
                            <input type=\"text\" class=\"form-control\" name=\"filter_customer_name\" value=\"";
        // line 56
        if (array_key_exists("filter_customer_name", $context)) {
            echo (isset($context["filter_customer_name"]) ? $context["filter_customer_name"] : null);
        }
        echo "\" />
                        </div>
                    </div>
                    <div class=\"col-sm-4\">
                        <div class=\"form-group\">
                            <label class=\"control-label\"><b>";
        // line 61
        echo (isset($context["column_email"]) ? $context["column_email"] : null);
        echo "</b></label>
                            <input type=\"text\" class=\"form-control\" name=\"filter_email\" value=\"";
        // line 62
        if (array_key_exists("filter_email", $context)) {
            echo (isset($context["filter_email"]) ? $context["filter_email"] : null);
        }
        echo "\" />
                        </div>
                        <div class=\"form-group\">
                            <label class=\"control-label\"><b>";
        // line 65
        echo (isset($context["column_product_name"]) ? $context["column_product_name"] : null);
        echo "</b></label>
                            <input type=\"text\" class=\"form-control\" name=\"filter_product_name\" value=\"";
        // line 66
        if (array_key_exists("filter_product_name", $context)) {
            echo (isset($context["filter_product_name"]) ? $context["filter_product_name"] : null);
        }
        echo "\" />
                        </div>
                    </div>
                    <div class=\"col-sm-4\">
                        <div class=\"form-group\">
                            <label class=\"control-label\"><b>";
        // line 71
        echo (isset($context["column_subject"]) ? $context["column_subject"] : null);
        echo "</b></label>
                            <input type=\"text\" class=\"form-control\" name=\"filter_subject\" value=\"";
        // line 72
        if (array_key_exists("filter_subject", $context)) {
            echo (isset($context["filter_subject"]) ? $context["filter_subject"] : null);
        }
        echo "\" />
                        </div>
                        <div class=\"form-group\">
                            <label class=\"control-label\"><b>";
        // line 75
        echo (isset($context["column_status"]) ? $context["column_status"] : null);
        echo "</b></label>
                            <select class=\"form-control\" name=\"filter_status\">
                                <option value=\"\"></option>
                                <option value=\"1\" ";
        // line 78
        if ((array_key_exists("filter_status", $context) && (isset($context["filter_status"]) ? $context["filter_status"] : null))) {
            echo "selected";
        }
        echo " >";
        echo (isset($context["text_open"]) ? $context["text_open"] : null);
        echo "</option>
                                <option value=\"0\" ";
        // line 79
        if (((array_key_exists("filter_status", $context) && ((isset($context["filter_status"]) ? $context["filter_status"] : null) != "")) &&  !(isset($context["filter_status"]) ? $context["filter_status"] : null))) {
            echo "selected";
        }
        echo " >";
        echo (isset($context["text_resolved"]) ? $context["text_resolved"] : null);
        echo "</option>
                            </select>
                        </div>
                        <div class=\"btn-group pull-right\">
                            <button class=\"btn btn-primary filter-btn\" type=\"button\" data-toggle=\"tooltip\" title=\"";
        // line 83
        echo (isset($context["button_filter"]) ? $context["button_filter"] : null);
        echo "\" ><i class=\"fa fa-filter\"></i></button>
                            <a href=\"";
        // line 84
        echo (isset($context["clear_filter"]) ? $context["clear_filter"] : null);
        echo "\" class=\"btn btn-default\" type=\"button\" data-toggle=\"tooltip\" title=\"";
        echo (isset($context["button_clear_filter"]) ? $context["button_clear_filter"] : null);
        echo "\" ><i class=\"fa fa-eraser\"></i></a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class=\"table-responsive\">
            <table class=\"table table-hover table-bordered\">
                <thead>
                    <tr>
                        <td class=\"text-left\"><b>
                             ";
        // line 95
        if (((isset($context["sort"]) ? $context["sort"] : null) == "pe.enquiry_id")) {
            echo " 
                                <a href=\"";
            // line 96
            echo (isset($context["sort_enquiry_id"]) ? $context["sort_enquiry_id"] : null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, (isset($context["order"]) ? $context["order"] : null));
            echo "\" >";
            echo (isset($context["column_enquiry_id"]) ? $context["column_enquiry_id"] : null);
            echo "</a>
                            ";
        } else {
            // line 97
            echo " 
                                <a href=\"";
            // line 98
            echo (isset($context["sort_enquiry_id"]) ? $context["sort_enquiry_id"] : null);
            echo "\" >";
            echo (isset($context["column_enquiry_id"]) ? $context["column_enquiry_id"] : null);
            echo "</a>
                            ";
        }
        // line 99
        echo " 
                        </b></td>
                        <td class=\"text-left\"><b>
                             ";
        // line 102
        if (((isset($context["sort"]) ? $context["sort"] : null) == "pe.customer_name")) {
            echo " 
                                <a href=\"";
            // line 103
            echo (isset($context["sort_customer_name"]) ? $context["sort_customer_name"] : null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, (isset($context["order"]) ? $context["order"] : null));
            echo "\" >";
            echo (isset($context["column_customer_name"]) ? $context["column_customer_name"] : null);
            echo "</a>
                            ";
        } else {
            // line 104
            echo " 
                                <a href=\"";
            // line 105
            echo (isset($context["sort_customer_name"]) ? $context["sort_customer_name"] : null);
            echo "\" >";
            echo (isset($context["column_customer_name"]) ? $context["column_customer_name"] : null);
            echo "</a>
                            ";
        }
        // line 106
        echo " 
                        </b></td>
                        <td class=\"text-left\"><b>
                             ";
        // line 109
        if (((isset($context["sort"]) ? $context["sort"] : null) == "pe.email")) {
            echo " 
                                <a href=\"";
            // line 110
            echo (isset($context["sort_email"]) ? $context["sort_email"] : null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, (isset($context["order"]) ? $context["order"] : null));
            echo "\" >";
            echo (isset($context["column_email"]) ? $context["column_email"] : null);
            echo "</a>
                            ";
        } else {
            // line 111
            echo " 
                                <a href=\"";
            // line 112
            echo (isset($context["sort_email"]) ? $context["sort_email"] : null);
            echo "\" >";
            echo (isset($context["column_email"]) ? $context["column_email"] : null);
            echo "</a>
                            ";
        }
        // line 113
        echo " 
                        </b></td>
                        <td class=\"text-left\"><b>
                             ";
        // line 116
        if (((isset($context["sort"]) ? $context["sort"] : null) == "pd.name")) {
            echo " 
                                <a href=\"";
            // line 117
            echo (isset($context["sort_name"]) ? $context["sort_name"] : null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, (isset($context["order"]) ? $context["order"] : null));
            echo "\" >";
            echo (isset($context["column_product_name"]) ? $context["column_product_name"] : null);
            echo "</a>
                            ";
        } else {
            // line 118
            echo " 
                                <a href=\"";
            // line 119
            echo (isset($context["sort_name"]) ? $context["sort_name"] : null);
            echo "\" >";
            echo (isset($context["column_product_name"]) ? $context["column_product_name"] : null);
            echo "</a>
                            ";
        }
        // line 120
        echo " 
                        </b></td>
                        <td class=\"text-center\"><b>";
        // line 122
        echo (isset($context["column_total_threads"]) ? $context["column_total_threads"] : null);
        echo "</b></td>
                        <td class=\"text-left\"><b>
                             ";
        // line 124
        if (((isset($context["sort"]) ? $context["sort"] : null) == "pe.subject")) {
            echo " 
                                <a href=\"";
            // line 125
            echo (isset($context["sort_subject"]) ? $context["sort_subject"] : null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, (isset($context["order"]) ? $context["order"] : null));
            echo "\" >";
            echo (isset($context["column_subject"]) ? $context["column_subject"] : null);
            echo "</a>
                            ";
        } else {
            // line 126
            echo " 
                                <a href=\"";
            // line 127
            echo (isset($context["sort_subject"]) ? $context["sort_subject"] : null);
            echo "\" >";
            echo (isset($context["column_subject"]) ? $context["column_subject"] : null);
            echo "</a>
                            ";
        }
        // line 128
        echo " 
                        </b></td>
                        <td class=\"text-left\"><b>
                             ";
        // line 131
        if (((isset($context["sort"]) ? $context["sort"] : null) == "pe.status")) {
            echo " 
                                <a href=\"";
            // line 132
            echo (isset($context["sort_status"]) ? $context["sort_status"] : null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, (isset($context["order"]) ? $context["order"] : null));
            echo "\" >";
            echo (isset($context["column_status"]) ? $context["column_status"] : null);
            echo "</a>
                            ";
        } else {
            // line 133
            echo " 
                                <a href=\"";
            // line 134
            echo (isset($context["sort_status"]) ? $context["sort_status"] : null);
            echo "\" >";
            echo (isset($context["column_status"]) ? $context["column_status"] : null);
            echo "</a>
                            ";
        }
        // line 135
        echo " 
                        </b></td>
                        <td class=\"text-center\"><b>";
        // line 137
        echo (isset($context["column_action"]) ? $context["column_action"] : null);
        echo "</b></td>
                    </tr>
                </thead>
                <tbody>
                    ";
        // line 141
        if ((array_key_exists("enquires", $context) && (isset($context["enquires"]) ? $context["enquires"] : null))) {
            echo " 
                        ";
            // line 142
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["enquires"]) ? $context["enquires"] : null));
            foreach ($context['_seq'] as $context["index"] => $context["enquiry"]) {
                echo " 
                            <tr>
                                <td class=\"text-left\">";
                // line 144
                echo $this->getAttribute($context["enquiry"], "enquiry_id", array(), "array");
                echo "</td>
                                <td class=\"text-left\">";
                // line 145
                echo $this->getAttribute($context["enquiry"], "customer_name", array(), "array");
                echo "</td>
                                <td class=\"text-left\">";
                // line 146
                echo $this->getAttribute($context["enquiry"], "email", array(), "array");
                echo "</td>
                                <td class=\"text-left\">";
                // line 147
                echo $this->getAttribute($context["enquiry"], "product_name", array(), "array");
                echo "</td>
                                <td class=\"text-center\">
                                    <span class=\"badge\">
                                     ";
                // line 150
                echo $this->getAttribute($context["enquiry"], "total_thread", array(), "array");
                echo " 
                                    </span>
                                </td>
                                <td class=\"text-left\">";
                // line 153
                echo $this->getAttribute($context["enquiry"], "subject", array(), "array");
                echo "</td>
                                <td class=\"text-left\">";
                // line 154
                echo (($this->getAttribute($context["enquiry"], "status", array(), "array")) ? ((isset($context["text_open"]) ? $context["text_open"] : null)) : ((isset($context["text_resolved"]) ? $context["text_resolved"] : null)));
                echo "</td>
                                <td class=\"text-center\">
                                    <a href=\"";
                // line 156
                echo $this->getAttribute($context["enquiry"], "view", array(), "array");
                echo "\" class=\"btn btn-primary\">
                                        <i class=\"fa fa-eye\"></i>
                                    </a>
                                </td>
                            </tr>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['index'], $context['enquiry'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 161
            echo " 
                    ";
        } else {
            // line 162
            echo " 
                        <tr>
                            <td class=\"text-center\" colspan=\"8\">";
            // line 164
            echo (isset($context["text_no_record"]) ? $context["text_no_record"] : null);
            echo "</td>
                        </tr>
                    ";
        }
        // line 166
        echo " 
                </tbody>
            </table>
        </div>
    </fieldset>
    <div class=\"row\">
    <div class=\"col-sm-6 text-left\">";
        // line 172
        echo (isset($context["pagination"]) ? $context["pagination"] : null);
        echo "</div>
    <div class=\"col-sm-6 text-right\">";
        // line 173
        echo (isset($context["results"]) ? $context["results"] : null);
        echo "</div>
    </div>
 ";
        // line 175
        echo (isset($context["content_bottom"]) ? $context["content_bottom"] : null);
        echo "</div>
 ";
        // line 176
        echo (isset($context["column_right"]) ? $context["column_right"] : null);
        echo "</div>
</div>
<script>
    function enableSubmitButton() {
        \$('.preorder-button').removeClass('hide');
    }
    function disableSubmitButton() {
        \$('.preorder-button').addClass('hide');
    }
    var onloadCallback = function() {
        preorder_captcha_id = grecaptcha.render('preorder-catpcha', {
            'sitekey' : '";
        // line 187
        echo (isset($context["module_wk_preorder_pro_site_key"]) ? $context["module_wk_preorder_pro_site_key"] : null);
        echo "',
            'callback': 'enableSubmitButton',
            'expired-callback' : 'disableSubmitButton',
        });
    };
</script>
<script src=\"https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit\" async defer></script>
    <div class=\"modal fade\" id=\"preorder-enquiry-modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\">
        <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
            <h4 class=\"modal-title\" id=\"myModalLabel\">";
        // line 199
        echo twig_slice($this->env, (isset($context["text_enquiry_heading"]) ? $context["text_enquiry_heading"] : null), 0, 12);
        echo "<span id=\"product-name-span\"></span></h4>
            </div>
            <div class=\"modal-body\">
            <form id=\"preorder-enquiry-form\" class=\"form-horizontal\" >
                <input type=\"hidden\" name=\"preorder_customer_id\" value=\"";
        // line 203
        echo (isset($context["customer_id"]) ? $context["customer_id"] : null);
        echo "\" />
                <div class=\"form-group\">
                    <label class=\"col-sm-3 control-label\" >
                        <b>";
        // line 206
        echo (isset($context["column_product_name"]) ? $context["column_product_name"] : null);
        echo "</b>
                    </label>
                    <div class=\"col-sm-9\">
                        <select name=\"preorder_product_id\" class=\"form-control\" >
                            ";
        // line 210
        if ((array_key_exists("preorder_products", $context) && (isset($context["preorder_products"]) ? $context["preorder_products"] : null))) {
            echo " 
                                ";
            // line 211
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["preorder_products"]) ? $context["preorder_products"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["preorder_product"]) {
                echo " 
                                    <option value=\"";
                // line 212
                echo $this->getAttribute($context["preorder_product"], "product_id", array(), "array");
                echo "\">";
                echo $this->getAttribute($context["preorder_product"], "name", array(), "array");
                echo "</option>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['preorder_product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 213
            echo " 
                            ";
        }
        // line 214
        echo " 
                        </select>
                    </div>
                </div>
                <div class=\"form-group required\">
                    <label class=\"col-sm-3 control-label\" >
                        <b>";
        // line 220
        echo (isset($context["text_name"]) ? $context["text_name"] : null);
        echo "</b>
                    </label>
                    <div class=\"col-sm-9\">
                        <input type=\"text\" class=\"form-control\" name=\"preorder_name\" value=\"";
        // line 223
        if (array_key_exists("customer_name", $context)) {
            echo (isset($context["customer_name"]) ? $context["customer_name"] : null);
        }
        echo "\" />
                    </div>
                </div>
                <div class=\"form-group required\">
                <label class=\"col-sm-3 control-label\" >
                    <b>";
        // line 228
        echo (isset($context["text_email"]) ? $context["text_email"] : null);
        echo "</b>
                </label>
                <div class=\"col-sm-9\">
                    <input type=\"email\" class=\"form-control\" name=\"preorder_email\" value=\"";
        // line 231
        if (array_key_exists("customer_email", $context)) {
            echo (isset($context["customer_email"]) ? $context["customer_email"] : null);
        }
        echo "\" />
                </div>
                </div>
                <div class=\"form-group required\">
                <label class=\"col-sm-3 control-label\" >
                    <b>";
        // line 236
        echo (isset($context["text_subject"]) ? $context["text_subject"] : null);
        echo "</b>
                </label>
                <div class=\"col-sm-9\">
                    <input type=\"text\" class=\"form-control\" name=\"preorder_subject\" />
                </div>
                </div>
                <div class=\"form-group required\">
                <label class=\"col-sm-3 control-label\" >
                    <b>";
        // line 244
        echo (isset($context["text_enquiry"]) ? $context["text_enquiry"] : null);
        echo "</b>
                </label>
                <div class=\"col-sm-9\">
                    <textarea class=\"form-control\" style=\"min-height:100px\" name=\"preorder_query\" ></textarea>
                </div>
                </div>
                <div class=\"form-group\">
                <label class=\"col-sm-3 control-label\" >
                    <b>";
        // line 252
        echo (isset($context["text_captcha"]) ? $context["text_captcha"] : null);
        echo "</b>
                </label>
                <div class=\"col-sm-9\">
                    <div id=\"preorder-catpcha\" ></div>
                </div>
                </div>
            </form>
            </div>
            <div class=\"modal-footer\">
            <button type=\"button\" class=\"btn btn-danger pull-left\" data-dismiss=\"modal\">";
        // line 261
        echo (isset($context["text_close"]) ? $context["text_close"] : null);
        echo "</button>
            <button type=\"button\" class=\"btn btn-primary preorder-button hide\">";
        // line 262
        echo (isset($context["text_submit"]) ? $context["text_submit"] : null);
        echo "</button>
            </div>
        </div>
        </div>
    </div>
</script>
<script>

    \$('.filter-btn').on('click', function() {
        filter_data = \$('#filter-form').serialize();
        location = 'index.php?route=account/wk_enquiry_list&'+filter_data;
    })

    \$('select[name=\"preorder_product_id\"]').on('change', function() {
        \$('span#product-name-span').text(\$(this).find(\"option:selected\").text());
    })
    \$('select[name=\"preorder_product_id\"]').trigger('change');

    \$('.preorder-button').on('click', function(){
        var form_data = \$('#preorder-enquiry-form').serializeArray();
        \$.ajax({
            url: 'index.php?route=checkout/precart/generateEnquiry',
            data: form_data,
            method: 'post',
            dataType: 'json',
            beforeSend: function() {
                \$('.enquiry-success').remove();
                \$('.preorder-button').button('loading');
                \$('input[name=\"preorder_name\"]').next('.text-danger').remove();
                \$('input[name=\"preorder_name\"]').parent().parent().removeClass('has-error')
                \$('input[name=\"preorder_email\"]').next('.text-danger').remove();
                \$('input[name=\"preorder_email\"]').parent().parent().removeClass('has-error')
                \$('input[name=\"preorder_subject\"]').next('.text-danger').remove();
                \$('input[name=\"preorder_subject\"]').parent().parent().removeClass('has-error')
                \$('textarea[name=\"preorder_query\"]').next('.text-danger').remove();
                \$('textarea[name=\"preorder_name\"]').parent().parent().removeClass('has-error')
                \$('#preorder-catpcha').next('.text-danger').remove();
                \$('#preorder-catpcha').parent().parent().removeClass('has-error')
            },
            complete: function() {
                \$('.preorder-button').button('reset');
            },
            success: function(response) {
                grecaptcha.reset(preorder_captcha_id);
                disableSubmitButton();
                if(response.success == false) {
                    if(response.error_name) {
                        \$('input[name=\"preorder_name\"]').after('<div class=\"text-danger\">'+response.error_name+'</div>').parent().parent().addClass('has-error');
                    }
                    if(response.error_email) {
                        \$('input[name=\"preorder_email\"]').after('<div class=\"text-danger\">'+response.error_email+'</div>').parent().parent().addClass('has-error');
                    }
                    if(response.error_subject) {
                        \$('input[name=\"preorder_subject\"]').after('<div class=\"text-danger\">'+response.error_subject+'</div>').parent().parent().addClass('has-error');
                    }
                    if(response.error_query) {
                        \$('textarea[name=\"preorder_query\"]').after('<div class=\"text-danger\">'+response.error_query+'</div>').parent().parent().addClass('has-error');
                    }
                    if(response.error_captcha) {
                        \$('#preorder-catpcha').after('<div class=\"text-danger\">'+response.error_captcha+'</div>').parent().parent().addClass('has-error');
                    }
                } else {
                    \$('input[name=\"preorder_subject\"]').val('');
                    \$('textarea[name=\"preorder_query\"]').val('');
                    \$('.modal-body').append('<div class=\"alert alert-success enquiry-success\"><i class=\"fa fa-check-o\"></i>'+response.success_msg+'<button class=\"close\" data-dismiss=\"alert\">&times;</button></div>');
                }
            },
        })
    });
</script>
";
        // line 332
        echo (isset($context["footer"]) ? $context["footer"] : null);
        echo " 
";
    }

    public function getTemplateName()
    {
        return "default/template/account/wk_enquiry_list.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  746 => 332,  673 => 262,  669 => 261,  657 => 252,  646 => 244,  635 => 236,  625 => 231,  619 => 228,  609 => 223,  603 => 220,  595 => 214,  591 => 213,  581 => 212,  575 => 211,  571 => 210,  564 => 206,  558 => 203,  551 => 199,  536 => 187,  522 => 176,  518 => 175,  513 => 173,  509 => 172,  501 => 166,  495 => 164,  491 => 162,  487 => 161,  475 => 156,  470 => 154,  466 => 153,  460 => 150,  454 => 147,  450 => 146,  446 => 145,  442 => 144,  435 => 142,  431 => 141,  424 => 137,  420 => 135,  413 => 134,  410 => 133,  401 => 132,  397 => 131,  392 => 128,  385 => 127,  382 => 126,  373 => 125,  369 => 124,  364 => 122,  360 => 120,  353 => 119,  350 => 118,  341 => 117,  337 => 116,  332 => 113,  325 => 112,  322 => 111,  313 => 110,  309 => 109,  304 => 106,  297 => 105,  294 => 104,  285 => 103,  281 => 102,  276 => 99,  269 => 98,  266 => 97,  257 => 96,  253 => 95,  237 => 84,  233 => 83,  222 => 79,  214 => 78,  208 => 75,  200 => 72,  196 => 71,  186 => 66,  182 => 65,  174 => 62,  170 => 61,  160 => 56,  156 => 55,  148 => 52,  144 => 51,  134 => 43,  128 => 41,  123 => 38,  117 => 36,  112 => 34,  108 => 33,  101 => 31,  86 => 18,  81 => 17,  78 => 16,  73 => 15,  69 => 14,  65 => 13,  61 => 12,  57 => 11,  54 => 10,  49 => 9,  45 => 8,  41 => 6,  31 => 5,  25 => 4,  19 => 1,);
    }
}
/* {{ header }} */
/* <div class="container">*/
/*   <ul class="breadcrumb">*/
/*     {% for breadcrumb in breadcrumbs %} */
/*     <li><a href="{{ breadcrumb['href'] }}">{{ breadcrumb['text'] }}</a></li>*/
/*     {% endfor %} */
/*   </ul>*/
/*   {% if (success) %} */
/*   <div class="alert alert-success"><i class="fa fa-check-circle"></i>{{ success }}</div>*/
/*   {% endif %} */
/*   <div class="row">{{ column_left }} */
/*     {% if (column_left and column_right) %} */
/*     {% set class = 'col-sm-6' %} */
/*     {% elseif (column_left or column_right) %} */
/*     {% set class = 'col-sm-9' %} */
/*     {% else %} */
/*     {% set class = 'col-sm-12' %} */
/*     {% endif %} */
/*     <style>*/
/*         .table a.desc:after, .table a.DESC:after {*/
/*             content: " \f106";*/
/*             font-family: FontAwesome;*/
/*             font-size: 14px;*/
/*         }*/
/*         .table a.asc:after, .table a.ASC:after {*/
/*             content: " \f107";*/
/*             font-family: FontAwesome;*/
/*             font-size: 14px;*/
/*         }*/
/*     </style>*/
/*     <div id="content" class="{{ class }}">{{ content_top }} */
/*       <h1>*/
/*      {{ heading_title_enquiry }} */
/*         {% if (preorder_products is defined and preorder_products) %} */
/*             <button class="btn btn-primary pull-right" type="button" data-toggle="modal" data-target="#preorder-enquiry-modal">*/
/*              {{ text_on_enquiry }} */
/*             </button>*/
/*         {% else %} */
/*             <button class="btn btn-danger pull-right" type="button" disabled >*/
/*                 <i class="fa fa-exclamation"></i>*/
/*               {{ text_no_product_to_enquiry }} */
/*             </button>*/
/*         {% endif %} */
/*       </h1>*/
/*       <fieldset>*/
/*         <div class="well">*/
/*             <form id="filter-form">*/
/*                 <div class="row">*/
/*                     <div class="col-sm-4">*/
/*                         <div class="form-group">*/
/*                             <label class="control-label"><b>{{ column_enquiry_id }}</b></label>*/
/*                             <input type="text" class="form-control" name="filter_enquiry_id" value="{% if (filter_enquiry_id is defined) %}{{ filter_enquiry_id }}{% endif %}" />*/
/*                         </div>*/
/*                         <div class="form-group">*/
/*                             <label class="control-label"><b>{{ column_customer_name }}</b></label>*/
/*                             <input type="text" class="form-control" name="filter_customer_name" value="{% if (filter_customer_name is defined) %}{{ filter_customer_name }}{% endif %}" />*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="col-sm-4">*/
/*                         <div class="form-group">*/
/*                             <label class="control-label"><b>{{ column_email }}</b></label>*/
/*                             <input type="text" class="form-control" name="filter_email" value="{% if (filter_email is defined) %}{{ filter_email }}{% endif %}" />*/
/*                         </div>*/
/*                         <div class="form-group">*/
/*                             <label class="control-label"><b>{{ column_product_name }}</b></label>*/
/*                             <input type="text" class="form-control" name="filter_product_name" value="{% if (filter_product_name is defined) %}{{ filter_product_name }}{% endif %}" />*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="col-sm-4">*/
/*                         <div class="form-group">*/
/*                             <label class="control-label"><b>{{ column_subject }}</b></label>*/
/*                             <input type="text" class="form-control" name="filter_subject" value="{% if (filter_subject is defined) %}{{ filter_subject }}{% endif %}" />*/
/*                         </div>*/
/*                         <div class="form-group">*/
/*                             <label class="control-label"><b>{{ column_status }}</b></label>*/
/*                             <select class="form-control" name="filter_status">*/
/*                                 <option value=""></option>*/
/*                                 <option value="1" {% if (filter_status is defined and filter_status) %}{{ "selected" }}{% endif %} >{{ text_open }}</option>*/
/*                                 <option value="0" {% if (filter_status is defined and filter_status != '' and not filter_status) %}{{ "selected" }}{% endif %} >{{ text_resolved }}</option>*/
/*                             </select>*/
/*                         </div>*/
/*                         <div class="btn-group pull-right">*/
/*                             <button class="btn btn-primary filter-btn" type="button" data-toggle="tooltip" title="{{ button_filter }}" ><i class="fa fa-filter"></i></button>*/
/*                             <a href="{{ clear_filter }}" class="btn btn-default" type="button" data-toggle="tooltip" title="{{ button_clear_filter }}" ><i class="fa fa-eraser"></i></a>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </form>*/
/*         </div>*/
/*         <div class="table-responsive">*/
/*             <table class="table table-hover table-bordered">*/
/*                 <thead>*/
/*                     <tr>*/
/*                         <td class="text-left"><b>*/
/*                              {% if (sort == 'pe.enquiry_id') %} */
/*                                 <a href="{{ sort_enquiry_id }}" class="{{ order | lower }}" >{{ column_enquiry_id }}</a>*/
/*                             {% else %} */
/*                                 <a href="{{ sort_enquiry_id }}" >{{ column_enquiry_id }}</a>*/
/*                             {% endif %} */
/*                         </b></td>*/
/*                         <td class="text-left"><b>*/
/*                              {% if (sort == 'pe.customer_name') %} */
/*                                 <a href="{{ sort_customer_name }}" class="{{ order | lower }}" >{{ column_customer_name }}</a>*/
/*                             {% else %} */
/*                                 <a href="{{ sort_customer_name }}" >{{ column_customer_name }}</a>*/
/*                             {% endif %} */
/*                         </b></td>*/
/*                         <td class="text-left"><b>*/
/*                              {% if (sort == 'pe.email') %} */
/*                                 <a href="{{ sort_email }}" class="{{ order | lower }}" >{{ column_email }}</a>*/
/*                             {% else %} */
/*                                 <a href="{{ sort_email }}" >{{ column_email }}</a>*/
/*                             {% endif %} */
/*                         </b></td>*/
/*                         <td class="text-left"><b>*/
/*                              {% if (sort == 'pd.name') %} */
/*                                 <a href="{{ sort_name }}" class="{{ order | lower }}" >{{ column_product_name }}</a>*/
/*                             {% else %} */
/*                                 <a href="{{ sort_name }}" >{{ column_product_name }}</a>*/
/*                             {% endif %} */
/*                         </b></td>*/
/*                         <td class="text-center"><b>{{ column_total_threads }}</b></td>*/
/*                         <td class="text-left"><b>*/
/*                              {% if (sort == 'pe.subject') %} */
/*                                 <a href="{{ sort_subject }}" class="{{ order | lower }}" >{{ column_subject }}</a>*/
/*                             {% else %} */
/*                                 <a href="{{ sort_subject }}" >{{ column_subject }}</a>*/
/*                             {% endif %} */
/*                         </b></td>*/
/*                         <td class="text-left"><b>*/
/*                              {% if (sort == 'pe.status') %} */
/*                                 <a href="{{ sort_status }}" class="{{ order | lower }}" >{{ column_status }}</a>*/
/*                             {% else %} */
/*                                 <a href="{{ sort_status }}" >{{ column_status }}</a>*/
/*                             {% endif %} */
/*                         </b></td>*/
/*                         <td class="text-center"><b>{{ column_action }}</b></td>*/
/*                     </tr>*/
/*                 </thead>*/
/*                 <tbody>*/
/*                     {% if (enquires is defined and enquires) %} */
/*                         {% for index,enquiry in enquires %} */
/*                             <tr>*/
/*                                 <td class="text-left">{{ enquiry['enquiry_id'] }}</td>*/
/*                                 <td class="text-left">{{ enquiry['customer_name'] }}</td>*/
/*                                 <td class="text-left">{{ enquiry['email'] }}</td>*/
/*                                 <td class="text-left">{{ enquiry['product_name'] }}</td>*/
/*                                 <td class="text-center">*/
/*                                     <span class="badge">*/
/*                                      {{ enquiry['total_thread'] }} */
/*                                     </span>*/
/*                                 </td>*/
/*                                 <td class="text-left">{{ enquiry['subject'] }}</td>*/
/*                                 <td class="text-left">{{ enquiry['status'] ? text_open : text_resolved }}</td>*/
/*                                 <td class="text-center">*/
/*                                     <a href="{{ enquiry['view'] }}" class="btn btn-primary">*/
/*                                         <i class="fa fa-eye"></i>*/
/*                                     </a>*/
/*                                 </td>*/
/*                             </tr>*/
/*                         {% endfor %} */
/*                     {% else %} */
/*                         <tr>*/
/*                             <td class="text-center" colspan="8">{{ text_no_record }}</td>*/
/*                         </tr>*/
/*                     {% endif %} */
/*                 </tbody>*/
/*             </table>*/
/*         </div>*/
/*     </fieldset>*/
/*     <div class="row">*/
/*     <div class="col-sm-6 text-left">{{ pagination }}</div>*/
/*     <div class="col-sm-6 text-right">{{ results }}</div>*/
/*     </div>*/
/*  {{ content_bottom }}</div>*/
/*  {{ column_right }}</div>*/
/* </div>*/
/* <script>*/
/*     function enableSubmitButton() {*/
/*         $('.preorder-button').removeClass('hide');*/
/*     }*/
/*     function disableSubmitButton() {*/
/*         $('.preorder-button').addClass('hide');*/
/*     }*/
/*     var onloadCallback = function() {*/
/*         preorder_captcha_id = grecaptcha.render('preorder-catpcha', {*/
/*             'sitekey' : '{{ module_wk_preorder_pro_site_key }}',*/
/*             'callback': 'enableSubmitButton',*/
/*             'expired-callback' : 'disableSubmitButton',*/
/*         });*/
/*     };*/
/* </script>*/
/* <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>*/
/*     <div class="modal fade" id="preorder-enquiry-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">*/
/*         <div class="modal-dialog" role="document">*/
/*         <div class="modal-content">*/
/*             <div class="modal-header">*/
/*             <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>*/
/*             <h4 class="modal-title" id="myModalLabel">{{ text_enquiry_heading|slice(0, 12) }}<span id="product-name-span"></span></h4>*/
/*             </div>*/
/*             <div class="modal-body">*/
/*             <form id="preorder-enquiry-form" class="form-horizontal" >*/
/*                 <input type="hidden" name="preorder_customer_id" value="{{ customer_id }}" />*/
/*                 <div class="form-group">*/
/*                     <label class="col-sm-3 control-label" >*/
/*                         <b>{{ column_product_name }}</b>*/
/*                     </label>*/
/*                     <div class="col-sm-9">*/
/*                         <select name="preorder_product_id" class="form-control" >*/
/*                             {% if (preorder_products is defined and preorder_products) %} */
/*                                 {% for preorder_product in preorder_products %} */
/*                                     <option value="{{ preorder_product['product_id'] }}">{{ preorder_product['name'] }}</option>*/
/*                                 {% endfor %} */
/*                             {% endif %} */
/*                         </select>*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="form-group required">*/
/*                     <label class="col-sm-3 control-label" >*/
/*                         <b>{{ text_name }}</b>*/
/*                     </label>*/
/*                     <div class="col-sm-9">*/
/*                         <input type="text" class="form-control" name="preorder_name" value="{% if (customer_name is defined) %}{{ customer_name }}{% endif %}" />*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="form-group required">*/
/*                 <label class="col-sm-3 control-label" >*/
/*                     <b>{{ text_email }}</b>*/
/*                 </label>*/
/*                 <div class="col-sm-9">*/
/*                     <input type="email" class="form-control" name="preorder_email" value="{% if (customer_email is defined) %}{{ customer_email }}{% endif %}" />*/
/*                 </div>*/
/*                 </div>*/
/*                 <div class="form-group required">*/
/*                 <label class="col-sm-3 control-label" >*/
/*                     <b>{{ text_subject }}</b>*/
/*                 </label>*/
/*                 <div class="col-sm-9">*/
/*                     <input type="text" class="form-control" name="preorder_subject" />*/
/*                 </div>*/
/*                 </div>*/
/*                 <div class="form-group required">*/
/*                 <label class="col-sm-3 control-label" >*/
/*                     <b>{{ text_enquiry }}</b>*/
/*                 </label>*/
/*                 <div class="col-sm-9">*/
/*                     <textarea class="form-control" style="min-height:100px" name="preorder_query" ></textarea>*/
/*                 </div>*/
/*                 </div>*/
/*                 <div class="form-group">*/
/*                 <label class="col-sm-3 control-label" >*/
/*                     <b>{{ text_captcha }}</b>*/
/*                 </label>*/
/*                 <div class="col-sm-9">*/
/*                     <div id="preorder-catpcha" ></div>*/
/*                 </div>*/
/*                 </div>*/
/*             </form>*/
/*             </div>*/
/*             <div class="modal-footer">*/
/*             <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">{{ text_close }}</button>*/
/*             <button type="button" class="btn btn-primary preorder-button hide">{{ text_submit }}</button>*/
/*             </div>*/
/*         </div>*/
/*         </div>*/
/*     </div>*/
/* </script>*/
/* <script>*/
/* */
/*     $('.filter-btn').on('click', function() {*/
/*         filter_data = $('#filter-form').serialize();*/
/*         location = 'index.php?route=account/wk_enquiry_list&'+filter_data;*/
/*     })*/
/* */
/*     $('select[name="preorder_product_id"]').on('change', function() {*/
/*         $('span#product-name-span').text($(this).find("option:selected").text());*/
/*     })*/
/*     $('select[name="preorder_product_id"]').trigger('change');*/
/* */
/*     $('.preorder-button').on('click', function(){*/
/*         var form_data = $('#preorder-enquiry-form').serializeArray();*/
/*         $.ajax({*/
/*             url: 'index.php?route=checkout/precart/generateEnquiry',*/
/*             data: form_data,*/
/*             method: 'post',*/
/*             dataType: 'json',*/
/*             beforeSend: function() {*/
/*                 $('.enquiry-success').remove();*/
/*                 $('.preorder-button').button('loading');*/
/*                 $('input[name="preorder_name"]').next('.text-danger').remove();*/
/*                 $('input[name="preorder_name"]').parent().parent().removeClass('has-error')*/
/*                 $('input[name="preorder_email"]').next('.text-danger').remove();*/
/*                 $('input[name="preorder_email"]').parent().parent().removeClass('has-error')*/
/*                 $('input[name="preorder_subject"]').next('.text-danger').remove();*/
/*                 $('input[name="preorder_subject"]').parent().parent().removeClass('has-error')*/
/*                 $('textarea[name="preorder_query"]').next('.text-danger').remove();*/
/*                 $('textarea[name="preorder_name"]').parent().parent().removeClass('has-error')*/
/*                 $('#preorder-catpcha').next('.text-danger').remove();*/
/*                 $('#preorder-catpcha').parent().parent().removeClass('has-error')*/
/*             },*/
/*             complete: function() {*/
/*                 $('.preorder-button').button('reset');*/
/*             },*/
/*             success: function(response) {*/
/*                 grecaptcha.reset(preorder_captcha_id);*/
/*                 disableSubmitButton();*/
/*                 if(response.success == false) {*/
/*                     if(response.error_name) {*/
/*                         $('input[name="preorder_name"]').after('<div class="text-danger">'+response.error_name+'</div>').parent().parent().addClass('has-error');*/
/*                     }*/
/*                     if(response.error_email) {*/
/*                         $('input[name="preorder_email"]').after('<div class="text-danger">'+response.error_email+'</div>').parent().parent().addClass('has-error');*/
/*                     }*/
/*                     if(response.error_subject) {*/
/*                         $('input[name="preorder_subject"]').after('<div class="text-danger">'+response.error_subject+'</div>').parent().parent().addClass('has-error');*/
/*                     }*/
/*                     if(response.error_query) {*/
/*                         $('textarea[name="preorder_query"]').after('<div class="text-danger">'+response.error_query+'</div>').parent().parent().addClass('has-error');*/
/*                     }*/
/*                     if(response.error_captcha) {*/
/*                         $('#preorder-catpcha').after('<div class="text-danger">'+response.error_captcha+'</div>').parent().parent().addClass('has-error');*/
/*                     }*/
/*                 } else {*/
/*                     $('input[name="preorder_subject"]').val('');*/
/*                     $('textarea[name="preorder_query"]').val('');*/
/*                     $('.modal-body').append('<div class="alert alert-success enquiry-success"><i class="fa fa-check-o"></i>'+response.success_msg+'<button class="close" data-dismiss="alert">&times;</button></div>');*/
/*                 }*/
/*             },*/
/*         })*/
/*     });*/
/* </script>*/
/* {{ footer }} */
/* */
