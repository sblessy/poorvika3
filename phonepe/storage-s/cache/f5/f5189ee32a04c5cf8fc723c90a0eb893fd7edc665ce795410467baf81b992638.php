<?php

/* so-destino/template/mobile/home.twig */
class __TwigTemplate_a71101dda4d7d4ddaa1ef86c0b502e8fed3ab3136667dbbdcb5e3157702c13ed extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        echo (isset($context["header"]) ? $context["header"] : null);
        echo "

    
    <div id=\"content\">
        
        ";
        // line 7
        echo (isset($context["content_mobile"]) ? $context["content_mobile"] : null);
        echo "
   
    </div>
   

";
        // line 12
        echo (isset($context["footer"]) ? $context["footer"] : null);
    }

    public function getTemplateName()
    {
        return "so-destino/template/mobile/home.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 12,  30 => 7,  22 => 2,  19 => 1,);
    }
}
/* */
/* {{ header }}*/
/* */
/*     */
/*     <div id="content">*/
/*         */
/*         {{ content_mobile }}*/
/*    */
/*     </div>*/
/*    */
/* */
/* {{ footer }}*/
