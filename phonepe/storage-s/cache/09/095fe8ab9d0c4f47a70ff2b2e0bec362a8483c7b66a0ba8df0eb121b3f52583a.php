<?php

/* extension/payment/pinepg.twig */
class __TwigTemplate_c247a434cd75805cf4f33a60c75b491235b13e79958393c21925ef0c96737946 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\">
        <button type=\"submit\" form=\"form-pinepg\" data-toggle=\"tooltip\" title=\"";
        // line 6
        echo (isset($context["button_save"]) ? $context["button_save"] : null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i></button>
        <a href=\"";
        // line 7
        echo (isset($context["cancel"]) ? $context["cancel"] : null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo (isset($context["button_cancel"]) ? $context["button_cancel"] : null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
      <h1>";
        // line 8
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
      <ul class=\"breadcrumb\">
        ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 11
            echo "        <li><a href=\"";
            echo $this->getAttribute($context["breadcrumb"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array());
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "      </ul>
    </div>
  </div>
  <div class=\"container-fluid\">
    ";
        // line 17
        if ((isset($context["error_warning"]) ? $context["error_warning"] : null)) {
            // line 18
            echo "    <div class=\"alert alert-danger\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo (isset($context["error_warning"]) ? $context["error_warning"] : null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 22
        echo "    <div class=\"panel panel-default\">
      
      <div class=\"panel-body\">
        <form action=\"";
        // line 25
        echo (isset($context["action"]) ? $context["action"] : null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-pinepg\" class=\"form-horizontal\">
            <ul class=\"nav nav-tabs\">
            <li class=\"active\"><a href=\"#tab-general\" data-toggle=\"tab\">";
        // line 27
        echo (isset($context["tab_general"]) ? $context["tab_general"] : null);
        echo "</a></li>
           
          </ul>
      <div class=\"tab-content\">
        <div class=\"tab-pane active\" id=\"tab-general\"> 
          <div class=\"form-group required\">
            <label class=\"col-sm-2 control-label\" for=\"input-merchant\">";
        // line 33
        echo (isset($context["entry_merchantid"]) ? $context["entry_merchantid"] : null);
        echo "</label>
            <div class=\"col-sm-10\">
              <input type=\"text\" name=\"payment_pinepg_merchantid\" value=\"";
        // line 35
        echo (isset($context["payment_pinepg_merchantid"]) ? $context["payment_pinepg_merchantid"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_merchantid"]) ? $context["entry_merchantid"] : null);
        echo "\" id=\"input-merchant\" class=\"form-control\" />
              ";
        // line 36
        if ((isset($context["error_merchant"]) ? $context["error_merchant"] : null)) {
            // line 37
            echo "              <div class=\"text-danger\">";
            echo (isset($context["error_merchant"]) ? $context["error_merchant"] : null);
            echo "</div>
              ";
        }
        // line 39
        echo "            </div>
          </div>
         
            <div class=\"form-group required\">
            <label class=\"col-sm-2 control-label\" for=\"input-access_code\">";
        // line 43
        echo (isset($context["entry_access_code"]) ? $context["entry_access_code"] : null);
        echo "</label>
            <div class=\"col-sm-10\">
              <input type=\"text\" name=\"payment_pinepg_access_code\" value=\"";
        // line 45
        echo (isset($context["payment_pinepg_access_code"]) ? $context["payment_pinepg_access_code"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_access_code"]) ? $context["entry_access_code"] : null);
        echo "\" id=\"input-access_code\" class=\"form-control\" />
              ";
        // line 46
        if ((isset($context["error_access_code"]) ? $context["error_access_code"] : null)) {
            // line 47
            echo "              <div class=\"text-danger\">";
            echo (isset($context["error_access_code"]) ? $context["error_access_code"] : null);
            echo "</div>
              ";
        }
        // line 49
        echo "            </div>
          </div>
         <div class=\"form-group required\">
            <label class=\"col-sm-2 control-label\" for=\"input-secure_secret\">";
        // line 52
        echo (isset($context["entry_secure_secret"]) ? $context["entry_secure_secret"] : null);
        echo "</label>
            <div class=\"col-sm-10\">
              <input type=\"text\" name=\"payment_pinepg_secure_secret\" value=\"";
        // line 54
        echo (isset($context["payment_pinepg_secure_secret"]) ? $context["payment_pinepg_secure_secret"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_secure_secret"]) ? $context["entry_secure_secret"] : null);
        echo "\" id=\"input-secure_secret\" class=\"form-control\" />
              ";
        // line 55
        if ((isset($context["error_secure_secret"]) ? $context["error_secure_secret"] : null)) {
            // line 56
            echo "              <div class=\"text-danger\">";
            echo (isset($context["error_secure_secret"]) ? $context["error_secure_secret"] : null);
            echo "</div>
              ";
        }
        // line 58
        echo "            </div>
          </div>
      
       
      
          </div>

            <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-mode\">";
        // line 66
        echo (isset($context["entry_mode"]) ? $context["entry_mode"] : null);
        echo "</label>
            <div class=\"col-sm-10\">
              <select name=\"payment_pinepg_mode\" id=\"payment_pinepg_mode\" class=\"form-control\">
               
        
        ";
        // line 71
        if (((isset($context["payment_pinepg_mode"]) ? $context["payment_pinepg_mode"] : null) == "live")) {
            // line 72
            echo "                <option value=\"live\" selected=\"selected\">";
            echo (isset($context["entry_mode_live"]) ? $context["entry_mode_live"] : null);
            echo "</option>
                <option value=\"test\">";
            // line 73
            echo (isset($context["entry_mode_test"]) ? $context["entry_mode_test"] : null);
            echo "</option>
         ";
        } elseif ((        // line 74
(isset($context["payment_pinepg_mode"]) ? $context["payment_pinepg_mode"] : null) == "test")) {
            // line 75
            echo "               <option value=\"live\" >";
            echo (isset($context["entry_mode_live"]) ? $context["entry_mode_live"] : null);
            echo "</option>
                <option value=\"test\" selected=\"selected\">";
            // line 76
            echo (isset($context["entry_mode_test"]) ? $context["entry_mode_test"] : null);
            echo "</option>
         ";
        } else {
            // line 78
            echo "               <option value=\"live\" selected=\"selected\">";
            echo (isset($context["entry_mode_live"]) ? $context["entry_mode_live"] : null);
            echo "</option>
                <option value=\"test\">";
            // line 79
            echo (isset($context["entry_mode_test"]) ? $context["entry_mode_test"] : null);
            echo "</option>
          ";
        }
        // line 81
        echo "              </select>
            </div>
          </div> 
      
        <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-status\">";
        // line 86
        echo (isset($context["entry_status"]) ? $context["entry_status"] : null);
        echo "</label>
            <div class=\"col-sm-10\">
              <select name=\"payment_pinepg_status\" id=\"payment_pinepg_status\" class=\"form-control\">
                
        
        ";
        // line 91
        if (((isset($context["payment_pinepg_status"]) ? $context["payment_pinepg_status"] : null) == 1)) {
            // line 92
            echo "                <option value=\"1\" selected=\"selected\">";
            echo (isset($context["entry_status_enabled"]) ? $context["entry_status_enabled"] : null);
            echo "</option>
                <option value=\"0\">";
            // line 93
            echo (isset($context["entry_status_disabled"]) ? $context["entry_status_disabled"] : null);
            echo "</option>
         ";
        } elseif ((        // line 94
(isset($context["payment_pinepg_status"]) ? $context["payment_pinepg_status"] : null) == 0)) {
            // line 95
            echo "               <option value=\"1\" >";
            echo (isset($context["entry_status_enabled"]) ? $context["entry_status_enabled"] : null);
            echo "</option>
                <option value=\"0\" selected=\"selected\">";
            // line 96
            echo (isset($context["entry_status_disabled"]) ? $context["entry_status_disabled"] : null);
            echo "</option>
         ";
        } else {
            // line 98
            echo "               <option value=\"1\" selected=\"selected\">";
            echo (isset($context["entry_status_enabled"]) ? $context["entry_status_enabled"] : null);
            echo "</option>
                <option value=\"0\">";
            // line 99
            echo (isset($context["entry_status_disabled"]) ? $context["entry_status_disabled"] : null);
            echo "</option>
          ";
        }
        // line 101
        echo "              </select>
            </div>
          </div> 
      
      
      
      
      
      
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-sort_order\">";
        // line 111
        echo (isset($context["entry_sort_order"]) ? $context["entry_sort_order"] : null);
        echo "</label>
            <div class=\"col-sm-10\">
              <input type=\"text\" name=\"payment_pinepg_sort_order\" value=\"";
        // line 113
        echo (isset($context["payment_pinepg_sort_order"]) ? $context["payment_pinepg_sort_order"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_sort_order"]) ? $context["entry_sort_order"] : null);
        echo "\" id=\"input-sort_order\" class=\"form-control\" />
            </div>
          </div>
      
        <div class=\"form-group required\">
            <label class=\"col-sm-2 control-label\" for=\"input-payment_mode\">";
        // line 118
        echo (isset($context["entry_payment_mode"]) ? $context["entry_payment_mode"] : null);
        echo "</label>
            <div class=\"col-sm-10\">
              <input type=\"text\" name=\"payment_pinepg_payment_mode\" value=\"";
        // line 120
        echo (isset($context["payment_pinepg_payment_mode"]) ? $context["payment_pinepg_payment_mode"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_payment_mode"]) ? $context["entry_payment_mode"] : null);
        echo "\" id=\"input-payment_mode\" class=\"form-control\" />

       ";
        // line 122
        if ((isset($context["error_payment_mode"]) ? $context["error_payment_mode"] : null)) {
            // line 123
            echo "      
              <div class=\"text-danger\">";
            // line 124
            echo (isset($context["error_payment_mode"]) ? $context["error_payment_mode"] : null);
            echo "</div>
              ";
        }
        // line 126
        echo "            </div>
      
          </div>
      
      
          </div>

        </div>
      

            



        </div>
        </form>
      </div>
    </div>
  </div>
</div>
";
        // line 146
        echo (isset($context["footer"]) ? $context["footer"] : null);
    }

    public function getTemplateName()
    {
        return "extension/payment/pinepg.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  331 => 146,  309 => 126,  304 => 124,  301 => 123,  299 => 122,  292 => 120,  287 => 118,  277 => 113,  272 => 111,  260 => 101,  255 => 99,  250 => 98,  245 => 96,  240 => 95,  238 => 94,  234 => 93,  229 => 92,  227 => 91,  219 => 86,  212 => 81,  207 => 79,  202 => 78,  197 => 76,  192 => 75,  190 => 74,  186 => 73,  181 => 72,  179 => 71,  171 => 66,  161 => 58,  155 => 56,  153 => 55,  147 => 54,  142 => 52,  137 => 49,  131 => 47,  129 => 46,  123 => 45,  118 => 43,  112 => 39,  106 => 37,  104 => 36,  98 => 35,  93 => 33,  84 => 27,  79 => 25,  74 => 22,  66 => 18,  64 => 17,  58 => 13,  47 => 11,  43 => 10,  38 => 8,  32 => 7,  28 => 6,  19 => 1,);
    }
}
/* {{ header }}{{ column_left }}*/
/* <div id="content">*/
/*   <div class="page-header">*/
/*     <div class="container-fluid">*/
/*       <div class="pull-right">*/
/*         <button type="submit" form="form-pinepg" data-toggle="tooltip" title="{{ button_save }}" class="btn btn-primary"><i class="fa fa-save"></i></button>*/
/*         <a href="{{ cancel }}" data-toggle="tooltip" title="{{ button_cancel }}" class="btn btn-default"><i class="fa fa-reply"></i></a></div>*/
/*       <h1>{{ heading_title }}</h1>*/
/*       <ul class="breadcrumb">*/
/*         {% for breadcrumb in breadcrumbs %}*/
/*         <li><a href="{{ breadcrumb.href }}">{{ breadcrumb.text }}</a></li>*/
/*         {% endfor %}*/
/*       </ul>*/
/*     </div>*/
/*   </div>*/
/*   <div class="container-fluid">*/
/*     {% if error_warning %}*/
/*     <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> {{ error_warning }}*/
/*       <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*     </div>*/
/*     {% endif %}*/
/*     <div class="panel panel-default">*/
/*       */
/*       <div class="panel-body">*/
/*         <form action="{{ action }}" method="post" enctype="multipart/form-data" id="form-pinepg" class="form-horizontal">*/
/*             <ul class="nav nav-tabs">*/
/*             <li class="active"><a href="#tab-general" data-toggle="tab">{{ tab_general }}</a></li>*/
/*            */
/*           </ul>*/
/*       <div class="tab-content">*/
/*         <div class="tab-pane active" id="tab-general"> */
/*           <div class="form-group required">*/
/*             <label class="col-sm-2 control-label" for="input-merchant">{{ entry_merchantid }}</label>*/
/*             <div class="col-sm-10">*/
/*               <input type="text" name="payment_pinepg_merchantid" value="{{ payment_pinepg_merchantid }}" placeholder="{{ entry_merchantid }}" id="input-merchant" class="form-control" />*/
/*               {% if error_merchant %}*/
/*               <div class="text-danger">{{ error_merchant }}</div>*/
/*               {% endif %}*/
/*             </div>*/
/*           </div>*/
/*          */
/*             <div class="form-group required">*/
/*             <label class="col-sm-2 control-label" for="input-access_code">{{ entry_access_code }}</label>*/
/*             <div class="col-sm-10">*/
/*               <input type="text" name="payment_pinepg_access_code" value="{{ payment_pinepg_access_code }}" placeholder="{{ entry_access_code }}" id="input-access_code" class="form-control" />*/
/*               {% if error_access_code %}*/
/*               <div class="text-danger">{{ error_access_code }}</div>*/
/*               {% endif %}*/
/*             </div>*/
/*           </div>*/
/*          <div class="form-group required">*/
/*             <label class="col-sm-2 control-label" for="input-secure_secret">{{ entry_secure_secret }}</label>*/
/*             <div class="col-sm-10">*/
/*               <input type="text" name="payment_pinepg_secure_secret" value="{{ payment_pinepg_secure_secret }}" placeholder="{{ entry_secure_secret }}" id="input-secure_secret" class="form-control" />*/
/*               {% if error_secure_secret %}*/
/*               <div class="text-danger">{{ error_secure_secret }}</div>*/
/*               {% endif %}*/
/*             </div>*/
/*           </div>*/
/*       */
/*        */
/*       */
/*           </div>*/
/* */
/*             <div class="form-group">*/
/*             <label class="col-sm-2 control-label" for="input-mode">{{ entry_mode }}</label>*/
/*             <div class="col-sm-10">*/
/*               <select name="payment_pinepg_mode" id="payment_pinepg_mode" class="form-control">*/
/*                */
/*         */
/*         {% if payment_pinepg_mode == 'live' %}*/
/*                 <option value="live" selected="selected">{{ entry_mode_live }}</option>*/
/*                 <option value="test">{{ entry_mode_test }}</option>*/
/*          {% elseif  payment_pinepg_mode  ==  'test' %}*/
/*                <option value="live" >{{ entry_mode_live }}</option>*/
/*                 <option value="test" selected="selected">{{ entry_mode_test }}</option>*/
/*          {% else %}*/
/*                <option value="live" selected="selected">{{ entry_mode_live }}</option>*/
/*                 <option value="test">{{ entry_mode_test }}</option>*/
/*           {% endif %}*/
/*               </select>*/
/*             </div>*/
/*           </div> */
/*       */
/*         <div class="form-group">*/
/*             <label class="col-sm-2 control-label" for="input-status">{{ entry_status }}</label>*/
/*             <div class="col-sm-10">*/
/*               <select name="payment_pinepg_status" id="payment_pinepg_status" class="form-control">*/
/*                 */
/*         */
/*         {% if payment_pinepg_status  ==  1 %}*/
/*                 <option value="1" selected="selected">{{ entry_status_enabled }}</option>*/
/*                 <option value="0">{{ entry_status_disabled }}</option>*/
/*          {% elseif  payment_pinepg_status  ==  0 %}*/
/*                <option value="1" >{{ entry_status_enabled }}</option>*/
/*                 <option value="0" selected="selected">{{ entry_status_disabled }}</option>*/
/*          {% else %}*/
/*                <option value="1" selected="selected">{{ entry_status_enabled }}</option>*/
/*                 <option value="0">{{ entry_status_disabled }}</option>*/
/*           {% endif %}*/
/*               </select>*/
/*             </div>*/
/*           </div> */
/*       */
/*       */
/*       */
/*       */
/*       */
/*       */
/*           <div class="form-group">*/
/*             <label class="col-sm-2 control-label" for="input-sort_order">{{ entry_sort_order }}</label>*/
/*             <div class="col-sm-10">*/
/*               <input type="text" name="payment_pinepg_sort_order" value="{{ payment_pinepg_sort_order }}" placeholder="{{ entry_sort_order }}" id="input-sort_order" class="form-control" />*/
/*             </div>*/
/*           </div>*/
/*       */
/*         <div class="form-group required">*/
/*             <label class="col-sm-2 control-label" for="input-payment_mode">{{ entry_payment_mode }}</label>*/
/*             <div class="col-sm-10">*/
/*               <input type="text" name="payment_pinepg_payment_mode" value="{{ payment_pinepg_payment_mode }}" placeholder="{{ entry_payment_mode }}" id="input-payment_mode" class="form-control" />*/
/* */
/*        {% if error_payment_mode %}*/
/*       */
/*               <div class="text-danger">{{ error_payment_mode }}</div>*/
/*               {% endif %}*/
/*             </div>*/
/*       */
/*           </div>*/
/*       */
/*       */
/*           </div>*/
/* */
/*         </div>*/
/*       */
/* */
/*             */
/* */
/* */
/* */
/*         </div>*/
/*         </form>*/
/*       </div>*/
/*     </div>*/
/*   </div>*/
/* </div>*/
/* {{ footer }}*/
