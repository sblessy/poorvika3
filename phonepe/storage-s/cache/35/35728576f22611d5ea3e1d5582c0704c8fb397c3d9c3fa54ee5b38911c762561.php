<?php

/* so-mobile/template/mobile/home.twig */
class __TwigTemplate_05c19758fe818ef88284c8a352f1014bb33eef0e84b70b7d5416a42d0bf17b53 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        echo (isset($context["header"]) ? $context["header"] : null);
        echo "
    <div id=\"content\">
        ";
        // line 4
        echo (isset($context["content_home"]) ? $context["content_home"] : null);
        echo "
    </div>

";
        // line 7
        echo (isset($context["footer"]) ? $context["footer"] : null);
    }

    public function getTemplateName()
    {
        return "so-mobile/template/mobile/home.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  33 => 7,  27 => 4,  22 => 2,  19 => 1,);
    }
}
/* */
/* {{ header }}*/
/*     <div id="content">*/
/*         {{ content_home }}*/
/*     </div>*/
/* */
/* {{ footer }}*/
