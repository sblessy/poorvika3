<?php

/* default/template/checkout/checkout.twig */
class __TwigTemplate_dfaacec3b3f7a65527bf7cf5e9ddb90994262902bf921937f4eceb229e20f4b0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo "
<style>
#checkout-checkout{
\tmargin-top: 130px;
}
</style>
<div id=\"checkout-checkout\" class=\"address-page\">
<div class=\"container\">
  
  ";
        // line 10
        if ((isset($context["error_warning"]) ? $context["error_warning"] : null)) {
            // line 11
            echo "  <div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo (isset($context["error_warning"]) ? $context["error_warning"] : null);
            echo "
    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
  </div>
  ";
        }
        // line 15
        echo "  <div class=\"row\">
    <div id=\"content\" class=\"";
        // line 16
        echo (isset($context["class"]) ? $context["class"] : null);
        echo "\">";
        echo (isset($context["content_top"]) ? $context["content_top"] : null);
        echo "
      <h1>";
        // line 17
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
      <div class=\"panel-group\" id=\"accordion\">
        <div class=\"panel panel-default\" style=\"display:none;\">
          <div class=\"panel-heading\">
            <h4 class=\"panel-title\">";
        // line 21
        echo (isset($context["text_checkout_option"]) ? $context["text_checkout_option"] : null);
        echo "</h4>
          </div>
          <div class=\"panel-collapse collapse\" id=\"collapse-checkout-option\">
            <div class=\"panel-body\"></div>
          </div>
        </div>
        ";
        // line 27
        if (( !(isset($context["logged"]) ? $context["logged"] : null) && ((isset($context["account"]) ? $context["account"] : null) != "guest"))) {
            // line 28
            echo "        <div class=\"panel panel-default\">
          <div class=\"panel-heading\">
            <h4 class=\"panel-title\">";
            // line 30
            echo (isset($context["text_checkout_account"]) ? $context["text_checkout_account"] : null);
            echo "</h4>
          </div>
          <div class=\"panel-collapse collapse\" id=\"addr_details\">
            <div class=\"panel-body\"></div>
          </div>
        </div>
        ";
        } else {
            // line 37
            echo "\t\t<div class=\"col-md-8\">
            <div class=\"cart-block address-block\">
                <h3><i class=\"fa fa-home\"></i>My Address<a id=\"new-addr\"><i class=\"fa fa-plus\"></i>Add New Address</a></h3>
\t\t\t\t<div id=\"addr_details\">
\t\t\t\t\t<div class=\"panel-body\"></div>
\t\t\t\t</div>
            </div>
        </div>
        
        ";
        }
        // line 47
        echo "        
\t\t<div class=\"col-md-8\">
            <div class=\"cart-block address-block\">
                <h3><i class=\"fa fa-home\"></i>My Shipping Address<a id=\"new-shipping-addr\"><i class=\"fa fa-plus\"></i>Add New Address</a></h3>
\t\t\t\t<div id=\"shipping-address-details\">
\t\t\t\t\t<div class=\"panel-body\"></div>
\t\t\t\t</div>
            </div>
        </div>
\t\t
\t\t<div class=\"col-md-8\">
            <div class=\"cart-block address-block\">
                <h3><i class=\"fa fa-home\"></i>Shipping Method</h3>
\t\t\t\t<div id=\"shipping-method-details\">
\t\t\t\t\t<div class=\"panel-body\"></div>
\t\t\t\t</div>
            </div>
        </div>
        
\t\t<div class=\"col-md-8\">
           <div class=\"cart-block payment-offers\">
                <h3><span><i class=\"fa fa-percent\"></i>payment offer</span></h3>
                <div class=\"offers-cont\">
                    <ul>
                        <li>Flat 0% discount on first prepaid transaction using Rupay debit card T&C</li>
                      
                    </ul>
                   <span>Show More<i class=\"fa fa-angle-down\"></i></span>
                   
                </div>
\t\t\t</div>
\t\t</div>
        
\t\t<div class=\"col-md-8\">
            <div class=\"cart-block address-block\">
                <h3><i class=\"fa fa-home\"></i>Payment Method</h3>
\t\t\t\t<div id=\"payment-method-details\">
\t\t\t\t\t<div class=\"panel-body\"></div>
\t\t\t\t</div>
            </div>
        </div>
        
        <div class=\"col-md-4\">
\t\t\t<div class=\"cart-block summary payment\">
\t\t\t\t<h3>Order Confirmation<a href=\"index.php?route=checkout/checkout\">Edit Cart</a></h3>
\t\t\t\t<div id=\"checkout-confirm-details\">
\t\t\t\t\t<div class=\"panel-body\"></div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
      ";
        // line 97
        echo (isset($context["content_bottom"]) ? $context["content_bottom"] : null);
        echo "</div>
    ";
        // line 98
        echo (isset($context["column_right"]) ? $context["column_right"] : null);
        echo "</div>
</div>
</div>
<script type=\"text/javascript\"><!--
\$(document).on('change', 'input[name=\\'account\\']', function() {
\tif (\$('#addr_details').parent().find('.panel-heading .panel-title > *').is('a')) {
\t\tif (this.value == 'register') {
\t\t\t\$('#addr_details').parent().find('.panel-heading .panel-title').html('<a href=\"#addr_details\" data-toggle=\"collapse\" data-parent=\"#accordion\" class=\"accordion-toggle\">";
        // line 105
        echo (isset($context["text_checkout_account"]) ? $context["text_checkout_account"] : null);
        echo " <i class=\"fa fa-caret-down\"></i></a>');
\t\t} else {
\t\t\t\$('#addr_details').parent().find('.panel-heading .panel-title').html('<a href=\"#addr_details\" data-toggle=\"collapse\" data-parent=\"#accordion\" class=\"accordion-toggle\">";
        // line 107
        echo (isset($context["text_checkout_payment_address"]) ? $context["text_checkout_payment_address"] : null);
        echo " <i class=\"fa fa-caret-down\"></i></a>');
\t\t}
\t} else {
\t\tif (this.value == 'register') {
\t\t\t\$('#addr_details').parent().find('.panel-heading .panel-title').html('";
        // line 111
        echo (isset($context["text_checkout_account"]) ? $context["text_checkout_account"] : null);
        echo "');
\t\t} else {
\t\t\t\$('#addr_details').parent().find('.panel-heading .panel-title').html('";
        // line 113
        echo (isset($context["text_checkout_payment_address"]) ? $context["text_checkout_payment_address"] : null);
        echo "');
\t\t}
\t}
});
\$('#new-addr').click(function (e) {
\t\$('#add_new_addr').trigger('click');
});
\$('#new-shipping-addr').click(function (e) {
\t\$('#add_new_ship_addr').trigger('click');
});
";
        // line 123
        if ( !(isset($context["logged"]) ? $context["logged"] : null)) {
            // line 124
            echo "\$(document).ready(function() {
\t\$(\"#modalLoginForm\").modal('show');
});
\$('.close').click(function (e) {
\t\$('#modalLoginForm').modal('hide');
\twindow.location.href = 'index.php?route=checkout/cart';
});
";
        } else {
            // line 132
            echo "\$(document).ready(function() {
  \$(\"#addr_details input:radio\").click(function() {

    alert(\"clicked\");

   });

  \$(\"input:radio:first\").prop(\"checked\", true).trigger(\"click\");

});
\$(document).ready(function() {
    \$.ajax({
        url: 'index.php?route=checkout/payment_address',
        dataType: 'html',
        success: function(html) {
            \$('#addr_details .panel-body').html(html);

\t\t\t\$('#addr_details').parent().find('.panel-heading .panel-title').html('<a href=\"#addr_details\" data-toggle=\"collapse\" data-parent=\"#accordion\" class=\"accordion-toggle\">";
            // line 149
            echo (isset($context["text_checkout_payment_address"]) ? $context["text_checkout_payment_address"] : null);
            echo " <i class=\"fa fa-caret-down\"></i></a>');
\t\t\t//\$('#button-payment-address').trigger('click');
\t\t\t//shipping address
\t\t\t\$.ajax({
\t\t\t\turl: 'index.php?route=checkout/shipping_address',
\t\t\t\tdataType: 'html',
\t\t\t\tsuccess: function(html) {
\t\t\t\t\t\$('#shipping-address-details .panel-body').html(html);

\t\t\t\t\t\$('#shipping-address-details').parent().find('.panel-heading .panel-title').html('<a href=\"#shipping-address-details\" data-toggle=\"collapse\" data-parent=\"#accordion\" class=\"accordion-toggle\">";
            // line 158
            echo (isset($context["text_checkout_shipping_address"]) ? $context["text_checkout_shipping_address"] : null);
            echo " <i class=\"fa fa-caret-down\"></i></a>');
\t\t\t\t\t//\$('#button-shipping-address').trigger('click');
\t\t\t\t\t\$('a[href=\\'#shipping-address-details\\']').trigger('click');
\t\t\t\t\t
\t\t\t\t\t//Shippping Method
\t\t\t\t\t\$.ajax({
\t\t\t\t\t\turl: 'index.php?route=checkout/shipping_method',
\t\t\t\t\t\tdataType: 'html',
\t\t\t\t\t\tcomplete: function() {
\t\t\t\t\t\t\t\$('#button-guest-shipping').button('reset');
\t\t\t\t\t\t},
\t\t\t\t\t\tsuccess: function(html) {
\t\t\t\t\t\t\t\$('#shipping-method-details .panel-body').html(html);

\t\t\t\t\t\t\t\$('#shipping-method-details').parent().find('.panel-heading .panel-title').html('<a href=\"#shipping-method-details\" data-toggle=\"collapse\" data-parent=\"#accordion\" class=\"accordion-toggle\">";
            // line 172
            echo (isset($context["text_checkout_shipping_method"]) ? $context["text_checkout_shipping_method"] : null);
            echo " <i class=\"fa fa-caret-down\"></i>');
\t\t\t\t\t\t\t//\$('#button-shipping-method').trigger('click');
\t\t\t\t\t\t\t\$('a[href=\\'#shipping-method-details\\']').trigger('click');
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t//Payment Method
\t\t\t\t\t\t\t\$.ajax({
\t\t\t\t\t\t\t\turl: 'index.php?route=checkout/payment_method',
\t\t\t\t\t\t\t\tdataType: 'html',
\t\t\t\t\t\t\t\tsuccess: function(html) {
\t\t\t\t\t\t\t\t\t\$('#payment-method-details .panel-body').html(html);

\t\t\t\t\t\t\t\t\t\$('#payment-method-details').parent().find('.panel-heading .panel-title').html('<a href=\"#payment-method-details\" data-toggle=\"collapse\" data-parent=\"#accordion\" class=\"accordion-toggle\">";
            // line 183
            echo (isset($context["text_checkout_payment_method"]) ? $context["text_checkout_payment_method"] : null);
            echo " <i class=\"fa fa-caret-down\"></i></a>');
\t\t\t\t\t\t\t\t\t//\$('#button-payment-method').trigger('click');
\t\t\t\t\t\t\t\t\t\$('a[href=\\'#payment-method-details\\']').trigger('click');

\t\t\t\t\t\t\t\t\t\$('#checkout-confirm-details').parent().find('.panel-heading .panel-title').html('";
            // line 187
            echo (isset($context["text_checkout_confirm"]) ? $context["text_checkout_confirm"] : null);
            echo "');
\t\t\t\t\t\t\t\t},
\t\t\t\t\t\t\t\terror: function(xhr, ajaxOptions, thrownError) {
\t\t\t\t\t\t\t\t\talert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t});

\t\t\t\t\t\t\t\$('#payment-method-details').parent().find('.panel-heading .panel-title').html('";
            // line 194
            echo (isset($context["text_checkout_payment_method"]) ? $context["text_checkout_payment_method"] : null);
            echo "');
\t\t\t\t\t\t\t\$('#checkout-confirm-details').parent().find('.panel-heading .panel-title').html('";
            // line 195
            echo (isset($context["text_checkout_confirm"]) ? $context["text_checkout_confirm"] : null);
            echo "');
\t\t\t\t\t\t},
\t\t\t\t\t\terror: function(xhr, ajaxOptions, thrownError) {
\t\t\t\t\t\t\talert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
\t\t\t\t\t\t}
\t\t\t\t\t});

\t\t\t\t\t\$('#shipping-method-details').parent().find('.panel-heading .panel-title').html('";
            // line 202
            echo (isset($context["text_checkout_shipping_method"]) ? $context["text_checkout_shipping_method"] : null);
            echo "');
\t\t\t\t\t\$('#payment-method-details').parent().find('.panel-heading .panel-title').html('";
            // line 203
            echo (isset($context["text_checkout_payment_method"]) ? $context["text_checkout_payment_method"] : null);
            echo "');
\t\t\t\t\t\$('#checkout-confirm-details').parent().find('.panel-heading .panel-title').html('";
            // line 204
            echo (isset($context["text_checkout_confirm"]) ? $context["text_checkout_confirm"] : null);
            echo "');
\t\t\t\t},
\t\t\t\terror: function(xhr, ajaxOptions, thrownError) {
\t\t\t\t\talert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
\t\t\t\t}
\t\t\t});
\t\t\t\$('a[href=\\'#addr_details\\']').trigger('click');
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
        }
    });
\t//shipping address
\t/*\$.ajax({
\t\turl: 'index.php?route=checkout/shipping_address',
\t\tdataType: 'html',
\t\tsuccess: function(html) {
\t\t\t\$('#shipping-address-details .panel-body').html(html);

\t\t\t\$('#shipping-address-details').parent().find('.panel-heading .panel-title').html('<a href=\"#shipping-address-details\" data-toggle=\"collapse\" data-parent=\"#accordion\" class=\"accordion-toggle\">";
            // line 223
            echo (isset($context["text_checkout_shipping_address"]) ? $context["text_checkout_shipping_address"] : null);
            echo " <i class=\"fa fa-caret-down\"></i></a>');
\t\t\t\$('#button-shipping-address').trigger('click');
\t\t\t\$('a[href=\\'#shipping-address-details\\']').trigger('click');

\t\t\t\$('#shipping-method-details').parent().find('.panel-heading .panel-title').html('";
            // line 227
            echo (isset($context["text_checkout_shipping_method"]) ? $context["text_checkout_shipping_method"] : null);
            echo "');
\t\t\t\$('#payment-method-details').parent().find('.panel-heading .panel-title').html('";
            // line 228
            echo (isset($context["text_checkout_payment_method"]) ? $context["text_checkout_payment_method"] : null);
            echo "');
\t\t\t\$('#checkout-confirm-details').parent().find('.panel-heading .panel-title').html('";
            // line 229
            echo (isset($context["text_checkout_confirm"]) ? $context["text_checkout_confirm"] : null);
            echo "');
\t\t},
\t\terror: function(xhr, ajaxOptions, thrownError) {
\t\t\talert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
\t\t}
\t});
\t//Shippping Method
\t\$.ajax({
\t\turl: 'index.php?route=checkout/shipping_method',
\t\tdataType: 'html',
\t\tcomplete: function() {
\t\t\t\$('#button-guest-shipping').button('reset');
\t\t},
\t\tsuccess: function(html) {
\t\t\t\$('#shipping-method-details .panel-body').html(html);

\t\t\t\$('#shipping-method-details').parent().find('.panel-heading .panel-title').html('<a href=\"#shipping-method-details\" data-toggle=\"collapse\" data-parent=\"#accordion\" class=\"accordion-toggle\">";
            // line 245
            echo (isset($context["text_checkout_shipping_method"]) ? $context["text_checkout_shipping_method"] : null);
            echo " <i class=\"fa fa-caret-down\"></i>');
\t\t\t\$('#button-shipping-method').trigger('click');
\t\t\t\$('a[href=\\'#shipping-method-details\\']').trigger('click');

\t\t\t\$('#payment-method-details').parent().find('.panel-heading .panel-title').html('";
            // line 249
            echo (isset($context["text_checkout_payment_method"]) ? $context["text_checkout_payment_method"] : null);
            echo "');
\t\t\t\$('#checkout-confirm-details').parent().find('.panel-heading .panel-title').html('";
            // line 250
            echo (isset($context["text_checkout_confirm"]) ? $context["text_checkout_confirm"] : null);
            echo "');
\t\t},
\t\terror: function(xhr, ajaxOptions, thrownError) {
\t\t\talert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
\t\t}
\t});
\t//Payment Method
\t\$.ajax({
\t\turl: 'index.php?route=checkout/payment_method',
\t\tdataType: 'html',
\t\tsuccess: function(html) {
\t\t\t\$('#payment-method-details .panel-body').html(html);

\t\t\t\$('#payment-method-details').parent().find('.panel-heading .panel-title').html('<a href=\"#payment-method-details\" data-toggle=\"collapse\" data-parent=\"#accordion\" class=\"accordion-toggle\">";
            // line 263
            echo (isset($context["text_checkout_payment_method"]) ? $context["text_checkout_payment_method"] : null);
            echo " <i class=\"fa fa-caret-down\"></i></a>');
\t\t\t\$('#button-payment-method').trigger('click');
\t\t\t\$('a[href=\\'#payment-method-details\\']').trigger('click');

\t\t\t\$('#checkout-confirm-details').parent().find('.panel-heading .panel-title').html('";
            // line 267
            echo (isset($context["text_checkout_confirm"]) ? $context["text_checkout_confirm"] : null);
            echo "');
\t\t},
\t\terror: function(xhr, ajaxOptions, thrownError) {
\t\t\talert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
\t\t}
\t});*/
\t//Confirm Order
\t/*\$.ajax({
\t\turl: 'index.php?route=checkout/confirm',
\t\tdataType: 'html',
\t\tcomplete: function() {
\t\t\t\$('#button-payment-method').button('reset');
\t\t},
\t\tsuccess: function(html) {
\t\t\t\$('#checkout-confirm-details .panel-body').html(html);

\t\t\t\$('#checkout-confirm-details').parent().find('.panel-heading .panel-title').html('<a href=\"#checkout-confirm-details\" data-toggle=\"collapse\" data-parent=\"#accordion\" class=\"accordion-toggle\">";
            // line 283
            echo (isset($context["text_checkout_confirm"]) ? $context["text_checkout_confirm"] : null);
            echo " <i class=\"fa fa-caret-down\"></i></a>');

\t\t\t\$('a[href=\\'#checkout-confirm-details\\']').trigger('click');
\t\t},
\t\terror: function(xhr, ajaxOptions, thrownError) {
\t\t\talert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
\t\t}
\t});*/
});

";
        }
        // line 294
        echo "
// Checkout
\$(document).delegate('#button-account', 'click', function() {
    \$.ajax({
        url: 'index.php?route=checkout/' + \$('input[name=\\'account\\']:checked').val(),
        dataType: 'html',
        beforeSend: function() {
        \t\$('#button-account').button('loading');
\t\t},
        complete: function() {
\t\t\t\$('#button-account').button('reset');
        },
        success: function(html) {
            \$('.alert-dismissible, .text-danger').remove();
\t\t\t\$('.form-group').removeClass('has-error');

            \$('#addr_details .panel-body').html(html);

\t\t\t\$('a[href=\\'#addr_details\\']').trigger('click');
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
        }
    });
});



// Register
\$(document).delegate('#button-register', 'click', function() {
    \$.ajax({
        url: 'index.php?route=checkout/register/save',
        type: 'post',
        data: \$('#addr_details input[type=\\'text\\'], #addr_details input[type=\\'date\\'], #addr_details input[type=\\'datetime-local\\'], #addr_details input[type=\\'time\\'], #addr_details input[type=\\'password\\'], #addr_details input[type=\\'hidden\\'], #addr_details input[type=\\'checkbox\\']:checked, #addr_details input[type=\\'radio\\']:checked, #addr_details textarea, #addr_details select'),
        dataType: 'json',
        beforeSend: function() {
\t\t\t\$('#button-register').button('loading');
\t\t},
        success: function(json) {
            \$('.alert-dismissible, .text-danger').remove();
            \$('.form-group').removeClass('has-error');

            if (json['redirect']) {
                location = json['redirect'];
            } else if (json['error']) {
                \$('#button-register').button('reset');

                if (json['error']['warning']) {
                    \$('#addr_details .panel-body').prepend('<div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ' + json['error']['warning'] + '<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>');
                }

\t\t\t\tfor (i in json['error']) {
\t\t\t\t\tvar element = \$('#input-payment-' + i.replace('_', '-'));

\t\t\t\t\tif (\$(element).parent().hasClass('input-group')) {
\t\t\t\t\t\t\$(element).parent().after('<div class=\"text-danger\">' + json['error'][i] + '</div>');
\t\t\t\t\t} else {
\t\t\t\t\t\t\$(element).after('<div class=\"text-danger\">' + json['error'][i] + '</div>');
\t\t\t\t\t}
\t\t\t\t}

\t\t\t\t// Highlight any found errors
\t\t\t\t\$('.text-danger').parent().addClass('has-error');
            } else {
                ";
        // line 358
        if ((isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
            // line 359
            echo "                var shipping_address = \$('#payment-address input[name=\\'shipping_address\\']:checked').prop('value');

                if (shipping_address) {
                    \$.ajax({
                        url: 'index.php?route=checkout/shipping_method',
                        dataType: 'html',
                        success: function(html) {
\t\t\t\t\t\t\t// Add the shipping address
                            \$.ajax({
                                url: 'index.php?route=checkout/shipping_address',
                                dataType: 'html',
                                success: function(html) {
                                    \$('#shipping-address-details .panel-body').html(html);

\t\t\t\t\t\t\t\t\t\$('#shipping-address-details').parent().find('.panel-heading .panel-title').html('<a href=\"#shipping-address-details\" data-toggle=\"collapse\" data-parent=\"#accordion\" class=\"accordion-toggle\">";
            // line 373
            echo (isset($context["text_checkout_shipping_address"]) ? $context["text_checkout_shipping_address"] : null);
            echo " <i class=\"fa fa-caret-down\"></i></a>');
                                },
                                error: function(xhr, ajaxOptions, thrownError) {
                                    alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
                                }
                            });

\t\t\t\t\t\t\t\$('#shipping-method-details .panel-body').html(html);

\t\t\t\t\t\t\t\$('#shipping-method-details').parent().find('.panel-heading .panel-title').html('<a href=\"#shipping-method-details\" data-toggle=\"collapse\" data-parent=\"#accordion\" class=\"accordion-toggle\">";
            // line 382
            echo (isset($context["text_checkout_shipping_method"]) ? $context["text_checkout_shipping_method"] : null);
            echo " <i class=\"fa fa-caret-down\"></i></a>');

   \t\t\t\t\t\t\t\$('a[href=\\'#shipping-method-details\\']').trigger('click');

\t\t\t\t\t\t\t\$('#shipping-method-details').parent().find('.panel-heading .panel-title').html('";
            // line 386
            echo (isset($context["text_checkout_shipping_method"]) ? $context["text_checkout_shipping_method"] : null);
            echo "');
\t\t\t\t\t\t\t\$('#payment-method-details').parent().find('.panel-heading .panel-title').html('";
            // line 387
            echo (isset($context["text_checkout_payment_method"]) ? $context["text_checkout_payment_method"] : null);
            echo "');
\t\t\t\t\t\t\t\$('#checkout-confirm-details').parent().find('.panel-heading .panel-title').html('";
            // line 388
            echo (isset($context["text_checkout_confirm"]) ? $context["text_checkout_confirm"] : null);
            echo "');
                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
                        }
                    });
                } else {
                    \$.ajax({
                        url: 'index.php?route=checkout/shipping_address',
                        dataType: 'html',
                        success: function(html) {
                            \$('#shipping-address-details .panel-body').html(html);

\t\t\t\t\t\t\t\$('#shipping-address-details').parent().find('.panel-heading .panel-title').html('<a href=\"#shipping-address-details\" data-toggle=\"collapse\" data-parent=\"#accordion\" class=\"accordion-toggle\">";
            // line 401
            echo (isset($context["text_checkout_shipping_address"]) ? $context["text_checkout_shipping_address"] : null);
            echo " <i class=\"fa fa-caret-down\"></i></a>');

\t\t\t\t\t\t\t\$('a[href=\\'#shipping-address-details\\']').trigger('click');

\t\t\t\t\t\t\t\$('#shipping-method-details').parent().find('.panel-heading .panel-title').html('";
            // line 405
            echo (isset($context["text_checkout_shipping_method"]) ? $context["text_checkout_shipping_method"] : null);
            echo "');
\t\t\t\t\t\t\t\$('#payment-method-details').parent().find('.panel-heading .panel-title').html('";
            // line 406
            echo (isset($context["text_checkout_payment_method"]) ? $context["text_checkout_payment_method"] : null);
            echo "');
\t\t\t\t\t\t\t\$('#checkout-confirm-details').parent().find('.panel-heading .panel-title').html('";
            // line 407
            echo (isset($context["text_checkout_confirm"]) ? $context["text_checkout_confirm"] : null);
            echo "');
                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
                        }
                    });
                }
                ";
        } else {
            // line 415
            echo "                \$.ajax({
                    url: 'index.php?route=checkout/payment_method',
                    dataType: 'html',
                    success: function(html) {
                        \$('#payment-method-details .panel-body').html(html);

\t\t\t\t\t\t\$('#payment-method-details').parent().find('.panel-heading .panel-title').html('<a href=\"#payment-method-details\" data-toggle=\"collapse\" data-parent=\"#accordion\" class=\"accordion-toggle\">";
            // line 421
            echo (isset($context["text_checkout_payment_method"]) ? $context["text_checkout_payment_method"] : null);
            echo " <i class=\"fa fa-caret-down\"></i></a>');

\t\t\t\t\t\t\$('a[href=\\'#payment-method-details\\']').trigger('click');

\t\t\t\t\t\t\$('#checkout-confirm-details').parent().find('.panel-heading .panel-title').html('";
            // line 425
            echo (isset($context["text_checkout_confirm"]) ? $context["text_checkout_confirm"] : null);
            echo "');
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
                    }
                });
                ";
        }
        // line 432
        echo "
                \$.ajax({
                    url: 'index.php?route=checkout/payment_address',
                    dataType: 'html',
                    complete: function() {
                        \$('#button-register').button('reset');
                    },
                    success: function(html) {
                        \$('#addr_details .panel-body').html(html);

\t\t\t\t\t\t\$('#addr_details').parent().find('.panel-heading .panel-title').html('<a href=\"#addr_details\" data-toggle=\"collapse\" data-parent=\"#accordion\" class=\"accordion-toggle\">";
        // line 442
        echo (isset($context["text_checkout_payment_address"]) ? $context["text_checkout_payment_address"] : null);
        echo " <i class=\"fa fa-caret-down\"></i></a>');
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
                    }
                });
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
        }
    });
});

// Payment Address
\$(document).delegate('#button-payment-address', 'click', function() {
    \$.ajax({
        url: 'index.php?route=checkout/payment_address/save',
        type: 'post',
        data: \$('#addr_details input[type=\\'text\\'], #addr_details input[type=\\'date\\'], #addr_details input[type=\\'datetime-local\\'], #addr_details input[type=\\'time\\'], #addr_details input[type=\\'password\\'], #addr_details input[type=\\'checkbox\\']:checked, #addr_details input[type=\\'radio\\']:checked, #addr_details input[type=\\'hidden\\'], #addr_details textarea, #addr_details radio'),
        dataType: 'json',
        beforeSend: function() {
        \t\$('#button-payment-address').button('loading');
\t\t},
        complete: function() {
\t\t\t\$('#button-payment-address').button('reset');
        },
        success: function(json) {
            \$('.alert-dismissible, .text-danger').remove();
\t\t\t\$('.form-group').removeClass('has-error');
            
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
        }
    });
});

// Shipping Address
\$(document).delegate('#button-shipping-address', 'click', function() {
    \$.ajax({
        url: 'index.php?route=checkout/shipping_address/save',
        type: 'post',
        data: \$('#shipping-address-details input[type=\\'text\\'], #shipping-address-details input[type=\\'date\\'], #shipping-address-details input[type=\\'datetime-local\\'], #shipping-address-details input[type=\\'time\\'], #shipping-address-details input[type=\\'password\\'], #shipping-address-details input[type=\\'checkbox\\']:checked, #shipping-address-details input[type=\\'radio\\']:checked, #shipping-address-details textarea, #shipping-address-details select'),
        dataType: 'json',
        beforeSend: function() {
\t\t\t\$('#button-shipping-address').button('loading');
\t    },
        success: function(json) {
            \$('.alert-dismissible, .text-danger').remove();
\t\t\t\$('.form-group').removeClass('has-error');

            if (json['redirect']) {
                location = json['redirect'];
            } else if (json['error']) {
                \$('#button-shipping-address').button('reset');

                if (json['error']['warning']) {
                    \$('#shipping-address-details .panel-body').prepend('<div class=\"alert alert-warning alert-dismissible\">' + json['error']['warning'] + '<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>');
                }

\t\t\t\tfor (i in json['error']) {
\t\t\t\t\tvar element = \$('#input-shipping-' + i.replace('_', '-'));

\t\t\t\t\tif (\$(element).parent().hasClass('input-group')) {
\t\t\t\t\t\t\$(element).parent().after('<div class=\"text-danger\">' + json['error'][i] + '</div>');
\t\t\t\t\t} else {
\t\t\t\t\t\t\$(element).after('<div class=\"text-danger\">' + json['error'][i] + '</div>');
\t\t\t\t\t}
\t\t\t\t}

\t\t\t\t// Highlight any found errors
\t\t\t\t\$('.text-danger').parent().parent().addClass('has-error');
            } else {
                \$.ajax({
                    url: 'index.php?route=checkout/shipping_method',
                    dataType: 'html',
                    complete: function() {
                        \$('#button-shipping-address').button('reset');
                    },
                    success: function(html) {
                        \$('#shipping-method-details .panel-body').html(html);

\t\t\t\t\t\t\$('#shipping-method-details').parent().find('.panel-heading .panel-title').html('<a href=\"#shipping-method-details\" data-toggle=\"collapse\" data-parent=\"#accordion\" class=\"accordion-toggle\">";
        // line 525
        echo (isset($context["text_checkout_shipping_method"]) ? $context["text_checkout_shipping_method"] : null);
        echo " <i class=\"fa fa-caret-down\"></i></a>');

\t\t\t\t\t\t\$('a[href=\\'#shipping-method-details\\']').trigger('click');

\t\t\t\t\t\t\$('#payment-method-details').parent().find('.panel-heading .panel-title').html('";
        // line 529
        echo (isset($context["text_checkout_payment_method"]) ? $context["text_checkout_payment_method"] : null);
        echo "');
\t\t\t\t\t\t\$('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('";
        // line 530
        echo (isset($context["text_checkout_confirm"]) ? $context["text_checkout_confirm"] : null);
        echo "');
\t\t\t\t\t\t
                        \$.ajax({
                            url: 'index.php?route=checkout/shipping_address',
                            dataType: 'html',
                            success: function(html) {
                                \$('#shipping-address-details .panel-body').html(html);
                            },
                            error: function(xhr, ajaxOptions, thrownError) {
                                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
                            }
                        });
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
                    }
                }).done(function() {
\t\t\t\t\t\$.ajax({
\t\t\t\t\t\turl: 'index.php?route=checkout/payment_address',
\t\t\t\t\t\tdataType: 'html',
\t\t\t\t\t\tsuccess: function(html) {
\t\t\t\t\t\t\t\$('#collapse-payment-address .panel-body').html(html);
\t\t\t\t\t\t},
\t\t\t\t\t\terror: function(xhr, ajaxOptions, thrownError) {
\t\t\t\t\t\t\talert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
\t\t\t\t\t\t}
\t\t\t\t\t});
\t\t\t\t});
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
        }
    });
});


\$(document).delegate('#button-shipping-method', 'click', function() {
    \$.ajax({
        url: 'index.php?route=checkout/shipping_method/save',
        type: 'post',
        data: \$('#shipping-method-details input[type=\\'radio\\']:checked, #shipping-method-details textarea'),
        dataType: 'json',
        beforeSend: function() {
        \t\$('#button-shipping-method').button('loading');
\t\t},
        success: function(json) {
            \$('.alert-dismissible, .text-danger').remove();

            if (json['redirect']) {
                location = json['redirect'];
            } else if (json['error']) {
                \$('#button-shipping-method').button('reset');

                if (json['error']['warning']) {
                    \$('#shipping-method-details .panel-body').prepend('<div class=\"alert alert-danger alert-dismissible\">' + json['error']['warning'] + '<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>');
                }
            } else {
                \$.ajax({
                    url: 'index.php?route=checkout/payment_method',
                    dataType: 'html',
                    complete: function() {
                        \$('#button-shipping-method').button('reset');
                    },
                    success: function(html) {
                        \$('#payment-method-details .panel-body').html(html);

\t\t\t\t\t\t\$('#payment-method-details').parent().find('.panel-heading .panel-title').html('<a href=\"#payment-method-details\" data-toggle=\"collapse\" data-parent=\"#accordion\" class=\"accordion-toggle\">";
        // line 597
        echo (isset($context["text_checkout_payment_method"]) ? $context["text_checkout_payment_method"] : null);
        echo " <i class=\"fa fa-caret-down\"></i></a>');

\t\t\t\t\t\t\$('a[href=\\'#payment-method-details\\']').trigger('click');

\t\t\t\t\t\t\$('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('";
        // line 601
        echo (isset($context["text_checkout_confirm"]) ? $context["text_checkout_confirm"] : null);
        echo "');
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
                    }
                });
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
        }
    });
});

\$(document).delegate('#button-payment-method', 'click', function() {
    \$.ajax({
        url: 'index.php?route=checkout/payment_method/save',
        type: 'post',
        data: \$('#payment-method-details input[type=\\'radio\\']:checked, #payment-method-details input[type=\\'checkbox\\']:checked, #payment-method-details textarea'),
        dataType: 'json',
        beforeSend: function() {
         \t\$('#button-payment-method').button('loading');
\t\t},
        success: function(json) {
            \$('.alert-dismissible, .text-danger').remove();

            if (json['redirect']) {
                location = json['redirect'];
            } else if (json['error']) {
                \$('#button-payment-method').button('reset');
                
                if (json['error']['warning']) {
                    \$('#payment-method-details .panel-body').prepend('<div class=\"alert alert-danger alert-dismissible\">' + json['error']['warning'] + '<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>');
                }
            } else {
                \$.ajax({
                    url: 'index.php?route=checkout/confirm',
                    dataType: 'html',
                    complete: function() {
                        \$('#button-payment-method').button('reset');
                    },
                    success: function(html) {
                        \$('#checkout-confirm-details .panel-body').html(html);

\t\t\t\t\t\t\$('#checkout-confirm-details').parent().find('.panel-heading .panel-title').html('<a href=\"#checkout-confirm-details\" data-toggle=\"collapse\" data-parent=\"#accordion\" class=\"accordion-toggle\">";
        // line 645
        echo (isset($context["text_checkout_confirm"]) ? $context["text_checkout_confirm"] : null);
        echo " <i class=\"fa fa-caret-down\"></i></a>');

\t\t\t\t\t\t\$('a[href=\\'#checkout-confirm-details\\']').trigger('click');
\t\t\t\t\t},
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
                    }
                });
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
        }
    });
});
//--></script>
";
        // line 661
        echo (isset($context["footer"]) ? $context["footer"] : null);
    }

    public function getTemplateName()
    {
        return "default/template/checkout/checkout.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  856 => 661,  837 => 645,  790 => 601,  783 => 597,  713 => 530,  709 => 529,  702 => 525,  616 => 442,  604 => 432,  594 => 425,  587 => 421,  579 => 415,  568 => 407,  564 => 406,  560 => 405,  553 => 401,  537 => 388,  533 => 387,  529 => 386,  522 => 382,  510 => 373,  494 => 359,  492 => 358,  426 => 294,  412 => 283,  393 => 267,  386 => 263,  370 => 250,  366 => 249,  359 => 245,  340 => 229,  336 => 228,  332 => 227,  325 => 223,  303 => 204,  299 => 203,  295 => 202,  285 => 195,  281 => 194,  271 => 187,  264 => 183,  250 => 172,  233 => 158,  221 => 149,  202 => 132,  192 => 124,  190 => 123,  177 => 113,  172 => 111,  165 => 107,  160 => 105,  150 => 98,  146 => 97,  94 => 47,  82 => 37,  72 => 30,  68 => 28,  66 => 27,  57 => 21,  50 => 17,  44 => 16,  41 => 15,  33 => 11,  31 => 10,  19 => 1,);
    }
}
/* {{ header }}*/
/* <style>*/
/* #checkout-checkout{*/
/* 	margin-top: 130px;*/
/* }*/
/* </style>*/
/* <div id="checkout-checkout" class="address-page">*/
/* <div class="container">*/
/*   */
/*   {% if error_warning %}*/
/*   <div class="alert alert-danger alert-dismissible"><i class="fa fa-exclamation-circle"></i> {{ error_warning }}*/
/*     <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*   </div>*/
/*   {% endif %}*/
/*   <div class="row">*/
/*     <div id="content" class="{{ class }}">{{ content_top }}*/
/*       <h1>{{ heading_title }}</h1>*/
/*       <div class="panel-group" id="accordion">*/
/*         <div class="panel panel-default" style="display:none;">*/
/*           <div class="panel-heading">*/
/*             <h4 class="panel-title">{{ text_checkout_option }}</h4>*/
/*           </div>*/
/*           <div class="panel-collapse collapse" id="collapse-checkout-option">*/
/*             <div class="panel-body"></div>*/
/*           </div>*/
/*         </div>*/
/*         {% if not logged and account != 'guest' %}*/
/*         <div class="panel panel-default">*/
/*           <div class="panel-heading">*/
/*             <h4 class="panel-title">{{ text_checkout_account }}</h4>*/
/*           </div>*/
/*           <div class="panel-collapse collapse" id="addr_details">*/
/*             <div class="panel-body"></div>*/
/*           </div>*/
/*         </div>*/
/*         {% else %}*/
/* 		<div class="col-md-8">*/
/*             <div class="cart-block address-block">*/
/*                 <h3><i class="fa fa-home"></i>My Address<a id="new-addr"><i class="fa fa-plus"></i>Add New Address</a></h3>*/
/* 				<div id="addr_details">*/
/* 					<div class="panel-body"></div>*/
/* 				</div>*/
/*             </div>*/
/*         </div>*/
/*         */
/*         {% endif %}*/
/*         */
/* 		<div class="col-md-8">*/
/*             <div class="cart-block address-block">*/
/*                 <h3><i class="fa fa-home"></i>My Shipping Address<a id="new-shipping-addr"><i class="fa fa-plus"></i>Add New Address</a></h3>*/
/* 				<div id="shipping-address-details">*/
/* 					<div class="panel-body"></div>*/
/* 				</div>*/
/*             </div>*/
/*         </div>*/
/* 		*/
/* 		<div class="col-md-8">*/
/*             <div class="cart-block address-block">*/
/*                 <h3><i class="fa fa-home"></i>Shipping Method</h3>*/
/* 				<div id="shipping-method-details">*/
/* 					<div class="panel-body"></div>*/
/* 				</div>*/
/*             </div>*/
/*         </div>*/
/*         */
/* 		<div class="col-md-8">*/
/*            <div class="cart-block payment-offers">*/
/*                 <h3><span><i class="fa fa-percent"></i>payment offer</span></h3>*/
/*                 <div class="offers-cont">*/
/*                     <ul>*/
/*                         <li>Flat 0% discount on first prepaid transaction using Rupay debit card T&C</li>*/
/*                       */
/*                     </ul>*/
/*                    <span>Show More<i class="fa fa-angle-down"></i></span>*/
/*                    */
/*                 </div>*/
/* 			</div>*/
/* 		</div>*/
/*         */
/* 		<div class="col-md-8">*/
/*             <div class="cart-block address-block">*/
/*                 <h3><i class="fa fa-home"></i>Payment Method</h3>*/
/* 				<div id="payment-method-details">*/
/* 					<div class="panel-body"></div>*/
/* 				</div>*/
/*             </div>*/
/*         </div>*/
/*         */
/*         <div class="col-md-4">*/
/* 			<div class="cart-block summary payment">*/
/* 				<h3>Order Confirmation<a href="index.php?route=checkout/checkout">Edit Cart</a></h3>*/
/* 				<div id="checkout-confirm-details">*/
/* 					<div class="panel-body"></div>*/
/* 				</div>*/
/* 			</div>*/
/* 		</div>*/
/*       {{ content_bottom }}</div>*/
/*     {{ column_right }}</div>*/
/* </div>*/
/* </div>*/
/* <script type="text/javascript"><!--*/
/* $(document).on('change', 'input[name=\'account\']', function() {*/
/* 	if ($('#addr_details').parent().find('.panel-heading .panel-title > *').is('a')) {*/
/* 		if (this.value == 'register') {*/
/* 			$('#addr_details').parent().find('.panel-heading .panel-title').html('<a href="#addr_details" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">{{ text_checkout_account }} <i class="fa fa-caret-down"></i></a>');*/
/* 		} else {*/
/* 			$('#addr_details').parent().find('.panel-heading .panel-title').html('<a href="#addr_details" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">{{ text_checkout_payment_address }} <i class="fa fa-caret-down"></i></a>');*/
/* 		}*/
/* 	} else {*/
/* 		if (this.value == 'register') {*/
/* 			$('#addr_details').parent().find('.panel-heading .panel-title').html('{{ text_checkout_account }}');*/
/* 		} else {*/
/* 			$('#addr_details').parent().find('.panel-heading .panel-title').html('{{ text_checkout_payment_address }}');*/
/* 		}*/
/* 	}*/
/* });*/
/* $('#new-addr').click(function (e) {*/
/* 	$('#add_new_addr').trigger('click');*/
/* });*/
/* $('#new-shipping-addr').click(function (e) {*/
/* 	$('#add_new_ship_addr').trigger('click');*/
/* });*/
/* {% if not logged %}*/
/* $(document).ready(function() {*/
/* 	$("#modalLoginForm").modal('show');*/
/* });*/
/* $('.close').click(function (e) {*/
/* 	$('#modalLoginForm').modal('hide');*/
/* 	window.location.href = 'index.php?route=checkout/cart';*/
/* });*/
/* {% else %}*/
/* $(document).ready(function() {*/
/*   $("#addr_details input:radio").click(function() {*/
/* */
/*     alert("clicked");*/
/* */
/*    });*/
/* */
/*   $("input:radio:first").prop("checked", true).trigger("click");*/
/* */
/* });*/
/* $(document).ready(function() {*/
/*     $.ajax({*/
/*         url: 'index.php?route=checkout/payment_address',*/
/*         dataType: 'html',*/
/*         success: function(html) {*/
/*             $('#addr_details .panel-body').html(html);*/
/* */
/* 			$('#addr_details').parent().find('.panel-heading .panel-title').html('<a href="#addr_details" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">{{ text_checkout_payment_address }} <i class="fa fa-caret-down"></i></a>');*/
/* 			//$('#button-payment-address').trigger('click');*/
/* 			//shipping address*/
/* 			$.ajax({*/
/* 				url: 'index.php?route=checkout/shipping_address',*/
/* 				dataType: 'html',*/
/* 				success: function(html) {*/
/* 					$('#shipping-address-details .panel-body').html(html);*/
/* */
/* 					$('#shipping-address-details').parent().find('.panel-heading .panel-title').html('<a href="#shipping-address-details" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">{{ text_checkout_shipping_address }} <i class="fa fa-caret-down"></i></a>');*/
/* 					//$('#button-shipping-address').trigger('click');*/
/* 					$('a[href=\'#shipping-address-details\']').trigger('click');*/
/* 					*/
/* 					//Shippping Method*/
/* 					$.ajax({*/
/* 						url: 'index.php?route=checkout/shipping_method',*/
/* 						dataType: 'html',*/
/* 						complete: function() {*/
/* 							$('#button-guest-shipping').button('reset');*/
/* 						},*/
/* 						success: function(html) {*/
/* 							$('#shipping-method-details .panel-body').html(html);*/
/* */
/* 							$('#shipping-method-details').parent().find('.panel-heading .panel-title').html('<a href="#shipping-method-details" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">{{ text_checkout_shipping_method }} <i class="fa fa-caret-down"></i>');*/
/* 							//$('#button-shipping-method').trigger('click');*/
/* 							$('a[href=\'#shipping-method-details\']').trigger('click');*/
/* 							*/
/* 							//Payment Method*/
/* 							$.ajax({*/
/* 								url: 'index.php?route=checkout/payment_method',*/
/* 								dataType: 'html',*/
/* 								success: function(html) {*/
/* 									$('#payment-method-details .panel-body').html(html);*/
/* */
/* 									$('#payment-method-details').parent().find('.panel-heading .panel-title').html('<a href="#payment-method-details" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">{{ text_checkout_payment_method }} <i class="fa fa-caret-down"></i></a>');*/
/* 									//$('#button-payment-method').trigger('click');*/
/* 									$('a[href=\'#payment-method-details\']').trigger('click');*/
/* */
/* 									$('#checkout-confirm-details').parent().find('.panel-heading .panel-title').html('{{ text_checkout_confirm }}');*/
/* 								},*/
/* 								error: function(xhr, ajaxOptions, thrownError) {*/
/* 									alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/* 								}*/
/* 							});*/
/* */
/* 							$('#payment-method-details').parent().find('.panel-heading .panel-title').html('{{ text_checkout_payment_method }}');*/
/* 							$('#checkout-confirm-details').parent().find('.panel-heading .panel-title').html('{{ text_checkout_confirm }}');*/
/* 						},*/
/* 						error: function(xhr, ajaxOptions, thrownError) {*/
/* 							alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/* 						}*/
/* 					});*/
/* */
/* 					$('#shipping-method-details').parent().find('.panel-heading .panel-title').html('{{ text_checkout_shipping_method }}');*/
/* 					$('#payment-method-details').parent().find('.panel-heading .panel-title').html('{{ text_checkout_payment_method }}');*/
/* 					$('#checkout-confirm-details').parent().find('.panel-heading .panel-title').html('{{ text_checkout_confirm }}');*/
/* 				},*/
/* 				error: function(xhr, ajaxOptions, thrownError) {*/
/* 					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/* 				}*/
/* 			});*/
/* 			$('a[href=\'#addr_details\']').trigger('click');*/
/*         },*/
/*         error: function(xhr, ajaxOptions, thrownError) {*/
/*             alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*         }*/
/*     });*/
/* 	//shipping address*/
/* 	/*$.ajax({*/
/* 		url: 'index.php?route=checkout/shipping_address',*/
/* 		dataType: 'html',*/
/* 		success: function(html) {*/
/* 			$('#shipping-address-details .panel-body').html(html);*/
/* */
/* 			$('#shipping-address-details').parent().find('.panel-heading .panel-title').html('<a href="#shipping-address-details" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">{{ text_checkout_shipping_address }} <i class="fa fa-caret-down"></i></a>');*/
/* 			$('#button-shipping-address').trigger('click');*/
/* 			$('a[href=\'#shipping-address-details\']').trigger('click');*/
/* */
/* 			$('#shipping-method-details').parent().find('.panel-heading .panel-title').html('{{ text_checkout_shipping_method }}');*/
/* 			$('#payment-method-details').parent().find('.panel-heading .panel-title').html('{{ text_checkout_payment_method }}');*/
/* 			$('#checkout-confirm-details').parent().find('.panel-heading .panel-title').html('{{ text_checkout_confirm }}');*/
/* 		},*/
/* 		error: function(xhr, ajaxOptions, thrownError) {*/
/* 			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/* 		}*/
/* 	});*/
/* 	//Shippping Method*/
/* 	$.ajax({*/
/* 		url: 'index.php?route=checkout/shipping_method',*/
/* 		dataType: 'html',*/
/* 		complete: function() {*/
/* 			$('#button-guest-shipping').button('reset');*/
/* 		},*/
/* 		success: function(html) {*/
/* 			$('#shipping-method-details .panel-body').html(html);*/
/* */
/* 			$('#shipping-method-details').parent().find('.panel-heading .panel-title').html('<a href="#shipping-method-details" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">{{ text_checkout_shipping_method }} <i class="fa fa-caret-down"></i>');*/
/* 			$('#button-shipping-method').trigger('click');*/
/* 			$('a[href=\'#shipping-method-details\']').trigger('click');*/
/* */
/* 			$('#payment-method-details').parent().find('.panel-heading .panel-title').html('{{ text_checkout_payment_method }}');*/
/* 			$('#checkout-confirm-details').parent().find('.panel-heading .panel-title').html('{{ text_checkout_confirm }}');*/
/* 		},*/
/* 		error: function(xhr, ajaxOptions, thrownError) {*/
/* 			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/* 		}*/
/* 	});*/
/* 	//Payment Method*/
/* 	$.ajax({*/
/* 		url: 'index.php?route=checkout/payment_method',*/
/* 		dataType: 'html',*/
/* 		success: function(html) {*/
/* 			$('#payment-method-details .panel-body').html(html);*/
/* */
/* 			$('#payment-method-details').parent().find('.panel-heading .panel-title').html('<a href="#payment-method-details" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">{{ text_checkout_payment_method }} <i class="fa fa-caret-down"></i></a>');*/
/* 			$('#button-payment-method').trigger('click');*/
/* 			$('a[href=\'#payment-method-details\']').trigger('click');*/
/* */
/* 			$('#checkout-confirm-details').parent().find('.panel-heading .panel-title').html('{{ text_checkout_confirm }}');*/
/* 		},*/
/* 		error: function(xhr, ajaxOptions, thrownError) {*/
/* 			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/* 		}*/
/* 	});*//* */
/* 	//Confirm Order*/
/* 	/*$.ajax({*/
/* 		url: 'index.php?route=checkout/confirm',*/
/* 		dataType: 'html',*/
/* 		complete: function() {*/
/* 			$('#button-payment-method').button('reset');*/
/* 		},*/
/* 		success: function(html) {*/
/* 			$('#checkout-confirm-details .panel-body').html(html);*/
/* */
/* 			$('#checkout-confirm-details').parent().find('.panel-heading .panel-title').html('<a href="#checkout-confirm-details" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">{{ text_checkout_confirm }} <i class="fa fa-caret-down"></i></a>');*/
/* */
/* 			$('a[href=\'#checkout-confirm-details\']').trigger('click');*/
/* 		},*/
/* 		error: function(xhr, ajaxOptions, thrownError) {*/
/* 			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/* 		}*/
/* 	});*//* */
/* });*/
/* */
/* {% endif %}*/
/* */
/* // Checkout*/
/* $(document).delegate('#button-account', 'click', function() {*/
/*     $.ajax({*/
/*         url: 'index.php?route=checkout/' + $('input[name=\'account\']:checked').val(),*/
/*         dataType: 'html',*/
/*         beforeSend: function() {*/
/*         	$('#button-account').button('loading');*/
/* 		},*/
/*         complete: function() {*/
/* 			$('#button-account').button('reset');*/
/*         },*/
/*         success: function(html) {*/
/*             $('.alert-dismissible, .text-danger').remove();*/
/* 			$('.form-group').removeClass('has-error');*/
/* */
/*             $('#addr_details .panel-body').html(html);*/
/* */
/* 			$('a[href=\'#addr_details\']').trigger('click');*/
/*         },*/
/*         error: function(xhr, ajaxOptions, thrownError) {*/
/*             alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*         }*/
/*     });*/
/* });*/
/* */
/* */
/* */
/* // Register*/
/* $(document).delegate('#button-register', 'click', function() {*/
/*     $.ajax({*/
/*         url: 'index.php?route=checkout/register/save',*/
/*         type: 'post',*/
/*         data: $('#addr_details input[type=\'text\'], #addr_details input[type=\'date\'], #addr_details input[type=\'datetime-local\'], #addr_details input[type=\'time\'], #addr_details input[type=\'password\'], #addr_details input[type=\'hidden\'], #addr_details input[type=\'checkbox\']:checked, #addr_details input[type=\'radio\']:checked, #addr_details textarea, #addr_details select'),*/
/*         dataType: 'json',*/
/*         beforeSend: function() {*/
/* 			$('#button-register').button('loading');*/
/* 		},*/
/*         success: function(json) {*/
/*             $('.alert-dismissible, .text-danger').remove();*/
/*             $('.form-group').removeClass('has-error');*/
/* */
/*             if (json['redirect']) {*/
/*                 location = json['redirect'];*/
/*             } else if (json['error']) {*/
/*                 $('#button-register').button('reset');*/
/* */
/*                 if (json['error']['warning']) {*/
/*                     $('#addr_details .panel-body').prepend('<div class="alert alert-danger alert-dismissible"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');*/
/*                 }*/
/* */
/* 				for (i in json['error']) {*/
/* 					var element = $('#input-payment-' + i.replace('_', '-'));*/
/* */
/* 					if ($(element).parent().hasClass('input-group')) {*/
/* 						$(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');*/
/* 					} else {*/
/* 						$(element).after('<div class="text-danger">' + json['error'][i] + '</div>');*/
/* 					}*/
/* 				}*/
/* */
/* 				// Highlight any found errors*/
/* 				$('.text-danger').parent().addClass('has-error');*/
/*             } else {*/
/*                 {% if shipping_required %}*/
/*                 var shipping_address = $('#payment-address input[name=\'shipping_address\']:checked').prop('value');*/
/* */
/*                 if (shipping_address) {*/
/*                     $.ajax({*/
/*                         url: 'index.php?route=checkout/shipping_method',*/
/*                         dataType: 'html',*/
/*                         success: function(html) {*/
/* 							// Add the shipping address*/
/*                             $.ajax({*/
/*                                 url: 'index.php?route=checkout/shipping_address',*/
/*                                 dataType: 'html',*/
/*                                 success: function(html) {*/
/*                                     $('#shipping-address-details .panel-body').html(html);*/
/* */
/* 									$('#shipping-address-details').parent().find('.panel-heading .panel-title').html('<a href="#shipping-address-details" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">{{ text_checkout_shipping_address }} <i class="fa fa-caret-down"></i></a>');*/
/*                                 },*/
/*                                 error: function(xhr, ajaxOptions, thrownError) {*/
/*                                     alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*                                 }*/
/*                             });*/
/* */
/* 							$('#shipping-method-details .panel-body').html(html);*/
/* */
/* 							$('#shipping-method-details').parent().find('.panel-heading .panel-title').html('<a href="#shipping-method-details" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">{{ text_checkout_shipping_method }} <i class="fa fa-caret-down"></i></a>');*/
/* */
/*    							$('a[href=\'#shipping-method-details\']').trigger('click');*/
/* */
/* 							$('#shipping-method-details').parent().find('.panel-heading .panel-title').html('{{ text_checkout_shipping_method }}');*/
/* 							$('#payment-method-details').parent().find('.panel-heading .panel-title').html('{{ text_checkout_payment_method }}');*/
/* 							$('#checkout-confirm-details').parent().find('.panel-heading .panel-title').html('{{ text_checkout_confirm }}');*/
/*                         },*/
/*                         error: function(xhr, ajaxOptions, thrownError) {*/
/*                             alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*                         }*/
/*                     });*/
/*                 } else {*/
/*                     $.ajax({*/
/*                         url: 'index.php?route=checkout/shipping_address',*/
/*                         dataType: 'html',*/
/*                         success: function(html) {*/
/*                             $('#shipping-address-details .panel-body').html(html);*/
/* */
/* 							$('#shipping-address-details').parent().find('.panel-heading .panel-title').html('<a href="#shipping-address-details" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">{{ text_checkout_shipping_address }} <i class="fa fa-caret-down"></i></a>');*/
/* */
/* 							$('a[href=\'#shipping-address-details\']').trigger('click');*/
/* */
/* 							$('#shipping-method-details').parent().find('.panel-heading .panel-title').html('{{ text_checkout_shipping_method }}');*/
/* 							$('#payment-method-details').parent().find('.panel-heading .panel-title').html('{{ text_checkout_payment_method }}');*/
/* 							$('#checkout-confirm-details').parent().find('.panel-heading .panel-title').html('{{ text_checkout_confirm }}');*/
/*                         },*/
/*                         error: function(xhr, ajaxOptions, thrownError) {*/
/*                             alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*                         }*/
/*                     });*/
/*                 }*/
/*                 {% else %}*/
/*                 $.ajax({*/
/*                     url: 'index.php?route=checkout/payment_method',*/
/*                     dataType: 'html',*/
/*                     success: function(html) {*/
/*                         $('#payment-method-details .panel-body').html(html);*/
/* */
/* 						$('#payment-method-details').parent().find('.panel-heading .panel-title').html('<a href="#payment-method-details" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">{{ text_checkout_payment_method }} <i class="fa fa-caret-down"></i></a>');*/
/* */
/* 						$('a[href=\'#payment-method-details\']').trigger('click');*/
/* */
/* 						$('#checkout-confirm-details').parent().find('.panel-heading .panel-title').html('{{ text_checkout_confirm }}');*/
/*                     },*/
/*                     error: function(xhr, ajaxOptions, thrownError) {*/
/*                         alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*                     }*/
/*                 });*/
/*                 {% endif %}*/
/* */
/*                 $.ajax({*/
/*                     url: 'index.php?route=checkout/payment_address',*/
/*                     dataType: 'html',*/
/*                     complete: function() {*/
/*                         $('#button-register').button('reset');*/
/*                     },*/
/*                     success: function(html) {*/
/*                         $('#addr_details .panel-body').html(html);*/
/* */
/* 						$('#addr_details').parent().find('.panel-heading .panel-title').html('<a href="#addr_details" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">{{ text_checkout_payment_address }} <i class="fa fa-caret-down"></i></a>');*/
/*                     },*/
/*                     error: function(xhr, ajaxOptions, thrownError) {*/
/*                         alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*                     }*/
/*                 });*/
/*             }*/
/*         },*/
/*         error: function(xhr, ajaxOptions, thrownError) {*/
/*             alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*         }*/
/*     });*/
/* });*/
/* */
/* // Payment Address*/
/* $(document).delegate('#button-payment-address', 'click', function() {*/
/*     $.ajax({*/
/*         url: 'index.php?route=checkout/payment_address/save',*/
/*         type: 'post',*/
/*         data: $('#addr_details input[type=\'text\'], #addr_details input[type=\'date\'], #addr_details input[type=\'datetime-local\'], #addr_details input[type=\'time\'], #addr_details input[type=\'password\'], #addr_details input[type=\'checkbox\']:checked, #addr_details input[type=\'radio\']:checked, #addr_details input[type=\'hidden\'], #addr_details textarea, #addr_details radio'),*/
/*         dataType: 'json',*/
/*         beforeSend: function() {*/
/*         	$('#button-payment-address').button('loading');*/
/* 		},*/
/*         complete: function() {*/
/* 			$('#button-payment-address').button('reset');*/
/*         },*/
/*         success: function(json) {*/
/*             $('.alert-dismissible, .text-danger').remove();*/
/* 			$('.form-group').removeClass('has-error');*/
/*             */
/*         },*/
/*         error: function(xhr, ajaxOptions, thrownError) {*/
/*             alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*         }*/
/*     });*/
/* });*/
/* */
/* // Shipping Address*/
/* $(document).delegate('#button-shipping-address', 'click', function() {*/
/*     $.ajax({*/
/*         url: 'index.php?route=checkout/shipping_address/save',*/
/*         type: 'post',*/
/*         data: $('#shipping-address-details input[type=\'text\'], #shipping-address-details input[type=\'date\'], #shipping-address-details input[type=\'datetime-local\'], #shipping-address-details input[type=\'time\'], #shipping-address-details input[type=\'password\'], #shipping-address-details input[type=\'checkbox\']:checked, #shipping-address-details input[type=\'radio\']:checked, #shipping-address-details textarea, #shipping-address-details select'),*/
/*         dataType: 'json',*/
/*         beforeSend: function() {*/
/* 			$('#button-shipping-address').button('loading');*/
/* 	    },*/
/*         success: function(json) {*/
/*             $('.alert-dismissible, .text-danger').remove();*/
/* 			$('.form-group').removeClass('has-error');*/
/* */
/*             if (json['redirect']) {*/
/*                 location = json['redirect'];*/
/*             } else if (json['error']) {*/
/*                 $('#button-shipping-address').button('reset');*/
/* */
/*                 if (json['error']['warning']) {*/
/*                     $('#shipping-address-details .panel-body').prepend('<div class="alert alert-warning alert-dismissible">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');*/
/*                 }*/
/* */
/* 				for (i in json['error']) {*/
/* 					var element = $('#input-shipping-' + i.replace('_', '-'));*/
/* */
/* 					if ($(element).parent().hasClass('input-group')) {*/
/* 						$(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');*/
/* 					} else {*/
/* 						$(element).after('<div class="text-danger">' + json['error'][i] + '</div>');*/
/* 					}*/
/* 				}*/
/* */
/* 				// Highlight any found errors*/
/* 				$('.text-danger').parent().parent().addClass('has-error');*/
/*             } else {*/
/*                 $.ajax({*/
/*                     url: 'index.php?route=checkout/shipping_method',*/
/*                     dataType: 'html',*/
/*                     complete: function() {*/
/*                         $('#button-shipping-address').button('reset');*/
/*                     },*/
/*                     success: function(html) {*/
/*                         $('#shipping-method-details .panel-body').html(html);*/
/* */
/* 						$('#shipping-method-details').parent().find('.panel-heading .panel-title').html('<a href="#shipping-method-details" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">{{ text_checkout_shipping_method }} <i class="fa fa-caret-down"></i></a>');*/
/* */
/* 						$('a[href=\'#shipping-method-details\']').trigger('click');*/
/* */
/* 						$('#payment-method-details').parent().find('.panel-heading .panel-title').html('{{ text_checkout_payment_method }}');*/
/* 						$('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('{{ text_checkout_confirm }}');*/
/* 						*/
/*                         $.ajax({*/
/*                             url: 'index.php?route=checkout/shipping_address',*/
/*                             dataType: 'html',*/
/*                             success: function(html) {*/
/*                                 $('#shipping-address-details .panel-body').html(html);*/
/*                             },*/
/*                             error: function(xhr, ajaxOptions, thrownError) {*/
/*                                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*                             }*/
/*                         });*/
/*                     },*/
/*                     error: function(xhr, ajaxOptions, thrownError) {*/
/*                         alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*                     }*/
/*                 }).done(function() {*/
/* 					$.ajax({*/
/* 						url: 'index.php?route=checkout/payment_address',*/
/* 						dataType: 'html',*/
/* 						success: function(html) {*/
/* 							$('#collapse-payment-address .panel-body').html(html);*/
/* 						},*/
/* 						error: function(xhr, ajaxOptions, thrownError) {*/
/* 							alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/* 						}*/
/* 					});*/
/* 				});*/
/*             }*/
/*         },*/
/*         error: function(xhr, ajaxOptions, thrownError) {*/
/*             alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*         }*/
/*     });*/
/* });*/
/* */
/* */
/* $(document).delegate('#button-shipping-method', 'click', function() {*/
/*     $.ajax({*/
/*         url: 'index.php?route=checkout/shipping_method/save',*/
/*         type: 'post',*/
/*         data: $('#shipping-method-details input[type=\'radio\']:checked, #shipping-method-details textarea'),*/
/*         dataType: 'json',*/
/*         beforeSend: function() {*/
/*         	$('#button-shipping-method').button('loading');*/
/* 		},*/
/*         success: function(json) {*/
/*             $('.alert-dismissible, .text-danger').remove();*/
/* */
/*             if (json['redirect']) {*/
/*                 location = json['redirect'];*/
/*             } else if (json['error']) {*/
/*                 $('#button-shipping-method').button('reset');*/
/* */
/*                 if (json['error']['warning']) {*/
/*                     $('#shipping-method-details .panel-body').prepend('<div class="alert alert-danger alert-dismissible">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');*/
/*                 }*/
/*             } else {*/
/*                 $.ajax({*/
/*                     url: 'index.php?route=checkout/payment_method',*/
/*                     dataType: 'html',*/
/*                     complete: function() {*/
/*                         $('#button-shipping-method').button('reset');*/
/*                     },*/
/*                     success: function(html) {*/
/*                         $('#payment-method-details .panel-body').html(html);*/
/* */
/* 						$('#payment-method-details').parent().find('.panel-heading .panel-title').html('<a href="#payment-method-details" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">{{ text_checkout_payment_method }} <i class="fa fa-caret-down"></i></a>');*/
/* */
/* 						$('a[href=\'#payment-method-details\']').trigger('click');*/
/* */
/* 						$('#collapse-checkout-confirm').parent().find('.panel-heading .panel-title').html('{{ text_checkout_confirm }}');*/
/*                     },*/
/*                     error: function(xhr, ajaxOptions, thrownError) {*/
/*                         alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*                     }*/
/*                 });*/
/*             }*/
/*         },*/
/*         error: function(xhr, ajaxOptions, thrownError) {*/
/*             alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*         }*/
/*     });*/
/* });*/
/* */
/* $(document).delegate('#button-payment-method', 'click', function() {*/
/*     $.ajax({*/
/*         url: 'index.php?route=checkout/payment_method/save',*/
/*         type: 'post',*/
/*         data: $('#payment-method-details input[type=\'radio\']:checked, #payment-method-details input[type=\'checkbox\']:checked, #payment-method-details textarea'),*/
/*         dataType: 'json',*/
/*         beforeSend: function() {*/
/*          	$('#button-payment-method').button('loading');*/
/* 		},*/
/*         success: function(json) {*/
/*             $('.alert-dismissible, .text-danger').remove();*/
/* */
/*             if (json['redirect']) {*/
/*                 location = json['redirect'];*/
/*             } else if (json['error']) {*/
/*                 $('#button-payment-method').button('reset');*/
/*                 */
/*                 if (json['error']['warning']) {*/
/*                     $('#payment-method-details .panel-body').prepend('<div class="alert alert-danger alert-dismissible">' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');*/
/*                 }*/
/*             } else {*/
/*                 $.ajax({*/
/*                     url: 'index.php?route=checkout/confirm',*/
/*                     dataType: 'html',*/
/*                     complete: function() {*/
/*                         $('#button-payment-method').button('reset');*/
/*                     },*/
/*                     success: function(html) {*/
/*                         $('#checkout-confirm-details .panel-body').html(html);*/
/* */
/* 						$('#checkout-confirm-details').parent().find('.panel-heading .panel-title').html('<a href="#checkout-confirm-details" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">{{ text_checkout_confirm }} <i class="fa fa-caret-down"></i></a>');*/
/* */
/* 						$('a[href=\'#checkout-confirm-details\']').trigger('click');*/
/* 					},*/
/*                     error: function(xhr, ajaxOptions, thrownError) {*/
/*                         alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*                     }*/
/*                 });*/
/*             }*/
/*         },*/
/*         error: function(xhr, ajaxOptions, thrownError) {*/
/*             alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*         }*/
/*     });*/
/* });*/
/* //--></script>*/
/* {{ footer }}*/
