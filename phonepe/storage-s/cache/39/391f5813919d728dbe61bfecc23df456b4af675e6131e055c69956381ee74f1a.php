<?php

/* default/template/extension/payment/pinepg.twig */
class __TwigTemplate_6fa8b1ea328cf3ba215ba6ba9e5eab45bed66c7d318cd13aeb15bf2ccb34b129 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<form action=\"";
        echo (isset($context["action"]) ? $context["action"] : null);
        echo "\" method=\"post\">
  <input type=\"hidden\" name=\"ppc_MerchantID\" value=\"";
        // line 2
        echo (isset($context["ppc_MerchantID"]) ? $context["ppc_MerchantID"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"ppc_MerchantAccessCode\" value=\"";
        // line 3
        echo (isset($context["ppc_MerchantAccessCode"]) ? $context["ppc_MerchantAccessCode"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"ppc_DIA_SECRET\" value=\"";
        // line 4
        echo (isset($context["ppc_DIA_SECRET"]) ? $context["ppc_DIA_SECRET"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"ppc_DIA_SECRET_TYPE\" value=\"";
        // line 5
        echo (isset($context["ppc_DIA_SECRET_TYPE"]) ? $context["ppc_DIA_SECRET_TYPE"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"ppc_PayModeOnLandingPage\" value=\"";
        // line 6
        echo (isset($context["ppc_PayModeOnLandingPage"]) ? $context["ppc_PayModeOnLandingPage"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"ppc_LPC_SEQ\" value=\"";
        // line 7
        echo (isset($context["ppc_LPC_SEQ"]) ? $context["ppc_LPC_SEQ"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"ppc_Amount\" value=\"";
        // line 8
        echo (isset($context["ppc_Amount"]) ? $context["ppc_Amount"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"ppc_NavigationMode\" value=\"";
        // line 9
        echo (isset($context["ppc_NavigationMode"]) ? $context["ppc_NavigationMode"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"ppc_Product_Code\" value=\"";
        // line 10
        echo (isset($context["ppc_Product_Code"]) ? $context["ppc_Product_Code"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"ppc_MerchantReturnURL\" value=\"";
        // line 11
        echo (isset($context["ppc_MerchantReturnURL"]) ? $context["ppc_MerchantReturnURL"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"ppc_TransactionType\" value=\"";
        // line 12
        echo (isset($context["ppc_TransactionType"]) ? $context["ppc_TransactionType"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"ppc_UniqueMerchantTxnID\" value=\"";
        // line 13
        echo (isset($context["ppc_UniqueMerchantTxnID"]) ? $context["ppc_UniqueMerchantTxnID"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"ppc_CustomerMobile\" value=\"";
        // line 14
        echo (isset($context["ppc_CustomerMobile"]) ? $context["ppc_CustomerMobile"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"ppc_CustomerEmail\" value=\"";
        // line 15
        echo (isset($context["ppc_CustomerEmail"]) ? $context["ppc_CustomerEmail"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"ppc_CustomerFirstName\" value=\"";
        // line 16
        echo (isset($context["ppc_CustomerFirstName"]) ? $context["ppc_CustomerFirstName"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"ppc_CustomerLastName\" value=\"";
        // line 17
        echo (isset($context["ppc_CustomerLastName"]) ? $context["ppc_CustomerLastName"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"ppc_CustomerCity\" value=\"";
        // line 18
        echo (isset($context["ppc_CustomerCity"]) ? $context["ppc_CustomerCity"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"ppc_CustomerState\" value=\"";
        // line 19
        echo (isset($context["ppc_CustomerState"]) ? $context["ppc_CustomerState"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"ppc_CustomerCountry\" value=\"";
        // line 20
        echo (isset($context["ppc_CustomerCountry"]) ? $context["ppc_CustomerCountry"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"ppc_CustomerAddress1\" value=\"";
        // line 21
        echo (isset($context["ppc_CustomerAddress1"]) ? $context["ppc_CustomerAddress1"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"ppc_CustomerAddress2\" value=\"";
        // line 22
        echo (isset($context["ppc_CustomerAddress2"]) ? $context["ppc_CustomerAddress2"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"ppc_CustomerAddressPIN\" value=\"";
        // line 23
        echo (isset($context["ppc_CustomerAddressPIN"]) ? $context["ppc_CustomerAddressPIN"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"ppc_ShippingFirstName\" value=\"";
        // line 24
        echo (isset($context["ppc_ShippingFirstName"]) ? $context["ppc_ShippingFirstName"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"ppc_ShippingLastName\" value=\"";
        // line 25
        echo (isset($context["ppc_ShippingLastName"]) ? $context["ppc_ShippingLastName"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"ppc_ShippingAddress1\" value=\"";
        // line 26
        echo (isset($context["ppc_ShippingAddress1"]) ? $context["ppc_ShippingAddress1"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"ppc_ShippingAddress2\" value=\"";
        // line 27
        echo (isset($context["ppc_ShippingAddress2"]) ? $context["ppc_ShippingAddress2"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"ppc_ShippingCity\" value=\"";
        // line 28
        echo (isset($context["ppc_ShippingCity"]) ? $context["ppc_ShippingCity"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"ppc_ShippingState\" value=\"";
        // line 29
        echo (isset($context["ppc_ShippingState"]) ? $context["ppc_ShippingState"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"ppc_ShippingCountry\" value=\"";
        // line 30
        echo (isset($context["ppc_ShippingCountry"]) ? $context["ppc_ShippingCountry"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"ppc_ShippingZipCode\" value=\"";
        // line 31
        echo (isset($context["ppc_ShippingZipCode"]) ? $context["ppc_ShippingZipCode"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"ppc_UdfField1\" value=\"";
        // line 32
        echo (isset($context["ppc_UdfField1"]) ? $context["ppc_UdfField1"] : null);
        echo "\" />
  <input type=\"hidden\" name=\"ppc_MerchantProductInfo\" value=\"";
        // line 33
        echo (isset($context["ppc_MerchantProductInfo"]) ? $context["ppc_MerchantProductInfo"] : null);
        echo "\" />

  
  <div class=\"buttons\">
    <div class=\"right\">
      <input type=\"submit\" value=\"";
        // line 38
        echo (isset($context["button_confirm"]) ? $context["button_confirm"] : null);
        echo "\" class=\"button\" />
    </div>
  </div>
</form>";
    }

    public function getTemplateName()
    {
        return "default/template/extension/payment/pinepg.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  156 => 38,  148 => 33,  144 => 32,  140 => 31,  136 => 30,  132 => 29,  128 => 28,  124 => 27,  120 => 26,  116 => 25,  112 => 24,  108 => 23,  104 => 22,  100 => 21,  96 => 20,  92 => 19,  88 => 18,  84 => 17,  80 => 16,  76 => 15,  72 => 14,  68 => 13,  64 => 12,  60 => 11,  56 => 10,  52 => 9,  48 => 8,  44 => 7,  40 => 6,  36 => 5,  32 => 4,  28 => 3,  24 => 2,  19 => 1,);
    }
}
/* <form action="{{ action }}" method="post">*/
/*   <input type="hidden" name="ppc_MerchantID" value="{{ ppc_MerchantID }}" />*/
/*   <input type="hidden" name="ppc_MerchantAccessCode" value="{{ ppc_MerchantAccessCode }}" />*/
/*   <input type="hidden" name="ppc_DIA_SECRET" value="{{ ppc_DIA_SECRET }}" />*/
/*   <input type="hidden" name="ppc_DIA_SECRET_TYPE" value="{{ ppc_DIA_SECRET_TYPE }}" />*/
/*   <input type="hidden" name="ppc_PayModeOnLandingPage" value="{{ ppc_PayModeOnLandingPage }}" />*/
/*   <input type="hidden" name="ppc_LPC_SEQ" value="{{ ppc_LPC_SEQ }}" />*/
/*   <input type="hidden" name="ppc_Amount" value="{{ ppc_Amount }}" />*/
/*   <input type="hidden" name="ppc_NavigationMode" value="{{ ppc_NavigationMode }}" />*/
/*   <input type="hidden" name="ppc_Product_Code" value="{{ ppc_Product_Code }}" />*/
/*   <input type="hidden" name="ppc_MerchantReturnURL" value="{{ ppc_MerchantReturnURL }}" />*/
/*   <input type="hidden" name="ppc_TransactionType" value="{{ ppc_TransactionType }}" />*/
/*   <input type="hidden" name="ppc_UniqueMerchantTxnID" value="{{ ppc_UniqueMerchantTxnID }}" />*/
/*   <input type="hidden" name="ppc_CustomerMobile" value="{{ ppc_CustomerMobile }}" />*/
/*   <input type="hidden" name="ppc_CustomerEmail" value="{{ ppc_CustomerEmail }}" />*/
/*   <input type="hidden" name="ppc_CustomerFirstName" value="{{ ppc_CustomerFirstName }}" />*/
/*   <input type="hidden" name="ppc_CustomerLastName" value="{{ ppc_CustomerLastName }}" />*/
/*   <input type="hidden" name="ppc_CustomerCity" value="{{ ppc_CustomerCity }}" />*/
/*   <input type="hidden" name="ppc_CustomerState" value="{{ ppc_CustomerState }}" />*/
/*   <input type="hidden" name="ppc_CustomerCountry" value="{{ ppc_CustomerCountry }}" />*/
/*   <input type="hidden" name="ppc_CustomerAddress1" value="{{ ppc_CustomerAddress1 }}" />*/
/*   <input type="hidden" name="ppc_CustomerAddress2" value="{{ ppc_CustomerAddress2 }}" />*/
/*   <input type="hidden" name="ppc_CustomerAddressPIN" value="{{ ppc_CustomerAddressPIN }}" />*/
/*   <input type="hidden" name="ppc_ShippingFirstName" value="{{ ppc_ShippingFirstName }}" />*/
/*   <input type="hidden" name="ppc_ShippingLastName" value="{{ ppc_ShippingLastName }}" />*/
/*   <input type="hidden" name="ppc_ShippingAddress1" value="{{ ppc_ShippingAddress1 }}" />*/
/*   <input type="hidden" name="ppc_ShippingAddress2" value="{{ ppc_ShippingAddress2 }}" />*/
/*   <input type="hidden" name="ppc_ShippingCity" value="{{ ppc_ShippingCity }}" />*/
/*   <input type="hidden" name="ppc_ShippingState" value="{{ ppc_ShippingState }}" />*/
/*   <input type="hidden" name="ppc_ShippingCountry" value="{{ ppc_ShippingCountry }}" />*/
/*   <input type="hidden" name="ppc_ShippingZipCode" value="{{ ppc_ShippingZipCode }}" />*/
/*   <input type="hidden" name="ppc_UdfField1" value="{{ ppc_UdfField1 }}" />*/
/*   <input type="hidden" name="ppc_MerchantProductInfo" value="{{ ppc_MerchantProductInfo }}" />*/
/* */
/*   */
/*   <div class="buttons">*/
/*     <div class="right">*/
/*       <input type="submit" value="{{ button_confirm }}" class="button" />*/
/*     </div>*/
/*   </div>*/
/* </form>*/
