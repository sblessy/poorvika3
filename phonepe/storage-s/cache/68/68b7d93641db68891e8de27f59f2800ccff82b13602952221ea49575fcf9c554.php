<?php

/* catalog/wk_preorder_productlist.twig */
class __TwigTemplate_7f6c7ce566c3bf1e82fcd54af0927badd1b20fa4ffe480773a3a41c0166b0ce0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo " 
";
        // line 2
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo " 
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
        <div class=\"pull-right\">
        ";
        // line 7
        if ((array_key_exists("convert", $context) && (isset($context["convert"]) ? $context["convert"] : null))) {
            echo " 
        <a href=\"";
            // line 8
            echo (isset($context["convert"]) ? $context["convert"] : null);
            echo "\" class=\"btn btn-primary\" data-toggle=\"tooptip\" title=\"";
            echo (isset($context["button_convert"]) ? $context["button_convert"] : null);
            echo "\" ><i class=\"fa fa-refresh\"></i>
          </a>
        ";
        }
        // line 10
        echo " 
      
          <a href=\"";
        // line 12
        echo (isset($context["add"]) ? $context["add"] : null);
        echo "\" class=\"btn btn-primary\" data-toggle=\"tooptip\" title=\"";
        echo (isset($context["button_add"]) ? $context["button_add"] : null);
        echo "\" >
            <i class=\"fa fa-plus\"></i>
          </a>
           <button type=\"button\" id=\"delete\" class=\"btn btn-danger\" data-toggle=\"tooltip\" title=\"";
        // line 15
        echo (isset($context["button_delete"]) ? $context["button_delete"] : null);
        echo "\">
            <i class=\"fa fa-trash\"></i>
          </button>
          
        </div>
        <h1>";
        // line 20
        echo (isset($context["heading_title_productlist"]) ? $context["heading_title_productlist"] : null);
        echo "</h1>
        <ul class=\"breadcrumb\">
          ";
        // line 22
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            echo " 
          <li><a href=\"";
            // line 23
            echo $this->getAttribute($context["breadcrumb"], "href", array(), "array");
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array(), "array");
            echo "</a></li>
          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 24
        echo " 
        </ul>
    </div>
  </div>

  <div class=\"container-fluid\">
    ";
        // line 30
        if ((isset($context["error_warning"]) ? $context["error_warning"] : null)) {
            echo " 
      <div class=\"alert alert-danger\">
        <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
        ";
            // line 33
            echo (isset($context["error_warning"]) ? $context["error_warning"] : null);
            echo " 
      </div>
    ";
        }
        // line 35
        echo " 
    ";
        // line 36
        if ((isset($context["error_warning_2"]) ? $context["error_warning_2"] : null)) {
            echo " 
      <div class=\"alert alert-danger\">
        <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
        ";
            // line 39
            echo (isset($context["error_warning_2"]) ? $context["error_warning_2"] : null);
            echo " 
      </div>
    ";
        }
        // line 41
        echo " 
    ";
        // line 42
        if ((isset($context["success"]) ? $context["success"] : null)) {
            echo " 
      <div class=\"alert alert-success\">
        <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
        ";
            // line 45
            echo (isset($context["success"]) ? $context["success"] : null);
            echo " 
      </div>
    ";
        }
        // line 47
        echo " 
    ";
        // line 48
        if ((array_key_exists("added_success", $context) && (isset($context["added_success"]) ? $context["added_success"] : null))) {
            echo " 
      <div class=\"alert alert-success\">
        <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
        ";
            // line 51
            echo (isset($context["added_success"]) ? $context["added_success"] : null);
            echo " 
      </div>
    ";
        }
        // line 53
        echo " 
    ";
        // line 54
        if ((array_key_exists("alreadyAdded", $context) && (isset($context["alreadyAdded"]) ? $context["alreadyAdded"] : null))) {
            echo " 
      <div class=\"alert alert-warning\">
        <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
        ";
            // line 57
            echo (isset($context["alreadyAdded"]) ? $context["alreadyAdded"] : null);
            echo " 
      </div>
    ";
        }
        // line 59
        echo " 
    ";
        // line 60
        if ((array_key_exists("failure", $context) && (isset($context["failure"]) ? $context["failure"] : null))) {
            echo " 
      <div class=\"alert alert-danger\">
        <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
        ";
            // line 63
            echo (isset($context["failure"]) ? $context["failure"] : null);
            echo " 
      </div>
    ";
        }
        // line 65
        echo " 
    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <h3 class=\"panel-title\"><i class=\"fa fa-list\"></i>";
        // line 68
        echo (isset($context["heading_title_productlist"]) ? $context["heading_title_productlist"] : null);
        echo "</h3>
      </div>
      <div class=\"panel-body\">
        <div class=\"well\">
          <div class=\"row\">
            <div class=\"col-sm-4\">
              <div class=\"form-group\">
                <label class=\"control-label\">
                  ";
        // line 76
        echo (isset($context["text_pname"]) ? $context["text_pname"] : null);
        echo " 
                </label>
                <input type=\"text\" class=\"form-control\" name=\"filter_name\" value=\"";
        // line 78
        echo (isset($context["name"]) ? $context["name"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["text_pname"]) ? $context["text_pname"] : null);
        echo "\" />
              </div>
              <div class=\"form-group\">
                <label class=\"control-label\">
                  ";
        // line 82
        echo (isset($context["text_deduction_type"]) ? $context["text_deduction_type"] : null);
        echo " 
                </label>
                <select name=\"filter_deduction_type\" class=\"form-control\">
                  <option value=\"\"></option>
                  <option value=\"pc\" ";
        // line 86
        if (((isset($context["deduction_type"]) ? $context["deduction_type"] : null) == "pc")) {
            echo " ";
            echo "selected";
        }
        echo " >";
        echo (isset($context["text_percentage"]) ? $context["text_percentage"] : null);
        echo "</option>
                  <option value=\"fp\" ";
        // line 87
        if (((isset($context["deduction_type"]) ? $context["deduction_type"] : null) == "fp")) {
            echo " ";
            echo "selected";
        }
        echo " >";
        echo (isset($context["text_fixed"]) ? $context["text_fixed"] : null);
        echo "</option>
                </select>
              </div>
            </div>
            <div class=\"col-sm-4\">
              <div class=\"form-group\">
                <label class=\"control-label\">
                  ";
        // line 94
        echo (isset($context["text_model"]) ? $context["text_model"] : null);
        echo " 
                </label>
                <input type=\"text\" class=\"form-control date\" name=\"filter_model\" value=\"";
        // line 96
        echo (isset($context["model"]) ? $context["model"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["text_model"]) ? $context["text_model"] : null);
        echo "\" />
              </div>
              <div class=\"form-group\">
                <label class=\"control-label\">
                  ";
        // line 100
        echo (isset($context["text_status"]) ? $context["text_status"] : null);
        echo " 
                </label>
                <select class=\"form-control\" name=\"filter_status\">
                  <option value=\"\"></option>
                  <option value=\"1\" ";
        // line 104
        if ((array_key_exists("status", $context) && (isset($context["status"]) ? $context["status"] : null))) {
            echo " ";
            echo "selected";
        }
        echo " >";
        echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
        echo "</option>
                  <option value=\"0\" ";
        // line 105
        if ((array_key_exists("status", $context) && ((isset($context["status"]) ? $context["status"] : null) == "0"))) {
            echo " ";
            echo "selected";
        }
        echo " >";
        echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
        echo "</option>
                </select>
              </div>
            </div>
            <div class=\"col-sm-4\">
              <div class=\"form-group\">
                <label class=\"control-label\">
                  ";
        // line 112
        echo (isset($context["text_preorder_price"]) ? $context["text_preorder_price"] : null);
        echo " 
                </label>
                <input type=\"text\" class=\"form-control\" name=\"filter_preorder_price\" value=\"";
        // line 114
        echo (isset($context["preorder_price"]) ? $context["preorder_price"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["text_preorder_price"]) ? $context["text_preorder_price"] : null);
        echo "\" />
              </div>
              <div class=\"btn-group pull-right\">
                  <button class=\"btn btn-primary\" type=\"button\" onclick=\"filter();\" data-toggle=\"tooltip\" title=\"";
        // line 117
        echo (isset($context["button_filter"]) ? $context["button_filter"] : null);
        echo "\" >
                    <i class=\"fa fa-filter\"></i>
                  </button>
                  <a class=\"btn btn-danger\" onclick=\"clearfilter();\" data-toggle=\"tooltip\" title=\"";
        // line 120
        echo (isset($context["button_clrfilter"]) ? $context["button_clrfilter"] : null);
        echo "\">
                    <i class=\"fa fa-eraser\"></i>
                  </a>
              </div>
            </div>
          </div>
        </div>
        <form action=\"";
        // line 127
        echo (isset($context["delete"]) ? $context["delete"] : null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form\" class=\"form-horizontal\">
          <table class=\"table table-bordered table-hover\">
            <thead>
              <tr>
              <td class=\"text-center\" style=\"width:1px\"><input type=\"checkbox\" onclick=\"\$('input[name*=\\'selected\\']').prop('checked', this.checked);\" /></td>
              <td class=\"text-left\">
                ";
        // line 133
        if (((isset($context["sort"]) ? $context["sort"] : null) == "pd.name")) {
            echo " 
                  <a href=\"";
            // line 134
            echo (isset($context["sort_pname"]) ? $context["sort_pname"] : null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, (isset($context["order"]) ? $context["order"] : null));
            echo "\">";
            echo (isset($context["text_pname"]) ? $context["text_pname"] : null);
            echo "</a>
                ";
        } else {
            // line 135
            echo " 
                  <a href=\"";
            // line 136
            echo (isset($context["sort_pname"]) ? $context["sort_pname"] : null);
            echo "\">";
            echo (isset($context["text_pname"]) ? $context["text_pname"] : null);
            echo "</a>
                ";
        }
        // line 137
        echo " 
              </td>
              <td class=\"text-left\">
                ";
        // line 140
        if (((isset($context["sort"]) ? $context["sort"] : null) == "p.model")) {
            echo " 
                  <a href=\"";
            // line 141
            echo (isset($context["sort_pmodel"]) ? $context["sort_pmodel"] : null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, (isset($context["order"]) ? $context["order"] : null));
            echo "\">";
            echo (isset($context["text_model"]) ? $context["text_model"] : null);
            echo "</a>
                ";
        } else {
            // line 142
            echo " 
                  <a href=\"";
            // line 143
            echo (isset($context["sort_pmodel"]) ? $context["sort_pmodel"] : null);
            echo "\">";
            echo (isset($context["text_model"]) ? $context["text_model"] : null);
            echo "</a>
                ";
        }
        // line 144
        echo " 
              </td>
              <td class=\"text-left\">
                ";
        // line 147
        if (((isset($context["sort"]) ? $context["sort"] : null) == "pp.initial_price")) {
            echo " 
                  <a href=\"";
            // line 148
            echo (isset($context["sort_wk_fixed_price"]) ? $context["sort_wk_fixed_price"] : null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, (isset($context["order"]) ? $context["order"] : null));
            echo "\">";
            echo (isset($context["text_preorder_price"]) ? $context["text_preorder_price"] : null);
            echo "</a>
                ";
        } else {
            // line 149
            echo " 
                  <a href=\"";
            // line 150
            echo (isset($context["sort_wk_fixed_price"]) ? $context["sort_wk_fixed_price"] : null);
            echo "\">";
            echo (isset($context["text_preorder_price"]) ? $context["text_preorder_price"] : null);
            echo "</a>
                ";
        }
        // line 151
        echo " 
              </td>

              <td class=\"text-left\">
                ";
        // line 155
        if (((isset($context["sort"]) ? $context["sort"] : null) == "pp.deduction_type")) {
            echo " 
                  <a href=\"";
            // line 156
            echo (isset($context["sort_deduction_type"]) ? $context["sort_deduction_type"] : null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, (isset($context["order"]) ? $context["order"] : null));
            echo "\">";
            echo (isset($context["text_deduction_type"]) ? $context["text_deduction_type"] : null);
            echo "</a>
                ";
        } else {
            // line 157
            echo " 
                  <a href=\"";
            // line 158
            echo (isset($context["sort_deduction_type"]) ? $context["sort_deduction_type"] : null);
            echo "\">";
            echo (isset($context["text_deduction_type"]) ? $context["text_deduction_type"] : null);
            echo "</a>
                ";
        }
        // line 159
        echo " 
              </td>
              <td class=\"text-center\">
                ";
        // line 162
        if (((isset($context["sort"]) ? $context["sort"] : null) == "pp.status")) {
            echo " 
                  <a href=\"";
            // line 163
            echo (isset($context["sort_pp_status"]) ? $context["sort_pp_status"] : null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, (isset($context["order"]) ? $context["order"] : null));
            echo "\">";
            echo (isset($context["text_status"]) ? $context["text_status"] : null);
            echo "</a>
                ";
        } else {
            // line 164
            echo " 
                  <a href=\"";
            // line 165
            echo (isset($context["sort_pp_status"]) ? $context["sort_pp_status"] : null);
            echo "\">";
            echo (isset($context["text_status"]) ? $context["text_status"] : null);
            echo "</a>
                ";
        }
        // line 166
        echo " 
              </td>
                <td class=\"text-center\">";
        // line 168
        echo (isset($context["text_action"]) ? $context["text_action"] : null);
        echo "</td>
              </tr>
            </thead>
            <tbody>

              ";
        // line 173
        if (array_key_exists("products", $context)) {
            echo " 
              ";
            // line 174
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                echo " 
              <tr>

                <td class=\"text-center\">
                  <input type=\"checkbox\" name=\"selected[]\" value=\"";
                // line 178
                echo $this->getAttribute($context["product"], "id", array(), "array");
                echo "\" />
                </td>
                <td class=\"text-left\">";
                // line 180
                echo $this->getAttribute($context["product"], "product_name", array(), "array");
                echo "</td>
                <td class=\"text-left\">";
                // line 181
                echo $this->getAttribute($context["product"], "product_model", array(), "array");
                echo "</td>
                <td class=\"text-left\">";
                // line 182
                echo $this->getAttribute($context["product"], "preorder_price", array(), "array");
                echo "</td>
                <td class=\"text-left\">";
                // line 183
                echo $this->getAttribute($context["product"], "deduction_type", array(), "array");
                echo "</td>
                <td class=\"text-center\">
                  ";
                // line 185
                if ($this->getAttribute($context["product"], "status", array(), "array")) {
                    echo " ";
                    echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
                    echo " ";
                } else {
                    echo " ";
                    echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
                    echo " ";
                }
                echo " 
                </td>
                <td class=\"text-center\">
                  ";
                // line 188
                if ($this->getAttribute($context["product"], "status", array(), "array")) {
                    echo " 
                    <a href=\"";
                    // line 189
                    echo $this->getAttribute($context["product"], "edit", array(), "array");
                    echo "\" data-toggle=\"tooltip\" title=\"";
                    echo (isset($context["text_edit"]) ? $context["text_edit"] : null);
                    echo "\" class=\"btn btn-warning\" >
                      <i class=\"fa fa-edit\"></i>
                    </a>
                  ";
                } else {
                    // line 192
                    echo " 
                    <button type=\"button\" class=\"btn btn-warning\" disabled >
                      <i class=\"fa fa-edit\"></i>
                    </button>
                  ";
                }
                // line 196
                echo " 
                </td>
              </tr>
              ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 199
            echo " 
              ";
        } else {
            // line 200
            echo " 
              <tr>
                <td class=\"text-center\" colspan=\"12\">";
            // line 202
            echo "no records founds";
            echo "</td>
              </tr>
              ";
        }
        // line 204
        echo " 
            </tbody>
          </table>
      </form>
      <div class=\"row\">
        <div class=\"col-sm-6 text-left\">";
        // line 209
        echo (isset($context["pagination"]) ? $context["pagination"] : null);
        echo "</div>
        <div class=\"col-sm-6 text-right\">";
        // line 210
        echo (isset($context["results"]) ? $context["results"] : null);
        echo "</div>
      </div>
  </div>
  </div>
</div>
</div>
<script type=\"text/javascript\"><!--
\$('body').on('click','#delete',function(){
  var flag = false;
  \$('input[type=\\'checkbox\\']').each(function(){
    if (\$(this).is(':checked')) {
      flag = true;
    }
  });
  console.log(flag);
  if (flag) {
      confirm('";
        // line 226
        echo (isset($context["text_confirmation"]) ? $context["text_confirmation"] : null);
        echo "') ? \$('form').submit() : false;
  } else {
     alert('";
        // line 228
        echo (isset($context["error_select"]) ? $context["error_select"] : null);
        echo "');
  }
});

function clearfilter() {
  url = 'index.php?route=catalog/wk_preorder/preorder_productlist&user_token=";
        // line 233
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "';
  location = url;
}

function filter() {
  url = 'index.php?route=catalog/wk_preorder/preorder_productlist&user_token=";
        // line 238
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "';

  var filter_name = \$('input[name=\\'filter_name\\']').val();


  if (filter_name) {
    url += '&name=' + encodeURIComponent(filter_name);
  }

  var filter_model = \$('input[name=\\'filter_model\\']').val();

  if (filter_model) {
    url += '&model=' + encodeURIComponent(filter_model);
  }

  var filter_preorder_price = \$('input[name=\\'filter_preorder_price\\']').val();

  if (filter_preorder_price) {
    url += '&preorder_price=' + encodeURIComponent(filter_preorder_price);
  }

  var filter_deduction_type = \$('select[name=\\'filter_deduction_type\\']').val();

  if (filter_deduction_type) {
    url += '&deduction_type=' + encodeURIComponent(filter_deduction_type);
  }
  var filter_status = \$('select[name=\\'filter_status\\']').val();

  if (filter_status) {
    url += '&status=' + encodeURIComponent(filter_status);
  }

  ";
        // line 270
        if ((array_key_exists("sort", $context) && (isset($context["sort"]) ? $context["sort"] : null))) {
            // line 271
            echo "  url += '&sort=";
            echo (isset($context["sort"]) ? $context["sort"] : null);
            echo "';
  ";
        }
        // line 272
        echo " 

  ";
        // line 274
        if ((array_key_exists("order", $context) && (isset($context["order"]) ? $context["order"] : null))) {
            // line 275
            echo "  url += '&order=";
            echo (isset($context["order"]) ? $context["order"] : null);
            echo "';
  ";
        }
        // line 276
        echo " 
  ";
        // line 277
        if ((array_key_exists("page", $context) && (isset($context["page"]) ? $context["page"] : null))) {
            // line 278
            echo "  url += '&page=";
            echo (isset($context["page"]) ? $context["page"] : null);
            echo "';
  ";
        }
        // line 279
        echo " 
  location = url;


}

//--></script>
<script>
\$('input[name=\\'filter_name\\']').autocomplete({

\t'source': function(request, response) {
\t\t\$.ajax({
\t\t\turl: 'index.php?route=catalog/wk_preorder/autocomplete&user_token=";
        // line 291
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "&filter_name=' +  encodeURIComponent(request),
\t\t\tdataType: 'json',
\t\t\tsuccess: function(json) {
\t\t\t\tresponse(\$.map(json, function(item) {
\t\t\t\t\treturn {
\t\t\t\t\t\tlabel: item['name'],
\t\t\t\t\t\tvalue: item['product_id']
\t\t\t\t\t}
\t\t\t\t}));
\t\t\t}
\t\t});
\t},
\t'select': function(item) {
\t\t\$('input[name=\\'filter_name\\']').val(item['label']);
\t}
});

\$('input[name=\\'filter_model\\']').autocomplete({

\t'source': function(request, response) {
\t\t\$.ajax({
\t\t\turl: 'index.php?route=catalog/wk_preorder/autocomplete&user_token=";
        // line 312
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "&filter_model=' +  encodeURIComponent(request),
\t\t\tdataType: 'json',
\t\t\tsuccess: function(json) {
\t\t\t\tresponse(\$.map(json, function(item) {
\t\t\t\t\treturn {
\t\t\t\t\t\tlabel: item['model'],
\t\t\t\t\t\tvalue: item['product_id']
\t\t\t\t\t}
\t\t\t\t}));
\t\t\t}
\t\t});
\t},
\t'select': function(item) {
\t\t\$('input[name=\\'filter_model\\']').val(item['label']);
\t}
});
</script>
";
        // line 329
        echo (isset($context["footer"]) ? $context["footer"] : null);
        echo " 
";
    }

    public function getTemplateName()
    {
        return "catalog/wk_preorder_productlist.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  738 => 329,  718 => 312,  694 => 291,  680 => 279,  674 => 278,  672 => 277,  669 => 276,  663 => 275,  661 => 274,  657 => 272,  651 => 271,  649 => 270,  614 => 238,  606 => 233,  598 => 228,  593 => 226,  574 => 210,  570 => 209,  563 => 204,  557 => 202,  553 => 200,  549 => 199,  540 => 196,  533 => 192,  524 => 189,  520 => 188,  506 => 185,  501 => 183,  497 => 182,  493 => 181,  489 => 180,  484 => 178,  475 => 174,  471 => 173,  463 => 168,  459 => 166,  452 => 165,  449 => 164,  440 => 163,  436 => 162,  431 => 159,  424 => 158,  421 => 157,  412 => 156,  408 => 155,  402 => 151,  395 => 150,  392 => 149,  383 => 148,  379 => 147,  374 => 144,  367 => 143,  364 => 142,  355 => 141,  351 => 140,  346 => 137,  339 => 136,  336 => 135,  327 => 134,  323 => 133,  314 => 127,  304 => 120,  298 => 117,  290 => 114,  285 => 112,  270 => 105,  261 => 104,  254 => 100,  245 => 96,  240 => 94,  225 => 87,  216 => 86,  209 => 82,  200 => 78,  195 => 76,  184 => 68,  179 => 65,  173 => 63,  167 => 60,  164 => 59,  158 => 57,  152 => 54,  149 => 53,  143 => 51,  137 => 48,  134 => 47,  128 => 45,  122 => 42,  119 => 41,  113 => 39,  107 => 36,  104 => 35,  98 => 33,  92 => 30,  84 => 24,  74 => 23,  68 => 22,  63 => 20,  55 => 15,  47 => 12,  43 => 10,  35 => 8,  31 => 7,  23 => 2,  19 => 1,);
    }
}
/* {{ header }} */
/* {{ column_left }} */
/* <div id="content">*/
/*   <div class="page-header">*/
/*     <div class="container-fluid">*/
/*         <div class="pull-right">*/
/*         {% if (convert is defined and convert) %} */
/*         <a href="{{ convert }}" class="btn btn-primary" data-toggle="tooptip" title="{{ button_convert }}" ><i class="fa fa-refresh"></i>*/
/*           </a>*/
/*         {% endif %} */
/*       */
/*           <a href="{{ add }}" class="btn btn-primary" data-toggle="tooptip" title="{{ button_add }}" >*/
/*             <i class="fa fa-plus"></i>*/
/*           </a>*/
/*            <button type="button" id="delete" class="btn btn-danger" data-toggle="tooltip" title="{{ button_delete }}">*/
/*             <i class="fa fa-trash"></i>*/
/*           </button>*/
/*           */
/*         </div>*/
/*         <h1>{{ heading_title_productlist }}</h1>*/
/*         <ul class="breadcrumb">*/
/*           {% for breadcrumb in breadcrumbs %} */
/*           <li><a href="{{ breadcrumb['href'] }}">{{ breadcrumb['text'] }}</a></li>*/
/*           {% endfor %} */
/*         </ul>*/
/*     </div>*/
/*   </div>*/
/* */
/*   <div class="container-fluid">*/
/*     {% if (error_warning) %} */
/*       <div class="alert alert-danger">*/
/*         <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*         {{ error_warning }} */
/*       </div>*/
/*     {% endif %} */
/*     {% if (error_warning_2) %} */
/*       <div class="alert alert-danger">*/
/*         <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*         {{ error_warning_2 }} */
/*       </div>*/
/*     {% endif %} */
/*     {% if (success) %} */
/*       <div class="alert alert-success">*/
/*         <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*         {{ success }} */
/*       </div>*/
/*     {% endif %} */
/*     {% if (added_success is defined and added_success) %} */
/*       <div class="alert alert-success">*/
/*         <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*         {{ added_success }} */
/*       </div>*/
/*     {% endif %} */
/*     {% if (alreadyAdded is defined and alreadyAdded) %} */
/*       <div class="alert alert-warning">*/
/*         <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*         {{ alreadyAdded }} */
/*       </div>*/
/*     {% endif %} */
/*     {% if (failure is defined and failure) %} */
/*       <div class="alert alert-danger">*/
/*         <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*         {{ failure }} */
/*       </div>*/
/*     {% endif %} */
/*     <div class="panel panel-default">*/
/*       <div class="panel-heading">*/
/*         <h3 class="panel-title"><i class="fa fa-list"></i>{{ heading_title_productlist }}</h3>*/
/*       </div>*/
/*       <div class="panel-body">*/
/*         <div class="well">*/
/*           <div class="row">*/
/*             <div class="col-sm-4">*/
/*               <div class="form-group">*/
/*                 <label class="control-label">*/
/*                   {{ text_pname }} */
/*                 </label>*/
/*                 <input type="text" class="form-control" name="filter_name" value="{{ name }}" placeholder="{{ text_pname }}" />*/
/*               </div>*/
/*               <div class="form-group">*/
/*                 <label class="control-label">*/
/*                   {{ text_deduction_type }} */
/*                 </label>*/
/*                 <select name="filter_deduction_type" class="form-control">*/
/*                   <option value=""></option>*/
/*                   <option value="pc" {% if (deduction_type == 'pc') %} {{ "selected" }}{% endif %} >{{ text_percentage }}</option>*/
/*                   <option value="fp" {% if (deduction_type == 'fp') %} {{ "selected" }}{% endif %} >{{ text_fixed }}</option>*/
/*                 </select>*/
/*               </div>*/
/*             </div>*/
/*             <div class="col-sm-4">*/
/*               <div class="form-group">*/
/*                 <label class="control-label">*/
/*                   {{ text_model }} */
/*                 </label>*/
/*                 <input type="text" class="form-control date" name="filter_model" value="{{ model }}" placeholder="{{ text_model }}" />*/
/*               </div>*/
/*               <div class="form-group">*/
/*                 <label class="control-label">*/
/*                   {{ text_status }} */
/*                 </label>*/
/*                 <select class="form-control" name="filter_status">*/
/*                   <option value=""></option>*/
/*                   <option value="1" {% if (status is defined and status) %} {{ "selected" }}{% endif %} >{{ text_enabled }}</option>*/
/*                   <option value="0" {% if (status is defined and status == "0") %} {{ "selected" }}{% endif %} >{{ text_disabled }}</option>*/
/*                 </select>*/
/*               </div>*/
/*             </div>*/
/*             <div class="col-sm-4">*/
/*               <div class="form-group">*/
/*                 <label class="control-label">*/
/*                   {{ text_preorder_price }} */
/*                 </label>*/
/*                 <input type="text" class="form-control" name="filter_preorder_price" value="{{ preorder_price }}" placeholder="{{ text_preorder_price }}" />*/
/*               </div>*/
/*               <div class="btn-group pull-right">*/
/*                   <button class="btn btn-primary" type="button" onclick="filter();" data-toggle="tooltip" title="{{ button_filter }}" >*/
/*                     <i class="fa fa-filter"></i>*/
/*                   </button>*/
/*                   <a class="btn btn-danger" onclick="clearfilter();" data-toggle="tooltip" title="{{ button_clrfilter }}">*/
/*                     <i class="fa fa-eraser"></i>*/
/*                   </a>*/
/*               </div>*/
/*             </div>*/
/*           </div>*/
/*         </div>*/
/*         <form action="{{ delete }}" method="post" enctype="multipart/form-data" id="form" class="form-horizontal">*/
/*           <table class="table table-bordered table-hover">*/
/*             <thead>*/
/*               <tr>*/
/*               <td class="text-center" style="width:1px"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>*/
/*               <td class="text-left">*/
/*                 {% if (sort == 'pd.name') %} */
/*                   <a href="{{ sort_pname }}" class="{{ order | lower }}">{{ text_pname }}</a>*/
/*                 {% else %} */
/*                   <a href="{{ sort_pname }}">{{ text_pname }}</a>*/
/*                 {% endif %} */
/*               </td>*/
/*               <td class="text-left">*/
/*                 {% if (sort == 'p.model') %} */
/*                   <a href="{{ sort_pmodel }}" class="{{ order | lower }}">{{ text_model }}</a>*/
/*                 {% else %} */
/*                   <a href="{{ sort_pmodel }}">{{ text_model }}</a>*/
/*                 {% endif %} */
/*               </td>*/
/*               <td class="text-left">*/
/*                 {% if (sort == 'pp.initial_price') %} */
/*                   <a href="{{ sort_wk_fixed_price }}" class="{{ order | lower }}">{{ text_preorder_price }}</a>*/
/*                 {% else %} */
/*                   <a href="{{ sort_wk_fixed_price }}">{{ text_preorder_price }}</a>*/
/*                 {% endif %} */
/*               </td>*/
/* */
/*               <td class="text-left">*/
/*                 {% if (sort == 'pp.deduction_type') %} */
/*                   <a href="{{ sort_deduction_type }}" class="{{ order | lower }}">{{ text_deduction_type }}</a>*/
/*                 {% else %} */
/*                   <a href="{{ sort_deduction_type }}">{{ text_deduction_type }}</a>*/
/*                 {% endif %} */
/*               </td>*/
/*               <td class="text-center">*/
/*                 {% if (sort == 'pp.status') %} */
/*                   <a href="{{ sort_pp_status }}" class="{{ order | lower }}">{{ text_status }}</a>*/
/*                 {% else %} */
/*                   <a href="{{ sort_pp_status }}">{{ text_status }}</a>*/
/*                 {% endif %} */
/*               </td>*/
/*                 <td class="text-center">{{ text_action }}</td>*/
/*               </tr>*/
/*             </thead>*/
/*             <tbody>*/
/* */
/*               {% if (products is defined) %} */
/*               {% for product in products %} */
/*               <tr>*/
/* */
/*                 <td class="text-center">*/
/*                   <input type="checkbox" name="selected[]" value="{{ product['id'] }}" />*/
/*                 </td>*/
/*                 <td class="text-left">{{ product['product_name'] }}</td>*/
/*                 <td class="text-left">{{ product['product_model'] }}</td>*/
/*                 <td class="text-left">{{ product['preorder_price'] }}</td>*/
/*                 <td class="text-left">{{ product['deduction_type'] }}</td>*/
/*                 <td class="text-center">*/
/*                   {% if (product['status']) %} {{ text_enabled }} {% else %} {{ text_disabled }} {% endif %} */
/*                 </td>*/
/*                 <td class="text-center">*/
/*                   {% if (product['status']) %} */
/*                     <a href="{{ product['edit'] }}" data-toggle="tooltip" title="{{ text_edit }}" class="btn btn-warning" >*/
/*                       <i class="fa fa-edit"></i>*/
/*                     </a>*/
/*                   {% else %} */
/*                     <button type="button" class="btn btn-warning" disabled >*/
/*                       <i class="fa fa-edit"></i>*/
/*                     </button>*/
/*                   {% endif %} */
/*                 </td>*/
/*               </tr>*/
/*               {% endfor %} */
/*               {% else %} */
/*               <tr>*/
/*                 <td class="text-center" colspan="12">{{ "no records founds" }}</td>*/
/*               </tr>*/
/*               {% endif %} */
/*             </tbody>*/
/*           </table>*/
/*       </form>*/
/*       <div class="row">*/
/*         <div class="col-sm-6 text-left">{{ pagination }}</div>*/
/*         <div class="col-sm-6 text-right">{{ results }}</div>*/
/*       </div>*/
/*   </div>*/
/*   </div>*/
/* </div>*/
/* </div>*/
/* <script type="text/javascript"><!--*/
/* $('body').on('click','#delete',function(){*/
/*   var flag = false;*/
/*   $('input[type=\'checkbox\']').each(function(){*/
/*     if ($(this).is(':checked')) {*/
/*       flag = true;*/
/*     }*/
/*   });*/
/*   console.log(flag);*/
/*   if (flag) {*/
/*       confirm('{{ text_confirmation }}') ? $('form').submit() : false;*/
/*   } else {*/
/*      alert('{{ error_select }}');*/
/*   }*/
/* });*/
/* */
/* function clearfilter() {*/
/*   url = 'index.php?route=catalog/wk_preorder/preorder_productlist&user_token={{ user_token }}';*/
/*   location = url;*/
/* }*/
/* */
/* function filter() {*/
/*   url = 'index.php?route=catalog/wk_preorder/preorder_productlist&user_token={{ user_token }}';*/
/* */
/*   var filter_name = $('input[name=\'filter_name\']').val();*/
/* */
/* */
/*   if (filter_name) {*/
/*     url += '&name=' + encodeURIComponent(filter_name);*/
/*   }*/
/* */
/*   var filter_model = $('input[name=\'filter_model\']').val();*/
/* */
/*   if (filter_model) {*/
/*     url += '&model=' + encodeURIComponent(filter_model);*/
/*   }*/
/* */
/*   var filter_preorder_price = $('input[name=\'filter_preorder_price\']').val();*/
/* */
/*   if (filter_preorder_price) {*/
/*     url += '&preorder_price=' + encodeURIComponent(filter_preorder_price);*/
/*   }*/
/* */
/*   var filter_deduction_type = $('select[name=\'filter_deduction_type\']').val();*/
/* */
/*   if (filter_deduction_type) {*/
/*     url += '&deduction_type=' + encodeURIComponent(filter_deduction_type);*/
/*   }*/
/*   var filter_status = $('select[name=\'filter_status\']').val();*/
/* */
/*   if (filter_status) {*/
/*     url += '&status=' + encodeURIComponent(filter_status);*/
/*   }*/
/* */
/*   {% if (sort is defined and sort) %}*/
/*   url += '&sort={{ sort }}';*/
/*   {% endif %} */
/* */
/*   {% if (order is defined and order) %}*/
/*   url += '&order={{ order }}';*/
/*   {% endif %} */
/*   {% if (page is defined and page) %}*/
/*   url += '&page={{ page }}';*/
/*   {% endif %} */
/*   location = url;*/
/* */
/* */
/* }*/
/* */
/* //--></script>*/
/* <script>*/
/* $('input[name=\'filter_name\']').autocomplete({*/
/* */
/* 	'source': function(request, response) {*/
/* 		$.ajax({*/
/* 			url: 'index.php?route=catalog/wk_preorder/autocomplete&user_token={{ user_token }}&filter_name=' +  encodeURIComponent(request),*/
/* 			dataType: 'json',*/
/* 			success: function(json) {*/
/* 				response($.map(json, function(item) {*/
/* 					return {*/
/* 						label: item['name'],*/
/* 						value: item['product_id']*/
/* 					}*/
/* 				}));*/
/* 			}*/
/* 		});*/
/* 	},*/
/* 	'select': function(item) {*/
/* 		$('input[name=\'filter_name\']').val(item['label']);*/
/* 	}*/
/* });*/
/* */
/* $('input[name=\'filter_model\']').autocomplete({*/
/* */
/* 	'source': function(request, response) {*/
/* 		$.ajax({*/
/* 			url: 'index.php?route=catalog/wk_preorder/autocomplete&user_token={{ user_token }}&filter_model=' +  encodeURIComponent(request),*/
/* 			dataType: 'json',*/
/* 			success: function(json) {*/
/* 				response($.map(json, function(item) {*/
/* 					return {*/
/* 						label: item['model'],*/
/* 						value: item['product_id']*/
/* 					}*/
/* 				}));*/
/* 			}*/
/* 		});*/
/* 	},*/
/* 	'select': function(item) {*/
/* 		$('input[name=\'filter_model\']').val(item['label']);*/
/* 	}*/
/* });*/
/* </script>*/
/* {{ footer }} */
/* */
