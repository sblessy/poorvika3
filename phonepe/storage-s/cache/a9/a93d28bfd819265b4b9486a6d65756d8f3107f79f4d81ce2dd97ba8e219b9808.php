<?php

/* so-destino/template/header/header1.twig */
class __TwigTemplate_5394d6505237f0f9bfcf91a04d932089634d794268ff8b7855e0656e25bbda31 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $context["hidden_headercenter"] = ((($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "toppanel_type"), "method") == "2")) ? ("hidden-compact") : (""));
        // line 3
        $context["hidden_headerbottom"] = ((($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "toppanel_type"), "method") == "1")) ? ("hidden-compact") : (""));
        // line 4
        echo "
<header id=\"header\" class=\" variant typeheader-";
        // line 5
        echo (((isset($context["typeheader"]) ? $context["typeheader"] : null)) ? ((isset($context["typeheader"]) ? $context["typeheader"] : null)) : ("1"));
        echo "\">

\t<!-- HEADER TOP -->
\t<div class=\"header-top compact-hidden\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"header-top-left  col-lg-6 col-md-5 col-sm-6 col-xs-6\">
\t\t\t\t\t<!-- LANGUAGE CURENCY -->
\t\t\t\t\t";
        // line 13
        if ($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "lang_status"), "method")) {
            // line 14
            echo "\t\t\t\t\t<ul class=\"top-link list-inline lang-curr\">
\t\t\t\t\t\t";
            // line 15
            if ((isset($context["currency"]) ? $context["currency"] : null)) {
                echo "<li class=\"currency\"> ";
                echo (isset($context["currency"]) ? $context["currency"] : null);
                echo "  </li> ";
            }
            // line 16
            echo "\t\t\t\t\t\t";
            if ((isset($context["language"]) ? $context["language"] : null)) {
                echo " <li class=\"language\">";
                echo (isset($context["language"]) ? $context["language"] : null);
                echo " </li>\t";
            }
            echo "\t\t\t
\t\t\t\t\t</ul>\t\t\t\t
\t\t\t\t\t";
        }
        // line 18
        echo " 
\t\t\t\t</div>
\t\t\t\t<div class=\"header-top-right collapsed-block col-lg-6 col-md-7 col-sm-6 col-xs-6\">
\t\t\t\t\t
\t\t\t\t\t<ul class=\"top-link list-inline\">

\t\t\t\t\t\t";
        // line 24
        if ($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "welcome_message_status"), "method")) {
            // line 25
            echo "\t\t\t\t\t\t<li class=\"hidden-sm hidden-xs welcome-msg\">
\t\t\t\t\t\t\t";
            // line 26
            if ( !twig_test_empty($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "welcome_message"), "method"))) {
                // line 27
                echo "\t\t\t\t\t\t\t\t";
                echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "decode_entities", array(0 => $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "welcome_message"), "method")), "method");
                echo "
\t\t\t\t\t\t\t";
            }
            // line 29
            echo "\t\t\t\t\t\t</li>
\t\t\t\t\t\t";
        }
        // line 31
        echo "

\t\t\t\t\t\t";
        // line 33
        if ((isset($context["logged"]) ? $context["logged"] : null)) {
            // line 34
            echo "\t\t\t\t\t\t\t<li><a href=\"";
            echo (isset($context["account"]) ? $context["account"] : null);
            echo "\">";
            echo (isset($context["text_account"]) ? $context["text_account"] : null);
            echo "</a></li>
\t\t\t\t            <li><a href=\"";
            // line 35
            echo (isset($context["order"]) ? $context["order"] : null);
            echo "\">";
            echo (isset($context["text_order"]) ? $context["text_order"] : null);
            echo "</a></li>
\t\t\t\t            <li><a href=\"";
            // line 36
            echo (isset($context["transaction"]) ? $context["transaction"] : null);
            echo "\">";
            echo (isset($context["text_transaction"]) ? $context["text_transaction"] : null);
            echo "</a></li>
\t\t\t\t            <li><a href=\"";
            // line 37
            echo (isset($context["download"]) ? $context["download"] : null);
            echo "\">";
            echo (isset($context["text_download"]) ? $context["text_download"] : null);
            echo "</a></li>
\t\t\t\t            <li><a href=\"";
            // line 38
            echo (isset($context["logout"]) ? $context["logout"] : null);
            echo "\"><i class=\"fa fa-sign-out\" aria-hidden=\"true\"></i> ";
            echo (isset($context["text_logout"]) ? $context["text_logout"] : null);
            echo "</a></li>
\t\t\t            ";
        } else {
            // line 40
            echo "\t\t\t\t            <li><a href=\"";
            echo (isset($context["register"]) ? $context["register"] : null);
            echo "\">";
            echo (isset($context["text_register"]) ? $context["text_register"] : null);
            echo "</a></li>
\t\t\t\t            <li><a href=\"";
            // line 41
            echo (isset($context["login"]) ? $context["login"] : null);
            echo "\">";
            echo (isset($context["text_login"]) ? $context["text_login"] : null);
            echo "</a></li>
\t\t\t            ";
        }
        // line 42
        echo "\t

\t\t\t\t\t\t<!-- WISHLIST  -->
\t\t\t\t\t\t";
        // line 45
        if ((isset($context["wishlist_status"]) ? $context["wishlist_status"] : null)) {
            // line 46
            echo "\t\t\t\t\t\t\t<li><a href=\"";
            echo (isset($context["wishlist"]) ? $context["wishlist"] : null);
            echo "\"  title=\"";
            echo (isset($context["text_wishlist"]) ? $context["text_wishlist"] : null);
            echo "\">";
            echo (isset($context["text_wishlist"]) ? $context["text_wishlist"] : null);
            echo "</a></li>
\t\t\t\t\t\t";
        }
        // line 47
        echo "\t
\t\t\t\t\t\t<!-- checkout -->
\t\t\t\t\t\t";
        // line 49
        if ($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "checkout_status"), "method")) {
            // line 50
            echo "\t\t\t\t\t\t\t<li class=\"checkout hidden-xs\"><a href=\"";
            echo (isset($context["checkout"]) ? $context["checkout"] : null);
            echo " \" class=\"btn-link\" title=\"";
            echo (isset($context["text_checkout"]) ? $context["text_checkout"] : null);
            echo " \"><span >";
            echo (isset($context["text_checkout"]) ? $context["text_checkout"] : null);
            echo " </span></a></li>
\t\t\t\t\t\t";
        }
        // line 51
        echo " \t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t</ul>
\t\t\t\t
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>

\t<!-- HEADER CENTER -->
\t<div class=\"header-center compact-hidden\">
\t\t<div class=\"container\">
\t\t\t<div class=\"row\">
\t\t\t\t<!-- LOGO -->
\t\t\t\t<div class=\"navbar-logo col-lg-3 col-md-3 col-sm-12 col-xs-12\">
\t\t\t\t\t<div class=\"logo\">
\t\t\t\t   \t\t";
        // line 66
        echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_logo", array(), "method");
        echo "
\t\t\t\t   \t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"header-center-right col-lg-9 col-md-9 col-sm-12 col-xs-12\">\t
\t\t\t\t\t
\t\t\t\t\t<div class=\"search-header-w\">
\t\t\t\t\t\t<div class=\"icon-search hidden-lg hidden-md hidden-sm\"><i class=\"fa fa-search\"></i></div>\t\t\t\t
\t\t\t\t\t\t";
        // line 73
        echo (isset($context["search_block"]) ? $context["search_block"] : null);
        echo "

\t\t\t\t\t</div>
\t\t\t\t\t";
        // line 76
        if (($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "phone_status"), "method") && $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "contact_number"), "method"))) {
            // line 77
            echo "\t\t\t\t\t<div class=\"telephone hidden-xs hidden-sm hidden-md\" >
\t\t\t\t\t\t

\t\t\t\t\t\t";
            // line 80
            echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "decode_entities", array(0 => $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "contact_number"), "method")), "method");
            echo "
\t\t\t\t\t</div>
\t\t\t\t\t";
        }
        // line 83
        echo "
\t\t\t\t\t<div class=\"shopping_cart\">\t\t\t\t\t\t\t
\t\t\t\t\t \t";
        // line 85
        echo (isset($context["cart"]) ? $context["cart"] : null);
        echo "
\t\t\t\t\t</div>
\t\t\t\t
\t\t\t\t</div>

\t\t\t</div>
\t\t</div>
\t</div>
\t<!-- HEADER BOTTOM -->
\t<div class=\"header-bottom\">
\t\t<div class=\"container\">
\t\t\t<div class=\"header-bottom-inner\">
\t\t\t\t<!-- Main menu -->\t\t\t\t
\t\t\t   ";
        // line 98
        echo (isset($context["content_menu1"]) ? $context["content_menu1"] : null);
        echo "
\t\t\t    <!-- //end Navbar -->\t\t\t\t\t\t\t\t\t
\t\t\t</div>
\t\t</div>
\t</div>

</header>";
    }

    public function getTemplateName()
    {
        return "so-destino/template/header/header1.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  235 => 98,  219 => 85,  215 => 83,  209 => 80,  204 => 77,  202 => 76,  196 => 73,  186 => 66,  169 => 51,  159 => 50,  157 => 49,  153 => 47,  143 => 46,  141 => 45,  136 => 42,  129 => 41,  122 => 40,  115 => 38,  109 => 37,  103 => 36,  97 => 35,  90 => 34,  88 => 33,  84 => 31,  80 => 29,  74 => 27,  72 => 26,  69 => 25,  67 => 24,  59 => 18,  48 => 16,  42 => 15,  39 => 14,  37 => 13,  26 => 5,  23 => 4,  21 => 3,  19 => 2,);
    }
}
/* {#=====Get variable : Config Select Block on header=====#}*/
/* {% set hidden_headercenter = soconfig.get_settings('toppanel_type') =='2'? 'hidden-compact' : '' %}*/
/* {% set hidden_headerbottom = soconfig.get_settings('toppanel_type') =='1'? 'hidden-compact' : '' %}*/
/* */
/* <header id="header" class=" variant typeheader-{{ typeheader ? typeheader : '1'}}">*/
/* */
/* 	<!-- HEADER TOP -->*/
/* 	<div class="header-top compact-hidden">*/
/* 		<div class="container">*/
/* 			<div class="row">*/
/* 				<div class="header-top-left  col-lg-6 col-md-5 col-sm-6 col-xs-6">*/
/* 					<!-- LANGUAGE CURENCY -->*/
/* 					{% if soconfig.get_settings('lang_status') %}*/
/* 					<ul class="top-link list-inline lang-curr">*/
/* 						{% if currency %}<li class="currency"> {{ currency }}  </li> {% endif %}*/
/* 						{% if language %} <li class="language">{{ language }} </li>	{% endif %}			*/
/* 					</ul>				*/
/* 					{% endif %} */
/* 				</div>*/
/* 				<div class="header-top-right collapsed-block col-lg-6 col-md-7 col-sm-6 col-xs-6">*/
/* 					*/
/* 					<ul class="top-link list-inline">*/
/* */
/* 						{% if soconfig.get_settings('welcome_message_status') %}*/
/* 						<li class="hidden-sm hidden-xs welcome-msg">*/
/* 							{% if soconfig.get_settings('welcome_message') is not empty %}*/
/* 								{{ soconfig.decode_entities( soconfig.get_settings('welcome_message') ) }}*/
/* 							{% endif %}*/
/* 						</li>*/
/* 						{% endif %}*/
/* */
/* */
/* 						{% if logged %}*/
/* 							<li><a href="{{ account }}">{{ text_account }}</a></li>*/
/* 				            <li><a href="{{ order }}">{{ text_order }}</a></li>*/
/* 				            <li><a href="{{ transaction }}">{{ text_transaction }}</a></li>*/
/* 				            <li><a href="{{ download }}">{{ text_download }}</a></li>*/
/* 				            <li><a href="{{ logout }}"><i class="fa fa-sign-out" aria-hidden="true"></i> {{ text_logout }}</a></li>*/
/* 			            {% else %}*/
/* 				            <li><a href="{{ register }}">{{ text_register }}</a></li>*/
/* 				            <li><a href="{{ login }}">{{ text_login }}</a></li>*/
/* 			            {% endif %}	*/
/* */
/* 						<!-- WISHLIST  -->*/
/* 						{% if wishlist_status %}*/
/* 							<li><a href="{{ wishlist }}"  title="{{ text_wishlist }}">{{ text_wishlist }}</a></li>*/
/* 						{% endif %}	*/
/* 						<!-- checkout -->*/
/* 						{% if soconfig.get_settings('checkout_status') %}*/
/* 							<li class="checkout hidden-xs"><a href="{{ checkout }} " class="btn-link" title="{{ text_checkout }} "><span >{{ text_checkout }} </span></a></li>*/
/* 						{% endif %} 											*/
/* 					</ul>*/
/* 				*/
/* 				</div>*/
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/* */
/* 	<!-- HEADER CENTER -->*/
/* 	<div class="header-center compact-hidden">*/
/* 		<div class="container">*/
/* 			<div class="row">*/
/* 				<!-- LOGO -->*/
/* 				<div class="navbar-logo col-lg-3 col-md-3 col-sm-12 col-xs-12">*/
/* 					<div class="logo">*/
/* 				   		{{soconfig.get_logo()}}*/
/* 				   	</div>*/
/* 				</div>*/
/* 				<div class="header-center-right col-lg-9 col-md-9 col-sm-12 col-xs-12">	*/
/* 					*/
/* 					<div class="search-header-w">*/
/* 						<div class="icon-search hidden-lg hidden-md hidden-sm"><i class="fa fa-search"></i></div>				*/
/* 						{{ search_block }}*/
/* */
/* 					</div>*/
/* 					{% if soconfig.get_settings('phone_status') and soconfig.get_settings('contact_number') %}*/
/* 					<div class="telephone hidden-xs hidden-sm hidden-md" >*/
/* 						*/
/* */
/* 						{{ soconfig.decode_entities( soconfig.get_settings('contact_number') ) }}*/
/* 					</div>*/
/* 					{% endif %}*/
/* */
/* 					<div class="shopping_cart">							*/
/* 					 	{{ cart }}*/
/* 					</div>*/
/* 				*/
/* 				</div>*/
/* */
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/* 	<!-- HEADER BOTTOM -->*/
/* 	<div class="header-bottom">*/
/* 		<div class="container">*/
/* 			<div class="header-bottom-inner">*/
/* 				<!-- Main menu -->				*/
/* 			   {{ content_menu1 }}*/
/* 			    <!-- //end Navbar -->									*/
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/* */
/* </header>*/
