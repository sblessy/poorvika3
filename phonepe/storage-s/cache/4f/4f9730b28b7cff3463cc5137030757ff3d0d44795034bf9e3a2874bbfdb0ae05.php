<?php

/* catalog/wk_preorder_enquiry.twig */
class __TwigTemplate_1d53dfd6909e8258c3244fe55a62e4ccd62c3a2f953ce77493538ef1bcc65dd3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo " 
";
        // line 2
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo " 
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\">
      <a href=\"";
        // line 7
        echo (isset($context["cancel"]) ? $context["cancel"] : null);
        echo "\" class=\"btn btn-default\" title=\"";
        echo (isset($context["button_cancel"]) ? $context["button_cancel"] : null);
        echo "\">
          <i class=\"fa fa-reply\"></i></a>
      </div>
      <h1>";
        // line 10
        echo (isset($context["heading_title_enquiry"]) ? $context["heading_title_enquiry"] : null);
        echo "</h1>
      <ul class=\"breadcrumb\">
        ";
        // line 12
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            echo " 
          <li><a href=\"";
            // line 13
            echo $this->getAttribute($context["breadcrumb"], "href", array(), "array");
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array(), "array");
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 14
        echo " 
      </ul>
    </div>
  </div>
  <div class=\"container-fluid\">
    ";
        // line 19
        if ((isset($context["error_warning"]) ? $context["error_warning"] : null)) {
            echo " 
      <div class=\"alert alert-danger\">";
            // line 20
            echo (isset($context["error_warning"]) ? $context["error_warning"] : null);
            echo "</div>
    ";
        }
        // line 21
        echo " 
    ";
        // line 22
        if ((isset($context["success"]) ? $context["success"] : null)) {
            echo " 
      <div class=\"alert alert-success\">";
            // line 23
            echo (isset($context["success"]) ? $context["success"] : null);
            echo "</div>
    ";
        }
        // line 24
        echo " 
    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <h3 class=\"panel-title\">";
        // line 27
        echo (isset($context["heading_title_enquiry"]) ? $context["heading_title_enquiry"] : null);
        echo "</h3>
      </div>
      <div class=\"panel-body\">
        <div class=\"jumbotron\">
            <h4><b>";
        // line 31
        echo (isset($context["text_subject"]) ? $context["text_subject"] : null);
        echo ": ";
        echo $this->getAttribute((isset($context["enquiry"]) ? $context["enquiry"] : null), "subject", array(), "array");
        echo "</b></h4>
            <p>";
        // line 32
        echo (isset($context["text_query"]) ? $context["text_query"] : null);
        echo ": ";
        echo $this->getAttribute((isset($context["enquiry"]) ? $context["enquiry"] : null), "query", array(), "array");
        echo "</p>
            <i>";
        // line 33
        echo (isset($context["text_current_status"]) ? $context["text_current_status"] : null);
        echo ": ";
        echo (($this->getAttribute((isset($context["enquiry"]) ? $context["enquiry"] : null), "status", array(), "array")) ? ((isset($context["text_open"]) ? $context["text_open"] : null)) : ((isset($context["text_resolved"]) ? $context["text_resolved"] : null)));
        echo "</i>
        </div>
        ";
        // line 35
        if ((array_key_exists("threads", $context) && (isset($context["threads"]) ? $context["threads"] : null))) {
            echo " 
            ";
            // line 36
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["threads"]) ? $context["threads"] : null));
            foreach ($context['_seq'] as $context["index"] => $context["thread"]) {
                echo " 
                <div class=\"col-sm-12\">
                    ";
                // line 38
                if ($this->getAttribute($context["thread"], "posted_by", array(), "array")) {
                    echo " 
                        <div class=\"col-sm-6 alert alert-warning pull-left\" style=\"box-sizing:border-box\">
                            <button class=\"close remove-thread\" data-toggle=\"tooltip\" title=\"Click to delete this thread\" data-thread-id=\"";
                    // line 40
                    echo $this->getAttribute($context["thread"], "thread_id", array(), "array");
                    echo "\" >&times;</button>
                            <b>Posted by: ";
                    // line 41
                    echo $this->getAttribute((isset($context["enquiry"]) ? $context["enquiry"] : null), "customer_name", array(), "array");
                    echo "</b></br>
                            <i>";
                    // line 42
                    echo $this->getAttribute($context["thread"], "date_added", array(), "array");
                    echo "</i>
                            <p>";
                    // line 43
                    echo $this->getAttribute($context["thread"], "query", array(), "array");
                    echo "</p>
                        </div>
                    ";
                } else {
                    // line 45
                    echo " 
                        <div class=\"col-sm-6 alert alert-info pull-right\" style=\"box-sizing:border-box\">
                            <button class=\"close remove-thread\" data-toggle=\"tooltip\" title=\"Click to delete this thread\" data-thread-id=\"";
                    // line 47
                    echo $this->getAttribute($context["thread"], "thread_id", array(), "array");
                    echo "\" >&times;</button>
                            <b>Posted by: Admin</b></br>
                            <i>";
                    // line 49
                    echo $this->getAttribute($context["thread"], "date_added", array(), "array");
                    echo "</i>
                            <p>";
                    // line 50
                    echo $this->getAttribute($context["thread"], "query", array(), "array");
                    echo "</p>
                        </div>
                    ";
                }
                // line 52
                echo " 
                </div>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['index'], $context['thread'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 54
            echo " 
        ";
        } else {
            // line 55
            echo " 
            <div class=\"alert alert-warning text-center\">
                ";
            // line 57
            echo (isset($context["text_no_thread"]) ? $context["text_no_thread"] : null);
            echo " 
            </div>
        ";
        }
        // line 59
        echo " 
        <form class=\"form-horizontal\" id=\"reply-form\" >
            <input type=\"hidden\" name=\"enquiry_id\" value=\"";
        // line 61
        echo $this->getAttribute((isset($context["enquiry"]) ? $context["enquiry"] : null), "enquiry_id", array(), "array");
        echo "\" />
            <div class=\"form-group required\">
                <label class=\"col-sm-3 control-label\">
                    ";
        // line 64
        echo (isset($context["text_query"]) ? $context["text_query"] : null);
        echo " 
                </label>
                <div class=\"col-sm-9\">
                    <textarea class=\"form-control\" name=\"query\" colspan=\"10\"></textarea>
                </div>
            </div>
            <div class=\"form-group\">
                <label class=\"col-sm-3 control-label\">
                    ";
        // line 72
        echo (isset($context["text_current_status"]) ? $context["text_current_status"] : null);
        echo " 
                </label>
                <div class=\"col-sm-9\">
                    <select class=\"form-control\" name=\"status\">
                        <option value=\"1\">";
        // line 76
        echo (isset($context["text_open"]) ? $context["text_open"] : null);
        echo "</option>
                        <option value=\"0\">";
        // line 77
        echo (isset($context["text_resolved"]) ? $context["text_resolved"] : null);
        echo "</option>
                    </select>
                </div>
            </div>
            <button class=\"btn btn-primary query-submit pull-right\" type=\"button\">Submit</button>
        </form>
      </div>
    </div>
  </div>
</div>
<script>
    \$('.query-submit').on('click', function() {
        form_data = \$('#reply-form').serializeArray();
        \$.ajax({
            url: 'index.php?route=catalog/wk_preorder/addThread&user_token=";
        // line 91
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "',
            data: form_data,
            method: 'post',
            dataType: 'json',
            beforeSend: function(){
                \$('.preorder-button').button('loading');
                \$('.alert-success, .alert-danger').remove();
            },
            complete: function() {
                \$('.preorder-button').button('reset');
            },
            success: function(response) {
                if(response.success) {
                    \$('.query-submit').before('<div class=\"alert alert-success\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button> '+response.msg+'</div>');
                    setTimeout(location.reload(), 2000);
                } else {
                    \$('.query-submit').before('<div class=\"alert alert-danger\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button> '+response.msg+'</div>');
                }
            }
        })
    })

    \$('.remove-thread').on('click', function(){
        thread_id = \$(this).data('thread-id');
        \$this = \$(this);
        confirmation = confirm('";
        // line 116
        echo (isset($context["text_confirmation"]) ? $context["text_confirmation"] : null);
        echo "');
        if(confirmation) {
            \$.ajax({
                url: 'index.php?route=catalog/wk_preorder/removeThread&user_token=";
        // line 119
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "',
                data: {
                    thread_id: thread_id
                },
                method: 'post',
                dataType: 'json',
                beforeSend: function(){
                    \$this.html('<i class=\"fa fa-circle-o-notch fa-spin\"></i>');
                },
                complete: function() {
                    \$this.html('&times;');
                },
                success: function(response) {
                    if(response.success) {
                        \$this.parent().parent().after('<div class=\"alert alert-success\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button> '+response.msg+'</div>');
                        setTimeout(location.reload(), 2000);
                    } else {
                        \$this.parent().parent().after('<div class=\"alert alert-danger\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button> '+response.msg+'</div>');
                    }
                }
            })
        }
    })
</script>
";
        // line 143
        echo (isset($context["footer"]) ? $context["footer"] : null);
        echo " 
";
    }

    public function getTemplateName()
    {
        return "catalog/wk_preorder_enquiry.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  304 => 143,  277 => 119,  271 => 116,  243 => 91,  226 => 77,  222 => 76,  215 => 72,  204 => 64,  198 => 61,  194 => 59,  188 => 57,  184 => 55,  180 => 54,  172 => 52,  166 => 50,  162 => 49,  157 => 47,  153 => 45,  147 => 43,  143 => 42,  139 => 41,  135 => 40,  130 => 38,  123 => 36,  119 => 35,  112 => 33,  106 => 32,  100 => 31,  93 => 27,  88 => 24,  83 => 23,  79 => 22,  76 => 21,  71 => 20,  67 => 19,  60 => 14,  50 => 13,  44 => 12,  39 => 10,  31 => 7,  23 => 2,  19 => 1,);
    }
}
/* {{ header }} */
/* {{ column_left }} */
/* <div id="content">*/
/*   <div class="page-header">*/
/*     <div class="container-fluid">*/
/*       <div class="pull-right">*/
/*       <a href="{{ cancel }}" class="btn btn-default" title="{{ button_cancel }}">*/
/*           <i class="fa fa-reply"></i></a>*/
/*       </div>*/
/*       <h1>{{ heading_title_enquiry }}</h1>*/
/*       <ul class="breadcrumb">*/
/*         {% for breadcrumb in breadcrumbs %} */
/*           <li><a href="{{ breadcrumb['href'] }}">{{ breadcrumb['text'] }}</a></li>*/
/*         {% endfor %} */
/*       </ul>*/
/*     </div>*/
/*   </div>*/
/*   <div class="container-fluid">*/
/*     {% if (error_warning) %} */
/*       <div class="alert alert-danger">{{ error_warning }}</div>*/
/*     {% endif %} */
/*     {% if (success) %} */
/*       <div class="alert alert-success">{{ success }}</div>*/
/*     {% endif %} */
/*     <div class="panel panel-default">*/
/*       <div class="panel-heading">*/
/*         <h3 class="panel-title">{{ heading_title_enquiry }}</h3>*/
/*       </div>*/
/*       <div class="panel-body">*/
/*         <div class="jumbotron">*/
/*             <h4><b>{{ text_subject }}: {{ enquiry['subject'] }}</b></h4>*/
/*             <p>{{ text_query }}: {{ enquiry['query'] }}</p>*/
/*             <i>{{ text_current_status }}: {{ enquiry['status'] ? text_open : text_resolved }}</i>*/
/*         </div>*/
/*         {% if (threads is defined and threads) %} */
/*             {% for index,thread in threads %} */
/*                 <div class="col-sm-12">*/
/*                     {% if (thread['posted_by']) %} */
/*                         <div class="col-sm-6 alert alert-warning pull-left" style="box-sizing:border-box">*/
/*                             <button class="close remove-thread" data-toggle="tooltip" title="Click to delete this thread" data-thread-id="{{ thread['thread_id'] }}" >&times;</button>*/
/*                             <b>Posted by: {{ enquiry['customer_name'] }}</b></br>*/
/*                             <i>{{ thread['date_added'] }}</i>*/
/*                             <p>{{ thread['query'] }}</p>*/
/*                         </div>*/
/*                     {% else %} */
/*                         <div class="col-sm-6 alert alert-info pull-right" style="box-sizing:border-box">*/
/*                             <button class="close remove-thread" data-toggle="tooltip" title="Click to delete this thread" data-thread-id="{{ thread['thread_id'] }}" >&times;</button>*/
/*                             <b>Posted by: Admin</b></br>*/
/*                             <i>{{ thread['date_added'] }}</i>*/
/*                             <p>{{ thread['query'] }}</p>*/
/*                         </div>*/
/*                     {% endif %} */
/*                 </div>*/
/*             {% endfor %} */
/*         {% else %} */
/*             <div class="alert alert-warning text-center">*/
/*                 {{ text_no_thread }} */
/*             </div>*/
/*         {% endif %} */
/*         <form class="form-horizontal" id="reply-form" >*/
/*             <input type="hidden" name="enquiry_id" value="{{ enquiry['enquiry_id'] }}" />*/
/*             <div class="form-group required">*/
/*                 <label class="col-sm-3 control-label">*/
/*                     {{ text_query }} */
/*                 </label>*/
/*                 <div class="col-sm-9">*/
/*                     <textarea class="form-control" name="query" colspan="10"></textarea>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="form-group">*/
/*                 <label class="col-sm-3 control-label">*/
/*                     {{ text_current_status }} */
/*                 </label>*/
/*                 <div class="col-sm-9">*/
/*                     <select class="form-control" name="status">*/
/*                         <option value="1">{{ text_open }}</option>*/
/*                         <option value="0">{{ text_resolved }}</option>*/
/*                     </select>*/
/*                 </div>*/
/*             </div>*/
/*             <button class="btn btn-primary query-submit pull-right" type="button">Submit</button>*/
/*         </form>*/
/*       </div>*/
/*     </div>*/
/*   </div>*/
/* </div>*/
/* <script>*/
/*     $('.query-submit').on('click', function() {*/
/*         form_data = $('#reply-form').serializeArray();*/
/*         $.ajax({*/
/*             url: 'index.php?route=catalog/wk_preorder/addThread&user_token={{ user_token }}',*/
/*             data: form_data,*/
/*             method: 'post',*/
/*             dataType: 'json',*/
/*             beforeSend: function(){*/
/*                 $('.preorder-button').button('loading');*/
/*                 $('.alert-success, .alert-danger').remove();*/
/*             },*/
/*             complete: function() {*/
/*                 $('.preorder-button').button('reset');*/
/*             },*/
/*             success: function(response) {*/
/*                 if(response.success) {*/
/*                     $('.query-submit').before('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button> '+response.msg+'</div>');*/
/*                     setTimeout(location.reload(), 2000);*/
/*                 } else {*/
/*                     $('.query-submit').before('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button> '+response.msg+'</div>');*/
/*                 }*/
/*             }*/
/*         })*/
/*     })*/
/* */
/*     $('.remove-thread').on('click', function(){*/
/*         thread_id = $(this).data('thread-id');*/
/*         $this = $(this);*/
/*         confirmation = confirm('{{ text_confirmation }}');*/
/*         if(confirmation) {*/
/*             $.ajax({*/
/*                 url: 'index.php?route=catalog/wk_preorder/removeThread&user_token={{ user_token }}',*/
/*                 data: {*/
/*                     thread_id: thread_id*/
/*                 },*/
/*                 method: 'post',*/
/*                 dataType: 'json',*/
/*                 beforeSend: function(){*/
/*                     $this.html('<i class="fa fa-circle-o-notch fa-spin"></i>');*/
/*                 },*/
/*                 complete: function() {*/
/*                     $this.html('&times;');*/
/*                 },*/
/*                 success: function(response) {*/
/*                     if(response.success) {*/
/*                         $this.parent().parent().after('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button> '+response.msg+'</div>');*/
/*                         setTimeout(location.reload(), 2000);*/
/*                     } else {*/
/*                         $this.parent().parent().after('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button> '+response.msg+'</div>');*/
/*                     }*/
/*                 }*/
/*             })*/
/*         }*/
/*     })*/
/* </script>*/
/* {{ footer }} */
/* */
