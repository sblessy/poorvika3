<?php

/* catalog/mail_list.twig */
class __TwigTemplate_0dd061989faa42ddc8badceb5c483abc2b75e71c098ee6280816f517d26aa4cd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo "
";
        // line 2
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\">
      <a href=\"";
        // line 7
        echo (isset($context["insert"]) ? $context["insert"] : null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo (isset($context["button_insert"]) ? $context["button_insert"] : null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-plus\"></i></a>
      <button type=\"button\" data-toggle=\"tooltip\" title=\"";
        // line 8
        echo (isset($context["button_delete"]) ? $context["button_delete"] : null);
        echo "\" class=\"btn btn-danger\" id=\"delete\"><i class=\"fa fa-trash-o\"></i></button>
    </div>
      <h1>";
        // line 10
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
      <ul class=\"breadcrumb\">
        ";
        // line 12
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 13
            echo "          <li><a href=\"";
            echo $this->getAttribute($context["breadcrumb"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array());
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 15
        echo "      </ul>
    </div>
  </div>

  <div class=\"container-fluid\">
    ";
        // line 20
        if ((isset($context["error_warning"]) ? $context["error_warning"] : null)) {
            // line 21
            echo "      <div class=\"alert alert-danger\"><i class=\"fa fa-exclamation-circle\"></i>";
            echo (isset($context["error_warning"]) ? $context["error_warning"] : null);
            echo "
        <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
      </div>
    ";
        }
        // line 25
        echo "    ";
        if ((isset($context["success"]) ? $context["success"] : null)) {
            // line 26
            echo "      <div class=\"alert alert-success\"><i class=\"fa fa-check-circle\"></i>";
            echo (isset($context["success"]) ? $context["success"] : null);
            echo "
        <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
      </div>
    ";
        }
        // line 30
        echo "
    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <h3 class=\"panel-title\"><i class=\"fa fa-list\"></i>";
        // line 33
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h3>
      </div>

      <div class=\"panel-body\">
        <div class=\"well\">
          <div class=\"row\">
            <div class=\"col-sm-4\">
              <div class=\"form-group\">
                <label class=\"control-label\" for=\"input-id\">";
        // line 41
        echo (isset($context["entry_id"]) ? $context["entry_id"] : null);
        echo "</label>
                <input type=\"text\" name=\"filter_id\" value=\"";
        // line 42
        echo (isset($context["filter_id"]) ? $context["filter_id"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_id"]) ? $context["entry_id"] : null);
        echo "\" id=\"input-id\" class=\"form-control\" />
              </div>

              <div class=\"form-group\">
                <label class=\"control-label\" for=\"input-message\">";
        // line 46
        echo (isset($context["entry_message"]) ? $context["entry_message"] : null);
        echo "</label>
                <input type=\"text\" name=\"filter_message\" value=\"";
        // line 47
        echo (isset($context["filter_message"]) ? $context["filter_message"] : null);
        echo "\"  placeholder=\"";
        echo (isset($context["entry_message"]) ? $context["entry_message"] : null);
        echo "\" id=\"input-message\" class=\"form-control\" />
              </div>
            </div>

            <div class=\"col-sm-4\">
              <div class=\"form-group\">
                <label class=\"control-label\" for=\"input-name\">";
        // line 53
        echo (isset($context["entry_name"]) ? $context["entry_name"] : null);
        echo "</label>
                <input type=\"text\" name=\"filter_name\" value=\"";
        // line 54
        echo (isset($context["filter_name"]) ? $context["filter_name"] : null);
        echo "\"  placeholder=\"";
        echo (isset($context["entry_name"]) ? $context["entry_name"] : null);
        echo "\" id=\"input-name\" class=\"form-control\" />
              </div>
            </div>

            <div class=\"col-sm-4\">
              <div class=\"form-group\">
                <label class=\"control-label\" for=\"input-subject\">";
        // line 60
        echo (isset($context["entry_subject"]) ? $context["entry_subject"] : null);
        echo "</label>
                <input type=\"text\" name=\"filter_subject\" value=\"";
        // line 61
        echo (isset($context["filter_subject"]) ? $context["filter_subject"] : null);
        echo "\"  placeholder=\"";
        echo (isset($context["entry_subject"]) ? $context["entry_subject"] : null);
        echo "\" id=\"input-subject\" class=\"form-control\" />
              </div>
              <div class=\"btn-group pull-right\">
                  <button class=\"btn btn-primary\" type=\"button\" onclick=\"filter();\" data-toggle=\"tooltip\" title=\"";
        // line 64
        echo (isset($context["button_filter"]) ? $context["button_filter"] : null);
        echo "\" >
                    <i class=\"fa fa-filter\"></i>
                  </button>
                  <a class=\"btn btn-danger\" onclick=\"clearfilter();\" data-toggle=\"tooltip\" title=\"";
        // line 67
        echo (isset($context["button_clrfilter"]) ? $context["button_clrfilter"] : null);
        echo "\">
                    <i class=\"fa fa-eraser\"></i>
                  </a>
              </div>
            </div>
          </div>
        </div>

        <form action=\"";
        // line 75
        echo (isset($context["delete"]) ? $context["delete"] : null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-transaction\">
          <div class=\"table-responsive\">
            <table class=\"table table-bordered table-hover\">
              <thead>
              <tr>
              <td width=\"1\" style=\"text-align: center;\"><input type=\"checkbox\" onclick=\"\$('input[name*=\\'selected\\']').prop('checked', this.checked);\" /></td>

                <td class=\"text-left\">
                  ";
        // line 83
        if (((isset($context["sort"]) ? $context["sort"] : null) == "id")) {
            // line 84
            echo "                  <a href=\"";
            echo (isset($context["sort_id"]) ? $context["sort_id"] : null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, (isset($context["order"]) ? $context["order"] : null));
            echo "\">";
            echo (isset($context["entry_id"]) ? $context["entry_id"] : null);
            echo "</a>
                  ";
        } else {
            // line 86
            echo "                  <a href=\"";
            echo (isset($context["sort_id"]) ? $context["sort_id"] : null);
            echo "\">";
            echo (isset($context["entry_id"]) ? $context["entry_id"] : null);
            echo "</a>
                  ";
        }
        // line 88
        echo "                </td>
                <td class=\"text-left\">
                  ";
        // line 90
        if (((isset($context["sort"]) ? $context["sort"] : null) == "name")) {
            // line 91
            echo "                  <a href=\"";
            echo (isset($context["sort_name"]) ? $context["sort_name"] : null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, (isset($context["order"]) ? $context["order"] : null));
            echo "\">";
            echo (isset($context["entry_name"]) ? $context["entry_name"] : null);
            echo "</a>
                   ";
        } else {
            // line 93
            echo "                  <a href=\"";
            echo (isset($context["sort_name"]) ? $context["sort_name"] : null);
            echo "\">";
            echo (isset($context["entry_name"]) ? $context["entry_name"] : null);
            echo "</a>
                   ";
        }
        // line 95
        echo "
                </td>

                <td class=\"text-left\">
                  ";
        // line 99
        if (((isset($context["sort"]) ? $context["sort"] : null) == "subject")) {
            // line 100
            echo "                  <a href=\"";
            echo (isset($context["sort_subject"]) ? $context["sort_subject"] : null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, (isset($context["order"]) ? $context["order"] : null));
            echo "\">";
            echo (isset($context["entry_subject"]) ? $context["entry_subject"] : null);
            echo "</a>
                    ";
        } else {
            // line 102
            echo "                  <a href=\"";
            echo (isset($context["sort_subject"]) ? $context["sort_subject"] : null);
            echo "\">";
            echo (isset($context["entry_subject"]) ? $context["entry_subject"] : null);
            echo "</a>
                  ";
        }
        // line 104
        echo "
                </td>

                <td class=\"text-left\">
                  ";
        // line 108
        if (((isset($context["sort"]) ? $context["sort"] : null) == "message")) {
            // line 109
            echo "                  <a href=\"";
            echo (isset($context["sort_message"]) ? $context["sort_message"] : null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, (isset($context["order"]) ? $context["order"] : null));
            echo "\">";
            echo (isset($context["entry_message"]) ? $context["entry_message"] : null);
            echo "</a>
                    ";
        } else {
            // line 111
            echo "                  <a href=\"";
            echo (isset($context["sort_message"]) ? $context["sort_message"] : null);
            echo "\">";
            echo (isset($context["entry_message"]) ? $context["entry_message"] : null);
            echo "</a>
                  ";
        }
        // line 113
        echo "
                </td>

                <td class=\"text-right\">";
        // line 116
        echo (isset($context["entry_action"]) ? $context["entry_action"] : null);
        echo "</td>
              </tr>
            </thead>

            <tbody>
              ";
        // line 121
        if ((isset($context["mails"]) ? $context["mails"] : null)) {
            // line 122
            echo "              ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["mails"]) ? $context["mails"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["result"]) {
                // line 123
                echo "                <tr>
                  <td style=\"text-align: center;\">";
                // line 124
                if ($this->getAttribute($context["result"], "selected", array())) {
                    // line 125
                    echo "                    <input type=\"checkbox\" name=\"selected[]\" value=\"";
                    echo $this->getAttribute($context["result"], "id", array());
                    echo "\" checked=\"checked\" />
                   ";
                } else {
                    // line 127
                    echo "                    <input type=\"checkbox\" name=\"selected[]\" value=\"";
                    echo $this->getAttribute($context["result"], "id", array());
                    echo "\" />
                    ";
                }
                // line 129
                echo "                  </td>
                  <td class=\"text-left\" >";
                // line 130
                echo $this->getAttribute($context["result"], "id", array());
                echo "</td>
                  <td class=\"text-left\">";
                // line 131
                echo $this->getAttribute($context["result"], "name", array());
                echo "</td>
                  <td class=\"text-left\">";
                // line 132
                echo $this->getAttribute($context["result"], "subject", array());
                echo "</td>
                  <td class=\"text-left\">";
                // line 133
                echo $this->getAttribute($context["result"], "message", array());
                echo " ";
                echo "</td>
                  <td class=\"text-right\">
                      <a href=\"";
                // line 135
                echo $this->getAttribute($context["result"], "action", array());
                echo "\" data-toggle=\"tooltip\" title=\"";
                echo (isset($context["text_edit"]) ? $context["text_edit"] : null);
                echo "\" class=\"btn btn-primary\"><i class=\"fa fa-pencil\"></i></a>
                  </td>

                </tr>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['result'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 140
            echo "              ";
        } else {
            // line 141
            echo "              <tr>
                <td class=\"text-center\" colspan=\"6\">";
            // line 142
            echo (isset($context["entry_no_records"]) ? $context["entry_no_records"] : null);
            echo "</td>
              </tr>
              ";
        }
        // line 145
        echo "            </tbody>
          </table>
        </div>
      </form>

      <div class=\"row\">
        <div class=\"col-sm-6 text-left\">";
        // line 151
        echo (isset($context["pagination"]) ? $context["pagination"] : null);
        echo "</div>
        <div class=\"col-sm-6 text-right\">";
        // line 152
        echo (isset($context["results"]) ? $context["results"] : null);
        echo "</div>
      </div>
      </div>
    </div>
  </div>
</div>

<script type=\"text/javascript\"><!--
\$('body').on('click','#delete',function(){
  var flag = false;
  \$('input[type=\\'checkbox\\']').each(function(){
    if (\$(this).is(':checked')) {
      flag = true;
    }
  });
  if (flag) {
      confirm('";
        // line 168
        echo (isset($context["text_confirm"]) ? $context["text_confirm"] : null);
        echo "') ? \$('#form-transaction').submit() : false;
  } else {
     alert('Please select first');
  }
});

\$('#form input').keydown(function(e) {
  if (e.keyCode == 13) {
    filter();
  }
});

function clearfilter() {
  url = 'index.php?route=catalog/mails&user_token=";
        // line 181
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "';
  location = url;
}

function filter() {

  url = 'index.php?route=catalog/mails&user_token=";
        // line 187
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "';

  var filter_name = \$('input[name=\\'filter_name\\']').val();

  if (filter_name) {
    url += '&filter_name=' + encodeURIComponent(filter_name);
  }

  var filter_id = \$('input[name=\\'filter_id\\']').val();

  if (filter_id) {
    url += '&filter_id=' + encodeURIComponent(filter_id);
  }

  var filter_subject = \$('input[name=\\'filter_subject\\']').val();

  if (filter_subject) {
    url += '&filter_subject=' + encodeURIComponent(filter_subject);
  }

  var filter_message = \$('input[name=\\'filter_message\\']').val();

  if (filter_message) {
    url += '&filter_message=' + encodeURIComponent(filter_message);
  }

  location = url;
}
//--></script>
";
        // line 216
        echo (isset($context["footer"]) ? $context["footer"] : null);
        echo "
";
    }

    public function getTemplateName()
    {
        return "catalog/mail_list.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  456 => 216,  424 => 187,  415 => 181,  399 => 168,  380 => 152,  376 => 151,  368 => 145,  362 => 142,  359 => 141,  356 => 140,  343 => 135,  337 => 133,  333 => 132,  329 => 131,  325 => 130,  322 => 129,  316 => 127,  310 => 125,  308 => 124,  305 => 123,  300 => 122,  298 => 121,  290 => 116,  285 => 113,  277 => 111,  267 => 109,  265 => 108,  259 => 104,  251 => 102,  241 => 100,  239 => 99,  233 => 95,  225 => 93,  215 => 91,  213 => 90,  209 => 88,  201 => 86,  191 => 84,  189 => 83,  178 => 75,  167 => 67,  161 => 64,  153 => 61,  149 => 60,  138 => 54,  134 => 53,  123 => 47,  119 => 46,  110 => 42,  106 => 41,  95 => 33,  90 => 30,  82 => 26,  79 => 25,  71 => 21,  69 => 20,  62 => 15,  51 => 13,  47 => 12,  42 => 10,  37 => 8,  31 => 7,  23 => 2,  19 => 1,);
    }
}
/* {{ header }}*/
/* {{ column_left }}*/
/* <div id="content">*/
/*   <div class="page-header">*/
/*     <div class="container-fluid">*/
/*       <div class="pull-right">*/
/*       <a href="{{ insert }}" data-toggle="tooltip" title="{{ button_insert}}" class="btn btn-primary"><i class="fa fa-plus"></i></a>*/
/*       <button type="button" data-toggle="tooltip" title="{{ button_delete}}" class="btn btn-danger" id="delete"><i class="fa fa-trash-o"></i></button>*/
/*     </div>*/
/*       <h1>{{ heading_title }}</h1>*/
/*       <ul class="breadcrumb">*/
/*         {% for breadcrumb in breadcrumbs %}*/
/*           <li><a href="{{ breadcrumb.href }}">{{ breadcrumb.text }}</a></li>*/
/*         {% endfor %}*/
/*       </ul>*/
/*     </div>*/
/*   </div>*/
/* */
/*   <div class="container-fluid">*/
/*     {% if error_warning %}*/
/*       <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i>{{ error_warning }}*/
/*         <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*       </div>*/
/*     {% endif %}*/
/*     {% if success %}*/
/*       <div class="alert alert-success"><i class="fa fa-check-circle"></i>{{ success }}*/
/*         <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*       </div>*/
/*     {% endif %}*/
/* */
/*     <div class="panel panel-default">*/
/*       <div class="panel-heading">*/
/*         <h3 class="panel-title"><i class="fa fa-list"></i>{{ heading_title }}</h3>*/
/*       </div>*/
/* */
/*       <div class="panel-body">*/
/*         <div class="well">*/
/*           <div class="row">*/
/*             <div class="col-sm-4">*/
/*               <div class="form-group">*/
/*                 <label class="control-label" for="input-id">{{ entry_id }}</label>*/
/*                 <input type="text" name="filter_id" value="{{ filter_id }}" placeholder="{{ entry_id }}" id="input-id" class="form-control" />*/
/*               </div>*/
/* */
/*               <div class="form-group">*/
/*                 <label class="control-label" for="input-message">{{ entry_message }}</label>*/
/*                 <input type="text" name="filter_message" value="{{ filter_message }}"  placeholder="{{ entry_message }}" id="input-message" class="form-control" />*/
/*               </div>*/
/*             </div>*/
/* */
/*             <div class="col-sm-4">*/
/*               <div class="form-group">*/
/*                 <label class="control-label" for="input-name">{{ entry_name }}</label>*/
/*                 <input type="text" name="filter_name" value="{{ filter_name }}"  placeholder="{{ entry_name }}" id="input-name" class="form-control" />*/
/*               </div>*/
/*             </div>*/
/* */
/*             <div class="col-sm-4">*/
/*               <div class="form-group">*/
/*                 <label class="control-label" for="input-subject">{{ entry_subject }}</label>*/
/*                 <input type="text" name="filter_subject" value="{{ filter_subject }}"  placeholder="{{ entry_subject }}" id="input-subject" class="form-control" />*/
/*               </div>*/
/*               <div class="btn-group pull-right">*/
/*                   <button class="btn btn-primary" type="button" onclick="filter();" data-toggle="tooltip" title="{{ button_filter }}" >*/
/*                     <i class="fa fa-filter"></i>*/
/*                   </button>*/
/*                   <a class="btn btn-danger" onclick="clearfilter();" data-toggle="tooltip" title="{{ button_clrfilter }}">*/
/*                     <i class="fa fa-eraser"></i>*/
/*                   </a>*/
/*               </div>*/
/*             </div>*/
/*           </div>*/
/*         </div>*/
/* */
/*         <form action="{{ delete }}" method="post" enctype="multipart/form-data" id="form-transaction">*/
/*           <div class="table-responsive">*/
/*             <table class="table table-bordered table-hover">*/
/*               <thead>*/
/*               <tr>*/
/*               <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>*/
/* */
/*                 <td class="text-left">*/
/*                   {% if sort == 'id'  %}*/
/*                   <a href="{{ sort_id }}" class="{{ order|lower }}">{{ entry_id }}</a>*/
/*                   {% else %}*/
/*                   <a href="{{ sort_id }}">{{ entry_id }}</a>*/
/*                   {% endif %}*/
/*                 </td>*/
/*                 <td class="text-left">*/
/*                   {% if sort == 'name' %}*/
/*                   <a href="{{ sort_name }}" class="{{ order|lower }}">{{ entry_name }}</a>*/
/*                    {% else %}*/
/*                   <a href="{{ sort_name }}">{{ entry_name }}</a>*/
/*                    {% endif %}*/
/* */
/*                 </td>*/
/* */
/*                 <td class="text-left">*/
/*                   {% if sort  ==  'subject' %}*/
/*                   <a href="{{ sort_subject }}" class="{{ order|lower }}">{{ entry_subject }}</a>*/
/*                     {% else %}*/
/*                   <a href="{{ sort_subject }}">{{ entry_subject }}</a>*/
/*                   {% endif %}*/
/* */
/*                 </td>*/
/* */
/*                 <td class="text-left">*/
/*                   {% if sort  ==  'message' %}*/
/*                   <a href="{{ sort_message }}" class="{{ order|lower }}">{{ entry_message }}</a>*/
/*                     {% else %}*/
/*                   <a href="{{ sort_message }}">{{ entry_message }}</a>*/
/*                   {% endif %}*/
/* */
/*                 </td>*/
/* */
/*                 <td class="text-right">{{ entry_action }}</td>*/
/*               </tr>*/
/*             </thead>*/
/* */
/*             <tbody>*/
/*               {% if mails %}*/
/*               {% for result in mails %}*/
/*                 <tr>*/
/*                   <td style="text-align: center;">{% if result.selected  %}*/
/*                     <input type="checkbox" name="selected[]" value="{{ result.id }}" checked="checked" />*/
/*                    {% else %}*/
/*                     <input type="checkbox" name="selected[]" value="{{ result.id }}" />*/
/*                     {% endif %}*/
/*                   </td>*/
/*                   <td class="text-left" >{{ result.id }}</td>*/
/*                   <td class="text-left">{{ result.name }}</td>*/
/*                   <td class="text-left">{{ result.subject }}</td>*/
/*                   <td class="text-left">{% autoescape false %}{{ result.message }} {% endautoescape %}</td>*/
/*                   <td class="text-right">*/
/*                       <a href="{{ result.action}}" data-toggle="tooltip" title="{{ text_edit }}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>*/
/*                   </td>*/
/* */
/*                 </tr>*/
/*                 {% endfor %}*/
/*               {% else %}*/
/*               <tr>*/
/*                 <td class="text-center" colspan="6">{{ entry_no_records }}</td>*/
/*               </tr>*/
/*               {% endif %}*/
/*             </tbody>*/
/*           </table>*/
/*         </div>*/
/*       </form>*/
/* */
/*       <div class="row">*/
/*         <div class="col-sm-6 text-left">{{ pagination }}</div>*/
/*         <div class="col-sm-6 text-right">{{ results }}</div>*/
/*       </div>*/
/*       </div>*/
/*     </div>*/
/*   </div>*/
/* </div>*/
/* */
/* <script type="text/javascript"><!--*/
/* $('body').on('click','#delete',function(){*/
/*   var flag = false;*/
/*   $('input[type=\'checkbox\']').each(function(){*/
/*     if ($(this).is(':checked')) {*/
/*       flag = true;*/
/*     }*/
/*   });*/
/*   if (flag) {*/
/*       confirm('{{ text_confirm }}') ? $('#form-transaction').submit() : false;*/
/*   } else {*/
/*      alert('Please select first');*/
/*   }*/
/* });*/
/* */
/* $('#form input').keydown(function(e) {*/
/*   if (e.keyCode == 13) {*/
/*     filter();*/
/*   }*/
/* });*/
/* */
/* function clearfilter() {*/
/*   url = 'index.php?route=catalog/mails&user_token={{ user_token }}';*/
/*   location = url;*/
/* }*/
/* */
/* function filter() {*/
/* */
/*   url = 'index.php?route=catalog/mails&user_token={{ user_token }}';*/
/* */
/*   var filter_name = $('input[name=\'filter_name\']').val();*/
/* */
/*   if (filter_name) {*/
/*     url += '&filter_name=' + encodeURIComponent(filter_name);*/
/*   }*/
/* */
/*   var filter_id = $('input[name=\'filter_id\']').val();*/
/* */
/*   if (filter_id) {*/
/*     url += '&filter_id=' + encodeURIComponent(filter_id);*/
/*   }*/
/* */
/*   var filter_subject = $('input[name=\'filter_subject\']').val();*/
/* */
/*   if (filter_subject) {*/
/*     url += '&filter_subject=' + encodeURIComponent(filter_subject);*/
/*   }*/
/* */
/*   var filter_message = $('input[name=\'filter_message\']').val();*/
/* */
/*   if (filter_message) {*/
/*     url += '&filter_message=' + encodeURIComponent(filter_message);*/
/*   }*/
/* */
/*   location = url;*/
/* }*/
/* //--></script>*/
/* {{ footer }}*/
/* */
