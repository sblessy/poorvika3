<?php

/* so-destino/template/product/product.twig */
class __TwigTemplate_5067ac67f1d1bc8e4d003ce31000790202db95e317c44e4c3613e631cd9651cc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo "


\t<link rel=\"stylesheet\" type=\"text/css\" href=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/css/owl.carousel.min.css\" />
<link rel=\"stylesheet\" type=\"text/css\" href=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/css/owl.theme.default.min.css\" />
<style>
\t.product-product{
\t\tmargin-top: 120px;
\t}
    .product-view .content-product-right h3{
        color: #666 !important;
        margin-top: 20px;
    }
    .pl-8{
        padding-left: 8px;
    }
    .attrcolor{
        color: #000;
    }
    .stock b{
          color: #16a904;
    background: #E9F5EB;
    border-radius: 20px;
    padding: 8px 13px;
    }
    .cont-right h4 {
    padding: 10px 0px 10px 20px;
}
    .stock b i {
        margin-right: 2px;
        color: inherit;
        font-size: 14px;
        vertical-align: middle;
    }
    .stock {
    display: inline-block;
    position: relative;
    top: -6px;
}
    .pl-20{
        padding-left: 20px;
    }
    .product-color-show{
        padding-top: 15px !important;
    }
    .cont-right h4 {
    padding-left: 20px;
}
    #checktext{
        margin-left: 10px;
    }
    #pin_avilability_ship {
        display: flex;
        display: -webkit-flex;
        display: -ms-flexbox;
        align-items: center;
        justify-content: flex-start;
        margin-top: 10px !important;
        margin-left: 0 !important;
    }
    .delivery_type {
        min-width: 90px;
        text-transform: capitalize;
        font-weight: 500 !important;
        color: rgb(102, 102, 102) !important;
        font-size: 14px !important;
    }
    .delivery_text, .invalid_pin {
        font-weight: 500 !important;
        font-style: italic;
    }
    .delivery_text {
        display: flex;
        display: -webkit-flex;
        display: -ms-flexbox;
        align-items: center;
        justify-content: flex-start;
    }
    span.img_pin {
        display: flex;
        display: -webkit-flex;
        display: -ms-flexbox;
        align-items: flex-start;
        flex-direction: column;
        justify-content: flex-start;
        font-size: 12px;
        color: #777;
        font-style: italic;
        margin-right: 10px;
    }
    .delivery_hrs img {
        width: 70px;
        margin-right: 5px;
    }
</style>


";
        // line 99
        if ((isset($context["url_asidePosition"]) ? $context["url_asidePosition"] : null)) {
            $context["col_position"] = (isset($context["url_asidePosition"]) ? $context["url_asidePosition"] : null);
        } else {
            // line 100
            $context["col_position"] = $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "catalog_col_position"), "method");
        }
        // line 101
        echo "
";
        // line 102
        if ((isset($context["url_asideType"]) ? $context["url_asideType"] : null)) {
            echo " ";
            $context["col_canvas"] = (isset($context["url_asideType"]) ? $context["url_asideType"] : null);
        } else {
            // line 103
            $context["col_canvas"] = $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "catalog_col_type"), "method");
        }
        // line 104
        echo "
";
        // line 105
        if ((isset($context["url_productGallery"]) ? $context["url_productGallery"] : null)) {
            echo " ";
            $context["productGallery"] = (isset($context["url_productGallery"]) ? $context["url_productGallery"] : null);
        } else {
            // line 106
            $context["productGallery"] = $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "thumbnails_position"), "method");
        }
        // line 107
        echo "
";
        // line 108
        if ((isset($context["url_sidebarsticky"]) ? $context["url_sidebarsticky"] : null)) {
            echo " ";
            $context["sidebar_sticky"] = (isset($context["url_sidebarsticky"]) ? $context["url_sidebarsticky"] : null);
        } else {
            // line 109
            echo " ";
            $context["sidebar_sticky"] = $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "catalog_sidebar_sticky"), "method");
        }
        // line 110
        echo "
";
        // line 111
        $context["desktop_canvas"] = ((((isset($context["col_canvas"]) ? $context["col_canvas"] : null) == "off_canvas")) ? ("desktop-offcanvas") : (""));
        // line 112
        echo "
<div class=\"content-main container product-detail  ";
        // line 113
        echo (isset($context["desktop_canvas"]) ? $context["desktop_canvas"] : null);
        echo "\">
\t<div class=\"row\">
\t\t
\t\t";
        // line 117
        echo "
\t\t";
        // line 118
        if (((isset($context["col_position"]) ? $context["col_position"] : null) == "outside")) {
            // line 119
            echo "\t\t\t";
            echo (isset($context["column_left"]) ? $context["column_left"] : null);
            echo "
\t\t\t
\t\t\t";
            // line 121
            if (((isset($context["col_canvas"]) ? $context["col_canvas"] : null) == "off_canvas")) {
                // line 122
                echo "\t\t\t\t";
                $context["class_pos"] = "col-sm-12";
                // line 123
                echo "\t    \t";
            } elseif (((isset($context["column_left"]) ? $context["column_left"] : null) && (isset($context["column_right"]) ? $context["column_right"] : null))) {
                // line 124
                echo "\t    \t\t";
                $context["class_pos"] = "col-md-6 col-xs-12 fluid-allsidebar";
                // line 125
                echo "\t\t    ";
            } elseif (((isset($context["column_left"]) ? $context["column_left"] : null) || (isset($context["column_right"]) ? $context["column_right"] : null))) {
                // line 126
                echo "\t\t    \t";
                $context["class_pos"] = "col-md-9 col-sm-12 col-xs-12 fluid-sidebar";
                // line 127
                echo "\t\t    ";
            } else {
                // line 128
                echo "\t\t    \t";
                $context["class_pos"] = "col-sm-12";
                // line 129
                echo "\t\t    ";
            }
            // line 130
            echo "\t\t";
        } else {
            // line 131
            echo "\t\t\t";
            $context["class_pos"] = "col-sm-12";
            // line 132
            echo "\t\t";
        }
        // line 133
        echo "\t\t";
        // line 134
        echo "    \t
\t\t<div id=\"content\" class=\"product-view ";
        // line 135
        echo (isset($context["class_pos"]) ? $context["class_pos"] : null);
        echo "\"> 
\t\t
\t\t";
        // line 138
        echo "\t\t";
        if (((isset($context["productGallery"]) ? $context["productGallery"] : null) == "grid")) {
            // line 139
            echo "\t\t\t";
            $context["class_left_gallery"] = "col-md-6 col-sm-12 col-xs-12";
            // line 140
            echo "\t\t\t";
            $context["class_right_gallery"] = "col-md-6 col-sm-12 col-xs-12";
            // line 141
            echo "\t\t";
        } elseif (((isset($context["productGallery"]) ? $context["productGallery"] : null) == "list")) {
            // line 142
            echo "\t\t\t";
            $context["class_left_gallery"] = "col-md-5 col-sm-12 col-xs-12";
            // line 143
            echo "\t\t\t";
            $context["class_right_gallery"] = "col-md-7 col-sm-12 col-xs-12";
            // line 144
            echo "\t\t";
        } elseif (((isset($context["productGallery"]) ? $context["productGallery"] : null) == "left")) {
            // line 145
            echo "\t\t\t";
            $context["class_left_gallery"] = "col-md-6 col-sm-12 col-xs-12";
            // line 146
            echo "\t\t\t";
            $context["class_right_gallery"] = "col-md-6 col-sm-12 col-xs-12";
            // line 147
            echo "\t\t\t";
        } elseif (((isset($context["productGallery"]) ? $context["productGallery"] : null) == "bottom")) {
            // line 148
            echo "\t\t";
            $context["class_left_gallery"] = "col-md-5 col-sm-12 col-xs-12";
            // line 149
            echo "\t\t\t";
            $context["class_right_gallery"] = "col-md-7 col-sm-12 col-xs-12";
            // line 150
            echo "\t\t";
        } else {
            // line 151
            echo "\t\t\t";
            $context["class_left_gallery"] = "col-md-12 col-sm-12 col-xs-12";
            // line 152
            echo "\t\t\t";
            $context["class_right_gallery"] = "col-md-12 col-sm-12 col-xs-12 col-gallery-slider";
            // line 153
            echo "\t\t";
        }
        // line 154
        echo "
\t\t";
        // line 156
        echo "\t\t";
        if (((isset($context["column_left"]) ? $context["column_left"] : null) || (isset($context["column_right"]) ? $context["column_right"] : null))) {
            // line 157
            echo "\t\t\t";
            $context["class_canvas"] = ((((isset($context["col_canvas"]) ? $context["col_canvas"] : null) == "off_canvas")) ? ("") : ("hidden-lg hidden-md"));
            // line 158
            echo "\t\t\t<a href=\"javascript:void(0)\" class=\" open-sidebar ";
            echo (isset($context["class_canvas"]) ? $context["class_canvas"] : null);
            echo "\"><i class=\"fa fa-bars\"></i>";
            echo (isset($context["text_sidebar"]) ? $context["text_sidebar"] : null);
            echo "</a>
\t\t\t<div class=\"sidebar-overlay \"></div>
\t\t";
        }
        // line 161
        echo "

\t\t<div class=\"content-product-mainheader clearfix\"> 
\t\t\t<div class=\"row\">\t
\t\t\t";
        // line 166
        echo "\t\t\t<div class=\"content-product-left  ";
        echo (isset($context["class_left_gallery"]) ? $context["class_left_gallery"] : null);
        echo "\" >
\t\t\t\t";
        // line 167
        if ((isset($context["images"]) ? $context["images"] : null)) {
            // line 168
            echo "\t\t\t\t\t<div class=\"so-loadeding\" ></div>
\t\t\t\t\t";
            // line 170
            echo "\t\t\t\t
 

\t\t\t\t\t";
            // line 173
            if (((isset($context["productGallery"]) ? $context["productGallery"] : null) == "left")) {
                // line 174
                echo "\t\t\t\t\t \t";
                $this->loadTemplate(((isset($context["theme_directory"]) ? $context["theme_directory"] : null) . "/template/product/gallery/gallery-left.twig"), "so-destino/template/product/product.twig", 174)->display($context);
                // line 175
                echo "
\t\t\t\t\t";
            } elseif ((            // line 176
(isset($context["productGallery"]) ? $context["productGallery"] : null) == "bottom")) {
                // line 177
                echo "\t\t\t\t\t\t";
                $this->loadTemplate(((isset($context["theme_directory"]) ? $context["theme_directory"] : null) . "/template/product/gallery/gallery-bottom.twig"), "so-destino/template/product/product.twig", 177)->display($context);
                // line 178
                echo "
\t\t\t\t\t";
            } elseif ((            // line 179
(isset($context["productGallery"]) ? $context["productGallery"] : null) == "grid")) {
                // line 180
                echo "\t\t\t\t\t\t";
                $this->loadTemplate(((isset($context["theme_directory"]) ? $context["theme_directory"] : null) . "/template/product/gallery/gallery-grid.twig"), "so-destino/template/product/product.twig", 180)->display($context);
                // line 181
                echo "
\t\t\t\t\t";
            } elseif ((            // line 182
(isset($context["productGallery"]) ? $context["productGallery"] : null) == "list")) {
                // line 183
                echo "\t\t\t\t\t\t";
                $this->loadTemplate(((isset($context["theme_directory"]) ? $context["theme_directory"] : null) . "/template/product/gallery/gallery-list.twig"), "so-destino/template/product/product.twig", 183)->display($context);
                // line 184
                echo "
\t\t\t\t\t";
            } elseif ((            // line 185
(isset($context["productGallery"]) ? $context["productGallery"] : null) == "slider")) {
                // line 186
                echo "\t\t\t\t\t\t";
                $this->loadTemplate(((isset($context["theme_directory"]) ? $context["theme_directory"] : null) . "/template/product/gallery/gallery-slider.twig"), "so-destino/template/product/product.twig", 186)->display($context);
                // line 187
                echo "\t\t\t\t\t";
            }
            // line 188
            echo "\t\t\t\t";
        }
        // line 189
        echo "\t\t\t\t  
\t\t\t\t\t<div class=\"col-md-12 pl-0 pr-0\">  
\t\t\t<div class=\"cart\">
\t\t\t    <input type=\"button\" value=\"";
        // line 192
        echo (isset($context["button_cart"]) ? $context["button_cart"] : null);
        echo "\" data-loading-text=\"";
        echo (isset($context["text_loading"]) ? $context["text_loading"] : null);
        echo "\" id=\"button-cart\" class=\"btn btn-mega btn-lg btn-product-page\">
\t\t\t </div>
\t\t\t
\t\t\t \t<div class=\"buynow\">
\t\t\t    <input type=\"button\" value=\"Buy Now\" id=\"button-cart\" class=\"btn btn-mega btn-lg btn-product-buy\">
\t\t\t </div>
\t\t\t
\t\t\t </div>
\t\t\t</div>
\t\t
        \t";
        // line 203
        echo "
\t\t\t";
        // line 205
        echo "\t\t\t
\t\t\t<div class=\"content-product-right ";
        // line 206
        echo (isset($context["class_right_gallery"]) ? $context["class_right_gallery"] : null);
        echo "\" itemprop=\"offerDetails\" itemscope itemtype=\"http://schema.org/Product\">
\t\t\t    <div class=\"cont-right\">
\t\t\t  
\t\t\t    <div class=\"product-breadcrumb\">
\t\t\t    ";
        // line 210
        $this->loadTemplate(((isset($context["theme_directory"]) ? $context["theme_directory"] : null) . "/template/soconfig/breadcrumbs.twig"), "so-destino/template/product/product.twig", 210)->display($context);
        // line 211
        echo "              <span>Add to compare <input type=\"checkbox\" id=\"product-compare\"></span>
              </div>

\t\t\t\t<div class=\"title-product\">
\t\t\t\t\t\t <h1 itemprop=\"name\">";
        // line 215
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
\t\t\t\t\t\t <ul class=\"product-share-links\">
\t\t\t\t\t\t     <li><span>Wishlist</span><a onclick=\"wishlist.add(";
        // line 217
        echo (isset($context["product_id"]) ? $context["product_id"] : null);
        echo ");\"><i class=\"fa fa-heart\"></i></a></li>
\t\t\t\t\t\t     <li><a onclick=\"compare.add(";
        // line 218
        echo (isset($context["product_id"]) ? $context["product_id"] : null);
        echo ");\"><i class=\"fa fa-share-alt\"></i></a></li>
\t\t\t\t\t\t     </ul>
\t\t\t\t\t</div>
\t\t\t\t<div class=\"title-product\">
\t\t\t\t\t<h3>Product Code : ";
        // line 222
        echo (isset($context["item_code"]) ? $context["item_code"] : null);
        echo "</h3>
\t\t\t\t</div>
\t\t\t\t
\t\t\t\t";
        // line 225
        if ((isset($context["review_status"]) ? $context["review_status"] : null)) {
            // line 226
            echo "\t\t\t\t\t";
            // line 227
            echo "\t\t\t\t\t<div class=\"box-review\"  itemprop=\"aggregateRating\" itemscope itemtype=\"http://schema.org/AggregateRating\">
\t\t\t\t\t\t";
            // line 228
            if ((isset($context["count_reviews"]) ? $context["count_reviews"] : null)) {
                // line 229
                echo "\t\t\t\t\t\t\t\t<meta itemprop=\"ratingValue\" content=\"";
                echo (isset($context["rating"]) ? $context["rating"] : null);
                echo "\">
\t\t\t\t\t\t\t\t<meta itemprop=\"ratingCount\" content=\"";
                // line 230
                echo (isset($context["count_reviews"]) ? $context["count_reviews"] : null);
                echo "\">
\t\t\t\t\t\t\t\t<meta itemprop=\"reviewCount\" content=\"";
                // line 231
                echo (isset($context["count_reviews"]) ? $context["count_reviews"] : null);
                echo "\">
\t\t\t\t\t\t";
            }
            // line 233
            echo "\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"rating\">
\t\t\t\t\t\t    <div class=\"rating-show\">
\t\t\t\t\t\t        <h6>4.5</h6>
\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t<div class=\"rating-box\">
\t\t\t\t\t\t\t";
            // line 239
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(1, 5));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 240
                echo "\t\t\t\t\t\t\t\t";
                if (((isset($context["rating"]) ? $context["rating"] : null) < $context["i"])) {
                    echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
                } else {
                    echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
                }
                // line 241
                echo "\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 242
            echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<a class=\"reviews_button\" href=\"\" onclick=\"\$('a[href=\\'#tab-review\\']').trigger('click'); return false;\">";
            // line 244
            echo (isset($context["reviews"]) ? $context["reviews"] : null);
            echo "</a>
\t\t\t\t\t\t";
            // line 245
            if ($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "product_order"), "method")) {
                // line 246
                echo "\t\t\t\t\t\t\t\t\t<span class=\"order-num\">";
                echo (isset($context["orders"]) ? $context["orders"] : null);
                echo "</span>
\t\t\t\t\t\t";
            }
            // line 248
            echo "\t\t\t\t\t
\t\t\t\t\t</div>
\t\t\t\t\t";
        }
        // line 251
        echo "
\t\t\t\t";
        // line 252
        if ((isset($context["price"]) ? $context["price"] : null)) {
            // line 253
            echo "
                ";
            // line 254
            if ((array_key_exists("text_discount_applied", $context) && (isset($context["text_discount_applied"]) ? $context["text_discount_applied"] : null))) {
                // line 255
                echo "                <h4><span class=\"bg-warning text-warning\">";
                echo (isset($context["text_discount_applied"]) ? $context["text_discount_applied"] : null);
                echo "</span></h4>
                ";
            }
            // line 257
            echo "                
                
\t\t\t\t\t";
            // line 260
            echo "\t\t\t\t\t <div class=\"col-md-12 pl-0\" style=\"margin-bottom: 10px;\">
                   
               
\t\t\t\t\t<div class=\"product_page_price price\" itemprop=\"offers\" itemscope itemtype=\"http://schema.org/Offer\">
\t\t\t\t\t\t";
            // line 264
            if ( !(isset($context["special"]) ? $context["special"] : null)) {
                // line 265
                echo "\t\t\t\t\t\t\t<span class=\"price-new\">
\t\t\t\t\t\t\t\t<span itemprop=\"price\" content=\"";
                // line 266
                echo (isset($context["price_value"]) ? $context["price_value"] : null);
                echo "\" id=\"price-old\"> 
 ";
                // line 267
                if (($this->getAttribute((isset($context["cfp_setting"]) ? $context["cfp_setting"] : null), "module_so_call_for_price_status", array()) && ((isset($context["price_0"]) ? $context["price_0"] : null) <= 0))) {
                    echo " 
 ";
                    // line 268
                    if (($this->getAttribute((isset($context["cfp_setting"]) ? $context["cfp_setting"] : null), "module_so_call_for_price_replace_cart", array(), "any", true, true) && ($this->getAttribute((isset($context["cfp_setting"]) ? $context["cfp_setting"] : null), "module_so_call_for_price_replace_cart", array()) == "0"))) {
                        echo " 
 <a data-fancybox data-type=\"ajax\" data-src=\"";
                        // line 269
                        echo (isset($context["base"]) ? $context["base"] : null);
                        echo "index.php?route=extension/module/so_call_for_price&product_id=";
                        echo $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "product_id", array());
                        echo "\" href=\"javascript:;\" class=\"callforprice\" style=\"color: #ff0000; font-weight: bold; font-size: 18px;\"><i class=\"fa fa-phone\" style=\"font-size: 18px;\"></i> ";
                        echo (isset($context["text_price_0"]) ? $context["text_price_0"] : null);
                        echo "</a> 
 ";
                    }
                    // line 270
                    echo " 
 ";
                } else {
                    // line 271
                    echo " 
  
 ";
                    // line 273
                    if (($this->getAttribute((isset($context["cfp_setting"]) ? $context["cfp_setting"] : null), "module_so_call_for_price_status", array()) && ((isset($context["price_0"]) ? $context["price_0"] : null) <= 0))) {
                        echo " 
 ";
                        // line 274
                        if (($this->getAttribute((isset($context["cfp_setting"]) ? $context["cfp_setting"] : null), "module_so_call_for_price_replace_cart", array(), "any", true, true) && ($this->getAttribute((isset($context["cfp_setting"]) ? $context["cfp_setting"] : null), "module_so_call_for_price_replace_cart", array()) == "0"))) {
                            echo " 
 <a data-fancybox data-type=\"ajax\" data-src=\"";
                            // line 275
                            echo (isset($context["base"]) ? $context["base"] : null);
                            echo "index.php?route=extension/module/so_call_for_price&product_id=";
                            echo $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "product_id", array());
                            echo "\" href=\"javascript:;\" class=\"callforprice\" style=\"color: #ff0000; font-weight: bold;\"><i class=\"fa fa-phone\"></i> ";
                            echo (isset($context["text_price_0"]) ? $context["text_price_0"] : null);
                            echo "</a> 
 ";
                        }
                        // line 276
                        echo " 
 ";
                    } else {
                        // line 277
                        echo " 
 ";
                        // line 278
                        echo (isset($context["price"]) ? $context["price"] : null);
                        echo " 
 ";
                    }
                    // line 279
                    echo " 
  
 ";
                }
                // line 281
                echo " 
 </span>
\t\t\t\t\t\t\t\t<meta itemprop=\"priceCurrency\" content=\"";
                // line 283
                echo (isset($context["currency"]) ? $context["currency"] : null);
                echo "\" />
\t\t\t\t\t\t\t</span>

\t\t\t\t\t\t";
            } else {
                // line 287
                echo "\t\t\t\t\t\t
\t\t\t\t\t\t\t<span class=\"price-new\">
\t\t\t\t\t\t\t\t<span itemprop=\"price\" content=\"";
                // line 289
                echo (isset($context["special_value"]) ? $context["special_value"] : null);
                echo "\" id=\"price-special\">";
                echo (isset($context["special"]) ? $context["special"] : null);
                echo "</span>
\t\t\t\t\t\t\t\t<meta itemprop=\"priceCurrency\" content=\"";
                // line 290
                echo (isset($context["currency"]) ? $context["currency"] : null);
                echo "\" />
\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t   <span class=\"price-old\" id=\"price-old\"> 
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t   </span>
\t\t\t\t\t\t   
\t\t\t\t\t\t";
            }
            // line 297
            echo "\t\t\t\t\t\t
\t\t\t\t\t\t";
            // line 298
            if (((isset($context["special"]) ? $context["special"] : null) && $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "discount_status"), "method"))) {
                echo " 
\t\t\t\t\t\t";
                // line 300
                echo "\t\t\t\t\t\t<span class=\"label-product label-sale\">
\t\t\t\t\t\t\t ";
                // line 301
                echo (isset($context["discount"]) ? $context["discount"] : null);
                echo "
\t\t\t\t\t\t</span>
\t\t\t\t\t\t";
            }
            // line 303
            echo " 

\t\t\t\t\t\t 
 ";
            // line 306
            if ((((isset($context["tax"]) ? $context["tax"] : null) && $this->getAttribute((isset($context["cfp_setting"]) ? $context["cfp_setting"] : null), "module_so_call_for_price_status", array())) && ((isset($context["price_0"]) ? $context["price_0"] : null) > 0))) {
                echo " 
 
\t\t\t\t\t\t\t<div class=\"price-tax\"><span>";
                // line 308
                echo (isset($context["text_tax"]) ? $context["text_tax"] : null);
                echo "</span> <span id=\"price-tax\"> ";
                echo (isset($context["tax"]) ? $context["tax"] : null);
                echo " </span></div>
\t\t\t\t\t\t";
            }
            // line 310
            echo "\t\t\t\t\t 
\t\t\t\t\t</div>
\t\t\t\t\t <div class=\"stock ptb-10 pl-20\"><b> <i class=\"fa fa-check-circle\"></i> ";
            // line 312
            echo (isset($context["stock"]) ? $context["stock"] : null);
            echo "</b></div>
\t\t\t\t\t </div>
\t\t\t\t\t";
        }
        // line 315
        echo "\t\t\t\t\t

\t\t\t\t";
        // line 317
        if ((isset($context["discounts"]) ? $context["discounts"] : null)) {
            echo " 
\t\t\t\t\t<ul class=\"list-unstyled text-success\">
\t\t\t\t\t";
            // line 319
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["discounts"]) ? $context["discounts"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["discount"]) {
                echo " 
\t\t\t\t\t\t<li><strong>";
                // line 320
                echo $this->getAttribute($context["discount"], "quantity", array());
                echo " ";
                echo (isset($context["text_discount"]) ? $context["text_discount"] : null);
                echo " ";
                echo $this->getAttribute($context["discount"], "price", array());
                echo "</strong> </li>
\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['discount'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 322
            echo "\t\t\t\t\t</ul>
\t\t\t\t";
        }
        // line 323
        echo " 
\t\t\t\t
\t\t\t   
                \t<div class=\"col-md-12\">
\t\t\t\t\t    <div class=\"product-storage\">
\t\t\t\t\t        <h3>Storage</h3>
\t\t\t\t\t        <ul>
\t\t\t\t\t            <li><strong>8GB+128GB</strong></li>
\t\t\t\t\t            <li><strong>8GB+256GB</strong></li>
\t\t\t\t\t            <li><strong>8GB+512GB</strong></li>
\t\t\t\t\t        </ul>
\t\t\t\t\t    </div>
\t\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-12 pl-0\">
\t\t\t\t    
\t\t\t\t     
\t\t\t\t\t    <div class=\"product-color\">
\t\t\t\t\t        <h3>Color:</h3>
\t\t\t\t\t        <div id=\"color-1\" class=\"product-color-change tabcontent product-color-show\">
\t\t\t\t\t            <span>White</span>
\t\t\t\t\t        </div>
\t\t\t\t\t        <div id=\"color-2\" class=\"product-color-change tabcontent color2\">
\t\t\t\t\t            <span>Black</span>
\t\t\t\t\t        </div>
\t\t\t\t\t        <div id=\"color-3\" class=\"product-color-change tabcontent color3\">
\t\t\t\t\t            <span>Green</span>
\t\t\t\t\t        </div>
\t\t\t\t\t        <div id=\"color-4\" class=\"product-color-change tabcontent color4\">
\t\t\t\t\t            <span>Blue</span>
\t\t\t\t\t        </div>
\t\t\t\t\t        <ul>
\t\t\t\t\t            <li><div class=\"hover-color hover-color1\"><span class=\"product-hover-color\">White</span></div><button class=\"tablinks\"  onclick=\"openCity(event, 'color-1')\"><img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/product-color1.jpg\"></button><strong>White</strong></li>
\t\t\t\t\t            <li><div class=\"hover-color hover-color2\"><span class=\"product-hover-color\">Black</span></div><button  class=\"tablinks\" onclick=\"openCity(event, 'color-2')\"><img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/product-color2.jpg\"></button><strong>Black</strong></li>
\t\t\t\t\t            <li><div class=\"hover-color hover-color3\"><span class=\"product-hover-color\">Green</span></div><button  class=\"tablinks\" onclick=\"openCity(event, 'color-3')\"><img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/product-color3.jpg\"></button><strong>Green</strong></li>
\t\t\t\t\t            <li><div class=\"hover-color hover-color4\"><span class=\"product-hover-color\">Blue</span></div><button  class=\"tablinks\" onclick=\"openCity(event, 'color-4')\"><img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/product-color4.jpg\"></button><strong>Blue</strong></li>
\t\t\t\t\t        </ul>
\t\t\t\t\t    </div>
\t\t\t\t\t</div>
\t\t\t\t
\t\t\t\t
\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t<div class=\"Product-offers\">
\t\t\t\t\t    <h3>Available Offers</h3>
\t\t\t\t\t    <ul>
\t\t\t\t\t        <li><i class=\"fa fa-percent\"></i><strong>Bank Offer</strong>flat 30% discount</li>
\t\t\t\t\t        <li><i class=\"fa fa-percent\"></i><strong>Bank Offer</strong>5% offer in Axis bank</li>
\t\t\t\t\t    </ul>
\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t    <div class=\"product-exchange\">
\t\t\t\t\t        <a href=\"#\">
\t\t\t\t\t        <i class=\"fa fa-exchange\"></i>
\t\t\t\t\t        <span>With exchange<strong>Up to &#x20B9; 14000</strong> <i class=\"fa fa-angle-right\"></i></span>
\t\t\t\t\t        
\t\t\t\t\t        </a>
\t\t\t\t\t    </div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t    <div class=\"product-exchange product-emi\">
\t\t\t\t\t        <a href=\"#\">
\t\t\t\t\t        <i class=\"fa fa-percent\"></i>
\t\t\t\t\t        <span>No cost EMI @<strong> &#x20B9; 4000/month</strong> <i class=\"fa fa-angle-right\"></i></span>
\t\t\t\t\t        
\t\t\t\t\t        </a>
\t\t\t\t\t    </div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t<div class=\"product-delivery\">
\t\t\t\t\t    <div class=\"delivery-box\">
\t\t\t\t\t    <span>Delivery</span>
\t\t\t\t\t   
\t\t\t\t\t    <div class=\"pincode\">
\t\t\t\t\t        <span><i class=\"fa fa-map-marker\"></i></span> 
\t\t\t\t\t        <input type=\"text\" id=\"checktext\" maxlength=\"6\" placeholder=\"Enter Delivery Pincode\" name=\"pincode\" value=\"";
        // line 397
        echo (isset($context["customer_pincode"]) ? $context["customer_pincode"] : null);
        echo "\">
\t\t\t\t\t        <strong><a id=\"verify\" onclick=\"handler(";
        // line 398
        echo (isset($context["product_id"]) ? $context["product_id"] : null);
        echo ")\">";
        if ((isset($context["customer_pincode"]) ? $context["customer_pincode"] : null)) {
            echo " Change ";
        } else {
            echo " Check ";
        }
        echo "</a></strong><span id=\"pre_loader\"> <img src=\"https://s1.poorvikamobile.com/image/data/poorvika_mobile_five/loader.gif\" alt=\"Loading\"></span>
\t\t\t\t\t   
\t\t\t\t\t   </div>
\t\t\t\t\t   </div>
\t\t\t\t\t   
\t\t\t\t\t    <ul>
\t\t\t\t\t        <li><img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/deliver-img1.png\"><span>2 hours</span></li>
\t\t\t\t\t        <li><img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/deliver-img2.png\"><span>Regular</span></li>
\t\t\t\t\t        <li><img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/deliver-img3.png\"><span>Pickup@store</span></li>
\t\t\t\t\t   </ul>
\t\t\t\t\t    
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"available\" style=\"display:none\">
\t\t\t\t\t\t<div class=\"delivery_note hidden\">
\t\t\t\t\t\t\t<div class=\"d_note\"><span>Available Shipping and Payments for</span> <span id=\"available-text\"></span></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t<div id=\"pin_avilability_ship\" class=\"delivery_hrs\">
\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t
\t\t\t\t\t<div id=\"prod_specs\" class=\"col-md-12 pr-0 pl-0\">
\t\t\t\t\t    <div class=\"specs-block\">
\t\t\t\t\t    <div class=\"product-specification\">
\t\t\t\t\t         <h2><img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/product-spec.png\">Specifications</h2>
\t\t\t\t\t    </div>
\t\t\t\t\t    
\t\t\t\t\t<div class=\"product-box-desc\">
\t\t\t\t\t<div class=\"inner-box-desc\">
\t\t\t            
\t\t\t            ";
        // line 429
        if ((isset($context["attribute_groups"]) ? $context["attribute_groups"] : null)) {
            // line 430
            echo "\t\t\t\t\t\t\t
\t\t\t              \t<ul class=\"product-property-list util-clearfix\">
\t\t\t\t                ";
            // line 432
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["attribute_groups"]) ? $context["attribute_groups"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["attribute_group"]) {
                // line 433
                echo "\t\t\t\t                    <h3 class=\"product-property-title\" > ";
                echo $this->getAttribute($context["attribute_group"], "name", array());
                echo "</h3>
\t\t\t\t                \t
\t\t\t\t\t                ";
                // line 435
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["attribute_group"], "attribute", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["attribute"]) {
                    // line 436
                    echo "\t\t\t\t\t                <li class=\"property-item\">
\t\t\t\t\t                    <div class=\"model ptb-10 pl-8\"><span>";
                    // line 437
                    echo $this->getAttribute($context["attribute"], "name", array());
                    echo " </span> <span class=\"attrcolor\">";
                    echo $this->getAttribute($context["attribute"], "text", array());
                    echo "</span></div>
\t\t\t\t\t                  
\t\t\t\t\t                </li>
\t\t\t\t\t                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attribute'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 441
                echo "\t\t\t\t                 \t
\t\t\t\t                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attribute_group'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 443
            echo "\t\t\t              \t</ul>
\t\t\t            ";
        }
        // line 445
        echo "\t\t\t\t\t\t\t
\t\t\t\t\t</div>\t
\t\t\t\t\t
\t\t\t\t\t

\t\t\t\t\t";
        // line 450
        if ($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "product_enablesold"), "method")) {
            // line 451
            echo "\t\t\t\t\t<div class=\"inner-box-sold \">
\t\t\t\t\t\t<div class=\"viewed\"><span>";
            // line 452
            echo (isset($context["text_viewed"]) ? $context["text_viewed"] : null);
            echo "</span> <span class=\"label label-primary\">";
            echo (isset($context["viewed"]) ? $context["viewed"] : null);
            echo "</span></div>\t
\t\t\t\t\t\t";
            // line 453
            if ((isset($context["sold"]) ? $context["sold"] : null)) {
                // line 454
                echo "\t\t\t\t\t\t<div class=\"sold\"><span>";
                echo (isset($context["text_sold_ready"]) ? $context["text_sold_ready"] : null);
                echo "</span> <span class=\"label label-success\"> ";
                echo (isset($context["sold"]) ? $context["sold"] : null);
                echo " </span></div>\t
\t\t\t\t\t\t";
            }
            // line 456
            echo "\t\t\t\t\t</div>\t
\t\t\t\t\t";
        }
        // line 458
        echo "\t\t\t\t\t
\t\t\t\t\t

\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t
\t\t\t\t</div>
\t\t\t\t
\t\t\t\t\t

\t\t\t\t";
        // line 469
        echo "\t\t\t\t";
        if (($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "countdown_status"), "method") && (isset($context["special_end_date"]) ? $context["special_end_date"] : null))) {
            // line 470
            echo "\t\t\t\t\t";
            $this->loadTemplate(((isset($context["theme_directory"]) ? $context["theme_directory"] : null) . "/template/soconfig/countdown.twig"), "so-destino/template/product/product.twig", 470)->display(array_merge($context, array("product" => (isset($context["product"]) ? $context["product"] : null), "special_end_date" => (isset($context["special_end_date"]) ? $context["special_end_date"] : null))));
            // line 471
            echo "\t\t\t\t";
        }
        // line 472
        echo "\t\t\t\t
\t\t\t\t
\t\t\t\t<div id=\"product\">\t
\t\t\t\t\t";
        // line 475
        if ((isset($context["options"]) ? $context["options"] : null)) {
            echo " 
\t\t\t\t\t<h3>";
            // line 476
            echo (isset($context["text_option"]) ? $context["text_option"] : null);
            echo "</h3>
 
 ";
            // line 478
            if ((((isset($context["option_data"]) ? $context["option_data"] : null) && $this->getAttribute((isset($context["option_data"]) ? $context["option_data"] : null), "product_option_value", array(), "any", true, true)) && $this->getAttribute((isset($context["option_data"]) ? $context["option_data"] : null), "product_option_value", array()))) {
                echo " 
 <ul id=\"so-colorswatch-selector-";
                // line 479
                echo (isset($context["product_id"]) ? $context["product_id"] : null);
                echo "\" class='so-colorswatch-productpage-icons'> 
 ";
                // line 480
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["option_data"]) ? $context["option_data"] : null), "product_option_value", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["option_value"]) {
                    echo " 
 <li class=\"option-item\"> 
 <a class=\"\" 
 data-product-option-value-id=\"";
                    // line 483
                    echo $this->getAttribute($context["option_value"], "product_option_value_id", array());
                    echo "\" 
 data-option-value-id=\"";
                    // line 484
                    echo $this->getAttribute($context["option_value"], "option_value_id", array());
                    echo "\" 
 data-color-image=\"";
                    // line 485
                    echo $this->getAttribute($context["option_value"], "color_image", array());
                    echo "\" 
 data-color-thumb-image=\"";
                    // line 486
                    echo $this->getAttribute($context["option_value"], "color_thumb_image", array());
                    echo "\" 
 style=\"width: ";
                    // line 487
                    echo (isset($context["width_product_page"]) ? $context["width_product_page"] : null);
                    echo "px; height: ";
                    echo (isset($context["height_product_page"]) ? $context["height_product_page"] : null);
                    echo "px; background-image: url('";
                    echo $this->getAttribute($context["option_value"], "image", array());
                    echo "')\"> 
 </a> 
 </li> 
 ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option_value'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 490
                echo " 
 <li class=\"selected-option\"><span></span></li> 
 </ul> 
 <script type=\"text/javascript\"> 
 var \$window_width = \$(window).width(); 
 var ProductOptionId = '";
                // line 495
                echo (isset($context["product_option_id"]) ? $context["product_option_id"] : null);
                echo "'; 
 var default_image = \$('.large-image img').attr('src'); 
 jQuery(document).ready(function(\$) { 
 \$('#input-option";
                // line 498
                echo (isset($context["product_option_id"]) ? $context["product_option_id"] : null);
                echo "').parent().hide(); 
 
 \$('#input-option";
                // line 500
                echo (isset($context["product_option_id"]) ? $context["product_option_id"] : null);
                echo " option').each(function(){ 
 var text = \$(this).text().replace(/\\s{2,}/g, ' '); 
 var val = \$(this).attr('value'); 
 \$('.so-colorswatch-productpage-icons li a').each(function(index, el){ 
 if(\$(el).data('product-option-value-id')== val){ 
 \$(el).attr('title', text); 
 } 
 }) 
 }) 
 
 ";
                // line 510
                if (((isset($context["colorswatch_type"]) ? $context["colorswatch_type"] : null) == "click")) {
                    echo " 
 \$(document).on('click', '.so-colorswatch-productpage-icons li.option-item', function(e){ 
 e.preventDefault(); 
 var option_value_id = \$(this).children('a').data('product-option-value-id'); 
 var option_id = \$(this).children('a').data('option-value-id'); 
 
 if (\$(this).hasClass('checked')) { 
 \$('.so-colorswatch-productpage-icons li.option-item').removeClass('checked'); 
 \$(this).removeClass('checked'); 
 \$('#input-option";
                    // line 519
                    echo (isset($context["product_option_id"]) ? $context["product_option_id"] : null);
                    echo "').val('').trigger('change'); 
 \$('.so-colorswatch-productpage-icons li.selected-option > span').html(''); 
 
 \$('.large-image img').attr('src', default_image); 
 } 
 else { 
 \$('.so-colorswatch-productpage-icons li.option-item').removeClass('checked'); 
 \$(this).removeClass('checked').addClass('checked'); 
 \$('#input-option";
                    // line 527
                    echo (isset($context["product_option_id"]) ? $context["product_option_id"] : null);
                    echo "').val(option_value_id).trigger('change'); 
 \$('.so-colorswatch-productpage-icons li.selected-option > span').html(\$(this).children('a').attr('title')); 
 
 if (\$(this).children('a').data('color-image') != '') { 
 \$('.large-image img').attr('src', \$(this).children('a').data('color-image')); 
 } 
 else { 
 \$('.large-image img').attr('src', default_image); 
 } 
 
 \$('#thumb-slider a.thumbnail').removeClass('active'); 
 } 
 }) 
 ";
                } else {
                    // line 540
                    echo " 
 if (\$window_width > 1199) { 
 \$('.so-colorswatch-productpage-icons li.option-item').hover(function(e){ 
 e.preventDefault(); 
 var option_value_id = \$(this).children('a').data('product-option-value-id'); 
 var option_id = \$(this).children('a').data('option-value-id'); 
 
 \$('.so-colorswatch-productpage-icons li.option-item').removeClass('checked'); 
 if (\$(this).hasClass('checked')) { 
 \$(this).removeClass('checked'); 
 \$('#input-option";
                    // line 550
                    echo (isset($context["product_option_id"]) ? $context["product_option_id"] : null);
                    echo "').val('').trigger('change'); 
 \$('.large-image img').attr('src', default_image); 
 
 } 
 else { 
 \$(this).removeClass('checked').addClass('checked'); 
 \$('#input-option";
                    // line 556
                    echo (isset($context["product_option_id"]) ? $context["product_option_id"] : null);
                    echo "').val(option_value_id).trigger('change'); 
 \$('.so-colorswatch-productpage-icons li.selected-option > span').html(\$(this).children('a').attr('title')); 
 
 if (\$(this).children('a').data('color-image') != '') { 
 \$('.large-image img').attr('src', \$(this).children('a').data('color-image')); 
 } 
 else { 
 \$('.large-image img').attr('src', default_image); 
 } 
 \$('#thumb-slider a.thumbnail').removeClass('active'); 
 } 
 }); 
 } 
 else { 
 \$(document).on('click', '.so-colorswatch-productpage-icons li.option-item', function(e){ 
 e.preventDefault(); 
 var option_value_id = \$(this).children('a').data('product-option-value-id'); 
 var option_id = \$(this).children('a').data('option-value-id'); 
 
 \$('.so-colorswatch-productpage-icons li.option-item').removeClass('checked'); 
 if (\$(this).hasClass('checked')) { 
 \$(this).removeClass('checked'); 
 \$('#input-option";
                    // line 578
                    echo (isset($context["product_option_id"]) ? $context["product_option_id"] : null);
                    echo "').val('').trigger('change'); 
 \$('.large-image img').attr('src', default_image); 
 
 } 
 else { 
 \$(this).removeClass('checked').addClass('checked'); 
 \$('#input-option";
                    // line 584
                    echo (isset($context["product_option_id"]) ? $context["product_option_id"] : null);
                    echo "').val(option_value_id).trigger('change'); 
 \$('.so-colorswatch-productpage-icons li.selected-option > span').html(\$(this).children('a').attr('title')); 
 
 if (\$(this).children('a').data('color-image') != '') { 
 \$('.large-image img').attr('src', \$(this).children('a').data('color-image')); 
 } 
 else { 
 \$('.large-image img').attr('src', default_image); 
 } 
 \$('#thumb-slider a.thumbnail').removeClass('active'); 
 } 
 }) 
 } 
 ";
                }
                // line 597
                echo " 
 }) 
 </script> 
 ";
            }
            // line 600
            echo " 
 
\t\t\t\t\t";
            // line 602
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["options"]) ? $context["options"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["option"]) {
                // line 603
                echo "\t\t\t\t\t\t
\t\t\t\t\t\t";
                // line 604
                if (($this->getAttribute($context["option"], "type", array()) == "select")) {
                    // line 605
                    echo "\t\t\t\t\t\t<div class=\"form-group";
                    if ($this->getAttribute($context["option"], "required", array())) {
                        echo " required ";
                    }
                    echo "\">
\t\t\t\t\t\t\t<label class=\"control-label\" for=\"input-option";
                    // line 606
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "\">";
                    echo $this->getAttribute($context["option"], "name", array());
                    echo "</label>
\t\t\t\t\t\t\t<select name=\"option[";
                    // line 607
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "]\" id=\"input-option";
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "\" class=\"form-control width50\">
\t\t\t\t\t\t\t\t<option value=\"\">";
                    // line 608
                    echo (isset($context["text_select"]) ? $context["text_select"] : null);
                    echo "</option>
\t\t\t\t\t\t\t";
                    // line 609
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["option"], "product_option_value", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["option_value"]) {
                        // line 610
                        echo "\t\t\t\t\t\t\t\t<option value=\"";
                        echo $this->getAttribute($context["option_value"], "product_option_value_id", array());
                        echo "\">";
                        echo $this->getAttribute($context["option_value"], "name", array());
                        echo "
\t\t\t\t\t\t\t\t";
                        // line 611
                        if ($this->getAttribute($context["option_value"], "price", array())) {
                            // line 612
                            echo "\t\t\t\t\t\t\t\t\t(";
                            echo $this->getAttribute($context["option_value"], "price_prefix", array());
                            echo $this->getAttribute($context["option_value"], "price", array());
                            echo ")
\t\t\t\t\t\t\t\t";
                        }
                        // line 614
                        echo "\t\t\t\t\t\t\t\t</option>
\t\t\t\t\t\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option_value'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 616
                    echo "\t\t\t\t\t\t  </select>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
                }
                // line 619
                echo "\t\t\t\t\t\t
\t\t\t\t\t\t";
                // line 620
                if (($this->getAttribute($context["option"], "type", array()) == "radio")) {
                    // line 621
                    echo "\t\t\t\t\t\t<div class=\"form-group";
                    if ($this->getAttribute($context["option"], "required", array())) {
                        echo " required ";
                    }
                    echo "\">
\t\t\t\t\t\t  \t<label class=\"control-label\">";
                    // line 622
                    echo $this->getAttribute($context["option"], "name", array());
                    echo "</label>
\t\t\t\t\t\t\t<div id=\"input-option";
                    // line 623
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "\">
\t\t\t\t\t\t\t\t";
                    // line 624
                    $context["radio_style"] = $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "radio_style"), "method");
                    // line 625
                    echo "\t\t\t\t\t\t\t\t";
                    $context["radio_type"] = (((isset($context["radio_style"]) ? $context["radio_style"] : null)) ? (" radio-type-button") : (""));
                    // line 626
                    echo "
\t\t\t\t\t\t\t\t";
                    // line 627
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["option"], "product_option_value", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["option_value"]) {
                        echo " 
\t\t\t\t\t\t\t\t";
                        // line 628
                        $context["radio_image"] = (($this->getAttribute($context["option_value"], "image", array())) ? ("option_image") : (""));
                        echo " 
\t\t\t\t\t\t\t\t";
                        // line 629
                        $context["radio_price"] = (((isset($context["radio_style"]) ? $context["radio_style"] : null)) ? (($this->getAttribute($context["option_value"], "price_prefix", array()) . $this->getAttribute($context["option_value"], "price", array()))) : (""));
                        echo " 
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t<div class=\"radio ";
                        // line 631
                        echo ((isset($context["radio_image"]) ? $context["radio_image"] : null) . (isset($context["radio_type"]) ? $context["radio_type"] : null));
                        echo "\">
\t\t\t\t\t\t\t\t\t\t<label>\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<input type=\"radio\" name=\"option[";
                        // line 633
                        echo $this->getAttribute($context["option"], "product_option_id", array());
                        echo "]\" value=\"";
                        echo $this->getAttribute($context["option_value"], "product_option_value_id", array());
                        echo "\" />
\t\t\t\t\t\t\t\t\t\t\t<span class=\"option-content-box\" data-title=\"";
                        // line 634
                        echo $this->getAttribute($context["option_value"], "name", array());
                        echo " ";
                        echo (isset($context["radio_price"]) ? $context["radio_price"] : null);
                        echo "\" data-toggle='tooltip'>
\t\t\t\t\t\t\t\t\t\t\t\t";
                        // line 635
                        if ($this->getAttribute($context["option_value"], "image", array())) {
                            echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"";
                            // line 636
                            echo $this->getAttribute($context["option_value"], "image", array());
                            echo " \" alt=\"";
                            echo $this->getAttribute($context["option_value"], "name", array());
                            echo "  ";
                            echo (isset($context["radio_price"]) ? $context["radio_price"] : null);
                            echo "\" /> 
\t\t\t\t\t\t\t\t\t\t\t\t";
                        }
                        // line 637
                        echo " 
\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"option-name\">";
                        // line 638
                        echo $this->getAttribute($context["option_value"], "name", array());
                        echo " </span>
\t\t\t\t\t\t\t\t\t\t\t\t";
                        // line 639
                        if (($this->getAttribute($context["option_value"], "price", array()) && ((isset($context["radio_style"]) ? $context["radio_style"] : null) != "1"))) {
                            echo " (";
                            echo $this->getAttribute($context["option_value"], "price_prefix", array());
                            echo " ";
                            echo $this->getAttribute($context["option_value"], "price", array());
                            echo " )";
                        }
                        echo " 
\t\t\t\t\t\t\t\t\t\t\t  
\t\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option_value'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 644
                    echo "\t
\t\t\t\t\t\t\t\t 
\t\t\t\t\t\t\t\t";
                    // line 646
                    if ((isset($context["radio_style"]) ? $context["radio_style"] : null)) {
                        echo " 
\t\t\t\t\t\t\t\t<script type=\"text/javascript\">
\t\t\t\t\t\t\t\t\t \$(document).ready(function(){
\t\t\t\t\t\t\t\t\t\t  \$('#input-option";
                        // line 649
                        echo $this->getAttribute($context["option"], "product_option_id", array());
                        echo " ').on('click', 'span', function () {
\t\t\t\t\t\t\t\t\t\t\t   \$('#input-option";
                        // line 650
                        echo $this->getAttribute($context["option"], "product_option_id", array());
                        echo "  span').removeClass(\"active\");
\t\t\t\t\t\t\t\t\t\t\t   \$(this).toggleClass(\"active\");
\t\t\t\t\t\t\t\t\t\t  });
\t\t\t\t\t\t\t\t\t });
\t\t\t\t\t\t\t\t</script>
\t\t\t\t\t\t\t\t";
                    }
                    // line 655
                    echo " 

\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
                }
                // line 660
                echo "
\t\t\t\t\t\t";
                // line 661
                if (($this->getAttribute($context["option"], "type", array()) == "checkbox")) {
                    // line 662
                    echo "\t\t\t\t\t\t<div class=\"form-group";
                    if ($this->getAttribute($context["option"], "required", array())) {
                        echo " required ";
                    }
                    echo "\">
\t\t\t\t\t\t  \t<label class=\"control-label\">";
                    // line 663
                    echo $this->getAttribute($context["option"], "name", array());
                    echo "</label>
\t\t\t\t\t\t  \t<div id=\"input-option";
                    // line 664
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "\">
\t\t\t\t\t\t\t\t";
                    // line 665
                    $context["radio_style"] = $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "radio_style"), "method");
                    // line 666
                    echo "\t\t\t\t\t\t\t\t";
                    $context["radio_type"] = (((isset($context["radio_style"]) ? $context["radio_style"] : null)) ? (" radio-type-button") : (""));
                    // line 667
                    echo "
\t\t\t\t\t\t\t\t";
                    // line 668
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["option"], "product_option_value", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["option_value"]) {
                        echo " 
\t\t\t\t\t\t\t\t";
                        // line 669
                        $context["radio_image"] = (($this->getAttribute($context["option_value"], "image", array())) ? ("option_image") : (""));
                        echo " 
\t\t\t\t\t\t\t\t";
                        // line 670
                        $context["radio_price"] = (((isset($context["radio_style"]) ? $context["radio_style"] : null)) ? (($this->getAttribute($context["option_value"], "price_prefix", array()) . $this->getAttribute($context["option_value"], "price", array()))) : (""));
                        echo " 
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t<div class=\"checkbox  ";
                        // line 672
                        echo ((isset($context["radio_image"]) ? $context["radio_image"] : null) . (isset($context["radio_type"]) ? $context["radio_type"] : null));
                        echo "\">
\t\t\t\t\t\t\t\t\t\t<label>
\t\t\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" name=\"option[";
                        // line 674
                        echo $this->getAttribute($context["option"], "product_option_id", array());
                        echo "][]\" value=\"";
                        echo $this->getAttribute($context["option_value"], "product_option_value_id", array());
                        echo "\" />
\t\t\t\t\t\t\t\t\t\t\t<span class=\"option-content-box\" data-title=\"";
                        // line 675
                        echo $this->getAttribute($context["option_value"], "name", array());
                        echo " ";
                        echo (isset($context["radio_price"]) ? $context["radio_price"] : null);
                        echo "\" data-toggle='tooltip'>
\t\t\t\t\t\t\t\t\t\t\t\t";
                        // line 676
                        if ($this->getAttribute($context["option_value"], "image", array())) {
                            echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"";
                            // line 677
                            echo $this->getAttribute($context["option_value"], "image", array());
                            echo " \" alt=\"";
                            echo $this->getAttribute($context["option_value"], "name", array());
                            echo "  ";
                            echo (isset($context["radio_price"]) ? $context["radio_price"] : null);
                            echo "\" /> 
\t\t\t\t\t\t\t\t\t\t\t\t";
                        }
                        // line 678
                        echo " 

\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"option-name\">";
                        // line 680
                        echo $this->getAttribute($context["option_value"], "name", array());
                        echo " </span>
\t\t\t\t\t\t\t\t\t\t\t\t";
                        // line 681
                        if (($this->getAttribute($context["option_value"], "price", array()) && ((isset($context["radio_style"]) ? $context["radio_style"] : null) != "1"))) {
                            echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t(";
                            // line 682
                            echo $this->getAttribute($context["option_value"], "price_prefix", array());
                            echo " ";
                            echo $this->getAttribute($context["option_value"], "price", array());
                            echo " )
\t\t\t\t\t\t\t\t\t\t\t\t";
                        }
                        // line 683
                        echo " 
\t\t\t\t\t\t\t\t\t\t\t  
\t\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option_value'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 688
                    echo "\t
\t\t\t\t\t\t\t\t 
\t\t\t\t\t\t\t\t";
                    // line 690
                    if ((isset($context["radio_style"]) ? $context["radio_style"] : null)) {
                        echo " 
\t\t\t\t\t\t\t\t<script type=\"text/javascript\">
\t\t\t\t\t\t\t\t\t \$(document).ready(function(){
\t\t\t\t\t\t\t\t\t\t  \$('#input-option";
                        // line 693
                        echo $this->getAttribute($context["option"], "product_option_id", array());
                        echo " ').on('click', 'span', function () {
\t\t\t\t\t\t\t\t\t\t\t   \$(this).toggleClass(\"active\");
\t\t\t\t\t\t\t\t\t\t  });
\t\t\t\t\t\t\t\t\t });
\t\t\t\t\t\t\t\t</script>
\t\t\t\t\t\t\t\t";
                    }
                    // line 698
                    echo " 

\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
                }
                // line 703
                echo "
\t\t\t\t\t\t";
                // line 704
                if (($this->getAttribute($context["option"], "type", array()) == "text")) {
                    // line 705
                    echo "\t\t\t\t\t\t<div class=\"form-group";
                    if ($this->getAttribute($context["option"], "required", array())) {
                        echo " required ";
                    }
                    echo "\">
\t\t\t\t\t\t  <label class=\"control-label\" for=\"input-option";
                    // line 706
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "\">";
                    echo $this->getAttribute($context["option"], "name", array());
                    echo "</label>
\t\t\t\t\t\t  <input type=\"text\" name=\"option[";
                    // line 707
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "]\" value=\"";
                    echo $this->getAttribute($context["option"], "value", array());
                    echo "\" placeholder=\"";
                    echo $this->getAttribute($context["option"], "name", array());
                    echo "\" id=\"input-option";
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "\" class=\"form-control\" />
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
                }
                // line 710
                echo "
\t\t\t\t\t\t";
                // line 711
                if (($this->getAttribute($context["option"], "type", array()) == "textarea")) {
                    // line 712
                    echo "\t\t\t\t\t\t<div class=\"form-group";
                    if ($this->getAttribute($context["option"], "required", array())) {
                        echo " required ";
                    }
                    echo "\">
\t\t\t\t\t\t  <label class=\"control-label\" for=\"input-option";
                    // line 713
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "\">";
                    echo $this->getAttribute($context["option"], "name", array());
                    echo "</label>
\t\t\t\t\t\t  <textarea name=\"option[";
                    // line 714
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "]\" rows=\"5\" placeholder=\"";
                    echo $this->getAttribute($context["option"], "name", array());
                    echo "\" id=\"input-option";
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "\" class=\"form-control\">";
                    echo $this->getAttribute($context["option"], "value", array());
                    echo "</textarea>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
                }
                // line 717
                echo "
\t\t\t\t\t\t";
                // line 718
                if (($this->getAttribute($context["option"], "type", array()) == "file")) {
                    // line 719
                    echo "\t\t\t\t\t\t<div class=\"form-group";
                    if ($this->getAttribute($context["option"], "required", array())) {
                        echo " required ";
                    }
                    echo "\">
\t\t\t\t\t\t  <label class=\"control-label\">";
                    // line 720
                    echo $this->getAttribute($context["option"], "name", array());
                    echo "</label>
\t\t\t\t\t\t  <button type=\"button\" id=\"button-upload";
                    // line 721
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "\" data-loading-text=\"";
                    echo (isset($context["text_loading"]) ? $context["text_loading"] : null);
                    echo "\" class=\"btn btn-default btn-block\"><i class=\"fa fa-upload\"></i> ";
                    echo (isset($context["button_upload"]) ? $context["button_upload"] : null);
                    echo "</button>
\t\t\t\t\t\t  <input type=\"hidden\" name=\"option[";
                    // line 722
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "]\" value=\"\" id=\"input-option";
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "\" />
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
                }
                // line 725
                echo "
\t\t\t\t\t\t";
                // line 726
                if (($this->getAttribute($context["option"], "type", array()) == "date")) {
                    // line 727
                    echo "\t\t\t\t\t\t<div class=\"form-group";
                    if ($this->getAttribute($context["option"], "required", array())) {
                        echo " required ";
                    }
                    echo "\">
\t\t\t\t\t\t  <label class=\"control-label\" for=\"input-option";
                    // line 728
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "\">";
                    echo $this->getAttribute($context["option"], "name", array());
                    echo "</label>
\t\t\t\t\t\t  <div class=\"input-group date\">
\t\t\t\t\t\t\t<input type=\"text\" name=\"option[";
                    // line 730
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "]\" value=\"";
                    echo $this->getAttribute($context["option"], "value", array());
                    echo "\" data-date-format=\"YYYY-MM-DD\" id=\"input-option";
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t<span class=\"input-group-btn\">
\t\t\t\t\t\t\t<button class=\"btn btn-default\" type=\"button\"><i class=\"fa fa-calendar\"></i></button>
\t\t\t\t\t\t\t</span></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
                }
                // line 736
                echo "
\t\t\t\t\t\t";
                // line 737
                if (($this->getAttribute($context["option"], "type", array()) == "datetime")) {
                    // line 738
                    echo "\t\t\t\t\t\t<div class=\"form-group";
                    if ($this->getAttribute($context["option"], "required", array())) {
                        echo " required ";
                    }
                    echo "\">
\t\t\t\t\t\t  <label class=\"control-label\" for=\"input-option";
                    // line 739
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "\">";
                    echo $this->getAttribute($context["option"], "name", array());
                    echo "</label>
\t\t\t\t\t\t  <div class=\"input-group datetime\">
\t\t\t\t\t\t\t<input type=\"text\" name=\"option[";
                    // line 741
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "]\" value=\"";
                    echo $this->getAttribute($context["option"], "value", array());
                    echo "\" data-date-format=\"YYYY-MM-DD HH:mm\" id=\"input-option";
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t<span class=\"input-group-btn\">
\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-default\"><i class=\"fa fa-calendar\"></i></button>
\t\t\t\t\t\t\t</span></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
                }
                // line 747
                echo "\t\t\t\t\t\t
\t\t\t\t\t\t";
                // line 748
                if (($this->getAttribute($context["option"], "type", array()) == "time")) {
                    // line 749
                    echo "\t\t\t\t\t\t<div class=\"form-group";
                    if ($this->getAttribute($context["option"], "required", array())) {
                        echo " required ";
                    }
                    echo "\">
\t\t\t\t\t\t\t<label class=\"control-label\" for=\"input-option";
                    // line 750
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "\">";
                    echo $this->getAttribute($context["option"], "name", array());
                    echo "</label>
\t\t\t\t\t\t\t<div class=\"input-group time\">
\t\t\t\t\t\t\t<input type=\"text\" name=\"option[";
                    // line 752
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "]\" value=\"";
                    echo $this->getAttribute($context["option"], "value", array());
                    echo "\" data-date-format=\"HH:mm\" id=\"input-option";
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t<span class=\"input-group-btn\">
\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-default\"><i class=\"fa fa-calendar\"></i></button>
\t\t\t\t\t\t\t</span></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
                }
                // line 758
                echo "\t\t\t\t\t\t
\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 760
            echo "\t\t\t\t\t";
        }
        // line 761
        echo "
\t\t\t\t\t<div class=\"box-cart clearfix form-group\">
\t\t\t\t\t\t";
        // line 763
        if ((isset($context["recurrings"]) ? $context["recurrings"] : null)) {
            // line 764
            echo "\t\t\t\t\t\t<h3>";
            echo (isset($context["text_payment_recurring"]) ? $context["text_payment_recurring"] : null);
            echo "</h3>
\t\t\t\t\t\t<div class=\"form-group required\">
\t\t\t\t\t\t\t<select name=\"recurring_id\" class=\"form-control\">
\t\t\t\t\t\t\t<option value=\"\">";
            // line 767
            echo (isset($context["text_select"]) ? $context["text_select"] : null);
            echo "</option>
\t\t\t\t\t\t\t";
            // line 768
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["recurrings"]) ? $context["recurrings"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["recurring"]) {
                // line 769
                echo "\t\t\t\t\t\t\t<option value=\"";
                echo $this->getAttribute($context["recurring"], "recurring_id", array());
                echo "\">";
                echo $this->getAttribute($context["recurring"], "name", array());
                echo "</option>
\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['recurring'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 771
            echo "\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t  <div class=\"help-block\" id=\"recurring-description\"></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
        }
        // line 775
        echo "\t\t\t\t\t  
\t\t\t\t\t\t<div class=\"form-group box-info-product\">
\t\t\t\t\t\t\t<div class=\"option quantity\">
\t\t\t\t\t\t\t\t<div class=\"input-group quantity-control\">
\t\t\t\t\t\t\t\t\t  <span class=\"input-group-addon product_quantity_down fa fa-minus\"></span>
\t\t\t\t\t\t\t\t\t  <input class=\"form-control\" type=\"text\" name=\"quantity\" value=\"";
        // line 780
        echo (isset($context["minimum"]) ? $context["minimum"] : null);
        echo "\" />
\t\t\t\t\t\t\t\t\t  <input type=\"hidden\" name=\"product_id\" value=\"";
        // line 781
        echo (isset($context["product_id"]) ? $context["product_id"] : null);
        echo "\" />\t\t\t\t\t\t\t\t  
\t\t\t\t\t\t\t\t\t  <span class=\"input-group-addon product_quantity_up fa fa-plus\"></span>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"detail-action\">
\t\t\t\t\t\t\t\t";
        // line 787
        echo "\t\t\t\t\t\t\t\t<div class=\"cart\"> 
 ";
        // line 788
        if (($this->getAttribute((isset($context["cfp_setting"]) ? $context["cfp_setting"] : null), "module_so_call_for_price_status", array()) && ((isset($context["price_0"]) ? $context["price_0"] : null) <= 0))) {
            echo " 
 ";
            // line 789
            if (($this->getAttribute((isset($context["cfp_setting"]) ? $context["cfp_setting"] : null), "module_so_call_for_price_hide_cart", array(), "any", true, true) && ($this->getAttribute((isset($context["cfp_setting"]) ? $context["cfp_setting"] : null), "module_so_call_for_price_hide_cart", array()) == "0"))) {
                echo " 
 ";
                // line 790
                if (($this->getAttribute((isset($context["cfp_setting"]) ? $context["cfp_setting"] : null), "module_so_call_for_price_replace_cart", array(), "any", true, true) && ($this->getAttribute((isset($context["cfp_setting"]) ? $context["cfp_setting"] : null), "module_so_call_for_price_replace_cart", array()) == "1"))) {
                    echo " 
 <input type=\"button\" value=\"";
                    // line 791
                    echo (isset($context["text_price_0"]) ? $context["text_price_0"] : null);
                    echo "\" data-fancybox data-type=\"ajax\" data-src=\"";
                    echo (isset($context["base"]) ? $context["base"] : null);
                    echo "index.php?route=extension/module/so_call_for_price&product_id=";
                    echo $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "product_id", array());
                    echo "\" data-loading-text=\"";
                    echo (isset($context["text_loading"]) ? $context["text_loading"] : null);
                    echo "\" class=\"btn btn-mega btn-lg callforprice\"> 
 ";
                } else {
                    // line 792
                    echo " 
 <input type=\"button\" value=\"";
                    // line 793
                    echo (isset($context["button_cart"]) ? $context["button_cart"] : null);
                    echo "\" data-loading-text=\"";
                    echo (isset($context["text_loading"]) ? $context["text_loading"] : null);
                    echo "\" class=\"btn btn-mega btn-lg\" style=\"cursor: default; background: #eee; color: #ccc; border: 1px solid #eee; text-shadow: none; box-shadow: none;\"> 
 ";
                }
                // line 794
                echo " 
 ";
            } else {
                // line 795
                echo " 
 ";
                // line 796
                if (($this->getAttribute((isset($context["cfp_setting"]) ? $context["cfp_setting"] : null), "module_so_call_for_price_replace_cart", array(), "any", true, true) && ($this->getAttribute((isset($context["cfp_setting"]) ? $context["cfp_setting"] : null), "module_so_call_for_price_replace_cart", array()) == "1"))) {
                    echo " 
 <input type=\"button\" value=\"";
                    // line 797
                    echo (isset($context["text_price_0"]) ? $context["text_price_0"] : null);
                    echo "\" data-fancybox data-type=\"ajax\" data-src=\"";
                    echo (isset($context["base"]) ? $context["base"] : null);
                    echo "index.php?route=extension/module/so_call_for_price&product_id=";
                    echo $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "product_id", array());
                    echo "\" data-loading-text=\"";
                    echo (isset($context["text_loading"]) ? $context["text_loading"] : null);
                    echo "\" class=\"btn btn-mega btn-lg \"> 
 ";
                }
                // line 798
                echo " 
 ";
            }
            // line 799
            echo " 
 ";
        } else {
            // line 800
            echo " 
 <input type=\"button\" value=\"";
            // line 801
            echo (isset($context["button_cart"]) ? $context["button_cart"] : null);
            echo "\" data-loading-text=\"";
            echo (isset($context["text_loading"]) ? $context["text_loading"] : null);
            echo "\" id=\"button-cart\" class=\"btn btn-mega btn-lg\" /> 
 ";
        }
        // line 802
        echo " 
 </div>
\t\t\t\t\t\t\t\t<div class=\"add-to-links wish_comp\">
\t\t\t\t\t\t\t\t\t<ul class=\"blank\">
\t\t\t\t\t\t\t\t\t\t<li class=\"wishlist\">
\t\t\t\t\t\t\t\t\t\t\t<a onclick=\"wishlist.add(";
        // line 807
        echo (isset($context["product_id"]) ? $context["product_id"] : null);
        echo ");\"><i class=\"fa fa-heart\"></i></a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li class=\"compare\">
\t\t\t\t\t\t\t\t\t\t\t<a onclick=\"compare.add(";
        // line 810
        echo (isset($context["product_id"]) ? $context["product_id"] : null);
        echo ");\"><i class=\"fa fa-retweet\"></i></a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t";
        // line 819
        if (((isset($context["minimum"]) ? $context["minimum"] : null) > 1)) {
            // line 820
            echo "\t\t\t\t\t\t\t<div class=\"alert alert-info\"><i class=\"fa fa-info-circle\"></i> ";
            echo (isset($context["text_minimum"]) ? $context["text_minimum"] : null);
            echo "</div>
\t\t\t\t\t\t";
        }
        // line 822
        echo "\t\t\t\t\t</div>

\t\t\t\t\t";
        // line 824
        if (($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "product_page_button"), "method") && $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "product_socialshare"), "method"))) {
            // line 825
            echo "\t\t\t\t\t<div class=\"form-group social-share clearfix\">
\t\t\t\t\t\t";
            // line 826
            echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "decode_entities", array(0 => $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "product_socialshare"), "method")), "method");
            echo "
\t\t\t\t\t</div>
\t\t\t\t\t";
        }
        // line 829
        echo "\t\t\t\t\t<!-- Go to www.addthis.com/dashboard to customize your tools -->
\t\t\t\t\t<script type=\"text/javascript\" src=\"//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-529be2200cc72db5\"></script>
\t\t\t\t\t
\t\t\t\t\t ";
        // line 832
        if ((isset($context["tags"]) ? $context["tags"] : null)) {
            // line 833
            echo "\t\t\t\t\t<div id=\"tab-tags\">
\t\t\t\t\t\t";
            // line 834
            echo (isset($context["text_tags"]) ? $context["text_tags"] : null);
            echo "
\t\t\t\t\t\t";
            // line 835
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(0, twig_length_filter($this->env, (isset($context["tags"]) ? $context["tags"] : null))));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 836
                echo "\t\t\t\t\t\t";
                if (($context["i"] < (twig_length_filter($this->env, (isset($context["tags"]) ? $context["tags"] : null)) - 1))) {
                    echo " <a class=\"btn btn-primary btn-sm\" href=\"";
                    echo $this->getAttribute($this->getAttribute((isset($context["tags"]) ? $context["tags"] : null), $context["i"], array(), "array"), "href", array());
                    echo "\">";
                    echo $this->getAttribute($this->getAttribute((isset($context["tags"]) ? $context["tags"] : null), $context["i"], array(), "array"), "tag", array());
                    echo "</a>
\t\t\t\t\t\t";
                } else {
                    // line 837
                    echo " 
\t\t\t\t\t\t";
                    // line 838
                    if ( !twig_test_empty($this->getAttribute((isset($context["tags"]) ? $context["tags"] : null), $context["i"], array(), "array"))) {
                        // line 839
                        echo "\t\t\t\t\t\t<a class=\"btn btn-primary btn-sm 22\" href=\"";
                        echo $this->getAttribute($this->getAttribute((isset($context["tags"]) ? $context["tags"] : null), $context["i"], array(), "array"), "href", array());
                        echo "\">";
                        echo $this->getAttribute($this->getAttribute((isset($context["tags"]) ? $context["tags"] : null), $context["i"], array(), "array"), "tag", array());
                        echo "</a> ";
                    }
                    // line 840
                    echo "\t\t\t\t\t\t";
                }
                // line 841
                echo "\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo " 
\t\t\t\t\t\t
\t\t\t\t\t 
\t\t\t\t\t</div>
\t\t\t\t\t";
        }
        // line 846
        echo "
\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t</div>
\t\t
\t\t\t";
        // line 852
        echo "\t\t\t</div>
\t\t</div>

\t\t";
        // line 856
        echo "\t\t";
        if ((isset($context["content_top"]) ? $context["content_top"] : null)) {
            // line 857
            echo "\t\t<div class=\"content-product-maintop form-group clearfix\">
\t\t\t";
            // line 858
            echo (isset($context["content_top"]) ? $context["content_top"] : null);
            echo "
\t\t</div>
\t\t";
        }
        // line 861
        echo "\t\t<div class=\"content-product-mainbody clearfix row\">
\t\t\t
\t\t\t";
        // line 863
        if (((isset($context["col_position"]) ? $context["col_position"] : null) == "inside")) {
            // line 864
            echo "\t\t\t";
            // line 865
            echo "\t\t\t\t";
            echo (isset($context["column_left"]) ? $context["column_left"] : null);
            echo "
\t\t\t    ";
            // line 866
            if (((isset($context["col_canvas"]) ? $context["col_canvas"] : null) == "off_canvas")) {
                // line 867
                echo "\t\t\t\t\t";
                $context["class_left"] = "col-sm-12";
                // line 868
                echo "\t\t    \t";
            } elseif (((isset($context["column_left"]) ? $context["column_left"] : null) && (isset($context["column_right"]) ? $context["column_right"] : null))) {
                // line 869
                echo "\t\t    \t\t";
                $context["class_left"] = "col-md-6 col-column3";
                // line 870
                echo "\t\t\t    ";
            } elseif (((isset($context["column_left"]) ? $context["column_left"] : null) || (isset($context["column_right"]) ? $context["column_right"] : null))) {
                // line 871
                echo "\t\t\t    \t";
                $context["class_left"] = "col-md-9 col-sm-12 col-xs-12";
                // line 872
                echo "\t\t\t    ";
            } else {
                // line 873
                echo "\t\t\t    \t";
                $context["class_left"] = "col-sm-12";
                // line 874
                echo "\t\t\t    ";
            }
            // line 875
            echo "\t\t\t";
        } else {
            // line 876
            echo "\t\t\t\t";
            $context["class_left"] = "col-sm-12";
            // line 877
            echo "\t\t\t";
        }
        // line 878
        echo "
\t\t    <div class=\"content-product-content ";
        // line 879
        echo (isset($context["class_left"]) ? $context["class_left"] : null);
        echo "\">
\t\t\t\t<div class=\"content-product-midde clearfix\">
\t\t\t\t\t";
        // line 882
        echo "\t\t\t\t\t";
        $context["related_position"] = ((($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "tabs_position"), "method") == 1)) ? ("vertical-tabs") : (""));
        // line 883
        echo "\t\t\t\t\t";
        $context["tabs_position"] = $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "tabs_position"), "method");
        // line 884
        echo "\t\t\t\t\t";
        $context["showmore"] = $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "product_enableshowmore"), "method");
        // line 885
        echo "\t\t\t\t\t";
        if ((isset($context["showmore"]) ? $context["showmore"] : null)) {
            echo " ";
            $context["class_showmore"] = "showdown";
            // line 886
            echo "\t\t\t\t\t";
        } else {
            echo " ";
            $context["class_showmore"] = "showup";
            // line 887
            echo "\t\t\t\t\t";
        }
        // line 888
        echo "
\t\t\t\t\t<div class=\"producttab \">
\t\t\t\t\t\t<div class=\"tabsslider ";
        // line 890
        echo (isset($context["related_position"]) ? $context["related_position"] : null);
        echo " ";
        if (((isset($context["tabs_position"]) ? $context["tabs_position"] : null) == 1)) {
            echo " ";
            echo "vertical-tabs";
            echo " ";
        } else {
            echo " ";
            echo "horizontal-tabs";
            echo " ";
        }
        echo " col-xs-12\">
\t\t\t\t\t\t\t";
        // line 892
        echo "\t\t\t\t\t\t\t";
        if (((isset($context["tabs_position"]) ? $context["tabs_position"] : null) == 2)) {
            // line 893
            echo "\t\t\t\t\t\t\t<ul class=\"nav nav-tabs font-sn\">
\t\t\t\t\t\t\t\t<li class=\"active\"><a data-toggle=\"tab\" href=\"#tab-description\">";
            // line 894
            echo (isset($context["tab_description"]) ? $context["tab_description"] : null);
            echo "</a></li>
\t\t\t\t\t\t\t\t
\t\t\t\t\t         
\t\t\t\t\t            ";
            // line 897
            if ((isset($context["review_status"]) ? $context["review_status"] : null)) {
                // line 898
                echo "\t\t\t\t\t           \t <li><a href=\"#tab-review\" data-toggle=\"tab\">";
                echo (isset($context["tab_review"]) ? $context["tab_review"] : null);
                echo "</a></li>
\t\t\t\t\t            ";
            }
            // line 900
            echo "\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t";
            // line 901
            if ($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "product_enableshipping"), "method")) {
                // line 902
                echo "\t\t\t\t\t\t\t\t <li><a href=\"#tab-contentshipping\" data-toggle=\"tab\">";
                echo (isset($context["tab_shipping"]) ? $context["tab_shipping"] : null);
                echo "</a></li>
\t\t\t\t\t\t\t\t";
            }
            // line 904
            echo "
\t\t\t\t\t\t\t\t";
            // line 905
            if ((isset($context["product_tabtitle"]) ? $context["product_tabtitle"] : null)) {
                // line 906
                echo "\t\t\t\t\t           \t <li><a href=\"#tab-customhtml\" data-toggle=\"tab\">";
                echo (isset($context["product_tabtitle"]) ? $context["product_tabtitle"] : null);
                echo "</a></li>
\t\t\t\t\t            ";
            }
            // line 908
            echo "
\t\t\t\t\t\t\t\t";
            // line 909
            if ((isset($context["product_video"]) ? $context["product_video"] : null)) {
                // line 910
                echo "\t\t\t\t\t           \t <li><a class=\"thumb-video\" href=\"";
                echo (isset($context["product_video"]) ? $context["product_video"] : null);
                echo "\"><i class=\"fa fa-youtube-play fa-lg\"></i> ";
                echo (isset($context["tab_video"]) ? $context["tab_video"] : null);
                echo "</a></li>
\t\t\t\t\t            ";
            }
            // line 912
            echo "\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</ul>

\t\t\t\t\t\t\t";
            // line 918
            echo "\t\t\t\t\t\t\t";
        } elseif (((isset($context["tabs_position"]) ? $context["tabs_position"] : null) == 1)) {
            // line 919
            echo "\t\t\t\t\t\t\t\t<ul class=\"nav nav-tabs col-lg-3 col-sm-4\">
\t\t\t\t\t\t\t\t<li class=\"active\"><a data-toggle=\"tab\" href=\"#tab-description\">";
            // line 920
            echo (isset($context["tab_description"]) ? $context["tab_description"] : null);
            echo "</a></li>
\t\t\t\t\t\t\t\t
\t\t\t\t\t            ";
            // line 922
            if ((isset($context["review_status"]) ? $context["review_status"] : null)) {
                // line 923
                echo "\t\t\t\t\t           \t <li><a href=\"#tab-review\" data-toggle=\"tab\">";
                echo (isset($context["tab_review"]) ? $context["tab_review"] : null);
                echo "</a></li>
\t\t\t\t\t            ";
            }
            // line 925
            echo "\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t";
            // line 926
            if ($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "product_enableshipping"), "method")) {
                // line 927
                echo "\t\t\t\t\t\t\t\t <li><a href=\"#tab-contentshipping\" data-toggle=\"tab\">";
                echo (isset($context["tab_shipping"]) ? $context["tab_shipping"] : null);
                echo "</a></li>
\t\t\t\t\t\t\t\t";
            }
            // line 929
            echo "
\t\t\t\t\t\t\t\t";
            // line 930
            if ((isset($context["product_tabtitle"]) ? $context["product_tabtitle"] : null)) {
                // line 931
                echo "\t\t\t\t\t           \t <li><a href=\"#tab-customhtml\" data-toggle=\"tab\">";
                echo (isset($context["product_tabtitle"]) ? $context["product_tabtitle"] : null);
                echo "</a></li>
\t\t\t\t\t            ";
            }
            // line 933
            echo "\t\t\t\t\t            
\t\t\t\t\t\t\t\t";
            // line 934
            if ((isset($context["product_video"]) ? $context["product_video"] : null)) {
                // line 935
                echo "\t\t\t\t\t           \t <li><a class=\"thumb-video\" href=\"";
                echo (isset($context["product_video"]) ? $context["product_video"] : null);
                echo "\"><i class=\"fa fa-youtube-play fa-lg\"></i> ";
                echo (isset($context["tab_video"]) ? $context["tab_video"] : null);
                echo "</a></li>
\t\t\t\t\t            ";
            }
            // line 937
            echo "\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t";
        }
        // line 941
        echo "
\t\t\t\t\t\t\t<div class=\"tab-content ";
        // line 942
        if (((isset($context["tabs_position"]) ? $context["tabs_position"] : null) == 1)) {
            echo " ";
            echo "col-lg-9 col-sm-8";
            echo " ";
        }
        echo " col-xs-12\">
\t\t\t\t\t\t\t\t<div class=\"tab-pane active\" id=\"tab-description\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t

\t\t\t\t\t\t            <h3 class=\"product-property-title\" > ";
        // line 947
        echo (isset($context["text_product_description"]) ? $context["text_product_description"] : null);
        echo "</h3>
\t\t\t\t\t\t            <div id=\"collapse-description\" class=\"desc-collapse ";
        // line 948
        echo (isset($context["class_showmore"]) ? $context["class_showmore"] : null);
        echo "\">
\t\t\t\t\t\t\t\t\t\t";
        // line 949
        echo (isset($context["description"]) ? $context["description"] : null);
        echo "
\t\t\t\t\t\t\t\t\t</div>\t

\t\t\t\t\t\t\t\t\t";
        // line 952
        if ((isset($context["showmore"]) ? $context["showmore"] : null)) {
            // line 953
            echo "\t\t\t\t\t\t\t\t\t<div class=\"button-toggle\">
\t\t\t\t\t\t\t\t         <a class=\"showmore\" data-toggle=\"collapse\" href=\"#\" aria-expanded=\"false\" aria-controls=\"collapse-footer\">
\t\t\t\t\t\t\t\t            <span class=\"toggle-more\">";
            // line 955
            echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "show_more"), "method");
            echo " <i class=\"fa fa-angle-down\"></i></span> 
\t\t\t\t\t\t\t\t            <span class=\"toggle-less\">";
            // line 956
            echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "show_less"), "method");
            echo " <i class=\"fa fa-angle-up\"></i></span>           
\t\t\t\t\t\t\t\t\t\t</a>        
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t";
        }
        // line 960
        echo "\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t

\t\t\t\t\t            ";
        // line 963
        if ((isset($context["review_status"]) ? $context["review_status"] : null)) {
            // line 964
            echo "\t\t\t\t\t            <div class=\"tab-pane\" id=\"tab-review\">
\t\t\t\t\t\t            <form class=\"form-horizontal\" id=\"form-review\">
\t\t\t\t\t\t                <div id=\"review\"></div>
\t\t\t\t\t\t                <h3>";
            // line 967
            echo (isset($context["text_write"]) ? $context["text_write"] : null);
            echo "</h3>
\t\t\t\t\t\t                ";
            // line 968
            if ((isset($context["review_guest"]) ? $context["review_guest"] : null)) {
                // line 969
                echo "\t\t\t\t\t\t                <div class=\"form-group required\">
\t\t\t\t\t\t                  <div class=\"col-sm-12\">
\t\t\t\t\t\t                    <label class=\"control-label\" for=\"input-name\">";
                // line 971
                echo (isset($context["entry_name"]) ? $context["entry_name"] : null);
                echo "</label>
\t\t\t\t\t\t                    <input type=\"text\" name=\"name\" value=\"";
                // line 972
                echo (isset($context["customer_name"]) ? $context["customer_name"] : null);
                echo "\" id=\"input-name\" class=\"form-control\" />
\t\t\t\t\t\t                  </div>
\t\t\t\t\t\t                </div>
\t\t\t\t\t\t                <div class=\"form-group required\">
\t\t\t\t\t\t                  <div class=\"col-sm-12\">
\t\t\t\t\t\t                    <label class=\"control-label\" for=\"input-review\">";
                // line 977
                echo (isset($context["entry_review"]) ? $context["entry_review"] : null);
                echo "</label>
\t\t\t\t\t\t                    <textarea name=\"text\" rows=\"5\" id=\"input-review\" class=\"form-control\"></textarea>
\t\t\t\t\t\t                    <div class=\"help-block\">";
                // line 979
                echo (isset($context["text_note"]) ? $context["text_note"] : null);
                echo "</div>
\t\t\t\t\t\t                  </div>
\t\t\t\t\t\t                </div>
\t\t\t\t\t\t                <div class=\"form-group required\">
\t\t\t\t\t\t                  <div class=\"col-sm-12\">
\t\t\t\t\t\t                    <label class=\"control-label\">";
                // line 984
                echo (isset($context["entry_rating"]) ? $context["entry_rating"] : null);
                echo "</label>
\t\t\t\t\t\t                    &nbsp;&nbsp;&nbsp; ";
                // line 985
                echo (isset($context["entry_bad"]) ? $context["entry_bad"] : null);
                echo "&nbsp;
\t\t\t\t\t\t                    <input type=\"radio\" name=\"rating\" value=\"1\" />
\t\t\t\t\t\t                    &nbsp;
\t\t\t\t\t\t                    <input type=\"radio\" name=\"rating\" value=\"2\" />
\t\t\t\t\t\t                    &nbsp;
\t\t\t\t\t\t                    <input type=\"radio\" name=\"rating\" value=\"3\" />
\t\t\t\t\t\t                    &nbsp;
\t\t\t\t\t\t                    <input type=\"radio\" name=\"rating\" value=\"4\" />
\t\t\t\t\t\t                    &nbsp;
\t\t\t\t\t\t                    <input type=\"radio\" name=\"rating\" value=\"5\" />
\t\t\t\t\t\t                    &nbsp;";
                // line 995
                echo (isset($context["entry_good"]) ? $context["entry_good"] : null);
                echo "</div>
\t\t\t\t\t\t                </div>
\t\t\t\t\t\t                ";
                // line 997
                echo (isset($context["captcha"]) ? $context["captcha"] : null);
                echo "
\t\t\t\t\t\t                
\t\t\t\t\t\t                  <div class=\"pull-right\">
\t\t\t\t\t\t                    <button type=\"button\" id=\"button-review\" data-loading-text=\"";
                // line 1000
                echo (isset($context["text_loading"]) ? $context["text_loading"] : null);
                echo "\" class=\"btn btn-primary\">";
                echo (isset($context["button_continue"]) ? $context["button_continue"] : null);
                echo "</button>
\t\t\t\t\t\t                  </div>
\t\t\t\t\t\t               
\t\t\t\t\t\t                ";
            } else {
                // line 1004
                echo "\t\t\t\t\t\t                ";
                echo (isset($context["text_login"]) ? $context["text_login"] : null);
                echo "
\t\t\t\t\t\t                ";
            }
            // line 1006
            echo "\t\t\t\t\t\t            </form>
\t\t\t\t\t            </div>
\t\t\t\t\t            ";
        }
        // line 1009
        echo "
\t\t\t\t\t            ";
        // line 1010
        if (($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "product_enableshipping"), "method") && $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "product_contentshipping"), "method"))) {
            // line 1011
            echo "\t\t\t\t\t\t\t\t<div class=\"tab-pane\" id=\"tab-contentshipping\">
\t\t\t\t\t\t\t\t\t";
            // line 1012
            echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "decode_entities", array(0 => $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "product_contentshipping"), "method")), "method");
            echo "
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        // line 1015
        echo "
\t\t\t\t\t\t\t\t";
        // line 1016
        if ((isset($context["product_tabtitle"]) ? $context["product_tabtitle"] : null)) {
            // line 1017
            echo "\t\t\t\t\t\t\t\t<div class=\"tab-pane \" id=\"tab-customhtml\">";
            echo (isset($context["product_tabcontent"]) ? $context["product_tabcontent"] : null);
            echo "</div>
\t\t\t\t\t\t\t\t";
        }
        // line 1019
        echo "\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t
\t\t\t\t";
        // line 1026
        echo "\t\t\t\t";
        if (((isset($context["products"]) ? $context["products"] : null) && $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "related_status"), "method"))) {
            // line 1027
            echo "\t\t\t\t<div class=\"content-product-bottom clearfix\">
\t\t\t\t\t<ul class=\"nav nav-tabs\">
\t\t\t\t\t  <li class=\"active\"><a data-toggle=\"tab\" href=\"#product-related\">";
            // line 1029
            echo (isset($context["text_related"]) ? $context["text_related"] : null);
            echo "</a></li> 
\t\t\t\t\t  <li><a data-toggle=\"tab\" href=\"#product-upsell\">";
            // line 1030
            echo (isset($context["text_upsell"]) ? $context["text_upsell"] : null);
            echo "</a></li>
\t\t\t\t\t</ul>
\t\t\t\t\t<div class=\"tab-content\">
\t\t\t\t\t  \t<div id=\"product-related\" class=\"tab-pane fade in active\">
\t\t\t\t\t\t\t";
            // line 1034
            $this->loadTemplate(((isset($context["theme_directory"]) ? $context["theme_directory"] : null) . "/template/soconfig/related_product.twig"), "so-destino/template/product/product.twig", 1034)->display($context);
            // line 1035
            echo "\t\t\t\t\t  \t</div>
\t\t\t\t\t  \t<div id=\"product-upsell\" class=\"tab-pane fade\">
\t\t\t\t\t  \t\t";
            // line 1038
            echo "\t\t\t\t\t  \t\t";
            echo (isset($context["content_bottom"]) ? $context["content_bottom"] : null);
            echo "
\t\t\t\t\t  \t</div>
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t</div>
\t\t\t\t";
        }
        // line 1044
        echo "
\t\t\t\t
\t\t\t</div>
\t\t\t";
        // line 1048
        echo "\t\t\t";
        if (((isset($context["col_position"]) ? $context["col_position"] : null) == "inside")) {
            echo " ";
            echo (isset($context["column_right"]) ? $context["column_right"] : null);
            echo " ";
        }
        // line 1049
        echo "
\t\t</div>
\t\t <div class=\"tabs-nav\">
\t\t\t        <ul>
\t\t\t            <li><i class=\"fa fa-dot-circle-o\"></i><a href=\"";
        // line 1053
        echo (isset($context["current_url"]) ? $context["current_url"] : null);
        echo "#prod_overview\">Overview</a></li>
\t\t\t            <li><i class=\"fa fa-gears\"></i><a href=\"";
        // line 1054
        echo (isset($context["current_url"]) ? $context["current_url"] : null);
        echo "#prod_specs\">Specs</a></li>
\t\t\t            <li><i class=\"fa fa-video-camera\"></i><a href=\"";
        // line 1055
        echo (isset($context["current_url"]) ? $context["current_url"] : null);
        echo "#prod_video\">Video</a></li>
\t\t\t            <li><i class=\"fa fa-star\"></i><a href=\"";
        // line 1056
        echo (isset($context["current_url"]) ? $context["current_url"] : null);
        echo "#prod_review\">Reviews</a></li>
\t\t\t            <li><i class=\"fa fa-cube\"></i><a href=\"";
        // line 1057
        echo (isset($context["current_url"]) ? $context["current_url"] : null);
        echo "#prod_related\">Related product</a></li>
\t\t\t            <li><i class=\"fa fa-exchange\"></i><a href=\"";
        // line 1058
        echo (isset($context["current_url"]) ? $context["current_url"] : null);
        echo "#prod_compare\">Compare product</a></li>
\t\t\t            <li><i class=\"fa fa-commenting\"></i><a href=\"";
        // line 1059
        echo (isset($context["current_url"]) ? $context["current_url"] : null);
        echo "#prod_question\">Question & Answer</a></li>
\t\t\t        </ul>
\t\t\t    </div>
\t\t<div class=\"content-product-main1\">
\t\t    ";
        // line 1063
        if ((isset($context["description"]) ? $context["description"] : null)) {
            // line 1064
            echo "\t\t\t\t\t <div id=\"prod_overview\" class=\"short_description form-group\" itemprop=\"description\">
\t\t\t\t\t\t<h3>Overview</h3>
\t\t\t\t\t\t
\t\t                    ";
            // line 1067
            echo (isset($context["description"]) ? $context["description"] : null);
            echo "  
\t\t                
\t\t\t\t\t</div>
\t\t\t\t\t";
        }
        // line 1071
        echo "\t\t    ";
        // line 1072
        echo "\t\t    <div class=\"col-md-12\">
\t\t        <div class=\"product-combo compare-product\" id=\"prod_compare\">
\t\t\t        <div class=\"combo-title\">
\t\t\t            <h2>Compate With similar Products</h2>
\t\t\t            <a href=\"\"><i class=\"fa fa-plus\"></i>Add Comparison</a>
\t\t\t        </div>
\t\t\t        <div id=\"collapse-description\" class=\"desc-collapse showdown compare\">
\t\t\t        <ul class=\"\">
\t\t\t            <li><div class=\"free-space\"></div>
\t\t\t            <div class=\"battery-power\">
\t\t\t                <strong>Battery</strong>
\t\t\t            </div>
\t\t\t            <div class=\"external-storage\">
\t\t\t                <strong>Expandable storage</strong>
\t\t\t            </div>
\t\t\t             <div class=\"external-storage\">
\t\t\t                <strong>Expandable storage</strong>
\t\t\t            </div>
\t\t\t            </li>
\t\t\t             <li><a href=\"\">
\t\t\t                <div class=\"combo-list\"><img src=\"http://poorvikabeta.webindia.com/image/cache/catalog/demo/canon_eos_5d_1-355x470.jpg\"></div>
\t\t\t                <div class=\"combo-content\">
\t\t\t                    <h5>Canon EOS 5D</h5>
\t\t\t                    <strong>4.5
\t\t\t                    ";
        // line 1096
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 5));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 1097
            echo "\t\t\t\t\t\t\t\t";
            if (((isset($context["rating"]) ? $context["rating"] : null) < $context["i"])) {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            } else {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            }
            // line 1098
            echo "\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1099
        echo "\t\t\t\t\t\t\t</strong> 
\t\t\t\t\t\t\t<small class=\"price\">&#8377; 45,000<strike class=\"old-price\">25,000</strike></small>
\t\t\t\t\t\t\t</div>
\t\t\t            </a>
\t\t\t             <div class=\"battery-power\">
\t\t\t                <strong>4000 mAH</strong>
\t\t\t            </div>
\t\t\t            <div class=\"external-storage\">
\t\t\t                <strong>128GB</strong>
\t\t\t            </div>
\t\t\t             <div class=\"external-storage\">
\t\t\t                <strong>128GB</strong>
\t\t\t            </div>
\t\t\t            </li>
\t\t\t             <li><a href=\"\">
\t\t\t                <div class=\"combo-list\"><img src=\"http://poorvikabeta.webindia.com/image/cache/catalog/demo/canon_eos_5d_1-355x470.jpg\"></div>
\t\t\t                <div class=\"combo-content\">
\t\t\t                    <h5>Canon EOS 5D</h5>
\t\t\t                    <strong>4.5
\t\t\t                    ";
        // line 1118
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 5));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 1119
            echo "\t\t\t\t\t\t\t\t";
            if (((isset($context["rating"]) ? $context["rating"] : null) < $context["i"])) {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            } else {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            }
            // line 1120
            echo "\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1121
        echo "\t\t\t\t\t\t\t</strong> 
\t\t\t\t\t\t\t<small class=\"price\">&#8377; 45,000<strike class=\"old-price\">25,000</strike></small>
\t\t\t\t\t\t\t</div>
\t\t\t            </a>
\t\t\t              <div class=\"battery-power\">
\t\t\t                <strong>4000 mAH</strong>
\t\t\t            </div>
\t\t\t            <div class=\"external-storage\">
\t\t\t                <strong>128GB</strong>
\t\t\t            </div>
\t\t\t             <div class=\"external-storage\">
\t\t\t                <strong>128GB</strong>
\t\t\t            </div>
\t\t\t            </li>
\t\t\t             <li><a href=\"\">
\t\t\t                <div class=\"combo-list\"><img src=\"http://poorvikabeta.webindia.com/image/cache/catalog/demo/canon_eos_5d_1-355x470.jpg\"></div>
\t\t\t                <div class=\"combo-content\">
\t\t\t                    <h5>Canon EOS 5D</h5>
\t\t\t                    <strong>4.5
\t\t\t                    ";
        // line 1140
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 5));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 1141
            echo "\t\t\t\t\t\t\t\t";
            if (((isset($context["rating"]) ? $context["rating"] : null) < $context["i"])) {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            } else {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            }
            // line 1142
            echo "\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1143
        echo "\t\t\t\t\t\t\t</strong> 
\t\t\t\t\t\t\t<small class=\"price\">&#8377; 45,000<strike class=\"old-price\">25,000</strike></small>
\t\t\t\t\t\t\t</div>
\t\t\t            </a>
\t\t\t             <div class=\"battery-power\">
\t\t\t                <strong>4000 mAH</strong>
\t\t\t            </div>
\t\t\t            <div class=\"external-storage\">
\t\t\t                <strong>128GB</strong>
\t\t\t            </div>
\t\t\t             <div class=\"external-storage\">
\t\t\t                <strong>128GB</strong>
\t\t\t            </div>
\t\t\t            </li>
\t\t\t             <li><a href=\"\">
\t\t\t                <div class=\"combo-list\"><img src=\"http://poorvikabeta.webindia.com/image/cache/catalog/demo/canon_eos_5d_1-355x470.jpg\"></div>
\t\t\t                <div class=\"combo-content\">
\t\t\t                    <h5>Canon EOS 5D</h5>
\t\t\t                    <strong>4.5
\t\t\t                    ";
        // line 1162
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 5));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 1163
            echo "\t\t\t\t\t\t\t\t";
            if (((isset($context["rating"]) ? $context["rating"] : null) < $context["i"])) {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            } else {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            }
            // line 1164
            echo "\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1165
        echo "\t\t\t\t\t\t\t</strong> 
\t\t\t\t\t\t\t<small class=\"price\">&#8377; 45,000<strike class=\"old-price\">25,000</strike></small>
\t\t\t\t\t\t\t</div>
\t\t\t            </a>
\t\t\t             <div class=\"battery-power\">
\t\t\t                <strong>4000 mAH</strong>
\t\t\t            </div>
\t\t\t            <div class=\"external-storage\">
\t\t\t                <strong>128GB</strong>
\t\t\t            </div>
\t\t\t             <div class=\"external-storage\">
\t\t\t                <strong>128GB</strong>
\t\t\t            </div>
\t\t\t            </li>
\t\t\t            
\t\t\t             
\t\t\t            
\t\t\t        </ul>
\t\t\t        </div>
\t\t\t       
\t\t\t       
\t\t\t\t\t\t\t\t\t<div class=\"gallery-button details-button\"><a href=\"\" class=\"btn btn-gallary btn-detail\">See Image Gallery</a></div>
\t\t\t    </div>
\t\t    </div>
\t\t    ";
        // line 1190
        echo "\t\t    ";
        // line 1191
        echo "\t\t    <div class=\"col-md-12\">
\t\t        <div id=\"prod_video\" class=\"product-combo product-video\">
\t\t       <div class=\"combo-title\">
\t\t\t            <h2><img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/video-product.png\">Video</h2>
\t\t\t             <a href=\"\"><i class=\"fa fa-youtube\"></i>Watch YouTube Reviews</a>
\t\t\t        </div>
\t\t\t        
\t\t\t        <ul class=\"video-carousel owl-carousel owl-theme\">
\t\t\t            <li>
\t\t\t                <div class=\"video-frame\">
\t\t\t                  <iframe width=\"200\" height=\"115\" src=\"https://www.youtube.com/embed/tgbNymZ7vqY\"></iframe>
                            </div>
                            <div class=\"video-content\">
                                <span>4.6<img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/video-green-arrow.png\"></span>
                                <strong>performance<small>Ultra fast</small></strong>
                            </div>
                       </li>
                        <li>
\t\t\t                <div class=\"video-frame\">
\t\t\t                  <iframe width=\"200\" height=\"115\" src=\"https://www.youtube.com/embed/tgbNymZ7vqY\"></iframe>
                            </div>
                             <div class=\"video-content\">
                                <span>4.6<img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/video-green-arrow.png\"></span>
                                <strong>performance<small>Ultra fast</small></strong>
                            </div>
                       </li>
                        <li>
\t\t\t                <div class=\"video-frame\">
\t\t\t                   <iframe width=\"200\" height=\"115\" src=\"https://www.youtube.com/embed/tgbNymZ7vqY\"></iframe>
                            </div>
                             <div class=\"video-content\">
                                <span>4.6<img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/video-green-arrow.png\"></span>
                                <strong>performance<small>Ultra fast</small></strong>
                            </div>
                       </li>
                        <li>
\t\t\t                <div class=\"video-frame\">
\t\t\t                   <iframe width=\"200\" height=\"115\" src=\"https://www.youtube.com/embed/tgbNymZ7vqY\"></iframe>
                            </div>
                             <div class=\"video-content\">
                                <span>4.6<img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/video-green-arrow.png\"></span>
                                <strong>performance<small>Ultra fast</small></strong>
                            </div>
                       </li>
                        <li>
\t\t\t                <div class=\"video-frame\">
\t\t\t                  <iframe width=\"200\" height=\"115\" src=\"https://www.youtube.com/embed/tgbNymZ7vqY\"></iframe>
                            </div>
                             <div class=\"video-content\">
                                <span>4.6<img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/video-green-arrow.png\"></span>
                                <strong>performance<small>Ultra fast</small></strong>
                            </div>
                       </li>
\t\t\t        </ul>
\t\t\t        
\t\t\t        </div>
\t\t    </div>
\t\t     ";
        // line 1249
        echo "\t\t      ";
        // line 1250
        echo "\t\t     <div id=\"prod_review\" class=\"col-md-12\">
\t\t         <div class=\"product-question review-product\">
\t\t              <div class=\"combo-title\">
\t\t\t            <h2><img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/video-product.png\">Customer Review</h2>
\t\t\t             <a href=\"\"><i class=\"fa fa-edit\"></i>Write Product Reviews</a>
\t\t\t          </div>
\t\t         </div>
\t\t     </div>
\t\t     <div class=\"col-md-5  pr-0\">
\t\t         <div class=\"reviwe-block\">
\t\t         <div class=\"review-card\">
\t\t             <div class=\"card-cont\">
\t\t             <span>Rating<strong>4.6<small>out of 5</small></strong></span>
\t\t             <small>35 Ratings 4 Reviews</small>
\t\t             </div>
\t\t             <div class=\"review-rating\">
\t\t                 <ul>
\t\t                     <li> ";
        // line 1267
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 5));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 1268
            echo "\t\t\t\t\t\t\t\t";
            if (((isset($context["rating"]) ? $context["rating"] : null) < $context["i"])) {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            } else {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            }
            // line 1269
            echo "\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo "<small>75%</small></li>
\t\t\t\t\t\t\t<li> ";
        // line 1270
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 4));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 1271
            echo "\t\t\t\t\t\t\t\t";
            if (((isset($context["rating"]) ? $context["rating"] : null) < $context["i"])) {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            } else {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            }
            // line 1272
            echo "\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo "<small>75%</small></li>
\t\t\t\t\t\t\t\t<li> ";
        // line 1273
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 3));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 1274
            echo "\t\t\t\t\t\t\t\t";
            if (((isset($context["rating"]) ? $context["rating"] : null) < $context["i"])) {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            } else {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            }
            // line 1275
            echo "\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo "<small>75%</small></li>
\t\t\t\t\t\t\t\t<li> ";
        // line 1276
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 2));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 1277
            echo "\t\t\t\t\t\t\t\t";
            if (((isset($context["rating"]) ? $context["rating"] : null) < $context["i"])) {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            } else {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            }
            // line 1278
            echo "\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo "<small>75%</small></li>
\t\t\t\t\t\t\t\t<li> ";
        // line 1279
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 1));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 1280
            echo "\t\t\t\t\t\t\t\t";
            if (((isset($context["rating"]) ? $context["rating"] : null) < $context["i"])) {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            } else {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            }
            // line 1281
            echo "\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo "<small>75%</small></li>
\t\t                 </ul>
\t\t             </div>
\t\t             
\t\t         </div>
\t\t         <div class=\"review-edit-box\">
\t\t             <strong>Write review for this product<span>share your feedback with other customer</span></strong>
\t\t             <div class=\"edit-button\"><a href=\"\"><i class=\"fa fa-edit\"></i>Write a Product review</a></div>
\t\t         </div>
\t\t         </div>
\t\t     </div>
\t\t     <div class=\"col-md-7 pl-0\">
\t\t        <div class=\"review-right-block\">
\t\t         <div class=\"review-mention\">
\t\t             <h3>Review Mention</h3>
\t\t             <span>battery life</span><span>value of money</span><span>Price range</span><span>best budget</span>
\t\t         </div>
\t\t         <div class=\"review-text\">
\t\t             <h3>Review</h3>
\t\t             <ul>
\t\t                 <li>
\t\t                     <div class=\"customer-block\">
\t\t                     <div class=\"customer-details\">
\t\t                         <div class=\"customer-profile\">
\t\t                         <img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/customer-review-img1.jpg\">
\t\t                         </div>
\t\t                         <strong>Nikil <br>
\t\t                          ";
        // line 1308
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 5));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 1309
            echo "\t\t\t\t\t\t\t\t";
            if (((isset($context["rating"]) ? $context["rating"] : null) < $context["i"])) {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            } else {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            }
            // line 1310
            echo "\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1311
        echo "\t\t\t\t\t\t\t<small>reviewed on 15 sep 2020</small>
\t\t                         </strong>
\t\t                     </div>
\t\t                     <div class=\"like-details\">
\t\t                         <small><i class=\"fa fa-thumbs-up\"></i></small>
\t\t                         <small><i class=\"fa fa-thumbs-down\"></i></small>
\t\t                     </div>
\t\t                     </div>
\t\t                     <div class=\"review-para\">
\t\t                         <span>More than 5 star, best budget mobile</span>
\t\t                         <p>Integer faucibus facilisis volutpat. Aliquam in suscipit orci. Nullam aliquam fermentum auctor. </p>
\t\t                     </div>
\t\t                 </li>
\t\t                  <li>
\t\t                     <div class=\"customer-block\">
\t\t                     <div class=\"customer-details\">
\t\t                         <div class=\"customer-profile\">
\t\t                         <img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/customer-review-img1.jpg\">
\t\t                         </div>
\t\t                         <strong>Nikil <br>
\t\t                          ";
        // line 1331
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 5));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 1332
            echo "\t\t\t\t\t\t\t\t";
            if (((isset($context["rating"]) ? $context["rating"] : null) < $context["i"])) {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            } else {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            }
            // line 1333
            echo "\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1334
        echo "\t\t\t\t\t\t\t<small>reviewed on 15 sep 2020</small>
\t\t                         </strong>
\t\t                     </div>
\t\t                     <div class=\"like-details\">
\t\t                         <small><i class=\"fa fa-thumbs-up\"></i></small>
\t\t                         <small><i class=\"fa fa-thumbs-down\"></i></small>
\t\t                     </div>
\t\t                     </div>
\t\t                     <div class=\"review-para\">
\t\t                         <span>More than 5 star, best budget mobile</span>
\t\t                         <p>Integer faucibus facilisis volutpat. Aliquam in suscipit orci. Nullam aliquam fermentum auctor..</p>
\t\t                     </div>
\t\t                 </li>
\t\t                  <li>
\t\t                     <div class=\"customer-block\">
\t\t                     <div class=\"customer-details\">
\t\t                         <div class=\"customer-profile\">
\t\t                         <img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/customer-review-img1.jpg\">
\t\t                         </div>
\t\t                         <strong>Nikil <br>
\t\t                          ";
        // line 1354
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 5));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 1355
            echo "\t\t\t\t\t\t\t\t";
            if (((isset($context["rating"]) ? $context["rating"] : null) < $context["i"])) {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            } else {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            }
            // line 1356
            echo "\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1357
        echo "\t\t\t\t\t\t\t<small>reviewed on 15 sep 2020</small>
\t\t                         </strong>
\t\t                     </div>
\t\t                     <div class=\"like-details\">
\t\t                         <small><i class=\"fa fa-thumbs-up\"></i></small>
\t\t                         <small><i class=\"fa fa-thumbs-down\"></i></small>
\t\t                     </div>
\t\t                     </div>
\t\t                     <div class=\"review-para\">
\t\t                         <span>More than 5 star, best budget mobile</span>
\t\t                         <p>Integer faucibus facilisis volutpat. Aliquam in suscipit orci. Nullam aliquam fermentum auctor..</p>
\t\t                     </div>
\t\t                 </li>
\t\t                 
\t\t             </ul>
\t\t         </div>
\t\t         </div>
\t\t     </div>
\t\t       ";
        // line 1376
        echo "\t\t     ";
        // line 1377
        echo "\t\t     <div class=\"col-md-8 pr-0\">
\t\t         <div class=\"product-question\" id=\"prod_question\">
\t\t               <div class=\"combo-title question-pro\">
\t\t\t            <h2><img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/related-product.png\">Question and Answer</h2>
\t\t\t            <span><input type=\"text\" placeholder=\"Search of Question and Answer..\"></span>
\t\t\t        </div>
\t\t\t        <div id=\"collapse-description\" class=\"desc-collapse showdown\">
\t\t\t        <ul>
\t\t\t            <li><div class=\"question-content\">
\t\t\t                <p class=\"question-cont\">Q : It is Quality Product?</p>
\t\t\t                <p class=\"answer-cont\">A : Yes it Quality Product.</p>
\t\t\t                </div>
\t\t\t                <div class=\"like-details\">
\t\t\t                    <span>by paratap</span>
\t\t\t                    <small><i class=\"fa fa-thumbs-up\"></i>930</small>
\t\t\t                    <small><i class=\"fa fa-thumbs-down\"></i>30</small>
\t\t\t                </div>
\t\t\t            </li>
\t\t\t             <li><div class=\"question-content\">
\t\t\t                <p class=\"question-cont\">Q : It is Quality Product?</p>
\t\t\t                <p class=\"answer-cont\">A : Yes it Quality Product.</p>
\t\t\t                </div>
\t\t\t                <div class=\"like-details\">
\t\t\t                    <span>by paratap</span>
\t\t\t                    <small><i class=\"fa fa-thumbs-up\"></i>930</small>
\t\t\t                    <small><i class=\"fa fa-thumbs-down\"></i>30</small>
\t\t\t                </div>
\t\t\t            </li>
\t\t\t             <li><div class=\"question-content\">
\t\t\t                <p class=\"question-cont\">Q : It is Quality Product?</p>
\t\t\t                <p class=\"answer-cont\">A : Yes it Quality Product.</p>
\t\t\t                </div>
\t\t\t                <div class=\"like-details\">
\t\t\t                    <span>by paratap</span>
\t\t\t                    <small><i class=\"fa fa-thumbs-up\"></i>930</small>
\t\t\t                    <small><i class=\"fa fa-thumbs-down\"></i>30</small>
\t\t\t                </div>
\t\t\t            </li>
\t\t\t             <li><div class=\"question-content\">
\t\t\t                <p class=\"question-cont\">Q : It is Quality Product?</p>
\t\t\t                <p class=\"answer-cont\">A : Yes it Quality Product.</p>
\t\t\t                </div>
\t\t\t                <div class=\"like-details\">
\t\t\t                    <span>by paratap</span>
\t\t\t                    <small><i class=\"fa fa-thumbs-up\"></i>930</small>
\t\t\t                    <small><i class=\"fa fa-thumbs-down\"></i>30</small>
\t\t\t                </div>
\t\t\t            </li>
\t\t\t        </ul>
\t\t\t        </div>
\t\t\t        <div class=\"button-toggle toggle1\">
\t\t\t\t\t\t\t\t         <a class=\"showmore\" data-toggle=\"collapse\" href=\"#\" aria-expanded=\"false\" aria-controls=\"collapse-footer\">
\t\t\t\t\t\t\t\t            <span class=\"toggle-more\">Show all answer question <i class=\"fa fa-angle-down\"></i></span> 
\t\t\t\t\t\t\t\t            <span class=\"toggle-less\">Show Less <i class=\"fa fa-angle-up\"></i></span>           
\t\t\t\t\t\t\t\t\t\t</a>      
\t\t\t\t\t\t\t\t\t\t<a href=\"\">
\t\t\t\t\t\t\t\t\t\t    <strong><i class=\"fa fa-edit\"></i>Ask Question</strong>
\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t</div>
\t\t         </div>
\t\t     </div>
\t\t     <div class=\"col-md-4 pl-0\">
\t\t         <div class=\"product-customer-image\">
\t\t              <div class=\"combo-title customer-img\">
\t\t\t            <h2>Customer Image</h2>
\t\t\t        </div>
\t\t\t        <ul>
\t\t\t            <li><a href=\"\"><img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/customer-img1.jpg\"></a></li>
\t\t\t            <li><a href=\"\"><img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/customer-img2.jpg\"></a></li>
\t\t\t            <li><a href=\"\"><img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/customer-img2.jpg\"></a></li>
\t\t\t            <li><a href=\"\"><img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/customer-img1.jpg\"></a></li>
\t\t\t        </ul>
\t\t\t        <div class=\"gallery-button\"><a href=\"\" class=\"btn btn-gallary\">See Image Gallery</a></div>
\t\t         </div>
\t\t     </div>
\t\t     ";
        // line 1453
        echo "\t\t     
\t\t     ";
        // line 1455
        echo "\t\t     <div class=\"col-md-12 pr-0 pl-0\">
\t\t\t    <div class=\"product-combo\">
\t\t\t        <div class=\"combo-title\">
\t\t\t            <h2>Buy Together Combo Offer</h2>
\t\t\t        </div>
\t\t\t        <ul>
\t\t\t            <li><div class=\"plus-ico\"><img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/plus-ico.png\"></div>
\t\t\t                <div class=\"combo-checked-box\"><input type=\"checkbox\" id=\"combo-box\"></div>
\t\t\t                <div class=\"combo-offer-img\"><img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/combo-offer-img.jpg\"></div>
\t\t\t                <div class=\"combo-list\"><img src=\"http://poorvikabeta.webindia.com/image/cache/catalog/demo/canon_eos_5d_1-355x470.jpg\"></div>
\t\t\t                <div class=\"combo-content\">
\t\t\t                    <h5>Canon EOS 5D</h5>
\t\t\t                    <strong>4.5
\t\t\t                    ";
        // line 1468
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 5));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 1469
            echo "\t\t\t\t\t\t\t\t";
            if (((isset($context["rating"]) ? $context["rating"] : null) < $context["i"])) {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            } else {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            }
            // line 1470
            echo "\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1471
        echo "\t\t\t\t\t\t\t</strong> 
\t\t\t\t\t\t\t<small class=\"price\">&#8377; 45,000<strike class=\"old-price\">25,000</strike></small>
\t\t\t\t\t\t\t</div>
\t\t\t            
\t\t\t            </li>
\t\t\t             <li><div class=\"plus-ico\"><img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/plus-ico.png\"></div>
\t\t\t                   <div class=\"combo-checked-box\"><input type=\"checkbox\" id=\"combo-box\"></div>
\t\t\t                <div class=\"combo-offer-img\"><img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/combo-offer-img.jpg\"></div>
\t\t\t                <div class=\"combo-list\"><img src=\"http://poorvikabeta.webindia.com/image/cache/catalog/demo/canon_eos_5d_1-355x470.jpg\"></div>
\t\t\t                <div class=\"combo-content\">
\t\t\t                    <h5>Canon EOS 5D</h5>
\t\t\t                    <strong>4.5
\t\t\t                    ";
        // line 1483
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 5));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 1484
            echo "\t\t\t\t\t\t\t\t";
            if (((isset($context["rating"]) ? $context["rating"] : null) < $context["i"])) {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            } else {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            }
            // line 1485
            echo "\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1486
        echo "\t\t\t\t\t\t\t</strong> 
\t\t\t\t\t\t\t<small class=\"price\">&#8377; 45,000<strike class=\"old-price\">25,000</strike></small>
\t\t\t\t\t\t\t</div>
\t\t\t            
\t\t\t            </li>
\t\t\t             <li><div class=\"plus-ico\"><img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/equal-ico.png\"></div>
\t\t\t                   <div class=\"combo-checked-box\"><input type=\"checkbox\" id=\"combo-box\"></div>
\t\t\t                <div class=\"combo-offer-img\"><img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/combo-offer-img.jpg\"></div>
\t\t\t                <div class=\"combo-list\"><img src=\"http://poorvikabeta.webindia.com/image/cache/catalog/demo/canon_eos_5d_1-355x470.jpg\"></div>
\t\t\t                <div class=\"combo-content\">
\t\t\t                    <h5>Canon EOS 5D</h5>
\t\t\t                    <strong>4.5
\t\t\t                    ";
        // line 1498
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 5));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 1499
            echo "\t\t\t\t\t\t\t\t";
            if (((isset($context["rating"]) ? $context["rating"] : null) < $context["i"])) {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            } else {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            }
            // line 1500
            echo "\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1501
        echo "\t\t\t\t\t\t\t</strong> 
\t\t\t\t\t\t\t<small class=\"price\">&#8377; 45,000<strike class=\"old-price\">25,000</strike></small>
\t\t\t\t\t\t\t</div>
\t\t\t            
\t\t\t            </li>
\t\t\t             <li class=\"combo-offer-bg\">
\t\t\t                <h4>Offer Summary</h4>
\t\t\t                <strike class=\"old-price\">25,000</strike>
\t\t\t                \t<small class=\"price\">&#8377; 45,000</small>
\t\t\t               <strong>You save 20,600<span>on 2 items</span></strong>
\t\t\t               
\t\t\t            <div class=\"cart\"><input type=\"button\" value=\"";
        // line 1512
        echo (isset($context["button_cart"]) ? $context["button_cart"] : null);
        echo "\" data-loading-text=\"";
        echo (isset($context["text_loading"]) ? $context["text_loading"] : null);
        echo "\" id=\"button-cart\" class=\"btn btn-mega btn-lg btn-offer\"></div>
\t\t\t            </li>
\t\t\t        </ul>
\t\t\t    </div>
\t\t\t</div>
\t\t\t";
        // line 1518
        echo "\t\t    ";
        // line 1519
        echo "\t\t    <div id=\"prod_related\" class=\"col-md-12\">
\t\t        <div class=\"product-combo related-product\">
\t\t\t        <div class=\"combo-title\">
\t\t\t            <h2><img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/related-product.png\">Related Product</h2>
\t\t\t        </div>
\t\t\t        <ul class=\"related-carousel owl-carousel owl-theme\">
\t\t\t            <li><a href=\"\">
\t\t\t                <div class=\"combo-list\"><img src=\"http://poorvikabeta.webindia.com/image/cache/catalog/demo/canon_eos_5d_1-355x470.jpg\"></div>
\t\t\t                <div class=\"combo-content\">
\t\t\t                    <h5>Canon EOS 5D</h5>
\t\t\t                    <strong>4.5
\t\t\t                    ";
        // line 1530
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 5));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 1531
            echo "\t\t\t\t\t\t\t\t";
            if (((isset($context["rating"]) ? $context["rating"] : null) < $context["i"])) {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            } else {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            }
            // line 1532
            echo "\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1533
        echo "\t\t\t\t\t\t\t</strong> 
\t\t\t\t\t\t\t<small class=\"price\">&#8377; 45,000<strike class=\"old-price\">25,000</strike></small>
\t\t\t\t\t\t\t</div>
\t\t\t            </a>
\t\t\t            </li>
\t\t\t             <li><a href=\"\">
\t\t\t                <div class=\"combo-list\"><img src=\"http://poorvikabeta.webindia.com/image/cache/catalog/demo/canon_eos_5d_1-355x470.jpg\"></div>
\t\t\t                <div class=\"combo-content\">
\t\t\t                    <h5>Canon EOS 5D</h5>
\t\t\t                    <strong>4.5
\t\t\t                    ";
        // line 1543
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 5));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 1544
            echo "\t\t\t\t\t\t\t\t";
            if (((isset($context["rating"]) ? $context["rating"] : null) < $context["i"])) {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            } else {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            }
            // line 1545
            echo "\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1546
        echo "\t\t\t\t\t\t\t</strong> 
\t\t\t\t\t\t\t<small class=\"price\">&#8377; 45,000<strike class=\"old-price\">25,000</strike></small>
\t\t\t\t\t\t\t</div>
\t\t\t            </a>
\t\t\t            </li>
\t\t\t             <li><a href=\"\">
\t\t\t                <div class=\"combo-list\"><img src=\"http://poorvikabeta.webindia.com/image/cache/catalog/demo/canon_eos_5d_1-355x470.jpg\"></div>
\t\t\t                <div class=\"combo-content\">
\t\t\t                    <h5>Canon EOS 5D</h5>
\t\t\t                    <strong>4.5
\t\t\t                    ";
        // line 1556
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 5));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 1557
            echo "\t\t\t\t\t\t\t\t";
            if (((isset($context["rating"]) ? $context["rating"] : null) < $context["i"])) {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            } else {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            }
            // line 1558
            echo "\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1559
        echo "\t\t\t\t\t\t\t</strong> 
\t\t\t\t\t\t\t<small class=\"price\">&#8377; 45,000<strike class=\"old-price\">25,000</strike></small>
\t\t\t\t\t\t\t</div>
\t\t\t            </a>
\t\t\t            </li>
\t\t\t             <li><a href=\"\">
\t\t\t                <div class=\"combo-list\"><img src=\"http://poorvikabeta.webindia.com/image/cache/catalog/demo/canon_eos_5d_1-355x470.jpg\"></div>
\t\t\t                <div class=\"combo-content\">
\t\t\t                    <h5>Canon EOS 5D</h5>
\t\t\t                    <strong>4.5
\t\t\t                    ";
        // line 1569
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 5));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 1570
            echo "\t\t\t\t\t\t\t\t";
            if (((isset($context["rating"]) ? $context["rating"] : null) < $context["i"])) {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            } else {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            }
            // line 1571
            echo "\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1572
        echo "\t\t\t\t\t\t\t</strong> 
\t\t\t\t\t\t\t<small class=\"price\">&#8377; 45,000<strike class=\"old-price\">25,000</strike></small>
\t\t\t\t\t\t\t</div>
\t\t\t            </a>
\t\t\t            </li>
\t\t\t             <li><a href=\"\">
\t\t\t                <div class=\"combo-list\"><img src=\"http://poorvikabeta.webindia.com/image/cache/catalog/demo/canon_eos_5d_1-355x470.jpg\"></div>
\t\t\t                <div class=\"combo-content\">
\t\t\t                    <h5>Canon EOS 5D</h5>
\t\t\t                    <strong>4.5
\t\t\t                    ";
        // line 1582
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 5));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 1583
            echo "\t\t\t\t\t\t\t\t";
            if (((isset($context["rating"]) ? $context["rating"] : null) < $context["i"])) {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            } else {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            }
            // line 1584
            echo "\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1585
        echo "\t\t\t\t\t\t\t</strong> 
\t\t\t\t\t\t\t<small class=\"price\">&#8377; 45,000<strike class=\"old-price\">25,000</strike></small>
\t\t\t\t\t\t\t</div>
\t\t\t            </a>
\t\t\t            </li>
\t\t\t            
\t\t\t             
\t\t\t            
\t\t\t        </ul>
\t\t\t    </div>
\t\t    </div>
\t\t</div>
    \t";
        // line 1598
        echo "    </div>
    
    ";
        // line 1601
        echo "    ";
        if (((isset($context["col_position"]) ? $context["col_position"] : null) == "outside")) {
            echo " ";
            echo (isset($context["column_right"]) ? $context["column_right"] : null);
            echo " ";
        }
        // line 1602
        echo "    </div>
</div>

<script src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/js/owl.carousel.min.js\"></script> 
<script type=\"text/javascript\">
<!--
\$('select[name=\\'recurring_id\\'], input[name=\"quantity\"]').change(function(){
\t\$.ajax({
\t\turl: 'index.php?route=product/product/getRecurringDescription',
\t\ttype: 'post',
\t\tdata: \$('input[name=\\'product_id\\'], input[name=\\'quantity\\'], select[name=\\'recurring_id\\']'),
\t\tdataType: 'json',
\t\tbeforeSend: function() {
\t\t\t\$('#recurring-description').html('');
\t\t},
\t\tsuccess: function(json) {
\t\t\t\$('.alert-dismissible, .text-danger').remove();

\t\t\tif (json['success']) {
\t\t\t\t\$('#recurring-description').html(json['success']);
\t\t\t}
\t\t}
\t});
});
//--></script>

<script type=\"text/javascript\"><!--
\$('#button-cart').on('click', function() {
\t
\t\$.ajax({
\t\turl: 'index.php?route=extension/soconfig/cart/add',
\t\ttype: 'post',
\t\tdata: \$('#product input[type=\\'text\\'], #product input[type=\\'hidden\\'], #product input[type=\\'radio\\']:checked, #product input[type=\\'checkbox\\']:checked, #product select, #product textarea'),
\t\tdataType: 'json',
\t\tbeforeSend: function() {
\t\t\t\$('#button-cart').button('loading');
\t\t},
\t\tcomplete: function() {
\t\t\t\$('#button-cart').button('reset');
\t\t},
\t\tsuccess: function(json) {
\t\t\t\$('.alert').remove();
\t\t\t\$('.text-danger').remove();
\t\t\t\$('.form-group').removeClass('has-error');
\t\t\tif (json['error']) {
\t\t\t\tif (json['error']['option']) {
\t\t\t\t\tfor (i in json['error']['option']) {
\t\t\t\t\t\tvar element = \$('#input-option' + i.replace('_', '-'));
 
 ";
        // line 1651
        if ((isset($context["option_data"]) ? $context["option_data"] : null)) {
            echo " 
 if(ProductOptionId != undefined && ProductOptionId==i.replace('_', '-')){ 
 \$('.so-colorswatch-productpage-icons').after('<div class=\"text-danger\">' + json['error']['option'][i] + '</div>'); 
 } 
 ";
        }
        // line 1655
        echo " 
 
\t\t\t\t\t\t
\t\t\t\t\t\tif (element.parent().hasClass('input-group')) {
\t\t\t\t\t\t\telement.parent().after('<div class=\"text-danger\">' + json['error']['option'][i] + '</div>');
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\telement.after('<div class=\"text-danger\">' + json['error']['option'][i] + '</div>');
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t}
\t\t\t\t
\t\t\t\tif (json['error']['recurring']) {
\t\t\t\t\t\$('select[name=\\'recurring_id\\']').after('<div class=\"text-danger\">' + json['error']['recurring'] + '</div>');
\t\t\t\t}
\t\t\t\t
\t\t\t\t// Highlight any found errors
\t\t\t\t\$('.text-danger').parent().addClass('has-error');
\t\t\t}
\t\t\t
\t\t\tif (json['success']) {
\t\t\t\t\$('.text-danger').remove();
\t\t\t\t\$('#wrapper').before('<div class=\"alert alert-success\"><i class=\"fa fa-check-circle\"></i> ' + json['success'] + ' <button type=\"button\" class=\"fa fa-close close\" data-dismiss=\"alert\"></button></div>');
\t\t\t\t
            setTimeout(function () {
                \$('#cart > button').html('<span id=\"cart-total\"><i class=\"fa fa-shopping-cart\"></i> ' + json['total'] + '</span>');
              }, 100);
            
\t\t\t\t\$('#cart > ul').load('index.php?route=common/cart/info ul li');
\t\t\t\t
\t\t\t\ttimer = setTimeout(function () {
\t\t\t\t\t\$('.alert').addClass('fadeOut');
\t\t\t\t}, 4000);
\t\t\t\t\$('.so-groups-sticky .popup-mycart .popup-content').load('index.php?route=extension/module/so_tools/info .popup-content .cart-header');
\t\t\t}
\t\t\t
\t\t
\t\t},
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
        }
\t});
});

//--></script> 

<script type=\"text/javascript\"><!--
\$('.date').datetimepicker({
\tlanguage: document.cookie.match(new RegExp('language=([^;]+)'))[1],
\tpickTime: false
});

\$('.datetime').datetimepicker({
\tlanguage: document.cookie.match(new RegExp('language=([^;]+)'))[1],
\tpickDate: true,
\tpickTime: true
});

\$('.time').datetimepicker({
\tlanguage: document.cookie.match(new RegExp('language=([^;]+)'))[1],
\tpickDate: false
});

\$('button[id^=\\'button-upload\\']').on('click', function() {
\tvar node = this;

\t\$('#form-upload').remove();

\t\$('body').prepend('<form enctype=\"multipart/form-data\" id=\"form-upload\" style=\"display: none;\"><input type=\"file\" name=\"file\" /></form>');

\t\$('#form-upload input[name=\\'file\\']').trigger('click');

\tif (typeof timer != 'undefined') {
\t\tclearInterval(timer);
\t}

\ttimer = setInterval(function() {
\t\tif (\$('#form-upload input[name=\\'file\\']').val() != '') {
\t\t\tclearInterval(timer);

\t\t\t\$.ajax({
\t\t\t\turl: 'index.php?route=tool/upload',
\t\t\t\ttype: 'post',
\t\t\t\tdataType: 'json',
\t\t\t\tdata: new FormData(\$('#form-upload')[0]),
\t\t\t\tcache: false,
\t\t\t\tcontentType: false,
\t\t\t\tprocessData: false,
\t\t\t\tbeforeSend: function() {
\t\t\t\t\t\$(node).button('loading');
\t\t\t\t},
\t\t\t\tcomplete: function() {
\t\t\t\t\t\$(node).button('reset');
\t\t\t\t},
\t\t\t\tsuccess: function(json) {
\t\t\t\t\t\$('.text-danger').remove();

\t\t\t\t\tif (json['error']) {
\t\t\t\t\t\t\$(node).parent().find('input').after('<div class=\"text-danger\">' + json['error'] + '</div>');
\t\t\t\t\t}

\t\t\t\t\tif (json['success']) {
\t\t\t\t\t\talert(json['success']);

\t\t\t\t\t\t\$(node).parent().find('input').val(json['code']);
\t\t\t\t\t}
\t\t\t\t},
\t\t\t\terror: function(xhr, ajaxOptions, thrownError) {
\t\t\t\t\talert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
\t\t\t\t}
\t\t\t});
\t\t}
\t}, 500);
});
//--></script> 
<script type=\"text/javascript\"><!--
\$('#review').delegate('.pagination a', 'click', function(e) {
    e.preventDefault();

    \$('#review').fadeOut('slow');
    \$('#review').load(this.href);
    \$('#review').fadeIn('slow');
});

\$('#review').load('index.php?route=product/product/review&product_id=";
        // line 1778
        echo (isset($context["product_id"]) ? $context["product_id"] : null);
        echo "');

\$('#button-review').on('click', function() {
\t\$.ajax({
\t\turl: 'index.php?route=product/product/write&product_id=";
        // line 1782
        echo (isset($context["product_id"]) ? $context["product_id"] : null);
        echo "',
\t\ttype: 'post',
\t\tdataType: 'json',
\t\tdata: \$(\"#form-review\").serialize(),
\t\tbeforeSend: function() {
\t\t\t\$('#button-review').button('loading');
\t\t},
\t\tcomplete: function() {
\t\t\t\$('#button-review').button('reset');
\t\t},
\t\tsuccess: function(json) {
\t\t\t\$('.alert-dismissible').remove();

\t\t\tif (json['error']) {
\t\t\t\t\$('#review').after('<div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ' + json['error'] + '</div>');
\t\t\t}

\t\t\tif (json['success']) {
\t\t\t\t\$('#review').after('<div class=\"alert alert-success alert-dismissible\"><i class=\"fa fa-check-circle\"></i> ' + json['success'] + '</div>');

\t\t\t\t\$('input[name=\\'name\\']').val('');
\t\t\t\t\$('textarea[name=\\'text\\']').val('');
\t\t\t\t\$('input[name=\\'rating\\']:checked').prop('checked', false);
\t\t\t}
\t\t}
\t});
});

//--></script>



<script type=\"text/javascript\"><!--
\t\$(document).ready(function() {
\t\t
\t\t// Initialize the sticky scrolling on an item 
\t\tsidebar_sticky = '";
        // line 1818
        echo (isset($context["sidebar_sticky"]) ? $context["sidebar_sticky"] : null);
        echo "';
\t\t
\t\tif(sidebar_sticky=='left'){
\t\t\t\$(\".left_column\").stick_in_parent({
\t\t\t    offset_top: 10,
\t\t\t    bottoming   : true
\t\t\t});
\t\t}else if (sidebar_sticky=='right'){
\t\t\t\$(\".right_column\").stick_in_parent({
\t\t\t    offset_top: 10,
\t\t\t    bottoming   : true
\t\t\t});
\t\t}else if (sidebar_sticky=='all'){
\t\t\t\$(\".content-aside\").stick_in_parent({
\t\t\t    offset_top: 10,
\t\t\t    bottoming   : true
\t\t\t});
\t\t}
\t\t

\t\t\$(\"#thumb-slider .image-additional\").each(function() {
\t\t\t\$(this).find(\"[data-index='0']\").addClass('active');
\t\t});
\t\t
\t\t\$('.product-options li.radio').click(function(){
\t\t\t\$(this).addClass(function() {
\t\t\t\tif(\$(this).hasClass(\"active\")) return \"\";
\t\t\t\treturn \"active\";
\t\t\t});
\t\t\t
\t\t\t\$(this).siblings(\"li\").removeClass(\"active\");
\t\t\t\$(this).parent().find('.selected-option').html('<span class=\"label label-success\">'+ \$(this).find('img').data('original-title') +'</span>');
\t\t})
\t\t
\t\t\$('.thumb-video').magnificPopup({
\t\t  type: 'iframe',
\t\t  iframe: {
\t\t\tpatterns: {
\t\t\t   youtube: {
\t\t\t\t  index: 'youtube.com/', // String that detects type of video (in this case YouTube). Simply via url.indexOf(index).
\t\t\t\t  id: 'v=', // String that splits URL in a two parts, second part should be %id%
\t\t\t\t  src: '//www.youtube.com/embed/%id%?autoplay=1' // URL that will be set as a source for iframe. 
\t\t\t\t\t},
\t\t\t\t}
\t\t\t}
\t\t});
\t});
//--></script>


<script type=\"text/javascript\">
var ajax_price = function() {
\t\$.ajax({
\t\ttype: 'POST',
\t\turl: 'index.php?route=extension/soconfig/liveprice/index',
\t\tdata: \$('.product-detail input[type=\\'text\\'], .product-detail input[type=\\'hidden\\'], .product-detail input[type=\\'radio\\']:checked, .product-detail input[type=\\'checkbox\\']:checked, .product-detail select, .product-detail textarea'),
\t\tdataType: 'json',
\t\t\tsuccess: function(json) {
\t\t\tif (json.success) {
\t\t\t\tchange_price('#price-special', json.new_price.special);
\t\t\t\tchange_price('#price-tax', json.new_price.tax);
\t\t\t\tchange_price('#price-old', json.new_price.price);
\t\t\t}
\t\t}
\t});
}

var change_price = function(id, new_price) {\$(id).html(new_price);}
\$('.product-detail input[type=\\'text\\'], .product-detail input[type=\\'hidden\\'], .product-detail input[type=\\'radio\\'], .product-detail input[type=\\'checkbox\\'], .product-detail select, .product-detail textarea, .product-detail input[name=\\'quantity\\']').on('change', function() {
\tajax_price();
});
</script>
<script>
function openColor(color) {
  var i;
  var x = document.getElementsByClassName(\"product-color-change\");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = \"none\";  
  }
  document.getElementById(color).style.display = \"block\";  
}
</script>


<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName(\"tabcontent\");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = \"none\";
  }
  tablinks = document.getElementsByClassName(\"tablinks\");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(\" active\", \"\");
  }
  document.getElementById(cityName).style.display = \"inline-block\";
  evt.currentTarget.className += \" active\";
}
</script>
<script>
 \$(document).ready(function(){
  \$('.related-carousel').owlCarousel({
    loop:true,
   autoplay:true,
    autoplayTimeout:3000,
    autoplayHoverPause:true,
    nav:true,
\tdots:false,
    responsive:{
        0:{
            items:1
        },
\t\t320:{
            items:1
        },
\t\t480:{
            items:1
        },
        600:{
            items:2
        },
\t\t767:{
            items:3
        },
\t\t991:{
            items:4
        },
        1200:{
            items:5
        }
    }
})
\$( \".owl-prev\").html('<i class=\"fa fa-lg fa-angle-left\"></i>');
 \$( \".owl-next\").html('<i class=\"fa fa-lg fa-angle-right\"></i>');
});\t

</script>
<script>
 \$(document).ready(function(){
  \$('.video-carousel').owlCarousel({
    loop:true,
   autoplay:true,
    autoplayTimeout:3000,
    autoplayHoverPause:true,
    nav:true,
\tdots:false,
    responsive:{
        0:{
            items:1
        },
\t\t320:{
            items:1
        },
\t\t480:{
            items:1
        },
        600:{
            items:2
        },
\t\t767:{
            items:2
        },
\t\t991:{
            items:3
        },
        1200:{
            items:4
        }
    }
})
\$( \".owl-prev\").html('<i class=\"fa fa-lg fa-arrow-left\"></i>');
 \$( \".owl-next\").html('<i class=\"fa fa-lg fa-arrow-right\"></i>');
});\t

</script>
<script src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyAAiQm2qLGFsER6Y96KdzQG633Pn6faIBw&callback=initMap\"></script>
<script>
\$( document ).ready(function() {
    ";
        // line 1996
        if ((isset($context["customer_pincode"]) ? $context["customer_pincode"] : null)) {
            // line 1997
            echo "       \$( \"#verify\" ).trigger( \"click\" );
    ";
        }
        // line 1999
        echo "});
</script>
\t\t<script>
\t\t\t\$(document).ready(function(){
\t\t\t    ";
        // line 2003
        if ((isset($context["customer_pincode"]) ? $context["customer_pincode"] : null)) {
            // line 2004
            echo "\t\t\t    \$('#verify').click(handler);
\t\t\t    ";
        }
        // line 2006
        echo "\t\t\t    
\t\t\t\t\$('#verify').click(handler);
\t\t\t});\t
\t\t\tfunction handler(product_id){ 
\t\t\t  var pincodevalue = \$(\"#checktext\").val();
\t\t\t\tif(pincodevalue != '' && pincodevalue.length=='6'){
\t\t\t\t    var geocoder = new google.maps.Geocoder();
\t\t\t\t    geocoder.geocode({\"address\":pincodevalue},function(results,status){
\t\t\t\t        if(status == google.maps.GeocoderStatus.OK){
        \t\t\t\t\tlatitude = results[0].geometry.location.lat();
        \t\t\t\t\tlangitude = results[0].geometry.location.lng();
        \t\t\t\t\t
        \t\t\t\t\t//console.log(pincodevalue,latitude,langitude);
        \t\t\t\t\t
        \t\t\t\t    \$.ajax({
            \t\t\t\t\ttype: \"POST\",
            \t\t\t\t\turl: \"index.php?route=product/product/checkpincode\",
            \t\t\t\t\tdata: { pincode : pincodevalue,latitude:latitude,langitude:langitude,product_id:product_id },
\t\t\t\t\t\t\t\tbeforeSend:function(){
\t\t\t\t\t\t\t\t\t\$(\"#verify\").hide();
\t\t\t\t\t\t\t\t\t\$(\"#pre_loader\").show();
\t\t\t\t\t\t\t\t},
\t\t\t\t\t\t\t\tcomplete:function(){
\t\t\t\t\t\t\t\t\t\$(\"#verify\").show();
\t\t\t\t\t\t\t\t\t\$(\"#pre_loader\").hide();
\t\t\t\t\t\t\t\t},
            \t\t\t\t\tsuccess: function(data){
            \t\t\t\t\t   
                \t\t\t\t\t\$(\".available\").css(\"display\",\"block\");
                \t\t\t\t\t\$(\"#available-text\").html(pincodevalue);
                \t\t\t\t\tvar html = '<div id=\"pin_avilability_ship\" class=\"delivery_hrs\">';
\t\t\t\t\t\t\t\t\tif(data == 21){
\t\t\t\t\t\t\t\t\t    html += '<div class=\"delivery_type\">Available</div><span class=\"delivery_text\"><span class=\"img_pin\"><img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/deliver-img1.png\" alt=\"delivery icon\">2 hours</span><span class=\"img_pin\"><img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/deliver-img2.png\" alt=\"delivery icon\">Regular</span><span class=\"img_pin\"><img class=\"pickup_store\" src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/deliver-img3.png\" alt=\"delivery icon\">Pickup@store</span></span>';
\t\t\t\t\t\t\t\t\t}if(data == 2){
\t\t\t\t\t\t\t\t\t    html += '<div class=\"delivery_type\">Available</div><span class=\"delivery_text\"><span class=\"img_pin\"><img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/deliver-img1.png\" alt=\"delivery icon\">2 hours</span><span class=\"img_pin\"><img class=\"pickup_store\" src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/deliver-img3.png\" alt=\"delivery icon\">Pickup@store</span></span>';
\t\t\t\t\t\t\t\t\t}else if(data == 1){
\t\t\t\t\t\t\t\t\t    html += '<div class=\"delivery_type\">Available</div><span class=\"delivery_text\"><span class=\"img_pin\"><img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/deliver-img2.png\" alt=\"delivery icon\">Regular</span></span>';
\t\t\t\t\t\t\t\t\t}else if(data == 3){
\t\t\t\t\t\t\t\t\t    html += '<div class=\"delivery_type\">Available</div><span class=\"delivery_text\" style=\"color:#ff6161;\">Currently out of stock in this pincode.</span>';
\t\t\t\t\t\t\t\t\t}else if(data == 0){
\t\t\t\t\t\t\t\t\t    html += '<div class=\"delivery_type\">Available</div><span class=\"delivery_text\">Out Of Stock</span>';
\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\thtml += '</div>'; 
                \t\t\t\t\t
                \t\t\t\t\t\$(\"#pin_avilability_ship\").replaceWith(html);
        \t\t\t\t\t    }
        \t\t\t\t    });
    \t\t\t\t\t    
    \t\t\t\t
\t\t\t\t        }else{
\t\t\t\t           \$(\"#pin_avilability_ship\").html(\"Delivery Not Available\"); 
\t\t\t\t        }
\t\t\t\t    });
    \t\t\t\t}else{
    \t\t\t\t\t\$(\"#pin_avilability_ship\").html(\"<span class='invalid_pin'>Invalid pincode</span>\");
\t\t\t\t\t }
\t\t\t\t
\t\t\t\t\t
\t\t\t\t\t}
\t\t\t\t\t\$('#checktext').keyup(function() {
\t\t\t\t\t  if (\$(this).val().length == 0) {
\t\t\t\t\t\t\$('#pin_avilability_ship').hide();
\t\t\t\t\t  }
\t\t\t\t\t}).keyup();
\t\t\t\t</script>

";
        // line 2072
        echo (isset($context["footer"]) ? $context["footer"] : null);
        echo " 
";
    }

    public function getTemplateName()
    {
        return "so-destino/template/product/product.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  3763 => 2072,  3695 => 2006,  3691 => 2004,  3689 => 2003,  3683 => 1999,  3679 => 1997,  3677 => 1996,  3496 => 1818,  3457 => 1782,  3450 => 1778,  3325 => 1655,  3317 => 1651,  3266 => 1602,  3259 => 1601,  3255 => 1598,  3241 => 1585,  3235 => 1584,  3228 => 1583,  3224 => 1582,  3212 => 1572,  3206 => 1571,  3199 => 1570,  3195 => 1569,  3183 => 1559,  3177 => 1558,  3170 => 1557,  3166 => 1556,  3154 => 1546,  3148 => 1545,  3141 => 1544,  3137 => 1543,  3125 => 1533,  3119 => 1532,  3112 => 1531,  3108 => 1530,  3095 => 1519,  3093 => 1518,  3083 => 1512,  3070 => 1501,  3064 => 1500,  3057 => 1499,  3053 => 1498,  3039 => 1486,  3033 => 1485,  3026 => 1484,  3022 => 1483,  3008 => 1471,  3002 => 1470,  2995 => 1469,  2991 => 1468,  2976 => 1455,  2973 => 1453,  2896 => 1377,  2894 => 1376,  2874 => 1357,  2868 => 1356,  2861 => 1355,  2857 => 1354,  2835 => 1334,  2829 => 1333,  2822 => 1332,  2818 => 1331,  2796 => 1311,  2790 => 1310,  2783 => 1309,  2779 => 1308,  2745 => 1281,  2738 => 1280,  2734 => 1279,  2726 => 1278,  2719 => 1277,  2715 => 1276,  2707 => 1275,  2700 => 1274,  2696 => 1273,  2688 => 1272,  2681 => 1271,  2677 => 1270,  2669 => 1269,  2662 => 1268,  2658 => 1267,  2639 => 1250,  2637 => 1249,  2578 => 1191,  2576 => 1190,  2550 => 1165,  2544 => 1164,  2537 => 1163,  2533 => 1162,  2512 => 1143,  2506 => 1142,  2499 => 1141,  2495 => 1140,  2474 => 1121,  2468 => 1120,  2461 => 1119,  2457 => 1118,  2436 => 1099,  2430 => 1098,  2423 => 1097,  2419 => 1096,  2393 => 1072,  2391 => 1071,  2384 => 1067,  2379 => 1064,  2377 => 1063,  2370 => 1059,  2366 => 1058,  2362 => 1057,  2358 => 1056,  2354 => 1055,  2350 => 1054,  2346 => 1053,  2340 => 1049,  2333 => 1048,  2328 => 1044,  2318 => 1038,  2314 => 1035,  2312 => 1034,  2305 => 1030,  2301 => 1029,  2297 => 1027,  2294 => 1026,  2286 => 1019,  2280 => 1017,  2278 => 1016,  2275 => 1015,  2269 => 1012,  2266 => 1011,  2264 => 1010,  2261 => 1009,  2256 => 1006,  2250 => 1004,  2241 => 1000,  2235 => 997,  2230 => 995,  2217 => 985,  2213 => 984,  2205 => 979,  2200 => 977,  2192 => 972,  2188 => 971,  2184 => 969,  2182 => 968,  2178 => 967,  2173 => 964,  2171 => 963,  2166 => 960,  2159 => 956,  2155 => 955,  2151 => 953,  2149 => 952,  2143 => 949,  2139 => 948,  2135 => 947,  2123 => 942,  2120 => 941,  2114 => 937,  2106 => 935,  2104 => 934,  2101 => 933,  2095 => 931,  2093 => 930,  2090 => 929,  2084 => 927,  2082 => 926,  2079 => 925,  2073 => 923,  2071 => 922,  2066 => 920,  2063 => 919,  2060 => 918,  2053 => 912,  2045 => 910,  2043 => 909,  2040 => 908,  2034 => 906,  2032 => 905,  2029 => 904,  2023 => 902,  2021 => 901,  2018 => 900,  2012 => 898,  2010 => 897,  2004 => 894,  2001 => 893,  1998 => 892,  1984 => 890,  1980 => 888,  1977 => 887,  1972 => 886,  1967 => 885,  1964 => 884,  1961 => 883,  1958 => 882,  1953 => 879,  1950 => 878,  1947 => 877,  1944 => 876,  1941 => 875,  1938 => 874,  1935 => 873,  1932 => 872,  1929 => 871,  1926 => 870,  1923 => 869,  1920 => 868,  1917 => 867,  1915 => 866,  1910 => 865,  1908 => 864,  1906 => 863,  1902 => 861,  1896 => 858,  1893 => 857,  1890 => 856,  1885 => 852,  1878 => 846,  1866 => 841,  1863 => 840,  1856 => 839,  1854 => 838,  1851 => 837,  1841 => 836,  1837 => 835,  1833 => 834,  1830 => 833,  1828 => 832,  1823 => 829,  1817 => 826,  1814 => 825,  1812 => 824,  1808 => 822,  1802 => 820,  1800 => 819,  1788 => 810,  1782 => 807,  1775 => 802,  1768 => 801,  1765 => 800,  1761 => 799,  1757 => 798,  1746 => 797,  1742 => 796,  1739 => 795,  1735 => 794,  1728 => 793,  1725 => 792,  1714 => 791,  1710 => 790,  1706 => 789,  1702 => 788,  1699 => 787,  1691 => 781,  1687 => 780,  1680 => 775,  1674 => 771,  1663 => 769,  1659 => 768,  1655 => 767,  1648 => 764,  1646 => 763,  1642 => 761,  1639 => 760,  1632 => 758,  1619 => 752,  1612 => 750,  1605 => 749,  1603 => 748,  1600 => 747,  1587 => 741,  1580 => 739,  1573 => 738,  1571 => 737,  1568 => 736,  1555 => 730,  1548 => 728,  1541 => 727,  1539 => 726,  1536 => 725,  1528 => 722,  1520 => 721,  1516 => 720,  1509 => 719,  1507 => 718,  1504 => 717,  1492 => 714,  1486 => 713,  1479 => 712,  1477 => 711,  1474 => 710,  1462 => 707,  1456 => 706,  1449 => 705,  1447 => 704,  1444 => 703,  1437 => 698,  1428 => 693,  1422 => 690,  1418 => 688,  1407 => 683,  1400 => 682,  1396 => 681,  1392 => 680,  1388 => 678,  1379 => 677,  1375 => 676,  1369 => 675,  1363 => 674,  1358 => 672,  1353 => 670,  1349 => 669,  1343 => 668,  1340 => 667,  1337 => 666,  1335 => 665,  1331 => 664,  1327 => 663,  1320 => 662,  1318 => 661,  1315 => 660,  1308 => 655,  1299 => 650,  1295 => 649,  1289 => 646,  1285 => 644,  1267 => 639,  1263 => 638,  1260 => 637,  1251 => 636,  1247 => 635,  1241 => 634,  1235 => 633,  1230 => 631,  1225 => 629,  1221 => 628,  1215 => 627,  1212 => 626,  1209 => 625,  1207 => 624,  1203 => 623,  1199 => 622,  1192 => 621,  1190 => 620,  1187 => 619,  1182 => 616,  1175 => 614,  1168 => 612,  1166 => 611,  1159 => 610,  1155 => 609,  1151 => 608,  1145 => 607,  1139 => 606,  1132 => 605,  1130 => 604,  1127 => 603,  1123 => 602,  1119 => 600,  1113 => 597,  1096 => 584,  1087 => 578,  1062 => 556,  1053 => 550,  1041 => 540,  1024 => 527,  1013 => 519,  1001 => 510,  988 => 500,  983 => 498,  977 => 495,  970 => 490,  956 => 487,  952 => 486,  948 => 485,  944 => 484,  940 => 483,  932 => 480,  928 => 479,  924 => 478,  919 => 476,  915 => 475,  910 => 472,  907 => 471,  904 => 470,  901 => 469,  889 => 458,  885 => 456,  877 => 454,  875 => 453,  869 => 452,  866 => 451,  864 => 450,  857 => 445,  853 => 443,  846 => 441,  834 => 437,  831 => 436,  827 => 435,  821 => 433,  817 => 432,  813 => 430,  811 => 429,  771 => 398,  767 => 397,  691 => 323,  687 => 322,  675 => 320,  669 => 319,  664 => 317,  660 => 315,  654 => 312,  650 => 310,  643 => 308,  638 => 306,  633 => 303,  627 => 301,  624 => 300,  620 => 298,  617 => 297,  607 => 290,  601 => 289,  597 => 287,  590 => 283,  586 => 281,  581 => 279,  576 => 278,  573 => 277,  569 => 276,  560 => 275,  556 => 274,  552 => 273,  548 => 271,  544 => 270,  535 => 269,  531 => 268,  527 => 267,  523 => 266,  520 => 265,  518 => 264,  512 => 260,  508 => 257,  502 => 255,  500 => 254,  497 => 253,  495 => 252,  492 => 251,  487 => 248,  481 => 246,  479 => 245,  475 => 244,  471 => 242,  465 => 241,  458 => 240,  454 => 239,  446 => 233,  441 => 231,  437 => 230,  432 => 229,  430 => 228,  427 => 227,  425 => 226,  423 => 225,  417 => 222,  410 => 218,  406 => 217,  401 => 215,  395 => 211,  393 => 210,  386 => 206,  383 => 205,  380 => 203,  365 => 192,  360 => 189,  357 => 188,  354 => 187,  351 => 186,  349 => 185,  346 => 184,  343 => 183,  341 => 182,  338 => 181,  335 => 180,  333 => 179,  330 => 178,  327 => 177,  325 => 176,  322 => 175,  319 => 174,  317 => 173,  312 => 170,  309 => 168,  307 => 167,  302 => 166,  296 => 161,  287 => 158,  284 => 157,  281 => 156,  278 => 154,  275 => 153,  272 => 152,  269 => 151,  266 => 150,  263 => 149,  260 => 148,  257 => 147,  254 => 146,  251 => 145,  248 => 144,  245 => 143,  242 => 142,  239 => 141,  236 => 140,  233 => 139,  230 => 138,  225 => 135,  222 => 134,  220 => 133,  217 => 132,  214 => 131,  211 => 130,  208 => 129,  205 => 128,  202 => 127,  199 => 126,  196 => 125,  193 => 124,  190 => 123,  187 => 122,  185 => 121,  179 => 119,  177 => 118,  174 => 117,  168 => 113,  165 => 112,  163 => 111,  160 => 110,  156 => 109,  151 => 108,  148 => 107,  145 => 106,  140 => 105,  137 => 104,  134 => 103,  129 => 102,  126 => 101,  123 => 100,  119 => 99,  19 => 1,);
    }
}
/* {{ header }}*/
/* */
/* */
/* 	<link rel="stylesheet" type="text/css" href="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/css/owl.carousel.min.css" />*/
/* <link rel="stylesheet" type="text/css" href="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/css/owl.theme.default.min.css" />*/
/* <style>*/
/* 	.product-product{*/
/* 		margin-top: 120px;*/
/* 	}*/
/*     .product-view .content-product-right h3{*/
/*         color: #666 !important;*/
/*         margin-top: 20px;*/
/*     }*/
/*     .pl-8{*/
/*         padding-left: 8px;*/
/*     }*/
/*     .attrcolor{*/
/*         color: #000;*/
/*     }*/
/*     .stock b{*/
/*           color: #16a904;*/
/*     background: #E9F5EB;*/
/*     border-radius: 20px;*/
/*     padding: 8px 13px;*/
/*     }*/
/*     .cont-right h4 {*/
/*     padding: 10px 0px 10px 20px;*/
/* }*/
/*     .stock b i {*/
/*         margin-right: 2px;*/
/*         color: inherit;*/
/*         font-size: 14px;*/
/*         vertical-align: middle;*/
/*     }*/
/*     .stock {*/
/*     display: inline-block;*/
/*     position: relative;*/
/*     top: -6px;*/
/* }*/
/*     .pl-20{*/
/*         padding-left: 20px;*/
/*     }*/
/*     .product-color-show{*/
/*         padding-top: 15px !important;*/
/*     }*/
/*     .cont-right h4 {*/
/*     padding-left: 20px;*/
/* }*/
/*     #checktext{*/
/*         margin-left: 10px;*/
/*     }*/
/*     #pin_avilability_ship {*/
/*         display: flex;*/
/*         display: -webkit-flex;*/
/*         display: -ms-flexbox;*/
/*         align-items: center;*/
/*         justify-content: flex-start;*/
/*         margin-top: 10px !important;*/
/*         margin-left: 0 !important;*/
/*     }*/
/*     .delivery_type {*/
/*         min-width: 90px;*/
/*         text-transform: capitalize;*/
/*         font-weight: 500 !important;*/
/*         color: rgb(102, 102, 102) !important;*/
/*         font-size: 14px !important;*/
/*     }*/
/*     .delivery_text, .invalid_pin {*/
/*         font-weight: 500 !important;*/
/*         font-style: italic;*/
/*     }*/
/*     .delivery_text {*/
/*         display: flex;*/
/*         display: -webkit-flex;*/
/*         display: -ms-flexbox;*/
/*         align-items: center;*/
/*         justify-content: flex-start;*/
/*     }*/
/*     span.img_pin {*/
/*         display: flex;*/
/*         display: -webkit-flex;*/
/*         display: -ms-flexbox;*/
/*         align-items: flex-start;*/
/*         flex-direction: column;*/
/*         justify-content: flex-start;*/
/*         font-size: 12px;*/
/*         color: #777;*/
/*         font-style: italic;*/
/*         margin-right: 10px;*/
/*     }*/
/*     .delivery_hrs img {*/
/*         width: 70px;*/
/*         margin-right: 5px;*/
/*     }*/
/* </style>*/
/* */
/* */
/* {#====  Variables url parameter ==== #}*/
/* {% if url_asidePosition %}{% set col_position = url_asidePosition %}*/
/* {% else %}{% set col_position = soconfig.get_settings('catalog_col_position') %}{% endif %}*/
/* */
/* {% if url_asideType %} {% set col_canvas = url_asideType %}*/
/* {% else %}{% set col_canvas = soconfig.get_settings('catalog_col_type') %}{% endif %}*/
/* */
/* {% if url_productGallery %} {% set productGallery = url_productGallery %}*/
/* {% else %}{% set productGallery = soconfig.get_settings('thumbnails_position') %}{% endif %}*/
/* */
/* {% if url_sidebarsticky %} {% set sidebar_sticky = url_sidebarsticky %}*/
/* {% else %} {% set sidebar_sticky = soconfig.get_settings('catalog_sidebar_sticky') %}{% endif %}*/
/* */
/* {% set desktop_canvas = col_canvas =='off_canvas' ? 'desktop-offcanvas' : '' %}*/
/* */
/* <div class="content-main container product-detail  {{desktop_canvas}}">*/
/* 	<div class="row">*/
/* 		*/
/* 		{#==== Column Left Outside ==== #}*/
/* */
/* 		{% if col_position== 'outside' %}*/
/* 			{{ column_left }}*/
/* 			*/
/* 			{% if col_canvas =='off_canvas' %}*/
/* 				{% set class_pos = 'col-sm-12' %}*/
/* 	    	{% elseif column_left and column_right %}*/
/* 	    		{% set class_pos = 'col-md-6 col-xs-12 fluid-allsidebar' %}*/
/* 		    {% elseif column_left or column_right %}*/
/* 		    	{% set class_pos = 'col-md-9 col-sm-12 col-xs-12 fluid-sidebar' %}*/
/* 		    {% else %}*/
/* 		    	{% set class_pos = 'col-sm-12' %}*/
/* 		    {% endif %}*/
/* 		{% else %}*/
/* 			{% set class_pos = 'col-sm-12' %}*/
/* 		{% endif %}*/
/* 		{#==== End Column Outside ==== #}*/
/*     	*/
/* 		<div id="content" class="product-view {{class_pos}}"> */
/* 		*/
/* 		{#====  Product Gallery ==== #}*/
/* 		{% if productGallery =='grid' %}*/
/* 			{% set class_left_gallery  = 'col-md-6 col-sm-12 col-xs-12' %}*/
/* 			{% set class_right_gallery = 'col-md-6 col-sm-12 col-xs-12' %}*/
/* 		{% elseif productGallery =='list' %}*/
/* 			{% set class_left_gallery  = 'col-md-5 col-sm-12 col-xs-12' %}*/
/* 			{% set class_right_gallery = 'col-md-7 col-sm-12 col-xs-12' %}*/
/* 		{% elseif productGallery =='left' %}*/
/* 			{% set class_left_gallery  = 'col-md-6 col-sm-12 col-xs-12' %}*/
/* 			{% set class_right_gallery = 'col-md-6 col-sm-12 col-xs-12' %}*/
/* 			{% elseif productGallery =='bottom' %}*/
/* 		{% set class_left_gallery  = 'col-md-5 col-sm-12 col-xs-12' %}*/
/* 			{% set class_right_gallery = 'col-md-7 col-sm-12 col-xs-12' %}*/
/* 		{% else %}*/
/* 			{% set class_left_gallery  = 'col-md-12 col-sm-12 col-xs-12' %}*/
/* 			{% set class_right_gallery = 'col-md-12 col-sm-12 col-xs-12 col-gallery-slider' %}*/
/* 		{% endif %}*/
/* */
/* 		{#====  Button Sidebar canvas==== #}*/
/* 		{% if column_left or column_right %}*/
/* 			{% set class_canvas = col_canvas =='off_canvas' ? '' : 'hidden-lg hidden-md' %}*/
/* 			<a href="javascript:void(0)" class=" open-sidebar {{class_canvas}}"><i class="fa fa-bars"></i>{{ text_sidebar }}</a>*/
/* 			<div class="sidebar-overlay "></div>*/
/* 		{% endif %}*/
/* */
/* */
/* 		<div class="content-product-mainheader clearfix"> */
/* 			<div class="row">	*/
/* 			{#========== Product Left ============#}*/
/* 			<div class="content-product-left  {{ class_left_gallery }}" >*/
/* 				{% if images %}*/
/* 					<div class="so-loadeding" ></div>*/
/* 					{#==== Gallery -  Thumbnails ==== #}*/
/* 				*/
/*  */
/* */
/* 					{% if productGallery=='left' %}*/
/* 					 	{% include theme_directory~'/template/product/gallery/gallery-left.twig' %}*/
/* */
/* 					{% elseif productGallery=='bottom' %}*/
/* 						{% include theme_directory~'/template/product/gallery/gallery-bottom.twig' %}*/
/* */
/* 					{% elseif productGallery=='grid' %}*/
/* 						{% include theme_directory~'/template/product/gallery/gallery-grid.twig' %}*/
/* */
/* 					{% elseif productGallery=='list' %}*/
/* 						{% include theme_directory~'/template/product/gallery/gallery-list.twig' %}*/
/* */
/* 					{% elseif productGallery=='slider' %}*/
/* 						{% include theme_directory~'/template/product/gallery/gallery-slider.twig' %}*/
/* 					{% endif %}*/
/* 				{% endif %}*/
/* 				  */
/* 					<div class="col-md-12 pl-0 pr-0">  */
/* 			<div class="cart">*/
/* 			    <input type="button" value="{{ button_cart }}" data-loading-text="{{ text_loading }}" id="button-cart" class="btn btn-mega btn-lg btn-product-page">*/
/* 			 </div>*/
/* 			*/
/* 			 	<div class="buynow">*/
/* 			    <input type="button" value="Buy Now" id="button-cart" class="btn btn-mega btn-lg btn-product-buy">*/
/* 			 </div>*/
/* 			*/
/* 			 </div>*/
/* 			</div>*/
/* 		*/
/*         	{#========== //Product Left ============#}*/
/* */
/* 			{#========== Product Right ============#}*/
/* 			*/
/* 			<div class="content-product-right {{ class_right_gallery }}" itemprop="offerDetails" itemscope itemtype="http://schema.org/Product">*/
/* 			    <div class="cont-right">*/
/* 			  */
/* 			    <div class="product-breadcrumb">*/
/* 			    {% include theme_directory~'/template/soconfig/breadcrumbs.twig' %}*/
/*               <span>Add to compare <input type="checkbox" id="product-compare"></span>*/
/*               </div>*/
/* */
/* 				<div class="title-product">*/
/* 						 <h1 itemprop="name">{{heading_title}}</h1>*/
/* 						 <ul class="product-share-links">*/
/* 						     <li><span>Wishlist</span><a onclick="wishlist.add({{ product_id }});"><i class="fa fa-heart"></i></a></li>*/
/* 						     <li><a onclick="compare.add({{ product_id }});"><i class="fa fa-share-alt"></i></a></li>*/
/* 						     </ul>*/
/* 					</div>*/
/* 				<div class="title-product">*/
/* 					<h3>Product Code : {{ item_code }}</h3>*/
/* 				</div>*/
/* 				*/
/* 				{% if review_status %}*/
/* 					{#======== Review - Rating ========== #}*/
/* 					<div class="box-review"  itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">*/
/* 						{% if count_reviews %}*/
/* 								<meta itemprop="ratingValue" content="{{rating}}">*/
/* 								<meta itemprop="ratingCount" content="{{count_reviews}}">*/
/* 								<meta itemprop="reviewCount" content="{{count_reviews}}">*/
/* 						{% endif %}*/
/* 						*/
/* 						<div class="rating">*/
/* 						    <div class="rating-show">*/
/* 						        <h6>4.5</h6>*/
/* 						    </div>*/
/* 							<div class="rating-box">*/
/* 							{% for i in 1..5 %}*/
/* 								{% if rating < i %}<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>{% else %}<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>{% endif %}*/
/* 							{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/* 						<a class="reviews_button" href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;">{{ reviews }}</a>*/
/* 						{% if soconfig.get_settings('product_order') %}*/
/* 									<span class="order-num">{{orders}}</span>*/
/* 						{% endif %}*/
/* 					*/
/* 					</div>*/
/* 					{% endif %}*/
/* */
/* 				{% if price %}*/
/* */
/*                 {% if (text_discount_applied is defined and text_discount_applied) %}*/
/*                 <h4><span class="bg-warning text-warning">{{ text_discount_applied }}</span></h4>*/
/*                 {% endif %}*/
/*                 */
/*                 */
/* 					{#========= Product - Price ========= #}*/
/* 					 <div class="col-md-12 pl-0" style="margin-bottom: 10px;">*/
/*                    */
/*                */
/* 					<div class="product_page_price price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">*/
/* 						{% if not special %}*/
/* 							<span class="price-new">*/
/* 								<span itemprop="price" content="{{ price_value }}" id="price-old"> */
/*  {% if (cfp_setting.module_so_call_for_price_status and price_0 <= 0) %} */
/*  {% if cfp_setting.module_so_call_for_price_replace_cart is defined and cfp_setting.module_so_call_for_price_replace_cart == '0' %} */
/*  <a data-fancybox data-type="ajax" data-src="{{ base }}index.php?route=extension/module/so_call_for_price&product_id={{ product.product_id }}" href="javascript:;" class="callforprice" style="color: #ff0000; font-weight: bold; font-size: 18px;"><i class="fa fa-phone" style="font-size: 18px;"></i> {{ text_price_0 }}</a> */
/*  {% endif %} */
/*  {% else %} */
/*   */
/*  {% if (cfp_setting.module_so_call_for_price_status and price_0 <= 0) %} */
/*  {% if cfp_setting.module_so_call_for_price_replace_cart is defined and cfp_setting.module_so_call_for_price_replace_cart == '0' %} */
/*  <a data-fancybox data-type="ajax" data-src="{{ base }}index.php?route=extension/module/so_call_for_price&product_id={{ product.product_id }}" href="javascript:;" class="callforprice" style="color: #ff0000; font-weight: bold;"><i class="fa fa-phone"></i> {{ text_price_0 }}</a> */
/*  {% endif %} */
/*  {% else %} */
/*  {{ price }} */
/*  {% endif %} */
/*   */
/*  {% endif %} */
/*  </span>*/
/* 								<meta itemprop="priceCurrency" content="{{currency}}" />*/
/* 							</span>*/
/* */
/* 						{% else %}*/
/* 						*/
/* 							<span class="price-new">*/
/* 								<span itemprop="price" content="{{special_value}}" id="price-special">{{ special }}</span>*/
/* 								<meta itemprop="priceCurrency" content="{{currency}}" />*/
/* 							</span>*/
/* 						   <span class="price-old" id="price-old"> */
/* 								*/
/* 						   </span>*/
/* 						   */
/* 						{% endif %}*/
/* 						*/
/* 						{% if special and soconfig.get_settings('discount_status')   %} */
/* 						{#=======Discount Label======= #}*/
/* 						<span class="label-product label-sale">*/
/* 							 {{ discount }}*/
/* 						</span>*/
/* 						{% endif %} */
/* */
/* 						 */
/*  {% if ((tax) and (cfp_setting.module_so_call_for_price_status) and (price_0 > 0)) %} */
/*  */
/* 							<div class="price-tax"><span>{{ text_tax }}</span> <span id="price-tax"> {{ tax }} </span></div>*/
/* 						{% endif %}*/
/* 					 */
/* 					</div>*/
/* 					 <div class="stock ptb-10 pl-20"><b> <i class="fa fa-check-circle"></i> {{ stock }}</b></div>*/
/* 					 </div>*/
/* 					{% endif %}*/
/* 					*/
/* */
/* 				{% if discounts %} */
/* 					<ul class="list-unstyled text-success">*/
/* 					{% for discount in discounts %} */
/* 						<li><strong>{{ discount.quantity }} {{ text_discount }} {{ discount.price }}</strong> </li>*/
/* 					{% endfor %}*/
/* 					</ul>*/
/* 				{% endif %} */
/* 				*/
/* 			   */
/*                 	<div class="col-md-12">*/
/* 					    <div class="product-storage">*/
/* 					        <h3>Storage</h3>*/
/* 					        <ul>*/
/* 					            <li><strong>8GB+128GB</strong></li>*/
/* 					            <li><strong>8GB+256GB</strong></li>*/
/* 					            <li><strong>8GB+512GB</strong></li>*/
/* 					        </ul>*/
/* 					    </div>*/
/* 					</div>*/
/* 				<div class="col-md-12 pl-0">*/
/* 				    */
/* 				     */
/* 					    <div class="product-color">*/
/* 					        <h3>Color:</h3>*/
/* 					        <div id="color-1" class="product-color-change tabcontent product-color-show">*/
/* 					            <span>White</span>*/
/* 					        </div>*/
/* 					        <div id="color-2" class="product-color-change tabcontent color2">*/
/* 					            <span>Black</span>*/
/* 					        </div>*/
/* 					        <div id="color-3" class="product-color-change tabcontent color3">*/
/* 					            <span>Green</span>*/
/* 					        </div>*/
/* 					        <div id="color-4" class="product-color-change tabcontent color4">*/
/* 					            <span>Blue</span>*/
/* 					        </div>*/
/* 					        <ul>*/
/* 					            <li><div class="hover-color hover-color1"><span class="product-hover-color">White</span></div><button class="tablinks"  onclick="openCity(event, 'color-1')"><img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/product-color1.jpg"></button><strong>White</strong></li>*/
/* 					            <li><div class="hover-color hover-color2"><span class="product-hover-color">Black</span></div><button  class="tablinks" onclick="openCity(event, 'color-2')"><img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/product-color2.jpg"></button><strong>Black</strong></li>*/
/* 					            <li><div class="hover-color hover-color3"><span class="product-hover-color">Green</span></div><button  class="tablinks" onclick="openCity(event, 'color-3')"><img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/product-color3.jpg"></button><strong>Green</strong></li>*/
/* 					            <li><div class="hover-color hover-color4"><span class="product-hover-color">Blue</span></div><button  class="tablinks" onclick="openCity(event, 'color-4')"><img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/product-color4.jpg"></button><strong>Blue</strong></li>*/
/* 					        </ul>*/
/* 					    </div>*/
/* 					</div>*/
/* 				*/
/* 				*/
/* 					<div class="col-md-12">*/
/* 					<div class="Product-offers">*/
/* 					    <h3>Available Offers</h3>*/
/* 					    <ul>*/
/* 					        <li><i class="fa fa-percent"></i><strong>Bank Offer</strong>flat 30% discount</li>*/
/* 					        <li><i class="fa fa-percent"></i><strong>Bank Offer</strong>5% offer in Axis bank</li>*/
/* 					    </ul>*/
/* 					</div>*/
/* 					</div>*/
/* 						<div class="col-md-12">*/
/* 					    <div class="product-exchange">*/
/* 					        <a href="#">*/
/* 					        <i class="fa fa-exchange"></i>*/
/* 					        <span>With exchange<strong>Up to &#x20B9; 14000</strong> <i class="fa fa-angle-right"></i></span>*/
/* 					        */
/* 					        </a>*/
/* 					    </div>*/
/* 					</div>*/
/* 					<div class="col-md-12">*/
/* 					    <div class="product-exchange product-emi">*/
/* 					        <a href="#">*/
/* 					        <i class="fa fa-percent"></i>*/
/* 					        <span>No cost EMI @<strong> &#x20B9; 4000/month</strong> <i class="fa fa-angle-right"></i></span>*/
/* 					        */
/* 					        </a>*/
/* 					    </div>*/
/* 					</div>*/
/* 					<div class="col-md-12">*/
/* 					<div class="product-delivery">*/
/* 					    <div class="delivery-box">*/
/* 					    <span>Delivery</span>*/
/* 					   */
/* 					    <div class="pincode">*/
/* 					        <span><i class="fa fa-map-marker"></i></span> */
/* 					        <input type="text" id="checktext" maxlength="6" placeholder="Enter Delivery Pincode" name="pincode" value="{{ customer_pincode }}">*/
/* 					        <strong><a id="verify" onclick="handler({{ product_id }})">{% if customer_pincode %} Change {% else %} Check {% endif %}</a></strong><span id="pre_loader"> <img src="https://s1.poorvikamobile.com/image/data/poorvika_mobile_five/loader.gif" alt="Loading"></span>*/
/* 					   */
/* 					   </div>*/
/* 					   </div>*/
/* 					   */
/* 					    <ul>*/
/* 					        <li><img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/deliver-img1.png"><span>2 hours</span></li>*/
/* 					        <li><img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/deliver-img2.png"><span>Regular</span></li>*/
/* 					        <li><img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/deliver-img3.png"><span>Pickup@store</span></li>*/
/* 					   </ul>*/
/* 					    */
/* 					</div>*/
/* 					<div class="available" style="display:none">*/
/* 						<div class="delivery_note hidden">*/
/* 							<div class="d_note"><span>Available Shipping and Payments for</span> <span id="available-text"></span></div>*/
/* 						</div>*/
/* 					</div>*/
/* 					<div class="clearfix"></div>*/
/* 					<div id="pin_avilability_ship" class="delivery_hrs">*/
/* 					</div>*/
/* 					</div>*/
/* 				*/
/* 					<div id="prod_specs" class="col-md-12 pr-0 pl-0">*/
/* 					    <div class="specs-block">*/
/* 					    <div class="product-specification">*/
/* 					         <h2><img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/product-spec.png">Specifications</h2>*/
/* 					    </div>*/
/* 					    */
/* 					<div class="product-box-desc">*/
/* 					<div class="inner-box-desc">*/
/* 			            */
/* 			            {% if attribute_groups %}*/
/* 							*/
/* 			              	<ul class="product-property-list util-clearfix">*/
/* 				                {% for attribute_group in attribute_groups %}*/
/* 				                    <h3 class="product-property-title" > {{attribute_group.name}}</h3>*/
/* 				                	*/
/* 					                {% for attribute in attribute_group.attribute %}*/
/* 					                <li class="property-item">*/
/* 					                    <div class="model ptb-10 pl-8"><span>{{ attribute.name }} </span> <span class="attrcolor">{{ attribute.text }}</span></div>*/
/* 					                  */
/* 					                </li>*/
/* 					                {% endfor %}*/
/* 				                 	*/
/* 				                {% endfor %}*/
/* 			              	</ul>*/
/* 			            {% endif %}*/
/* 							*/
/* 					</div>	*/
/* 					*/
/* 					*/
/* */
/* 					{% if soconfig.get_settings('product_enablesold')   %}*/
/* 					<div class="inner-box-sold ">*/
/* 						<div class="viewed"><span>{{ text_viewed }}</span> <span class="label label-primary">{{ viewed }}</span></div>	*/
/* 						{% if sold %}*/
/* 						<div class="sold"><span>{{ text_sold_ready }}</span> <span class="label label-success"> {{ sold }} </span></div>	*/
/* 						{% endif %}*/
/* 					</div>	*/
/* 					{% endif %}*/
/* 					*/
/* 					*/
/* */
/* 				</div>*/
/* 				</div>*/
/* 				*/
/* 				</div>*/
/* 				*/
/* 					*/
/* */
/* 				{#===== Show CountDown Product =======#}*/
/* 				{% if soconfig.get_settings('countdown_status') and special_end_date %}*/
/* 					{% include theme_directory~'/template/soconfig/countdown.twig' with {product: product,special_end_date:special_end_date} %}*/
/* 				{% endif %}*/
/* 				*/
/* 				*/
/* 				<div id="product">	*/
/* 					{% if options %} */
/* 					<h3>{{ text_option }}</h3>*/
/*  */
/*  {% if option_data and option_data.product_option_value is defined and option_data.product_option_value %} */
/*  <ul id="so-colorswatch-selector-{{ product_id }}" class='so-colorswatch-productpage-icons'> */
/*  {% for option_value in option_data.product_option_value %} */
/*  <li class="option-item"> */
/*  <a class="" */
/*  data-product-option-value-id="{{ option_value.product_option_value_id }}" */
/*  data-option-value-id="{{ option_value.option_value_id }}" */
/*  data-color-image="{{ option_value.color_image }}" */
/*  data-color-thumb-image="{{ option_value.color_thumb_image }}" */
/*  style="width: {{ width_product_page }}px; height: {{ height_product_page }}px; background-image: url('{{ option_value.image }}')"> */
/*  </a> */
/*  </li> */
/*  {% endfor %} */
/*  <li class="selected-option"><span></span></li> */
/*  </ul> */
/*  <script type="text/javascript"> */
/*  var $window_width = $(window).width(); */
/*  var ProductOptionId = '{{ product_option_id }}'; */
/*  var default_image = $('.large-image img').attr('src'); */
/*  jQuery(document).ready(function($) { */
/*  $('#input-option{{ product_option_id }}').parent().hide(); */
/*  */
/*  $('#input-option{{ product_option_id}} option').each(function(){ */
/*  var text = $(this).text().replace(/\s{2,}/g, ' '); */
/*  var val = $(this).attr('value'); */
/*  $('.so-colorswatch-productpage-icons li a').each(function(index, el){ */
/*  if($(el).data('product-option-value-id')== val){ */
/*  $(el).attr('title', text); */
/*  } */
/*  }) */
/*  }) */
/*  */
/*  {% if colorswatch_type == 'click' %} */
/*  $(document).on('click', '.so-colorswatch-productpage-icons li.option-item', function(e){ */
/*  e.preventDefault(); */
/*  var option_value_id = $(this).children('a').data('product-option-value-id'); */
/*  var option_id = $(this).children('a').data('option-value-id'); */
/*  */
/*  if ($(this).hasClass('checked')) { */
/*  $('.so-colorswatch-productpage-icons li.option-item').removeClass('checked'); */
/*  $(this).removeClass('checked'); */
/*  $('#input-option{{ product_option_id }}').val('').trigger('change'); */
/*  $('.so-colorswatch-productpage-icons li.selected-option > span').html(''); */
/*  */
/*  $('.large-image img').attr('src', default_image); */
/*  } */
/*  else { */
/*  $('.so-colorswatch-productpage-icons li.option-item').removeClass('checked'); */
/*  $(this).removeClass('checked').addClass('checked'); */
/*  $('#input-option{{ product_option_id }}').val(option_value_id).trigger('change'); */
/*  $('.so-colorswatch-productpage-icons li.selected-option > span').html($(this).children('a').attr('title')); */
/*  */
/*  if ($(this).children('a').data('color-image') != '') { */
/*  $('.large-image img').attr('src', $(this).children('a').data('color-image')); */
/*  } */
/*  else { */
/*  $('.large-image img').attr('src', default_image); */
/*  } */
/*  */
/*  $('#thumb-slider a.thumbnail').removeClass('active'); */
/*  } */
/*  }) */
/*  {% else %} */
/*  if ($window_width > 1199) { */
/*  $('.so-colorswatch-productpage-icons li.option-item').hover(function(e){ */
/*  e.preventDefault(); */
/*  var option_value_id = $(this).children('a').data('product-option-value-id'); */
/*  var option_id = $(this).children('a').data('option-value-id'); */
/*  */
/*  $('.so-colorswatch-productpage-icons li.option-item').removeClass('checked'); */
/*  if ($(this).hasClass('checked')) { */
/*  $(this).removeClass('checked'); */
/*  $('#input-option{{ product_option_id }}').val('').trigger('change'); */
/*  $('.large-image img').attr('src', default_image); */
/*  */
/*  } */
/*  else { */
/*  $(this).removeClass('checked').addClass('checked'); */
/*  $('#input-option{{ product_option_id }}').val(option_value_id).trigger('change'); */
/*  $('.so-colorswatch-productpage-icons li.selected-option > span').html($(this).children('a').attr('title')); */
/*  */
/*  if ($(this).children('a').data('color-image') != '') { */
/*  $('.large-image img').attr('src', $(this).children('a').data('color-image')); */
/*  } */
/*  else { */
/*  $('.large-image img').attr('src', default_image); */
/*  } */
/*  $('#thumb-slider a.thumbnail').removeClass('active'); */
/*  } */
/*  }); */
/*  } */
/*  else { */
/*  $(document).on('click', '.so-colorswatch-productpage-icons li.option-item', function(e){ */
/*  e.preventDefault(); */
/*  var option_value_id = $(this).children('a').data('product-option-value-id'); */
/*  var option_id = $(this).children('a').data('option-value-id'); */
/*  */
/*  $('.so-colorswatch-productpage-icons li.option-item').removeClass('checked'); */
/*  if ($(this).hasClass('checked')) { */
/*  $(this).removeClass('checked'); */
/*  $('#input-option{{ product_option_id }}').val('').trigger('change'); */
/*  $('.large-image img').attr('src', default_image); */
/*  */
/*  } */
/*  else { */
/*  $(this).removeClass('checked').addClass('checked'); */
/*  $('#input-option{{ product_option_id }}').val(option_value_id).trigger('change'); */
/*  $('.so-colorswatch-productpage-icons li.selected-option > span').html($(this).children('a').attr('title')); */
/*  */
/*  if ($(this).children('a').data('color-image') != '') { */
/*  $('.large-image img').attr('src', $(this).children('a').data('color-image')); */
/*  } */
/*  else { */
/*  $('.large-image img').attr('src', default_image); */
/*  } */
/*  $('#thumb-slider a.thumbnail').removeClass('active'); */
/*  } */
/*  }) */
/*  } */
/*  {% endif %} */
/*  }) */
/*  </script> */
/*  {% endif %} */
/*  */
/* 					{% for option in options %}*/
/* 						*/
/* 						{% if option.type == 'select' %}*/
/* 						<div class="form-group{% if option.required %} required {% endif %}">*/
/* 							<label class="control-label" for="input-option{{ option.product_option_id }}">{{ option.name }}</label>*/
/* 							<select name="option[{{ option.product_option_id }}]" id="input-option{{ option.product_option_id }}" class="form-control width50">*/
/* 								<option value="">{{ text_select }}</option>*/
/* 							{% for option_value in option.product_option_value %}*/
/* 								<option value="{{ option_value.product_option_value_id }}">{{ option_value.name }}*/
/* 								{% if option_value.price %}*/
/* 									({{ option_value.price_prefix }}{{ option_value.price }})*/
/* 								{% endif %}*/
/* 								</option>*/
/* 							{% endfor %}*/
/* 						  </select>*/
/* 						</div>*/
/* 						{% endif %}*/
/* 						*/
/* 						{% if option.type == 'radio' %}*/
/* 						<div class="form-group{% if option.required %} required {% endif %}">*/
/* 						  	<label class="control-label">{{ option.name }}</label>*/
/* 							<div id="input-option{{ option.product_option_id }}">*/
/* 								{% set radio_style 	 = soconfig.get_settings('radio_style') %}*/
/* 								{% set radio_type 	 = radio_style ? ' radio-type-button':'' %}*/
/* */
/* 								{% for option_value in option.product_option_value %} */
/* 								{% set radio_image 	=  option_value.image ? 'option_image' : '' %} */
/* 								{% set radio_price 	=  radio_style ? option_value.price_prefix ~ option_value.price : '' %} */
/* 								*/
/* 									<div class="radio {{ radio_image ~ radio_type }}">*/
/* 										<label>							*/
/* 											<input type="radio" name="option[{{ option.product_option_id }}]" value="{{ option_value.product_option_value_id }}" />*/
/* 											<span class="option-content-box" data-title="{{ option_value.name}} {{ radio_price }}" data-toggle='tooltip'>*/
/* 												{% if option_value.image %} */
/* 													<img src="{{ option_value.image }} " alt="{{ option_value.name}}  {{radio_price}}" /> */
/* 												{% endif %} */
/* 												<span class="option-name">{{ option_value.name }} </span>*/
/* 												{% if option_value.price  and  radio_style  != '1' %} ({{ option_value.price_prefix }} {{ option_value.price }} ){% endif %} */
/* 											  */
/* 											</span>*/
/* 										</label>*/
/* 									</div>*/
/* 								{% endfor %}	*/
/* 								 */
/* 								{% if radio_style %} */
/* 								<script type="text/javascript">*/
/* 									 $(document).ready(function(){*/
/* 										  $('#input-option{{ option.product_option_id }} ').on('click', 'span', function () {*/
/* 											   $('#input-option{{ option.product_option_id }}  span').removeClass("active");*/
/* 											   $(this).toggleClass("active");*/
/* 										  });*/
/* 									 });*/
/* 								</script>*/
/* 								{% endif %} */
/* */
/* 							</div>*/
/* 						</div>*/
/* 						{% endif %}*/
/* */
/* 						{% if option.type == 'checkbox' %}*/
/* 						<div class="form-group{% if option.required %} required {% endif %}">*/
/* 						  	<label class="control-label">{{ option.name }}</label>*/
/* 						  	<div id="input-option{{ option.product_option_id }}">*/
/* 								{% set radio_style 	 = soconfig.get_settings('radio_style') %}*/
/* 								{% set radio_type 	 = radio_style ? ' radio-type-button':'' %}*/
/* */
/* 								{% for option_value in option.product_option_value %} */
/* 								{% set radio_image 	=  option_value.image ? 'option_image' : '' %} */
/* 								{% set radio_price 	=  radio_style ? option_value.price_prefix ~ option_value.price : '' %} */
/* 								*/
/* 									<div class="checkbox  {{ radio_image ~ radio_type }}">*/
/* 										<label>*/
/* 											<input type="checkbox" name="option[{{ option.product_option_id }}][]" value="{{ option_value.product_option_value_id }}" />*/
/* 											<span class="option-content-box" data-title="{{ option_value.name}} {{ radio_price }}" data-toggle='tooltip'>*/
/* 												{% if option_value.image %} */
/* 													<img src="{{ option_value.image }} " alt="{{ option_value.name}}  {{radio_price}}" /> */
/* 												{% endif %} */
/* */
/* 												<span class="option-name">{{ option_value.name }} </span>*/
/* 												{% if option_value.price  and  radio_style  != '1' %} */
/* 													({{ option_value.price_prefix }} {{ option_value.price }} )*/
/* 												{% endif %} */
/* 											  */
/* 											</span>*/
/* 										</label>*/
/* 									</div>*/
/* 								{% endfor %}	*/
/* 								 */
/* 								{% if radio_style %} */
/* 								<script type="text/javascript">*/
/* 									 $(document).ready(function(){*/
/* 										  $('#input-option{{ option.product_option_id }} ').on('click', 'span', function () {*/
/* 											   $(this).toggleClass("active");*/
/* 										  });*/
/* 									 });*/
/* 								</script>*/
/* 								{% endif %} */
/* */
/* 							</div>*/
/* 						</div>*/
/* 						{% endif %}*/
/* */
/* 						{% if option.type == 'text' %}*/
/* 						<div class="form-group{% if option.required %} required {% endif %}">*/
/* 						  <label class="control-label" for="input-option{{ option.product_option_id }}">{{ option.name }}</label>*/
/* 						  <input type="text" name="option[{{ option.product_option_id }}]" value="{{ option.value }}" placeholder="{{ option.name }}" id="input-option{{ option.product_option_id }}" class="form-control" />*/
/* 						</div>*/
/* 						{% endif %}*/
/* */
/* 						{% if option.type == 'textarea' %}*/
/* 						<div class="form-group{% if option.required %} required {% endif %}">*/
/* 						  <label class="control-label" for="input-option{{ option.product_option_id }}">{{ option.name }}</label>*/
/* 						  <textarea name="option[{{ option.product_option_id }}]" rows="5" placeholder="{{ option.name }}" id="input-option{{ option.product_option_id }}" class="form-control">{{ option.value }}</textarea>*/
/* 						</div>*/
/* 						{% endif %}*/
/* */
/* 						{% if option.type == 'file' %}*/
/* 						<div class="form-group{% if option.required %} required {% endif %}">*/
/* 						  <label class="control-label">{{ option.name }}</label>*/
/* 						  <button type="button" id="button-upload{{ option.product_option_id }}" data-loading-text="{{ text_loading }}" class="btn btn-default btn-block"><i class="fa fa-upload"></i> {{ button_upload }}</button>*/
/* 						  <input type="hidden" name="option[{{ option.product_option_id }}]" value="" id="input-option{{ option.product_option_id }}" />*/
/* 						</div>*/
/* 						{% endif %}*/
/* */
/* 						{% if option.type == 'date' %}*/
/* 						<div class="form-group{% if option.required %} required {% endif %}">*/
/* 						  <label class="control-label" for="input-option{{ option.product_option_id }}">{{ option.name }}</label>*/
/* 						  <div class="input-group date">*/
/* 							<input type="text" name="option[{{ option.product_option_id }}]" value="{{ option.value }}" data-date-format="YYYY-MM-DD" id="input-option{{ option.product_option_id }}" class="form-control" />*/
/* 							<span class="input-group-btn">*/
/* 							<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>*/
/* 							</span></div>*/
/* 						</div>*/
/* 						{% endif %}*/
/* */
/* 						{% if option.type == 'datetime' %}*/
/* 						<div class="form-group{% if option.required %} required {% endif %}">*/
/* 						  <label class="control-label" for="input-option{{ option.product_option_id }}">{{ option.name }}</label>*/
/* 						  <div class="input-group datetime">*/
/* 							<input type="text" name="option[{{ option.product_option_id }}]" value="{{ option.value }}" data-date-format="YYYY-MM-DD HH:mm" id="input-option{{ option.product_option_id }}" class="form-control" />*/
/* 							<span class="input-group-btn">*/
/* 							<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>*/
/* 							</span></div>*/
/* 						</div>*/
/* 						{% endif %}*/
/* 						*/
/* 						{% if option.type == 'time' %}*/
/* 						<div class="form-group{% if option.required %} required {% endif %}">*/
/* 							<label class="control-label" for="input-option{{ option.product_option_id }}">{{ option.name }}</label>*/
/* 							<div class="input-group time">*/
/* 							<input type="text" name="option[{{ option.product_option_id }}]" value="{{ option.value }}" data-date-format="HH:mm" id="input-option{{ option.product_option_id }}" class="form-control" />*/
/* 							<span class="input-group-btn">*/
/* 							<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>*/
/* 							</span></div>*/
/* 						</div>*/
/* 						{% endif %}*/
/* 						*/
/* 					{% endfor %}*/
/* 					{% endif %}*/
/* */
/* 					<div class="box-cart clearfix form-group">*/
/* 						{% if recurrings %}*/
/* 						<h3>{{ text_payment_recurring }}</h3>*/
/* 						<div class="form-group required">*/
/* 							<select name="recurring_id" class="form-control">*/
/* 							<option value="">{{ text_select }}</option>*/
/* 							{% for recurring in recurrings %}*/
/* 							<option value="{{ recurring.recurring_id }}">{{ recurring.name }}</option>*/
/* 							{% endfor %}*/
/* 							</select>*/
/* 						  <div class="help-block" id="recurring-description"></div>*/
/* 						</div>*/
/* 						{% endif %}*/
/* 					  */
/* 						<div class="form-group box-info-product">*/
/* 							<div class="option quantity">*/
/* 								<div class="input-group quantity-control">*/
/* 									  <span class="input-group-addon product_quantity_down fa fa-minus"></span>*/
/* 									  <input class="form-control" type="text" name="quantity" value="{{ minimum }}" />*/
/* 									  <input type="hidden" name="product_id" value="{{ product_id }}" />								  */
/* 									  <span class="input-group-addon product_quantity_up fa fa-plus"></span>*/
/* 								</div>*/
/* 							</div>*/
/* 							<div class="detail-action">*/
/* 								{# =========button Cart ======#}*/
/* 								<div class="cart"> */
/*  {% if (cfp_setting.module_so_call_for_price_status and price_0 <= 0) %} */
/*  {% if cfp_setting.module_so_call_for_price_hide_cart is defined and cfp_setting.module_so_call_for_price_hide_cart == '0' %} */
/*  {% if cfp_setting.module_so_call_for_price_replace_cart is defined and cfp_setting.module_so_call_for_price_replace_cart == '1' %} */
/*  <input type="button" value="{{ text_price_0 }}" data-fancybox data-type="ajax" data-src="{{ base }}index.php?route=extension/module/so_call_for_price&product_id={{ product.product_id }}" data-loading-text="{{ text_loading }}" class="btn btn-mega btn-lg callforprice"> */
/*  {% else %} */
/*  <input type="button" value="{{ button_cart }}" data-loading-text="{{ text_loading }}" class="btn btn-mega btn-lg" style="cursor: default; background: #eee; color: #ccc; border: 1px solid #eee; text-shadow: none; box-shadow: none;"> */
/*  {% endif %} */
/*  {% else %} */
/*  {% if cfp_setting.module_so_call_for_price_replace_cart is defined and cfp_setting.module_so_call_for_price_replace_cart == '1' %} */
/*  <input type="button" value="{{ text_price_0 }}" data-fancybox data-type="ajax" data-src="{{ base }}index.php?route=extension/module/so_call_for_price&product_id={{ product.product_id }}" data-loading-text="{{ text_loading }}" class="btn btn-mega btn-lg "> */
/*  {% endif %} */
/*  {% endif %} */
/*  {% else %} */
/*  <input type="button" value="{{ button_cart }}" data-loading-text="{{ text_loading }}" id="button-cart" class="btn btn-mega btn-lg" /> */
/*  {% endif %} */
/*  </div>*/
/* 								<div class="add-to-links wish_comp">*/
/* 									<ul class="blank">*/
/* 										<li class="wishlist">*/
/* 											<a onclick="wishlist.add({{ product_id }});"><i class="fa fa-heart"></i></a>*/
/* 										</li>*/
/* 										<li class="compare">*/
/* 											<a onclick="compare.add({{ product_id }});"><i class="fa fa-retweet"></i></a>*/
/* 										</li>*/
/* 										*/
/* 									</ul>*/
/* 								</div>*/
/* 							</div>*/
/* 						</div>*/
/* */
/* 						<div class="clearfix"></div>*/
/* 						{% if minimum > 1 %}*/
/* 							<div class="alert alert-info"><i class="fa fa-info-circle"></i> {{ text_minimum }}</div>*/
/* 						{% endif %}*/
/* 					</div>*/
/* */
/* 					{% if soconfig.get_settings('product_page_button') and soconfig.get_settings('product_socialshare') %}*/
/* 					<div class="form-group social-share clearfix">*/
/* 						{{ soconfig.decode_entities( soconfig.get_settings('product_socialshare') ) }}*/
/* 					</div>*/
/* 					{% endif %}*/
/* 					<!-- Go to www.addthis.com/dashboard to customize your tools -->*/
/* 					<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-529be2200cc72db5"></script>*/
/* 					*/
/* 					 {% if tags %}*/
/* 					<div id="tab-tags">*/
/* 						{{ text_tags }}*/
/* 						{% for i in 0..tags|length %}*/
/* 						{% if i < (tags|length - 1) %} <a class="btn btn-primary btn-sm" href="{{ tags[i].href }}">{{ tags[i].tag }}</a>*/
/* 						{% else %} */
/* 						{% if tags[i] is not empty  %}*/
/* 						<a class="btn btn-primary btn-sm 22" href="{{ tags[i].href }}">{{ tags[i].tag }}</a> {% endif %}*/
/* 						{% endif %}*/
/* 						{% endfor %} */
/* 						*/
/* 					 */
/* 					</div>*/
/* 					{% endif %}*/
/* */
/* 				</div>*/
/* 					</div>*/
/* 			</div>*/
/* 		*/
/* 			{#========== //Product Right ============#}*/
/* 			</div>*/
/* 		</div>*/
/* */
/* 		{#====  content_Top==== #}*/
/* 		{% if content_top %}*/
/* 		<div class="content-product-maintop form-group clearfix">*/
/* 			{{ content_top }}*/
/* 		</div>*/
/* 		{% endif %}*/
/* 		<div class="content-product-mainbody clearfix row">*/
/* 			*/
/* 			{% if col_position== 'inside' %}*/
/* 			{#====  Column left inside==== #}*/
/* 				{{ column_left }}*/
/* 			    {% if col_canvas =='off_canvas' %}*/
/* 					{% set class_left = 'col-sm-12' %}*/
/* 		    	{% elseif column_left and column_right %}*/
/* 		    		{% set class_left = 'col-md-6 col-column3' %}*/
/* 			    {% elseif column_left or column_right %}*/
/* 			    	{% set class_left = 'col-md-9 col-sm-12 col-xs-12' %}*/
/* 			    {% else %}*/
/* 			    	{% set class_left = 'col-sm-12' %}*/
/* 			    {% endif %}*/
/* 			{% else %}*/
/* 				{% set class_left = 'col-sm-12' %}*/
/* 			{% endif %}*/
/* */
/* 		    <div class="content-product-content {{ class_left }}">*/
/* 				<div class="content-product-midde clearfix">*/
/* 					{#========== TAB BLOCK ============#}*/
/* 					{% set related_position = soconfig.get_settings('tabs_position') == 1 ? 'vertical-tabs' : ''  %}*/
/* 					{% set tabs_position	= soconfig.get_settings('tabs_position')  %}*/
/* 					{% set showmore			= soconfig.get_settings('product_enableshowmore')  %}*/
/* 					{% if showmore %} {% set class_showmore = 'showdown' %}*/
/* 					{% else %} {% set class_showmore = 'showup' %}*/
/* 					{% endif %}*/
/* */
/* 					<div class="producttab ">*/
/* 						<div class="tabsslider {{related_position}} {% if tabs_position == 1 %} {{'vertical-tabs'}} {% else %} {{'horizontal-tabs'}} {% endif %} col-xs-12">*/
/* 							{#========= Tabs - Bottom horizontal =========#}*/
/* 							{% if tabs_position == 2 %}*/
/* 							<ul class="nav nav-tabs font-sn">*/
/* 								<li class="active"><a data-toggle="tab" href="#tab-description">{{ tab_description }}</a></li>*/
/* 								*/
/* 					         */
/* 					            {% if review_status %}*/
/* 					           	 <li><a href="#tab-review" data-toggle="tab">{{ tab_review }}</a></li>*/
/* 					            {% endif %}*/
/* 								*/
/* 								{% if soconfig.get_settings('product_enableshipping') %}*/
/* 								 <li><a href="#tab-contentshipping" data-toggle="tab">{{ tab_shipping}}</a></li>*/
/* 								{% endif %}*/
/* */
/* 								{% if product_tabtitle %}*/
/* 					           	 <li><a href="#tab-customhtml" data-toggle="tab">{{ product_tabtitle}}</a></li>*/
/* 					            {% endif %}*/
/* */
/* 								{% if product_video %}*/
/* 					           	 <li><a class="thumb-video" href="{{product_video}}"><i class="fa fa-youtube-play fa-lg"></i> {{ tab_video}}</a></li>*/
/* 					            {% endif %}*/
/* 								*/
/* 								*/
/* 								*/
/* 							</ul>*/
/* */
/* 							{#========= Tabs - Left vertical =========#}*/
/* 							{% elseif tabs_position == 1  %}*/
/* 								<ul class="nav nav-tabs col-lg-3 col-sm-4">*/
/* 								<li class="active"><a data-toggle="tab" href="#tab-description">{{ tab_description }}</a></li>*/
/* 								*/
/* 					            {% if review_status %}*/
/* 					           	 <li><a href="#tab-review" data-toggle="tab">{{ tab_review }}</a></li>*/
/* 					            {% endif %}*/
/* 								*/
/* 								{% if soconfig.get_settings('product_enableshipping')  %}*/
/* 								 <li><a href="#tab-contentshipping" data-toggle="tab">{{ tab_shipping}}</a></li>*/
/* 								{% endif %}*/
/* */
/* 								{% if product_tabtitle %}*/
/* 					           	 <li><a href="#tab-customhtml" data-toggle="tab">{{ product_tabtitle}}</a></li>*/
/* 					            {% endif %}*/
/* 					            */
/* 								{% if product_video %}*/
/* 					           	 <li><a class="thumb-video" href="{{product_video}}"><i class="fa fa-youtube-play fa-lg"></i> {{ tab_video}}</a></li>*/
/* 					            {% endif %}*/
/* 								*/
/* 								*/
/* 								</ul>*/
/* 							{% endif %}*/
/* */
/* 							<div class="tab-content {% if tabs_position == 1  %} {{ 'col-lg-9 col-sm-8' }} {% endif %} col-xs-12">*/
/* 								<div class="tab-pane active" id="tab-description">*/
/* 									*/
/* 									*/
/* */
/* 						            <h3 class="product-property-title" > {{text_product_description}}</h3>*/
/* 						            <div id="collapse-description" class="desc-collapse {{class_showmore}}">*/
/* 										{{ description }}*/
/* 									</div>	*/
/* */
/* 									{% if showmore %}*/
/* 									<div class="button-toggle">*/
/* 								         <a class="showmore" data-toggle="collapse" href="#" aria-expanded="false" aria-controls="collapse-footer">*/
/* 								            <span class="toggle-more">{{ objlang.get('show_more') }} <i class="fa fa-angle-down"></i></span> */
/* 								            <span class="toggle-less">{{ objlang.get('show_less') }} <i class="fa fa-angle-up"></i></span>           */
/* 										</a>        */
/* 									</div>*/
/* 									{% endif %}*/
/* 								</div>*/
/* 								*/
/* */
/* 					            {% if review_status %}*/
/* 					            <div class="tab-pane" id="tab-review">*/
/* 						            <form class="form-horizontal" id="form-review">*/
/* 						                <div id="review"></div>*/
/* 						                <h3>{{ text_write }}</h3>*/
/* 						                {% if review_guest %}*/
/* 						                <div class="form-group required">*/
/* 						                  <div class="col-sm-12">*/
/* 						                    <label class="control-label" for="input-name">{{ entry_name }}</label>*/
/* 						                    <input type="text" name="name" value="{{ customer_name }}" id="input-name" class="form-control" />*/
/* 						                  </div>*/
/* 						                </div>*/
/* 						                <div class="form-group required">*/
/* 						                  <div class="col-sm-12">*/
/* 						                    <label class="control-label" for="input-review">{{ entry_review }}</label>*/
/* 						                    <textarea name="text" rows="5" id="input-review" class="form-control"></textarea>*/
/* 						                    <div class="help-block">{{ text_note }}</div>*/
/* 						                  </div>*/
/* 						                </div>*/
/* 						                <div class="form-group required">*/
/* 						                  <div class="col-sm-12">*/
/* 						                    <label class="control-label">{{ entry_rating }}</label>*/
/* 						                    &nbsp;&nbsp;&nbsp; {{ entry_bad }}&nbsp;*/
/* 						                    <input type="radio" name="rating" value="1" />*/
/* 						                    &nbsp;*/
/* 						                    <input type="radio" name="rating" value="2" />*/
/* 						                    &nbsp;*/
/* 						                    <input type="radio" name="rating" value="3" />*/
/* 						                    &nbsp;*/
/* 						                    <input type="radio" name="rating" value="4" />*/
/* 						                    &nbsp;*/
/* 						                    <input type="radio" name="rating" value="5" />*/
/* 						                    &nbsp;{{ entry_good }}</div>*/
/* 						                </div>*/
/* 						                {{ captcha }}*/
/* 						                */
/* 						                  <div class="pull-right">*/
/* 						                    <button type="button" id="button-review" data-loading-text="{{ text_loading }}" class="btn btn-primary">{{ button_continue }}</button>*/
/* 						                  </div>*/
/* 						               */
/* 						                {% else %}*/
/* 						                {{ text_login }}*/
/* 						                {% endif %}*/
/* 						            </form>*/
/* 					            </div>*/
/* 					            {% endif %}*/
/* */
/* 					            {% if soconfig.get_settings('product_enableshipping') and soconfig.get_settings('product_contentshipping') %}*/
/* 								<div class="tab-pane" id="tab-contentshipping">*/
/* 									{{ soconfig.decode_entities( soconfig.get_settings('product_contentshipping') ) }}*/
/* 								</div>*/
/* 								{% endif %}*/
/* */
/* 								{% if product_tabtitle %}*/
/* 								<div class="tab-pane " id="tab-customhtml">{{ product_tabcontent }}</div>*/
/* 								{% endif %}*/
/* 								*/
/* 							</div>*/
/* 						</div>*/
/* 					</div>*/
/* 				</div>*/
/* 				*/
/* 				{#====  Related_Product==== #}*/
/* 				{% if products and soconfig.get_settings('related_status') %}*/
/* 				<div class="content-product-bottom clearfix">*/
/* 					<ul class="nav nav-tabs">*/
/* 					  <li class="active"><a data-toggle="tab" href="#product-related">{{ text_related }}</a></li> */
/* 					  <li><a data-toggle="tab" href="#product-upsell">{{ text_upsell }}</a></li>*/
/* 					</ul>*/
/* 					<div class="tab-content">*/
/* 					  	<div id="product-related" class="tab-pane fade in active">*/
/* 							{% include theme_directory~'/template/soconfig/related_product.twig' %}*/
/* 					  	</div>*/
/* 					  	<div id="product-upsell" class="tab-pane fade">*/
/* 					  		{#====  content_bottom==== #}*/
/* 					  		{{ content_bottom }}*/
/* 					  	</div>*/
/* 					</div>*/
/* 					*/
/* 				</div>*/
/* 				{% endif %}*/
/* */
/* 				*/
/* 			</div>*/
/* 			{#====  Column Right inside==== #}*/
/* 			{% if col_position== 'inside' %} {{ column_right }} {% endif %}*/
/* */
/* 		</div>*/
/* 		 <div class="tabs-nav">*/
/* 			        <ul>*/
/* 			            <li><i class="fa fa-dot-circle-o"></i><a href="{{ current_url }}#prod_overview">Overview</a></li>*/
/* 			            <li><i class="fa fa-gears"></i><a href="{{ current_url }}#prod_specs">Specs</a></li>*/
/* 			            <li><i class="fa fa-video-camera"></i><a href="{{ current_url }}#prod_video">Video</a></li>*/
/* 			            <li><i class="fa fa-star"></i><a href="{{ current_url }}#prod_review">Reviews</a></li>*/
/* 			            <li><i class="fa fa-cube"></i><a href="{{ current_url }}#prod_related">Related product</a></li>*/
/* 			            <li><i class="fa fa-exchange"></i><a href="{{ current_url }}#prod_compare">Compare product</a></li>*/
/* 			            <li><i class="fa fa-commenting"></i><a href="{{ current_url }}#prod_question">Question & Answer</a></li>*/
/* 			        </ul>*/
/* 			    </div>*/
/* 		<div class="content-product-main1">*/
/* 		    {% if description %}*/
/* 					 <div id="prod_overview" class="short_description form-group" itemprop="description">*/
/* 						<h3>Overview</h3>*/
/* 						*/
/* 		                    {{ description }}  */
/* 		                */
/* 					</div>*/
/* 					{% endif %}*/
/* 		    {#====  Comparision-product==== #}*/
/* 		    <div class="col-md-12">*/
/* 		        <div class="product-combo compare-product" id="prod_compare">*/
/* 			        <div class="combo-title">*/
/* 			            <h2>Compate With similar Products</h2>*/
/* 			            <a href=""><i class="fa fa-plus"></i>Add Comparison</a>*/
/* 			        </div>*/
/* 			        <div id="collapse-description" class="desc-collapse showdown compare">*/
/* 			        <ul class="">*/
/* 			            <li><div class="free-space"></div>*/
/* 			            <div class="battery-power">*/
/* 			                <strong>Battery</strong>*/
/* 			            </div>*/
/* 			            <div class="external-storage">*/
/* 			                <strong>Expandable storage</strong>*/
/* 			            </div>*/
/* 			             <div class="external-storage">*/
/* 			                <strong>Expandable storage</strong>*/
/* 			            </div>*/
/* 			            </li>*/
/* 			             <li><a href="">*/
/* 			                <div class="combo-list"><img src="http://poorvikabeta.webindia.com/image/cache/catalog/demo/canon_eos_5d_1-355x470.jpg"></div>*/
/* 			                <div class="combo-content">*/
/* 			                    <h5>Canon EOS 5D</h5>*/
/* 			                    <strong>4.5*/
/* 			                    {% for i in 1..5 %}*/
/* 								{% if rating < i %}<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>{% else %}<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>{% endif %}*/
/* 							{% endfor %}*/
/* 							</strong> */
/* 							<small class="price">&#8377; 45,000<strike class="old-price">25,000</strike></small>*/
/* 							</div>*/
/* 			            </a>*/
/* 			             <div class="battery-power">*/
/* 			                <strong>4000 mAH</strong>*/
/* 			            </div>*/
/* 			            <div class="external-storage">*/
/* 			                <strong>128GB</strong>*/
/* 			            </div>*/
/* 			             <div class="external-storage">*/
/* 			                <strong>128GB</strong>*/
/* 			            </div>*/
/* 			            </li>*/
/* 			             <li><a href="">*/
/* 			                <div class="combo-list"><img src="http://poorvikabeta.webindia.com/image/cache/catalog/demo/canon_eos_5d_1-355x470.jpg"></div>*/
/* 			                <div class="combo-content">*/
/* 			                    <h5>Canon EOS 5D</h5>*/
/* 			                    <strong>4.5*/
/* 			                    {% for i in 1..5 %}*/
/* 								{% if rating < i %}<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>{% else %}<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>{% endif %}*/
/* 							{% endfor %}*/
/* 							</strong> */
/* 							<small class="price">&#8377; 45,000<strike class="old-price">25,000</strike></small>*/
/* 							</div>*/
/* 			            </a>*/
/* 			              <div class="battery-power">*/
/* 			                <strong>4000 mAH</strong>*/
/* 			            </div>*/
/* 			            <div class="external-storage">*/
/* 			                <strong>128GB</strong>*/
/* 			            </div>*/
/* 			             <div class="external-storage">*/
/* 			                <strong>128GB</strong>*/
/* 			            </div>*/
/* 			            </li>*/
/* 			             <li><a href="">*/
/* 			                <div class="combo-list"><img src="http://poorvikabeta.webindia.com/image/cache/catalog/demo/canon_eos_5d_1-355x470.jpg"></div>*/
/* 			                <div class="combo-content">*/
/* 			                    <h5>Canon EOS 5D</h5>*/
/* 			                    <strong>4.5*/
/* 			                    {% for i in 1..5 %}*/
/* 								{% if rating < i %}<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>{% else %}<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>{% endif %}*/
/* 							{% endfor %}*/
/* 							</strong> */
/* 							<small class="price">&#8377; 45,000<strike class="old-price">25,000</strike></small>*/
/* 							</div>*/
/* 			            </a>*/
/* 			             <div class="battery-power">*/
/* 			                <strong>4000 mAH</strong>*/
/* 			            </div>*/
/* 			            <div class="external-storage">*/
/* 			                <strong>128GB</strong>*/
/* 			            </div>*/
/* 			             <div class="external-storage">*/
/* 			                <strong>128GB</strong>*/
/* 			            </div>*/
/* 			            </li>*/
/* 			             <li><a href="">*/
/* 			                <div class="combo-list"><img src="http://poorvikabeta.webindia.com/image/cache/catalog/demo/canon_eos_5d_1-355x470.jpg"></div>*/
/* 			                <div class="combo-content">*/
/* 			                    <h5>Canon EOS 5D</h5>*/
/* 			                    <strong>4.5*/
/* 			                    {% for i in 1..5 %}*/
/* 								{% if rating < i %}<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>{% else %}<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>{% endif %}*/
/* 							{% endfor %}*/
/* 							</strong> */
/* 							<small class="price">&#8377; 45,000<strike class="old-price">25,000</strike></small>*/
/* 							</div>*/
/* 			            </a>*/
/* 			             <div class="battery-power">*/
/* 			                <strong>4000 mAH</strong>*/
/* 			            </div>*/
/* 			            <div class="external-storage">*/
/* 			                <strong>128GB</strong>*/
/* 			            </div>*/
/* 			             <div class="external-storage">*/
/* 			                <strong>128GB</strong>*/
/* 			            </div>*/
/* 			            </li>*/
/* 			            */
/* 			             */
/* 			            */
/* 			        </ul>*/
/* 			        </div>*/
/* 			       */
/* 			       */
/* 									<div class="gallery-button details-button"><a href="" class="btn btn-gallary btn-detail">See Image Gallery</a></div>*/
/* 			    </div>*/
/* 		    </div>*/
/* 		    {#==== End-Comparision-product==== #}*/
/* 		    {#====product-video==== #}*/
/* 		    <div class="col-md-12">*/
/* 		        <div id="prod_video" class="product-combo product-video">*/
/* 		       <div class="combo-title">*/
/* 			            <h2><img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/video-product.png">Video</h2>*/
/* 			             <a href=""><i class="fa fa-youtube"></i>Watch YouTube Reviews</a>*/
/* 			        </div>*/
/* 			        */
/* 			        <ul class="video-carousel owl-carousel owl-theme">*/
/* 			            <li>*/
/* 			                <div class="video-frame">*/
/* 			                  <iframe width="200" height="115" src="https://www.youtube.com/embed/tgbNymZ7vqY"></iframe>*/
/*                             </div>*/
/*                             <div class="video-content">*/
/*                                 <span>4.6<img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/video-green-arrow.png"></span>*/
/*                                 <strong>performance<small>Ultra fast</small></strong>*/
/*                             </div>*/
/*                        </li>*/
/*                         <li>*/
/* 			                <div class="video-frame">*/
/* 			                  <iframe width="200" height="115" src="https://www.youtube.com/embed/tgbNymZ7vqY"></iframe>*/
/*                             </div>*/
/*                              <div class="video-content">*/
/*                                 <span>4.6<img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/video-green-arrow.png"></span>*/
/*                                 <strong>performance<small>Ultra fast</small></strong>*/
/*                             </div>*/
/*                        </li>*/
/*                         <li>*/
/* 			                <div class="video-frame">*/
/* 			                   <iframe width="200" height="115" src="https://www.youtube.com/embed/tgbNymZ7vqY"></iframe>*/
/*                             </div>*/
/*                              <div class="video-content">*/
/*                                 <span>4.6<img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/video-green-arrow.png"></span>*/
/*                                 <strong>performance<small>Ultra fast</small></strong>*/
/*                             </div>*/
/*                        </li>*/
/*                         <li>*/
/* 			                <div class="video-frame">*/
/* 			                   <iframe width="200" height="115" src="https://www.youtube.com/embed/tgbNymZ7vqY"></iframe>*/
/*                             </div>*/
/*                              <div class="video-content">*/
/*                                 <span>4.6<img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/video-green-arrow.png"></span>*/
/*                                 <strong>performance<small>Ultra fast</small></strong>*/
/*                             </div>*/
/*                        </li>*/
/*                         <li>*/
/* 			                <div class="video-frame">*/
/* 			                  <iframe width="200" height="115" src="https://www.youtube.com/embed/tgbNymZ7vqY"></iframe>*/
/*                             </div>*/
/*                              <div class="video-content">*/
/*                                 <span>4.6<img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/video-green-arrow.png"></span>*/
/*                                 <strong>performance<small>Ultra fast</small></strong>*/
/*                             </div>*/
/*                        </li>*/
/* 			        </ul>*/
/* 			        */
/* 			        </div>*/
/* 		    </div>*/
/* 		     {#====End-product-video==== #}*/
/* 		      {#====product-review==== #}*/
/* 		     <div id="prod_review" class="col-md-12">*/
/* 		         <div class="product-question review-product">*/
/* 		              <div class="combo-title">*/
/* 			            <h2><img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/video-product.png">Customer Review</h2>*/
/* 			             <a href=""><i class="fa fa-edit"></i>Write Product Reviews</a>*/
/* 			          </div>*/
/* 		         </div>*/
/* 		     </div>*/
/* 		     <div class="col-md-5  pr-0">*/
/* 		         <div class="reviwe-block">*/
/* 		         <div class="review-card">*/
/* 		             <div class="card-cont">*/
/* 		             <span>Rating<strong>4.6<small>out of 5</small></strong></span>*/
/* 		             <small>35 Ratings 4 Reviews</small>*/
/* 		             </div>*/
/* 		             <div class="review-rating">*/
/* 		                 <ul>*/
/* 		                     <li> {% for i in 1..5 %}*/
/* 								{% if rating < i %}<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>{% else %}<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>{% endif %}*/
/* 							{% endfor %}<small>75%</small></li>*/
/* 							<li> {% for i in 1..4 %}*/
/* 								{% if rating < i %}<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>{% else %}<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>{% endif %}*/
/* 							{% endfor %}<small>75%</small></li>*/
/* 								<li> {% for i in 1..3 %}*/
/* 								{% if rating < i %}<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>{% else %}<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>{% endif %}*/
/* 							{% endfor %}<small>75%</small></li>*/
/* 								<li> {% for i in 1..2 %}*/
/* 								{% if rating < i %}<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>{% else %}<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>{% endif %}*/
/* 							{% endfor %}<small>75%</small></li>*/
/* 								<li> {% for i in 1..1 %}*/
/* 								{% if rating < i %}<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>{% else %}<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>{% endif %}*/
/* 							{% endfor %}<small>75%</small></li>*/
/* 		                 </ul>*/
/* 		             </div>*/
/* 		             */
/* 		         </div>*/
/* 		         <div class="review-edit-box">*/
/* 		             <strong>Write review for this product<span>share your feedback with other customer</span></strong>*/
/* 		             <div class="edit-button"><a href=""><i class="fa fa-edit"></i>Write a Product review</a></div>*/
/* 		         </div>*/
/* 		         </div>*/
/* 		     </div>*/
/* 		     <div class="col-md-7 pl-0">*/
/* 		        <div class="review-right-block">*/
/* 		         <div class="review-mention">*/
/* 		             <h3>Review Mention</h3>*/
/* 		             <span>battery life</span><span>value of money</span><span>Price range</span><span>best budget</span>*/
/* 		         </div>*/
/* 		         <div class="review-text">*/
/* 		             <h3>Review</h3>*/
/* 		             <ul>*/
/* 		                 <li>*/
/* 		                     <div class="customer-block">*/
/* 		                     <div class="customer-details">*/
/* 		                         <div class="customer-profile">*/
/* 		                         <img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/customer-review-img1.jpg">*/
/* 		                         </div>*/
/* 		                         <strong>Nikil <br>*/
/* 		                          {% for i in 1..5 %}*/
/* 								{% if rating < i %}<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>{% else %}<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>{% endif %}*/
/* 							{% endfor %}*/
/* 							<small>reviewed on 15 sep 2020</small>*/
/* 		                         </strong>*/
/* 		                     </div>*/
/* 		                     <div class="like-details">*/
/* 		                         <small><i class="fa fa-thumbs-up"></i></small>*/
/* 		                         <small><i class="fa fa-thumbs-down"></i></small>*/
/* 		                     </div>*/
/* 		                     </div>*/
/* 		                     <div class="review-para">*/
/* 		                         <span>More than 5 star, best budget mobile</span>*/
/* 		                         <p>Integer faucibus facilisis volutpat. Aliquam in suscipit orci. Nullam aliquam fermentum auctor. </p>*/
/* 		                     </div>*/
/* 		                 </li>*/
/* 		                  <li>*/
/* 		                     <div class="customer-block">*/
/* 		                     <div class="customer-details">*/
/* 		                         <div class="customer-profile">*/
/* 		                         <img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/customer-review-img1.jpg">*/
/* 		                         </div>*/
/* 		                         <strong>Nikil <br>*/
/* 		                          {% for i in 1..5 %}*/
/* 								{% if rating < i %}<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>{% else %}<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>{% endif %}*/
/* 							{% endfor %}*/
/* 							<small>reviewed on 15 sep 2020</small>*/
/* 		                         </strong>*/
/* 		                     </div>*/
/* 		                     <div class="like-details">*/
/* 		                         <small><i class="fa fa-thumbs-up"></i></small>*/
/* 		                         <small><i class="fa fa-thumbs-down"></i></small>*/
/* 		                     </div>*/
/* 		                     </div>*/
/* 		                     <div class="review-para">*/
/* 		                         <span>More than 5 star, best budget mobile</span>*/
/* 		                         <p>Integer faucibus facilisis volutpat. Aliquam in suscipit orci. Nullam aliquam fermentum auctor..</p>*/
/* 		                     </div>*/
/* 		                 </li>*/
/* 		                  <li>*/
/* 		                     <div class="customer-block">*/
/* 		                     <div class="customer-details">*/
/* 		                         <div class="customer-profile">*/
/* 		                         <img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/customer-review-img1.jpg">*/
/* 		                         </div>*/
/* 		                         <strong>Nikil <br>*/
/* 		                          {% for i in 1..5 %}*/
/* 								{% if rating < i %}<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>{% else %}<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>{% endif %}*/
/* 							{% endfor %}*/
/* 							<small>reviewed on 15 sep 2020</small>*/
/* 		                         </strong>*/
/* 		                     </div>*/
/* 		                     <div class="like-details">*/
/* 		                         <small><i class="fa fa-thumbs-up"></i></small>*/
/* 		                         <small><i class="fa fa-thumbs-down"></i></small>*/
/* 		                     </div>*/
/* 		                     </div>*/
/* 		                     <div class="review-para">*/
/* 		                         <span>More than 5 star, best budget mobile</span>*/
/* 		                         <p>Integer faucibus facilisis volutpat. Aliquam in suscipit orci. Nullam aliquam fermentum auctor..</p>*/
/* 		                     </div>*/
/* 		                 </li>*/
/* 		                 */
/* 		             </ul>*/
/* 		         </div>*/
/* 		         </div>*/
/* 		     </div>*/
/* 		       {#====End-product-review==== #}*/
/* 		     {#====Question-product==== #}*/
/* 		     <div class="col-md-8 pr-0">*/
/* 		         <div class="product-question" id="prod_question">*/
/* 		               <div class="combo-title question-pro">*/
/* 			            <h2><img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/related-product.png">Question and Answer</h2>*/
/* 			            <span><input type="text" placeholder="Search of Question and Answer.."></span>*/
/* 			        </div>*/
/* 			        <div id="collapse-description" class="desc-collapse showdown">*/
/* 			        <ul>*/
/* 			            <li><div class="question-content">*/
/* 			                <p class="question-cont">Q : It is Quality Product?</p>*/
/* 			                <p class="answer-cont">A : Yes it Quality Product.</p>*/
/* 			                </div>*/
/* 			                <div class="like-details">*/
/* 			                    <span>by paratap</span>*/
/* 			                    <small><i class="fa fa-thumbs-up"></i>930</small>*/
/* 			                    <small><i class="fa fa-thumbs-down"></i>30</small>*/
/* 			                </div>*/
/* 			            </li>*/
/* 			             <li><div class="question-content">*/
/* 			                <p class="question-cont">Q : It is Quality Product?</p>*/
/* 			                <p class="answer-cont">A : Yes it Quality Product.</p>*/
/* 			                </div>*/
/* 			                <div class="like-details">*/
/* 			                    <span>by paratap</span>*/
/* 			                    <small><i class="fa fa-thumbs-up"></i>930</small>*/
/* 			                    <small><i class="fa fa-thumbs-down"></i>30</small>*/
/* 			                </div>*/
/* 			            </li>*/
/* 			             <li><div class="question-content">*/
/* 			                <p class="question-cont">Q : It is Quality Product?</p>*/
/* 			                <p class="answer-cont">A : Yes it Quality Product.</p>*/
/* 			                </div>*/
/* 			                <div class="like-details">*/
/* 			                    <span>by paratap</span>*/
/* 			                    <small><i class="fa fa-thumbs-up"></i>930</small>*/
/* 			                    <small><i class="fa fa-thumbs-down"></i>30</small>*/
/* 			                </div>*/
/* 			            </li>*/
/* 			             <li><div class="question-content">*/
/* 			                <p class="question-cont">Q : It is Quality Product?</p>*/
/* 			                <p class="answer-cont">A : Yes it Quality Product.</p>*/
/* 			                </div>*/
/* 			                <div class="like-details">*/
/* 			                    <span>by paratap</span>*/
/* 			                    <small><i class="fa fa-thumbs-up"></i>930</small>*/
/* 			                    <small><i class="fa fa-thumbs-down"></i>30</small>*/
/* 			                </div>*/
/* 			            </li>*/
/* 			        </ul>*/
/* 			        </div>*/
/* 			        <div class="button-toggle toggle1">*/
/* 								         <a class="showmore" data-toggle="collapse" href="#" aria-expanded="false" aria-controls="collapse-footer">*/
/* 								            <span class="toggle-more">Show all answer question <i class="fa fa-angle-down"></i></span> */
/* 								            <span class="toggle-less">Show Less <i class="fa fa-angle-up"></i></span>           */
/* 										</a>      */
/* 										<a href="">*/
/* 										    <strong><i class="fa fa-edit"></i>Ask Question</strong>*/
/* 										</a>*/
/* 									</div>*/
/* 		         </div>*/
/* 		     </div>*/
/* 		     <div class="col-md-4 pl-0">*/
/* 		         <div class="product-customer-image">*/
/* 		              <div class="combo-title customer-img">*/
/* 			            <h2>Customer Image</h2>*/
/* 			        </div>*/
/* 			        <ul>*/
/* 			            <li><a href=""><img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/customer-img1.jpg"></a></li>*/
/* 			            <li><a href=""><img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/customer-img2.jpg"></a></li>*/
/* 			            <li><a href=""><img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/customer-img2.jpg"></a></li>*/
/* 			            <li><a href=""><img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/customer-img1.jpg"></a></li>*/
/* 			        </ul>*/
/* 			        <div class="gallery-button"><a href="" class="btn btn-gallary">See Image Gallery</a></div>*/
/* 		         </div>*/
/* 		     </div>*/
/* 		     {#====End-Question-product==== #}*/
/* 		     */
/* 		     {#====compare-product==== #}*/
/* 		     <div class="col-md-12 pr-0 pl-0">*/
/* 			    <div class="product-combo">*/
/* 			        <div class="combo-title">*/
/* 			            <h2>Buy Together Combo Offer</h2>*/
/* 			        </div>*/
/* 			        <ul>*/
/* 			            <li><div class="plus-ico"><img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/plus-ico.png"></div>*/
/* 			                <div class="combo-checked-box"><input type="checkbox" id="combo-box"></div>*/
/* 			                <div class="combo-offer-img"><img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/combo-offer-img.jpg"></div>*/
/* 			                <div class="combo-list"><img src="http://poorvikabeta.webindia.com/image/cache/catalog/demo/canon_eos_5d_1-355x470.jpg"></div>*/
/* 			                <div class="combo-content">*/
/* 			                    <h5>Canon EOS 5D</h5>*/
/* 			                    <strong>4.5*/
/* 			                    {% for i in 1..5 %}*/
/* 								{% if rating < i %}<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>{% else %}<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>{% endif %}*/
/* 							{% endfor %}*/
/* 							</strong> */
/* 							<small class="price">&#8377; 45,000<strike class="old-price">25,000</strike></small>*/
/* 							</div>*/
/* 			            */
/* 			            </li>*/
/* 			             <li><div class="plus-ico"><img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/plus-ico.png"></div>*/
/* 			                   <div class="combo-checked-box"><input type="checkbox" id="combo-box"></div>*/
/* 			                <div class="combo-offer-img"><img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/combo-offer-img.jpg"></div>*/
/* 			                <div class="combo-list"><img src="http://poorvikabeta.webindia.com/image/cache/catalog/demo/canon_eos_5d_1-355x470.jpg"></div>*/
/* 			                <div class="combo-content">*/
/* 			                    <h5>Canon EOS 5D</h5>*/
/* 			                    <strong>4.5*/
/* 			                    {% for i in 1..5 %}*/
/* 								{% if rating < i %}<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>{% else %}<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>{% endif %}*/
/* 							{% endfor %}*/
/* 							</strong> */
/* 							<small class="price">&#8377; 45,000<strike class="old-price">25,000</strike></small>*/
/* 							</div>*/
/* 			            */
/* 			            </li>*/
/* 			             <li><div class="plus-ico"><img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/equal-ico.png"></div>*/
/* 			                   <div class="combo-checked-box"><input type="checkbox" id="combo-box"></div>*/
/* 			                <div class="combo-offer-img"><img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/combo-offer-img.jpg"></div>*/
/* 			                <div class="combo-list"><img src="http://poorvikabeta.webindia.com/image/cache/catalog/demo/canon_eos_5d_1-355x470.jpg"></div>*/
/* 			                <div class="combo-content">*/
/* 			                    <h5>Canon EOS 5D</h5>*/
/* 			                    <strong>4.5*/
/* 			                    {% for i in 1..5 %}*/
/* 								{% if rating < i %}<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>{% else %}<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>{% endif %}*/
/* 							{% endfor %}*/
/* 							</strong> */
/* 							<small class="price">&#8377; 45,000<strike class="old-price">25,000</strike></small>*/
/* 							</div>*/
/* 			            */
/* 			            </li>*/
/* 			             <li class="combo-offer-bg">*/
/* 			                <h4>Offer Summary</h4>*/
/* 			                <strike class="old-price">25,000</strike>*/
/* 			                	<small class="price">&#8377; 45,000</small>*/
/* 			               <strong>You save 20,600<span>on 2 items</span></strong>*/
/* 			               */
/* 			            <div class="cart"><input type="button" value="{{ button_cart }}" data-loading-text="{{ text_loading }}" id="button-cart" class="btn btn-mega btn-lg btn-offer"></div>*/
/* 			            </li>*/
/* 			        </ul>*/
/* 			    </div>*/
/* 			</div>*/
/* 			{#====end-compare-product==== #}*/
/* 		    {#====related-product==== #}*/
/* 		    <div id="prod_related" class="col-md-12">*/
/* 		        <div class="product-combo related-product">*/
/* 			        <div class="combo-title">*/
/* 			            <h2><img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/related-product.png">Related Product</h2>*/
/* 			        </div>*/
/* 			        <ul class="related-carousel owl-carousel owl-theme">*/
/* 			            <li><a href="">*/
/* 			                <div class="combo-list"><img src="http://poorvikabeta.webindia.com/image/cache/catalog/demo/canon_eos_5d_1-355x470.jpg"></div>*/
/* 			                <div class="combo-content">*/
/* 			                    <h5>Canon EOS 5D</h5>*/
/* 			                    <strong>4.5*/
/* 			                    {% for i in 1..5 %}*/
/* 								{% if rating < i %}<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>{% else %}<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>{% endif %}*/
/* 							{% endfor %}*/
/* 							</strong> */
/* 							<small class="price">&#8377; 45,000<strike class="old-price">25,000</strike></small>*/
/* 							</div>*/
/* 			            </a>*/
/* 			            </li>*/
/* 			             <li><a href="">*/
/* 			                <div class="combo-list"><img src="http://poorvikabeta.webindia.com/image/cache/catalog/demo/canon_eos_5d_1-355x470.jpg"></div>*/
/* 			                <div class="combo-content">*/
/* 			                    <h5>Canon EOS 5D</h5>*/
/* 			                    <strong>4.5*/
/* 			                    {% for i in 1..5 %}*/
/* 								{% if rating < i %}<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>{% else %}<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>{% endif %}*/
/* 							{% endfor %}*/
/* 							</strong> */
/* 							<small class="price">&#8377; 45,000<strike class="old-price">25,000</strike></small>*/
/* 							</div>*/
/* 			            </a>*/
/* 			            </li>*/
/* 			             <li><a href="">*/
/* 			                <div class="combo-list"><img src="http://poorvikabeta.webindia.com/image/cache/catalog/demo/canon_eos_5d_1-355x470.jpg"></div>*/
/* 			                <div class="combo-content">*/
/* 			                    <h5>Canon EOS 5D</h5>*/
/* 			                    <strong>4.5*/
/* 			                    {% for i in 1..5 %}*/
/* 								{% if rating < i %}<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>{% else %}<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>{% endif %}*/
/* 							{% endfor %}*/
/* 							</strong> */
/* 							<small class="price">&#8377; 45,000<strike class="old-price">25,000</strike></small>*/
/* 							</div>*/
/* 			            </a>*/
/* 			            </li>*/
/* 			             <li><a href="">*/
/* 			                <div class="combo-list"><img src="http://poorvikabeta.webindia.com/image/cache/catalog/demo/canon_eos_5d_1-355x470.jpg"></div>*/
/* 			                <div class="combo-content">*/
/* 			                    <h5>Canon EOS 5D</h5>*/
/* 			                    <strong>4.5*/
/* 			                    {% for i in 1..5 %}*/
/* 								{% if rating < i %}<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>{% else %}<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>{% endif %}*/
/* 							{% endfor %}*/
/* 							</strong> */
/* 							<small class="price">&#8377; 45,000<strike class="old-price">25,000</strike></small>*/
/* 							</div>*/
/* 			            </a>*/
/* 			            </li>*/
/* 			             <li><a href="">*/
/* 			                <div class="combo-list"><img src="http://poorvikabeta.webindia.com/image/cache/catalog/demo/canon_eos_5d_1-355x470.jpg"></div>*/
/* 			                <div class="combo-content">*/
/* 			                    <h5>Canon EOS 5D</h5>*/
/* 			                    <strong>4.5*/
/* 			                    {% for i in 1..5 %}*/
/* 								{% if rating < i %}<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>{% else %}<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>{% endif %}*/
/* 							{% endfor %}*/
/* 							</strong> */
/* 							<small class="price">&#8377; 45,000<strike class="old-price">25,000</strike></small>*/
/* 							</div>*/
/* 			            </a>*/
/* 			            </li>*/
/* 			            */
/* 			             */
/* 			            */
/* 			        </ul>*/
/* 			    </div>*/
/* 		    </div>*/
/* 		</div>*/
/*     	{#====  End-related-product==== #}*/
/*     </div>*/
/*     */
/*     {#====  Column Right outside==== #}*/
/*     {% if col_position== 'outside' %} {{ column_right }} {% endif %}*/
/*     </div>*/
/* </div>*/
/* */
/* <script src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/js/owl.carousel.min.js"></script> */
/* <script type="text/javascript">*/
/* <!--*/
/* $('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){*/
/* 	$.ajax({*/
/* 		url: 'index.php?route=product/product/getRecurringDescription',*/
/* 		type: 'post',*/
/* 		data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),*/
/* 		dataType: 'json',*/
/* 		beforeSend: function() {*/
/* 			$('#recurring-description').html('');*/
/* 		},*/
/* 		success: function(json) {*/
/* 			$('.alert-dismissible, .text-danger').remove();*/
/* */
/* 			if (json['success']) {*/
/* 				$('#recurring-description').html(json['success']);*/
/* 			}*/
/* 		}*/
/* 	});*/
/* });*/
/* //--></script>*/
/* */
/* <script type="text/javascript"><!--*/
/* $('#button-cart').on('click', function() {*/
/* 	*/
/* 	$.ajax({*/
/* 		url: 'index.php?route=extension/soconfig/cart/add',*/
/* 		type: 'post',*/
/* 		data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),*/
/* 		dataType: 'json',*/
/* 		beforeSend: function() {*/
/* 			$('#button-cart').button('loading');*/
/* 		},*/
/* 		complete: function() {*/
/* 			$('#button-cart').button('reset');*/
/* 		},*/
/* 		success: function(json) {*/
/* 			$('.alert').remove();*/
/* 			$('.text-danger').remove();*/
/* 			$('.form-group').removeClass('has-error');*/
/* 			if (json['error']) {*/
/* 				if (json['error']['option']) {*/
/* 					for (i in json['error']['option']) {*/
/* 						var element = $('#input-option' + i.replace('_', '-'));*/
/*  */
/*  {% if option_data %} */
/*  if(ProductOptionId != undefined && ProductOptionId==i.replace('_', '-')){ */
/*  $('.so-colorswatch-productpage-icons').after('<div class="text-danger">' + json['error']['option'][i] + '</div>'); */
/*  } */
/*  {% endif %} */
/*  */
/* 						*/
/* 						if (element.parent().hasClass('input-group')) {*/
/* 							element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');*/
/* 						} else {*/
/* 							element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');*/
/* 						}*/
/* 					}*/
/* 				}*/
/* 				*/
/* 				if (json['error']['recurring']) {*/
/* 					$('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');*/
/* 				}*/
/* 				*/
/* 				// Highlight any found errors*/
/* 				$('.text-danger').parent().addClass('has-error');*/
/* 			}*/
/* 			*/
/* 			if (json['success']) {*/
/* 				$('.text-danger').remove();*/
/* 				$('#wrapper').before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="fa fa-close close" data-dismiss="alert"></button></div>');*/
/* 				*/
/*             setTimeout(function () {*/
/*                 $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');*/
/*               }, 100);*/
/*             */
/* 				$('#cart > ul').load('index.php?route=common/cart/info ul li');*/
/* 				*/
/* 				timer = setTimeout(function () {*/
/* 					$('.alert').addClass('fadeOut');*/
/* 				}, 4000);*/
/* 				$('.so-groups-sticky .popup-mycart .popup-content').load('index.php?route=extension/module/so_tools/info .popup-content .cart-header');*/
/* 			}*/
/* 			*/
/* 		*/
/* 		},*/
/*         error: function(xhr, ajaxOptions, thrownError) {*/
/*             alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*         }*/
/* 	});*/
/* });*/
/* */
/* //--></script> */
/* */
/* <script type="text/javascript"><!--*/
/* $('.date').datetimepicker({*/
/* 	language: document.cookie.match(new RegExp('language=([^;]+)'))[1],*/
/* 	pickTime: false*/
/* });*/
/* */
/* $('.datetime').datetimepicker({*/
/* 	language: document.cookie.match(new RegExp('language=([^;]+)'))[1],*/
/* 	pickDate: true,*/
/* 	pickTime: true*/
/* });*/
/* */
/* $('.time').datetimepicker({*/
/* 	language: document.cookie.match(new RegExp('language=([^;]+)'))[1],*/
/* 	pickDate: false*/
/* });*/
/* */
/* $('button[id^=\'button-upload\']').on('click', function() {*/
/* 	var node = this;*/
/* */
/* 	$('#form-upload').remove();*/
/* */
/* 	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');*/
/* */
/* 	$('#form-upload input[name=\'file\']').trigger('click');*/
/* */
/* 	if (typeof timer != 'undefined') {*/
/* 		clearInterval(timer);*/
/* 	}*/
/* */
/* 	timer = setInterval(function() {*/
/* 		if ($('#form-upload input[name=\'file\']').val() != '') {*/
/* 			clearInterval(timer);*/
/* */
/* 			$.ajax({*/
/* 				url: 'index.php?route=tool/upload',*/
/* 				type: 'post',*/
/* 				dataType: 'json',*/
/* 				data: new FormData($('#form-upload')[0]),*/
/* 				cache: false,*/
/* 				contentType: false,*/
/* 				processData: false,*/
/* 				beforeSend: function() {*/
/* 					$(node).button('loading');*/
/* 				},*/
/* 				complete: function() {*/
/* 					$(node).button('reset');*/
/* 				},*/
/* 				success: function(json) {*/
/* 					$('.text-danger').remove();*/
/* */
/* 					if (json['error']) {*/
/* 						$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');*/
/* 					}*/
/* */
/* 					if (json['success']) {*/
/* 						alert(json['success']);*/
/* */
/* 						$(node).parent().find('input').val(json['code']);*/
/* 					}*/
/* 				},*/
/* 				error: function(xhr, ajaxOptions, thrownError) {*/
/* 					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/* 				}*/
/* 			});*/
/* 		}*/
/* 	}, 500);*/
/* });*/
/* //--></script> */
/* <script type="text/javascript"><!--*/
/* $('#review').delegate('.pagination a', 'click', function(e) {*/
/*     e.preventDefault();*/
/* */
/*     $('#review').fadeOut('slow');*/
/*     $('#review').load(this.href);*/
/*     $('#review').fadeIn('slow');*/
/* });*/
/* */
/* $('#review').load('index.php?route=product/product/review&product_id={{ product_id }}');*/
/* */
/* $('#button-review').on('click', function() {*/
/* 	$.ajax({*/
/* 		url: 'index.php?route=product/product/write&product_id={{ product_id }}',*/
/* 		type: 'post',*/
/* 		dataType: 'json',*/
/* 		data: $("#form-review").serialize(),*/
/* 		beforeSend: function() {*/
/* 			$('#button-review').button('loading');*/
/* 		},*/
/* 		complete: function() {*/
/* 			$('#button-review').button('reset');*/
/* 		},*/
/* 		success: function(json) {*/
/* 			$('.alert-dismissible').remove();*/
/* */
/* 			if (json['error']) {*/
/* 				$('#review').after('<div class="alert alert-danger alert-dismissible"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');*/
/* 			}*/
/* */
/* 			if (json['success']) {*/
/* 				$('#review').after('<div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');*/
/* */
/* 				$('input[name=\'name\']').val('');*/
/* 				$('textarea[name=\'text\']').val('');*/
/* 				$('input[name=\'rating\']:checked').prop('checked', false);*/
/* 			}*/
/* 		}*/
/* 	});*/
/* });*/
/* */
/* //--></script>*/
/* */
/* */
/* */
/* <script type="text/javascript"><!--*/
/* 	$(document).ready(function() {*/
/* 		*/
/* 		// Initialize the sticky scrolling on an item */
/* 		sidebar_sticky = '{{sidebar_sticky}}';*/
/* 		*/
/* 		if(sidebar_sticky=='left'){*/
/* 			$(".left_column").stick_in_parent({*/
/* 			    offset_top: 10,*/
/* 			    bottoming   : true*/
/* 			});*/
/* 		}else if (sidebar_sticky=='right'){*/
/* 			$(".right_column").stick_in_parent({*/
/* 			    offset_top: 10,*/
/* 			    bottoming   : true*/
/* 			});*/
/* 		}else if (sidebar_sticky=='all'){*/
/* 			$(".content-aside").stick_in_parent({*/
/* 			    offset_top: 10,*/
/* 			    bottoming   : true*/
/* 			});*/
/* 		}*/
/* 		*/
/* */
/* 		$("#thumb-slider .image-additional").each(function() {*/
/* 			$(this).find("[data-index='0']").addClass('active');*/
/* 		});*/
/* 		*/
/* 		$('.product-options li.radio').click(function(){*/
/* 			$(this).addClass(function() {*/
/* 				if($(this).hasClass("active")) return "";*/
/* 				return "active";*/
/* 			});*/
/* 			*/
/* 			$(this).siblings("li").removeClass("active");*/
/* 			$(this).parent().find('.selected-option').html('<span class="label label-success">'+ $(this).find('img').data('original-title') +'</span>');*/
/* 		})*/
/* 		*/
/* 		$('.thumb-video').magnificPopup({*/
/* 		  type: 'iframe',*/
/* 		  iframe: {*/
/* 			patterns: {*/
/* 			   youtube: {*/
/* 				  index: 'youtube.com/', // String that detects type of video (in this case YouTube). Simply via url.indexOf(index).*/
/* 				  id: 'v=', // String that splits URL in a two parts, second part should be %id%*/
/* 				  src: '//www.youtube.com/embed/%id%?autoplay=1' // URL that will be set as a source for iframe. */
/* 					},*/
/* 				}*/
/* 			}*/
/* 		});*/
/* 	});*/
/* //--></script>*/
/* */
/* */
/* <script type="text/javascript">*/
/* var ajax_price = function() {*/
/* 	$.ajax({*/
/* 		type: 'POST',*/
/* 		url: 'index.php?route=extension/soconfig/liveprice/index',*/
/* 		data: $('.product-detail input[type=\'text\'], .product-detail input[type=\'hidden\'], .product-detail input[type=\'radio\']:checked, .product-detail input[type=\'checkbox\']:checked, .product-detail select, .product-detail textarea'),*/
/* 		dataType: 'json',*/
/* 			success: function(json) {*/
/* 			if (json.success) {*/
/* 				change_price('#price-special', json.new_price.special);*/
/* 				change_price('#price-tax', json.new_price.tax);*/
/* 				change_price('#price-old', json.new_price.price);*/
/* 			}*/
/* 		}*/
/* 	});*/
/* }*/
/* */
/* var change_price = function(id, new_price) {$(id).html(new_price);}*/
/* $('.product-detail input[type=\'text\'], .product-detail input[type=\'hidden\'], .product-detail input[type=\'radio\'], .product-detail input[type=\'checkbox\'], .product-detail select, .product-detail textarea, .product-detail input[name=\'quantity\']').on('change', function() {*/
/* 	ajax_price();*/
/* });*/
/* </script>*/
/* <script>*/
/* function openColor(color) {*/
/*   var i;*/
/*   var x = document.getElementsByClassName("product-color-change");*/
/*   for (i = 0; i < x.length; i++) {*/
/*     x[i].style.display = "none";  */
/*   }*/
/*   document.getElementById(color).style.display = "block";  */
/* }*/
/* </script>*/
/* */
/* */
/* <script>*/
/* function openCity(evt, cityName) {*/
/*   var i, tabcontent, tablinks;*/
/*   tabcontent = document.getElementsByClassName("tabcontent");*/
/*   for (i = 0; i < tabcontent.length; i++) {*/
/*     tabcontent[i].style.display = "none";*/
/*   }*/
/*   tablinks = document.getElementsByClassName("tablinks");*/
/*   for (i = 0; i < tablinks.length; i++) {*/
/*     tablinks[i].className = tablinks[i].className.replace(" active", "");*/
/*   }*/
/*   document.getElementById(cityName).style.display = "inline-block";*/
/*   evt.currentTarget.className += " active";*/
/* }*/
/* </script>*/
/* <script>*/
/*  $(document).ready(function(){*/
/*   $('.related-carousel').owlCarousel({*/
/*     loop:true,*/
/*    autoplay:true,*/
/*     autoplayTimeout:3000,*/
/*     autoplayHoverPause:true,*/
/*     nav:true,*/
/* 	dots:false,*/
/*     responsive:{*/
/*         0:{*/
/*             items:1*/
/*         },*/
/* 		320:{*/
/*             items:1*/
/*         },*/
/* 		480:{*/
/*             items:1*/
/*         },*/
/*         600:{*/
/*             items:2*/
/*         },*/
/* 		767:{*/
/*             items:3*/
/*         },*/
/* 		991:{*/
/*             items:4*/
/*         },*/
/*         1200:{*/
/*             items:5*/
/*         }*/
/*     }*/
/* })*/
/* $( ".owl-prev").html('<i class="fa fa-lg fa-angle-left"></i>');*/
/*  $( ".owl-next").html('<i class="fa fa-lg fa-angle-right"></i>');*/
/* });	*/
/* */
/* </script>*/
/* <script>*/
/*  $(document).ready(function(){*/
/*   $('.video-carousel').owlCarousel({*/
/*     loop:true,*/
/*    autoplay:true,*/
/*     autoplayTimeout:3000,*/
/*     autoplayHoverPause:true,*/
/*     nav:true,*/
/* 	dots:false,*/
/*     responsive:{*/
/*         0:{*/
/*             items:1*/
/*         },*/
/* 		320:{*/
/*             items:1*/
/*         },*/
/* 		480:{*/
/*             items:1*/
/*         },*/
/*         600:{*/
/*             items:2*/
/*         },*/
/* 		767:{*/
/*             items:2*/
/*         },*/
/* 		991:{*/
/*             items:3*/
/*         },*/
/*         1200:{*/
/*             items:4*/
/*         }*/
/*     }*/
/* })*/
/* $( ".owl-prev").html('<i class="fa fa-lg fa-arrow-left"></i>');*/
/*  $( ".owl-next").html('<i class="fa fa-lg fa-arrow-right"></i>');*/
/* });	*/
/* */
/* </script>*/
/* <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAAiQm2qLGFsER6Y96KdzQG633Pn6faIBw&callback=initMap"></script>*/
/* <script>*/
/* $( document ).ready(function() {*/
/*     {% if customer_pincode %}*/
/*        $( "#verify" ).trigger( "click" );*/
/*     {% endif %}*/
/* });*/
/* </script>*/
/* 		<script>*/
/* 			$(document).ready(function(){*/
/* 			    {% if customer_pincode %}*/
/* 			    $('#verify').click(handler);*/
/* 			    {% endif %}*/
/* 			    */
/* 				$('#verify').click(handler);*/
/* 			});	*/
/* 			function handler(product_id){ */
/* 			  var pincodevalue = $("#checktext").val();*/
/* 				if(pincodevalue != '' && pincodevalue.length=='6'){*/
/* 				    var geocoder = new google.maps.Geocoder();*/
/* 				    geocoder.geocode({"address":pincodevalue},function(results,status){*/
/* 				        if(status == google.maps.GeocoderStatus.OK){*/
/*         					latitude = results[0].geometry.location.lat();*/
/*         					langitude = results[0].geometry.location.lng();*/
/*         					*/
/*         					//console.log(pincodevalue,latitude,langitude);*/
/*         					*/
/*         				    $.ajax({*/
/*             					type: "POST",*/
/*             					url: "index.php?route=product/product/checkpincode",*/
/*             					data: { pincode : pincodevalue,latitude:latitude,langitude:langitude,product_id:product_id },*/
/* 								beforeSend:function(){*/
/* 									$("#verify").hide();*/
/* 									$("#pre_loader").show();*/
/* 								},*/
/* 								complete:function(){*/
/* 									$("#verify").show();*/
/* 									$("#pre_loader").hide();*/
/* 								},*/
/*             					success: function(data){*/
/*             					   */
/*                 					$(".available").css("display","block");*/
/*                 					$("#available-text").html(pincodevalue);*/
/*                 					var html = '<div id="pin_avilability_ship" class="delivery_hrs">';*/
/* 									if(data == 21){*/
/* 									    html += '<div class="delivery_type">Available</div><span class="delivery_text"><span class="img_pin"><img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/deliver-img1.png" alt="delivery icon">2 hours</span><span class="img_pin"><img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/deliver-img2.png" alt="delivery icon">Regular</span><span class="img_pin"><img class="pickup_store" src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/deliver-img3.png" alt="delivery icon">Pickup@store</span></span>';*/
/* 									}if(data == 2){*/
/* 									    html += '<div class="delivery_type">Available</div><span class="delivery_text"><span class="img_pin"><img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/deliver-img1.png" alt="delivery icon">2 hours</span><span class="img_pin"><img class="pickup_store" src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/deliver-img3.png" alt="delivery icon">Pickup@store</span></span>';*/
/* 									}else if(data == 1){*/
/* 									    html += '<div class="delivery_type">Available</div><span class="delivery_text"><span class="img_pin"><img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/deliver-img2.png" alt="delivery icon">Regular</span></span>';*/
/* 									}else if(data == 3){*/
/* 									    html += '<div class="delivery_type">Available</div><span class="delivery_text" style="color:#ff6161;">Currently out of stock in this pincode.</span>';*/
/* 									}else if(data == 0){*/
/* 									    html += '<div class="delivery_type">Available</div><span class="delivery_text">Out Of Stock</span>';*/
/* 									}*/
/* 									html += '</div>'; */
/*                 					*/
/*                 					$("#pin_avilability_ship").replaceWith(html);*/
/*         					    }*/
/*         				    });*/
/*     					    */
/*     				*/
/* 				        }else{*/
/* 				           $("#pin_avilability_ship").html("Delivery Not Available"); */
/* 				        }*/
/* 				    });*/
/*     				}else{*/
/*     					$("#pin_avilability_ship").html("<span class='invalid_pin'>Invalid pincode</span>");*/
/* 					 }*/
/* 				*/
/* 					*/
/* 					}*/
/* 					$('#checktext').keyup(function() {*/
/* 					  if ($(this).val().length == 0) {*/
/* 						$('#pin_avilability_ship').hide();*/
/* 					  }*/
/* 					}).keyup();*/
/* 				</script>*/
/* */
/* {{ footer }} */
/* */
