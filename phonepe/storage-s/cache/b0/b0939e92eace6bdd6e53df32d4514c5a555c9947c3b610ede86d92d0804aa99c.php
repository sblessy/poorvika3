<?php

/* default/template/extension/module/tf_filter.twig */
class __TwigTemplate_b8da33f77674160973fb76b9e9cd2098eddc48afefadd75dc9e8aa17026bb378 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"tf-filter-";
        echo (isset($context["module_class_id"]) ? $context["module_class_id"] : null);
        echo "\" class=\"panel tf-filter panel-default\">
<div data-toggle=\"collapse\" href=\"#tf-filter-content-";
        // line 2
        echo (isset($context["module_class_id"]) ? $context["module_class_id"] : null);
        echo "\" class=\"panel-heading";
        echo (((isset($context["collapsed"]) ? $context["collapsed"] : null)) ? (" collapsed") : (""));
        echo "\">
  <h4 class=\"panel-title\">";
        // line 3
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h4>
  ";
        // line 4
        if ((isset($context["reset_all"]) ? $context["reset_all"] : null)) {
            // line 5
            echo "    <span data-tf-reset=\"all\" data-toggle=\"tooltip\" title=\"";
            echo (isset($context["text_reset_all"]) ? $context["text_reset_all"] : null);
            echo "\" class=\"tf-filter-reset hide text-danger\"><i class=\"fa fa-times\"></i></span>
  ";
        }
        // line 7
        echo "  <i class=\"fa fa-chevron-circle-down\" aria-hidden=\"true\"></i>
</div>
<div id=\"tf-filter-content-";
        // line 9
        echo (isset($context["module_class_id"]) ? $context["module_class_id"] : null);
        echo "\" data-mz-base-z-index=\"99\" class=\"collapse";
        echo (( !(isset($context["collapsed"]) ? $context["collapsed"] : null)) ? (" in") : (""));
        echo " tf-list-filter-group row\">
";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["filters"]) ? $context["filters"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["filter"]) {
            echo " 
  ";
            // line 11
            if (($this->getAttribute($context["filter"], "type", array(), "array") == "price")) {
                echo " ";
                // line 12
                echo "  <div class=\"tf-filter-group col-xs-";
                echo twig_round((12 / (isset($context["column_xs"]) ? $context["column_xs"] : null)), 0, "ceil");
                echo " col-sm-";
                echo twig_round((12 / (isset($context["column_sm"]) ? $context["column_sm"] : null)), 0, "ceil");
                echo " col-md-";
                echo twig_round((12 / (isset($context["column_md"]) ? $context["column_md"] : null)), 0, "ceil");
                echo " col-lg-";
                echo twig_round((12 / (isset($context["column_lg"]) ? $context["column_lg"] : null)), 0, "ceil");
                echo "\">
    <div class=\"tf-filter-group-header ";
                // line 13
                echo (($this->getAttribute($context["filter"], "collapse", array(), "array")) ? ("collapsed") : (""));
                echo "\" data-toggle=\"collapse\" href=\"#tf-filter-panel-";
                echo (isset($context["module_class_id"]) ? $context["module_class_id"] : null);
                echo "-";
                echo $context["key"];
                echo "\">
      <span class=\"tf-filter-group-title\">";
                // line 14
                echo (isset($context["text_price"]) ? $context["text_price"] : null);
                echo "</span>
      ";
                // line 15
                if ((isset($context["reset_group"]) ? $context["reset_group"] : null)) {
                    // line 16
                    echo "        ";
                    if ((($this->getAttribute($this->getAttribute($context["filter"], "selected", array(), "array"), "min", array(), "array") != $this->getAttribute($context["filter"], "min_price", array(), "array")) || ($this->getAttribute($this->getAttribute($context["filter"], "selected", array(), "array"), "max", array(), "array") != $this->getAttribute($context["filter"], "max_price", array(), "array")))) {
                        // line 17
                        echo "        <a data-tf-reset=\"price\" data-toggle=\"tooltip\" title=\"";
                        echo (isset($context["text_reset"]) ? $context["text_reset"] : null);
                        echo "\" class=\"tf-filter-reset\"><i class=\"fa fa-times\"></i></a>
        ";
                    } else {
                        // line 19
                        echo "        <a data-tf-reset=\"price\" data-toggle=\"tooltip\" title=\"";
                        echo (isset($context["text_reset"]) ? $context["text_reset"] : null);
                        echo "\" class=\"tf-filter-reset hide\"><i class=\"fa fa-times\"></i></a>
        ";
                    }
                    // line 21
                    echo "      ";
                }
                // line 22
                echo "      <i class=\"fa fa-caret-up toggle-icon\"></i>
    </div>
    <div id=\"tf-filter-panel-";
                // line 24
                echo (isset($context["module_class_id"]) ? $context["module_class_id"] : null);
                echo "-";
                echo $context["key"];
                echo "\" class=\"collapse ";
                echo (( !$this->getAttribute($context["filter"], "collapse", array(), "array")) ? ("in") : (""));
                echo "\" >
      <div class=\"tf-filter-group-content\">
        <div data-role=\"rangeslider\"></div>
        <div class=\"row\">
          <div class=\"col-xs-6\"><input type=\"number\" class=\"form-control\" name=\"tf_fp[min]\" value=\"";
                // line 28
                echo $this->getAttribute($this->getAttribute($context["filter"], "selected", array(), "array"), "min", array(), "array");
                echo "\" min=\"";
                echo $this->getAttribute($context["filter"], "min_price", array(), "array");
                echo "\" max=\"";
                echo ($this->getAttribute($context["filter"], "max_price", array(), "array") - 1);
                echo "\" /></div>
          <div class=\"col-xs-6\"><input type=\"number\" class=\"form-control\" name=\"tf_fp[max]\" value=\"";
                // line 29
                echo $this->getAttribute($this->getAttribute($context["filter"], "selected", array(), "array"), "max", array(), "array");
                echo "\" min=\"";
                echo ($this->getAttribute($context["filter"], "min_price", array(), "array") + 1);
                echo "\" max=\"";
                echo $this->getAttribute($context["filter"], "max_price", array(), "array");
                echo "\" /></div>
        </div>
      </div>
    </div>
  </div>
        
  ";
            } elseif ((($this->getAttribute(            // line 35
$context["filter"], "type", array(), "array") == "sub_category") && $this->getAttribute($context["filter"], "values", array(), "array"))) {
                echo " ";
                // line 36
                echo "  <div class=\"tf-filter-group col-xs-";
                echo twig_round((12 / (isset($context["column_xs"]) ? $context["column_xs"] : null)), 0, "ceil");
                echo " col-sm-";
                echo twig_round((12 / (isset($context["column_sm"]) ? $context["column_sm"] : null)), 0, "ceil");
                echo " col-md-";
                echo twig_round((12 / (isset($context["column_md"]) ? $context["column_md"] : null)), 0, "ceil");
                echo " col-lg-";
                echo twig_round((12 / (isset($context["column_lg"]) ? $context["column_lg"] : null)), 0, "ceil");
                echo "\">
    <div class=\"tf-filter-group-header ";
                // line 37
                echo (($this->getAttribute($context["filter"], "collapse", array(), "array")) ? ("collapsed") : (""));
                echo "\" data-toggle=\"collapse\" href=\"#tf-filter-panel-";
                echo (isset($context["module_class_id"]) ? $context["module_class_id"] : null);
                echo "-";
                echo $context["key"];
                echo "\">
      <span class=\"tf-filter-group-title\">";
                // line 38
                echo (isset($context["text_sub_category"]) ? $context["text_sub_category"] : null);
                echo "</span>
      ";
                // line 39
                if ((isset($context["reset_group"]) ? $context["reset_group"] : null)) {
                    // line 40
                    echo "        ";
                    $context["total_selected"] = 0;
                    // line 41
                    echo "        ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["filter"], "values", array(), "array"));
                    foreach ($context['_seq'] as $context["_key"] => $context["sub_category"]) {
                        if ($this->getAttribute($context["sub_category"], "selected", array(), "array")) {
                            $context["total_selected"] = ((isset($context["total_selected"]) ? $context["total_selected"] : null) + 1);
                        }
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sub_category'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 42
                    echo "        <a data-tf-reset=\"check\" data-toggle=\"tooltip\" title=\"";
                    echo (isset($context["text_reset"]) ? $context["text_reset"] : null);
                    echo "\" class=\" tf-filter-reset";
                    echo (( !(isset($context["total_selected"]) ? $context["total_selected"] : null)) ? (" hide") : (""));
                    echo "\"><i class=\"fa fa-times\"></i></a>
      ";
                }
                // line 44
                echo "      <i class=\"fa fa-caret-up toggle-icon\"></i>
    </div>
    <div id=\"tf-filter-panel-";
                // line 46
                echo (isset($context["module_class_id"]) ? $context["module_class_id"] : null);
                echo "-";
                echo $context["key"];
                echo "\" class=\"collapse ";
                echo (( !$this->getAttribute($context["filter"], "collapse", array(), "array")) ? ("in") : (""));
                echo "\" >
      ";
                // line 47
                if ($this->getAttribute($context["filter"], "search", array(), "array")) {
                    // line 48
                    echo "      <div class=\"tf-filter-group-search\"><i class=\"fa fa-search\"></i> <input type=\"search\" placeholder=\"";
                    echo (isset($context["text_search"]) ? $context["text_search"] : null);
                    echo "\"/></div>
      ";
                }
                // line 50
                echo "      <div class=\"tf-filter-group-content ";
                echo (isset($context["overflow"]) ? $context["overflow"] : null);
                echo "\">
      ";
                // line 51
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["filter"], "values", array(), "array"));
                foreach ($context['_seq'] as $context["_key"] => $context["sub_category"]) {
                    echo " 
        <div class=\"form-check tf-filter-value custom-";
                    // line 52
                    echo $this->getAttribute($context["filter"], "input_type", array(), "array");
                    echo " ";
                    echo $this->getAttribute($context["filter"], "list_type", array(), "array");
                    echo "\">
          <label class=\"form-check-label\">
            ";
                    // line 54
                    if ($this->getAttribute($context["sub_category"], "selected", array(), "array")) {
                        echo " 
            <input type=\"";
                        // line 55
                        echo $this->getAttribute($context["filter"], "input_type", array(), "array");
                        echo "\" name=\"tf_fsc\" value=\"";
                        echo $this->getAttribute($context["sub_category"], "category_id", array(), "array");
                        echo "\" class=\"form-check-input\" checked>
            ";
                    } else {
                        // line 56
                        echo " 
            <input type=\"";
                        // line 57
                        echo $this->getAttribute($context["filter"], "input_type", array(), "array");
                        echo "\" name=\"tf_fsc\" value=\"";
                        echo $this->getAttribute($context["sub_category"], "category_id", array(), "array");
                        echo "\" class=\"form-check-input\" ";
                        echo (( !$this->getAttribute($context["sub_category"], "status", array(), "array")) ? ("disabled") : (""));
                        echo ">
            ";
                    }
                    // line 58
                    echo " 
            ";
                    // line 59
                    if ((($this->getAttribute($context["filter"], "list_type", array(), "array") == "image") || ($this->getAttribute($context["filter"], "list_type", array(), "array") == "both"))) {
                        echo " 
            <img src=\"";
                        // line 60
                        echo $this->getAttribute($context["sub_category"], "image", array(), "array");
                        echo "\" title=\"";
                        echo $this->getAttribute($context["sub_category"], "name", array(), "array");
                        echo "\" alt=\"";
                        echo $this->getAttribute($context["sub_category"], "name", array(), "array");
                        echo "\" />
            ";
                    } else {
                        // line 62
                        echo "            <span class=\"checkmark fa\"></span>
            ";
                    }
                    // line 63
                    echo " 
            ";
                    // line 64
                    if ((($this->getAttribute($context["filter"], "list_type", array(), "array") == "text") || ($this->getAttribute($context["filter"], "list_type", array(), "array") == "both"))) {
                        echo " 
              ";
                        // line 65
                        echo $this->getAttribute($context["sub_category"], "name", array(), "array");
                        echo "
            ";
                    }
                    // line 67
                    echo "          </label>
          ";
                    // line 68
                    if (((isset($context["count_product"]) ? $context["count_product"] : null) && ($this->getAttribute($context["filter"], "list_type", array(), "array") != "image"))) {
                        // line 69
                        echo "            ";
                        if ($this->getAttribute($context["sub_category"], "total", array(), "array")) {
                            echo " 
            <span class=\"label label-info tf-product-total\">";
                            // line 70
                            echo $this->getAttribute($context["sub_category"], "total", array(), "array");
                            echo "</span>
            ";
                        } else {
                            // line 72
                            echo "            <span class=\"label label-info label-danger tf-product-total\">";
                            echo $this->getAttribute($context["sub_category"], "total", array(), "array");
                            echo "</span>
            ";
                        }
                        // line 73
                        echo " 
          ";
                    }
                    // line 75
                    echo "        </div>
      ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sub_category'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 77
                echo "      ";
                if ((((isset($context["overflow"]) ? $context["overflow"] : null) == "more") && (twig_length_filter($this->env, $this->getAttribute($context["filter"], "values", array(), "array")) >= 7))) {
                    // line 78
                    echo "        <a class=\"tf-see-more btn-link\" data-toggle=\"tf-seemore\" data-show=\"";
                    echo (isset($context["text_see_more"]) ? $context["text_see_more"] : null);
                    echo "\" data-hide=\"";
                    echo (isset($context["text_see_less"]) ? $context["text_see_less"] : null);
                    echo "\" href=\"#\">";
                    echo (isset($context["text_see_more"]) ? $context["text_see_more"] : null);
                    echo "</a>
      ";
                }
                // line 80
                echo "      </div>
    </div>
  </div>
      
  ";
            } elseif ((($this->getAttribute(            // line 84
$context["filter"], "type", array(), "array") == "manufacturer") && $this->getAttribute($context["filter"], "values", array(), "array"))) {
                echo " ";
                // line 85
                echo "  <div class=\"tf-filter-group col-xs-";
                echo twig_round((12 / (isset($context["column_xs"]) ? $context["column_xs"] : null)), 0, "ceil");
                echo " col-sm-";
                echo twig_round((12 / (isset($context["column_sm"]) ? $context["column_sm"] : null)), 0, "ceil");
                echo " col-md-";
                echo twig_round((12 / (isset($context["column_md"]) ? $context["column_md"] : null)), 0, "ceil");
                echo " col-lg-";
                echo twig_round((12 / (isset($context["column_lg"]) ? $context["column_lg"] : null)), 0, "ceil");
                echo "\">
    <div class=\"tf-filter-group-header ";
                // line 86
                echo (($this->getAttribute($context["filter"], "collapse", array(), "array")) ? ("collapsed") : (""));
                echo "\" data-toggle=\"collapse\" href=\"#tf-filter-panel-";
                echo (isset($context["module_class_id"]) ? $context["module_class_id"] : null);
                echo "-";
                echo $context["key"];
                echo "\">
      <span class=\"tf-filter-group-title\">";
                // line 87
                echo (isset($context["text_manufacturer"]) ? $context["text_manufacturer"] : null);
                echo "</span>
      ";
                // line 88
                if ((isset($context["reset_group"]) ? $context["reset_group"] : null)) {
                    // line 89
                    echo "        ";
                    $context["total_selected"] = 0;
                    // line 90
                    echo "        ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["filter"], "values", array(), "array"));
                    foreach ($context['_seq'] as $context["_key"] => $context["manufacturer"]) {
                        if ($this->getAttribute($context["manufacturer"], "selected", array(), "array")) {
                            $context["total_selected"] = ((isset($context["total_selected"]) ? $context["total_selected"] : null) + 1);
                        }
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['manufacturer'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 91
                    echo "        <a data-tf-reset=\"check\" data-toggle=\"tooltip\" title=\"";
                    echo (isset($context["text_reset"]) ? $context["text_reset"] : null);
                    echo "\" class=\" tf-filter-reset";
                    echo (( !(isset($context["total_selected"]) ? $context["total_selected"] : null)) ? (" hide") : (""));
                    echo "\"><i class=\"fa fa-times\"></i></a>
      ";
                }
                // line 93
                echo "      <i class=\"fa fa-caret-up toggle-icon\"></i>
    </div>
    <div id=\"tf-filter-panel-";
                // line 95
                echo (isset($context["module_class_id"]) ? $context["module_class_id"] : null);
                echo "-";
                echo $context["key"];
                echo "\" class=\"collapse ";
                echo (( !$this->getAttribute($context["filter"], "collapse", array(), "array")) ? ("in") : (""));
                echo "\" >
      ";
                // line 96
                if ($this->getAttribute($context["filter"], "search", array(), "array")) {
                    // line 97
                    echo "      <div class=\"tf-filter-group-search\"><i class=\"fa fa-search\"></i> <input type=\"search\" placeholder=\"";
                    echo (isset($context["text_search"]) ? $context["text_search"] : null);
                    echo "\"/></div>
      ";
                }
                // line 99
                echo "      <div class=\"tf-filter-group-content ";
                echo (isset($context["overflow"]) ? $context["overflow"] : null);
                echo "\">
      ";
                // line 100
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["filter"], "values", array(), "array"));
                foreach ($context['_seq'] as $context["_key"] => $context["manufacturer"]) {
                    echo " 
        <div class=\"form-check tf-filter-value custom-";
                    // line 101
                    echo $this->getAttribute($context["filter"], "input_type", array(), "array");
                    echo " ";
                    echo $this->getAttribute($context["filter"], "list_type", array(), "array");
                    echo "\">
          <label class=\"form-check-label\">
            ";
                    // line 103
                    if ($this->getAttribute($context["manufacturer"], "selected", array(), "array")) {
                        echo " 
            <input type=\"";
                        // line 104
                        echo $this->getAttribute($context["filter"], "input_type", array(), "array");
                        echo "\" name=\"tf_fm\" value=\"";
                        echo $this->getAttribute($context["manufacturer"], "manufacturer_id", array(), "array");
                        echo "\" class=\"form-check-input\" checked>
            ";
                    } else {
                        // line 105
                        echo " 
            <input type=\"";
                        // line 106
                        echo $this->getAttribute($context["filter"], "input_type", array(), "array");
                        echo "\" name=\"tf_fm\" value=\"";
                        echo $this->getAttribute($context["manufacturer"], "manufacturer_id", array(), "array");
                        echo "\" class=\"form-check-input\" ";
                        echo (( !$this->getAttribute($context["manufacturer"], "status", array(), "array")) ? ("disabled") : (""));
                        echo ">
            ";
                    }
                    // line 107
                    echo " 
            ";
                    // line 108
                    if ((($this->getAttribute($context["filter"], "list_type", array(), "array") == "image") || ($this->getAttribute($context["filter"], "list_type", array(), "array") == "both"))) {
                        echo " 
            <img src=\"";
                        // line 109
                        echo $this->getAttribute($context["manufacturer"], "image", array(), "array");
                        echo "\" title=\"";
                        echo $this->getAttribute($context["manufacturer"], "name", array(), "array");
                        echo "\" alt=\"";
                        echo $this->getAttribute($context["manufacturer"], "name", array(), "array");
                        echo "\" />
            ";
                    } else {
                        // line 111
                        echo "            <span class=\"checkmark fa\"></span>
            ";
                    }
                    // line 112
                    echo " 
            ";
                    // line 113
                    if ((($this->getAttribute($context["filter"], "list_type", array(), "array") == "text") || ($this->getAttribute($context["filter"], "list_type", array(), "array") == "both"))) {
                        echo " 
              ";
                        // line 114
                        echo $this->getAttribute($context["manufacturer"], "name", array(), "array");
                        echo "
            ";
                    }
                    // line 116
                    echo "          </label>
          ";
                    // line 117
                    if (((isset($context["count_product"]) ? $context["count_product"] : null) && ($this->getAttribute($context["filter"], "list_type", array(), "array") != "image"))) {
                        // line 118
                        echo "            ";
                        if ($this->getAttribute($context["manufacturer"], "total", array(), "array")) {
                            echo " 
            <span class=\"label label-info tf-product-total\">";
                            // line 119
                            echo $this->getAttribute($context["manufacturer"], "total", array(), "array");
                            echo "</span>
            ";
                        } else {
                            // line 121
                            echo "            <span class=\"label label-info label-danger tf-product-total\">";
                            echo $this->getAttribute($context["manufacturer"], "total", array(), "array");
                            echo "</span>
            ";
                        }
                        // line 122
                        echo " 
          ";
                    }
                    // line 124
                    echo "        </div>
      ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['manufacturer'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 126
                echo "      ";
                if ((((isset($context["overflow"]) ? $context["overflow"] : null) == "more") && (twig_length_filter($this->env, $this->getAttribute($context["filter"], "values", array(), "array")) >= 7))) {
                    // line 127
                    echo "        <a class=\"tf-see-more btn-link\" data-toggle=\"tf-seemore\" data-show=\"";
                    echo (isset($context["text_see_more"]) ? $context["text_see_more"] : null);
                    echo "\" data-hide=\"";
                    echo (isset($context["text_see_less"]) ? $context["text_see_less"] : null);
                    echo "\" href=\"#\">";
                    echo (isset($context["text_see_more"]) ? $context["text_see_more"] : null);
                    echo "</a>
      ";
                }
                // line 129
                echo "      </div>
    </div>
  </div>
  ";
            } elseif (($this->getAttribute(            // line 132
$context["filter"], "type", array(), "array") == "search")) {
                echo " ";
                // line 133
                echo "  <div class=\"tf-filter-group col-xs-";
                echo twig_round((12 / (isset($context["column_xs"]) ? $context["column_xs"] : null)), 0, "ceil");
                echo " col-sm-";
                echo twig_round((12 / (isset($context["column_sm"]) ? $context["column_sm"] : null)), 0, "ceil");
                echo " col-md-";
                echo twig_round((12 / (isset($context["column_md"]) ? $context["column_md"] : null)), 0, "ceil");
                echo " col-lg-";
                echo twig_round((12 / (isset($context["column_lg"]) ? $context["column_lg"] : null)), 0, "ceil");
                echo "\">
    <div class=\"tf-filter-group-header ";
                // line 134
                echo (($this->getAttribute($context["filter"], "collapse", array(), "array")) ? ("collapsed") : (""));
                echo "\" data-toggle=\"collapse\" href=\"#tf-filter-panel-";
                echo (isset($context["module_class_id"]) ? $context["module_class_id"] : null);
                echo "-";
                echo $context["key"];
                echo "\">
      <span  class=\"tf-filter-group-title\">";
                // line 135
                echo (isset($context["text_search"]) ? $context["text_search"] : null);
                echo "</span>
      ";
                // line 136
                if ((isset($context["reset_group"]) ? $context["reset_group"] : null)) {
                    // line 137
                    echo "        ";
                    if ($this->getAttribute($context["filter"], "keyword", array(), "array")) {
                        // line 138
                        echo "        <a data-tf-reset=\"text\" data-toggle=\"tooltip\" title=\"";
                        echo (isset($context["text_reset"]) ? $context["text_reset"] : null);
                        echo "\" class=\"tf-filter-reset\"><i class=\"fa fa-times\"></i></a>
        ";
                    } else {
                        // line 140
                        echo "        <a data-tf-reset=\"text\" data-toggle=\"tooltip\" title=\"";
                        echo (isset($context["text_reset"]) ? $context["text_reset"] : null);
                        echo "\" class=\"tf-filter-reset hide\"><i class=\"fa fa-times\"></i></a>
        ";
                    }
                    // line 142
                    echo "      ";
                }
                // line 143
                echo "      <i class=\"fa fa-caret-up toggle-icon\"></i>
    </div>
    <div id=\"tf-filter-panel-";
                // line 145
                echo (isset($context["module_class_id"]) ? $context["module_class_id"] : null);
                echo "-";
                echo $context["key"];
                echo "\" class=\"collapse ";
                echo (( !$this->getAttribute($context["filter"], "collapse", array(), "array")) ? ("in") : (""));
                echo "\" >
      <div class=\"tf-filter-group-content\">
        <input type=\"text\" name=\"tf_fq\" value=\"";
                // line 147
                echo $this->getAttribute($context["filter"], "keyword", array(), "array");
                echo "\" placeholder=\"";
                echo (isset($context["text_search_placeholder"]) ? $context["text_search_placeholder"] : null);
                echo "\" class=\"form-control\" />
      </div>
    </div>
  </div>
  ";
            } elseif (($this->getAttribute(            // line 151
$context["filter"], "type", array(), "array") == "availability")) {
                echo " ";
                // line 152
                echo "  <div class=\"tf-filter-group col-xs-";
                echo twig_round((12 / (isset($context["column_xs"]) ? $context["column_xs"] : null)), 0, "ceil");
                echo " col-sm-";
                echo twig_round((12 / (isset($context["column_sm"]) ? $context["column_sm"] : null)), 0, "ceil");
                echo " col-md-";
                echo twig_round((12 / (isset($context["column_md"]) ? $context["column_md"] : null)), 0, "ceil");
                echo " col-lg-";
                echo twig_round((12 / (isset($context["column_lg"]) ? $context["column_lg"] : null)), 0, "ceil");
                echo "\">
    <div class=\"tf-filter-group-header ";
                // line 153
                echo (($this->getAttribute($context["filter"], "collapse", array(), "array")) ? ("collapsed") : (""));
                echo "\" data-toggle=\"collapse\" href=\"#tf-filter-panel-";
                echo (isset($context["module_class_id"]) ? $context["module_class_id"] : null);
                echo "-";
                echo $context["key"];
                echo "\">
      <span class=\"tf-filter-group-title\">";
                // line 154
                echo (isset($context["text_availability"]) ? $context["text_availability"] : null);
                echo "</span>
      ";
                // line 155
                if ((isset($context["reset_group"]) ? $context["reset_group"] : null)) {
                    // line 156
                    echo "        ";
                    if (($this->getAttribute($this->getAttribute($this->getAttribute($context["filter"], "values", array(), "array"), "in_stock", array(), "array"), "selected", array(), "array") || $this->getAttribute($this->getAttribute($this->getAttribute($context["filter"], "values", array(), "array"), "out_of_stock", array(), "array"), "selected", array(), "array"))) {
                        // line 157
                        echo "        <a data-tf-reset=\"check\" data-toggle=\"tooltip\" title=\"";
                        echo (isset($context["text_reset"]) ? $context["text_reset"] : null);
                        echo "\" class=\" tf-filter-reset\"><i class=\"fa fa-times\"></i></a>
        ";
                    } else {
                        // line 159
                        echo "        <a data-tf-reset=\"check\" data-toggle=\"tooltip\" title=\"";
                        echo (isset($context["text_reset"]) ? $context["text_reset"] : null);
                        echo "\" class=\" tf-filter-reset hide\"><i class=\"fa fa-times\"></i></a>
        ";
                    }
                    // line 161
                    echo "      ";
                }
                // line 162
                echo "      <i class=\"fa fa-caret-up toggle-icon\"></i>
    </div>
    <div id=\"tf-filter-panel-";
                // line 164
                echo (isset($context["module_class_id"]) ? $context["module_class_id"] : null);
                echo "-";
                echo $context["key"];
                echo "\" class=\"collapse ";
                echo (( !$this->getAttribute($context["filter"], "collapse", array(), "array")) ? ("in") : (""));
                echo "\" >
      <div class=\"tf-filter-group-content\">
        <div class=\"form-check tf-filter-value custom-radio\">
          <label class=\"form-check-label\">
            ";
                // line 168
                if ($this->getAttribute($this->getAttribute($this->getAttribute($context["filter"], "values", array(), "array"), "in_stock", array(), "array"), "selected", array(), "array")) {
                    echo " 
            <input type=\"radio\" value=\"1\" name=\"tf_fs\" class=\"form-check-input\" checked>
            ";
                } else {
                    // line 170
                    echo " 
            <input type=\"radio\" value=\"1\" name=\"tf_fs\" class=\"form-check-input\" ";
                    // line 171
                    echo (( !$this->getAttribute($this->getAttribute($this->getAttribute($context["filter"], "values", array(), "array"), "in_stock", array(), "array"), "status", array(), "array")) ? ("disabled") : (""));
                    echo ">
            ";
                }
                // line 172
                echo " 
            <span class=\"checkmark fa\"></span>
            ";
                // line 174
                echo (isset($context["text_in_stock"]) ? $context["text_in_stock"] : null);
                echo "
          </label>
          ";
                // line 176
                if ((isset($context["count_product"]) ? $context["count_product"] : null)) {
                    // line 177
                    echo "            ";
                    if ($this->getAttribute($this->getAttribute($this->getAttribute($context["filter"], "values", array(), "array"), "in_stock", array(), "array"), "total", array(), "array")) {
                        echo " 
            <span class=\"label label-info tf-product-total\">";
                        // line 178
                        echo $this->getAttribute($this->getAttribute($this->getAttribute($context["filter"], "values", array(), "array"), "in_stock", array(), "array"), "total", array(), "array");
                        echo "</span>
            ";
                    } else {
                        // line 180
                        echo "            <span class=\"label label-info label-danger tf-product-total\">";
                        echo $this->getAttribute($this->getAttribute($this->getAttribute($context["filter"], "values", array(), "array"), "in_stock", array(), "array"), "total", array(), "array");
                        echo "</span>
            ";
                    }
                    // line 181
                    echo " 
          ";
                }
                // line 183
                echo "        </div>
        <div class=\"form-check tf-filter-value custom-radio\">
          <label class=\"form-check-label\">
            ";
                // line 186
                if ($this->getAttribute($this->getAttribute($this->getAttribute($context["filter"], "values", array(), "array"), "out_of_stock", array(), "array"), "selected", array(), "array")) {
                    echo " 
            <input type=\"radio\" value=\"0\" name=\"tf_fs\" class=\"form-check-input\" checked>
            ";
                } else {
                    // line 188
                    echo " 
            <input type=\"radio\" value=\"0\" name=\"tf_fs\" class=\"form-check-input\" ";
                    // line 189
                    echo (( !$this->getAttribute($this->getAttribute($this->getAttribute($context["filter"], "values", array(), "array"), "out_of_stock", array(), "array"), "status", array(), "array")) ? ("disabled") : (""));
                    echo ">
            ";
                }
                // line 190
                echo " 
            <span class=\"checkmark fa\"></span>
            ";
                // line 192
                echo (isset($context["text_out_of_stock"]) ? $context["text_out_of_stock"] : null);
                echo "
          </label>
          ";
                // line 194
                if ((isset($context["count_product"]) ? $context["count_product"] : null)) {
                    // line 195
                    echo "            ";
                    if ($this->getAttribute($this->getAttribute($this->getAttribute($context["filter"], "values", array(), "array"), "out_of_stock", array(), "array"), "total", array(), "array")) {
                        echo " 
            <span class=\"label label-info tf-product-total\">";
                        // line 196
                        echo $this->getAttribute($this->getAttribute($this->getAttribute($context["filter"], "values", array(), "array"), "out_of_stock", array(), "array"), "total", array(), "array");
                        echo "</span>
            ";
                    } else {
                        // line 198
                        echo "            <span class=\"label label-info label-danger tf-product-total\">";
                        echo $this->getAttribute($this->getAttribute($this->getAttribute($context["filter"], "values", array(), "array"), "out_of_stock", array(), "array"), "total", array(), "array");
                        echo "</span>
            ";
                    }
                    // line 199
                    echo " 
          ";
                }
                // line 201
                echo "        </div>
      </div>
    </div>
  </div>
  ";
            } elseif (($this->getAttribute(            // line 205
$context["filter"], "type", array(), "array") == "rating")) {
                echo " ";
                // line 206
                echo "  <div class=\"tf-filter-group col-xs-";
                echo twig_round((12 / (isset($context["column_xs"]) ? $context["column_xs"] : null)), 0, "ceil");
                echo " col-sm-";
                echo twig_round((12 / (isset($context["column_sm"]) ? $context["column_sm"] : null)), 0, "ceil");
                echo " col-md-";
                echo twig_round((12 / (isset($context["column_md"]) ? $context["column_md"] : null)), 0, "ceil");
                echo " col-lg-";
                echo twig_round((12 / (isset($context["column_lg"]) ? $context["column_lg"] : null)), 0, "ceil");
                echo "\">
    <div class=\"tf-filter-group-header ";
                // line 207
                echo (($this->getAttribute($context["filter"], "collapse", array(), "array")) ? ("collapsed") : (""));
                echo "\" data-toggle=\"collapse\" href=\"#tf-filter-panel-";
                echo (isset($context["module_class_id"]) ? $context["module_class_id"] : null);
                echo "-";
                echo $context["key"];
                echo "\">
      <span class=\"tf-filter-group-title\">";
                // line 208
                echo (isset($context["text_rating"]) ? $context["text_rating"] : null);
                echo "</span>
      ";
                // line 209
                if ((isset($context["reset_group"]) ? $context["reset_group"] : null)) {
                    // line 210
                    echo "        ";
                    $context["total_selected"] = 0;
                    // line 211
                    echo "        ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["filter"], "values", array(), "array"));
                    foreach ($context['_seq'] as $context["_key"] => $context["rating"]) {
                        if ($this->getAttribute($context["rating"], "selected", array(), "array")) {
                            $context["total_selected"] = ((isset($context["total_selected"]) ? $context["total_selected"] : null) + 1);
                        }
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['rating'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 212
                    echo "        <a data-tf-reset=\"check\" data-toggle=\"tooltip\" title=\"";
                    echo (isset($context["text_reset"]) ? $context["text_reset"] : null);
                    echo "\" class=\"tf-filter-reset";
                    echo (( !(isset($context["total_selected"]) ? $context["total_selected"] : null)) ? (" hide") : (""));
                    echo "\"><i class=\"fa fa-times\"></i></a>
      ";
                }
                // line 214
                echo "      <i class=\"fa fa-caret-up toggle-icon\"></i>
    </div>
    <div id=\"tf-filter-panel-";
                // line 216
                echo (isset($context["module_class_id"]) ? $context["module_class_id"] : null);
                echo "-";
                echo $context["key"];
                echo "\" class=\"collapse ";
                echo (( !$this->getAttribute($context["filter"], "collapse", array(), "array")) ? ("in") : (""));
                echo "\" >
      <div class=\"tf-filter-group-content\">
        ";
                // line 218
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["filter"], "values", array(), "array"));
                foreach ($context['_seq'] as $context["_key"] => $context["rating"]) {
                    echo " 
          <div class=\"form-check tf-filter-value custom-radio\">
            <label class=\"form-check-label\">
              ";
                    // line 221
                    if ($this->getAttribute($context["rating"], "selected", array(), "array")) {
                        echo " 
              <input type=\"radio\" value=\"";
                        // line 222
                        echo $this->getAttribute($context["rating"], "rating", array(), "array");
                        echo "\" name=\"tf_fr\" class=\"form-check-input\" checked>
              ";
                    } else {
                        // line 223
                        echo " 
              <input type=\"radio\" value=\"";
                        // line 224
                        echo $this->getAttribute($context["rating"], "rating", array(), "array");
                        echo "\" name=\"tf_fr\" class=\"form-check-input\" ";
                        echo (( !$this->getAttribute($context["rating"], "status", array(), "array")) ? ("disabled") : (""));
                        echo ">
              ";
                    }
                    // line 226
                    echo "              <span class=\"checkmark fa\"></span>
              <span class=\"rating\">
                ";
                    // line 228
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(range(1, 5));
                    foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                        echo " 
                  ";
                        // line 229
                        if (($this->getAttribute($context["rating"], "rating", array(), "array") < $context["i"])) {
                            echo " 
                  <span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>
                  ";
                        } else {
                            // line 231
                            echo " 
                  <span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>
                  ";
                        }
                        // line 233
                        echo " 
                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 234
                    echo " 
              </span>
              ";
                    // line 236
                    echo (isset($context["text_and_up"]) ? $context["text_and_up"] : null);
                    echo "
            </label>
            ";
                    // line 238
                    if ((isset($context["count_product"]) ? $context["count_product"] : null)) {
                        // line 239
                        echo "              ";
                        if ($this->getAttribute($context["rating"], "total", array(), "array")) {
                            echo " 
              <span class=\"label label-info tf-product-total\">";
                            // line 240
                            echo $this->getAttribute($context["rating"], "total", array(), "array");
                            echo "</span>
              ";
                        } else {
                            // line 242
                            echo "              <span class=\"label label-info label-danger tf-product-total\">";
                            echo $this->getAttribute($context["rating"], "total", array(), "array");
                            echo "</span>
              ";
                        }
                        // line 243
                        echo " 
            ";
                    }
                    // line 245
                    echo "          </div>
        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['rating'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 246
                echo " 
      </div>
    </div>
  </div>
  ";
            } elseif (($this->getAttribute(            // line 250
$context["filter"], "type", array(), "array") == "discount")) {
                echo " ";
                // line 251
                echo "  <div class=\"tf-filter-group col-xs-";
                echo twig_round((12 / (isset($context["column_xs"]) ? $context["column_xs"] : null)), 0, "ceil");
                echo " col-sm-";
                echo twig_round((12 / (isset($context["column_sm"]) ? $context["column_sm"] : null)), 0, "ceil");
                echo " col-md-";
                echo twig_round((12 / (isset($context["column_md"]) ? $context["column_md"] : null)), 0, "ceil");
                echo " col-lg-";
                echo twig_round((12 / (isset($context["column_lg"]) ? $context["column_lg"] : null)), 0, "ceil");
                echo "\">
    <div class=\"tf-filter-group-header ";
                // line 252
                echo (($this->getAttribute($context["filter"], "collapse", array(), "array")) ? ("collapsed") : (""));
                echo "\" data-toggle=\"collapse\" href=\"#tf-filter-panel-";
                echo (isset($context["module_class_id"]) ? $context["module_class_id"] : null);
                echo "-";
                echo $context["key"];
                echo "\">
      <span class=\"tf-filter-group-title\">";
                // line 253
                echo (isset($context["text_discount"]) ? $context["text_discount"] : null);
                echo "</span>
      ";
                // line 254
                if ((isset($context["reset_group"]) ? $context["reset_group"] : null)) {
                    // line 255
                    echo "        ";
                    $context["total_selected"] = 0;
                    // line 256
                    echo "        ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["filter"], "values", array(), "array"));
                    foreach ($context['_seq'] as $context["_key"] => $context["discount"]) {
                        if ($this->getAttribute($context["discount"], "selected", array(), "array")) {
                            $context["total_selected"] = ((isset($context["total_selected"]) ? $context["total_selected"] : null) + 1);
                        }
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['discount'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 257
                    echo "        <a data-tf-reset=\"check\" data-toggle=\"tooltip\" title=\"";
                    echo (isset($context["text_reset"]) ? $context["text_reset"] : null);
                    echo "\" class=\"tf-filter-reset";
                    echo (( !(isset($context["total_selected"]) ? $context["total_selected"] : null)) ? (" hide") : (""));
                    echo "\"><i class=\"fa fa-times\"></i></a>
      ";
                }
                // line 259
                echo "      <i class=\"fa fa-caret-up toggle-icon\"></i>
    </div>
    <div id=\"tf-filter-panel-";
                // line 261
                echo (isset($context["module_class_id"]) ? $context["module_class_id"] : null);
                echo "-";
                echo $context["key"];
                echo "\" class=\"collapse ";
                echo (( !$this->getAttribute($context["filter"], "collapse", array(), "array")) ? ("in") : (""));
                echo "\" >
      <div class=\"tf-filter-group-content\">
        ";
                // line 263
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["filter"], "values", array(), "array"));
                foreach ($context['_seq'] as $context["_key"] => $context["discount"]) {
                    echo " 
          <div class=\"form-check tf-filter-value custom-radio\">
            <label class=\"form-check-label\">
              ";
                    // line 266
                    if ($this->getAttribute($context["discount"], "selected", array(), "array")) {
                        echo " 
              <input type=\"radio\" value=\"";
                        // line 267
                        echo $this->getAttribute($context["discount"], "value", array(), "array");
                        echo "\" name=\"tf_fd\" class=\"form-check-input\" checked>
              ";
                    } else {
                        // line 268
                        echo " 
              <input type=\"radio\" value=\"";
                        // line 269
                        echo $this->getAttribute($context["discount"], "value", array(), "array");
                        echo "\" name=\"tf_fd\" class=\"form-check-input\" ";
                        echo (( !$this->getAttribute($context["discount"], "status", array(), "array")) ? ("disabled") : (""));
                        echo ">
              ";
                    }
                    // line 271
                    echo "              <span class=\"checkmark fa\"></span>
              ";
                    // line 272
                    echo $this->getAttribute($context["discount"], "name", array(), "array");
                    echo "
            </label>
            ";
                    // line 274
                    if ((isset($context["count_product"]) ? $context["count_product"] : null)) {
                        // line 275
                        echo "              ";
                        if ($this->getAttribute($context["discount"], "total", array(), "array")) {
                            echo " 
              <span class=\"label label-info tf-product-total\">";
                            // line 276
                            echo $this->getAttribute($context["discount"], "total", array(), "array");
                            echo "</span>
              ";
                        } else {
                            // line 278
                            echo "              <span class=\"label label-info label-danger tf-product-total\">";
                            echo $this->getAttribute($context["discount"], "total", array(), "array");
                            echo "</span>
              ";
                        }
                        // line 279
                        echo " 
            ";
                    }
                    // line 281
                    echo "          </div>
        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['discount'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 283
                echo "      </div>
    </div>
  </div>
  ";
            } elseif (($this->getAttribute(            // line 286
$context["filter"], "type", array(), "array") == "filter")) {
                echo " ";
                // line 287
                echo "  <div class=\"tf-filter-group col-xs-";
                echo twig_round((12 / (isset($context["column_xs"]) ? $context["column_xs"] : null)), 0, "ceil");
                echo " col-sm-";
                echo twig_round((12 / (isset($context["column_sm"]) ? $context["column_sm"] : null)), 0, "ceil");
                echo " col-md-";
                echo twig_round((12 / (isset($context["column_md"]) ? $context["column_md"] : null)), 0, "ceil");
                echo " col-lg-";
                echo twig_round((12 / (isset($context["column_lg"]) ? $context["column_lg"] : null)), 0, "ceil");
                echo "\">
    <div class=\"tf-filter-group-header ";
                // line 288
                echo (($this->getAttribute($context["filter"], "collapse", array(), "array")) ? ("collapsed") : (""));
                echo "\" data-toggle=\"collapse\" href=\"#tf-filter-panel-";
                echo (isset($context["module_class_id"]) ? $context["module_class_id"] : null);
                echo "-";
                echo $context["key"];
                echo "\">
      <span  class=\"tf-filter-group-title\">";
                // line 289
                echo $this->getAttribute($context["filter"], "name", array(), "array");
                echo "</span>
      ";
                // line 290
                if ((isset($context["reset_group"]) ? $context["reset_group"] : null)) {
                    // line 291
                    echo "        ";
                    $context["total_selected"] = 0;
                    // line 292
                    echo "        ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["filter"], "values", array(), "array"));
                    foreach ($context['_seq'] as $context["_key"] => $context["value"]) {
                        if ($this->getAttribute($context["value"], "selected", array(), "array")) {
                            $context["total_selected"] = ((isset($context["total_selected"]) ? $context["total_selected"] : null) + 1);
                        }
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['value'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 293
                    echo "        <a data-tf-reset=\"check\" data-toggle=\"tooltip\" title=\"";
                    echo (isset($context["text_reset"]) ? $context["text_reset"] : null);
                    echo "\" class=\" tf-filter-reset";
                    echo (( !(isset($context["total_selected"]) ? $context["total_selected"] : null)) ? (" hide") : (""));
                    echo "\"><i class=\"fa fa-times\"></i></a>
      ";
                }
                // line 295
                echo "      <i class=\"fa fa-caret-up toggle-icon\"></i>
    </div>
    <div id=\"tf-filter-panel-";
                // line 297
                echo (isset($context["module_class_id"]) ? $context["module_class_id"] : null);
                echo "-";
                echo $context["key"];
                echo "\" class=\"collapse ";
                echo (( !$this->getAttribute($context["filter"], "collapse", array(), "array")) ? ("in") : (""));
                echo "\" >
      ";
                // line 298
                if ($this->getAttribute($context["filter"], "search", array(), "array")) {
                    // line 299
                    echo "      <div class=\"tf-filter-group-search\"><i class=\"fa fa-search\"></i> <input type=\"search\" placeholder=\"";
                    echo (isset($context["text_search"]) ? $context["text_search"] : null);
                    echo "\"/></div>
      ";
                }
                // line 301
                echo "      <div class=\"tf-filter-group-content ";
                echo (isset($context["overflow"]) ? $context["overflow"] : null);
                echo "\">
        ";
                // line 302
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["filter"], "values", array(), "array"));
                foreach ($context['_seq'] as $context["_key"] => $context["value"]) {
                    echo " 
          <div class=\"form-check tf-filter-value custom-checkbox\">
            <label class=\"form-check-label\">
              ";
                    // line 305
                    if ($this->getAttribute($context["value"], "selected", array(), "array")) {
                        echo " 
              <input type=\"checkbox\" name=\"tf_ff\" value=\"";
                        // line 306
                        echo $this->getAttribute($context["value"], "filter_id", array(), "array");
                        echo "\" class=\"form-check-input\" checked>
              ";
                    } else {
                        // line 307
                        echo " 
              <input type=\"checkbox\" name=\"tf_ff\" value=\"";
                        // line 308
                        echo $this->getAttribute($context["value"], "filter_id", array(), "array");
                        echo "\" class=\"form-check-input\" ";
                        echo (( !$this->getAttribute($context["value"], "status", array(), "array")) ? ("disabled") : (""));
                        echo ">
              ";
                    }
                    // line 310
                    echo "              <span class=\"checkmark fa\"></span>
              ";
                    // line 311
                    echo $this->getAttribute($context["value"], "name", array(), "array");
                    echo "
            </label>
            ";
                    // line 313
                    if ((isset($context["count_product"]) ? $context["count_product"] : null)) {
                        // line 314
                        echo "              ";
                        if ($this->getAttribute($context["value"], "total", array(), "array")) {
                            echo " 
              <span class=\"label label-info tf-product-total\">";
                            // line 315
                            echo $this->getAttribute($context["value"], "total", array(), "array");
                            echo "</span>
              ";
                        } else {
                            // line 317
                            echo "              <span class=\"label label-info label-danger tf-product-total\">";
                            echo $this->getAttribute($context["value"], "total", array(), "array");
                            echo "</span>
              ";
                        }
                        // line 318
                        echo " 
            ";
                    }
                    // line 320
                    echo "          </div>
        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['value'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 322
                echo "        ";
                if ((((isset($context["overflow"]) ? $context["overflow"] : null) == "more") && (twig_length_filter($this->env, $this->getAttribute($context["filter"], "values", array(), "array")) >= 7))) {
                    // line 323
                    echo "          <a class=\"tf-see-more btn-link\" data-toggle=\"tf-seemore\" data-show=\"";
                    echo (isset($context["text_see_more"]) ? $context["text_see_more"] : null);
                    echo "\" data-hide=\"";
                    echo (isset($context["text_see_less"]) ? $context["text_see_less"] : null);
                    echo "\" href=\"#\">";
                    echo (isset($context["text_see_more"]) ? $context["text_see_more"] : null);
                    echo "</a>
        ";
                }
                // line 325
                echo "      </div>
    </div>
  </div>
  ";
            } elseif (($this->getAttribute(            // line 328
$context["filter"], "type", array(), "array") == "custom")) {
                echo " ";
                // line 329
                echo "  <div class=\"tf-filter-group col-xs-";
                echo twig_round((12 / (isset($context["column_xs"]) ? $context["column_xs"] : null)), 0, "ceil");
                echo " col-sm-";
                echo twig_round((12 / (isset($context["column_sm"]) ? $context["column_sm"] : null)), 0, "ceil");
                echo " col-md-";
                echo twig_round((12 / (isset($context["column_md"]) ? $context["column_md"] : null)), 0, "ceil");
                echo " col-lg-";
                echo twig_round((12 / (isset($context["column_lg"]) ? $context["column_lg"] : null)), 0, "ceil");
                echo "\">
    <div class=\"tf-filter-group-header ";
                // line 330
                echo (($this->getAttribute($context["filter"], "collapse", array(), "array")) ? ("collapsed") : (""));
                echo "\" data-toggle=\"collapse\" href=\"#tf-filter-panel-";
                echo (isset($context["module_class_id"]) ? $context["module_class_id"] : null);
                echo "-";
                echo $context["key"];
                echo "\">
      <span class=\"tf-filter-group-title\">";
                // line 331
                echo $this->getAttribute($context["filter"], "name", array(), "array");
                echo "</span>
      ";
                // line 332
                if ((isset($context["reset_group"]) ? $context["reset_group"] : null)) {
                    // line 333
                    echo "        ";
                    $context["total_selected"] = 0;
                    // line 334
                    echo "        ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["filter"], "values", array(), "array"));
                    foreach ($context['_seq'] as $context["_key"] => $context["value"]) {
                        if ($this->getAttribute($context["value"], "selected", array(), "array")) {
                            $context["total_selected"] = ((isset($context["total_selected"]) ? $context["total_selected"] : null) + 1);
                        }
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['value'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 335
                    echo "        <a data-tf-reset=\"check\" data-toggle=\"tooltip\" title=\"";
                    echo (isset($context["text_reset"]) ? $context["text_reset"] : null);
                    echo "\" class=\"tf-filter-reset";
                    echo (( !(isset($context["total_selected"]) ? $context["total_selected"] : null)) ? (" hide") : (""));
                    echo "\"><i class=\"fa fa-times\"></i></a>
      ";
                }
                // line 337
                echo "      <i class=\"fa fa-caret-up toggle-icon\"></i>
    </div>
    <div id=\"tf-filter-panel-";
                // line 339
                echo (isset($context["module_class_id"]) ? $context["module_class_id"] : null);
                echo "-";
                echo $context["key"];
                echo "\" data-custom-filter=\"";
                echo $this->getAttribute($context["filter"], "filter_id", array(), "array");
                echo "\" class=\"collapse ";
                echo (( !$this->getAttribute($context["filter"], "collapse", array(), "array")) ? ("in") : (""));
                echo "\" >
      ";
                // line 340
                if ($this->getAttribute($context["filter"], "search", array(), "array")) {
                    // line 341
                    echo "      <div class=\"tf-filter-group-search\"><i class=\"fa fa-search\"></i> <input type=\"search\" placeholder=\"";
                    echo (isset($context["text_search"]) ? $context["text_search"] : null);
                    echo "\"/></div>
      ";
                }
                // line 343
                echo "      <div class=\"tf-filter-group-content ";
                echo (isset($context["overflow"]) ? $context["overflow"] : null);
                echo "\">
        ";
                // line 344
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["filter"], "values", array(), "array"));
                foreach ($context['_seq'] as $context["_key"] => $context["value"]) {
                    // line 345
                    echo "        <div class=\"form-check tf-filter-value custom-";
                    echo $this->getAttribute($context["filter"], "input_type", array(), "array");
                    echo " ";
                    echo $this->getAttribute($context["filter"], "list_type", array(), "array");
                    echo "\">
          <label class=\"form-check-label\">
            ";
                    // line 347
                    if ($this->getAttribute($context["value"], "selected", array(), "array")) {
                        echo " 
            <input type=\"";
                        // line 348
                        echo $this->getAttribute($context["filter"], "input_type", array(), "array");
                        echo "\" name=\"tf_fc";
                        echo $this->getAttribute($context["filter"], "filter_id", array(), "array");
                        echo "\" value=\"";
                        echo $this->getAttribute($context["value"], "value_id", array(), "array");
                        echo "\" class=\"form-check-input\" checked>
            ";
                    } else {
                        // line 349
                        echo " 
            <input type=\"";
                        // line 350
                        echo $this->getAttribute($context["filter"], "input_type", array(), "array");
                        echo "\" name=\"tf_fc";
                        echo $this->getAttribute($context["filter"], "filter_id", array(), "array");
                        echo "\" value=\"";
                        echo $this->getAttribute($context["value"], "value_id", array(), "array");
                        echo "\" class=\"form-check-input\" ";
                        echo (( !$this->getAttribute($context["value"], "status", array(), "array")) ? ("disabled") : (""));
                        echo ">
            ";
                    }
                    // line 351
                    echo " 
            ";
                    // line 352
                    if ((($this->getAttribute($context["filter"], "list_type", array(), "array") == "image") || ($this->getAttribute($context["filter"], "list_type", array(), "array") == "both"))) {
                        echo " 
            <img src=\"";
                        // line 353
                        echo $this->getAttribute($context["value"], "image", array(), "array");
                        echo "\" title=\"";
                        echo $this->getAttribute($context["value"], "name", array(), "array");
                        echo "\" alt=\"";
                        echo $this->getAttribute($context["value"], "name", array(), "array");
                        echo "\" />
            ";
                    } else {
                        // line 355
                        echo "            <span class=\"checkmark fa\"></span>
            ";
                    }
                    // line 356
                    echo " 
            ";
                    // line 357
                    if ((($this->getAttribute($context["filter"], "list_type", array(), "array") == "text") || ($this->getAttribute($context["filter"], "list_type", array(), "array") == "both"))) {
                        echo " 
              ";
                        // line 358
                        echo $this->getAttribute($context["value"], "name", array(), "array");
                        echo "
            ";
                    }
                    // line 360
                    echo "          </label>
          ";
                    // line 361
                    if (((isset($context["count_product"]) ? $context["count_product"] : null) && ($this->getAttribute($context["filter"], "list_type", array(), "array") != "image"))) {
                        // line 362
                        echo "            ";
                        if ($this->getAttribute($context["value"], "total", array(), "array")) {
                            echo " 
            <span class=\"label label-info tf-product-total\">";
                            // line 363
                            echo $this->getAttribute($context["value"], "total", array(), "array");
                            echo "</span>
            ";
                        } else {
                            // line 365
                            echo "            <span class=\"label label-info label-danger tf-product-total\">";
                            echo $this->getAttribute($context["value"], "total", array(), "array");
                            echo "</span>
            ";
                        }
                        // line 366
                        echo " 
          ";
                    }
                    // line 368
                    echo "        </div>
       ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['value'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 370
                echo "       ";
                if ((((isset($context["overflow"]) ? $context["overflow"] : null) == "more") && (twig_length_filter($this->env, $this->getAttribute($context["filter"], "values", array(), "array")) >= 7))) {
                    // line 371
                    echo "        <a class=\"tf-see-more btn-link\" data-toggle=\"tf-seemore\" data-show=\"";
                    echo (isset($context["text_see_more"]) ? $context["text_see_more"] : null);
                    echo "\" data-hide=\"";
                    echo (isset($context["text_see_less"]) ? $context["text_see_less"] : null);
                    echo "\" href=\"#\">";
                    echo (isset($context["text_see_more"]) ? $context["text_see_more"] : null);
                    echo "</a>
       ";
                }
                // line 373
                echo "      </div>
    </div>
  </div>
  ";
            }
            // line 376
            echo " 
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['filter'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 378
        echo "</div>
</div>
<script>
\$(function(){
    if(window.innerWidth < 767){ // Collaped all panel in small device
        \$('.tf-filter .collapse.in').collapse(\"hide\");
    }
    
    // Filter
    var paginationContainer = \$('#content').children('.row').last();
    var productContainer = paginationContainer.prev();
    
    productContainer.css('position', 'relative');
            
    \$('#tf-filter-";
        // line 392
        echo (isset($context["module_class_id"]) ? $context["module_class_id"] : null);
        echo "').tf_filter({
        requestURL: \"";
        // line 393
        echo (isset($context["requestURL"]) ? $context["requestURL"] : null);
        echo "\",
        searchEl: \$('.tf-filter-group-search input'),
        ajax: ";
        // line 395
        echo (((isset($context["ajax"]) ? $context["ajax"] : null)) ? ("true") : ("false"));
        echo ",
        delay: ";
        // line 396
        echo (((isset($context["delay"]) ? $context["delay"] : null)) ? ("true") : ("false"));
        echo ",
        search_in_description: ";
        // line 397
        echo (((isset($context["search_in_description"]) ? $context["search_in_description"] : null)) ? ("true") : ("false"));
        echo ",
        countProduct: ";
        // line 398
        echo (((isset($context["count_product"]) ? $context["count_product"] : null)) ? ("true") : ("false"));
        echo ",
        sortBy: '";
        // line 399
        echo (isset($context["sort_by"]) ? $context["sort_by"] : null);
        echo "',
        onParamChange: function(param){
            \$(\"#input-limit,#input-sort\").find('option').each(function(){
                var url = \$(this).attr('value');
                \$(this).attr('value', modifyURLQuery(url, \$.extend({}, param, {page: null})));
            });
            var currency = \$('#form-currency input[name=\"redirect\"]');
            currency.val(modifyURLQuery(currency.val(), \$.extend({}, param, {tf_fp: null, page: null})));
            
            // Show or hide reset all button
            if(\$('.tf-filter-group [data-tf-reset]:not(.hide)').length){
                \$('[data-tf-reset=\"all\"]').removeClass('hide');
            } else {
                \$('[data-tf-reset=\"all\"]').addClass('hide');
            }
        },
        onInputChange: function(e){
            var filter_group = \$(e.target).closest('.tf-filter-group');
            
            var is_input_selected = false;
            
            // Hide Reset for Checkbox or radio
            if(filter_group.find('input[type=\"checkbox\"]:checked,input[type=\"radio\"]:checked').length){
                is_input_selected = true;
            }
            
            // Hide Reset for price
            if(\$(e.target).filter('[name=\"tf_fp[min]\"],[name=\"tf_fp[max]\"]').length){
                if(\$('[name=\"tf_fp[min]\"]').val() !== \$('[name=\"tf_fp[min]\"]').attr('min') || \$('[name=\"tf_fp[max]\"]').val() !== \$('[name=\"tf_fp[max]\"]').attr('max')){
                    is_input_selected = true;
                }
            }
            
            // Hide reset for text
            if(\$(e.target).filter('[type=\"text\"]').val()){
                is_input_selected = true;
            }
            
            // Hide or show reset buton
            if(is_input_selected){
                filter_group.find('[data-tf-reset]').removeClass('hide');
            } else {
                filter_group.find('[data-tf-reset]').addClass('hide');
            }
        },
        onReset: function(el_reset){
            var type = \$(el_reset).data('tf-reset');
            
            // Reset price
            if(type === 'price' || type === 'all'){
                price_slider.slider(\"values\", [parseFloat(price_slider.slider(\"option\", 'min')), parseFloat(price_slider.slider(\"option\", 'max'))]);
            }
            
            // Hide reset button
            if(\$(el_reset).data('tf-reset') !== 'all'){
                \$(el_reset).addClass('hide');
            } else {
                \$('[data-tf-reset]').addClass('hide');
            }
        },
        onBeforeSend: function(){
            productContainer.append('<div class=\"tf-loader\"><img src=\"catalog/view/javascript/maza/loader.gif\" /></div>');
        },
        onResult: function(json){
            var content = \$(json['content']).find('#content');
            var products = content.children('.row').last().prev().html();
            var pagination = content.children('.row').last().html();
            
            // Add result products to container
            if(products){
                \$(productContainer).html(products);
                
                \$('#list-view.active').click();
                \$('#grid-view.active').click();
            } else {
                \$(productContainer).html(\"<div class='col-xs-12 text-center'>";
        // line 474
        echo (isset($context["text_no_result"]) ? $context["text_no_result"] : null);
        echo "</div>\");
            }

            // Add pagination to container
            if(pagination){
                \$(paginationContainer).html(pagination);
            } else {
                \$(paginationContainer).empty();
            }
        }
    });
    
    // Price slider
    var price_slider = \$(\".tf-filter [data-role='rangeslider']\").slider({
        range: true,
        min: parseFloat(\$('[name=\"tf_fp[min]\"]').attr('min')),
        max: parseFloat(\$('[name=\"tf_fp[max]\"]').attr('max')),
        values: [parseFloat(\$('[name=\"tf_fp[min]\"]').val()), parseFloat(\$('[name=\"tf_fp[max]\"]').val())],
        slide: function( event, ui ) {
            \$('[name=\"tf_fp[min]\"]').val(ui.values[0]);
            \$('[name=\"tf_fp[max]\"]').val(ui.values[1]);
        },
        change: function( event, ui ) {
            // Hide Reset for price
            if(\$('[name=\"tf_fp[min]\"]').val() !== \$('[name=\"tf_fp[min]\"]').attr('min') || \$('[name=\"tf_fp[max]\"]').val() !== \$('[name=\"tf_fp[max]\"]').attr('max')){
                \$('[data-tf-reset=\"price\"]').removeClass('hide');
            } else {
                \$('[data-tf-reset=\"price\"]').addClass('hide');
            }
            
            // Trigger filter change
            \$('#tf-filter-";
        // line 505
        echo (isset($context["module_class_id"]) ? $context["module_class_id"] : null);
        echo "').change();
        }
    });
    \$('[name=\"tf_fp[min]\"]').change(function(){
        price_slider.slider(\"values\", 0, \$(this).val());
    });
    \$('[name=\"tf_fp[max]\"]').change(function(){
        price_slider.slider(\"values\", 1, \$(this).val());
    });
    
    // Show reset all button if filter is selected
    if(\$('.tf-filter-group [data-tf-reset]:not(.hide)').length){
        \$('[data-tf-reset=\"all\"]').removeClass('hide');
    }
    
    // Fix z-index
    \$('.tf-filter-group .collapse').on('show.bs.collapse', function(){
        var z_index = Number(\$('#tf-filter-content-";
        // line 522
        echo (isset($context["module_class_id"]) ? $context["module_class_id"] : null);
        echo "').data('mz-base-z-index')) + 1;
        \$(this).css('z-index', z_index);
        \$('#tf-filter-content-";
        // line 524
        echo (isset($context["module_class_id"]) ? $context["module_class_id"] : null);
        echo "').data('mz-base-z-index', z_index);
    });
});
</script>
<link href=\"catalog/view/theme/default/stylesheet/tf_filter.css\" rel=\"stylesheet\" media=\"screen\" />";
    }

    public function getTemplateName()
    {
        return "default/template/extension/module/tf_filter.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1598 => 524,  1593 => 522,  1573 => 505,  1539 => 474,  1461 => 399,  1457 => 398,  1453 => 397,  1449 => 396,  1445 => 395,  1440 => 393,  1436 => 392,  1420 => 378,  1413 => 376,  1407 => 373,  1397 => 371,  1394 => 370,  1387 => 368,  1383 => 366,  1377 => 365,  1372 => 363,  1367 => 362,  1365 => 361,  1362 => 360,  1357 => 358,  1353 => 357,  1350 => 356,  1346 => 355,  1337 => 353,  1333 => 352,  1330 => 351,  1319 => 350,  1316 => 349,  1307 => 348,  1303 => 347,  1295 => 345,  1291 => 344,  1286 => 343,  1280 => 341,  1278 => 340,  1268 => 339,  1264 => 337,  1256 => 335,  1244 => 334,  1241 => 333,  1239 => 332,  1235 => 331,  1227 => 330,  1216 => 329,  1213 => 328,  1208 => 325,  1198 => 323,  1195 => 322,  1188 => 320,  1184 => 318,  1178 => 317,  1173 => 315,  1168 => 314,  1166 => 313,  1161 => 311,  1158 => 310,  1151 => 308,  1148 => 307,  1143 => 306,  1139 => 305,  1131 => 302,  1126 => 301,  1120 => 299,  1118 => 298,  1110 => 297,  1106 => 295,  1098 => 293,  1086 => 292,  1083 => 291,  1081 => 290,  1077 => 289,  1069 => 288,  1058 => 287,  1055 => 286,  1050 => 283,  1043 => 281,  1039 => 279,  1033 => 278,  1028 => 276,  1023 => 275,  1021 => 274,  1016 => 272,  1013 => 271,  1006 => 269,  1003 => 268,  998 => 267,  994 => 266,  986 => 263,  977 => 261,  973 => 259,  965 => 257,  953 => 256,  950 => 255,  948 => 254,  944 => 253,  936 => 252,  925 => 251,  922 => 250,  916 => 246,  909 => 245,  905 => 243,  899 => 242,  894 => 240,  889 => 239,  887 => 238,  882 => 236,  878 => 234,  871 => 233,  866 => 231,  860 => 229,  854 => 228,  850 => 226,  843 => 224,  840 => 223,  835 => 222,  831 => 221,  823 => 218,  814 => 216,  810 => 214,  802 => 212,  790 => 211,  787 => 210,  785 => 209,  781 => 208,  773 => 207,  762 => 206,  759 => 205,  753 => 201,  749 => 199,  743 => 198,  738 => 196,  733 => 195,  731 => 194,  726 => 192,  722 => 190,  717 => 189,  714 => 188,  708 => 186,  703 => 183,  699 => 181,  693 => 180,  688 => 178,  683 => 177,  681 => 176,  676 => 174,  672 => 172,  667 => 171,  664 => 170,  658 => 168,  647 => 164,  643 => 162,  640 => 161,  634 => 159,  628 => 157,  625 => 156,  623 => 155,  619 => 154,  611 => 153,  600 => 152,  597 => 151,  588 => 147,  579 => 145,  575 => 143,  572 => 142,  566 => 140,  560 => 138,  557 => 137,  555 => 136,  551 => 135,  543 => 134,  532 => 133,  529 => 132,  524 => 129,  514 => 127,  511 => 126,  504 => 124,  500 => 122,  494 => 121,  489 => 119,  484 => 118,  482 => 117,  479 => 116,  474 => 114,  470 => 113,  467 => 112,  463 => 111,  454 => 109,  450 => 108,  447 => 107,  438 => 106,  435 => 105,  428 => 104,  424 => 103,  417 => 101,  411 => 100,  406 => 99,  400 => 97,  398 => 96,  390 => 95,  386 => 93,  378 => 91,  366 => 90,  363 => 89,  361 => 88,  357 => 87,  349 => 86,  338 => 85,  335 => 84,  329 => 80,  319 => 78,  316 => 77,  309 => 75,  305 => 73,  299 => 72,  294 => 70,  289 => 69,  287 => 68,  284 => 67,  279 => 65,  275 => 64,  272 => 63,  268 => 62,  259 => 60,  255 => 59,  252 => 58,  243 => 57,  240 => 56,  233 => 55,  229 => 54,  222 => 52,  216 => 51,  211 => 50,  205 => 48,  203 => 47,  195 => 46,  191 => 44,  183 => 42,  171 => 41,  168 => 40,  166 => 39,  162 => 38,  154 => 37,  143 => 36,  140 => 35,  127 => 29,  119 => 28,  108 => 24,  104 => 22,  101 => 21,  95 => 19,  89 => 17,  86 => 16,  84 => 15,  80 => 14,  72 => 13,  61 => 12,  58 => 11,  52 => 10,  46 => 9,  42 => 7,  36 => 5,  34 => 4,  30 => 3,  24 => 2,  19 => 1,);
    }
}
/* <div id="tf-filter-{{ module_class_id }}" class="panel tf-filter panel-default">*/
/* <div data-toggle="collapse" href="#tf-filter-content-{{ module_class_id }}" class="panel-heading{{ collapsed?' collapsed' }}">*/
/*   <h4 class="panel-title">{{ heading_title }}</h4>*/
/*   {% if reset_all %}*/
/*     <span data-tf-reset="all" data-toggle="tooltip" title="{{ text_reset_all }}" class="tf-filter-reset hide text-danger"><i class="fa fa-times"></i></span>*/
/*   {% endif %}*/
/*   <i class="fa fa-chevron-circle-down" aria-hidden="true"></i>*/
/* </div>*/
/* <div id="tf-filter-content-{{ module_class_id }}" data-mz-base-z-index="99" class="collapse{{ not collapsed?' in' }} tf-list-filter-group row">*/
/* {% for key,filter in filters %} */
/*   {% if (filter['type'] == 'price') %} {# price range #}*/
/*   <div class="tf-filter-group col-xs-{{ (12/column_xs)|round(0, 'ceil') }} col-sm-{{ (12/column_sm)|round(0, 'ceil') }} col-md-{{ (12/column_md)|round(0, 'ceil') }} col-lg-{{ (12/column_lg)|round(0, 'ceil') }}">*/
/*     <div class="tf-filter-group-header {{ filter['collapse']?'collapsed' }}" data-toggle="collapse" href="#tf-filter-panel-{{ module_class_id }}-{{ key }}">*/
/*       <span class="tf-filter-group-title">{{ text_price }}</span>*/
/*       {% if reset_group %}*/
/*         {% if filter['selected']['min'] != filter['min_price'] or filter['selected']['max'] != filter['max_price'] %}*/
/*         <a data-tf-reset="price" data-toggle="tooltip" title="{{ text_reset }}" class="tf-filter-reset"><i class="fa fa-times"></i></a>*/
/*         {% else %}*/
/*         <a data-tf-reset="price" data-toggle="tooltip" title="{{ text_reset }}" class="tf-filter-reset hide"><i class="fa fa-times"></i></a>*/
/*         {% endif %}*/
/*       {% endif %}*/
/*       <i class="fa fa-caret-up toggle-icon"></i>*/
/*     </div>*/
/*     <div id="tf-filter-panel-{{ module_class_id }}-{{ key }}" class="collapse {{ not filter['collapse']?'in':'' }}" >*/
/*       <div class="tf-filter-group-content">*/
/*         <div data-role="rangeslider"></div>*/
/*         <div class="row">*/
/*           <div class="col-xs-6"><input type="number" class="form-control" name="tf_fp[min]" value="{{ filter['selected']['min'] }}" min="{{ filter['min_price'] }}" max="{{ filter['max_price'] - 1 }}" /></div>*/
/*           <div class="col-xs-6"><input type="number" class="form-control" name="tf_fp[max]" value="{{ filter['selected']['max'] }}" min="{{ filter['min_price'] + 1 }}" max="{{ filter['max_price'] }}" /></div>*/
/*         </div>*/
/*       </div>*/
/*     </div>*/
/*   </div>*/
/*         */
/*   {% elseif (filter['type'] == 'sub_category' and filter['values']) %} {# Manufacturer #}*/
/*   <div class="tf-filter-group col-xs-{{ (12/column_xs)|round(0, 'ceil') }} col-sm-{{ (12/column_sm)|round(0, 'ceil') }} col-md-{{ (12/column_md)|round(0, 'ceil') }} col-lg-{{ (12/column_lg)|round(0, 'ceil') }}">*/
/*     <div class="tf-filter-group-header {{ filter['collapse']?'collapsed' }}" data-toggle="collapse" href="#tf-filter-panel-{{ module_class_id }}-{{ key }}">*/
/*       <span class="tf-filter-group-title">{{ text_sub_category }}</span>*/
/*       {% if reset_group %}*/
/*         {% set total_selected = 0 %}*/
/*         {% for sub_category in filter['values'] if sub_category['selected'] %}{% set total_selected = total_selected + 1 %}{% endfor %}*/
/*         <a data-tf-reset="check" data-toggle="tooltip" title="{{ text_reset }}" class=" tf-filter-reset{{ not total_selected?' hide' }}"><i class="fa fa-times"></i></a>*/
/*       {% endif %}*/
/*       <i class="fa fa-caret-up toggle-icon"></i>*/
/*     </div>*/
/*     <div id="tf-filter-panel-{{ module_class_id }}-{{ key }}" class="collapse {{ not filter['collapse']?'in':'' }}" >*/
/*       {% if filter['search'] %}*/
/*       <div class="tf-filter-group-search"><i class="fa fa-search"></i> <input type="search" placeholder="{{ text_search }}"/></div>*/
/*       {% endif %}*/
/*       <div class="tf-filter-group-content {{ overflow }}">*/
/*       {% for sub_category in filter['values'] %} */
/*         <div class="form-check tf-filter-value custom-{{ filter['input_type'] }} {{ filter['list_type'] }}">*/
/*           <label class="form-check-label">*/
/*             {% if (sub_category['selected']) %} */
/*             <input type="{{ filter['input_type'] }}" name="tf_fsc" value="{{ sub_category['category_id'] }}" class="form-check-input" checked>*/
/*             {% else %} */
/*             <input type="{{ filter['input_type'] }}" name="tf_fsc" value="{{ sub_category['category_id'] }}" class="form-check-input" {{ not sub_category['status']?'disabled' }}>*/
/*             {% endif %} */
/*             {% if (filter['list_type'] == 'image' or filter['list_type'] == 'both') %} */
/*             <img src="{{ sub_category['image'] }}" title="{{ sub_category['name'] }}" alt="{{ sub_category['name'] }}" />*/
/*             {% else %}*/
/*             <span class="checkmark fa"></span>*/
/*             {% endif %} */
/*             {% if (filter['list_type'] == 'text' or filter['list_type'] == 'both') %} */
/*               {{ sub_category['name'] }}*/
/*             {% endif %}*/
/*           </label>*/
/*           {% if count_product and filter['list_type'] != 'image' %}*/
/*             {% if (sub_category['total']) %} */
/*             <span class="label label-info tf-product-total">{{ sub_category['total'] }}</span>*/
/*             {% else %}*/
/*             <span class="label label-info label-danger tf-product-total">{{ sub_category['total'] }}</span>*/
/*             {% endif %} */
/*           {% endif %}*/
/*         </div>*/
/*       {% endfor %}*/
/*       {% if overflow == 'more' and filter['values']|length >= 7 %}*/
/*         <a class="tf-see-more btn-link" data-toggle="tf-seemore" data-show="{{ text_see_more }}" data-hide="{{ text_see_less }}" href="#">{{ text_see_more }}</a>*/
/*       {% endif %}*/
/*       </div>*/
/*     </div>*/
/*   </div>*/
/*       */
/*   {% elseif (filter['type'] == 'manufacturer' and filter['values']) %} {# Manufacturer #}*/
/*   <div class="tf-filter-group col-xs-{{ (12/column_xs)|round(0, 'ceil') }} col-sm-{{ (12/column_sm)|round(0, 'ceil') }} col-md-{{ (12/column_md)|round(0, 'ceil') }} col-lg-{{ (12/column_lg)|round(0, 'ceil') }}">*/
/*     <div class="tf-filter-group-header {{ filter['collapse']?'collapsed' }}" data-toggle="collapse" href="#tf-filter-panel-{{ module_class_id }}-{{ key }}">*/
/*       <span class="tf-filter-group-title">{{ text_manufacturer }}</span>*/
/*       {% if reset_group %}*/
/*         {% set total_selected = 0 %}*/
/*         {% for manufacturer in filter['values'] if manufacturer['selected'] %}{% set total_selected = total_selected + 1 %}{% endfor %}*/
/*         <a data-tf-reset="check" data-toggle="tooltip" title="{{ text_reset }}" class=" tf-filter-reset{{ not total_selected?' hide' }}"><i class="fa fa-times"></i></a>*/
/*       {% endif %}*/
/*       <i class="fa fa-caret-up toggle-icon"></i>*/
/*     </div>*/
/*     <div id="tf-filter-panel-{{ module_class_id }}-{{ key }}" class="collapse {{ not filter['collapse']?'in':'' }}" >*/
/*       {% if filter['search'] %}*/
/*       <div class="tf-filter-group-search"><i class="fa fa-search"></i> <input type="search" placeholder="{{ text_search }}"/></div>*/
/*       {% endif %}*/
/*       <div class="tf-filter-group-content {{ overflow }}">*/
/*       {% for manufacturer in filter['values'] %} */
/*         <div class="form-check tf-filter-value custom-{{ filter['input_type'] }} {{ filter['list_type'] }}">*/
/*           <label class="form-check-label">*/
/*             {% if (manufacturer['selected']) %} */
/*             <input type="{{ filter['input_type'] }}" name="tf_fm" value="{{ manufacturer['manufacturer_id'] }}" class="form-check-input" checked>*/
/*             {% else %} */
/*             <input type="{{ filter['input_type'] }}" name="tf_fm" value="{{ manufacturer['manufacturer_id'] }}" class="form-check-input" {{ not manufacturer['status']?'disabled' }}>*/
/*             {% endif %} */
/*             {% if (filter['list_type'] == 'image' or filter['list_type'] == 'both') %} */
/*             <img src="{{ manufacturer['image'] }}" title="{{ manufacturer['name'] }}" alt="{{ manufacturer['name'] }}" />*/
/*             {% else %}*/
/*             <span class="checkmark fa"></span>*/
/*             {% endif %} */
/*             {% if (filter['list_type'] == 'text' or filter['list_type'] == 'both') %} */
/*               {{ manufacturer['name'] }}*/
/*             {% endif %}*/
/*           </label>*/
/*           {% if count_product and filter['list_type'] != 'image' %}*/
/*             {% if (manufacturer['total']) %} */
/*             <span class="label label-info tf-product-total">{{ manufacturer['total'] }}</span>*/
/*             {% else %}*/
/*             <span class="label label-info label-danger tf-product-total">{{ manufacturer['total'] }}</span>*/
/*             {% endif %} */
/*           {% endif %}*/
/*         </div>*/
/*       {% endfor %}*/
/*       {% if overflow == 'more' and filter['values']|length >= 7 %}*/
/*         <a class="tf-see-more btn-link" data-toggle="tf-seemore" data-show="{{ text_see_more }}" data-hide="{{ text_see_less }}" href="#">{{ text_see_more }}</a>*/
/*       {% endif %}*/
/*       </div>*/
/*     </div>*/
/*   </div>*/
/*   {% elseif (filter['type'] == 'search') %} {# Search #}*/
/*   <div class="tf-filter-group col-xs-{{ (12/column_xs)|round(0, 'ceil') }} col-sm-{{ (12/column_sm)|round(0, 'ceil') }} col-md-{{ (12/column_md)|round(0, 'ceil') }} col-lg-{{ (12/column_lg)|round(0, 'ceil') }}">*/
/*     <div class="tf-filter-group-header {{ filter['collapse']?'collapsed' }}" data-toggle="collapse" href="#tf-filter-panel-{{ module_class_id }}-{{ key }}">*/
/*       <span  class="tf-filter-group-title">{{ text_search }}</span>*/
/*       {% if reset_group %}*/
/*         {% if filter['keyword'] %}*/
/*         <a data-tf-reset="text" data-toggle="tooltip" title="{{ text_reset }}" class="tf-filter-reset"><i class="fa fa-times"></i></a>*/
/*         {% else %}*/
/*         <a data-tf-reset="text" data-toggle="tooltip" title="{{ text_reset }}" class="tf-filter-reset hide"><i class="fa fa-times"></i></a>*/
/*         {% endif %}*/
/*       {% endif %}*/
/*       <i class="fa fa-caret-up toggle-icon"></i>*/
/*     </div>*/
/*     <div id="tf-filter-panel-{{ module_class_id }}-{{ key }}" class="collapse {{ not filter['collapse']?'in':'' }}" >*/
/*       <div class="tf-filter-group-content">*/
/*         <input type="text" name="tf_fq" value="{{ filter['keyword'] }}" placeholder="{{ text_search_placeholder }}" class="form-control" />*/
/*       </div>*/
/*     </div>*/
/*   </div>*/
/*   {% elseif (filter['type'] == 'availability') %} {# Availability #}*/
/*   <div class="tf-filter-group col-xs-{{ (12/column_xs)|round(0, 'ceil') }} col-sm-{{ (12/column_sm)|round(0, 'ceil') }} col-md-{{ (12/column_md)|round(0, 'ceil') }} col-lg-{{ (12/column_lg)|round(0, 'ceil') }}">*/
/*     <div class="tf-filter-group-header {{ filter['collapse']?'collapsed' }}" data-toggle="collapse" href="#tf-filter-panel-{{ module_class_id }}-{{ key }}">*/
/*       <span class="tf-filter-group-title">{{ text_availability }}</span>*/
/*       {% if reset_group %}*/
/*         {% if filter['values']['in_stock']['selected'] or filter['values']['out_of_stock']['selected'] %}*/
/*         <a data-tf-reset="check" data-toggle="tooltip" title="{{ text_reset }}" class=" tf-filter-reset"><i class="fa fa-times"></i></a>*/
/*         {% else %}*/
/*         <a data-tf-reset="check" data-toggle="tooltip" title="{{ text_reset }}" class=" tf-filter-reset hide"><i class="fa fa-times"></i></a>*/
/*         {% endif %}*/
/*       {% endif %}*/
/*       <i class="fa fa-caret-up toggle-icon"></i>*/
/*     </div>*/
/*     <div id="tf-filter-panel-{{ module_class_id }}-{{ key }}" class="collapse {{ not filter['collapse']?'in':'' }}" >*/
/*       <div class="tf-filter-group-content">*/
/*         <div class="form-check tf-filter-value custom-radio">*/
/*           <label class="form-check-label">*/
/*             {% if (filter['values']['in_stock']['selected']) %} */
/*             <input type="radio" value="1" name="tf_fs" class="form-check-input" checked>*/
/*             {% else %} */
/*             <input type="radio" value="1" name="tf_fs" class="form-check-input" {{ not filter['values']['in_stock']['status']?'disabled' }}>*/
/*             {% endif %} */
/*             <span class="checkmark fa"></span>*/
/*             {{ text_in_stock }}*/
/*           </label>*/
/*           {% if count_product %}*/
/*             {% if (filter['values']['in_stock']['total']) %} */
/*             <span class="label label-info tf-product-total">{{ filter['values']['in_stock']['total'] }}</span>*/
/*             {% else %}*/
/*             <span class="label label-info label-danger tf-product-total">{{ filter['values']['in_stock']['total'] }}</span>*/
/*             {% endif %} */
/*           {% endif %}*/
/*         </div>*/
/*         <div class="form-check tf-filter-value custom-radio">*/
/*           <label class="form-check-label">*/
/*             {% if (filter['values']['out_of_stock']['selected']) %} */
/*             <input type="radio" value="0" name="tf_fs" class="form-check-input" checked>*/
/*             {% else %} */
/*             <input type="radio" value="0" name="tf_fs" class="form-check-input" {{ not filter['values']['out_of_stock']['status']?'disabled' }}>*/
/*             {% endif %} */
/*             <span class="checkmark fa"></span>*/
/*             {{ text_out_of_stock }}*/
/*           </label>*/
/*           {% if count_product %}*/
/*             {% if (filter['values']['out_of_stock']['total']) %} */
/*             <span class="label label-info tf-product-total">{{ filter['values']['out_of_stock']['total'] }}</span>*/
/*             {% else %}*/
/*             <span class="label label-info label-danger tf-product-total">{{ filter['values']['out_of_stock']['total'] }}</span>*/
/*             {% endif %} */
/*           {% endif %}*/
/*         </div>*/
/*       </div>*/
/*     </div>*/
/*   </div>*/
/*   {% elseif (filter['type'] == 'rating') %} {# Rating #}*/
/*   <div class="tf-filter-group col-xs-{{ (12/column_xs)|round(0, 'ceil') }} col-sm-{{ (12/column_sm)|round(0, 'ceil') }} col-md-{{ (12/column_md)|round(0, 'ceil') }} col-lg-{{ (12/column_lg)|round(0, 'ceil') }}">*/
/*     <div class="tf-filter-group-header {{ filter['collapse']?'collapsed' }}" data-toggle="collapse" href="#tf-filter-panel-{{ module_class_id }}-{{ key }}">*/
/*       <span class="tf-filter-group-title">{{ text_rating }}</span>*/
/*       {% if reset_group %}*/
/*         {% set total_selected = 0 %}*/
/*         {% for rating in filter['values'] if rating['selected'] %}{% set total_selected = total_selected + 1 %}{% endfor %}*/
/*         <a data-tf-reset="check" data-toggle="tooltip" title="{{ text_reset }}" class="tf-filter-reset{{ not total_selected?' hide' }}"><i class="fa fa-times"></i></a>*/
/*       {% endif %}*/
/*       <i class="fa fa-caret-up toggle-icon"></i>*/
/*     </div>*/
/*     <div id="tf-filter-panel-{{ module_class_id }}-{{ key }}" class="collapse {{ not filter['collapse']?'in':'' }}" >*/
/*       <div class="tf-filter-group-content">*/
/*         {% for rating in filter['values'] %} */
/*           <div class="form-check tf-filter-value custom-radio">*/
/*             <label class="form-check-label">*/
/*               {% if (rating['selected']) %} */
/*               <input type="radio" value="{{ rating['rating'] }}" name="tf_fr" class="form-check-input" checked>*/
/*               {% else %} */
/*               <input type="radio" value="{{ rating['rating'] }}" name="tf_fr" class="form-check-input" {{ not rating['status']?'disabled' }}>*/
/*               {% endif %}*/
/*               <span class="checkmark fa"></span>*/
/*               <span class="rating">*/
/*                 {% for i in range(1, 5) %} */
/*                   {% if (rating['rating'] < i) %} */
/*                   <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>*/
/*                   {% else %} */
/*                   <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>*/
/*                   {% endif %} */
/*                 {% endfor %} */
/*               </span>*/
/*               {{ text_and_up }}*/
/*             </label>*/
/*             {% if count_product %}*/
/*               {% if (rating['total']) %} */
/*               <span class="label label-info tf-product-total">{{ rating['total'] }}</span>*/
/*               {% else %}*/
/*               <span class="label label-info label-danger tf-product-total">{{ rating['total'] }}</span>*/
/*               {% endif %} */
/*             {% endif %}*/
/*           </div>*/
/*         {% endfor %} */
/*       </div>*/
/*     </div>*/
/*   </div>*/
/*   {% elseif (filter['type'] == 'discount') %} {# Discount #}*/
/*   <div class="tf-filter-group col-xs-{{ (12/column_xs)|round(0, 'ceil') }} col-sm-{{ (12/column_sm)|round(0, 'ceil') }} col-md-{{ (12/column_md)|round(0, 'ceil') }} col-lg-{{ (12/column_lg)|round(0, 'ceil') }}">*/
/*     <div class="tf-filter-group-header {{ filter['collapse']?'collapsed' }}" data-toggle="collapse" href="#tf-filter-panel-{{ module_class_id }}-{{ key }}">*/
/*       <span class="tf-filter-group-title">{{ text_discount }}</span>*/
/*       {% if reset_group %}*/
/*         {% set total_selected = 0 %}*/
/*         {% for discount in filter['values'] if discount['selected'] %}{% set total_selected = total_selected + 1 %}{% endfor %}*/
/*         <a data-tf-reset="check" data-toggle="tooltip" title="{{ text_reset }}" class="tf-filter-reset{{ not total_selected?' hide' }}"><i class="fa fa-times"></i></a>*/
/*       {% endif %}*/
/*       <i class="fa fa-caret-up toggle-icon"></i>*/
/*     </div>*/
/*     <div id="tf-filter-panel-{{ module_class_id }}-{{ key }}" class="collapse {{ not filter['collapse']?'in':'' }}" >*/
/*       <div class="tf-filter-group-content">*/
/*         {% for discount in filter['values'] %} */
/*           <div class="form-check tf-filter-value custom-radio">*/
/*             <label class="form-check-label">*/
/*               {% if (discount['selected']) %} */
/*               <input type="radio" value="{{ discount['value'] }}" name="tf_fd" class="form-check-input" checked>*/
/*               {% else %} */
/*               <input type="radio" value="{{ discount['value'] }}" name="tf_fd" class="form-check-input" {{ not discount['status']?'disabled' }}>*/
/*               {% endif %}*/
/*               <span class="checkmark fa"></span>*/
/*               {{ discount['name'] }}*/
/*             </label>*/
/*             {% if count_product %}*/
/*               {% if (discount['total']) %} */
/*               <span class="label label-info tf-product-total">{{ discount['total'] }}</span>*/
/*               {% else %}*/
/*               <span class="label label-info label-danger tf-product-total">{{ discount['total'] }}</span>*/
/*               {% endif %} */
/*             {% endif %}*/
/*           </div>*/
/*         {% endfor %}*/
/*       </div>*/
/*     </div>*/
/*   </div>*/
/*   {% elseif (filter['type'] == 'filter') %} {# Filter #}*/
/*   <div class="tf-filter-group col-xs-{{ (12/column_xs)|round(0, 'ceil') }} col-sm-{{ (12/column_sm)|round(0, 'ceil') }} col-md-{{ (12/column_md)|round(0, 'ceil') }} col-lg-{{ (12/column_lg)|round(0, 'ceil') }}">*/
/*     <div class="tf-filter-group-header {{ filter['collapse']?'collapsed' }}" data-toggle="collapse" href="#tf-filter-panel-{{ module_class_id }}-{{ key }}">*/
/*       <span  class="tf-filter-group-title">{{ filter['name'] }}</span>*/
/*       {% if reset_group %}*/
/*         {% set total_selected = 0 %}*/
/*         {% for value in filter['values'] if value['selected'] %}{% set total_selected = total_selected + 1 %}{% endfor %}*/
/*         <a data-tf-reset="check" data-toggle="tooltip" title="{{ text_reset }}" class=" tf-filter-reset{{ not total_selected?' hide' }}"><i class="fa fa-times"></i></a>*/
/*       {% endif %}*/
/*       <i class="fa fa-caret-up toggle-icon"></i>*/
/*     </div>*/
/*     <div id="tf-filter-panel-{{ module_class_id }}-{{ key }}" class="collapse {{ not filter['collapse']?'in':'' }}" >*/
/*       {% if filter['search'] %}*/
/*       <div class="tf-filter-group-search"><i class="fa fa-search"></i> <input type="search" placeholder="{{ text_search }}"/></div>*/
/*       {% endif %}*/
/*       <div class="tf-filter-group-content {{ overflow }}">*/
/*         {% for value in filter['values'] %} */
/*           <div class="form-check tf-filter-value custom-checkbox">*/
/*             <label class="form-check-label">*/
/*               {% if (value['selected']) %} */
/*               <input type="checkbox" name="tf_ff" value="{{ value['filter_id'] }}" class="form-check-input" checked>*/
/*               {% else %} */
/*               <input type="checkbox" name="tf_ff" value="{{ value['filter_id'] }}" class="form-check-input" {{ not value['status']?'disabled' }}>*/
/*               {% endif %}*/
/*               <span class="checkmark fa"></span>*/
/*               {{ value['name'] }}*/
/*             </label>*/
/*             {% if count_product %}*/
/*               {% if (value['total']) %} */
/*               <span class="label label-info tf-product-total">{{ value['total'] }}</span>*/
/*               {% else %}*/
/*               <span class="label label-info label-danger tf-product-total">{{ value['total'] }}</span>*/
/*               {% endif %} */
/*             {% endif %}*/
/*           </div>*/
/*         {% endfor %}*/
/*         {% if overflow == 'more' and filter['values']|length >= 7 %}*/
/*           <a class="tf-see-more btn-link" data-toggle="tf-seemore" data-show="{{ text_see_more }}" data-hide="{{ text_see_less }}" href="#">{{ text_see_more }}</a>*/
/*         {% endif %}*/
/*       </div>*/
/*     </div>*/
/*   </div>*/
/*   {% elseif (filter['type'] == 'custom') %} {# Custom filter #}*/
/*   <div class="tf-filter-group col-xs-{{ (12/column_xs)|round(0, 'ceil') }} col-sm-{{ (12/column_sm)|round(0, 'ceil') }} col-md-{{ (12/column_md)|round(0, 'ceil') }} col-lg-{{ (12/column_lg)|round(0, 'ceil') }}">*/
/*     <div class="tf-filter-group-header {{ filter['collapse']?'collapsed' }}" data-toggle="collapse" href="#tf-filter-panel-{{ module_class_id }}-{{ key }}">*/
/*       <span class="tf-filter-group-title">{{ filter['name'] }}</span>*/
/*       {% if reset_group %}*/
/*         {% set total_selected = 0 %}*/
/*         {% for value in filter['values'] if value['selected'] %}{% set total_selected = total_selected + 1 %}{% endfor %}*/
/*         <a data-tf-reset="check" data-toggle="tooltip" title="{{ text_reset }}" class="tf-filter-reset{{ not total_selected?' hide' }}"><i class="fa fa-times"></i></a>*/
/*       {% endif %}*/
/*       <i class="fa fa-caret-up toggle-icon"></i>*/
/*     </div>*/
/*     <div id="tf-filter-panel-{{ module_class_id }}-{{ key }}" data-custom-filter="{{ filter['filter_id'] }}" class="collapse {{ not filter['collapse']?'in':'' }}" >*/
/*       {% if filter['search'] %}*/
/*       <div class="tf-filter-group-search"><i class="fa fa-search"></i> <input type="search" placeholder="{{ text_search }}"/></div>*/
/*       {% endif %}*/
/*       <div class="tf-filter-group-content {{ overflow }}">*/
/*         {% for value in filter['values'] %}*/
/*         <div class="form-check tf-filter-value custom-{{ filter['input_type'] }} {{ filter['list_type'] }}">*/
/*           <label class="form-check-label">*/
/*             {% if (value['selected']) %} */
/*             <input type="{{ filter['input_type'] }}" name="tf_fc{{ filter['filter_id'] }}" value="{{ value['value_id'] }}" class="form-check-input" checked>*/
/*             {% else %} */
/*             <input type="{{ filter['input_type'] }}" name="tf_fc{{ filter['filter_id'] }}" value="{{ value['value_id'] }}" class="form-check-input" {{ (not value['status'])?'disabled' }}>*/
/*             {% endif %} */
/*             {% if (filter['list_type'] == 'image' or filter['list_type'] == 'both') %} */
/*             <img src="{{ value['image'] }}" title="{{ value['name'] }}" alt="{{ value['name'] }}" />*/
/*             {% else %}*/
/*             <span class="checkmark fa"></span>*/
/*             {% endif %} */
/*             {% if (filter['list_type'] == 'text' or filter['list_type'] == 'both') %} */
/*               {{ value['name'] }}*/
/*             {% endif %}*/
/*           </label>*/
/*           {% if count_product and filter['list_type'] != 'image' %}*/
/*             {% if (value['total']) %} */
/*             <span class="label label-info tf-product-total">{{ value['total'] }}</span>*/
/*             {% else %}*/
/*             <span class="label label-info label-danger tf-product-total">{{ value['total'] }}</span>*/
/*             {% endif %} */
/*           {% endif %}*/
/*         </div>*/
/*        {% endfor %}*/
/*        {% if overflow == 'more' and filter['values']|length >= 7 %}*/
/*         <a class="tf-see-more btn-link" data-toggle="tf-seemore" data-show="{{ text_see_more }}" data-hide="{{ text_see_less }}" href="#">{{ text_see_more }}</a>*/
/*        {% endif %}*/
/*       </div>*/
/*     </div>*/
/*   </div>*/
/*   {% endif %} */
/* {% endfor %}*/
/* </div>*/
/* </div>*/
/* <script>*/
/* $(function(){*/
/*     if(window.innerWidth < 767){ // Collaped all panel in small device*/
/*         $('.tf-filter .collapse.in').collapse("hide");*/
/*     }*/
/*     */
/*     // Filter*/
/*     var paginationContainer = $('#content').children('.row').last();*/
/*     var productContainer = paginationContainer.prev();*/
/*     */
/*     productContainer.css('position', 'relative');*/
/*             */
/*     $('#tf-filter-{{ module_class_id }}').tf_filter({*/
/*         requestURL: "{{ requestURL }}",*/
/*         searchEl: $('.tf-filter-group-search input'),*/
/*         ajax: {{ ajax?'true':'false' }},*/
/*         delay: {{ delay?'true':'false' }},*/
/*         search_in_description: {{ search_in_description?'true':'false' }},*/
/*         countProduct: {{ count_product?'true':'false' }},*/
/*         sortBy: '{{ sort_by }}',*/
/*         onParamChange: function(param){*/
/*             $("#input-limit,#input-sort").find('option').each(function(){*/
/*                 var url = $(this).attr('value');*/
/*                 $(this).attr('value', modifyURLQuery(url, $.extend({}, param, {page: null})));*/
/*             });*/
/*             var currency = $('#form-currency input[name="redirect"]');*/
/*             currency.val(modifyURLQuery(currency.val(), $.extend({}, param, {tf_fp: null, page: null})));*/
/*             */
/*             // Show or hide reset all button*/
/*             if($('.tf-filter-group [data-tf-reset]:not(.hide)').length){*/
/*                 $('[data-tf-reset="all"]').removeClass('hide');*/
/*             } else {*/
/*                 $('[data-tf-reset="all"]').addClass('hide');*/
/*             }*/
/*         },*/
/*         onInputChange: function(e){*/
/*             var filter_group = $(e.target).closest('.tf-filter-group');*/
/*             */
/*             var is_input_selected = false;*/
/*             */
/*             // Hide Reset for Checkbox or radio*/
/*             if(filter_group.find('input[type="checkbox"]:checked,input[type="radio"]:checked').length){*/
/*                 is_input_selected = true;*/
/*             }*/
/*             */
/*             // Hide Reset for price*/
/*             if($(e.target).filter('[name="tf_fp[min]"],[name="tf_fp[max]"]').length){*/
/*                 if($('[name="tf_fp[min]"]').val() !== $('[name="tf_fp[min]"]').attr('min') || $('[name="tf_fp[max]"]').val() !== $('[name="tf_fp[max]"]').attr('max')){*/
/*                     is_input_selected = true;*/
/*                 }*/
/*             }*/
/*             */
/*             // Hide reset for text*/
/*             if($(e.target).filter('[type="text"]').val()){*/
/*                 is_input_selected = true;*/
/*             }*/
/*             */
/*             // Hide or show reset buton*/
/*             if(is_input_selected){*/
/*                 filter_group.find('[data-tf-reset]').removeClass('hide');*/
/*             } else {*/
/*                 filter_group.find('[data-tf-reset]').addClass('hide');*/
/*             }*/
/*         },*/
/*         onReset: function(el_reset){*/
/*             var type = $(el_reset).data('tf-reset');*/
/*             */
/*             // Reset price*/
/*             if(type === 'price' || type === 'all'){*/
/*                 price_slider.slider("values", [parseFloat(price_slider.slider("option", 'min')), parseFloat(price_slider.slider("option", 'max'))]);*/
/*             }*/
/*             */
/*             // Hide reset button*/
/*             if($(el_reset).data('tf-reset') !== 'all'){*/
/*                 $(el_reset).addClass('hide');*/
/*             } else {*/
/*                 $('[data-tf-reset]').addClass('hide');*/
/*             }*/
/*         },*/
/*         onBeforeSend: function(){*/
/*             productContainer.append('<div class="tf-loader"><img src="catalog/view/javascript/maza/loader.gif" /></div>');*/
/*         },*/
/*         onResult: function(json){*/
/*             var content = $(json['content']).find('#content');*/
/*             var products = content.children('.row').last().prev().html();*/
/*             var pagination = content.children('.row').last().html();*/
/*             */
/*             // Add result products to container*/
/*             if(products){*/
/*                 $(productContainer).html(products);*/
/*                 */
/*                 $('#list-view.active').click();*/
/*                 $('#grid-view.active').click();*/
/*             } else {*/
/*                 $(productContainer).html("<div class='col-xs-12 text-center'>{{ text_no_result }}</div>");*/
/*             }*/
/* */
/*             // Add pagination to container*/
/*             if(pagination){*/
/*                 $(paginationContainer).html(pagination);*/
/*             } else {*/
/*                 $(paginationContainer).empty();*/
/*             }*/
/*         }*/
/*     });*/
/*     */
/*     // Price slider*/
/*     var price_slider = $(".tf-filter [data-role='rangeslider']").slider({*/
/*         range: true,*/
/*         min: parseFloat($('[name="tf_fp[min]"]').attr('min')),*/
/*         max: parseFloat($('[name="tf_fp[max]"]').attr('max')),*/
/*         values: [parseFloat($('[name="tf_fp[min]"]').val()), parseFloat($('[name="tf_fp[max]"]').val())],*/
/*         slide: function( event, ui ) {*/
/*             $('[name="tf_fp[min]"]').val(ui.values[0]);*/
/*             $('[name="tf_fp[max]"]').val(ui.values[1]);*/
/*         },*/
/*         change: function( event, ui ) {*/
/*             // Hide Reset for price*/
/*             if($('[name="tf_fp[min]"]').val() !== $('[name="tf_fp[min]"]').attr('min') || $('[name="tf_fp[max]"]').val() !== $('[name="tf_fp[max]"]').attr('max')){*/
/*                 $('[data-tf-reset="price"]').removeClass('hide');*/
/*             } else {*/
/*                 $('[data-tf-reset="price"]').addClass('hide');*/
/*             }*/
/*             */
/*             // Trigger filter change*/
/*             $('#tf-filter-{{ module_class_id }}').change();*/
/*         }*/
/*     });*/
/*     $('[name="tf_fp[min]"]').change(function(){*/
/*         price_slider.slider("values", 0, $(this).val());*/
/*     });*/
/*     $('[name="tf_fp[max]"]').change(function(){*/
/*         price_slider.slider("values", 1, $(this).val());*/
/*     });*/
/*     */
/*     // Show reset all button if filter is selected*/
/*     if($('.tf-filter-group [data-tf-reset]:not(.hide)').length){*/
/*         $('[data-tf-reset="all"]').removeClass('hide');*/
/*     }*/
/*     */
/*     // Fix z-index*/
/*     $('.tf-filter-group .collapse').on('show.bs.collapse', function(){*/
/*         var z_index = Number($('#tf-filter-content-{{ module_class_id }}').data('mz-base-z-index')) + 1;*/
/*         $(this).css('z-index', z_index);*/
/*         $('#tf-filter-content-{{ module_class_id }}').data('mz-base-z-index', z_index);*/
/*     });*/
/* });*/
/* </script>*/
/* <link href="catalog/view/theme/default/stylesheet/tf_filter.css" rel="stylesheet" media="screen" />*/
