<?php

/* so-destino/template/information/contact.twig */
class __TwigTemplate_130ead8b4bc54852d085acfb6eb7f9c133f444954e9992f78f13f27c41434833 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo "
<div class=\"breadcrumbs\">
  <div class=\"container\">
      <ul class=\"breadcrumb\">
        ";
        // line 5
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 6
            echo "        <li><a href=\"";
            echo $this->getAttribute($context["breadcrumb"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array());
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 8
        echo "      </ul>
    </div>
</div>
<div class=\"container\">
  
  <div class=\"row\">";
        // line 13
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "
    ";
        // line 14
        if (((isset($context["column_left"]) ? $context["column_left"] : null) && (isset($context["column_right"]) ? $context["column_right"] : null))) {
            // line 15
            echo "    ";
            $context["class"] = "col-sm-6";
            // line 16
            echo "    ";
        } elseif (((isset($context["column_left"]) ? $context["column_left"] : null) || (isset($context["column_right"]) ? $context["column_right"] : null))) {
            // line 17
            echo "    ";
            $context["class"] = "col-sm-9";
            // line 18
            echo "    ";
        } else {
            // line 19
            echo "    ";
            $context["class"] = "col-sm-12";
            // line 20
            echo "    ";
        }
        // line 21
        echo "\t
    <div id=\"content\" class=\"";
        // line 22
        echo (isset($context["class"]) ? $context["class"] : null);
        echo "\">";
        echo (isset($context["content_top"]) ? $context["content_top"] : null);
        echo "
    \t<div class=\"info-contact row\">
\t\t\t
\t\t\t<div class=\"col-sm-4 col-xs-12\">
\t\t\t\t<div class=\"info-store\">
\t\t          <div class=\"name-store\"><h3>";
        // line 27
        echo (isset($context["store"]) ? $context["store"] : null);
        echo "</h3></div>
\t\t          ";
        // line 28
        if ((isset($context["comment"]) ? $context["comment"] : null)) {
            // line 29
            echo "\t\t            <div class=\"comment\">
\t\t            \t";
            // line 30
            echo (isset($context["comment"]) ? $context["comment"] : null);
            echo "
\t\t            </div>
\t\t          ";
        }
        // line 32
        echo " 
\t\t            <address>
\t\t            <div class=\"address clearfix form-group\"><div class=\"pull-left\"><i class=\"fa fa-home\"></i></div><div class=\"text\">";
        // line 34
        echo (isset($context["address"]) ? $context["address"] : null);
        echo "</div></div>
\t\t            <div class=\"form-group\"><div class=\"pull-left\"><i class=\"fa fa-phone\"></i></div><div class=\"text\">";
        // line 35
        echo (isset($context["telephone"]) ? $context["telephone"] : null);
        echo "</div></div>
\t\t            ";
        // line 36
        if ((isset($context["fax"]) ? $context["fax"] : null)) {
            // line 37
            echo "\t\t            <div class=\"form-group\"><div class=\"pull-left\"><i class=\"fa fa-fax\"></i></div><div class=\"text\"> ";
            echo (isset($context["fax"]) ? $context["fax"] : null);
            echo "</div></div>
\t\t            ";
        }
        // line 38
        echo " 
\t\t            ";
        // line 39
        if ((isset($context["open"]) ? $context["open"] : null)) {
            // line 40
            echo "\t\t            <div class=\"form-group\"><div class=\"pull-left\"><i class=\"fa fa-clock-o\"></i></div><div class=\"text\">";
            echo " ";
            echo (isset($context["open"]) ? $context["open"] : null);
            echo "</div>  </div>
\t\t           ";
        }
        // line 41
        echo " 
\t\t            ";
        // line 42
        if ((isset($context["geocode"]) ? $context["geocode"] : null)) {
            // line 43
            echo "\t\t                    <a href=\"https://maps.google.com/maps?q=<?php echo urlencode(\$geocode); ?>&hl=<?php echo \$geocode_hl; ?>&t=m&z=15\" target=\"_blank\" class=\"btn btn-info hidden\"><i class=\"fa fa-map-marker\"></i> ";
            echo (isset($context["button_map"]) ? $context["button_map"] : null);
            echo "</a>
\t\t                     ";
        }
        // line 44
        echo " 
\t\t            </address>
\t\t            <div class=\"socials\">
\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t<li><a href=\"https://www.facebook.com/flytheme\"><i class=\"fa fa-facebook\"></i></a></li>
\t\t\t\t\t\t\t<li><a href=\"https://twitter.com/Flytheme\"><i class=\"fa fa-twitter\"></i></a></li>       
\t\t\t\t\t\t\t<li><a href=\"https://plus.google.com/u/0/b/102399087761949580069/102399087761949580069/posts\"><i class=\"fa fa-google-plus\"></i></a></li>
\t\t\t\t\t\t\t<li><a href=\"https://www.linkedin.com/in/flytheme\"><i class=\"fa fa-linkedin\"></i></a></li>
\t\t\t\t\t\t\t<li><a href=\"#\"><i class=\"fa fa-tumblr\"></i></a></li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t        </div>
\t\t\t<div class=\"col-sm-8 col-xs-12\">
\t\t\t\t<div class=\"contact-form\">
\t\t\t\t   <form action=\"";
        // line 59
        echo (isset($context["action"]) ? $context["action"] : null);
        echo " \" method=\"post\" enctype=\"multipart/form-data\" class=\"form-horizontal\">
\t\t\t\t\t<fieldset>
\t\t\t\t\t\t<legend><h2>";
        // line 61
        echo (isset($context["text_contact"]) ? $context["text_contact"] : null);
        echo " </h2></legend>
\t                  
\t\t\t\t\t  <div class=\"form-group required\">
\t\t\t\t\t\t<div class=\"col-sm-12\">
\t\t\t\t\t\t  <input type=\"text\" name=\"name\" value=\"";
        // line 65
        echo (isset($context["name"]) ? $context["name"] : null);
        echo "\" id=\"input-name\" class=\"form-control\" placeholder=\"";
        echo (isset($context["entry_name"]) ? $context["entry_name"] : null);
        echo " *\"/>
\t\t\t\t\t\t  ";
        // line 66
        if ((isset($context["error_name"]) ? $context["error_name"] : null)) {
            echo " 
\t\t\t\t\t\t  <div class=\"text-danger\">";
            // line 67
            echo (isset($context["error_name"]) ? $context["error_name"] : null);
            echo " </div>
\t\t\t\t\t\t  ";
        }
        // line 68
        echo " 
\t\t\t\t\t\t</div>
\t\t\t\t\t  </div>
\t\t\t\t\t  <div class=\"form-group required\">
\t\t\t\t\t\t<div class=\"col-sm-12\">
\t\t\t\t\t\t  <input type=\"text\" name=\"email\" value=\"";
        // line 73
        echo (isset($context["email"]) ? $context["email"] : null);
        echo "\" id=\"input-email\" class=\"form-control\" placeholder=\"";
        echo (isset($context["entry_email"]) ? $context["entry_email"] : null);
        echo " *\" />
\t\t\t\t\t\t  ";
        // line 74
        if ((isset($context["error_email"]) ? $context["error_email"] : null)) {
            echo " 
\t\t\t\t\t\t  <div class=\"text-danger\">";
            // line 75
            echo (isset($context["error_email"]) ? $context["error_email"] : null);
            echo " </div>
\t\t\t\t\t\t  ";
        }
        // line 76
        echo " 
\t\t\t\t\t\t</div>
\t\t\t\t\t  </div>
\t\t\t\t\t  <div class=\"form-group required\">
\t\t\t\t\t\t<div class=\"col-sm-12\">
\t\t\t\t\t\t  <textarea name=\"enquiry\" value=\"";
        // line 81
        echo (isset($context["entry_enquiry"]) ? $context["entry_enquiry"] : null);
        echo "\" rows=\"10\" id=\"input-enquiry\" placeholder=\"";
        echo (isset($context["entry_enquiry"]) ? $context["entry_enquiry"] : null);
        echo " *\" class=\"form-control\">";
        echo (isset($context["enquiry"]) ? $context["enquiry"] : null);
        echo "</textarea>
\t\t\t\t\t\t  ";
        // line 82
        if ((isset($context["error_enquiry"]) ? $context["error_enquiry"] : null)) {
            echo " 
\t\t\t\t\t\t  <div class=\"text-danger\">";
            // line 83
            echo (isset($context["error_enquiry"]) ? $context["error_enquiry"] : null);
            echo " </div>
\t\t\t\t\t\t  ";
        }
        // line 84
        echo " 
\t\t\t\t\t\t</div>
\t\t\t\t\t  </div>
\t\t\t\t\t  ";
        // line 87
        echo (isset($context["captcha"]) ? $context["captcha"] : null);
        echo " 
\t\t\t\t\t</fieldset>
\t\t\t\t\t<div class=\"buttons\">
\t\t\t\t\t  <div class=\"pull-left\">
\t\t\t\t\t\t<button class=\"btn btn-info\" type=\"submit\"><span>";
        // line 91
        echo (isset($context["button_submit"]) ? $context["button_submit"] : null);
        echo " </span></button>
\t\t\t\t\t  </div>
\t\t\t\t\t</div>
\t\t\t\t  </form>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
    \t<div class=\"info-store\">
\t\t\t";
        // line 99
        if ((isset($context["image"]) ? $context["image"] : null)) {
            echo " 
\t\t\t<div id=\"map-canvas-img\" class=\"form-group\"><img src=\"";
            // line 100
            echo (isset($context["image"]) ? $context["image"] : null);
            echo " \" alt=\"";
            echo (isset($context["store"]) ? $context["store"] : null);
            echo " \" title=\"";
            echo (isset($context["store"]) ? $context["store"] : null);
            echo " \" /></div>
\t\t 
\t\t\t";
        } else {
            // line 103
            echo "\t\t\t\t<div id=\"map-canvas\"></div>
\t\t\t\t<script src=\"https://maps.googleapis.com/maps/api/js?key=";
            // line 104
            echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "mapkeys"), "method");
            echo "\"></script>
\t\t\t\t<script type=\"text/javascript\">
\t\t\t\t\t
\t\t\t\t\tfunction showLatLgn() {
\t\t\t\t\t\tvar geocoder = new google.maps.Geocoder();
\t\t\t\t\t\tvar latlng = new google.maps.LatLng(";
            // line 109
            echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "mapgeocode"), "method");
            echo ");

\t\t\t\t\t\tgeocoder.geocode({\"latLng\": latlng}, function (results, status) {
\t\t\t\t\t\t
\t\t\t\t\t\t\tif (status == google.maps.GeocoderStatus.OK) {
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\tvar add = results[1].formatted_address; //this is the full address
\t\t\t\t\t\t\t\tvar myOptions = {
\t\t\t\t\t\t\t\t\tzoom: ";
            // line 117
            echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "mapzoom"), "method");
            echo ",
\t\t\t\t\t\t\t\t\tcenter: latlng,
\t\t\t\t\t\t\t\t\tmapTypeId: google.maps.MapTypeId.ROADMAP
\t\t\t\t\t\t\t\t};
\t\t\t\t\t\t\t\tvar map = new google.maps.Map(document.getElementById(\"map-canvas\"), myOptions);
\t\t\t\t\t\t\t\tvar marker = new google.maps.Marker({
\t\t\t\t\t\t\t\t\tmap: map,
\t\t\t\t\t\t\t\t\tposition: latlng
\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t\tmarker.setTitle('Address');
\t\t\t\t\t\t\t\tattachSecretMessage(marker,add);
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\ttry {
\t\t\t\t\t\t\t\t\talert(\"Address not found\");
\t\t\t\t\t\t\t\t} catch (e) {
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}

\t\t\t\t\t\t})
\t\t\t\t\t}
\t\t\t\t\t
\t\t\t\t
\t\t\t\t\tfunction showLocation() {
\t\t\t\t\t\tvar address = \"";
            // line 140
            echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "mapaddress"), "method");
            echo "\";
\t\t\t\t\t\tvar geocoder = new google.maps.Geocoder();
\t\t\t\t\t\tgeocoder.geocode({\"address\": address}, function (results, status) {
\t\t\t\t\t\t\t// If the Geocoding was successful
\t\t\t\t\t\t\tif (status == google.maps.GeocoderStatus.OK) {
\t\t\t\t\t\t\t\tvar myOptions = {
\t\t\t\t\t\t\t\t\tzoom: ";
            // line 146
            echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "mapzoom"), "method");
            echo ",
\t\t\t\t\t\t\t\t\tcenter: results[0].geometry.location,
\t\t\t\t\t\t\t\t\tmapTypeId: google.maps.MapTypeId.ROADMAP
\t\t\t\t\t\t\t\t};
\t\t\t\t\t\t\t\tvar map = new google.maps.Map(document.getElementById(\"map-canvas\"), myOptions);

\t\t\t\t\t\t\t\t// Add a marker at the address.
\t\t\t\t\t\t\t\tvar marker = new google.maps.Marker({
\t\t\t\t\t\t\t\t\tmap: map,
\t\t\t\t\t\t\t\t\tposition: results[0].geometry.location
\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t\tmarker.setTitle('Address');
\t\t\t\t\t\t\t\tattachSecretMessage(marker, address);
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\ttry {
\t\t\t\t\t\t\t\t\talert(address + \" not found\");
\t\t\t\t\t\t\t\t} catch (e) {
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t});
\t\t\t\t\t}
\t\t\t\t\t
\t\t\t\t\t
\t\t\t\t\tfunction attachSecretMessage(marker, message) {
\t\t\t\t\t\tvar infowindow = new google.maps.InfoWindow(
\t\t\t\t\t\t\t{
\t\t\t\t\t\t\t\tcontent: message
\t\t\t\t\t\t\t});
\t\t\t\t\t\tgoogle.maps.event.addListener(marker, 'click', function () {
\t\t\t\t\t\t\tinfowindow.open(marker.get('map'), marker);
\t\t\t\t\t\t});
\t\t\t\t\t}

\t\t\t\t\t
\t\t\t\t\t";
            // line 180
            if (twig_test_empty($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "mapaddress"), "method"))) {
                // line 181
                echo "\t\t\t\t\t\tgoogle.maps.event.addDomListener(window, 'load', showLatLgn);
\t\t\t\t\t";
            } else {
                // line 183
                echo "\t\t\t\t\t\tgoogle.maps.event.addDomListener(window, 'load', showLocation);
\t\t\t\t\t";
            }
            // line 184
            echo " 

\t\t\t\t</script>
\t\t\t 
\t\t\t ";
        }
        // line 188
        echo " 
\t\t</div>
\t\t
\t\t
\t\t
      
\t  
      
      
      ";
        // line 197
        echo (isset($context["content_bottom"]) ? $context["content_bottom"] : null);
        echo "</div>
    ";
        // line 198
        echo (isset($context["column_right"]) ? $context["column_right"] : null);
        echo "</div>
</div>
";
        // line 200
        echo (isset($context["footer"]) ? $context["footer"] : null);
        echo "
";
    }

    public function getTemplateName()
    {
        return "so-destino/template/information/contact.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  401 => 200,  396 => 198,  392 => 197,  381 => 188,  374 => 184,  370 => 183,  366 => 181,  364 => 180,  327 => 146,  318 => 140,  292 => 117,  281 => 109,  273 => 104,  270 => 103,  260 => 100,  256 => 99,  245 => 91,  238 => 87,  233 => 84,  228 => 83,  224 => 82,  216 => 81,  209 => 76,  204 => 75,  200 => 74,  194 => 73,  187 => 68,  182 => 67,  178 => 66,  172 => 65,  165 => 61,  160 => 59,  143 => 44,  137 => 43,  135 => 42,  132 => 41,  125 => 40,  123 => 39,  120 => 38,  114 => 37,  112 => 36,  108 => 35,  104 => 34,  100 => 32,  94 => 30,  91 => 29,  89 => 28,  85 => 27,  75 => 22,  72 => 21,  69 => 20,  66 => 19,  63 => 18,  60 => 17,  57 => 16,  54 => 15,  52 => 14,  48 => 13,  41 => 8,  30 => 6,  26 => 5,  19 => 1,);
    }
}
/* {{ header }}*/
/* <div class="breadcrumbs">*/
/*   <div class="container">*/
/*       <ul class="breadcrumb">*/
/*         {% for breadcrumb in breadcrumbs %}*/
/*         <li><a href="{{ breadcrumb.href }}">{{ breadcrumb.text }}</a></li>*/
/*         {% endfor %}*/
/*       </ul>*/
/*     </div>*/
/* </div>*/
/* <div class="container">*/
/*   */
/*   <div class="row">{{ column_left }}*/
/*     {% if column_left and column_right %}*/
/*     {% set class = 'col-sm-6' %}*/
/*     {% elseif column_left or column_right %}*/
/*     {% set class = 'col-sm-9' %}*/
/*     {% else %}*/
/*     {% set class = 'col-sm-12' %}*/
/*     {% endif %}*/
/* 	*/
/*     <div id="content" class="{{ class }}">{{ content_top }}*/
/*     	<div class="info-contact row">*/
/* 			*/
/* 			<div class="col-sm-4 col-xs-12">*/
/* 				<div class="info-store">*/
/* 		          <div class="name-store"><h3>{{ store }}</h3></div>*/
/* 		          {% if comment %}*/
/* 		            <div class="comment">*/
/* 		            	{{ comment }}*/
/* 		            </div>*/
/* 		          {% endif %} */
/* 		            <address>*/
/* 		            <div class="address clearfix form-group"><div class="pull-left"><i class="fa fa-home"></i></div><div class="text">{{ address }}</div></div>*/
/* 		            <div class="form-group"><div class="pull-left"><i class="fa fa-phone"></i></div><div class="text">{{ telephone }}</div></div>*/
/* 		            {% if fax %}*/
/* 		            <div class="form-group"><div class="pull-left"><i class="fa fa-fax"></i></div><div class="text"> {{ fax }}</div></div>*/
/* 		            {% endif %} */
/* 		            {% if open %}*/
/* 		            <div class="form-group"><div class="pull-left"><i class="fa fa-clock-o"></i></div><div class="text">{# {{ text_open }} #} {{ open }}</div>  </div>*/
/* 		           {% endif %} */
/* 		            {% if geocode %}*/
/* 		                    <a href="https://maps.google.com/maps?q=<?php echo urlencode($geocode); ?>&hl=<?php echo $geocode_hl; ?>&t=m&z=15" target="_blank" class="btn btn-info hidden"><i class="fa fa-map-marker"></i> {{ button_map }}</a>*/
/* 		                     {% endif %} */
/* 		            </address>*/
/* 		            <div class="socials">*/
/* 						<ul>*/
/* 							<li><a href="https://www.facebook.com/flytheme"><i class="fa fa-facebook"></i></a></li>*/
/* 							<li><a href="https://twitter.com/Flytheme"><i class="fa fa-twitter"></i></a></li>       */
/* 							<li><a href="https://plus.google.com/u/0/b/102399087761949580069/102399087761949580069/posts"><i class="fa fa-google-plus"></i></a></li>*/
/* 							<li><a href="https://www.linkedin.com/in/flytheme"><i class="fa fa-linkedin"></i></a></li>*/
/* 							<li><a href="#"><i class="fa fa-tumblr"></i></a></li>*/
/* 						</ul>*/
/* 					</div>*/
/* 				</div>*/
/* 	        </div>*/
/* 			<div class="col-sm-8 col-xs-12">*/
/* 				<div class="contact-form">*/
/* 				   <form action="{{ action }} " method="post" enctype="multipart/form-data" class="form-horizontal">*/
/* 					<fieldset>*/
/* 						<legend><h2>{{ text_contact }} </h2></legend>*/
/* 	                  */
/* 					  <div class="form-group required">*/
/* 						<div class="col-sm-12">*/
/* 						  <input type="text" name="name" value="{{ name }}" id="input-name" class="form-control" placeholder="{{ entry_name }} *"/>*/
/* 						  {% if error_name %} */
/* 						  <div class="text-danger">{{ error_name }} </div>*/
/* 						  {% endif %} */
/* 						</div>*/
/* 					  </div>*/
/* 					  <div class="form-group required">*/
/* 						<div class="col-sm-12">*/
/* 						  <input type="text" name="email" value="{{ email }}" id="input-email" class="form-control" placeholder="{{ entry_email }} *" />*/
/* 						  {% if error_email %} */
/* 						  <div class="text-danger">{{ error_email }} </div>*/
/* 						  {% endif %} */
/* 						</div>*/
/* 					  </div>*/
/* 					  <div class="form-group required">*/
/* 						<div class="col-sm-12">*/
/* 						  <textarea name="enquiry" value="{{ entry_enquiry }}" rows="10" id="input-enquiry" placeholder="{{ entry_enquiry }} *" class="form-control">{{ enquiry }}</textarea>*/
/* 						  {% if error_enquiry %} */
/* 						  <div class="text-danger">{{ error_enquiry }} </div>*/
/* 						  {% endif %} */
/* 						</div>*/
/* 					  </div>*/
/* 					  {{ captcha }} */
/* 					</fieldset>*/
/* 					<div class="buttons">*/
/* 					  <div class="pull-left">*/
/* 						<button class="btn btn-info" type="submit"><span>{{ button_submit }} </span></button>*/
/* 					  </div>*/
/* 					</div>*/
/* 				  </form>*/
/* 				</div>*/
/* 			</div>*/
/* 		</div>*/
/*     	<div class="info-store">*/
/* 			{% if image %} */
/* 			<div id="map-canvas-img" class="form-group"><img src="{{ image }} " alt="{{ store }} " title="{{ store }} " /></div>*/
/* 		 */
/* 			{% else %}*/
/* 				<div id="map-canvas"></div>*/
/* 				<script src="https://maps.googleapis.com/maps/api/js?key={{soconfig.get_settings('mapkeys')}}"></script>*/
/* 				<script type="text/javascript">*/
/* 					*/
/* 					function showLatLgn() {*/
/* 						var geocoder = new google.maps.Geocoder();*/
/* 						var latlng = new google.maps.LatLng({{soconfig.get_settings('mapgeocode')}});*/
/* */
/* 						geocoder.geocode({"latLng": latlng}, function (results, status) {*/
/* 						*/
/* 							if (status == google.maps.GeocoderStatus.OK) {*/
/* 							*/
/* 								var add = results[1].formatted_address; //this is the full address*/
/* 								var myOptions = {*/
/* 									zoom: {{soconfig.get_settings('mapzoom')}},*/
/* 									center: latlng,*/
/* 									mapTypeId: google.maps.MapTypeId.ROADMAP*/
/* 								};*/
/* 								var map = new google.maps.Map(document.getElementById("map-canvas"), myOptions);*/
/* 								var marker = new google.maps.Marker({*/
/* 									map: map,*/
/* 									position: latlng*/
/* 								});*/
/* 								marker.setTitle('Address');*/
/* 								attachSecretMessage(marker,add);*/
/* 							} else {*/
/* 								try {*/
/* 									alert("Address not found");*/
/* 								} catch (e) {*/
/* 								}*/
/* 							}*/
/* */
/* 						})*/
/* 					}*/
/* 					*/
/* 				*/
/* 					function showLocation() {*/
/* 						var address = "{{soconfig.get_settings('mapaddress')}}";*/
/* 						var geocoder = new google.maps.Geocoder();*/
/* 						geocoder.geocode({"address": address}, function (results, status) {*/
/* 							// If the Geocoding was successful*/
/* 							if (status == google.maps.GeocoderStatus.OK) {*/
/* 								var myOptions = {*/
/* 									zoom: {{soconfig.get_settings('mapzoom')}},*/
/* 									center: results[0].geometry.location,*/
/* 									mapTypeId: google.maps.MapTypeId.ROADMAP*/
/* 								};*/
/* 								var map = new google.maps.Map(document.getElementById("map-canvas"), myOptions);*/
/* */
/* 								// Add a marker at the address.*/
/* 								var marker = new google.maps.Marker({*/
/* 									map: map,*/
/* 									position: results[0].geometry.location*/
/* 								});*/
/* 								marker.setTitle('Address');*/
/* 								attachSecretMessage(marker, address);*/
/* 							} else {*/
/* 								try {*/
/* 									alert(address + " not found");*/
/* 								} catch (e) {*/
/* 								}*/
/* 							}*/
/* 						});*/
/* 					}*/
/* 					*/
/* 					*/
/* 					function attachSecretMessage(marker, message) {*/
/* 						var infowindow = new google.maps.InfoWindow(*/
/* 							{*/
/* 								content: message*/
/* 							});*/
/* 						google.maps.event.addListener(marker, 'click', function () {*/
/* 							infowindow.open(marker.get('map'), marker);*/
/* 						});*/
/* 					}*/
/* */
/* 					*/
/* 					{% if soconfig.get_settings('mapaddress') is empty %}*/
/* 						google.maps.event.addDomListener(window, 'load', showLatLgn);*/
/* 					{% else %}*/
/* 						google.maps.event.addDomListener(window, 'load', showLocation);*/
/* 					{% endif %} */
/* */
/* 				</script>*/
/* 			 */
/* 			 {% endif %} */
/* 		</div>*/
/* 		*/
/* 		*/
/* 		*/
/*       */
/* 	  */
/*       */
/*       */
/*       {{ content_bottom }}</div>*/
/*     {{ column_right }}</div>*/
/* </div>*/
/* {{ footer }}*/
/* */
