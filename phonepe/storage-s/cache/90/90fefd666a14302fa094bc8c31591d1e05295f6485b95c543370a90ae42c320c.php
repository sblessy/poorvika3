<?php

/* extension/payment/razorpay.twig */
class __TwigTemplate_7cdc0c017621d0d49171789acee9ab096afa71bc86ed22d2c4944e249a00c345 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\">
        <button type=\"submit\" form=\"form-razorpay\" data-toggle=\"tooltip\" title=\"";
        // line 6
        echo (isset($context["button_save"]) ? $context["button_save"] : null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i></button>
        <a href=\"";
        // line 7
        echo (isset($context["cancel"]) ? $context["cancel"] : null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo (isset($context["button_cancel"]) ? $context["button_cancel"] : null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
      <h1>";
        // line 8
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
      <ul class=\"breadcrumb\">
        ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 11
            echo "        <li><a href=\"";
            echo $this->getAttribute($context["breadcrumb"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array());
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "      </ul>
    </div>
  </div>
  <div class=\"container-fluid\">
    ";
        // line 17
        if ((isset($context["error_warning"]) ? $context["error_warning"] : null)) {
            // line 18
            echo "    <div class=\"alert alert-danger\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo (isset($context["error_warning"]) ? $context["error_warning"] : null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 22
        echo "    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <h3 class=\"panel-title\"><i class=\"fa fa-pencil\"></i> ";
        // line 24
        echo (isset($context["text_edit"]) ? $context["text_edit"] : null);
        echo "</h3>
      </div>
      <div class=\"panel-body\">
        <form action=\"";
        // line 27
        echo (isset($context["action"]) ? $context["action"] : null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-razorpay\" class=\"form-horizontal\">
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-key-id\"><span data-toggle=\"tooltip\" title=\"";
        // line 29
        echo (isset($context["help_key_id"]) ? $context["help_key_id"] : null);
        echo "\">";
        echo (isset($context["entry_key_id"]) ? $context["entry_key_id"] : null);
        echo "</span></label>
            <div class=\"col-sm-10\">
              <input type=\"text\" name=\"payment_razorpay_key_id\" value=\"";
        // line 31
        echo (isset($context["razorpay_key_id"]) ? $context["razorpay_key_id"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_key_id"]) ? $context["entry_key_id"] : null);
        echo "\" id=\"input-key-id\" class=\"form-control\" />
              ";
        // line 32
        if ((isset($context["error_key_id"]) ? $context["error_key_id"] : null)) {
            // line 33
            echo "              <div class=\"text-danger\">";
            echo (isset($context["error_key_id"]) ? $context["error_key_id"] : null);
            echo "</div>
              ";
        }
        // line 35
        echo "            </div>
          </div>
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-key-secret\">";
        // line 38
        echo (isset($context["entry_key_secret"]) ? $context["entry_key_secret"] : null);
        echo "</label>
            <div class=\"col-sm-10\">
              <input type=\"text\" name=\"payment_razorpay_key_secret\" value=\"";
        // line 40
        echo (isset($context["razorpay_key_secret"]) ? $context["razorpay_key_secret"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_key_secret"]) ? $context["entry_key_secret"] : null);
        echo "\" id=\"input-key-secret\" class=\"form-control\" />
              ";
        // line 41
        if ((isset($context["error_key_secret"]) ? $context["error_key_secret"] : null)) {
            // line 42
            echo "              <div class=\"text-danger\">";
            echo (isset($context["error_key_secret"]) ? $context["error_key_secret"] : null);
            echo "</div>
              ";
        }
        // line 44
        echo "            </div>
          </div>
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-order-status\"><span data-toggle=\"tooltip\" title=\"";
        // line 47
        echo (isset($context["help_order_status"]) ? $context["help_order_status"] : null);
        echo "\">";
        echo (isset($context["entry_order_status"]) ? $context["entry_order_status"] : null);
        echo "</span></label>
            <div class=\"col-sm-10\">
              <select name=\"payment_razorpay_order_status_id\" id=\"input-order-status\" class=\"form-control\">
                ";
        // line 50
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["order_statuses"]) ? $context["order_statuses"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["order_status"]) {
            // line 51
            echo "                  ";
            if ((((isset($context["razorpay_order_status_id"]) ? $context["razorpay_order_status_id"] : null) && ($this->getAttribute($context["order_status"], "order_status_id", array()) == (isset($context["razorpay_order_status_id"]) ? $context["razorpay_order_status_id"] : null))) || ($this->getAttribute($context["order_status"], "order_status_id", array()) == 2))) {
                // line 52
                echo "                    <option value=\"";
                echo $this->getAttribute($context["order_status"], "order_status_id", array());
                echo "\" selected=\"selected\">";
                echo $this->getAttribute($context["order_status"], "name", array());
                echo "</option>
                  ";
            } else {
                // line 54
                echo "                    <option value=\"";
                echo $this->getAttribute($context["order_status"], "order_status_id", array());
                echo "\">";
                echo $this->getAttribute($context["order_status"], "name", array());
                echo "</option>
                  ";
            }
            // line 56
            echo "                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['order_status'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 57
        echo "              </select>
            </div>
          </div>
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-status\">";
        // line 61
        echo (isset($context["entry_status"]) ? $context["entry_status"] : null);
        echo "</label>
            <div class=\"col-sm-10\">
              <select name=\"payment_razorpay_status\" id=\"input-status\" class=\"form-control\">
                ";
        // line 64
        if ((isset($context["razorpay_status"]) ? $context["razorpay_status"] : null)) {
            // line 65
            echo "                <option value=\"1\" selected=\"selected\">";
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "</option>
                <option value=\"0\">";
            // line 66
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "</option>
                ";
        } else {
            // line 68
            echo "                <option value=\"1\">";
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "</option>
                <option value=\"0\" selected=\"selected\">";
            // line 69
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "</option>
                ";
        }
        // line 71
        echo "              </select>
            </div>
          </div>
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-sort-order\">";
        // line 75
        echo (isset($context["entry_sort_order"]) ? $context["entry_sort_order"] : null);
        echo "</label>
            <div class=\"col-sm-10\">
              <input type=\"text\" name=\"payment_razorpay_sort_order\" value=\"";
        // line 77
        echo (isset($context["razorpay_sort_order"]) ? $context["razorpay_sort_order"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_sort_order"]) ? $context["entry_sort_order"] : null);
        echo "\" id=\"input-sort-order\" class=\"form-control\" />
            </div>
          </div>

          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-pay-action\">";
        // line 82
        echo (isset($context["entry_payment_action"]) ? $context["entry_payment_action"] : null);
        echo "</label>
            <div class=\"col-sm-10\">
              <select name=\"payment_razorpay_payment_action\" id=\"input-pay-action\" class=\"form-control\">
                ";
        // line 85
        if (( !(isset($context["razorpay_payment_action"]) ? $context["razorpay_payment_action"] : null) || ((isset($context["razorpay_payment_action"]) ? $context["razorpay_payment_action"] : null) == "capture"))) {
            echo " 
                <option value=\"capture\" selected=\"selected\">";
            // line 86
            echo (isset($context["text_capture"]) ? $context["text_capture"] : null);
            echo "</option>
                <option value=\"authorize\">";
            // line 87
            echo (isset($context["text_authorize"]) ? $context["text_authorize"] : null);
            echo "</option>
                ";
        } else {
            // line 89
            echo "                <option value=\"capture\">";
            echo (isset($context["text_capture"]) ? $context["text_capture"] : null);
            echo "</option>
                <option value=\"authorize\" selected=\"selected\">";
            // line 90
            echo (isset($context["text_authorize"]) ? $context["text_authorize"] : null);
            echo "</option>
                ";
        }
        // line 92
        echo "              </select>
            </div>
          </div>

          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-max-capture-delay\"><span data-toggle=\"tooltip\" title=\"";
        // line 97
        echo (isset($context["help_max_delay"]) ? $context["help_max_delay"] : null);
        echo "\">";
        echo (isset($context["entry_max_capture_delay"]) ? $context["entry_max_capture_delay"] : null);
        echo "</span></label>
            <div class=\"col-sm-10\">
              <input type=\"text\" name=\"payment_razorpay_max_capture_delay\" value=\"";
        // line 99
        echo (isset($context["razorpay_max_capture_delay"]) ? $context["razorpay_max_capture_delay"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_max_capture_delay1"]) ? $context["entry_max_capture_delay1"] : null);
        echo "\" id=\"input-max-capture-delay\" class=\"form-control\" />
            </div>
          </div>

          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-webhook-status\">";
        // line 104
        echo (isset($context["entry_webhook_status"]) ? $context["entry_webhook_status"] : null);
        echo "</label>
            <div class=\"col-sm-10\">
              <select name=\"payment_razorpay_webhook_status\" id=\"input-webhook-status\" class=\"form-control\">
                ";
        // line 107
        if ((isset($context["razorpay_webhook_status"]) ? $context["razorpay_webhook_status"] : null)) {
            // line 108
            echo "                <option value=\"1\" selected=\"selected\">";
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "</option>
                <option value=\"0\">";
            // line 109
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "</option>
                ";
        } else {
            // line 111
            echo "                <option value=\"1\">";
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "</option>
                <option value=\"0\" selected=\"selected\">";
            // line 112
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "</option>
                ";
        }
        // line 114
        echo "              </select>
            </div>
          </div>
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-webhook-url\"><span data-toggle=\"tooltip\" title=\"";
        // line 118
        echo (isset($context["help_webhook_url"]) ? $context["help_webhook_url"] : null);
        echo "\">";
        echo (isset($context["entry_webhook_url"]) ? $context["entry_webhook_url"] : null);
        echo "</span></label>
            <div class=\"col-sm-10\">
              <div class=\"input-group\"><span class=\"input-group-addon\"><i class=\"fa fa-link\"></i></span>
                <input type=\"text\" readonly value=\"";
        // line 121
        echo (isset($context["razorpay_webhook_url"]) ? $context["razorpay_webhook_url"] : null);
        echo "\" id=\"input-webhook-url\" class=\"form-control\" />
              </div>
            </div>
          </div>
          <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-webhook-secret\">";
        // line 126
        echo (isset($context["entry_webhook_secret"]) ? $context["entry_webhook_secret"] : null);
        echo "</label>
            <div class=\"col-sm-10\">
              <input type=\"text\" name=\"payment_razorpay_webhook_secret\" value=\"";
        // line 128
        echo (isset($context["razorpay_webhook_secret"]) ? $context["razorpay_webhook_secret"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_webhook_secret"]) ? $context["entry_webhook_secret"] : null);
        echo "\" id=\"input-webhook-secret\" class=\"form-control\" />
              ";
        // line 129
        if ((isset($context["error_webhook_secret"]) ? $context["error_webhook_secret"] : null)) {
            // line 130
            echo "              <div class=\"text-danger\">";
            echo (isset($context["error_webhook_secret"]) ? $context["error_webhook_secret"] : null);
            echo "</div>
              ";
        }
        // line 132
        echo "            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
";
        // line 139
        echo (isset($context["footer"]) ? $context["footer"] : null);
    }

    public function getTemplateName()
    {
        return "extension/payment/razorpay.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  362 => 139,  353 => 132,  347 => 130,  345 => 129,  339 => 128,  334 => 126,  326 => 121,  318 => 118,  312 => 114,  307 => 112,  302 => 111,  297 => 109,  292 => 108,  290 => 107,  284 => 104,  274 => 99,  267 => 97,  260 => 92,  255 => 90,  250 => 89,  245 => 87,  241 => 86,  237 => 85,  231 => 82,  221 => 77,  216 => 75,  210 => 71,  205 => 69,  200 => 68,  195 => 66,  190 => 65,  188 => 64,  182 => 61,  176 => 57,  170 => 56,  162 => 54,  154 => 52,  151 => 51,  147 => 50,  139 => 47,  134 => 44,  128 => 42,  126 => 41,  120 => 40,  115 => 38,  110 => 35,  104 => 33,  102 => 32,  96 => 31,  89 => 29,  84 => 27,  78 => 24,  74 => 22,  66 => 18,  64 => 17,  58 => 13,  47 => 11,  43 => 10,  38 => 8,  32 => 7,  28 => 6,  19 => 1,);
    }
}
/* {{ header }}{{ column_left }}*/
/* <div id="content">*/
/*   <div class="page-header">*/
/*     <div class="container-fluid">*/
/*       <div class="pull-right">*/
/*         <button type="submit" form="form-razorpay" data-toggle="tooltip" title="{{ button_save }}" class="btn btn-primary"><i class="fa fa-save"></i></button>*/
/*         <a href="{{ cancel }}" data-toggle="tooltip" title="{{ button_cancel }}" class="btn btn-default"><i class="fa fa-reply"></i></a></div>*/
/*       <h1>{{ heading_title }}</h1>*/
/*       <ul class="breadcrumb">*/
/*         {% for breadcrumb in breadcrumbs %}*/
/*         <li><a href="{{ breadcrumb.href }}">{{ breadcrumb.text }}</a></li>*/
/*         {% endfor %}*/
/*       </ul>*/
/*     </div>*/
/*   </div>*/
/*   <div class="container-fluid">*/
/*     {% if error_warning %}*/
/*     <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> {{ error_warning }}*/
/*       <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*     </div>*/
/*     {% endif %}*/
/*     <div class="panel panel-default">*/
/*       <div class="panel-heading">*/
/*         <h3 class="panel-title"><i class="fa fa-pencil"></i> {{ text_edit }}</h3>*/
/*       </div>*/
/*       <div class="panel-body">*/
/*         <form action="{{ action }}" method="post" enctype="multipart/form-data" id="form-razorpay" class="form-horizontal">*/
/*           <div class="form-group">*/
/*             <label class="col-sm-2 control-label" for="input-key-id"><span data-toggle="tooltip" title="{{ help_key_id }}">{{ entry_key_id }}</span></label>*/
/*             <div class="col-sm-10">*/
/*               <input type="text" name="payment_razorpay_key_id" value="{{ razorpay_key_id }}" placeholder="{{ entry_key_id }}" id="input-key-id" class="form-control" />*/
/*               {% if error_key_id %}*/
/*               <div class="text-danger">{{ error_key_id }}</div>*/
/*               {% endif %}*/
/*             </div>*/
/*           </div>*/
/*           <div class="form-group">*/
/*             <label class="col-sm-2 control-label" for="input-key-secret">{{ entry_key_secret }}</label>*/
/*             <div class="col-sm-10">*/
/*               <input type="text" name="payment_razorpay_key_secret" value="{{ razorpay_key_secret }}" placeholder="{{ entry_key_secret }}" id="input-key-secret" class="form-control" />*/
/*               {% if error_key_secret %}*/
/*               <div class="text-danger">{{ error_key_secret }}</div>*/
/*               {% endif %}*/
/*             </div>*/
/*           </div>*/
/*           <div class="form-group">*/
/*             <label class="col-sm-2 control-label" for="input-order-status"><span data-toggle="tooltip" title="{{ help_order_status }}">{{ entry_order_status }}</span></label>*/
/*             <div class="col-sm-10">*/
/*               <select name="payment_razorpay_order_status_id" id="input-order-status" class="form-control">*/
/*                 {% for order_status in order_statuses %}*/
/*                   {% if ((razorpay_order_status_id and order_status.order_status_id == razorpay_order_status_id) or order_status.order_status_id == 2) %}*/
/*                     <option value="{{ order_status.order_status_id }}" selected="selected">{{ order_status.name }}</option>*/
/*                   {% else %}*/
/*                     <option value="{{ order_status.order_status_id }}">{{ order_status.name }}</option>*/
/*                   {% endif %}*/
/*                 {% endfor %}*/
/*               </select>*/
/*             </div>*/
/*           </div>*/
/*           <div class="form-group">*/
/*             <label class="col-sm-2 control-label" for="input-status">{{ entry_status }}</label>*/
/*             <div class="col-sm-10">*/
/*               <select name="payment_razorpay_status" id="input-status" class="form-control">*/
/*                 {% if razorpay_status %}*/
/*                 <option value="1" selected="selected">{{ text_enabled }}</option>*/
/*                 <option value="0">{{ text_disabled }}</option>*/
/*                 {% else %}*/
/*                 <option value="1">{{ text_enabled }}</option>*/
/*                 <option value="0" selected="selected">{{ text_disabled }}</option>*/
/*                 {% endif %}*/
/*               </select>*/
/*             </div>*/
/*           </div>*/
/*           <div class="form-group">*/
/*             <label class="col-sm-2 control-label" for="input-sort-order">{{ entry_sort_order }}</label>*/
/*             <div class="col-sm-10">*/
/*               <input type="text" name="payment_razorpay_sort_order" value="{{ razorpay_sort_order }}" placeholder="{{ entry_sort_order }}" id="input-sort-order" class="form-control" />*/
/*             </div>*/
/*           </div>*/
/* */
/*           <div class="form-group">*/
/*             <label class="col-sm-2 control-label" for="input-pay-action">{{ entry_payment_action }}</label>*/
/*             <div class="col-sm-10">*/
/*               <select name="payment_razorpay_payment_action" id="input-pay-action" class="form-control">*/
/*                 {% if not razorpay_payment_action or (razorpay_payment_action == 'capture') %} */
/*                 <option value="capture" selected="selected">{{ text_capture }}</option>*/
/*                 <option value="authorize">{{ text_authorize }}</option>*/
/*                 {% else %}*/
/*                 <option value="capture">{{ text_capture }}</option>*/
/*                 <option value="authorize" selected="selected">{{ text_authorize }}</option>*/
/*                 {% endif %}*/
/*               </select>*/
/*             </div>*/
/*           </div>*/
/* */
/*           <div class="form-group">*/
/*             <label class="col-sm-2 control-label" for="input-max-capture-delay"><span data-toggle="tooltip" title="{{ help_max_delay }}">{{ entry_max_capture_delay }}</span></label>*/
/*             <div class="col-sm-10">*/
/*               <input type="text" name="payment_razorpay_max_capture_delay" value="{{ razorpay_max_capture_delay }}" placeholder="{{ entry_max_capture_delay1 }}" id="input-max-capture-delay" class="form-control" />*/
/*             </div>*/
/*           </div>*/
/* */
/*           <div class="form-group">*/
/*             <label class="col-sm-2 control-label" for="input-webhook-status">{{ entry_webhook_status }}</label>*/
/*             <div class="col-sm-10">*/
/*               <select name="payment_razorpay_webhook_status" id="input-webhook-status" class="form-control">*/
/*                 {% if razorpay_webhook_status %}*/
/*                 <option value="1" selected="selected">{{ text_enabled }}</option>*/
/*                 <option value="0">{{ text_disabled }}</option>*/
/*                 {% else %}*/
/*                 <option value="1">{{ text_enabled }}</option>*/
/*                 <option value="0" selected="selected">{{ text_disabled }}</option>*/
/*                 {% endif %}*/
/*               </select>*/
/*             </div>*/
/*           </div>*/
/*           <div class="form-group">*/
/*             <label class="col-sm-2 control-label" for="input-webhook-url"><span data-toggle="tooltip" title="{{ help_webhook_url }}">{{ entry_webhook_url }}</span></label>*/
/*             <div class="col-sm-10">*/
/*               <div class="input-group"><span class="input-group-addon"><i class="fa fa-link"></i></span>*/
/*                 <input type="text" readonly value="{{ razorpay_webhook_url }}" id="input-webhook-url" class="form-control" />*/
/*               </div>*/
/*             </div>*/
/*           </div>*/
/*           <div class="form-group">*/
/*             <label class="col-sm-2 control-label" for="input-webhook-secret">{{ entry_webhook_secret }}</label>*/
/*             <div class="col-sm-10">*/
/*               <input type="text" name="payment_razorpay_webhook_secret" value="{{ razorpay_webhook_secret }}" placeholder="{{ entry_webhook_secret }}" id="input-webhook-secret" class="form-control" />*/
/*               {% if error_webhook_secret %}*/
/*               <div class="text-danger">{{ error_webhook_secret }}</div>*/
/*               {% endif %}*/
/*             </div>*/
/*           </div>*/
/*         </form>*/
/*       </div>*/
/*     </div>*/
/*   </div>*/
/* </div>*/
/* {{ footer }}*/
