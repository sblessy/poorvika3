<?php

/* so-destino/template/extension/module/so_tools/infocart.twig */
class __TwigTemplate_3e8da78530df78530e6689be4b8f90833dad6245b07c3857499bfb16f611243a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"popup popup-mycart popup-hidden\" id=\"popup-mycart\">
\t<div class=\"popup-screen\">
\t\t<div class=\"popup-position\">
\t\t\t<div class=\"popup-container popup-small\">
\t\t\t\t<div class=\"popup-html\">
\t\t\t\t\t<div class=\"popup-header\">
\t\t\t\t\t\t<span><i class=\"fa fa-shopping-cart\"></i>";
        // line 7
        echo (isset($context["text_shopping_cart"]) ? $context["text_shopping_cart"] : null);
        echo "</span>
\t\t\t\t\t\t<a class=\"popup-close\" data-target=\"popup-close\" data-popup-close=\"#popup-mycart\"><i class=\"fa fa-times\"></i></a>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"popup-content\">
\t\t\t\t\t\t<div class=\"cart-header\">
\t\t\t\t\t\t\t";
        // line 12
        if (((isset($context["products"]) ? $context["products"] : null) || (isset($context["vouchers"]) ? $context["vouchers"] : null))) {
            // line 13
            echo "\t\t\t\t\t\t\t\t<div class=\"notification gray\">
\t\t\t\t\t\t\t\t\t<p>";
            // line 14
            echo (isset($context["text_items_product"]) ? $context["text_items_product"] : null);
            echo "</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<table class=\"table table-striped\">
\t\t\t\t\t\t\t\t\t";
            // line 17
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 18
                echo "\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t  \t\t\t<td class=\"text-left first\">
\t\t\t\t\t\t\t\t  \t\t\t\t";
                // line 20
                if ($this->getAttribute($context["product"], "thumb", array())) {
                    // line 21
                    echo "\t\t\t\t\t\t\t\t    \t\t\t\t<a href=\"";
                    echo $this->getAttribute($context["product"], "href", array());
                    echo "\"><img src=\"";
                    echo $this->getAttribute($context["product"], "thumb", array());
                    echo "\" alt=\"";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "\" title=\"";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "\" class=\"img-thumbnail\" /></a>
\t\t\t\t\t\t\t\t    \t\t\t";
                }
                // line 23
                echo "\t\t\t\t\t\t\t\t    \t\t</td>
\t\t\t\t\t\t\t\t  \t\t\t<td class=\"text-left\">
\t\t\t\t\t\t\t\t  \t\t\t\t<a href=\"";
                // line 25
                echo $this->getAttribute($context["product"], "href", array());
                echo "\">";
                echo $this->getAttribute($context["product"], "name", array());
                echo "</a>
\t\t\t\t\t\t\t\t    \t\t\t";
                // line 26
                if ($this->getAttribute($context["product"], "option", array())) {
                    // line 27
                    echo "\t\t\t\t\t\t\t\t    \t\t\t\t";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["product"], "option", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["option"]) {
                        // line 28
                        echo "\t\t\t\t\t\t\t\t    \t\t\t\t\t<br />
\t\t\t\t\t\t\t\t    \t\t\t\t\t- <small>";
                        // line 29
                        echo $this->getAttribute($context["option"], "name", array());
                        echo " ";
                        echo $this->getAttribute($context["option"], "value", array());
                        echo "</small>
\t\t\t\t\t\t\t\t    \t\t\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 31
                    echo "\t\t\t\t\t\t\t\t    \t\t\t";
                }
                // line 32
                echo "\t\t\t\t\t\t\t\t    \t\t\t";
                if ($this->getAttribute($context["product"], "recurring", array())) {
                    // line 33
                    echo "\t\t\t\t\t\t\t\t    \t\t\t\t<br />
\t\t\t\t\t\t\t\t    \t\t\t\t- <small>";
                    // line 34
                    echo (isset($context["text_recurring"]) ? $context["text_recurring"] : null);
                    echo " ";
                    echo $this->getAttribute($context["product"], "recurring", array());
                    echo "</small>
\t\t\t\t\t\t\t\t    \t\t\t";
                }
                // line 36
                echo "\t\t\t\t\t\t\t\t    \t\t</td>
\t\t\t\t\t\t\t\t  \t\t\t<td class=\"text-right\">x ";
                // line 37
                echo $this->getAttribute($context["product"], "quantity", array());
                echo "</td>
\t\t\t\t\t\t\t\t  \t\t\t<td class=\"text-right total-price\">";
                // line 38
                echo $this->getAttribute($context["product"], "total", array());
                echo "</td>
\t\t\t\t\t\t\t\t  \t\t\t<td class=\"text-right last\"><a href=\"javascript:;\" onclick=\"cart.remove('";
                // line 39
                echo $this->getAttribute($context["product"], "cart_id", array());
                echo "');\" title=\"";
                echo (isset($context["button_remove"]) ? $context["button_remove"] : null);
                echo "\"><i class=\"fa fa-trash\"></i></a></td>
\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 42
            echo "\t\t\t\t\t\t\t\t\t";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["vouchers"]) ? $context["vouchers"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["voucher"]) {
                // line 43
                echo "\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t  \t\t\t<td class=\"text-left first\"></td>
\t\t\t\t\t\t\t\t  \t\t\t<td class=\"text-left\">";
                // line 45
                echo $this->getAttribute($context["voucher"], "description", array());
                echo "</td>
\t\t\t\t\t\t\t\t  \t\t\t<td class=\"text-right\">x&nbsp;1</td>
\t\t\t\t\t\t\t\t  \t\t\t<td class=\"text-right\">";
                // line 47
                echo $this->getAttribute($context["voucher"], "amount", array());
                echo "</td>
\t\t\t\t\t\t\t\t  \t\t\t<td class=\"text-right last\"><a href=\"javascript:;\" onclick=\"voucher.remove('";
                // line 48
                echo $this->getAttribute($context["voucher"], "key", array());
                echo "');\" title=\"";
                echo (isset($context["button_remove"]) ? $context["button_remove"] : null);
                echo "\"><i class=\"fa fa-trash\"></i></a></td>
\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['voucher'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 51
            echo "\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t<div class=\"cart-bottom\">
\t\t\t\t\t\t\t\t\t<table class=\"table table-striped\">
\t\t\t\t\t\t\t\t  \t\t";
            // line 54
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["totals"]) ? $context["totals"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["total"]) {
                // line 55
                echo "\t\t\t\t\t\t\t\t  \t\t\t<tr>
\t\t\t\t\t\t\t\t    \t\t\t<td class=\"text-left\"><strong>";
                // line 56
                echo $this->getAttribute($context["total"], "title", array());
                echo "</strong></td>
\t\t\t\t\t\t\t\t    \t\t\t<td class=\"text-right\">";
                // line 57
                echo $this->getAttribute($context["total"], "text", array());
                echo "</td>
\t\t\t\t\t\t\t\t  \t\t\t</tr>
\t\t\t\t\t\t\t\t  \t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['total'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 60
            echo "\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t\t<p class=\"text-center\">
\t\t\t\t\t\t\t\t\t\t<a href=\"";
            // line 62
            echo (isset($context["cart"]) ? $context["cart"] : null);
            echo "\" class=\"btn btn-view-cart\"><strong>";
            echo (isset($context["text_cart"]) ? $context["text_cart"] : null);
            echo "</strong></a>
\t\t\t\t\t\t\t\t\t\t<a href=\"";
            // line 63
            echo (isset($context["checkout"]) ? $context["checkout"] : null);
            echo "\" class=\"btn btn-checkout\"><strong>";
            echo (isset($context["text_checkout"]) ? $context["text_checkout"] : null);
            echo "</strong></a>
\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t";
        } else {
            // line 67
            echo "\t\t\t\t\t\t\t\t<div class=\"notification gray\">
\t\t\t\t\t\t\t\t\t<i class=\"fa fa-shopping-cart info-icon\"></i>
\t\t\t\t\t\t\t\t\t<p>";
            // line 69
            echo (isset($context["text_empty"]) ? $context["text_empty"] : null);
            echo "</p>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t";
        }
        // line 72
        echo "\t\t\t\t\t\t</div>
\t\t\t\t\t</div>\t\t\t
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
</div>";
    }

    public function getTemplateName()
    {
        return "so-destino/template/extension/module/so_tools/infocart.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  219 => 72,  213 => 69,  209 => 67,  200 => 63,  194 => 62,  190 => 60,  181 => 57,  177 => 56,  174 => 55,  170 => 54,  165 => 51,  154 => 48,  150 => 47,  145 => 45,  141 => 43,  136 => 42,  125 => 39,  121 => 38,  117 => 37,  114 => 36,  107 => 34,  104 => 33,  101 => 32,  98 => 31,  88 => 29,  85 => 28,  80 => 27,  78 => 26,  72 => 25,  68 => 23,  56 => 21,  54 => 20,  50 => 18,  46 => 17,  40 => 14,  37 => 13,  35 => 12,  27 => 7,  19 => 1,);
    }
}
/* <div class="popup popup-mycart popup-hidden" id="popup-mycart">*/
/* 	<div class="popup-screen">*/
/* 		<div class="popup-position">*/
/* 			<div class="popup-container popup-small">*/
/* 				<div class="popup-html">*/
/* 					<div class="popup-header">*/
/* 						<span><i class="fa fa-shopping-cart"></i>{{ text_shopping_cart }}</span>*/
/* 						<a class="popup-close" data-target="popup-close" data-popup-close="#popup-mycart"><i class="fa fa-times"></i></a>*/
/* 					</div>*/
/* 					<div class="popup-content">*/
/* 						<div class="cart-header">*/
/* 							{% if products or vouchers %}*/
/* 								<div class="notification gray">*/
/* 									<p>{{ text_items_product }}</p>*/
/* 								</div>*/
/* 								<table class="table table-striped">*/
/* 									{% for product in products %}*/
/* 										<tr>*/
/* 								  			<td class="text-left first">*/
/* 								  				{% if product.thumb %}*/
/* 								    				<a href="{{ product.href }}"><img src="{{ product.thumb }}" alt="{{ product.name }}" title="{{ product.name }}" class="img-thumbnail" /></a>*/
/* 								    			{% endif %}*/
/* 								    		</td>*/
/* 								  			<td class="text-left">*/
/* 								  				<a href="{{ product.href }}">{{ product.name }}</a>*/
/* 								    			{% if product.option %}*/
/* 								    				{% for option in product.option %}*/
/* 								    					<br />*/
/* 								    					- <small>{{ option.name }} {{ option.value }}</small>*/
/* 								    				{% endfor %}*/
/* 								    			{% endif %}*/
/* 								    			{% if product.recurring %}*/
/* 								    				<br />*/
/* 								    				- <small>{{ text_recurring }} {{ product.recurring }}</small>*/
/* 								    			{% endif %}*/
/* 								    		</td>*/
/* 								  			<td class="text-right">x {{ product.quantity }}</td>*/
/* 								  			<td class="text-right total-price">{{ product.total }}</td>*/
/* 								  			<td class="text-right last"><a href="javascript:;" onclick="cart.remove('{{ product.cart_id }}');" title="{{ button_remove }}"><i class="fa fa-trash"></i></a></td>*/
/* 										</tr>*/
/* 									{% endfor %}*/
/* 									{% for voucher in vouchers %}*/
/* 										<tr>*/
/* 								  			<td class="text-left first"></td>*/
/* 								  			<td class="text-left">{{ voucher.description }}</td>*/
/* 								  			<td class="text-right">x&nbsp;1</td>*/
/* 								  			<td class="text-right">{{ voucher.amount }}</td>*/
/* 								  			<td class="text-right last"><a href="javascript:;" onclick="voucher.remove('{{ voucher.key }}');" title="{{ button_remove }}"><i class="fa fa-trash"></i></a></td>*/
/* 										</tr>*/
/* 									{% endfor %}*/
/* 								</table>*/
/* 								<div class="cart-bottom">*/
/* 									<table class="table table-striped">*/
/* 								  		{% for total in totals %}*/
/* 								  			<tr>*/
/* 								    			<td class="text-left"><strong>{{ total.title }}</strong></td>*/
/* 								    			<td class="text-right">{{ total.text }}</td>*/
/* 								  			</tr>*/
/* 								  		{% endfor %}*/
/* 									</table>*/
/* 									<p class="text-center">*/
/* 										<a href="{{ cart }}" class="btn btn-view-cart"><strong>{{ text_cart }}</strong></a>*/
/* 										<a href="{{ checkout }}" class="btn btn-checkout"><strong>{{ text_checkout }}</strong></a>*/
/* 									</p>*/
/* 								</div>*/
/* 							{% else %}*/
/* 								<div class="notification gray">*/
/* 									<i class="fa fa-shopping-cart info-icon"></i>*/
/* 									<p>{{ text_empty }}</p>*/
/* 								</div>*/
/* 							{% endif %}*/
/* 						</div>*/
/* 					</div>			*/
/* 				</div>*/
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/* </div>*/
