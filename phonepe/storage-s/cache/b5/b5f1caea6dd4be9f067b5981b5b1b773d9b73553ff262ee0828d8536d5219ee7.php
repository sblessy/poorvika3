<?php

/* extension/module/wk_preorder_pro.twig */
class __TwigTemplate_dbcfe987fc5df3039869f896e4437876a0316d0de121de0713574c041be1b603 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo " 
";
        // line 2
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo " 
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\">
        <a href=\"https://www.google.com/recaptcha/intro/v3.html\" target=\"_blank\" class=\"btn btn-info\"><i class=\"fa fa-link\" aria-hidden=\"true\"></i> Create Recaptcha</a>
        <a href=\"http://webkul.com/blog/opencart-preorder-pro-version-2/\" target=\"_blank\" class=\"btn btn-info\"><i class=\"fa fa-book\" aria-hidden=\"true\"></i> User Guide</a>
        <a href=\"https://webkul.uvdesk.com/en/\" target=\"_blank\" class=\"btn btn-primary\"><i class=\"fa fa-ticket\" aria-hidden=\"true\"></i> Support</a>
        <button  onclick=\"\$('#form').submit();\" class=\"btn btn-primary\" title=\"";
        // line 10
        echo (isset($context["button_save"]) ? $context["button_save"] : null);
        echo "\">
          <i class=\"fa fa-save\"></i>
        </button>
        <a href=\"";
        // line 13
        echo (isset($context["cancel"]) ? $context["cancel"] : null);
        echo "\" class=\"btn btn-default\" title=\"";
        echo (isset($context["button_cancel"]) ? $context["button_cancel"] : null);
        echo "\">
          <i class=\"fa fa-reply\"></i>
        </a>
      </div>
      <h1>";
        // line 17
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
      <ul class=\"breadcrumb\">
        ";
        // line 19
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            echo " 
          <li><a href=\"";
            // line 20
            echo $this->getAttribute($context["breadcrumb"], "href", array(), "array");
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array(), "array");
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 21
        echo " 
      </ul>
    </div>
  </div>
  <div class=\"container-fluid\">
    ";
        // line 26
        if ((array_key_exists("error_warning", $context) && (isset($context["error_warning"]) ? $context["error_warning"] : null))) {
            echo " 
      <div class=\"alert alert-danger\">
       ";
            // line 28
            echo (isset($context["error_warning"]) ? $context["error_warning"] : null);
            echo " 
        <button class=\"close\" data-dismiss=\"alert\" type=\"button\">&times;</button>
      </div>
    ";
        }
        // line 31
        echo " 
    <div class=\"alert alert-danger hide\" id=\"percentage_warning\">
      <i class=\"fa fa-exclamation-circle\"></i>
      <button class=\"close\" data-dismiss=\"alert\" type=\"button\">&times;</button>
     ";
        // line 35
        echo (isset($context["percentage_warning"]) ? $context["percentage_warning"] : null);
        echo " 
    </div>
    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <h3 class=\"panel-title\">
           ";
        // line 40
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo " 
        </h3>
      </div>
      <div class=\"panel-body\">

        <form action=\"";
        // line 45
        echo (isset($context["action"]) ? $context["action"] : null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form\" class=\"form-horizontal\">
          <ul class=\"nav nav-tabs\">
            <li class=\"active\"><a href=\"#tab-general\" data-toggle=\"tab\">General</a></li>
            <li><a href=\"#tab-mail\" data-toggle=\"tab\">Mail</a></li>
          </ul>
        <div class=\"tab-content\">
            <div class=\"tab-pane active\" id=\"tab-general\">
              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\">
                   ";
        // line 54
        echo (isset($context["text_status"]) ? $context["text_status"] : null);
        echo " 
                </label>
                <div class=\"col-sm-10\">
                  <select name=\"module_wk_preorder_pro_status\" class=\"form-control\">
                    <option value=\"0\" ";
        // line 58
        if ((array_key_exists("module_wk_preorder_pro_status", $context) &&  !(isset($context["module_wk_preorder_pro_status"]) ? $context["module_wk_preorder_pro_status"] : null))) {
            echo "selected";
        }
        echo ">";
        echo (isset($context["text_disable"]) ? $context["text_disable"] : null);
        echo " </option>
                    <option value=\"1\" ";
        // line 59
        if ((array_key_exists("module_wk_preorder_pro_status", $context) && (isset($context["module_wk_preorder_pro_status"]) ? $context["module_wk_preorder_pro_status"] : null))) {
            echo "selected";
        }
        echo " >";
        echo (isset($context["text_enable"]) ? $context["text_enable"] : null);
        echo " </option>
                  </select>
                  ";
        // line 61
        if ((array_key_exists("error_invalid_status", $context) && (isset($context["error_invalid_status"]) ? $context["error_invalid_status"] : null))) {
            // line 62
            echo "                  <div class = \"text-danger\">";
            echo (isset($context["error_invalid_status"]) ? $context["error_invalid_status"] : null);
            echo "</div>
                  ";
        }
        // line 63
        echo " 
                </div>
              </div>

              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-module_wk_preorder_pro_timezone\">";
        // line 68
        echo (isset($context["text_time_zone"]) ? $context["text_time_zone"] : null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <select name=\"module_wk_preorder_pro_timezone\" id=\"input-module_wk_preorder_pro_timezone\" class=\"form-control\">
                    ";
        // line 71
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["store_timezone"]) ? $context["store_timezone"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["zone"]) {
            echo " 
                      ";
            // line 72
            if (((isset($context["module_wk_preorder_pro_timezone"]) ? $context["module_wk_preorder_pro_timezone"] : null) == $context["key"])) {
                echo " 
                        <option value=\"";
                // line 73
                echo $context["key"];
                echo "\" selected=\"selected\">";
                echo $context["zone"];
                echo "</option>
                      ";
            } else {
                // line 74
                echo " 
                        <option value=\"";
                // line 75
                echo $context["key"];
                echo "\">";
                echo $context["zone"];
                echo "</option>
                      ";
            }
            // line 76
            echo " 
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['zone'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 77
        echo " 
                  </select>
                  ";
        // line 79
        if ((array_key_exists("error_timezone", $context) && (isset($context["error_timezone"]) ? $context["error_timezone"] : null))) {
            echo " 
                    <div class = \"text-danger\">";
            // line 80
            echo (isset($context["error_timezone"]) ? $context["error_timezone"] : null);
            echo "</div>
                  ";
        }
        // line 81
        echo " 
                </div>
              </div>
              <div class=\"form-group required\">
                <label class=\"col-sm-2 control-label\">
                 ";
        // line 86
        echo (isset($context["text_site_key"]) ? $context["text_site_key"] : null);
        echo " 
                </label>
                <div class=\"col-sm-10\">
                  <input type=\"text\" class=\"form-control\" name=\"module_wk_preorder_pro_site_key\" value=\"";
        // line 89
        if (array_key_exists("module_wk_preorder_pro_site_key", $context)) {
            echo (isset($context["module_wk_preorder_pro_site_key"]) ? $context["module_wk_preorder_pro_site_key"] : null);
        }
        echo "\" />
                  ";
        // line 90
        if ((array_key_exists("error_warning_site_key", $context) && (isset($context["error_warning_site_key"]) ? $context["error_warning_site_key"] : null))) {
            echo " 
                    <div class=\"text-danger\">
                       ";
            // line 92
            echo (isset($context["error_warning_site_key"]) ? $context["error_warning_site_key"] : null);
            echo " 
                    </div>
                  ";
        }
        // line 94
        echo " 
                </div>
              </div>
              <div class=\"form-group required\">
                <label class=\"col-sm-2 control-label\">
                 ";
        // line 99
        echo (isset($context["text_secret_key"]) ? $context["text_secret_key"] : null);
        echo " 
                </label>
                <div class=\"col-sm-10\">
                  <input type=\"text\" class=\"form-control\" name=\"module_wk_preorder_pro_secret_key\" value=\"";
        // line 102
        if (array_key_exists("module_wk_preorder_pro_secret_key", $context)) {
            echo (isset($context["module_wk_preorder_pro_secret_key"]) ? $context["module_wk_preorder_pro_secret_key"] : null);
        }
        echo "\" />
                  ";
        // line 103
        if ((array_key_exists("error_warning_secret_key", $context) && (isset($context["error_warning_secret_key"]) ? $context["error_warning_secret_key"] : null))) {
            echo " 
                    <div class=\"text-danger\">
                       ";
            // line 105
            echo (isset($context["error_warning_secret_key"]) ? $context["error_warning_secret_key"] : null);
            echo " 
                    </div>
                  ";
        }
        // line 107
        echo " 
                </div>
              </div>
              <div class=\"form-group\">
              <label class=\"col-sm-2 control-label\">
                <span data-toggle=\"tooltip\" data-original-title=\"";
        // line 112
        echo (isset($context["text_server_key_info"]) ? $context["text_server_key_info"] : null);
        echo "\">
                     ";
        // line 113
        echo (isset($context["text_server_key"]) ? $context["text_server_key"] : null);
        echo " 
                    </span>
                    </label>
                <div class=\"col-sm-10\">
                  <input type=\"text\" class=\"form-control\" name=\"module_wk_preorder_pro_server_key\" value=\"";
        // line 117
        if (array_key_exists("module_wk_preorder_pro_server_key", $context)) {
            echo (isset($context["module_wk_preorder_pro_server_key"]) ? $context["module_wk_preorder_pro_server_key"] : null);
        }
        echo "\" />
                </div>
              </div>
              <div class=\"form-group\">
              <label class=\"col-sm-2 control-label\">
                <span data-toggle=\"tooltip\" data-original-title=\"";
        // line 122
        echo (isset($context["text_messenger_id_info"]) ? $context["text_messenger_id_info"] : null);
        echo "\">
                     ";
        // line 123
        echo (isset($context["text_messenger_id"]) ? $context["text_messenger_id"] : null);
        echo " 
                    </span>
                    </label>
                <div class=\"col-sm-10\">
                  <input type=\"text\" class=\"form-control\" name=\"module_wk_preorder_pro_messenger_id\" value=\"";
        // line 127
        if (array_key_exists("module_wk_preorder_pro_messenger_id", $context)) {
            echo (isset($context["module_wk_preorder_pro_messenger_id"]) ? $context["module_wk_preorder_pro_messenger_id"] : null);
        }
        echo "\" />
                </div>
              </div>
                <div class=\"form-group required\">
                  <label class=\"col-sm-2 control-label\">
                    <span data-toggle=\"tooltip\" data-original-title=\"";
        // line 132
        echo (isset($context["text_preorder_info"]) ? $context["text_preorder_info"] : null);
        echo "\">
                     ";
        // line 133
        echo (isset($context["text_preorder"]) ? $context["text_preorder"] : null);
        echo " 
                    </span>
                  </label>
                  <div class=\"col-sm-10\">
                  ";
        // line 137
        if ((isset($context["languages"]) ? $context["languages"] : null)) {
            echo " 
 ";
            // line 138
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
                echo " 
                     <div class=\"input-group\"><span class=\"input-group-addon\"><img src=\"language/";
                // line 139
                echo $this->getAttribute($context["language"], "code", array(), "array");
                echo "/";
                echo $this->getAttribute($context["language"], "code", array(), "array");
                echo ".png\" title=\"";
                echo $this->getAttribute($context["language"], "name", array(), "array");
                echo "\" /></span>
                      <input type=\"text\" name=\"module_wk_preorder_pro_text[";
                // line 140
                echo $this->getAttribute($context["language"], "language_id", array(), "array");
                echo "]\" value=\"";
                echo (($this->getAttribute((isset($context["module_wk_preorder_pro_text"]) ? $context["module_wk_preorder_pro_text"] : null), $this->getAttribute($context["language"], "language_id", array(), "array"), array(), "array", true, true)) ? ($this->getAttribute((isset($context["module_wk_preorder_pro_text"]) ? $context["module_wk_preorder_pro_text"] : null), $this->getAttribute($context["language"], "language_id", array(), "array"), array(), "array")) : (""));
                echo "\" placeholder=\"";
                echo (isset($context["text_preorder"]) ? $context["text_preorder"] : null);
                echo "\" class=\"form-control\" />
                    </div>
                    ";
                // line 142
                if (($this->getAttribute((isset($context["error_preorder_text"]) ? $context["error_preorder_text"] : null), $this->getAttribute($context["language"], "language_id", array(), "array"), array(), "array", true, true) && $this->getAttribute((isset($context["error_preorder_text"]) ? $context["error_preorder_text"] : null), $this->getAttribute($context["language"], "language_id", array(), "array"), array(), "array"))) {
                    echo " 
                      <div class=\"text-danger\">
                         ";
                    // line 144
                    echo $this->getAttribute((isset($context["error_preorder_text"]) ? $context["error_preorder_text"] : null), $this->getAttribute($context["language"], "language_id", array(), "array"), array(), "array");
                    echo " 
                      </div>
                    ";
                }
                // line 146
                echo " ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo " ";
        }
        echo " 
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\">
                    <span data-toggle=\"tooltip\" data-original-title=\"";
        // line 151
        echo (isset($context["text_preorder_text_help"]) ? $context["text_preorder_text_help"] : null);
        echo "\">
                     ";
        // line 152
        echo (isset($context["text_preorder_text"]) ? $context["text_preorder_text"] : null);
        echo " 
                    </span>
                  </label>
                  <div class=\"col-sm-10\">
                    ";
        // line 156
        if ((isset($context["languages"]) ? $context["languages"] : null)) {
            echo " 
 ";
            // line 157
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
                echo " 

                    <div class=\"input-group\"><span class=\"input-group-addon\"><img src=\"language/";
                // line 159
                echo $this->getAttribute($context["language"], "code", array(), "array");
                echo "/";
                echo $this->getAttribute($context["language"], "code", array(), "array");
                echo ".png\" title=\"";
                echo $this->getAttribute($context["language"], "name", array(), "array");
                echo "\" /></span>
                      <input type=\"text\" name=\"module_wk_preorder_pro_preordertext[";
                // line 160
                echo $this->getAttribute($context["language"], "language_id", array(), "array");
                echo "]\" value=\"";
                echo (($this->getAttribute((isset($context["module_wk_preorder_pro_preordertext"]) ? $context["module_wk_preorder_pro_preordertext"] : null), $this->getAttribute($context["language"], "language_id", array(), "array"), array(), "array", true, true)) ? ($this->getAttribute((isset($context["module_wk_preorder_pro_preordertext"]) ? $context["module_wk_preorder_pro_preordertext"] : null), $this->getAttribute($context["language"], "language_id", array(), "array"), array(), "array")) : (""));
                echo "\" placeholder=\"";
                echo (isset($context["text_preorder"]) ? $context["text_preorder"] : null);
                echo "\" class=\"form-control\" />
                    </div>
                     ";
                // line 162
                if (($this->getAttribute((isset($context["error_preordertext"]) ? $context["error_preordertext"] : null), $this->getAttribute($context["language"], "language_id", array(), "array"), array(), "array", true, true) && $this->getAttribute((isset($context["error_preordertext"]) ? $context["error_preordertext"] : null), $this->getAttribute($context["language"], "language_id", array(), "array"), array(), "array"))) {
                    echo " 
                      <div class=\"text-danger\">
                         ";
                    // line 164
                    echo $this->getAttribute((isset($context["error_preordertext"]) ? $context["error_preordertext"] : null), $this->getAttribute($context["language"], "language_id", array(), "array"), array(), "array");
                    echo " 
                      </div>
                    ";
                }
                // line 166
                echo " ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo " ";
        }
        echo " 
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\">
                    <span data-toggle=\"tooltip\" data-original-title=\"";
        // line 171
        echo (isset($context["text_notification_mode_help"]) ? $context["text_notification_mode_help"] : null);
        echo "\">
                     ";
        // line 172
        echo (isset($context["text_notification_mode"]) ? $context["text_notification_mode"] : null);
        echo " 
                    </span>
                  </label>
                  <div class=\"col-sm-10\">
                    <select name=\"module_wk_preorder_pro_notification_mode\" class=\"form-control\">
                      <option value=\"1\" ";
        // line 177
        if ((array_key_exists("module_wk_preorder_pro_notification_mode", $context) && ((isset($context["module_wk_preorder_pro_notification_mode"]) ? $context["module_wk_preorder_pro_notification_mode"] : null) == "1"))) {
            echo "selected";
        }
        echo " >";
        echo (isset($context["text_auto"]) ? $context["text_auto"] : null);
        echo " </option>
                      <option value=\"2\" ";
        // line 178
        if ((array_key_exists("module_wk_preorder_pro_notification_mode", $context) && ((isset($context["module_wk_preorder_pro_notification_mode"]) ? $context["module_wk_preorder_pro_notification_mode"] : null) == "2"))) {
            echo "selected";
        }
        echo ">";
        echo (isset($context["text_manually"]) ? $context["text_manually"] : null);
        echo " </option>
                      <option value=\"3\" ";
        // line 179
        if ((array_key_exists("module_wk_preorder_pro_notification_mode", $context) && ((isset($context["module_wk_preorder_pro_notification_mode"]) ? $context["module_wk_preorder_pro_notification_mode"] : null) == "3"))) {
            echo "selected";
        }
        echo ">";
        echo (isset($context["text_both"]) ? $context["text_both"] : null);
        echo " </option>
                    </select>
                     ";
        // line 181
        if ((array_key_exists("error_mode", $context) && (isset($context["error_mode"]) ? $context["error_mode"] : null))) {
            echo " 
                    <div class=\"text-danger\">
                       ";
            // line 183
            echo (isset($context["error_mode"]) ? $context["error_mode"] : null);
            echo " 
                    </div>
                  ";
        }
        // line 185
        echo " 
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\">";
        // line 189
        echo (isset($context["text_discount"]) ? $context["text_discount"] : null);
        echo "</label>
                  <div class=\"col-sm-10\">
                    <select name=\"module_wk_preorder_pro_discount\" class=\"form-control\">
                       <option value=\"0\" ";
        // line 192
        if ((array_key_exists("module_wk_preorder_pro_discount", $context) &&  !(isset($context["module_wk_preorder_pro_discount"]) ? $context["module_wk_preorder_pro_discount"] : null))) {
            echo "selected";
        }
        echo ">";
        echo (isset($context["text_disable"]) ? $context["text_disable"] : null);
        echo " </option>
                      <option value=\"1\" ";
        // line 193
        if ((array_key_exists("module_wk_preorder_pro_discount", $context) && (isset($context["module_wk_preorder_pro_discount"]) ? $context["module_wk_preorder_pro_discount"] : null))) {
            echo "selected";
        }
        echo " >";
        echo (isset($context["text_enable"]) ? $context["text_enable"] : null);
        echo " </option>
                    </select>
                    ";
        // line 195
        if ((array_key_exists("error_discount", $context) && (isset($context["error_discount"]) ? $context["error_discount"] : null))) {
            echo " 
                    <div class=\"text-danger\">
                       ";
            // line 197
            echo (isset($context["error_discount"]) ? $context["error_discount"] : null);
            echo " 
                    </div>
                  ";
        }
        // line 199
        echo " 
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\">";
        // line 203
        echo (isset($context["text_discount_type"]) ? $context["text_discount_type"] : null);
        echo "</label>
                  <div class=\"col-sm-10\">
                    <select name=\"module_wk_preorder_pro_discount_type\" class=\"form-control\">
                       <option value=\"0\" ";
        // line 206
        if ((array_key_exists("module_wk_preorder_pro_discount_type", $context) &&  !(isset($context["module_wk_preorder_pro_discount_type"]) ? $context["module_wk_preorder_pro_discount_type"] : null))) {
            echo "selected";
        }
        echo ">";
        echo (isset($context["text_fixed"]) ? $context["text_fixed"] : null);
        echo " </option>
                      <option value=\"1\" ";
        // line 207
        if ((array_key_exists("module_wk_preorder_pro_discount_type", $context) && (isset($context["module_wk_preorder_pro_discount_type"]) ? $context["module_wk_preorder_pro_discount_type"] : null))) {
            echo "selected";
        }
        echo " >";
        echo (isset($context["text_per"]) ? $context["text_per"] : null);
        echo " </option>
                    </select>
                    ";
        // line 209
        if ((array_key_exists("error_discount_type", $context) && (isset($context["error_discount_type"]) ? $context["error_discount_type"] : null))) {
            echo " 
                    <div class=\"text-danger\">
                       ";
            // line 211
            echo (isset($context["error_discount_type"]) ? $context["error_discount_type"] : null);
            echo " 
                    </div>
                  ";
        }
        // line 213
        echo " 
                  </div>
                </div>
                <div class=\"form-group required\">
                  <label class=\"col-sm-2 control-label\">
                    <span data-toggle=\"tooltip\" data-original-title=\"";
        // line 218
        echo (isset($context["help_convert_product"]) ? $context["help_convert_product"] : null);
        echo "\">
                     ";
        // line 219
        echo (isset($context["text_convert_product"]) ? $context["text_convert_product"] : null);
        echo " 
                    </span>
                  </label>
                  <div class=\"col-sm-10\">     
                    <select name=\"module_wk_preorder_pro_convert_preorder\" class=\"form-control\">
                    <option value=\"1\" ";
        // line 224
        if ((array_key_exists("module_wk_preorder_pro_convert_preorder", $context) && ((isset($context["module_wk_preorder_pro_convert_preorder"]) ? $context["module_wk_preorder_pro_convert_preorder"] : null) == "1"))) {
            echo "selected";
        }
        echo ">";
        echo (isset($context["text_manually"]) ? $context["text_manually"] : null);
        echo " </option> 
                      <option value=\"2\" ";
        // line 225
        if ((array_key_exists("module_wk_preorder_pro_convert_preorder", $context) && ((isset($context["module_wk_preorder_pro_convert_preorder"]) ? $context["module_wk_preorder_pro_convert_preorder"] : null) == "2"))) {
            echo "selected";
        }
        echo " >";
        echo (isset($context["text_auto"]) ? $context["text_auto"] : null);
        echo " </option>
                    </select> 
                    ";
        // line 227
        if ((array_key_exists("error_convert", $context) && (isset($context["error_convert"]) ? $context["error_convert"] : null))) {
            echo " 
                    <div class=\"text-danger\">
                       ";
            // line 229
            echo (isset($context["error_convert"]) ? $context["error_convert"] : null);
            echo " 
                    </div>
                  ";
        }
        // line 231
        echo " 
                  </div>
                </div>
                <div class=\"form-group required\">
                  <label class=\"col-sm-2 control-label\">
                    <span data-toggle=\"tooltip\" data-original-title=\"";
        // line 236
        echo (isset($context["help_order_days"]) ? $context["help_order_days"] : null);
        echo "\">
                     ";
        // line 237
        echo (isset($context["text_order_days"]) ? $context["text_order_days"] : null);
        echo " 
                    </span>
                  </label>
                  <div class=\"col-sm-10\">     
                    <input type = \"text\" name = \"module_wk_preorder_pro_preorder_days\" value = \"";
        // line 241
        echo (isset($context["module_wk_preorder_pro_preorder_days"]) ? $context["module_wk_preorder_pro_preorder_days"] : null);
        echo "\" class = \"form-control\" />
                     ";
        // line 242
        if ((array_key_exists("error_invalid_days", $context) && (isset($context["error_invalid_days"]) ? $context["error_invalid_days"] : null))) {
            echo " 
                    <div class=\"text-danger\">
                       ";
            // line 244
            echo (isset($context["error_invalid_days"]) ? $context["error_invalid_days"] : null);
            echo " 
                    </div>
                  ";
        }
        // line 246
        echo " 
                  </div>
                </div>
                <div class=\"form-group required\">
                  <label class=\"col-sm-2 control-label\">
                    <span data-toggle=\"tooltip\" data-original-title=\"";
        // line 251
        echo (isset($context["text_preorder_quantity_info"]) ? $context["text_preorder_quantity_info"] : null);
        echo "\">
                     ";
        // line 252
        echo (isset($context["text_preorder_quantity"]) ? $context["text_preorder_quantity"] : null);
        echo " 
                    </span>
                  </label>
                  <div class=\"col-sm-10\">     
                    <input type = \"text\" name = \"module_wk_preorder_pro_quantity\" value = \"";
        // line 256
        echo (isset($context["module_wk_preorder_pro_quantity"]) ? $context["module_wk_preorder_pro_quantity"] : null);
        echo "\" class = \"form-control\" />
                    ";
        // line 257
        if ((array_key_exists("error_module_wk_preorder_pro_quantity", $context) && (isset($context["error_module_wk_preorder_pro_quantity"]) ? $context["error_module_wk_preorder_pro_quantity"] : null))) {
            echo " 
                    <div class=\"text-danger\">
                       ";
            // line 259
            echo (isset($context["error_module_wk_preorder_pro_quantity"]) ? $context["error_module_wk_preorder_pro_quantity"] : null);
            echo " 
                    </div>
                  ";
        }
        // line 261
        echo " 
                  </div>
                </div>
                <div class=\"form-group required\">
                  <label class=\"col-sm-2 control-label\">
                    <span data-toggle=\"tooltip\" data-original-title=\"";
        // line 266
        echo (isset($context["text_quantity_per_order_info"]) ? $context["text_quantity_per_order_info"] : null);
        echo "\">
                     ";
        // line 267
        echo (isset($context["text_quantity_per_order"]) ? $context["text_quantity_per_order"] : null);
        echo " 
                    </span>
                  </label>
                  <div class=\"col-sm-10\">     
                    <input type = \"text\" name = \"module_wk_preorder_pro_order_quantity\" value = \"";
        // line 271
        echo (isset($context["module_wk_preorder_pro_order_quantity"]) ? $context["module_wk_preorder_pro_order_quantity"] : null);
        echo "\" class = \"form-control\" />
                    ";
        // line 272
        if ((array_key_exists("error_module_wk_preorder_pro_order_quantity", $context) && (isset($context["error_module_wk_preorder_pro_order_quantity"]) ? $context["error_module_wk_preorder_pro_order_quantity"] : null))) {
            echo " 
                    <div class=\"text-danger\">
                       ";
            // line 274
            echo (isset($context["error_module_wk_preorder_pro_order_quantity"]) ? $context["error_module_wk_preorder_pro_order_quantity"] : null);
            echo " 
                    </div>
                  ";
        }
        // line 276
        echo " 
                  </div>
                </div>
                <div class=\"form-group required\">
                  <label class=\"col-sm-2 control-label\">
                    <span data-toggle=\"tooltip\" data-original-title=\"";
        // line 281
        echo (isset($context["text_order_per_customer_info"]) ? $context["text_order_per_customer_info"] : null);
        echo "\">
                     ";
        // line 282
        echo (isset($context["text_order_per_customer"]) ? $context["text_order_per_customer"] : null);
        echo " 
                    </span>
                  </label>
                  <div class=\"col-sm-10\">     
                    <input type = \"text\" name = \"module_wk_preorder_pro_customer_order\" value = \"";
        // line 286
        echo (isset($context["module_wk_preorder_pro_customer_order"]) ? $context["module_wk_preorder_pro_customer_order"] : null);
        echo "\" class = \"form-control\" />
                    ";
        // line 287
        if ((array_key_exists("error_module_wk_preorder_pro_customer_order", $context) && (isset($context["error_module_wk_preorder_pro_customer_order"]) ? $context["error_module_wk_preorder_pro_customer_order"] : null))) {
            echo " 
                    <div class=\"text-danger\">
                       ";
            // line 289
            echo (isset($context["error_module_wk_preorder_pro_customer_order"]) ? $context["error_module_wk_preorder_pro_customer_order"] : null);
            echo " 
                    </div>
                  ";
        }
        // line 291
        echo " 
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\">
                    <span data-toggle=\"tooltip\" data-original-title=\"";
        // line 296
        echo (isset($context["text_product_notification_status_info"]) ? $context["text_product_notification_status_info"] : null);
        echo "\">
                     ";
        // line 297
        echo (isset($context["text_product_notification_status"]) ? $context["text_product_notification_status"] : null);
        echo " 
                    </span>
                  </label>
                  <div class=\"col-sm-10\">
                    <select name=\"module_wk_preorder_pro_notification_status\" class=\"form-control\">
                      <option value=\"\"></option>
                      ";
        // line 303
        if ((isset($context["stock_statuses"]) ? $context["stock_statuses"] : null)) {
            // line 304
            echo " ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["stock_statuses"]) ? $context["stock_statuses"] : null));
            foreach ($context['_seq'] as $context["key"] => $context["value"]) {
                echo " 
                        <option value=\"";
                // line 305
                echo $this->getAttribute($context["value"], "stock_status_id", array(), "array");
                echo "\" ";
                if ((array_key_exists("module_wk_preorder_pro_notification_status", $context) && ((isset($context["module_wk_preorder_pro_notification_status"]) ? $context["module_wk_preorder_pro_notification_status"] : null) == $this->getAttribute($context["value"], "stock_status_id", array(), "array")))) {
                    echo "selected";
                }
                echo " >";
                echo $this->getAttribute($context["value"], "name", array(), "array");
                echo "</option>
                      ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 307
            echo " ";
        }
        echo " 
                    </select>
                     ";
        // line 309
        if ((array_key_exists("error_notification", $context) && (isset($context["error_notification"]) ? $context["error_notification"] : null))) {
            echo " 
                    <div class=\"text-danger\">
                       ";
            // line 311
            echo (isset($context["error_notification"]) ? $context["error_notification"] : null);
            echo " 
                    </div>
                  ";
        }
        // line 313
        echo " 
                  </div>
                </div>
                <div class=\"form-group\">
                  <label class=\"col-sm-2 control-label\">
                    <span data-toggle=\"tooltip\" data-original-title=\"";
        // line 318
        echo (isset($context["text_product_stock_status_info"]) ? $context["text_product_stock_status_info"] : null);
        echo "\">
                     ";
        // line 319
        echo (isset($context["text_product_stock_status"]) ? $context["text_product_stock_status"] : null);
        echo " 
                    </span>
                  </label>
                  <div class=\"col-sm-10\">
                    <select name=\"module_wk_preorder_pro_stock_status\" class=\"form-control\">
                      <option value=\"\"></option>
                      ";
        // line 325
        if ((isset($context["stock_statuses"]) ? $context["stock_statuses"] : null)) {
            // line 326
            echo " ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["stock_statuses"]) ? $context["stock_statuses"] : null));
            foreach ($context['_seq'] as $context["key"] => $context["value"]) {
                echo " 
                        <option value=\"";
                // line 327
                echo $this->getAttribute($context["value"], "stock_status_id", array(), "array");
                echo "\" ";
                if ((array_key_exists("module_wk_preorder_pro_stock_status", $context) && ((isset($context["module_wk_preorder_pro_stock_status"]) ? $context["module_wk_preorder_pro_stock_status"] : null) == $this->getAttribute($context["value"], "stock_status_id", array(), "array")))) {
                    echo "selected";
                }
                echo " >";
                echo $this->getAttribute($context["value"], "name", array(), "array");
                echo "</option>
                      ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 329
            echo " ";
        }
        echo " 
                    </select>
                     ";
        // line 331
        if ((array_key_exists("error_stock", $context) && (isset($context["error_stock"]) ? $context["error_stock"] : null))) {
            echo " 
                    <div class=\"text-danger\">
                       ";
            // line 333
            echo (isset($context["error_stock"]) ? $context["error_stock"] : null);
            echo " 
                    </div>
                  ";
        }
        // line 335
        echo " 
                  </div>
                </div>
                <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-order-status\">";
        // line 339
        echo (isset($context["entry_order_status"]) ? $context["entry_order_status"] : null);
        echo "</label>
                <div class=\"col-sm-10\">
                  <select name=\"module_wk_preorder_pro_order_status\" id=\"input-order-status\" class=\"form-control\">
                    ";
        // line 342
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["order_statuses"]) ? $context["order_statuses"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["order_status"]) {
            echo " 
                    ";
            // line 343
            if (($this->getAttribute($context["order_status"], "order_status_id", array(), "array") == (isset($context["module_wk_preorder_pro_order_status"]) ? $context["module_wk_preorder_pro_order_status"] : null))) {
                echo " 
                    <option value=\"";
                // line 344
                echo $this->getAttribute($context["order_status"], "order_status_id", array(), "array");
                echo "\" selected=\"selected\">";
                echo $this->getAttribute($context["order_status"], "name", array(), "array");
                echo "</option>
                    ";
            } else {
                // line 345
                echo " 
                    <option value=\"";
                // line 346
                echo $this->getAttribute($context["order_status"], "order_status_id", array(), "array");
                echo "\">";
                echo $this->getAttribute($context["order_status"], "name", array(), "array");
                echo "</option>
                    ";
            }
            // line 347
            echo " 
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['order_status'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 348
        echo " 
                  </select>
                  ";
        // line 350
        if ((array_key_exists("error_order_status", $context) && (isset($context["error_order_status"]) ? $context["error_order_status"] : null))) {
            echo " 
                    <div class=\"text-danger\">
                       ";
            // line 352
            echo (isset($context["error_order_status"]) ? $context["error_order_status"] : null);
            echo " 
                    </div>
                  ";
        }
        // line 354
        echo " 
                </div>
              </div>
            </div>
            <div class=\"tab-pane\" id=\"tab-mail\">
              <div class=\"alert alert-info\"><i class=\"fa fa-info-circle\"></i>";
        // line 359
        echo (isset($context["text_info_mail"]) ? $context["text_info_mail"] : null);
        echo " 
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
              </div>

              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\">
                  <span data-toggle=\"tooltip\" data-original-title=\"";
        // line 365
        echo (isset($context["entry_mail_keywords"]) ? $context["entry_mail_keywords"] : null);
        echo "\" >
                 ";
        // line 366
        echo (isset($context["entry_mail_keywords"]) ? $context["entry_mail_keywords"] : null);
        echo " 
                  </span>
                </label>
                <div class=\"col-sm-10\">
                  <textarea class=\"form-control\" name=\"module_wk_preorder_pro_mail_keywords\" style=\"height:150px\">";
        // line 370
        if ((isset($context["module_wk_preorder_pro_mail_keywords"]) ? $context["module_wk_preorder_pro_mail_keywords"] : null)) {
            echo (isset($context["module_wk_preorder_pro_mail_keywords"]) ? $context["module_wk_preorder_pro_mail_keywords"] : null);
        } else {
            echo " 

{config_logo}
{config_icon}
{config_currency}
{config_name}
{config_owner}
{config_address}
{config_geocode}
{config_email}
{config_telephone}
{product_link}
{customer_name}
{product_name}
{product_image}
{product_model}
{product_quantity}  ";
        }
        // line 386
        echo " 
                    </textarea>
                </div>
              </div>

              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\" for=\"input-mail_preorder_pro_notify_customer\"><span data-toggle=\"tooltip\" title=\"";
        // line 392
        echo (isset($context["entry_notification_mail_to_customer_info"]) ? $context["entry_notification_mail_to_customer_info"] : null);
        echo "\">";
        echo (isset($context["entry_notification_mail_to_customer"]) ? $context["entry_notification_mail_to_customer"] : null);
        echo "</span></label>
                <div class=\"col-sm-10\">
                  <select name=\"module_wk_preorder_pro_mail_notify_customer\" id=\"input-mail_preorder_pro_notify_customer\" class=\"form-control\">
                    <option value=\"\"></option>
                    ";
        // line 396
        if ((isset($context["mails"]) ? $context["mails"] : null)) {
            echo " 
                      ";
            // line 397
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["mails"]) ? $context["mails"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["mail"]) {
                echo " 
                      <option value=\"";
                // line 398
                echo $this->getAttribute($context["mail"], "id", array(), "array");
                echo "\" ";
                if (((isset($context["module_wk_preorder_pro_mail_notify_customer"]) ? $context["module_wk_preorder_pro_mail_notify_customer"] : null) == $this->getAttribute($context["mail"], "id", array(), "array"))) {
                    echo "selected";
                }
                echo ">";
                echo $this->getAttribute($context["mail"], "name", array(), "array");
                echo "</option>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['mail'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        // line 399
        echo " 
                  </select>
                </div>
                ";
        // line 402
        if ((array_key_exists("error_customer_mail", $context) && (isset($context["error_customer_mail"]) ? $context["error_customer_mail"] : null))) {
            // line 403
            echo "                <div class  = \"text-danger\">";
            echo (isset($context["error_customer_mail"]) ? $context["error_customer_mail"] : null);
            echo "</div>
                ";
        }
        // line 404
        echo " 
              </div>

              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\">
                  <span data-toggle=\"tooltip\" data-original-title=\"";
        // line 409
        echo (isset($context["text_preorder_email_help"]) ? $context["text_preorder_email_help"] : null);
        echo "\" >
                   ";
        // line 410
        echo (isset($context["text_preorder_email"]) ? $context["text_preorder_email"] : null);
        echo " 
                  </span>
                </label>
                <div class=\"col-sm-10\">
                  <select name=\"module_wk_preorder_pro_mail_status\" class=\"form-control\">
                    <option value=\"1\" ";
        // line 415
        if ((array_key_exists("module_wk_preorder_pro_mail_status", $context) && (isset($context["module_wk_preorder_pro_mail_status"]) ? $context["module_wk_preorder_pro_mail_status"] : null))) {
            echo "selected";
        }
        echo " >";
        echo (isset($context["text_enable"]) ? $context["text_enable"] : null);
        echo " </option>
                    <option value=\"0\" ";
        // line 416
        if ((array_key_exists("module_wk_preorder_pro_mail_status", $context) &&  !(isset($context["module_wk_preorder_pro_mail_status"]) ? $context["module_wk_preorder_pro_mail_status"] : null))) {
            echo "selected";
        }
        echo ">";
        echo (isset($context["text_disable"]) ? $context["text_disable"] : null);
        echo " </option>
                  </select>
                  ";
        // line 418
        if ((array_key_exists("error_mail", $context) && (isset($context["error_mail"]) ? $context["error_mail"] : null))) {
            // line 419
            echo "                  <div class = \"text-danger\">";
            echo (isset($context["error_mail"]) ? $context["error_mail"] : null);
            echo "</div>
                  ";
        }
        // line 420
        echo " 
                </div>
              </div>

              <div class=\"form-group\">
                <label class=\"col-sm-2 control-label\">
                  <span data-toggle=\"tooltip\" data-original-title=\"";
        // line 426
        echo (isset($context["entry_mail_admin_message_help"]) ? $context["entry_mail_admin_message_help"] : null);
        echo "\" >
                   ";
        // line 427
        echo (isset($context["entry_mail_admin_message"]) ? $context["entry_mail_admin_message"] : null);
        echo " 
                  </span>
                </label>
                <div class=\"col-sm-10\">

                 ";
        // line 432
        if ((isset($context["languages"]) ? $context["languages"] : null)) {
            echo " 
 ";
            // line 433
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
                echo " 

                    <div class=\"input-group\"><span class=\"input-group-addon\"><img src=\"language/";
                // line 435
                echo $this->getAttribute($context["language"], "code", array(), "array");
                echo "/";
                echo $this->getAttribute($context["language"], "code", array(), "array");
                echo ".png\" title=\"";
                echo $this->getAttribute($context["language"], "name", array(), "array");
                echo "\" /></span>
                      <textarea class=\"form-control\" name=\"module_wk_preorder_pro_mail_admin_message[";
                // line 436
                echo $this->getAttribute($context["language"], "language_id", array(), "array");
                echo "]\" style=\"height:110px\">";
                if ($this->getAttribute((isset($context["module_wk_preorder_pro_mail_admin_message"]) ? $context["module_wk_preorder_pro_mail_admin_message"] : null), $this->getAttribute($context["language"], "language_id", array(), "array"), array(), "array", true, true)) {
                    echo $this->getAttribute((isset($context["module_wk_preorder_pro_mail_admin_message"]) ? $context["module_wk_preorder_pro_mail_admin_message"] : null), $this->getAttribute($context["language"], "language_id", array(), "array"), array(), "array");
                }
                echo "</textarea>
                    </div>
                     ";
                // line 438
                if (($this->getAttribute((isset($context["error_admin_msg"]) ? $context["error_admin_msg"] : null), $this->getAttribute($context["language"], "language_id", array(), "array"), array(), "array", true, true) && $this->getAttribute((isset($context["error_admin_msg"]) ? $context["error_admin_msg"] : null), $this->getAttribute($context["language"], "language_id", array(), "array"), array(), "array"))) {
                    // line 439
                    echo "                  <div class = \"text-danger\">";
                    echo $this->getAttribute((isset($context["error_admin_msg"]) ? $context["error_admin_msg"] : null), $this->getAttribute($context["language"], "language_id", array(), "array"), array(), "array");
                    echo "</div>
                  ";
                }
                // line 440
                echo " 
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 441
            echo " ";
        }
        echo " 

                </div>
              </div>

            </div>
        </div>
        </form>
    </div>
  </div>
</div>
</div>
";
        // line 453
        echo (isset($context["footer"]) ? $context["footer"] : null);
        echo " 
";
    }

    public function getTemplateName()
    {
        return "extension/module/wk_preorder_pro.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1166 => 453,  1150 => 441,  1143 => 440,  1137 => 439,  1135 => 438,  1126 => 436,  1118 => 435,  1111 => 433,  1107 => 432,  1099 => 427,  1095 => 426,  1087 => 420,  1081 => 419,  1079 => 418,  1070 => 416,  1062 => 415,  1054 => 410,  1050 => 409,  1043 => 404,  1037 => 403,  1035 => 402,  1030 => 399,  1015 => 398,  1009 => 397,  1005 => 396,  996 => 392,  988 => 386,  966 => 370,  959 => 366,  955 => 365,  946 => 359,  939 => 354,  933 => 352,  928 => 350,  924 => 348,  917 => 347,  910 => 346,  907 => 345,  900 => 344,  896 => 343,  890 => 342,  884 => 339,  878 => 335,  872 => 333,  867 => 331,  861 => 329,  847 => 327,  840 => 326,  838 => 325,  829 => 319,  825 => 318,  818 => 313,  812 => 311,  807 => 309,  801 => 307,  787 => 305,  780 => 304,  778 => 303,  769 => 297,  765 => 296,  758 => 291,  752 => 289,  747 => 287,  743 => 286,  736 => 282,  732 => 281,  725 => 276,  719 => 274,  714 => 272,  710 => 271,  703 => 267,  699 => 266,  692 => 261,  686 => 259,  681 => 257,  677 => 256,  670 => 252,  666 => 251,  659 => 246,  653 => 244,  648 => 242,  644 => 241,  637 => 237,  633 => 236,  626 => 231,  620 => 229,  615 => 227,  606 => 225,  598 => 224,  590 => 219,  586 => 218,  579 => 213,  573 => 211,  568 => 209,  559 => 207,  551 => 206,  545 => 203,  539 => 199,  533 => 197,  528 => 195,  519 => 193,  511 => 192,  505 => 189,  499 => 185,  493 => 183,  488 => 181,  479 => 179,  471 => 178,  463 => 177,  455 => 172,  451 => 171,  437 => 166,  431 => 164,  426 => 162,  417 => 160,  409 => 159,  402 => 157,  398 => 156,  391 => 152,  387 => 151,  373 => 146,  367 => 144,  362 => 142,  353 => 140,  345 => 139,  339 => 138,  335 => 137,  328 => 133,  324 => 132,  314 => 127,  307 => 123,  303 => 122,  293 => 117,  286 => 113,  282 => 112,  275 => 107,  269 => 105,  264 => 103,  258 => 102,  252 => 99,  245 => 94,  239 => 92,  234 => 90,  228 => 89,  222 => 86,  215 => 81,  210 => 80,  206 => 79,  202 => 77,  195 => 76,  188 => 75,  185 => 74,  178 => 73,  174 => 72,  168 => 71,  162 => 68,  155 => 63,  149 => 62,  147 => 61,  138 => 59,  130 => 58,  123 => 54,  111 => 45,  103 => 40,  95 => 35,  89 => 31,  82 => 28,  77 => 26,  70 => 21,  60 => 20,  54 => 19,  49 => 17,  40 => 13,  34 => 10,  23 => 2,  19 => 1,);
    }
}
/* {{ header }} */
/* {{ column_left }} */
/* <div id="content">*/
/*   <div class="page-header">*/
/*     <div class="container-fluid">*/
/*       <div class="pull-right">*/
/*         <a href="https://www.google.com/recaptcha/intro/v3.html" target="_blank" class="btn btn-info"><i class="fa fa-link" aria-hidden="true"></i> Create Recaptcha</a>*/
/*         <a href="http://webkul.com/blog/opencart-preorder-pro-version-2/" target="_blank" class="btn btn-info"><i class="fa fa-book" aria-hidden="true"></i> User Guide</a>*/
/*         <a href="https://webkul.uvdesk.com/en/" target="_blank" class="btn btn-primary"><i class="fa fa-ticket" aria-hidden="true"></i> Support</a>*/
/*         <button  onclick="$('#form').submit();" class="btn btn-primary" title="{{ button_save }}">*/
/*           <i class="fa fa-save"></i>*/
/*         </button>*/
/*         <a href="{{ cancel }}" class="btn btn-default" title="{{ button_cancel }}">*/
/*           <i class="fa fa-reply"></i>*/
/*         </a>*/
/*       </div>*/
/*       <h1>{{ heading_title }}</h1>*/
/*       <ul class="breadcrumb">*/
/*         {% for breadcrumb in breadcrumbs %} */
/*           <li><a href="{{ breadcrumb['href'] }}">{{ breadcrumb['text'] }}</a></li>*/
/*         {% endfor %} */
/*       </ul>*/
/*     </div>*/
/*   </div>*/
/*   <div class="container-fluid">*/
/*     {% if (error_warning is defined and error_warning) %} */
/*       <div class="alert alert-danger">*/
/*        {{ error_warning }} */
/*         <button class="close" data-dismiss="alert" type="button">&times;</button>*/
/*       </div>*/
/*     {% endif %} */
/*     <div class="alert alert-danger hide" id="percentage_warning">*/
/*       <i class="fa fa-exclamation-circle"></i>*/
/*       <button class="close" data-dismiss="alert" type="button">&times;</button>*/
/*      {{ percentage_warning }} */
/*     </div>*/
/*     <div class="panel panel-default">*/
/*       <div class="panel-heading">*/
/*         <h3 class="panel-title">*/
/*            {{ heading_title }} */
/*         </h3>*/
/*       </div>*/
/*       <div class="panel-body">*/
/* */
/*         <form action="{{ action }}" method="post" enctype="multipart/form-data" id="form" class="form-horizontal">*/
/*           <ul class="nav nav-tabs">*/
/*             <li class="active"><a href="#tab-general" data-toggle="tab">General</a></li>*/
/*             <li><a href="#tab-mail" data-toggle="tab">Mail</a></li>*/
/*           </ul>*/
/*         <div class="tab-content">*/
/*             <div class="tab-pane active" id="tab-general">*/
/*               <div class="form-group">*/
/*                 <label class="col-sm-2 control-label">*/
/*                    {{ text_status }} */
/*                 </label>*/
/*                 <div class="col-sm-10">*/
/*                   <select name="module_wk_preorder_pro_status" class="form-control">*/
/*                     <option value="0" {% if (module_wk_preorder_pro_status is defined and not module_wk_preorder_pro_status) %}{{ 'selected' }}{% endif %}>{{ text_disable }} </option>*/
/*                     <option value="1" {% if (module_wk_preorder_pro_status is defined and module_wk_preorder_pro_status) %}{{ 'selected' }}{% endif %} >{{ text_enable }} </option>*/
/*                   </select>*/
/*                   {% if (error_invalid_status is defined and error_invalid_status) %}*/
/*                   <div class = "text-danger">{{ error_invalid_status }}</div>*/
/*                   {% endif %} */
/*                 </div>*/
/*               </div>*/
/* */
/*               <div class="form-group">*/
/*                 <label class="col-sm-2 control-label" for="input-module_wk_preorder_pro_timezone">{{ text_time_zone }}</label>*/
/*                 <div class="col-sm-10">*/
/*                   <select name="module_wk_preorder_pro_timezone" id="input-module_wk_preorder_pro_timezone" class="form-control">*/
/*                     {% for key,zone in store_timezone %} */
/*                       {% if (module_wk_preorder_pro_timezone == key) %} */
/*                         <option value="{{ key }}" selected="selected">{{ zone }}</option>*/
/*                       {% else %} */
/*                         <option value="{{ key }}">{{ zone }}</option>*/
/*                       {% endif %} */
/*                     {% endfor %} */
/*                   </select>*/
/*                   {% if (error_timezone is defined and error_timezone) %} */
/*                     <div class = "text-danger">{{ error_timezone }}</div>*/
/*                   {% endif %} */
/*                 </div>*/
/*               </div>*/
/*               <div class="form-group required">*/
/*                 <label class="col-sm-2 control-label">*/
/*                  {{ text_site_key }} */
/*                 </label>*/
/*                 <div class="col-sm-10">*/
/*                   <input type="text" class="form-control" name="module_wk_preorder_pro_site_key" value="{% if (module_wk_preorder_pro_site_key is defined) %}{{ module_wk_preorder_pro_site_key }}{% endif %}" />*/
/*                   {% if (error_warning_site_key is defined and error_warning_site_key) %} */
/*                     <div class="text-danger">*/
/*                        {{ error_warning_site_key }} */
/*                     </div>*/
/*                   {% endif %} */
/*                 </div>*/
/*               </div>*/
/*               <div class="form-group required">*/
/*                 <label class="col-sm-2 control-label">*/
/*                  {{ text_secret_key }} */
/*                 </label>*/
/*                 <div class="col-sm-10">*/
/*                   <input type="text" class="form-control" name="module_wk_preorder_pro_secret_key" value="{% if (module_wk_preorder_pro_secret_key is defined) %}{{ module_wk_preorder_pro_secret_key }}{% endif %}" />*/
/*                   {% if (error_warning_secret_key is defined and error_warning_secret_key) %} */
/*                     <div class="text-danger">*/
/*                        {{ error_warning_secret_key }} */
/*                     </div>*/
/*                   {% endif %} */
/*                 </div>*/
/*               </div>*/
/*               <div class="form-group">*/
/*               <label class="col-sm-2 control-label">*/
/*                 <span data-toggle="tooltip" data-original-title="{{ text_server_key_info }}">*/
/*                      {{ text_server_key }} */
/*                     </span>*/
/*                     </label>*/
/*                 <div class="col-sm-10">*/
/*                   <input type="text" class="form-control" name="module_wk_preorder_pro_server_key" value="{% if (module_wk_preorder_pro_server_key is defined) %}{{ module_wk_preorder_pro_server_key }}{% endif %}" />*/
/*                 </div>*/
/*               </div>*/
/*               <div class="form-group">*/
/*               <label class="col-sm-2 control-label">*/
/*                 <span data-toggle="tooltip" data-original-title="{{ text_messenger_id_info }}">*/
/*                      {{ text_messenger_id }} */
/*                     </span>*/
/*                     </label>*/
/*                 <div class="col-sm-10">*/
/*                   <input type="text" class="form-control" name="module_wk_preorder_pro_messenger_id" value="{% if (module_wk_preorder_pro_messenger_id is defined) %}{{ module_wk_preorder_pro_messenger_id }}{% endif %}" />*/
/*                 </div>*/
/*               </div>*/
/*                 <div class="form-group required">*/
/*                   <label class="col-sm-2 control-label">*/
/*                     <span data-toggle="tooltip" data-original-title="{{ text_preorder_info }}">*/
/*                      {{ text_preorder }} */
/*                     </span>*/
/*                   </label>*/
/*                   <div class="col-sm-10">*/
/*                   {% if (languages) %} */
/*  {% for language in languages %} */
/*                      <div class="input-group"><span class="input-group-addon"><img src="language/{{ language['code'] }}/{{ language['code'] }}.png" title="{{ language['name'] }}" /></span>*/
/*                       <input type="text" name="module_wk_preorder_pro_text[{{ language['language_id'] }}]" value="{{ module_wk_preorder_pro_text[language['language_id']] is defined ? module_wk_preorder_pro_text[language['language_id']] : '' }}" placeholder="{{ text_preorder }}" class="form-control" />*/
/*                     </div>*/
/*                     {% if (error_preorder_text[language['language_id']] is defined and error_preorder_text[language['language_id']]) %} */
/*                       <div class="text-danger">*/
/*                          {{ error_preorder_text[language['language_id']] }} */
/*                       </div>*/
/*                     {% endif %} {% endfor %} {% endif %} */
/*                   </div>*/
/*                 </div>*/
/*                 <div class="form-group">*/
/*                   <label class="col-sm-2 control-label">*/
/*                     <span data-toggle="tooltip" data-original-title="{{ text_preorder_text_help }}">*/
/*                      {{ text_preorder_text }} */
/*                     </span>*/
/*                   </label>*/
/*                   <div class="col-sm-10">*/
/*                     {% if (languages) %} */
/*  {% for language in languages %} */
/* */
/*                     <div class="input-group"><span class="input-group-addon"><img src="language/{{ language['code'] }}/{{ language['code'] }}.png" title="{{ language['name'] }}" /></span>*/
/*                       <input type="text" name="module_wk_preorder_pro_preordertext[{{ language['language_id'] }}]" value="{{ module_wk_preorder_pro_preordertext[language['language_id']] is defined ? module_wk_preorder_pro_preordertext[language['language_id']] : '' }}" placeholder="{{ text_preorder }}" class="form-control" />*/
/*                     </div>*/
/*                      {% if (error_preordertext[language['language_id']] is defined and error_preordertext[language['language_id']]) %} */
/*                       <div class="text-danger">*/
/*                          {{ error_preordertext[language['language_id']] }} */
/*                       </div>*/
/*                     {% endif %} {% endfor %} {% endif %} */
/*                   </div>*/
/*                 </div>*/
/*                 <div class="form-group">*/
/*                   <label class="col-sm-2 control-label">*/
/*                     <span data-toggle="tooltip" data-original-title="{{ text_notification_mode_help }}">*/
/*                      {{ text_notification_mode }} */
/*                     </span>*/
/*                   </label>*/
/*                   <div class="col-sm-10">*/
/*                     <select name="module_wk_preorder_pro_notification_mode" class="form-control">*/
/*                       <option value="1" {% if (module_wk_preorder_pro_notification_mode is defined and module_wk_preorder_pro_notification_mode == '1') %}{{ 'selected' }}{% endif %} >{{ text_auto }} </option>*/
/*                       <option value="2" {% if (module_wk_preorder_pro_notification_mode is defined and module_wk_preorder_pro_notification_mode == '2') %}{{ 'selected' }}{% endif %}>{{ text_manually }} </option>*/
/*                       <option value="3" {% if (module_wk_preorder_pro_notification_mode is defined and module_wk_preorder_pro_notification_mode == '3') %}{{ 'selected' }}{% endif %}>{{ text_both }} </option>*/
/*                     </select>*/
/*                      {% if (error_mode is defined and error_mode) %} */
/*                     <div class="text-danger">*/
/*                        {{ error_mode }} */
/*                     </div>*/
/*                   {% endif %} */
/*                   </div>*/
/*                 </div>*/
/*                 <div class="form-group">*/
/*                   <label class="col-sm-2 control-label">{{ text_discount }}</label>*/
/*                   <div class="col-sm-10">*/
/*                     <select name="module_wk_preorder_pro_discount" class="form-control">*/
/*                        <option value="0" {% if (module_wk_preorder_pro_discount is defined and not module_wk_preorder_pro_discount) %}{{ 'selected' }}{% endif %}>{{ text_disable }} </option>*/
/*                       <option value="1" {% if (module_wk_preorder_pro_discount is defined and module_wk_preorder_pro_discount) %}{{ 'selected' }}{% endif %} >{{ text_enable }} </option>*/
/*                     </select>*/
/*                     {% if (error_discount is defined and error_discount) %} */
/*                     <div class="text-danger">*/
/*                        {{ error_discount }} */
/*                     </div>*/
/*                   {% endif %} */
/*                   </div>*/
/*                 </div>*/
/*                 <div class="form-group">*/
/*                   <label class="col-sm-2 control-label">{{ text_discount_type }}</label>*/
/*                   <div class="col-sm-10">*/
/*                     <select name="module_wk_preorder_pro_discount_type" class="form-control">*/
/*                        <option value="0" {% if (module_wk_preorder_pro_discount_type is defined and not module_wk_preorder_pro_discount_type) %}{{ 'selected' }}{% endif %}>{{ text_fixed }} </option>*/
/*                       <option value="1" {% if (module_wk_preorder_pro_discount_type is defined and module_wk_preorder_pro_discount_type) %}{{ 'selected' }}{% endif %} >{{ text_per }} </option>*/
/*                     </select>*/
/*                     {% if (error_discount_type is defined and error_discount_type) %} */
/*                     <div class="text-danger">*/
/*                        {{ error_discount_type }} */
/*                     </div>*/
/*                   {% endif %} */
/*                   </div>*/
/*                 </div>*/
/*                 <div class="form-group required">*/
/*                   <label class="col-sm-2 control-label">*/
/*                     <span data-toggle="tooltip" data-original-title="{{ help_convert_product }}">*/
/*                      {{ text_convert_product }} */
/*                     </span>*/
/*                   </label>*/
/*                   <div class="col-sm-10">     */
/*                     <select name="module_wk_preorder_pro_convert_preorder" class="form-control">*/
/*                     <option value="1" {% if (module_wk_preorder_pro_convert_preorder is defined and module_wk_preorder_pro_convert_preorder == '1') %}{{ 'selected' }}{% endif %}>{{ text_manually }} </option> */
/*                       <option value="2" {% if (module_wk_preorder_pro_convert_preorder is defined and module_wk_preorder_pro_convert_preorder == '2') %}{{ 'selected' }}{% endif %} >{{ text_auto }} </option>*/
/*                     </select> */
/*                     {% if (error_convert is defined and error_convert) %} */
/*                     <div class="text-danger">*/
/*                        {{ error_convert }} */
/*                     </div>*/
/*                   {% endif %} */
/*                   </div>*/
/*                 </div>*/
/*                 <div class="form-group required">*/
/*                   <label class="col-sm-2 control-label">*/
/*                     <span data-toggle="tooltip" data-original-title="{{ help_order_days }}">*/
/*                      {{ text_order_days }} */
/*                     </span>*/
/*                   </label>*/
/*                   <div class="col-sm-10">     */
/*                     <input type = "text" name = "module_wk_preorder_pro_preorder_days" value = "{{ module_wk_preorder_pro_preorder_days }}" class = "form-control" />*/
/*                      {% if (error_invalid_days is defined and error_invalid_days) %} */
/*                     <div class="text-danger">*/
/*                        {{ error_invalid_days }} */
/*                     </div>*/
/*                   {% endif %} */
/*                   </div>*/
/*                 </div>*/
/*                 <div class="form-group required">*/
/*                   <label class="col-sm-2 control-label">*/
/*                     <span data-toggle="tooltip" data-original-title="{{ text_preorder_quantity_info }}">*/
/*                      {{ text_preorder_quantity }} */
/*                     </span>*/
/*                   </label>*/
/*                   <div class="col-sm-10">     */
/*                     <input type = "text" name = "module_wk_preorder_pro_quantity" value = "{{ module_wk_preorder_pro_quantity }}" class = "form-control" />*/
/*                     {% if (error_module_wk_preorder_pro_quantity is defined and error_module_wk_preorder_pro_quantity) %} */
/*                     <div class="text-danger">*/
/*                        {{ error_module_wk_preorder_pro_quantity }} */
/*                     </div>*/
/*                   {% endif %} */
/*                   </div>*/
/*                 </div>*/
/*                 <div class="form-group required">*/
/*                   <label class="col-sm-2 control-label">*/
/*                     <span data-toggle="tooltip" data-original-title="{{ text_quantity_per_order_info }}">*/
/*                      {{ text_quantity_per_order }} */
/*                     </span>*/
/*                   </label>*/
/*                   <div class="col-sm-10">     */
/*                     <input type = "text" name = "module_wk_preorder_pro_order_quantity" value = "{{ module_wk_preorder_pro_order_quantity }}" class = "form-control" />*/
/*                     {% if (error_module_wk_preorder_pro_order_quantity is defined and error_module_wk_preorder_pro_order_quantity) %} */
/*                     <div class="text-danger">*/
/*                        {{ error_module_wk_preorder_pro_order_quantity }} */
/*                     </div>*/
/*                   {% endif %} */
/*                   </div>*/
/*                 </div>*/
/*                 <div class="form-group required">*/
/*                   <label class="col-sm-2 control-label">*/
/*                     <span data-toggle="tooltip" data-original-title="{{ text_order_per_customer_info }}">*/
/*                      {{ text_order_per_customer }} */
/*                     </span>*/
/*                   </label>*/
/*                   <div class="col-sm-10">     */
/*                     <input type = "text" name = "module_wk_preorder_pro_customer_order" value = "{{ module_wk_preorder_pro_customer_order }}" class = "form-control" />*/
/*                     {% if (error_module_wk_preorder_pro_customer_order is defined and error_module_wk_preorder_pro_customer_order) %} */
/*                     <div class="text-danger">*/
/*                        {{ error_module_wk_preorder_pro_customer_order }} */
/*                     </div>*/
/*                   {% endif %} */
/*                   </div>*/
/*                 </div>*/
/*                 <div class="form-group">*/
/*                   <label class="col-sm-2 control-label">*/
/*                     <span data-toggle="tooltip" data-original-title="{{ text_product_notification_status_info }}">*/
/*                      {{ text_product_notification_status }} */
/*                     </span>*/
/*                   </label>*/
/*                   <div class="col-sm-10">*/
/*                     <select name="module_wk_preorder_pro_notification_status" class="form-control">*/
/*                       <option value=""></option>*/
/*                       {% if (stock_statuses) %}*/
/*  {% for key,value in stock_statuses %} */
/*                         <option value="{{ value['stock_status_id'] }}" {% if (module_wk_preorder_pro_notification_status is defined and module_wk_preorder_pro_notification_status == value['stock_status_id']) %}{{ "selected" }}{% endif %} >{{ value['name'] }}</option>*/
/*                       {% endfor %}*/
/*  {% endif %} */
/*                     </select>*/
/*                      {% if (error_notification is defined and error_notification) %} */
/*                     <div class="text-danger">*/
/*                        {{ error_notification }} */
/*                     </div>*/
/*                   {% endif %} */
/*                   </div>*/
/*                 </div>*/
/*                 <div class="form-group">*/
/*                   <label class="col-sm-2 control-label">*/
/*                     <span data-toggle="tooltip" data-original-title="{{ text_product_stock_status_info }}">*/
/*                      {{ text_product_stock_status }} */
/*                     </span>*/
/*                   </label>*/
/*                   <div class="col-sm-10">*/
/*                     <select name="module_wk_preorder_pro_stock_status" class="form-control">*/
/*                       <option value=""></option>*/
/*                       {% if (stock_statuses) %}*/
/*  {% for key,value in stock_statuses %} */
/*                         <option value="{{ value['stock_status_id'] }}" {% if (module_wk_preorder_pro_stock_status is defined and module_wk_preorder_pro_stock_status == value['stock_status_id']) %}{{ "selected" }}{% endif %} >{{ value['name'] }}</option>*/
/*                       {% endfor %}*/
/*  {% endif %} */
/*                     </select>*/
/*                      {% if (error_stock is defined and error_stock) %} */
/*                     <div class="text-danger">*/
/*                        {{ error_stock }} */
/*                     </div>*/
/*                   {% endif %} */
/*                   </div>*/
/*                 </div>*/
/*                 <div class="form-group">*/
/*                 <label class="col-sm-2 control-label" for="input-order-status">{{ entry_order_status }}</label>*/
/*                 <div class="col-sm-10">*/
/*                   <select name="module_wk_preorder_pro_order_status" id="input-order-status" class="form-control">*/
/*                     {% for order_status in order_statuses %} */
/*                     {% if (order_status['order_status_id'] == module_wk_preorder_pro_order_status) %} */
/*                     <option value="{{ order_status['order_status_id'] }}" selected="selected">{{ order_status['name'] }}</option>*/
/*                     {% else %} */
/*                     <option value="{{ order_status['order_status_id'] }}">{{ order_status['name'] }}</option>*/
/*                     {% endif %} */
/*                     {% endfor %} */
/*                   </select>*/
/*                   {% if (error_order_status is defined and error_order_status) %} */
/*                     <div class="text-danger">*/
/*                        {{ error_order_status }} */
/*                     </div>*/
/*                   {% endif %} */
/*                 </div>*/
/*               </div>*/
/*             </div>*/
/*             <div class="tab-pane" id="tab-mail">*/
/*               <div class="alert alert-info"><i class="fa fa-info-circle"></i>{{ text_info_mail }} */
/*                 <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*               </div>*/
/* */
/*               <div class="form-group">*/
/*                 <label class="col-sm-2 control-label">*/
/*                   <span data-toggle="tooltip" data-original-title="{{ entry_mail_keywords }}" >*/
/*                  {{ entry_mail_keywords }} */
/*                   </span>*/
/*                 </label>*/
/*                 <div class="col-sm-10">*/
/*                   <textarea class="form-control" name="module_wk_preorder_pro_mail_keywords" style="height:150px">{% if ( module_wk_preorder_pro_mail_keywords ) %}{{ module_wk_preorder_pro_mail_keywords }}{% else %} */
/* */
/* {config_logo}*/
/* {config_icon}*/
/* {config_currency}*/
/* {config_name}*/
/* {config_owner}*/
/* {config_address}*/
/* {config_geocode}*/
/* {config_email}*/
/* {config_telephone}*/
/* {product_link}*/
/* {customer_name}*/
/* {product_name}*/
/* {product_image}*/
/* {product_model}*/
/* {product_quantity}  {% endif %} */
/*                     </textarea>*/
/*                 </div>*/
/*               </div>*/
/* */
/*               <div class="form-group">*/
/*                 <label class="col-sm-2 control-label" for="input-mail_preorder_pro_notify_customer"><span data-toggle="tooltip" title="{{ entry_notification_mail_to_customer_info }}">{{ entry_notification_mail_to_customer }}</span></label>*/
/*                 <div class="col-sm-10">*/
/*                   <select name="module_wk_preorder_pro_mail_notify_customer" id="input-mail_preorder_pro_notify_customer" class="form-control">*/
/*                     <option value=""></option>*/
/*                     {% if (mails) %} */
/*                       {% for mail in mails %} */
/*                       <option value="{{ mail['id'] }}" {% if ( module_wk_preorder_pro_mail_notify_customer == mail['id']) %}{{ 'selected' }}{% endif %}>{{ mail['name'] }}</option>*/
/*                     {% endfor %}{% endif %} */
/*                   </select>*/
/*                 </div>*/
/*                 {% if (error_customer_mail is defined and error_customer_mail) %}*/
/*                 <div class  = "text-danger">{{ error_customer_mail }}</div>*/
/*                 {% endif %} */
/*               </div>*/
/* */
/*               <div class="form-group">*/
/*                 <label class="col-sm-2 control-label">*/
/*                   <span data-toggle="tooltip" data-original-title="{{ text_preorder_email_help }}" >*/
/*                    {{ text_preorder_email }} */
/*                   </span>*/
/*                 </label>*/
/*                 <div class="col-sm-10">*/
/*                   <select name="module_wk_preorder_pro_mail_status" class="form-control">*/
/*                     <option value="1" {% if (module_wk_preorder_pro_mail_status is defined and module_wk_preorder_pro_mail_status) %}{{ 'selected' }}{% endif %} >{{ text_enable }} </option>*/
/*                     <option value="0" {% if (module_wk_preorder_pro_mail_status is defined and not module_wk_preorder_pro_mail_status) %}{{ 'selected' }}{% endif %}>{{ text_disable }} </option>*/
/*                   </select>*/
/*                   {% if (error_mail is defined and error_mail) %}*/
/*                   <div class = "text-danger">{{ error_mail }}</div>*/
/*                   {% endif %} */
/*                 </div>*/
/*               </div>*/
/* */
/*               <div class="form-group">*/
/*                 <label class="col-sm-2 control-label">*/
/*                   <span data-toggle="tooltip" data-original-title="{{ entry_mail_admin_message_help }}" >*/
/*                    {{ entry_mail_admin_message }} */
/*                   </span>*/
/*                 </label>*/
/*                 <div class="col-sm-10">*/
/* */
/*                  {% if (languages) %} */
/*  {% for language in languages %} */
/* */
/*                     <div class="input-group"><span class="input-group-addon"><img src="language/{{ language['code'] }}/{{ language['code'] }}.png" title="{{ language['name'] }}" /></span>*/
/*                       <textarea class="form-control" name="module_wk_preorder_pro_mail_admin_message[{{ language['language_id'] }}]" style="height:110px">{% if ( module_wk_preorder_pro_mail_admin_message[language['language_id']] is defined) %}{{ module_wk_preorder_pro_mail_admin_message[language['language_id']] }}{% endif %}</textarea>*/
/*                     </div>*/
/*                      {% if (error_admin_msg[language['language_id']] is defined and error_admin_msg[language['language_id']]) %}*/
/*                   <div class = "text-danger">{{ error_admin_msg[language['language_id']] }}</div>*/
/*                   {% endif %} */
/*                     {% endfor %} {% endif %} */
/* */
/*                 </div>*/
/*               </div>*/
/* */
/*             </div>*/
/*         </div>*/
/*         </form>*/
/*     </div>*/
/*   </div>*/
/* </div>*/
/* </div>*/
/* {{ footer }} */
/* */
