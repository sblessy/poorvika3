<?php

/* so-mobile/template/soconfig/panel_bar.twig */
class __TwigTemplate_91a4b8e985dbcdf343be9cd7f0bf535a39304ccc5241a08f2c5e4a4cbb4bc20a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 9
        echo "
<nav class=\"bar bar-tab\">
\t<a class=\"tab-item\" href=\"";
        // line 11
        echo (isset($context["home"]) ? $context["home"] : null);
        echo "\" data-transition=\"slide-in\">
\t\t<span class=\"icon icon-home\"></span>
\t\t<span class=\"tab-label\">";
        // line 13
        echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "text_home"), "method");
        echo " </span>
\t</a>
\t<a class=\"tab-item\" href=\"";
        // line 15
        echo (isset($context["menu_search"]) ? $context["menu_search"] : null);
        echo "\" data-transition=\"slide-in\">
\t\t<span class=\"icon icon-search\"></span>
\t\t<span class=\"tab-label\">";
        // line 17
        echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "text_search"), "method");
        echo " </span>
\t</a>
\t<a class=\"tab-item item-cart\" href=\"";
        // line 19
        echo (isset($context["shopping_cart"]) ? $context["shopping_cart"] : null);
        echo "\" data-transition=\"slide-in\">
\t\t<span class=\"icon icon-download\"></span>
\t\t<div id=\"cart\" class=\"btn-shopping-cart\">
\t\t\t<span class=\"total-shopping-cart cart-total-full\">
\t\t\t\t";
        // line 23
        echo (isset($context["text_items"]) ? $context["text_items"] : null);
        echo " 
\t\t\t</span>
\t\t</div>
\t\t
\t\t<span class=\"tab-label\">";
        // line 27
        echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "text_cart"), "method");
        echo " </span>
\t</a>
\t<a class=\"tab-item\" href=\"";
        // line 29
        echo (isset($context["login"]) ? $context["login"] : null);
        echo "\" data-transition=\"slide-in\">
\t\t<span class=\"icon icon-person\"></span>
\t\t<span class=\"tab-label\">";
        // line 31
        echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "text_account"), "method");
        echo " </span>
\t</a>

\t";
        // line 34
        if ($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "barmore_status"), "method")) {
            // line 35
            echo "\t<a class=\"tab-item tab-item--more tooltip-popovers\"  href=\"";
            echo (isset($context["actual_link"]) ? $context["actual_link"] : null);
            echo "#popover\">
\t\t<span class=\"icon icon-more\"></span>
\t\t<span class=\"tab-label\">";
            // line 37
            echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "text_more"), "method");
            echo " </span>
\t</a>
\t";
        }
        // line 40
        echo "</nav>

";
        // line 42
        if ($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "barmore_status"), "method")) {
            // line 43
            echo "<div id=\"popover\" class=\"popover fade bottom in right\">
\t<ul class=\"table-view\">
\t\t";
            // line 45
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "listmenus"), "method"));
            foreach ($context['_seq'] as $context["_key"] => $context["menuitem"]) {
                echo " 
\t\t\t
\t\t\t";
                // line 47
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["menuitem"], "namemore", array()));
                foreach ($context['_seq'] as $context["nameid"] => $context["menuname"]) {
                    // line 48
                    echo "\t\t\t\t";
                    if (($context["nameid"] == (isset($context["lang_id"]) ? $context["lang_id"] : null))) {
                        // line 49
                        echo "\t\t\t\t<li class=\"table-view-cell\"><a href=\"";
                        echo $this->getAttribute($context["menuitem"], "link", array());
                        echo "\">";
                        echo $context["menuname"];
                        echo "</a></li>
\t\t\t\t";
                    }
                    // line 51
                    echo "\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['nameid'], $context['menuname'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 52
                echo "\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['menuitem'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 53
            echo "\t</ul>
</div>
";
        }
        // line 56
        echo "\t


";
    }

    public function getTemplateName()
    {
        return "so-mobile/template/soconfig/panel_bar.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  136 => 56,  131 => 53,  125 => 52,  119 => 51,  111 => 49,  108 => 48,  104 => 47,  97 => 45,  93 => 43,  91 => 42,  87 => 40,  81 => 37,  75 => 35,  73 => 34,  67 => 31,  62 => 29,  57 => 27,  50 => 23,  43 => 19,  38 => 17,  33 => 15,  28 => 13,  23 => 11,  19 => 9,);
    }
}
/* {#*/
/* ****************************************************** */
/*  * @package	SO Framework for Opencart 3.x*/
/*  * @author	http://www.opencartworks.com*/
/*  * @license	GNU General Public License*/
/*  * @copyright(C) 2008-2017 opencartworks.com. All rights reserved.*/
/*  *******************************************************/
/* #}*/
/* */
/* <nav class="bar bar-tab">*/
/* 	<a class="tab-item" href="{{ home }}" data-transition="slide-in">*/
/* 		<span class="icon icon-home"></span>*/
/* 		<span class="tab-label">{{ objlang.get('text_home') }} </span>*/
/* 	</a>*/
/* 	<a class="tab-item" href="{{ menu_search}}" data-transition="slide-in">*/
/* 		<span class="icon icon-search"></span>*/
/* 		<span class="tab-label">{{ objlang.get('text_search') }} </span>*/
/* 	</a>*/
/* 	<a class="tab-item item-cart" href="{{ shopping_cart }}" data-transition="slide-in">*/
/* 		<span class="icon icon-download"></span>*/
/* 		<div id="cart" class="btn-shopping-cart">*/
/* 			<span class="total-shopping-cart cart-total-full">*/
/* 				{{ text_items }} */
/* 			</span>*/
/* 		</div>*/
/* 		*/
/* 		<span class="tab-label">{{ objlang.get('text_cart') }} </span>*/
/* 	</a>*/
/* 	<a class="tab-item" href="{{ login }}" data-transition="slide-in">*/
/* 		<span class="icon icon-person"></span>*/
/* 		<span class="tab-label">{{ objlang.get('text_account') }} </span>*/
/* 	</a>*/
/* */
/* 	{% if soconfig.get_settings('barmore_status')  %}*/
/* 	<a class="tab-item tab-item--more tooltip-popovers"  href="{{ actual_link }}#popover">*/
/* 		<span class="icon icon-more"></span>*/
/* 		<span class="tab-label">{{ objlang.get('text_more') }} </span>*/
/* 	</a>*/
/* 	{% endif %}*/
/* </nav>*/
/* */
/* {% if soconfig.get_settings('barmore_status') %}*/
/* <div id="popover" class="popover fade bottom in right">*/
/* 	<ul class="table-view">*/
/* 		{% for menuitem in soconfig.get_settings('listmenus') %} */
/* 			*/
/* 			{% for nameid, menuname in menuitem.namemore %}*/
/* 				{% if nameid == lang_id %}*/
/* 				<li class="table-view-cell"><a href="{{ menuitem.link }}">{{ menuname }}</a></li>*/
/* 				{% endif %}*/
/* 			{% endfor %}*/
/* 		{% endfor %}*/
/* 	</ul>*/
/* </div>*/
/* {% endif %}*/
/* 	*/
/* */
/* */
/* */
