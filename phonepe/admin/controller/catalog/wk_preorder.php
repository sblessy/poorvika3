<?php
################################################################################################
#  Preorder Pro Opencart 2.x.x.x From Webkul  http://webkul.com 	#
################################################################################################
class ControllerCatalogWkPreorder extends Controller {

	private $error = array();
	private $data = array();

	public function __construct($registry) {
		parent::__construct($registry);
		$registry->get('document')->addScript('view/javascript/jscolor/js/colorpicker.js');
		$registry->get('document')->addScript('view/javascript/jscolor/css/colorpicker.css');
	
    	$this->data = array_merge($this->data, $this->load->language('catalog/wk_preorder'));
    	$this->load->model('extension/module/wk_preorder_pro');
		$this->load->model('catalog/product');
		$this->load->model('tool/image');
	}
	public function index() {
		if(!$this->config->get('module_wk_preorder_pro_status')) {
      		$this->response->redirect($this->url->link('common/dashboard','&user_token=' . $this->session->data['user_token'],true));
		}
		$this->document->setTitle($this->language->get('heading_title_enquiry_list'));

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}

		if (isset($this->request->get['filter_email'])) {
			$filter_email = $this->request->get['filter_email'];
		} else {
			$filter_email = null;
		}

		if (isset($this->request->get['filter_subject'])) {
			$filter_subject = $this->request->get['filter_subject'];
		} else {
			$filter_subject = null;
		}

		if (isset($this->request->get['filter_product_name'])) {
			$filter_product_name = $this->request->get['filter_product_name'];
		} else {
			$filter_product_name = null;
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'pe.customer_name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}

		if (isset($this->request->get['page'])){
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		$url = '';
		$url = $this->setRequestgetVars('');

		$this->data['filter_value'] = array(
			'filter_name'              => $filter_name,
			'filter_email'             => $filter_email,
			'filter_subject'           => $filter_subject,
			'filter_product_name'	   => $filter_product_name,
			'filter_status' 		   => $filter_status,
			'sort'                     => $sort,
			'order'                    => $order,
			'start'                    => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'                    => $this->config->get('config_limit_admin')
		);

  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title_enquiry_list'),
			'href'      => $this->url->link('catalog/wk_preorder', 'user_token=' . $this->session->data['user_token']. $url , true)
   		);

		$enquiries = $this->model_extension_module_wk_preorder_pro->getEnquiries($this->data['filter_value']);
		$enquiries_total = $this->model_extension_module_wk_preorder_pro->getEnquiriesTotal($this->data['filter_value']);

		$this->data['delete'] = $this->url->link('catalog/wk_preorder/delete', 'user_token=' . $this->session->data['user_token']. $url, true);

		$this->data['enquiries'] = array();

		if($enquiries) {
			foreach ($enquiries as $key => $enquiry) {
				$this->data['enquiries'][] = array(
					'enquiry_id' => $enquiry['enquiry_id'],
					'product_id' => $enquiry['product_id'],
					'name' => $enquiry['name'],
					'customer_name' => $enquiry['customer_name'],
					'email' => $enquiry['email'],
					'subject' => $enquiry['subject'],
					'total_thread' => $enquiry['total_threads'],
					'status' => $enquiry['status'],
					'view' => $this->url->link('catalog/wk_preorder/enquiry', 'user_token='. $this->session->data['user_token'] . '&id='.$enquiry['enquiry_id'], true),
				);
			}
		}

 		$this->data['user_token'] = $this->session->data['user_token'];

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['error_warning'])) {
			$this->data['error'] = $this->session->data['error_warning'];
			unset($this->session->data['error_warning']);
		} else {
			$this->data['error'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';
		$url = $this->setRequestgetVar('sort');

		$this->data['sort_name'] = $this->url->link('catalog/wk_preorder', 'user_token=' . $this->session->data['user_token']. '&sort=pe.customer_name'.$url , true);
		$this->data['sort_email'] = $this->url->link('catalog/wk_preorder', 'user_token=' . $this->session->data['user_token'] . '&sort=pe.email' . $url, true);
		$this->data['sort_subject'] = $this->url->link('catalog/wk_preorder', 'user_token=' . $this->session->data['user_token'] . '&sort=pe.subject' . $url, true);
		$this->data['sort_product_name'] = $this->url->link('catalog/wk_preorder', 'user_token=' . $this->session->data['user_token'] . '&sort=pd.name' . $url, true);
		$this->data['sort_status'] = $this->url->link('catalog/wk_preorder', 'user_token=' . $this->session->data['user_token'] . '&sort=pe.status' . $url, true);

		$url = '';
		$url = $this->setRequestgetVar('page');

		$pagination = new Pagination();
		$pagination->total = $enquiries_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('catalog/wk_preorder', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

		$this->data['pagination'] = $pagination->render();

		$this->data['results'] = sprintf($this->language->get('text_pagination'), ($enquiries_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($enquiries_total - $this->config->get('config_limit_admin'))) ? $enquiries_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $enquiries_total, ceil($enquiries_total / $this->config->get('config_limit_admin')));

		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_email'] = $filter_email;
		$this->data['filter_subject'] = $filter_subject;
		$this->data['filter_product_name'] = $filter_product_name;
		$this->data['filter_status'] = $filter_status;

		$this->data['header'] = $this->load->Controller('common/header');
		$this->data['column_left'] = $this->load->Controller('common/column_left');
		$this->data['footer'] = $this->load->Controller('common/footer');

	   	$this->response->setOutput($this->load->view('catalog/wk_precustomer_list',$this->data));
  	}

	public function enquiry(){
		if(!$this->config->get('module_wk_preorder_pro_status')) {
      		$this->response->redirect($this->url->link('common/dashboard','&user_token=' . $this->session->data['user_token'],true));
    	}

		$this->document->setTitle($this->language->get('heading_title_enquiry'));

		if(isset($this->request->get['id']) && $this->request->get['id']) {
			$enquiry_id = $this->request->get['id'];
		} else {
			$enquiry_id = 0;
		}

		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title_enquiry_list'),
			'href'      => $this->url->link('catalog/wk_preorder', 'user_token=' . $this->session->data['user_token'] , true)
   		);

		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title_enquiry'),
			'href'      => $this->url->link('catalog/wk_preorder/enquiry', 'user_token=' . $this->session->data['user_token'] . '&id=' . $enquiry_id , true)
   		);

		$enquiry = $this->model_extension_module_wk_preorder_pro->getEnquiry($enquiry_id);

		$this->data['enquiry'] = array();
		if($enquiry) {
			$this->data['enquiry'] = $enquiry;
			$threads = $this->model_extension_module_wk_preorder_pro->getEnquiryThreads($enquiry_id, $enquiry['customer_id']);
			$this->data['threads'] = array();
			if($threads) {
				foreach ($threads as $key => $thread) {
					$this->data['threads'][] = array(
						'thread_id' => $thread['thread_id'],
						'enquiry_id' => $thread['enquiry_id'],
						'query' => $thread['query'],
						'posted_by' => $thread['posted_by'],
						'date_added' => $thread['date_added'],
						'status' => $thread['status'],
					);
				}
			}
		} else {
			$this->session->data['error_warning'] = $this->language->get('error_in_general');
			$this->response->redirect($this->url->link('catalog/wk_preorder', 'user_token=' . $this->session->data['user_token'] , true));
		}

		$this->data['user_token'] = $this->session->data['user_token'];

		$this->data['cancel'] = $this->url->link('catalog/wk_preorder', 'user_token=' . $this->session->data['user_token'], true);
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->data['header'] = $this->load->Controller('common/header');
		$this->data['column_left'] = $this->load->Controller('common/column_left');
		$this->data['footer'] = $this->load->Controller('common/footer');

	   	$this->response->setOutput($this->load->view('catalog/wk_preorder_enquiry',$this->data));
	}

	public function addThread() {
		$json = array();
		
		if ($this->validate()) {

			if($this->request->server['REQUEST_METHOD'] == 'POST' && $this->request->post) {
				if(isset($this->request->post['enquiry_id']) && !$this->request->post['enquiry_id']) {
					$json['success'] = false;
					$json['msg'] = $this->language->get('error_no_enquiry_id');
				} else if(!isset($this->request->post['enquiry_id'])) {
					$json['success'] = false;
					$json['msg'] = $this->language->get('error_in_general');
				}
				if(!isset($this->request->post['query']) || !$this->request->post['query'] || strlen(trim($this->request->post['query'])) < 10 || strlen(trim($this->request->post['query'])) > 500 ) {
					$json['success'] = false;
					$json['msg'] = $this->language->get('error_query_length');
				}
				if(!$json) {
					$this->model_extension_module_wk_preorder_pro->addThread($this->request->post);
					$json['success'] = true;
					$json['msg'] = $this->language->get('success_thread_added');
				}
			} else {
				$json['success'] = false;
				$json['msg'] = $this->language->get('error_in_general');
			}
		} else {
			$json['success'] = false;
			$json['msg'] = $this->language->get('error_permission');
		}
		
		$this->response->setOutput(json_encode($json));
	}

	public function removeThread() {
		$json = array();
		if($this->request->server['REQUEST_METHOD'] == 'POST' && $this->request->post) {
			$thread_id = $this->request->post['thread_id'];
			$this->model_extension_module_wk_preorder_pro->removeThread($thread_id);
			$json['success'] = true;
			$json['msg'] = $this->language->get('success_thread_delete');
		} else {
			$json['success'] = false;
			$json['msg'] = $this->language->get('error_in_general');
		}
		$this->response->setOutput(json_encode($json));
	}

	public function getListOrders() {

		if(!$this->config->get('module_wk_preorder_pro_status')) {
      		$this->response->redirect($this->url->link('common/dashboard','&user_token=' . $this->session->data['user_token'],true));
    	}

		$this->document->setTitle($this->language->get('heading_title_order'));

		$this->data['heading_title'] = $this->language->get('heading_title_order');
		$this->data['button_update'] = $this->language->get('button_update');
		$this->data['text_update_info'] = $this->language->get('text_update_info');
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['button_delete'] = $this->language->get('button_delete');
		//user
		$this->data['text_order_id'] = $this->language->get('text_order_id');
		$this->data['text_date'] = $this->language->get('text_date');
		$this->data['text_name'] = $this->language->get('text_name');
		$this->data['text_total'] = $this->language->get('text_total');

		$this->data['text_query'] = $this->language->get('text_query');
		$this->data['text_products'] = $this->language->get('text_products');
		$this->data['text_quantity'] = $this->language->get('text_quantity');
		$this->data['text_notifyc'] = $this->language->get('text_notifyc');
		$this->data['button_clrfilter'] = $this->language->get('button_clrfilter');
		$this->data['text_pname'] = $this->language->get('text_pname');
		$this->data['text_model'] = $this->language->get('text_model');

		if(isset($this->request->get['page'])){
			$page = $this->request->get['page'];
		}else{
			$page = 1;
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}

		if (isset($this->request->get['filter_id'])) {
			$filter_id = $this->request->get['filter_id'];
		} else {
			$filter_id = null;
		}

		if (isset($this->request->get['filter_date'])) {
			$filter_date = $this->request->get['filter_date'];
		} else {
			$filter_date = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'id';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_id'])) {
			$url .= '&filter_id=' . $this->request->get['filter_id'];
		}

		$this->data['filter_data'] = array(
			'filter_name'              => $filter_name,
			'filter_date'              => $filter_date,
			'filter_id'                => $filter_id,
			'sort'                     => $sort,
			'order'                    => $order,
			'start'                    => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'                    => $this->config->get('config_admin_limit')
		);


  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title_order'),
			'href'      => $this->url->link('catalog/wk_preorder/getListOrders', 'user_token=' . $this->session->data['user_token']. $url , true)
   		);

		$product_total = $this->model_extension_module_wk_preorder_pro->viewtotalOrders($this->data['filter_data']);

		$results = $this->model_extension_module_wk_preorder_pro->viewOrders($this->data['filter_data']);

		$this->data['delete'] = $this->url->link('catalog/wk_preorder/update', 'user_token=' . $this->session->data['user_token']. $url, true);

		$this->data['result_orders'] = array();

		foreach ($results as $result) {
			if($result['order_id']){
				$action = array();

				$action[] = array(
					'text' => $this->language->get('text_view'),
					'href' => $this->url->link('sale/order/info', 'user_token=' . $this->session->data['user_token'] .'&order_id=' . $result['order_id']. $url, true)
				);

				$resultsProduct = $this->model_extension_module_wk_preorder_pro->viewProductsOrder($result['order_id']);

				$products = array();
				foreach ($resultsProduct as $value) {
					if(file_exists(DIR_IMAGE.$value['image'])){
						$value['image'] = $this->model_tool_image->resize($value['image'],100,100);
					}else{
						$value['image'] = $this->model_tool_image->resize('no_image.jpg',100,100);
					}
					$products[] = array('name' 		=> $value['name'],
										'link' 		=> $this->url->link('catalog/product', 'user_token=' . $this->session->data['user_token'] . '&filter_name=' .$value['name'] ,true),
										'image'		=> $value['image'],
										'quantity' 	=> $value['quantity'],
										'model'   	=> $value['model']
										);
				}

				$this->data['result_orders'][] = array(
					'selected'=>False,
					'id' => $result['order_id'],
					'order_id' => $result['order_id'],
					'name' => $result['firstname'].' '.$result['lastname'],
					'date' => $result['date_added'],
					'product' => $products,
					'action'     => $action
					);
			}

		}


 		$this->data['user_token'] = $this->session->data['user_token'];

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['sort_name'] = $this->url->link('catalog/wk_preorder/getListOrders', 'user_token=' . $this->session->data['user_token']. '&sort=o.firstname'.$url , true);
		$this->data['sort_id'] = $this->url->link('catalog/wk_preorder/getListOrders', 'user_token=' . $this->session->data['user_token'] . '&sort=op.order_id' . $url, true);
		$this->data['sort_date'] = $this->url->link('catalog/wk_preorder/getListOrders', 'user_token=' . $this->session->data['user_token'] . '&sort=o.date_added' . $url, true);

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_date'])) {
			$url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_id'])) {
			$url .= '&filter_id=' . $this->request->get['filter_id'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $product_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('catalog/wk_preorder/getListOrders', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

		$this->data['pagination'] = $pagination->render();

		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_date'] = $filter_date;
		$this->data['filter_id'] = $filter_id;

		$this->data['header'] = $this->load->Controller('common/header');
		$this->data['column_left'] = $this->load->Controller('common/column_left');
		$this->data['footer'] = $this->load->Controller('common/footer');

	   	$this->response->setOutput($this->load->view('catalog/wk_preorder_list',$this->data));
  	}

  	public function update() {
		$url = '';
		$url = $this->setRequestgetVars('');
    	$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->post['selected']) && $this->validateForm()) {

			foreach ($this->request->post['selected'] as $id) {
				$this->model_extension_module_wk_preorder_pro->deleteOrderEntry($id);
	  		}

			$this->session->data['success'] = $this->language->get('text_success');

			$url='';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/wk_preorder/getListOrders', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}

    	$this->getListOrders();
  	}

  	public function preorder_productlist() {

		if(!$this->config->get('module_wk_preorder_pro_status')) {
	      $this->response->redirect($this->url->link('common/dashboard','&user_token=' . $this->session->data['user_token'],true));
	    }

  		$this->document->setTitle($this->language->get('heading_title_productlist'));

		  $this->data['user_token'] = $this->session->data['user_token'];
		  
  		if(isset($this->request->get['name'])) {
  			$this->data['name'] = $this->request->get['name'];
  		} else {
  			$this->data['name'] = null;
  		}

  		if(isset($this->request->get['model'])) {
  			$this->data['model'] = $this->request->get['model'];
  		} else {
  			$this->data['model'] = null;
  		}

  		if(isset($this->request->get['preorder_price'])) {
  			$this->data['preorder_price'] = $this->request->get['preorder_price'];
  		} else {
  			$this->data['preorder_price'] = null;
  		}

  		if(isset($this->request->get['deduction_type'])) {
  			$this->data['deduction_type'] = $this->request->get['deduction_type'];
  		} else {
  			$this->data['deduction_type'] = null;
  		}

  		if(isset($this->request->get['status'])) {
  			$status = $this->request->get['status'];
  		} else {
			$status = null;
		}
		

		$this->data['status'] = $status;

		if(isset($this->request->get['sort'])) {
			$this->data['sort'] = $this->request->get['sort'];
		} else {
			$this->data['sort'] = 'pd.name';
		}

		if(isset($this->request->get['order'])) {
			$this->data['order'] = $this->request->get['order'];
		} else {
			$this->data['order'] = 'ASC';
	  	}
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		
		$url = '';
		$url = $this->setRequestgetVar('');
  		$filter_data = array(
  			'sort' 			 => $this->data['sort'],
  			'order' 		 => $this->data['order'],
  			'name' 			 => $this->data['name'],
  			'model' 		 => $this->data['model'],
  			'preorder_price' => $this->data['preorder_price'],
  			'deduction_type' => $this->data['deduction_type'],
  			'status' 		 => $status,
			'start'          => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'          => $this->config->get('config_limit_admin')
  		);

  		$this->data['breadcrumbs'] = array();

  		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true),
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title_productlist'),
			'href'      => $this->url->link('catalog/wk_preorder/preorder_productlist', 'user_token=' . $this->session->data['user_token'] .$url , true),
   		);

   		$this->data['delete'] = $this->url->link('catalog/wk_preorder/delete_preorder_product', 'user_token=' . $this->session->data['user_token'] . $url, true);
		$this->data['add'] = $this->url->link('catalog/wk_preorder/preorder_productadd', 'user_token=' . $this->session->data['user_token'] . $url , true);
		if ($this->config->get('module_wk_preorder_pro_convert_preorder') == 1) {
			$this->data['convert'] = $this->url->link('catalog/wk_preorder/preorder_productconvert', 'user_token=' . $this->session->data['user_token'] . $url , true);
		}
		
		$url = '';
		$url = $this->setRequestgetVar('sort');

		$this->data['sort_pname'] = $this->url->link('catalog/wk_preorder/preorder_productlist', 'user_token=' . $this->session->data['user_token'] .'&sort=pd.name'. $url, true);

		$this->data['sort_pmodel'] = $this->url->link('catalog/wk_preorder/preorder_productlist', 'user_token=' . $this->session->data['user_token'] . '&sort=p.model' . $url, true);

		$this->data['sort_wk_fixed_price'] = $this->url->link('catalog/wk_preorder/preorder_productlist', 'user_token=' . $this->session->data['user_token'] . '&sort=pp.initial_price' . $url, true);

		$this->data['sort_deduction_type'] = $this->url->link('catalog/wk_preorder/preorder_productlist', 'user_token=' . $this->session->data['user_token'] . '&sort=pp.deduction_type' . $url, true);

		$this->data['sort_pp_status'] = $this->url->link('catalog/wk_preorder/preorder_productlist', 'user_token=' . $this->session->data['user_token'] . '&sort=pp.status' . $url, true);

   		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		if(isset($this->session->data['preorder_product_add']) && $this->session->data['preorder_product_add'])  {
			if(isset($this->session->data['preorder_product_add']['success']) && $this->session->data['preorder_product_add']['success']) {
				$this->data['added_success'] = sprintf($this->language->get('success_added'), rtrim($this->session->data['preorder_product_add']['success'],','));
			}
			if(isset($this->session->data['preorder_product_add']['alreadyAdded']) && $this->session->data['preorder_product_add']['alreadyAdded']) {
				$this->data['alreadyAdded'] = sprintf($this->language->get('error_already_added'), rtrim($this->session->data['preorder_product_add']['alreadyAdded'],','));
			}
			if(isset($this->session->data['preorder_product_add']['failure']) && $this->session->data['preorder_product_add']['failure']) {
				$this->data['failure'] = sprintf($this->language->get('error_failure'), rtrim($this->session->data['preorder_product_add']['failure'],','));
			}
			unset($this->session->data['preorder_product_add']);
		}

		if(isset($this->session->data['error_warning']) && $this->session->data['error_warning']) {
			$this->data['error_warning_2'] = $this->session->data['error_warning'];
			unset($this->session->data['error_warning']);
		} else {
			$this->data['error_warning_2'] = '';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';
		$url = $this->setRequestgetVar('page');
		$product_total = $this->model_extension_module_wk_preorder_pro->getTotalProducts($filter_data);
		$products = $this->model_extension_module_wk_preorder_pro->getproducts($filter_data);

		$pagination = new Pagination();
		$pagination->total = $product_total;
		$pagination->page = $page;
		$pagination->limit =  $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/wk_preorder/preorder_productlist', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

		$this->data['pagination'] = $pagination->render();

		$this->data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($product_total - $this->config->get('config_limit_admin'))) ? $product_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $product_total, ceil($product_total / $this->config->get('config_limit_admin')));


  		if($products) {
	  		foreach ($products as $key => $product) {
	  			if($product['deduction_type'] == 'pc'){
	  				$deduction_type = $this->language->get('text_percentage');;
	  			}else{
	  				$deduction_type = $this->language->get('text_fixed');;
	  			}
	  			$this->data['products'][] = array(
		  			'id'	=> $product['id'],
		  			'product_id' => $product['product_id'],
		  			'product_name' => $product['name'],
		  			'product_model' => $product['model'],
		  			'deduction_type' => $deduction_type,
		  			'status' => $product['status'],
		  			'preorder_price' => $this->currency->format($product['initial_price'], $this->config->get('config_currency')),
					'edit' => $this->url->link('catalog/wk_preorder/preorder_productadd', 'user_token=' . $this->session->data['user_token']. '&id='.$product['id'] . $url, true),
		  		);
	  		}
	  	}

	  	$this->data['header'] = $this->load->Controller('common/header');
	  	$this->data['footer'] = $this->load->Controller('common/footer');
	  	$this->data['column_left'] = $this->load->Controller('common/column_left');

	    $this->response->setOutput($this->load->view('catalog/wk_preorder_productlist',$this->data));
  	}

	public function preorder_productadd(){

		$url = '';
		$url = $this->setRequestgetVar('');
		if(!$this->config->get('module_wk_preorder_pro_status')) {
      		$this->response->redirect($this->url->link('common/dashboard','&user_token=' . $this->session->data['user_token'],true));
    	}
		$this->data['user_token'] = $this->session->data['user_token'];

		$this->document->addScript('view/javascript/jscolor/js/colorpicker.js');
		$this->document->addStyle('view/javascript/jscolor/css/colorpicker.css');

		if($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validateForm()) {
			$result = $this->model_extension_module_wk_preorder_pro->cretaePreOrderProduct($this->request->post);
			if(isset($this->request->post['id']) && $this->request->post['id']) {
				$this->session->data['success'] = $this->language->get('success_product_update');
			} else {
				$this->session->data['preorder_product_add'] = $result;
			}
			$this->response->redirect($this->url->link('catalog/wk_preorder/preorder_productlist', 'user_token='. $this->session->data['user_token'] . $url, true));
		}

		$preOrderProduct = array();
		$this->data['preOrderProduct'] = array();
		if(isset($this->request->get['id']) && $this->request->get['id']) {
			$this->data['id'] = $this->request->get['id'];
			$preOrderProduct = $this->model_extension_module_wk_preorder_pro->getPreOrderProductDetails($this->request->get['id']);
			if(!$preOrderProduct) {
				$this->session->data['error_warning'] = $this->language->get('error_no_preorder_product');
				$this->response->redirect($this->url->link('catalog/wk_preorder/preorder_productlist', 'user_token=' . $this->session->data['user_token'] . $url, true));
			}
			$this->data['pre_order_product'][$preOrderProduct['product_id']]['product_id'] = $preOrderProduct['product_id'];
			$this->data['pre_order_product'][$preOrderProduct['product_id']]['name'] = $preOrderProduct['name'];
			$this->data['preOrderProduct'] = $preOrderProduct;
			$this->document->setTitle($this->language->get('heading_title_productedit'));
			$this->data['heading_title'] = $this->language->get('heading_title_productedit');
		} else {
			$this->document->setTitle($this->language->get('heading_title_productadd'));
			$this->data['heading_title'] = $this->language->get('heading_title_productadd');
		}
		if ($this->config->get('module_wk_preorder_pro_discount_type')) {
			$this->data['type'] = '%';
		} else {
			$this->data['type'] = $this->config->get('config_currency');
		}

		$this->data['module_wk_preorder_pro_discount'] = $this->config->get('module_wk_preorder_pro_discount');
		if($this->error) {
			foreach ($this->error as $key => $value) {
				$this->data[$key] = $value;
			}
		}

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		$values = array(
			'pre_order_product' => 'pre_order_product',
			'deduction_type' => 'wk_deduction_method',
			'wk_percentage_price' => 'wk_percentage_price',
			'preorder_quantity' => 'wk_preorder_quantity',
			'order_quantity' => 'wk_quantity_per_order',
			'customer_order' => 'wk_order_per_customer',
			'one_ip_one_order' => 'wk_one_ip_one_order',
			'preorder_start_date' => 'wk_preorder_start_date',
			'wk_available_date' => 'wk_available_date',
			'status' => 'wk_status',
			'wk_preorder_discount'	=> 'wk_preorder_discount',
		);

		foreach ($values as $key => $value) {
			
			if(isset($this->request->post[$value])) {
				$this->data[$value] = $this->request->post[$value];
			} else if($preOrderProduct && isset($preOrderProduct[$key]) || $key == 'wk_preorder_discount' && isset($preOrderProduct['discount'])) {
				if ($key == 'wk_preorder_discount') {
					$this->data['wk_preorder_discount'] = $preOrderProduct['discount'];
				} else {
					$this->data[$value] = $preOrderProduct[$key];
				}
			}
		}

		$this->data['breadcrumbs'] = array();

  		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true),
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title_productlist'),
			'href'      => $this->url->link('catalog/wk_preorder/preorder_productlist', 'user_token=' . $this->session->data['user_token'] .$url , true),
   		);


		if (isset($this->data['id'])) {
			$href = $this->url->link('catalog/wk_preorder/preorder_productadd&id='.$this->data['id'], 'user_token=' . $this->session->data['user_token'] , true);
			$this->data['add'] = $this->url->link('catalog/wk_preorder/preorder_productadd&id='.$this->data['id'], 'user_token=' . $this->session->data['user_token'] . $url, true);
		} else {
			$href = $this->url->link('catalog/wk_preorder/preorder_productadd', 'user_token=' . $this->session->data['user_token'] . $url, true);
			$this->data['add'] = $this->url->link('catalog/wk_preorder/preorder_productadd', 'user_token=' . $this->session->data['user_token'] . $url, true);
		}
		$this->data['breadcrumbs'][] = array(
      		'text'  => $this->language->get('heading_title_productadd'),
			'href'  => $href,
   		);

		$this->data['cancel'] = $this->url->link('catalog/wk_preorder/preorder_productlist', 'user_token=' . $this->session->data['user_token'] . $url, true);

		$this->data['header'] = $this->load->Controller('common/header');
		$this->data['footer'] = $this->load->Controller('common/footer');
		$this->data['column_left'] = $this->load->Controller('common/column_left');

		$this->response->setOutput($this->load->view('catalog/wk_preorder_productadd',$this->data));
	}

  	public function delete_preorder_product() {
		$url = '';
		$url = $this->setRequestgetVar('');
		if ( $this->validate()) {
			if (isset($this->request->post['selected'])) {

				foreach ($this->request->post['selected'] as $id) {
					$this->model_extension_module_wk_preorder_pro->delete_preorder_productct($id);
				  }
	
				$this->session->data['success'] = $this->language->get('text_success_deleteproduct');
	
			} else {
				$this->session->data['error_warning'] = $this->language->get('error_deleteproduct');
	
			}
		} else {
			$this->session->data['error_warning'] = $this->language->get('error_permission');
		}
		
		$this->response->redirect($this->url->link('catalog/wk_preorder/preorder_productlist', 'user_token=' . $this->session->data['user_token'] . $url, true));
	  }
	
	public function preorder_productconvert() {
		$url = '';
		$url = $this->setRequestgetVar('');
		if ($this->validate()) {
			$this->model_extension_module_wk_preorder_pro->convertAll();
			$this->session->data['success'] = $this->language->get('text_success_convert');
		} else {
			$this->session->data['error_warning'] = $this->language->get('error_permission');
		}
		
		$this->response->redirect($this->url->link('catalog/wk_preorder/preorder_productlist', 'user_token=' . $this->session->data['user_token'], true));
	}
  	public function change_status_preorder_product() {
		if (isset($this->request->post['selected']) && $this->validate()) {

			foreach ($this->request->post['selected'] as $id) {
				$this->model_extension_module_wk_preorder_pro->delete_preorder_productct($id);
	  		}

			$this->session->data['success'] = $this->language->get('text_success_deleteproduct');
			$this->response->redirect($this->url->link('catalog/wk_preorder/preorder_productlist', 'user_token=' . $this->session->data['user_token'], true));

		} else {
			$this->getListOrders();
		}
  	}

  	public function email() {

    	$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->get['id']) && $this->validate()) {

			$id = $this->request->get['id'];

			$this->model_extension_module_wk_preorder_pro->emailToCustomer($id,'enquiry');

			$this->session->data['success'] = $this->language->get('text_success_notify');

			$url='';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/wk_preorder', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}

    	$this->getListOrders();
  	}

  	public function delete() {
		$url = '';
		$url = $this->setRequestgetVars('');
    	$this->document->setTitle($this->language->get('heading_title'));

		if($this->request->server['REQUEST_METHOD'] == 'POST') {
			if ($this->validate()) {
				if(isset($this->request->post['selected']) && $this->request->post['selected']) {
					foreach ($this->request->post['selected'] as $id) {
						$this->model_extension_module_wk_preorder_pro->deleteEnquiry($id);
					}
					$this->session->data['success'] = $this->language->get('text_success_delete_enquiry');
				} else {
					$this->session->data['error_warning'] = $this->language->get('error_delete_enquiry');
				}
			} else {
				$this->session->data['error_warning'] = $this->language->get('error_permission');
			}
		} else {
			$this->session->data['error_warning'] = $this->language->get('error_in_general');
		}

		$this->response->redirect($this->url->link('catalog/wk_preorder', 'user_token=' . $this->session->data['user_token'] . $url, true));
  	}

	private function validate() {
		if (!$this->user->hasPermission('modify', 'catalog/wk_preorder')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	private function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/wk_preorder')) {
			$this->error['warning'] = $this->language->get('error_permission');
			return false;
		}

		if(isset($this->request->post['pre_order_product']) && !$this->request->post['pre_order_product']) {
			$this->error['form_error_no_product'] = $this->language->get('error_no_product');
		} else if(!isset($this->request->post['pre_order_product'])) {
			$this->error['form_error_no_product'] = $this->language->get('error_no_product');
		} 

		if(isset($this->request->post['pre_order_product']) && !isset($this->request->post['id'])) {
			$product_name = array();
			foreach ($this->request->post['pre_order_product'] as $key => $pre_order_product) {
				$result = $this->model_extension_module_wk_preorder_pro->getPreOrderProductExists($pre_order_product['product_id']);
				if($result) {
					$product_name[] = $pre_order_product['name'];
				}
				if($product_name) {
					$this->error['form_error_no_product'] = sprintf($this->language->get('error_product_exist'), implode($product_name,','));
				}
			}
		}

		if (!isset($this->request->post['wk_deduction_method']) || !in_array($this->request->post['wk_deduction_method'], array('fp', 'pc'))) {
			$this->error['error_method'] = $this->data['error_status'];
		} else if (isset($this->request->post['wk_deduction_method']) && $this->request->post['wk_deduction_method'] == 'pc') {
			if(!isset($this->request->post['wk_percentage_price']) || !$this->request->post['wk_percentage_price'] || $this->request->post['wk_percentage_price'] > 100 || !is_numeric($this->request->post['wk_percentage_price'])) {
				$this->error['form_error_no_percentage'] = $this->language->get('error_no_percentage');
			}
		}

		if(!isset($this->request->post['wk_preorder_discount']) || ($this->request->post['wk_preorder_discount'] != '' && !is_numeric($this->request->post['wk_preorder_discount']))) {
			$this->error['error_discount'] = $this->language->get('error_no_discount');
		} else if(!isset($this->error['error_discount']) || !$this->error['error_discount']) {
			if ($this->config->get('module_wk_preorder_pro_discount_type') && $this->request->post['wk_preorder_discount'] > 100) {
				$this->error['error_discount'] = $this->language->get('error_no_discount');
			} else if (!$this->config->get('module_wk_preorder_pro_discount_type') && $this->request->post['wk_preorder_discount'] > 10000){
				$this->error['error_discount'] = $this->language->get('error_no_discount_fixed');
			}
		}

		if(!isset($this->request->post['wk_preorder_start_date']) || $this->request->post['wk_preorder_start_date'] == '') {
			$this->error['form_error_start_date'] = $this->language->get('error_start_date');
		}

		if(isset($this->request->post['wk_available_date']) && $this->request->post['wk_available_date'] == '') {
			$this->error['form_error_available_date'] = $this->language->get('error_available_date');
		}

		if(strtotime($this->request->post['wk_preorder_start_date']) > strtotime($this->request->post['wk_available_date']) ){
			$this->error['form_error_available_date'] = $this->language->get('error_till_date') ;
		}

		if (!isset($this->request->post['wk_preorder_quantity']) || $this->request->post['wk_preorder_quantity'] < 1 || $this->request->post['wk_preorder_quantity'] > 500 || ctype_space($this->request->post['wk_preorder_quantity']) || !is_numeric($this->request->post['wk_preorder_quantity'])) {
			$this->error['quantity_error'] = $this->language->get('quantity_err');
		}

		if (!isset($this->request->post['wk_quantity_per_order']) || $this->request->post['wk_quantity_per_order'] < 1 || $this->request->post['wk_quantity_per_order'] > 500 || ctype_space($this->request->post['wk_quantity_per_order']) || !is_numeric($this->request->post['wk_quantity_per_order'])) {
			$this->error['quantity_per_oerror'] = $this->language->get('quantity_per_oerr');
		} else if($this->request->post['wk_quantity_per_order'] > $this->request->post['wk_preorder_quantity']) {
			$this->error['quantity_per_oerror'] = $this->language->get('order_les_error');
		}

		if (!isset($this->request->post['wk_order_per_customer']) || $this->request->post['wk_order_per_customer'] < 1 || $this->request->post['wk_order_per_customer'] > 500 || ctype_space($this->request->post['wk_order_per_customer']) || !is_numeric($this->request->post['wk_order_per_customer'])) {
			$this->error['quantity_cust_error'] = $this->language->get('per_cust_error');
		} 
		if (!isset($this->request->post['wk_one_ip_one_order']) || !in_array($this->request->post['wk_one_ip_one_order'], array('0', '1'))) {
			$this->error['error_ip'] = $this->data['error_status'];
		}

		if (!isset($this->request->post['wk_status']) || !in_array($this->request->post['wk_status'], array('0', '1'))) {
			$this->error['error_preorder_status'] = $this->data['error_status'];
		}
		if($this->error) {
			$this->error['form_error_warning'] = $this->language->get('error_warning');
		}
		return !$this->error;
	}

	public function autocomplete() {
		$json = array();
	
		if (isset($this->request->get['filter_name']) || isset($this->request->get['filter_model'])) {
		  if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		  } else {
			$filter_name = '';
		  }
	
		  if (isset($this->request->get['filter_model'])) {
			$filter_model = $this->request->get['filter_model'];
		  } else {
			$filter_model = '';
		  }
	
		  if (isset($this->request->get['limit'])) {
			$limit = $this->request->get['limit'];
		  } else {
			$limit = 5;
		  }
	
		  $filter_data = array(
			'name'  => $filter_name,
			'model' => $filter_model,
			'start'        => 0,
			'limit'        => $limit
		  );
	
		  $results = $this->model_extension_module_wk_preorder_pro->getproducts($filter_data);
	
		  foreach ($results as $result) {
	
			$json[] = array(
			  'product_id' => $result['product_id'],
			  'name'       => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
			  'model'      => $result['model'],
			);
		  }
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	  }

	public function autocompleteFilter() {
		$json = array();
	
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}

		if (isset($this->request->get['filter_email'])) {
			$filter_email = $this->request->get['filter_email'];
		} else {
			$filter_email = '';
		}

		if (isset($this->request->get['filter_product_name'])) {
			$filter_product_name = $this->request->get['filter_product_name'];
		} else {
			$filter_product_name = '';
		}
		if (isset($this->request->get['filter_subject'])) {
			$filter_subject = $this->request->get['filter_subject'];
		} else {
			$filter_subject = '';
		}

		if (isset($this->request->get['limit'])) {
			$limit = $this->request->get['limit'];
		} else {
			$limit = 5;
		}
	
		  $filter_data = array(
			'filter_name'  => $filter_name,
			'filter_email' => $filter_email,
			'filter_product_name'=> $filter_product_name,
			'filter_subject'	=> $filter_subject,
			'start'        => 0,
			'limit'        => $limit
		  );
	
		  $results = $this->model_extension_module_wk_preorder_pro->getEnquiries($filter_data);
		  if ($results) {
			foreach ($results as $key => $result) {
	
				$json[] = array(
				  'product_id' => $result['product_id'],
				  'name'       => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
				  'customer_name' => strip_tags(html_entity_decode($result['customer_name'], ENT_QUOTES, 'UTF-8')),
				  'email'	=> $result['email'],
				  'subject'		=> $result['subject'],
				);
			  }
		  }
		 
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	  }
	  public function setRequestgetVar($type = '') {  

		$order = 'ASC';
	
		if(isset($this->request->get['order']) && $this->request->get['order'])
			$order = $this->request->get['order'];
			
		//setting get variable in URL code starts here
		$url = '';
			
		$uri_component = array(
			'name',
			'model',
			'preorder_price',
			'deduction_type',
			'status',   
			'sort',
			'page',
		);
	
		switch ($type) {
			case 'page':
			foreach ($uri_component as $key => $value) {
				if($value != 'page')
				$url .=  $this->ocutilities->_setStringURLs($value);
			}
			break;
	
			case 'sort':
				foreach ($uri_component as $key => $value) {
				if($value != 'sort')
				$url .=  $this->ocutilities->_setStringURLs($value);
				}
				if ($order == 'ASC') {
				$order = 'DESC';
				} else {
				$order = 'ASC';
				}
			break;
			
			default:
			foreach ($uri_component as $key => $value) {
				$url .=  $this->ocutilities->_setStringURLs($value);
			}
			break;
		}
	
		$url .= '&order=' . $order;        
			
		//setting get variable in URL code ends here
	
		return $url;
	}
	public function setRequestgetVars($type = '') {  

		$order = 'ASC';
	
		if(isset($this->request->get['order']) && $this->request->get['order'])
			$order = $this->request->get['order'];
			
		//setting get variable in URL code starts here
		$url = '';
			
		$uri_component = array(
			'filter_name',
			'filter_email',
			'filter_subject',
			'filter_product_name',
			'filter_status',   
			'sort',
			'page',
		);
	
		switch ($type) {
			case 'page':
			foreach ($uri_component as $key => $value) {
				if($value != 'page')
				$url .=  $this->ocutilities->_setStringURLs($value);
			}
			break;
	
			case 'sort':
				foreach ($uri_component as $key => $value) {
				if($value != 'sort')
				$url .=  $this->ocutilities->_setStringURLs($value);
				}
				if ($order == 'ASC') {
				$order = 'DESC';
				} else {
				$order = 'ASC';
				}
			break;
			
			default:
			foreach ($uri_component as $key => $value) {
				$url .=  $this->ocutilities->_setStringURLs($value);
			}
			break;
		}
	
		$url .= '&order=' . $order;        
			
		//setting get variable in URL code ends here
	
		return $url;
	}

}
