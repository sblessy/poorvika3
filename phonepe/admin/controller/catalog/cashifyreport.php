<?php 
ob_start();
class ControllerCatalogCashifyreport extends Controller {
	private $error = array(); 
     
  	public function index() {
  		$this->load->model('catalog/cashifyreport_new');
		//$this->load->language('catalog/service_new');
    	
		$this->document->setTitle("Cashify Report"); 
		$data['heading_title'] = "Cashify Report";
		
		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
      		
			
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => "Cashify Report",
			'href'      => $this->url->link('catalog/cashifyreport', 'user_token=' . $this->session->data['user_token'], true)       		
      		
   		);

		$data['user_token'] = $this->session->data['user_token'];

        $data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		
		$data['csh_lists'] = $this->model_catalog_cashifyreport_new->getAllList();
		
		//$this->getList();
		$this->response->setOutput($this->load->view('catalog/cashify_report', $data));
  	}
  	public function getCashifyData() {
  		$this->load->model('catalog/cashifyreport_new');
  		$cashify_id = $_POST['cashify_id'];
  		$data = array();
  		$data = $this->model_catalog_cashifyreport_new->getList($cashify_id);
  		echo json_encode($data);
  		
  	}
}
	
?>