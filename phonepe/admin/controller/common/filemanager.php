<?php
putenv('GOOGLE_APPLICATION_CREDENTIALS='. DIR_SYSTEM .'credentials/MyProject40277-fe82019f2a3a.json');
require DIR_SYSTEM.'library/vendor/autoload.php';
use Google\Cloud\Storage\StorageClient;

class ControllerCommonFileManager extends Controller {
	public function index() {
		$this->load->language('common/filemanager');

		// Find which protocol to use to pass the full image link back
		if ($this->request->server['HTTPS']) {
			$server = HTTPS_CATALOG;
		} else {
			$server = HTTP_CATALOG;
		}
		
		
		$storage = new StorageClient;
		$bucketname = "img.poorvika.com";
		$bucket = $storage->bucket($bucketname);
		$data['images'] = array();
		
		if (isset($this->request->get['directory'])) {
			
			$directory = $this->request->get['directory'];
		} else {
			$directory = "";
		}
		
		
		$options = ['prefix' => $directory];
		//$options = ['prefix' => 'data/AAA Acc/Apple/'];
		foreach ($bucket->objects($options) as $object) {
			$image_name = $object->name();
			$array_path = explode('/',$image_name);
			
			array_pop($array_path);
			if(!empty($array_path)){
				if(!empty($directory)){
					$array_search = explode('/',$directory);
					$array_count = count($array_search);
					
					$last_array = array_pop($array_search);
					$current_array_val = array_search($last_array, $array_path);
					
						if(!empty($array_path[$current_array_val+1])){
							$next_array_val = $array_path[$current_array_val+1];
							$path = $directory.'/'.$next_array_val;
						}
						
				}else{
					$next_array_val = $array_path[-1+1];
					$path = $next_array_val;
				}
				//echo "<pre>";print_r($path);die;
				if(!empty($next_array_val)){
					$data['images'][$next_array_val] = array(
						'name'  => $next_array_val,
						'type'  => 'directory',
						'href'  => $this->url->link('common/filemanager', 'user_token=' . $this->session->data['user_token'] . '&directory=' . urlencode(html_entity_decode($path, ENT_QUOTES, 'UTF-8')) , true)
					);
				}else{
					
					$image_name = explode('/',$object->name());
					$dir_images = array_pop($image_name);
					if(!empty($object->name()) && !empty($dir_images)){
						$data['images'][$dir_images] = array(
							'thumb' =>  "https://storage.cloud.google.com/".$bucketname.'/'.$object->name(),
							'name' => $dir_images,
							'type'  => 'image',
							'href'  => "https://storage.cloud.google.com/".$bucketname.'/'.$object->name()
						);
					}
				}
				
			}else{
				$data['images'][] = array(
					'thumb' =>  "https://storage.cloud.google.com/".$bucketname.'/'.$object->name(),
					'name' => $object->name(),
					'type'  => 'image',
					'href'  => "https://storage.cloud.google.com/".$bucketname.'/'.$object->name()
				);
			}
						
		}
		//die;
				
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['entry_search'] = $this->language->get('entry_search');
		$data['entry_folder'] = $this->language->get('entry_folder');

		$data['button_parent'] = $this->language->get('button_parent');
		$data['button_refresh'] = $this->language->get('button_refresh');
		$data['button_upload'] = $this->language->get('button_upload');
		$data['button_folder'] = $this->language->get('button_folder');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_search'] = $this->language->get('button_search');

		$data['user_token'] = $this->session->data['user_token'];

		if (isset($this->request->get['directory'])) {
			$data['directory'] = urlencode($this->request->get['directory']);
		} else {
			$data['directory'] = '';
		}
		
		if (isset($this->request->get['filter_name'])) {
			$data['filter_name'] = $this->request->get['filter_name'];
		} else {
			$data['filter_name'] = '';
		}
		

		// Return the target ID for the file manager to set the value
		if (isset($this->request->get['target'])) {
			$data['target'] = $this->request->get['target'];
		} else {
			$data['target'] = '';
		}
		
		//thumb-image
		// Return the thumbnail for the file manager to show a thumbnail
		if (isset($this->request->get['thumb'])) {
			$data['thumb'] = $this->request->get['thumb'];
		} else {
			$data['thumb'] = '';
		}

		// Parent
		$url = '';

		if (isset($this->request->get['directory'])) {
			$pos = strrpos($this->request->get['directory'], '/');

			if ($pos) {
				$url .= '&directory=' . urlencode(substr($this->request->get['directory'], 0, $pos));
			}
		}

		if (isset($this->request->get['target'])) {
			$url .= '&target=' . $this->request->get['target'];
		}

		if (isset($this->request->get['thumb'])) {
			$url .= '&thumb=' . $this->request->get['thumb'];
		}

		$data['parent'] = $this->url->link('common/filemanager', 'user_token=' . $this->session->data['user_token'] . $url, true);

		// Refresh
		$url = '';

		if (isset($this->request->get['directory'])) {
			$url .= '&directory=' . urlencode($this->request->get['directory']);
		}

		if (isset($this->request->get['target'])) {
			$url .= '&target=' . $this->request->get['target'];
		}

		if (isset($this->request->get['thumb'])) {
			$url .= '&thumb=' . $this->request->get['thumb'];
		}

		$data['refresh'] = $this->url->link('common/filemanager', 'user_token=' . $this->session->data['user_token'] . $url, true);

		$url = '';

		if (isset($this->request->get['directory'])) {
			$url .= '&directory=' . urlencode(html_entity_decode($this->request->get['directory'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['target'])) {
			$url .= '&target=' . $this->request->get['target'];
		}

		if (isset($this->request->get['thumb'])) {
			$url .= '&thumb=' . $this->request->get['thumb'];
		}
		
		$image_total = 1;
		$page = 1;
		$pagination = new Pagination();
		$pagination->total = $image_total;
		$pagination->page = $page;
		$pagination->limit = 1;
		$pagination->url = $this->url->link('common/filemanager', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$this->response->setOutput($this->load->view('common/filemanager', $data));
	}

	public function upload() {
		$this->load->language('common/filemanager');

		$json = array();

		// Check user has permission
		if (!$this->user->hasPermission('modify', 'common/filemanager')) {
			$json['error'] = $this->language->get('error_permission');
		}

		if (!$json) {
			// Check if multiple files are uploaded or just one
			$files = array();

			if (!empty($this->request->files['file']['name']) && is_array($this->request->files['file']['name'])) {
				foreach (array_keys($this->request->files['file']['name']) as $key) {
					$files[] = array(
						'name'     => $this->request->files['file']['name'][$key],
						'type'     => $this->request->files['file']['type'][$key],
						'tmp_name' => $this->request->files['file']['tmp_name'][$key],
						'error'    => $this->request->files['file']['error'][$key],
						'size'     => $this->request->files['file']['size'][$key]
					);
				}
			}

			foreach ($files as $file) {
				if (is_file($file['tmp_name'])) {
					// Sanitize the filename
					$filename = basename(html_entity_decode($file['name'], ENT_QUOTES, 'UTF-8'));
					
					if (isset($this->request->get['directory'])) {
						$directory_name = $this->request->get['directory'].'/'.$filename;
					} else {
						$directory_name = $filename;
					}
					//echo "<pre>";print_r($directory_name);die;
					//GCP cloud image upload start
						$storage = new StorageClient();
						$file = fopen($file['tmp_name'],'r');
						$bucketName = "img.poorvika.com";
						$bucket = $storage->bucket($bucketName);
						$object = $bucket->upload($file,
							['name' => $directory_name
						]);
						
						if(!empty($object->name())){
							$json['success'] = $this->language->get('text_uploaded');	
						}else{
							$json['error'] = $this->language->get('error_upload');
						}
					//GCP End
					
				} else {
					$json['error'] = $this->language->get('error_upload');
				}

				
			}
		}

		if (!$json) {
			$json['success'] = $this->language->get('text_uploaded');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function folder() {
		$this->load->language('common/filemanager');

		$json = array();

		// Check user has permission
		if (!$this->user->hasPermission('modify', 'common/filemanager')) {
			$json['error'] = $this->language->get('error_permission');
		}

		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			// // Sanitize the folder name
			$folderName = basename(html_entity_decode($this->request->post['folder'], ENT_QUOTES, 'UTF-8'));
			
			if (!empty($this->request->get['directory'])) {
				$directory = $this->request->get['directory'].'/';
			} else {
				$directory = '';
			}
			
			if(!empty($directory) && !empty($folderName)){
				$folder = $directory.$folderName.'/';
			}else if(!empty($folderName)){
				$folder = $folderName.'/';			
			}else{
				$folder = 0;
			}
			
			if(!empty($folder)){
				$storage = new StorageClient();
				$bucketName = "img.poorvika.com";;
				$bucket = $storage->bucket($bucketName);
				$options = [ 'name' => $folder ];
				
				$upload = $bucket->upload( NULL, $options );
				if($upload->name()){
					$json['success'] = "Folder Created successfuflly";
				}else{
					$json['error'] = "Already created";
				}
				
			}else{
				$json['error'] = "Enter the folder Name";
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function delete() {
		$this->load->language('common/filemanager');

		$json = array();

		// Check user has permission
		if (!$this->user->hasPermission('modify', 'common/filemanager')) {
			$json['error'] = $this->language->get('error_permission');
		}

		if (!empty($this->request->post['path'])){
			$paths = $this->request->post['path'];
		} else {
			$paths = array();
		}
		
		
		$storage = new StorageClient;
		$bucketName = "img.poorvika.com";
		$bucket = $storage->bucket($bucketName);
		
		foreach($paths as $path){
			if(!empty($this->request->get['directory'])){
				
				if(strpos($path,'.') !== false){
					$objectName = $this->request->get['directory'].'/'.$path;
				}else{
					$objectName = $this->request->get['directory'].'/'.$path.'/';
				}
				
			}else{
				if(strpos($path,'.') !== false){
					$objectName = '/'.$path;
				}else{
					$objectName = $path.'/';
				}
				
			}
			
			$object = $bucket->object($objectName);
			$object->delete();
			$delete = "This directory Deleted";
		}
		
		if(!empty($delete)){
			$json['success'] = $delete;
			
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	

		
	}
}
