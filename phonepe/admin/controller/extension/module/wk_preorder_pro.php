<?php
###################################################################
#  Preorder Pro Opencart 3.x.x.x From Webkul  http://webkul.com   #
###################################################################
class ControllerExtensionModulewkPreorderPro extends Controller {

	private $error = array();
	private $data = array();

	public function __construct($registry) {
		parent::__construct($registry);
		$registry->get('document')->addScript('view/javascript/jscolor/js/colorpicker.js');
		$registry->get('document')->addScript('view/javascript/jscolor/css/colorpicker.css');
	
		$this->load->model("localisation/language");
		$this->load->model('setting/setting');
		$this->load->model('localisation/order_status');
		$this->load->model('extension/module/wk_preorder_pro');
		$this->load->model('setting/event');
		$this->load->model('localisation/stock_status');
		$this->load->model('catalog/mail');
		$this->data = array_merge($this->data, $this->load->language('extension/module/wk_preorder_pro'));
		// Get All Order statuses
		$this->data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
		// Set Store Time zone
		$this->data['store_timezone'] = $this->timeZone();
		// Get Store languages
		$this->data['languages'] = $this->model_localisation_language->getLanguages();
		
	}
	public function install() {
	
		$this->model_extension_module_wk_preorder_pro->createTable();
		$this->model_setting_event->addEvent('module_wk_preorder_pro', 'catalog/controller/checkout/cart/remove/after', 'checkout/precart/clearPreOrderSessionTrigger');
	}

	public function uninstall() {
	
		$this->model_extension_module_wk_preorder_pro->dropTable();
		$this->model_setting_event->deleteEvent('module_wk_preorder_pro');
	}

	public function index() {

		$this->document->setTitle($this->data['heading_title']);

		// Get Pre Order Mails
		$this->data['mails'] = $this->model_catalog_mail->gettotal();
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('module_wk_preorder_pro', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
		}
		
		
		$this->data['user_token'] = $this->session->data['user_token'];

		//CONFIG
		$config_data = array(
			'module_wk_preorder_pro_status',
			'module_wk_preorder_pro_text',
			'module_wk_preorder_pro_notification_mode',
			'module_wk_preorder_pro_link',
			'module_wk_preorder_pro_mail_status',
			'module_wk_preorder_pro_mail_admin_message',
			'module_wk_preorder_pro_preordertext',
			'module_wk_preorder_pro_convert_preorder',
			'module_wk_preorder_pro_preorder_days',
			'module_wk_preorder_pro_discount',
			'module_wk_preorder_pro_discount_type',
			'module_wk_preorder_pro_quantity',
			'module_wk_preorder_pro_order_quantity',
			'module_wk_preorder_pro_customer_order',
			'module_wk_preorder_pro_guest_user',
			'module_wk_preorder_pro_order_status',
			'module_wk_preorder_pro_timezone',
			'module_wk_preorder_pro_notification_status',
			'module_wk_preorder_pro_stock_status',
			'module_wk_preorder_pro_site_key',
			'module_wk_preorder_pro_secret_key',
			'module_wk_preorder_pro_mail_keywords',
			'module_wk_preorder_pro_mail_notify_customer',
			'module_wk_preorder_pro_server_key',
			'module_wk_preorder_pro_messenger_id',
		);

		foreach ($config_data as $conf) {
			
			if (isset($this->request->post[$conf])) {
				$this->data[$conf] = $this->request->post[$conf];
			} else if ($this->config->get($conf)) {
				$this->data[$conf] = $this->config->get($conf);
			} else {
				if ($conf == 'module_wk_preorder_pro_preordertext' || $conf == 'module_wk_preorder_pro_text' || $conf == 'module_wk_preorder_pro_mail_admin_message') {
					$this->data[$conf] = array();
				} else {
					$this->data[$conf] = '';
				}
			}
		}

		if($this->error) {
			foreach ($this->error as $key => $value) {
				if (isset($this->error[$key]) && $this->error[$key]) {
					$this->data[$key] = $value;
				}
			}
		}

		

		$this->data['stock_statuses'] = $this->model_localisation_stock_status->getStockStatuses();

  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('extension/module/wk_preorder_pro', 'user_token=' . $this->session->data['user_token'], true)
   		);

		$this->data['action'] = $this->url->link('extension/module/wk_preorder_pro', 'user_token=' . $this->session->data['user_token'], true);

		$this->data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);

		$this->data['header'] = $this->load->Controller('common/header');
		$this->data['column_left'] = $this->load->Controller('common/column_left');
		$this->data['footer'] = $this->load->Controller('common/footer');

	    $this->response->setOutput($this->load->view('extension/module/wk_preorder_pro',$this->data));
	}

	public function timeZone() {
		return array(
			'Kwajalein' => '(GMT-12:00) International Date Line West',
			'Pacific/Midway' => '(GMT-11:00) Midway Island',
			'Pacific/Samoa' => '(GMT-11:00) Samoa',
			'Pacific/Honolulu' => '(GMT-10:00) Hawaii',
			'America/Anchorage' => '(GMT-09:00) Alaska',
			'America/Los_Angeles' => '(GMT-08:00) Pacific Time (US &amp; Canada)',
			'America/Tijuana' => '(GMT-08:00) Tijuana, Baja California',
			'America/Denver' => '(GMT-07:00) Mountain Time (US &amp; Canada)',
			'America/Chihuahua' => '(GMT-07:00) Chihuahua',
			'America/Mazatlan' => '(GMT-07:00) Mazatlan',
			'America/Phoenix' => '(GMT-07:00) Arizona',
			'America/Regina' => '(GMT-06:00) Saskatchewan',
			'America/Tegucigalpa' => '(GMT-06:00) Central America',
			'America/Chicago' => '(GMT-06:00) Central Time (US &amp; Canada)',
			'America/Mexico_City' => '(GMT-06:00) Mexico City',
			'America/Monterrey' => '(GMT-06:00) Monterrey',
			'America/New_York' => '(GMT-05:00) Eastern Time (US &amp; Canada)',
			'America/Bogota' => '(GMT-05:00) Bogota',
			'America/Lima' => '(GMT-05:00) Lima',
			'America/Rio_Branco' => '(GMT-05:00) Rio Branco',
			'America/Indiana/Indianapolis' => '(GMT-05:00) Indiana (East)',
			'America/Caracas' => '(GMT-04:30) Caracas',
			'America/Halifax' => '(GMT-04:00) Atlantic Time (Canada)',
			'America/Manaus' => '(GMT-04:00) Manaus',
			'America/Santiago' => '(GMT-04:00) Santiago',
			'America/La_Paz' => '(GMT-04:00) La Paz',
			'America/St_Johns' => '(GMT-03:30) Newfoundland',
			'America/Argentina/Buenos_Aires' => '(GMT-03:00) Georgetown',
			'America/Sao_Paulo' => '(GMT-03:00) Brasilia',
			'America/Godthab' => '(GMT-03:00) Greenland',
			'America/Montevideo' => '(GMT-03:00) Montevideo',
			'Atlantic/South_Georgia' => '(GMT-02:00) Mid-Atlantic',
			'Atlantic/Azores' => '(GMT-01:00) Azores',
			'Atlantic/Cape_Verde' => '(GMT-01:00) Cape Verde Is.',
			'Europe/Dublin' => '(GMT) Dublin',
			'Europe/Lisbon' => '(GMT) Lisbon',
			'Europe/London' => '(GMT) London',
			'Africa/Monrovia' => '(GMT) Monrovia',
			'Atlantic/Reykjavik' => '(GMT) Reykjavik',
			'Africa/Casablanca' => '(GMT) Casablanca',
			'Europe/Belgrade' => '(GMT+01:00) Belgrade',
			'Europe/Bratislava' => '(GMT+01:00) Bratislava',
			'Europe/Budapest' => '(GMT+01:00) Budapest',
			'Europe/Ljubljana' => '(GMT+01:00) Ljubljana',
			'Europe/Prague' => '(GMT+01:00) Prague',
			'Europe/Sarajevo' => '(GMT+01:00) Sarajevo',
			'Europe/Skopje' => '(GMT+01:00) Skopje',
			'Europe/Warsaw' => '(GMT+01:00) Warsaw',
			'Europe/Zagreb' => '(GMT+01:00) Zagreb',
			'Europe/Brussels' => '(GMT+01:00) Brussels',
			'Europe/Copenhagen' => '(GMT+01:00) Copenhagen',
			'Europe/Madrid' => '(GMT+01:00) Madrid',
			'Europe/Paris' => '(GMT+01:00) Paris',
			'Africa/Algiers' => '(GMT+01:00) West Central Africa',
			'Europe/Amsterdam' => '(GMT+01:00) Amsterdam',
			'Europe/Berlin' => '(GMT+01:00) Berlin',
			'Europe/Rome' => '(GMT+01:00) Rome',
			'Europe/Stockholm' => '(GMT+01:00) Stockholm',
			'Europe/Vienna' => '(GMT+01:00) Vienna',
			'Europe/Minsk' => '(GMT+02:00) Minsk',
			'Africa/Cairo' => '(GMT+02:00) Cairo',
			'Europe/Helsinki' => '(GMT+02:00) Helsinki',
			'Europe/Riga' => '(GMT+02:00) Riga',
			'Europe/Sofia' => '(GMT+02:00) Sofia',
			'Europe/Tallinn' => '(GMT+02:00) Tallinn',
			'Europe/Vilnius' => '(GMT+02:00) Vilnius',
			'Europe/Athens' => '(GMT+02:00) Athens',
			'Europe/Bucharest' => '(GMT+02:00) Bucharest',
			'Europe/Istanbul' => '(GMT+02:00) Istanbul',
			'Asia/Jerusalem' => '(GMT+02:00) Jerusalem',
			'Asia/Amman' => '(GMT+02:00) Amman',
			'Asia/Beirut' => '(GMT+02:00) Beirut',
			'Africa/Windhoek' => '(GMT+02:00) Windhoek',
			'Africa/Harare' => '(GMT+02:00) Harare',
			'Asia/Kuwait' => '(GMT+03:00) Kuwait',
			'Asia/Riyadh' => '(GMT+03:00) Riyadh',
			'Asia/Baghdad' => '(GMT+03:00) Baghdad',
			'Africa/Nairobi' => '(GMT+03:00) Nairobi',
			'Asia/Tbilisi' => '(GMT+03:00) Tbilisi',
			'Europe/Moscow' => '(GMT+03:00) Moscow',
			'Europe/Volgograd' => '(GMT+03:00) Volgograd',
			'Asia/Tehran' => '(GMT+03:30) Tehran',
			'Asia/Muscat' => '(GMT+04:00) Muscat',
			'Asia/Baku' => '(GMT+04:00) Baku',
			'Asia/Yerevan' => '(GMT+04:00) Yerevan',
			'Asia/Yekaterinburg' => '(GMT+05:00) Ekaterinburg',
			'Asia/Karachi' => '(GMT+05:00) Karachi',
			'Asia/Tashkent' => '(GMT+05:00) Tashkent',
			'Asia/Kolkata' => '(GMT+05:30) Calcutta',
			'Asia/Colombo' => '(GMT+05:30) Sri Jayawardenepura',
			'Asia/Katmandu' => '(GMT+05:45) Kathmandu',
			'Asia/Dhaka' => '(GMT+06:00) Dhaka',
			'Asia/Almaty' => '(GMT+06:00) Almaty',
			'Asia/Novosibirsk' => '(GMT+06:00) Novosibirsk',
			'Asia/Rangoon' => '(GMT+06:30) Yangon (Rangoon)',
			'Asia/Krasnoyarsk' => '(GMT+07:00) Krasnoyarsk',
			'Asia/Bangkok' => '(GMT+07:00) Bangkok',
			'Asia/Jakarta' => '(GMT+07:00) Jakarta',
			'Asia/Brunei' => '(GMT+08:00) Beijing',
			'Asia/Chongqing' => '(GMT+08:00) Chongqing',
			'Asia/Hong_Kong' => '(GMT+08:00) Hong Kong',
			'Asia/Urumqi' => '(GMT+08:00) Urumqi',
			'Asia/Irkutsk' => '(GMT+08:00) Irkutsk',
			'Asia/Ulaanbaatar' => '(GMT+08:00) Ulaan Bataar',
			'Asia/Kuala_Lumpur' => '(GMT+08:00) Kuala Lumpur',
			'Asia/Singapore' => '(GMT+08:00) Singapore',
			'Asia/Taipei' => '(GMT+08:00) Taipei',
			'Australia/Perth' => '(GMT+08:00) Perth',
			'Asia/Seoul' => '(GMT+09:00) Seoul',
			'Asia/Tokyo' => '(GMT+09:00) Tokyo',
			'Asia/Yakutsk' => '(GMT+09:00) Yakutsk',
			'Australia/Darwin' => '(GMT+09:30) Darwin',
			'Australia/Adelaide' => '(GMT+09:30) Adelaide',
			'Australia/Canberra' => '(GMT+10:00) Canberra',
			'Australia/Melbourne' => '(GMT+10:00) Melbourne',
			'Australia/Sydney' => '(GMT+10:00) Sydney',
			'Australia/Brisbane' => '(GMT+10:00) Brisbane',
			'Australia/Hobart' => '(GMT+10:00) Hobart',
			'Asia/Vladivostok' => '(GMT+10:00) Vladivostok',
			'Pacific/Guam' => '(GMT+10:00) Guam',
			'Pacific/Port_Moresby' => '(GMT+10:00) Port Moresby',
			'Asia/Magadan' => '(GMT+11:00) Magadan',
			'Pacific/Fiji' => '(GMT+12:00) Fiji',
			'Asia/Kamchatka' => '(GMT+12:00) Kamchatka',
			'Pacific/Auckland' => '(GMT+12:00) Auckland',
			'Pacific/Tongatapu' => '(GMT+13:00) Nukualofa');
	}
	private function validate() {

		if (!$this->user->hasPermission('modify', 'extension/module/wk_preorder_pro')) {
			$this->error['error_warning'] = $this->language->get('error_permission');
		}

		if (!isset($this->request->post['module_wk_preorder_pro_status']) || !in_array($this->request->post['module_wk_preorder_pro_status'], array('0','1'))) {
			$this->error['error_invalid_status'] = $this->data['error_status'];
		}
		if (!isset($this->request->post['module_wk_preorder_pro_discount']) || !in_array($this->request->post['module_wk_preorder_pro_discount'], array('0','1'))) {
			$this->error['error_discount'] = $this->data['error_status'];
		}
		if (!isset($this->request->post['module_wk_preorder_pro_discount_type']) || !in_array($this->request->post['module_wk_preorder_pro_discount_type'], array('0','1'))) {
			$this->error['error_discount_type'] = $this->data['error_status'];
		}
		if (!isset($this->request->post['module_wk_preorder_pro_mail_status']) || !in_array($this->request->post['module_wk_preorder_pro_mail_status'], array('0','1'))) {
			$this->error['error_mail'] = $this->data['error_status'];
		}

		if (!isset($this->request->post['module_wk_preorder_pro_notification_mode']) || !in_array($this->request->post['module_wk_preorder_pro_notification_mode'], array('1','2','3'))) {
			$this->error['error_mode'] = $this->data['error_status'];
		}

		if (!isset($this->request->post['module_wk_preorder_pro_convert_preorder']) || !in_array($this->request->post['module_wk_preorder_pro_convert_preorder'], array('1','2'))) {
			$this->error['error_convert'] = $this->data['error_status'];
		}
		
		if (!isset($this->request->post['module_wk_preorder_pro_notification_status']) || !in_array($this->request->post['module_wk_preorder_pro_notification_status'], array('5','6', '7','8'))) {
			$this->error['error_notification'] = $this->data['error_status'];
		}
		if (!isset($this->request->post['module_wk_preorder_pro_stock_status']) || !in_array($this->request->post['module_wk_preorder_pro_stock_status'], array('5','6', '7','8'))) {
			$this->error['error_stock'] = $this->data['error_status'];
		}
		
		if (!isset($this->request->post['module_wk_preorder_pro_timezone']) || !(array_key_exists($this->request->post['module_wk_preorder_pro_timezone'], $this->data['store_timezone']))) {
			$this->error['error_timezone'] = $this->data['error_time_zone'];
		}

		if (!isset($this->request->post['module_wk_preorder_pro_order_status'])) {
			$this->error['error_order_status'] = $this->data['error_status'];
		} else if (isset($this->data['order_statuses']) && !empty($this->data['order_statuses'])) {
			foreach ($this->data['order_statuses'] as $key => $value) {
				$order_statuses[] =  $value['order_status_id'];
			}
			if (!in_array($this->request->post['module_wk_preorder_pro_order_status'], $order_statuses)) {
				$this->error['error_order_status'] = $this->data['error_status'];
			}
		}

		foreach($this->data['languages'] as $language) {

			if (!isset($this->request->post['module_wk_preorder_pro_text'])) {
				$this->error['error_warning'] = $this->data['error_form_general'];
 			} else {
				 if (!isset($this->request->post['module_wk_preorder_pro_text'][$language['language_id']]) || strlen(trim($this->request->post['module_wk_preorder_pro_text'][$language['language_id']])) < 3 || strlen(trim($this->request->post['module_wk_preorder_pro_text'][$language['language_id']])) > 10) {
					$this->error['error_preorder_text'][$language['language_id']] = $this->data['error_text'];
				 }
			 }

			if (!isset($this->request->post['module_wk_preorder_pro_preordertext'])) {
				$this->error['error_warning'] = $this->data['error_form_general'];
 			} else {
				 if (!isset($this->request->post['module_wk_preorder_pro_preordertext'][$language['language_id']]) || strlen(trim($this->request->post['module_wk_preorder_pro_preordertext'][$language['language_id']])) > 100) {
					$this->error['error_preordertext'][$language['language_id']] = $this->data['error_order_text'];
				}
			}

			if (!isset($this->request->post['module_wk_preorder_pro_mail_admin_message'])) {
				$this->error['error_warning'] = $this->data['error_form_general'];
 			} else {
				 if (!isset($this->request->post['module_wk_preorder_pro_mail_admin_message'][$language['language_id']]) || strlen(trim($this->request->post['module_wk_preorder_pro_mail_admin_message'][$language['language_id']])) > 255) {
					$this->error['error_admin_msg'][$language['language_id']] = $this->data['error_admin_text'];
				}
			}
		}
		if(isset($this->request->post['module_wk_preorder_pro_site_key']) && strlen(trim($this->request->post['module_wk_preorder_pro_site_key'])) < 15) {
			$this->error['error_warning_site_key'] = $this->data['error_site_key'];
		}
		if(isset($this->request->post['module_wk_preorder_pro_secret_key']) && strlen(trim($this->request->post['module_wk_preorder_pro_secret_key'])) < 15) {
			$this->error['error_warning_secret_key'] = $this->data['error_secret_key'];
		}
		
		if (!isset($this->request->post['module_wk_preorder_pro_preorder_days']) || !$this->request->post['module_wk_preorder_pro_preorder_days'] || $this->request->post['module_wk_preorder_pro_preorder_days'] < 0 || !is_numeric($this->request->post['module_wk_preorder_pro_preorder_days']) || $this->request->post['module_wk_preorder_pro_preorder_days'] > 500) {
			$this->error['error_invalid_days'] = $this->data['error_days'];
		}
		$mails = array();
		if ($this->data['mails']) {
			foreach($this->data['mails'] as $mail) {
				$mails[] = $mail['id'];
			}
		}
		if (!isset($this->request->post['module_wk_preorder_pro_mail_notify_customer']) ) {
			$this->error['error_warning'] = $this->data['error_form_general'];
		} else if ((!$mails && $this->request->post['module_wk_preorder_pro_mail_notify_customer']) || $mails && $this->request->post['module_wk_preorder_pro_mail_notify_customer'] && !in_array($this->request->post['module_wk_preorder_pro_mail_notify_customer'], $mails)) {
			$this->error['error_customer_mail'] = $this->data['error_status'];
		}
		$arr_nums = array(
			'module_wk_preorder_pro_quantity',
			'module_wk_preorder_pro_order_quantity',
			'module_wk_preorder_pro_customer_order',
		);
		foreach ($arr_nums as $value) {
			if (!isset($this->request->post[$value]) || $this->request->post[$value]  <= 0|| !is_numeric($this->request->post[$value]) || $this->request->post[$value] > 500) {
				$this->error['error_' . $value] = $this->data['error_quantity'];
			}
		}

		if ($this->error && !isset($this->error['error_warning'])) {
			$this->error['error_warning'] = $this->data['error_form_general'];
		}
	 	return !$this->error;

	}

	public function addToken() {
		$json = array();
		if (isset($this->request->post['currentuser_token']) && $this->request->post['currentuser_token']) {
			$this->model_extension_module_wk_preorder_pro->addToken($this->request->post['currentuser_token']);
			$json['success'] = true;
		} else {

		}
		
	}
}

?>
