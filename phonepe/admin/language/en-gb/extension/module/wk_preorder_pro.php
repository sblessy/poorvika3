<?php
#####################################################################
#  Preorder Pro Opencart 2.x.x.x From Webkul  http://webkul.com 	#
#####################################################################

// Heading Goes here:
$_['heading_title']      	                   = 'Pre-Order Pro';

// Text
$_['text_enable']     	 	                   = 'Enable';
$_['text_disable']     	 	                   = 'Disable';
$_['text_redirect']     	                   = 'Redirect to Contact';
$_['text_preorder_cart']                       = 'Add to Pre-Order Cart';
$_['text_to_cart']     	 	                   = 'Add to Cart';
$_['text_site_key']                            = 'Google recaptcha site key';
$_['text_secret_key']   	                   = 'Google recaptcha secret key';
$_['text_menu_preorder']     	 	           = 'Pre-Order';
$_['text_menu_preorders']     	               = 'Pre-Orders';
$_['text_menu_inquiry']                        = 'Pre-Order Enquiries';
$_['text_menu_productlist']     	 	       = 'Pre-Order Product List';
$_['text_menu_mails']     	 	               = 'Mails';
$_['text_general']                             = 'General';
$_['text_add_product']     	 	               = 'Add Product';
$_['text_module']     	 	                   = 'Modules';
$_['text_config']                              = 'Configuration';
$_['text_success']    	 	                   = 'Success: You have modified module Pre-Order Pro Module !';
$_['text_welcome']  	  	                   = ' Under the general tab, settings are universal for all pre-order products that are created by this module and from add product tab, you can add products for pre-order.';
$_['text_time_zone']                           = 'Store Timezone';
$_['text_preorder']	  		                   = 'Pre Order Text';
$_['text_preorder_info']   	                   = 'Entered text will be your Add to cart button text.';
$_['text_preorder_color'] 	                   = 'Button Color';
$_['text_preorder_color_info']                 = 'This selected color will be your Add to Cart button color if product is out of stock or you can left it blank.';
$_['text_status']                              = 'Status';
$_['text_preorder_link']                       = 'Pre Order button Work';
$_['text_preorder_link_info']                  = 'Select the action which will be taken when click on the add to cart button. This is universal setting for all the products.';
$_['text_add_preorder_product']                = 'Add product';
$_['text_remove_preorder_product_info']        = 'Select products which you want to remove from pre-order.';
$_['text_remove_preorder_product']             = 'Remove Pre order product';
$_['text_add_preorder_product_info']           = 'Select products which you want to become pre-order.';
$_['text_product_notification_status'] 	       = 'Notification Status';
$_['text_product_notification_status_info']    = 'Please set status when automatic notification will be sent to buyer ';
$_['text_product_stock_status'] 	           = 'Product Stock Status';
$_['text_product_stock_status_info']           = 'Stock status which will be applied when product is being set as pre-order product';
$_['text_deduction_method'] 	               = 'Select Pre-Order mode';
$_['text_deduction_method_info']               = 'To set what kind of pre-order will be for current products';
$_['text_as_per'] 	                           = 'Allow pre order';
$_['text_as_per_info']                         = 'Select on which pre order will be applied';
$_['text_notification_mode'] 	               = 'Notification Mode';
$_['text_notification_mode_help'] 	           = 'Select what would be the preferred mode of notification which will be sent to the customer about pre-order availability';
$_['text_preorder_email']                      = 'Pre-Order email';
$_['text_preorder_email_help']                 = 'Set enable if you want pre-order email other than opencart update email else disable';
$_['text_preorder_text']                       = 'Pre-Order message';
$_['text_preorder_text_help']                  = 'Enter text that you want to display on product page';
$_['text_auto'] 	                           = 'Automatic';
$_['text_manually'] 	                       = 'Manually';
$_['text_both'] 	                           = 'Both';
$_['text_percentage'] 	                       = 'Percentage';
$_['text_quantity'] 	                       = 'On quantity';
$_['text_available_date']                      = 'On available date';
$_['text_processing']                          = 'Processing...';
$_['text_apply']                               = 'Apply';
$_['text_applied']                             = 'Applied';
$_['text_convert_product']                     = 'Convert to Pre-Order';
$_['help_convert_product']                     = 'Convert the Out Of Stock Products to Pre-Order';
$_['text_order_days']                          = 'No. of Days';
$_['help_order_days']                          = 'Default No. of Days for Pre-Orders Produts';
$_['text_preorder_quantity'] 	               = 'Pre-Order Quantity';
$_['text_quantity_per_order'] 	               = 'Quantity per order';
$_['text_order_per_customer'] 	               = 'Order per customer';
$_['text_preorder_quantity_info'] 	           = 'How many total quantity of selected product will be on for pre-order';
$_['text_quantity_per_order_info'] 	           = 'How many product quantity can contain single order';
$_['text_order_per_customer_info'] 	           = 'How many order(s) a customer can place';
$_['text_server_key']                           = 'Server Key';
$_['text_server_key_info']                      = 'Server Key for Push Notification';
$_['text_messenger_id']                        = 'Messenger ID';
$_['text_messenger_id_info']                   = 'Messenger ID for Push Notification';
$_['entry_order_status']                       = 'Order Status for Pre-Ordered Products';
$_['text_discount']                            = 'Discount On Pre-Order Products';
$_['text_discount_type']                       = 'Discount Type';
$_['text_fixed']                               = 'Fixed';
$_['text_per']                                 = 'Percentage';

// Error
$_['error_form_general']    	               = 'Warning: Please check the form carefully!';
$_['error_site_key']  	                       = 'Google recaptcha site key is required field!';
$_['error_secret_key']  	                   = 'Google recaptcha secret key is required field!';
$_['error_permission']  	                   = ' Warning: You do not have permission to modify module Preorder Pro Module !!';
$_['percentage_warning']  	                   = ' Warning: As you are selecting percentage mode of pre-order then you make it sure that products do not have any kind of deduction except shipping!';
$_['error_no_product_id']  	                   = 'Error: No product id is supplied!';
$_['error_required']                           = 'Warning: The field is required!';
$_['error_text']                               = 'Warning: Pre order text should be 3 to 10 char!';
$_['error_admin_text']                         = 'Warning: The Message must be less than 255 characters!';
$_['error_status']                             = 'Warning: Please select the valid option!';
$_['error_days']                               = 'Warning: Pre Order Days are required and must be between 1 and 60!';
$_['error_quantity']                           = 'Warning: Pre Order Days are required and must be between 1 and 500!';
$_['error_order_text']                         = 'Warning: Pre order text should be 3 to 100 char!';
//Mail
$_['text_info_mail']	 	 			       = 'Under this tab you can set Mail for different conditions which will occur in pre-order.';

// Entry
$_['entry_status']      	                   = 'Status :';
$_['entry_mail_keywords']  				       = 'Mail Keywords';
$_['entry_order_mail_to_admin']                = 'Order mail to admin';
$_['entry_order_mail_to_admin_info']  	       = 'Select mail, which will send to admin after create order';
$_['entry_order_mail_to_customer']             = 'Order mail to customer';
$_['entry_order_mail_to_customer_info']  	   = 'Select mail, which will send to customer after create order';
$_['entry_notification_mail_to_admin']         = 'Notification mail to admin';
$_['entry_notification_mail_to_admin_info']    = 'Select mail, which will send to admin after notify customer about product availability';
$_['entry_notification_mail_to_customer']      = 'Notification mail to customer';
$_['entry_notification_mail_to_customer_info'] = 'Select mail, which will send to customer after notify customer about product availability';
$_['entry_mail_admin_message']                 = 'Admin Message';
$_['entry_mail_admin_message_help']            = 'This message send to customer on pre order with order mail';
