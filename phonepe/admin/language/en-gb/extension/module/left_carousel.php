<?php
// Heading
$_['heading_title']    = 'Left Crousel';

// Text
$_['text_home']      = 'Home';
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified left crousel module!';
$_['text_edit']        = 'Edit Left Crousel Module';
$_['text_enabled']        = 'Enabled';
$_['text_disabled']        = 'Disabled';
$_['button_save']        = 'Save';
$_['button_cancel']        = 'Cancel';
// Entry
$_['entry_name']       = 'Module Name';
$_['entry_product']    = 'Products';
$_['entry_limit']      = 'Limit';
$_['entry_width']      = 'Width';
$_['entry_height']     = 'Height';
$_['entry_status']     = 'Status';

// Help
$_['help_product']     = '(Autocomplete)';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify left crousel module!';
$_['error_name']       = 'Module Name must be between 3 and 64 characters!';
$_['error_width']      = 'Width required!';
$_['error_height']     = 'Height required!';