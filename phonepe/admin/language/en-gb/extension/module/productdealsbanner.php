<?php
// Heading
$_['heading_title']    = 'Product Deals Banner';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Success: You have modified banner module!';
$_['text_edit']        = 'Edit Banner Module';
$_['text_enabled']    =  'Enabled';
$_['text_disabled']    =  'Disabled';
// Entry
$_['entry_name']       = 'Module Name';
$_['entry_banner']     = 'Banner';
$_['entry_dimension']  = 'Dimension (W x H) and Resize Type';
$_['entry_width']      = 'Width';
$_['entry_height']     = 'Height';
$_['entry_status']     = 'Status';
$_['button_save']     = 'Save';
$_['button_cancel']     = 'Cancel';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify banner module!';
$_['error_name']       = 'Module Name must be between 3 and 64 characters!';
$_['error_width']      = 'Width required!';
$_['error_height']     = 'Height required!';