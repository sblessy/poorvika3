<?php
// Heading
$_['heading_title']    = 'Whatsapp';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Success: You have modified whatsapp module!';
$_['text_edit']        = 'Edit whatsapp Module';
$_['text_yes']         = 'Yes';
$_['text_no']          = 'No';

// Entry
$_['entry_name']       = 'Name';
$_['entry_status']     = 'Status';
$_['entry_share']      = 'Whatsapp Share';
$_['entry_Chat']       = 'Whatsapp Chat';
$_['entry_cphone']     = 'Whatsapp Chat Mobile Number';
$_['entry_chatmsg']    = 'Whatsapp Chat Message';
$_['entry_order']      = 'Order Via Whatsapp';
$_['entry_ophone']     = 'Order Mobile Number';
$_['entry_ordermsg']   = 'Order Message';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify whatsapp module!';
