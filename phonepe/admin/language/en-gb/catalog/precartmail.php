<?php
################################################################################################
#  Preorder Pro Opencart 2.x.x.x From Webkul  http://webkul.com 	#
################################################################################################

//user
$_['subject']    			  = 'Subject';
$_['text_hello']    		  = 'Hello ';
$_['text_subject']    	 	  = 'Your Pre Order Products is available Now. ';

$_['text_pname']    		  = 'Product Name ';
$_['text_pqty']		    	  = 'Product Quantity';
$_['text_pimage']	    	  = 'Product Image';
$_['text_pmodel']	    	  = 'Product Model';
$_['text_thanksadmin']    	  = 'Thank you, Admin';
$_['query']    			  	  = 'Your Query';
$_['name']    			  	  = 'Customer Name ';
$_['email']    			      = 'Email ';


// Entry
$_['entry_name']      	      = 'Name';
$_['button_back']    	      = 'Back';
$_['button_save']    	      = 'Save';
$_['button_cancel']    	      = 'Cancel';
$_['button_delete']    	      = 'Delete';
$_['text_edit']    	          = 'Edit';