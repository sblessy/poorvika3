<?php
#####################################################################
#  Preorder Pro Opencart 2.x.x.x From Webkul  http://webkul.com 	#
#####################################################################
class ModelExtensionModulewkPreorderPro extends Model {

	public function getEnquiry($id) {
		$sql = "SELECT * FROM ".DB_PREFIX."preorder_enquiry WHERE enquiry_id = '".(int)$id."' ";
		$result = $this->db->query($sql)->row;
		if($result) {
			return $result;
		} else {
			return false;
		}
	}

	public function addThread($data) {
		
		$sql = "INSERT INTO ".DB_PREFIX."preorder_enquiry_threads SET enquiry_id = '".(int)$data['enquiry_id']."', posted_by = '0', query = '".$this->db->escape($data['query'])."', date_added = NOW(), status = '".(int)$data['status']."' ";
		$this->db->query($sql);
		$this->db->query("UPDATE ".DB_PREFIX."preorder_enquiry SET status = '".(int)$data['status']."' WHERE enquiry_id = '" . (int)$data['enquiry_id'] . "'");
		return true;
	}

	public function removeThread($thread_id) {
		$this->db->query("DELETE FROM ".DB_PREFIX."preorder_enquiry_threads WHERE thread_id = '".(int)$thread_id."' ");
		return true;
	}

	public function getEnquiryThreads($enquiry_id, $customer_id) {
		$sql = "SELECT * FROM ".DB_PREFIX."preorder_enquiry_threads WHERE enquiry_id = '".(int)$enquiry_id."' AND ( posted_by = '".(int)$customer_id."' || posted_by = '0' ) ";
		$result = $this->db->query($sql)->rows;
		if($result) {
			return $result;
		} else {
			return false;
		}
	}

	public function getEnquiries($filter_data){
		$sql = "SELECT pe.enquiry_id,pe.product_id,pe.email, pe.subject,pd.name, pe.customer_name, pe.status, (SELECT COUNT(thread_id) FROM ".DB_PREFIX."preorder_enquiry_threads pet WHERE pet.enquiry_id = pe.enquiry_id ) as total_threads FROM ".DB_PREFIX."preorder_enquiry pe LEFT JOIN ".DB_PREFIX."product_description pd ON (pd.product_id=pe.product_id) LEFT JOIN ".DB_PREFIX."customer c ON (c.customer_id=pe.customer_id) WHERE pd.language_id = '".(int)$this->config->get('config_language_id')."' ";

		if(isset($filter_data['filter_name']) && $filter_data['filter_name']) {
			$sql .= " AND pe.customer_name like '".$this->db->escape($filter_data['filter_name'])."%' ";
		}

		if(isset($filter_data['filter_email']) && $filter_data['filter_email']) {
			$sql .= " AND pe.email like '".$this->db->escape($filter_data['filter_email'])."%' ";
		}

		if(isset($filter_data['filter_subject']) && $filter_data['filter_subject']) {
			$sql .= " AND pe.subject like '".$this->db->escape($filter_data['filter_subject'])."%' ";
		}

		if(isset($filter_data['filter_product_name']) && $filter_data['filter_product_name']) {
			$sql .= " AND pd.name like '".$this->db->escape($filter_data['filter_product_name'])."%' ";
		}

		if(isset($filter_data['filter_status'])) {
			$sql .= " AND pe.status = '". $filter_data['filter_status'] . "' ";
		}

		$sort_data = array(
			'c.firstname',
			'pe.email',
			'pe.subject',
			'pd.name',
			'pe.status',
		);

		if (isset($filter_data['sort']) && in_array($filter_data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $filter_data['sort'];
		} else {
			$sql .= " ORDER BY pe.enquiry_id ";
		}

		if (isset($filter_data['order']) && ($filter_data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($filter_data['start']) || isset($filter_data['limit'])) {
			if ($filter_data['start'] < 0) {
				$filter_data['start'] = 0;
			}

			if ($filter_data['limit'] < 1) {
				$filter_data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$filter_data['start'] . "," . (int)$filter_data['limit'];
		}

		$result = $this->db->query($sql)->rows;

		if($result) {
			return $result;
		} else {
			return false;
		}

	}

	public function getEnquiriesTotal($filter_data){
		$sql = "SELECT * FROM ".DB_PREFIX."preorder_enquiry pe LEFT JOIN ".DB_PREFIX."product_description pd ON (pd.product_id=pe.product_id) WHERE pd.language_id = '".(int)$this->config->get('config_language_id')."' ";

		if(isset($filter_data['filter_name']) && $filter_data['filter_name']) {
			$sql .= " AND pe.customer_name like '%".$this->db->escape($filter_data['filter_name'])."%' ";
		}

		if(isset($filter_data['filter_email']) && $filter_data['filter_email']) {
			$sql .= " AND pe.email like '%".$this->db->escape($filter_data['filter_email'])."%' ";
		}

		if(isset($filter_data['filter_subject']) && $filter_data['filter_subject']) {
			$sql .= " AND pe.subject like '%".$this->db->escape($filter_data['filter_subject'])."%' ";
		}

		if(isset($filter_data['filter_product_name]']) && $filter_data['filter_product_name]']) {
			$sql .= " AND pd.name like '%".$this->db->escape($filter_data['filter_product_name]'])."%' ";
		}

		if(isset($filter_data['filter_status'])) {
			$sql .= " AND pe.status = '". $filter_data['filter_status']."' ";
		}
		$result = $this->db->query($sql)->rows;
		if($result) {
			return count($result);
		} else {
			return false;
		}
	}

	public function getPreOrderProductDetails($id) {
		$sql = "SELECT * FROM ".DB_PREFIX."preorder_products pp LEFT JOIN ".DB_PREFIX."product_description pd ON (pd.product_id=pp.product_id) WHERE pd.language_id = '".(int)$this->config->get('config_language_id')."' AND pp.id = '".(int)$id."' ";

		$result = $this->db->query($sql)->row;
		if($result) {
			return $result;
		} else {
			return false;
		}
	}

	public function getPreOrderProductExists($product_id) {
		$sql = "SELECT pp.product_id FROM ".DB_PREFIX."preorder_products pp LEFT JOIN ".DB_PREFIX."product_description pd ON (pd.product_id=pp.product_id) WHERE pd.language_id = '".(int)$this->config->get('config_language_id')."' AND pp.product_id = '".(int)$product_id."' AND pp.status = 1 ";

		$result = $this->db->query($sql)->row;
		if($result) {
			return $result;
		} else {
			return false;
		}
	}

	public function get_total_productcount($filter_data) {
		// $sql = "SELECT ppd.id,ppd.order_id,ppd.notified,ppd.product_id,ppd.status,c.firstname,c.lastname,c.email,oo.total,pd.name FROM `".DB_PREFIX."order` oo LEFT JOIN   `".DB_PREFIX."preordred_product_detail` ppd ON (oo.order_id=ppd.order_id) LEFT JOIN `".DB_PREFIX."customer` LEFT JOIN `".DB_PREFIX."product_description`` pd ON pd.product_id=ppd.product_id WHERE pd.language_id='".$this->config->get('config_language_id')."'";
		$sql = "SELECT ppd.id,ppd.order_id,ppd.notified,ppd.product_id,ppd.status,c.firstname,c.lastname,c.email,oo.total,pd.name FROM `".DB_PREFIX."preordred_product_detail` ppd LEFT JOIN `".DB_PREFIX."customer` c ON c.customer_id=ppd.customer_id LEFT JOIN `" . DB_PREFIX . "order` oo ON oo.order_id=ppd.order_id LEFT JOIN `".DB_PREFIX."product_description` pd ON pd.product_id=ppd.product_id WHERE pd.language_id='".$this->config->get('config_language_id')."'";

		if(!empty($filter_data['pname'])) {
			$sql .= " AND LCASE(pd.name) like '" . $this->db->escape(utf8_strtolower($filter_data['pname']))."%' ";
		}

		if(!empty($filter_data['name'])) {
			$sql .= " AND ( LCASE(c.firstname) like '" . $this->db->escape(utf8_strtolower($filter_data['name'])) . "%' || LCASE(c.firstname) like '" . $this->db->escape(utf8_strtolower($filter_data['name'])) . "%' || LCASE(CONCAT(c.firstname,' ',c.lastname)) like '" . $this->db->escape(utf8_strtolower($filter_data['name']))."%' )";
		}

		if(!empty($filter_data['customer_mail'])) {
			$sql .= " AND LCASE(c.email) like '" . $this->db->escape(utf8_strtolower($filter_data['customer_mail']))."%' ";
		}

		if(!empty($filter_data['notified'])) {
			if($filter_data['notified'] == 'yes'){
				$sql .= " AND ppd.notified = '1' ";
			}else if($filter_data['notified'] == 'no') {
				$sql .= " AND ppd.notified = '0' ";
			}
		}

		if(!empty($filter_data['status'])) {
			if($filter_data['status'] == 'yes'){
				$sql .= " AND ppd.status = '0' ";
			}else if($filter_data['status'] == 'no') {
				$sql .= " AND ppd.status = '1' ";
			}
		}

		$result = $this->db->query($sql)->rows;
		return count($result);
	}

	public function get_Preordered_list($filter_data){
		$sql = "SELECT ppd.id,ppd.order_id,ppd.notified,ppd.product_id,ppd.status,c.firstname,c.lastname,c.email,oo.total,pd.name FROM `".DB_PREFIX."preordred_product_detail` ppd LEFT JOIN `".DB_PREFIX."customer` c ON c.customer_id=ppd.customer_id LEFT JOIN `".DB_PREFIX."order` oo ON oo.order_id=ppd.order_id LEFT JOIN `".DB_PREFIX."product_description` pd ON pd.product_id=ppd.product_id WHERE pd.language_id='".$this->config->get('config_language_id')."' ";

		if(!empty($filter_data['pname'])) {
			$sql .= " AND LCASE(pd.name) like '" . $this->db->escape(utf8_strtolower($filter_data['pname']))."%' ";
		}

		if(!empty($filter_data['name'])) {
			$sql .= " AND ( LCASE(c.firstname) like '" . $this->db->escape(utf8_strtolower($filter_data['name'])) . "%' || LCASE(c.firstname) like '" . $this->db->escape(utf8_strtolower($filter_data['name'])) . "%' || LCASE(CONCAT(c.firstname,' ',c.lastname)) like '" . $this->db->escape(utf8_strtolower($filter_data['name']))."%' )";
		}

		if(!empty($filter_data['customer_mail'])) {
			$sql .= " AND LCASE(c.email) like '" . $this->db->escape(utf8_strtolower($filter_data['customer_mail']))."%' ";
		}

		if(!empty($filter_data['notified'])) {
			if($filter_data['notified'] == 'yes'){
				$sql .= " AND ppd.notified = '1' ";
			}else if($filter_data['notified'] == 'no') {
				$sql .= " AND ppd.notified = '0' ";
			}
		}

		if(!empty($filter_data['status'])) {
			if($filter_data['status'] == 'yes'){
				$sql .= " AND ppd.status = '1' ";
			}else if($filter_data['status'] == 'no') {
				$sql .= " AND ppd.status = '0' ";
			}
		}

		$sort_data = array(
			'ppd.order_id',
			'pd.name',
			'c.firstname',
			'c.email',
			'oo.total',
			'ppd.status',
			'ppd.notified',
		);

		if (isset($filter_data['sort']) && in_array($filter_data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $filter_data['sort'];
		} else {
			$sql .= " ORDER BY ppd.order_id ";
		}

		if (isset($filter_data['order']) && ($filter_data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($filter_data['start']) || isset($filter_data['limit'])) {
			if ($filter_data['start'] < 0) {
				$filter_data['start'] = 0;
			}

			if ($filter_data['limit'] < 1) {
				$filter_data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$filter_data['start'] . "," . (int)$filter_data['limit'];
		}

		$result = $this->db->query($sql)->rows;

		return $result;
	}

	public function delete_preorder_productct($id){
		$result = $this->db->query("SELECT product_id,base_price,cur_available_date,cur_quantity FROM `".DB_PREFIX."preorder_products` WHERE id = '".(int)$id."' ")->row;

		$this->db->query("UPDATE `".DB_PREFIX."product` SET price = '".(float)$result['base_price']."', quantity = '".(int)$result['cur_quantity']."', date_available = '". $this->db->escape($result['cur_available_date'])."' , stock_status_id = '5', subtract = '1' WHERE product_id = '".$result['product_id']."'");

		$this->db->query("DELETE FROM `".DB_PREFIX."preorder_products` WHERE id = '".(int)$id."' ");

	}

	public function getTotalProducts($filter_data) {

		$sql = "SELECT COUNT(*) AS total FROM `".DB_PREFIX."preorder_products` pp LEFT JOIN `".DB_PREFIX."product` p ON p.product_id = pp.product_id LEFT JOIN `".DB_PREFIX."product_description` pd ON pp.product_id = pd.product_id WHERE pd.language_id='".$this->config->get('config_language_id')."' ";

		if($filter_data['name']){
			$sql .= " AND LCASE(pd.name) like '" . $this->db->escape(utf8_strtolower($filter_data['name']))."%' ";
		}

		if($filter_data['model']){
			$sql .= " AND LCASE(p.model) like '" . $this->db->escape(utf8_strtolower($filter_data['model']))."%' ";
		}

		if($filter_data['preorder_price']){
			$sql .= " AND pp.initial_price = ".(float)$filter_data['preorder_price'];
		}

		if($filter_data['deduction_type']){
			$sql .= " AND LCASE(pp.deduction_type) = '" . $this->db->escape(utf8_strtolower($filter_data['deduction_type']))."' ";
		}

		if(isset($filter_data['status'])){
			$sql .= " AND pp.status = '".(int)$filter_data['status']."' ";
		}

		$result = $this->db->query($sql);

		return $result->row['total'];
	}

	public function getproducts($filter_data) {

		$sql = "SELECT pp.*,p.model,pd.name FROM `".DB_PREFIX."preorder_products` pp LEFT JOIN `".DB_PREFIX."product` p ON p.product_id = pp.product_id LEFT JOIN `".DB_PREFIX."product_description` pd ON pp.product_id = pd.product_id WHERE pd.language_id='".$this->config->get('config_language_id')."' ";

		if(isset($filter_data['name']) && $filter_data['name']){
			$sql .= " AND LCASE(pd.name) like '" . $this->db->escape(utf8_strtolower($filter_data['name']))."%' ";
		}

		if(isset($filter_data['model']) && $filter_data['model']){
			$sql .= " AND LCASE(p.model) like '" . $this->db->escape(utf8_strtolower($filter_data['model']))."%' ";
		}

		if(isset($filter_data['preorder_price']) && $filter_data['preorder_price']){
			$sql .= " AND pp.initial_price = ".(float)$filter_data['preorder_price'];
		}

		if(isset($filter_data['deduction_type']) && $filter_data['deduction_type']){
			$sql .= " AND LCASE(pp.deduction_type) = '" . $this->db->escape(utf8_strtolower($filter_data['deduction_type']))."' ";
		}

		if(isset($filter_data['status']) && !is_null($filter_data['status'])){
			$sql .= " AND pp.status = '".(int)$filter_data['status']."' ";
		}

		$sort_data = array(
			'p.model',
			'pd.name',
			'pp.initial_price',
			'pp.deduction_type',
			'pp.status',
		);

		if (isset($filter_data['sort']) && in_array($filter_data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $filter_data['sort'];
		} else {
			$sql .= " ORDER BY pd.name ";
		}

		if (isset($filter_data['order']) && ($filter_data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($filter_data['start']) || isset($filter_data['limit'])) {
			if ($filter_data['start'] < 0) {
				$filter_data['start'] = 0;
			}

			if ($filter_data['limit'] < 1) {
				$filter_data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$filter_data['start'] . "," . (int)$filter_data['limit'];
		}

		$result = $this->db->query($sql)->rows;

		if($result){
			return $result;
		}else{
			false;
		}
	}

	private function isExist($id){
		$result = $this->db->query("SELECT product_id FROM `" .DB_PREFIX. "preorder_products` WHERE product_id = '" . (int)$id."' AND status = 1 ")->row;
		return $result;
	}

	public function cretaePreOrderProduct($data){

		$products = $data['pre_order_product'];
		$this->load->model('catalog/product');
		$faultId = '';
		$currectId = '';
		$alreadyAdded = '';
		$details = array();
		$stock_status = 5;
		if($this->config->get('module_wk_preorder_pro_stock_status')) {
			$stock_status = $this->config->get('module_wk_preorder_pro_stock_status');
		}

		foreach ($products as $key => $value) {
			$details[$value['product_id']] = $this->model_catalog_product->getProduct($value['product_id']);
			if(empty($details[$value['product_id']])){
				$faultId .= $value['name'].",";
			} else {
				if(isset($data['id']) && !$data['id']) {
					$chkId = $this->isExist($value['product_id']);
				}
				if(isset($chkId['product_id'])){
					$alreadyAdded .= $value['name'].",";
					continue;
				}
				$currectId .= $value['name'].",";
				$product_id = $value['product_id'];
				$cur_price = $details[$value['product_id']]['price'];
				if($data['wk_deduction_method'] == 'pc') {
					$preorder_price =  round(( $details[$value['product_id']]['price'] * $data['wk_percentage_price'] ) / 100);
					$rem_price = round($details[$value['product_id']]['price'] - $preorder_price);
				} else {
					$preorder_price =  round($details[$value['product_id']]['price'] - 0);
					$rem_price = round($details[$value['product_id']]['price'] - $preorder_price);
				}

				$cur_available_date = $details[$value['product_id']]['date_available'];
				$cur_quantity = $details[$value['product_id']]['quantity'];
				if(isset($data['id']) && $data['id']) {
					$sql = "UPDATE `" .DB_PREFIX. "preorder_products` SET product_id = '".(int)$product_id."', base_price = '".(float)$cur_price."', initial_price = '".(float)$preorder_price."', rem_price = '".(float)$rem_price."', deduction_type = '".$this->db->escape($data['wk_deduction_method'])."', cur_quantity = '".(int)$cur_quantity."', cur_available_date = '".$this->db->escape($cur_available_date)."', wk_available_date = '".$this->db->escape($data['wk_available_date'])."', preorder_quantity = '".(int)$data['wk_preorder_quantity']."', order_quantity = '".(int)$data['wk_quantity_per_order']."', customer_order = '".(int)$data['wk_order_per_customer']."', one_ip_one_order = '".(int)$data['wk_one_ip_one_order']."', preorder_start_date = '".$this->db->escape($data['wk_available_date'])."', preorder_start_date = '".$this->db->escape($data['wk_preorder_start_date'])."', wk_percentage_price = '".(float)$data['wk_percentage_price']."', discount = '" .(float)$data['wk_preorder_discount'] ."', status = '".(int)$data['wk_status']."' WHERE id = '".(int)$data['id']."' ";
				} else {
					$sql = "INSERT INTO `" .DB_PREFIX. "preorder_products` SET product_id = '".(int)$product_id."', base_price = '".(float)$cur_price."', initial_price = '".(float)$preorder_price."', rem_price = '".(float)$rem_price."', deduction_type = '".$this->db->escape($data['wk_deduction_method'])."', cur_quantity = '".(int)$cur_quantity."', cur_available_date = '".$this->db->escape($cur_available_date)."', wk_available_date = '".$this->db->escape($data['wk_available_date'])."', preorder_quantity = '".(int)$data['wk_preorder_quantity']."', order_quantity = '".(int)$data['wk_quantity_per_order']."', customer_order = '".(int)$data['wk_order_per_customer']."', one_ip_one_order = '".(int)$data['wk_one_ip_one_order']."', preorder_start_date = '".$this->db->escape($data['wk_preorder_start_date'])."', wk_percentage_price = '".(float)$data['wk_percentage_price']."', discount = '" .(float)$data['wk_preorder_discount'] ."', status = '".(int)$data['wk_status']."' ";
				}

				$this->db->query($sql);

				$this->db->query("UPDATE `".DB_PREFIX."product` SET stock_status_id = '".(int)$stock_status."', subtract = '0' WHERE product_id = '".(int)$product_id."'");
			}
		}

		$success = array(
			'failure' => $faultId,
			'success' => $currectId,
			'alreadyAdded' => $alreadyAdded,
		);
		return $success;
	}

	public function getPreorderProduct($product_id) {
        $data = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product_preorder_pro` WHERE product_id = '" . (int)$product_id . "'")->row;
        if($data AND isset($data['date']))
        	return $data['date'];
    	else
    		return '' ;
    }

    public function addPreorderProduct($product_id,$date) {
    	$this->db->query("DELETE FROM `" . DB_PREFIX . "product_preorder_pro` WHERE product_id = '".(int)$product_id."'");
      $this->db->query("INSERT INTO `" . DB_PREFIX . "product_preorder_pro` SET product_id = '".(int)$product_id."' ,date = '" . $this->db->escape(utf8_strtolower($date))."'");
    }

    public function getPreorderOrder($order_id,$product_id = false) {
        $add = '';
        if($product_id)
            $add = "AND product_id = '" . (int)$product_id . "'";

        $data = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_preorder` WHERE order_id ='".(int)$order_id."'" . $add . "")->row;
        if($data AND isset($data['id']))
            return true;
        else
            return false ;
    }

    public function viewCustomers($data) {

        $sql = "SELECT * FROM `" . DB_PREFIX . "order_preorder_customer` opc WHERE 1 ";

		$implode = array();

		if (!empty($data['filter_name'])) {
			$implode[] = "LCASE(opc.name) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
		}

		if (!empty($data['filter_email'])) {
			$implode[] = "LCASE(opc.email) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_email'])) . "%'";
		}

		if (!empty($data['filter_query'])) {
			$implode[] = "LCASE(opc.query) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_query'])) . "%'";
		}

		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}

		$sort_data = array(
			'name',
			'email',
			'query',
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY opc.id";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
    }

    public function viewtotalCustomers($data) {

        $sql = "SELECT * FROM `" . DB_PREFIX . "order_preorder_customer` opc WHERE 1 ";

		$implode = array();

		if (!empty($data['filter_name'])) {
			$implode[] = "LCASE(opc.name) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
		}

		if (!empty($data['filter_email'])) {
			$implode[] = "LCASE(opc.email) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_email'])) . "%'";
		}

		if (!empty($data['filter_query'])) {
			$implode[] = "LCASE(opc.query) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_query'])) . "%'";
		}

		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}

		$sort_data = array(
			'name',
			'email',
			'query',
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY opc.id";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		$query = $this->db->query($sql);

		return count($query->rows);
    }

    public function viewProductsCustomer($c_id) {
        $data = $this->db->query("SELECT p.model,op.quantity,pd.name,p.image,p.product_id FROM `" . DB_PREFIX . "order_preorder` op LEFT JOIN `" . DB_PREFIX . "product` p ON (op.product_id = p.product_id) LEFT JOIN `" . DB_PREFIX . "product_description` pd ON (p.product_id = pd.product_id) WHERE op.c_id ='".(int)$c_id."' AND pd.language_id = '".$this->config->get('config_language_id')."'")->rows;
        return $data;
    }

    public function deleteEnquiry($id) {
        $this->db->query("DELETE FROM `" . DB_PREFIX . "preorder_enquiry` WHERE enquiry_id = '" . (int)$id . "' ");
        $this->db->query("DELETE FROM `" . DB_PREFIX . "preorder_enquiry_threads` WHERE enquiry_id = '" . (int)$id . "' ");
    }

    public function viewOrders($data) {

        $sql = "SELECT DISTINCT oo.order_id,oo.firstname,oo.lastname,oo.date_added FROM `" . DB_PREFIX . "order_preorder` op LEFT JOIN `" . DB_PREFIX . "order` oo ON (op.order_id = oo.order_id) WHERE op.order_id > 0  ";

		$implode = array();

		if (!empty($data['filter_id'])) {
			$implode[] = "op.order_id = '" . $this->db->escape($data['filter_id']) . "'";
		}

		if (!empty($data['filter_name'])) {
			$implode[] = "LCASE(CONCAT(oo.firstname,' ',oo.lastname)) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
		}

		if (!empty($data['filter_date'])) {
			$implode[] = "LCASE(oo.date_added) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_date'])) . "%'";
		}

		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}

		$sort_data = array(
			'oo.firstname',
			'oo.date_added',
			'op.order_id',
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY op.id";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
    }

    public function viewtotalOrders($data) {

         $sql = "SELECT DISTINCT oo.order_id,oo.firstname,oo.lastname,oo.date_added FROM `" . DB_PREFIX . "order_preorder` op LEFT JOIN `" . DB_PREFIX . "order` oo ON (op.order_id = oo.order_id) WHERE op.order_id > 0  ";

		$implode = array();

		if (!empty($data['filter_id'])) {
			$implode[] = "op.order_id = '" . $this->db->escape($data['filter_id']) . "'";
		}

		if (!empty($data['filter_name'])) {
			$implode[] = "LCASE(CONCAT(oo.firstname,' ',oo.lastname)) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
		}

		if (!empty($data['filter_date'])) {
			$implode[] = "LCASE(oo.date_added) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_date'])) . "%'";
		}

		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}

		$sort_data = array(
			'oo.firstname',
			'oo.date_added',
			'op.order_id',
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY op.id";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		$query = $this->db->query($sql);

		return count($query->rows);
    }

    public function viewProductsOrder($o_id) {

        $data = $this->db->query("SELECT p.model,op.quantity,pd.name,p.image,p.product_id FROM `" . DB_PREFIX . "order_preorder` op LEFT JOIN `" . DB_PREFIX . "product` p ON (op.product_id = p.product_id) LEFT JOIN `" . DB_PREFIX . "product_description` pd ON (p.product_id = pd.product_id) WHERE op.order_id ='".$o_id."' AND pd.language_id = '".$this->config->get('config_language_id')."'")->rows;
        return $data;
    }

    public function getCustomerPreorderProduct($preorder_product_id) {
        $data = $this->db->query("SELECT p.model,ppd.quantity,pd.name,p.image,p.product_id FROM `" . DB_PREFIX . "preordred_product_detail` ppd LEFT JOIN `" . DB_PREFIX . "product` p ON (ppd.product_id = p.product_id) LEFT JOIN `" . DB_PREFIX . "product_description` pd ON (p.product_id = pd.product_id) WHERE ppd.preorder_product_id ='".(int)$preorder_product_id."' AND ppd.notified = '0' AND pd.language_id = '".(int)$this->config->get('config_language_id')."'")->rows;

        return $data;
    }

    public function deleteOrderEntry($o_id) {
        $this->db->query("DELETE FROM `" . DB_PREFIX . "order_preorder` WHERE order_id = '" . (int)$o_id . "'");
    }


	public function deleteOrder($id){

    $this->db->query("DELETE FROM `".DB_PREFIX."preordred_product_detail` WHERE order_id = '".(int)$id."' ");
    $this->db->query("DELETE FROM `".DB_PREFIX."order` WHERE order_id = '".(int)$id."' ");
    }

    public function emailToCustomer($id,$type){


        // Send out mail to admin about preorder
        $language = new Language('catalog/');

        $language->load('catalog/precartmail');

        // $template = new Template();

        $data['text_hello'] = $language->get('text_hello');
        $data['text_name'] = $this->config->get('config_name');
        $data['text_cname'] = $language->get('name');
        $data['text_subject'] = $language->get('text_subject');
        $data['text_email'] = $language->get('email');
        $data['text_query'] = $language->get('query');
        $data['text_pname'] = $language->get('text_pname');
        $data['text_pqty'] = $language->get('text_pqty');
        $data['text_pimage'] = $language->get('text_pimage');
        $data['text_pmodel'] = $language->get('text_pmodel');
        $data['text_thanksadmin'] = $language->get('text_thanksadmin');

        if ($type=="order") {
        	$customer = $this->db->query("SELECT c.email,c.firstname,ppd.product_id, ppd.preorder_product_id FROM `".DB_PREFIX."customer` c LEFT JOIN `".DB_PREFIX."preordred_product_detail` ppd ON (ppd.customer_id=c.customer_id) WHERE ppd.order_id = '".(int)$id."'")->row;
        	$data['customer']['email'] = $customer['email'];
        	$data['customer']['name'] = $customer['firstname'];
        	$data['products'] = $this->getCustomerPreorderProduct($customer['preorder_product_id']);
        } else if ($type=="auto") {
        	$customer = $this->db->query("SELECT email,firstname,ppd.product_id, ppd.preorder_product_id FROM `".DB_PREFIX."customer` c LEFT JOIN `".DB_PREFIX."preordred_product_detail` ppd ON (ppd.customer_id=c.customer_id) WHERE ppd.id = '".(int)$id."'")->row;
        	$data['customer']['email'] = $customer['email'];
        	$data['customer']['name'] = $customer['firstname'];
        	$data['products'] = $this->getCustomerPreorderProduct($customer['preorder_product_id']);
        } else {
	        $data['customer'] = $this->db->query("SELECT * FROM `".DB_PREFIX."order_preorder_customer` WHERE id = '".(int)$id."'")->row;
	        $data['products'] = $this->viewProductsCustomer($id);
	    }

        $data['store_name'] = $this->config->get('store_name');
        $data['store_url'] = HTTP_CATALOG;
        $data['product_link'] = HTTP_CATALOG.'index.php?route=product/product&product_id=';
        $data['logo'] = HTTP_CATALOG.'image/' . $this->config->get('config_logo');
        $html = $this->load->view('catalog/precartmail',$data);

				$tocustomer=array();
        $tocustomer['emailto']=$data['customer']['email'];
        $tocustomer['message']=$html;
        $tocustomer['mailfrom']=$this->config->get('config_email');
        $tocustomer['subject']=$data['text_subject'];
        $tocustomer['name']=$data['customer']['name'];


				if($this->config->get('module_wk_preorder_pro_mail_notify_customer')){
					$this->load->model('catalog/mail');
						$this->model_catalog_mail->mail($data);
		    }else{
						$this->sendMail($tocustomer);
				}
        if($type == "order"){
        	$this->db->query("UPDATE `".DB_PREFIX."preordred_product_detail` SET notified = 1 WHERE order_id = '$id'");
        } else if ($type == "auto") {
        	$this->db->query("UPDATE `".DB_PREFIX."preordred_product_detail` SET notified = 1 WHERE id = '$id'");

        } else {
        	$this->db->query("UPDATE `".DB_PREFIX."order_preorder_customer` SET notify = 1 WHERE id = '$id'");
        }

	}
	
	public function convertAll() {
		$products = $this->db->query("SELECT p.product_id, pd.name  FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX ."product_description pd ON(p.product_id = pd.product_id) WHERE p.stock_status_id = '5' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.product_id NOT IN (SELECT product_id FROM ".DB_PREFIX."preorder_products)")->rows;
		$result = array();
		
		if ($products) {
      		$current_DateTime = new DateTime(null, new DateTimeZone($this->config->get('module_wk_preorder_pro_timezone')));
			$cur_date = $current_DateTime->format('Y-m-d');
			$end_date = date('Y-m-d',strtotime( $cur_date."+" . $this->config->get('module_wk_preorder_pro_preorder_days')." days") );
			$result['wk_deduction_method'] = 'fp';
			$result['wk_percentage_price'] = '0';
			$result['wk_preorder_quantity'] = $this->config->get('module_wk_preorder_pro_quantity');
			$result['wk_quantity_per_order'] = $this->config->get('module_wk_preorder_pro_order_quantity');
			$result['wk_order_per_customer'] = $this->config->get('module_wk_preorder_pro_customer_order');
			$result['wk_one_ip_one_order'] = '0';
			$result['wk_preorder_start_date'] = $cur_date;
			$result['wk_available_date'] = $end_date;
			$result['wk_preorder_discount'] = '';
			$result['wk_status'] = '1';
			foreach($products as $product) {
				$result['pre_order_product'] = array();
				$result['pre_order_product'][$product['product_id']] = array(
					'product_id'	=> $product['product_id'],
					'name'		    => $product['name'],
				);
				$this->cretaePreOrderProduct($result);
			}
		}
	}

    public function sendMail($data){

    	$text = $data['message'];

        if(VERSION == '2.0.0.0' || VERSION == '2.0.1.0' || VERSION == '2.0.1.1'  ) {
            /*Old mail code*/
            $mail = new Mail($this->config->get('config_mail'));
            $mail->setTo($data['emailto']);
            $mail->setFrom($data['mailfrom']);
            $mail->setSender($data['name']);
            $mail->setSubject(html_entity_decode($data['subject'], ENT_QUOTES, 'UTF-8'));
            $mail->setHtml(html_entity_decode($text, ENT_QUOTES, 'UTF-8'));
            $mail->send();
        } else {
            $mail = new Mail($this->config->get('config_mail_engine'));
            $mail->protocol = $this->config->get('config_mail_protocol');
            $mail->parameter = $this->config->get('config_mail_parameter');
            $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
            $mail->smtp_username = $this->config->get('config_mail_smtp_username');
            $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
            $mail->smtp_port = $this->config->get('config_mail_smtp_port');
            $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
            $mail->setTo($data['emailto']);
            $mail->setFrom($data['mailfrom']);
            $mail->setSender($data['name']);
            $mail->setSubject(html_entity_decode($data['subject'], ENT_QUOTES, 'UTF-8'));
            $mail->setHtml(html_entity_decode($text, ENT_QUOTES, 'UTF-8'));
            $mail->send();
        }
    }

    public function createTable() {
		$this->db->query("CREATE TABLE IF NOT EXISTS `".DB_PREFIX."preorder_enquiry` (
			`enquiry_id` int(11) NOT NULL AUTO_INCREMENT,
			`product_id` int(11) NOT NULL,
			`customer_id` int(11) NOT NULL,
			`customer_name` varchar(50) NOT NULL,
			`email` varchar(100) NOT NULL,
			`subject` varchar(100) NOT NULL,
			`query` varchar(600) NOT NULL,
			`status` tinyint(1) NOT NULL,
			PRIMARY KEY (`enquiry_id`)
			) AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 "
		);

		$this->db->query("CREATE TABLE IF NOT EXISTS `".DB_PREFIX."preorder_enquiry_threads` (
			`thread_id` int(11) NOT NULL AUTO_INCREMENT,
			`enquiry_id` int(11) NOT NULL,
			`posted_by` int(11) NOT NULL,
			`query` varchar(500) NOT NULL,
			`date_added` datetime NOT NULL,
			`status` tinyint(1) NOT NULL,
			PRIMARY KEY (`thread_id`)
			) AUTO_INCREMENT=1 DEFAULT CHARSET=utf8"
		);

		$this->db->query("CREATE TABLE IF NOT EXISTS `".DB_PREFIX."preorder_products` (
			`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
			`product_id` int(100) NOT NULL,
			`base_price` float NOT NULL,
			`initial_price` float NOT NULL,
			`rem_price` float NOT NULL,
			`deduction_type` varchar(100) NOT NULL,
			`cur_quantity` int(100) DEFAULT NULL,
			`cur_available_date` date NOT NULL,
			`wk_available_date` date NOT NULL,
			`preorder_quantity` int(11) NOT NULL,
			`order_quantity` int(11) NOT NULL,
			`customer_order` int(11) NOT NULL,
			`one_ip_one_order` tinyint(1) NOT NULL,
			`preorder_start_date` date NOT NULL,
			`wk_percentage_price` double NOT NULL,
			`wk_fixed_price` float NOT NULL,
			`discount` float NOT NULL,
			`wk_work_on` varchar(10) NOT NULL,
			`status` int(10) NOT NULL,
			PRIMARY KEY (`id`)
			) AUTO_INCREMENT=1 DEFAULT CHARSET=utf8"
		);

		$this->db->query("CREATE TABLE IF NOT EXISTS `".DB_PREFIX."preordred_product_detail` (
			`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
			`order_id` int(50) NOT NULL,
			`product_id` int(50) NOT NULL,
			`preorder_product_id` int(50) NOT NULL,
			`customer_id` int(50) NOT NULL,
			`quantity` int(50) NOT NULL,
			`notified` int(5) NOT NULL,
			`status` int(10) NOT NULL,
			`paid_amount` float NOT NULL,
			PRIMARY KEY (`id`)
			) AUTO_INCREMENT=1 DEFAULT CHARSET=utf8"
		);

		$this->db->query("CREATE TABLE IF NOT EXISTS `".DB_PREFIX."product_preorder_pro` (
			`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
			`product_id` int(100) NOT NULL,
			`date` varchar(200) NOT NULL,
			PRIMARY KEY (`id`)
			) DEFAULT CHARSET=utf8"
		);

		$this->db->query("CREATE TABLE IF NOT EXISTS `".DB_PREFIX."order_preorder_customer` (
			`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
			`customer_id` int(100) NOT NULL,
			`name` varchar(100) NOT NULL,
			`email` varchar(100) NOT NULL,
			`query` varchar(1000) NOT NULL,
			`notify` int(100) NOT NULL,
			PRIMARY KEY (`id`)
			) DEFAULT CHARSET=utf8"
		);

		$this->db->query("CREATE TABLE IF NOT EXISTS `".DB_PREFIX."order_preorder` (
			`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
			`c_id` int(100) NOT NULL,
			`product_id` int(100) NOT NULL,
			`quantity` int(100) NOT NULL,
			`order_id` int(200) NOT NULL,
			PRIMARY KEY (`id`)
			) DEFAULT CHARSET=utf8"
		);
		$this->db->query("CREATE TABLE IF NOT EXISTS `".DB_PREFIX."preorder_mail` (
			`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
		  `name` varchar(100) NOT NULL,
		  `subject` varchar(1000) NOT NULL,
		  `message` varchar(5000) NOT NULL,
			PRIMARY KEY (`id`)
			) DEFAULT CHARSET=utf8"
		);

		$this->db->query("CREATE TABLE IF NOT EXISTS `".DB_PREFIX."preorder_push` (
			`user_id` INT(11) NOT NULL,
		  	`token` text NOT NULL,
		  	`endpoint` text NOT NULL,
			  PRIMARY KEY (`user_id`)
			) DEFAULT CHARSET=utf8"
		);
	}

	public function dropTable() {
		$this->db->query("DROP TABLE IF EXISTS ".DB_PREFIX."preorder_enquiry ");
		$this->db->query("DROP TABLE IF EXISTS ".DB_PREFIX."preorder_enquiry_threads ");
		$this->db->query("DROP TABLE IF EXISTS ".DB_PREFIX."preorder_products ");
		$this->db->query("DROP TABLE IF EXISTS ".DB_PREFIX."preordred_product_detail ");
		$this->db->query("DROP TABLE IF EXISTS ".DB_PREFIX."product_preorder_pro ");
		$this->db->query("DROP TABLE IF EXISTS ".DB_PREFIX."order_preorder_customer ");
		$this->db->query("DROP TABLE IF EXISTS ".DB_PREFIX."order_preorder ");
		$this->db->query("DROP TABLE IF EXISTS ".DB_PREFIX."preorder_mail ");
		$this->db->query("DROP TABLE IF EXISTS ".DB_PREFIX."preorder_push ");

	}

	public function addToken($currentToken) {
		if ($currentToken) {
			$user_id = $this->user->getId();
			$getuser = $this->db->query("SELECT user_id FROM " . DB_PREFIX . "preorder_push WHERE user_id = '" . $user_id . "'")->row;
			$headers = getallheaders();
		
			if (isset($headers['User-Agent']) && $headers['User-Agent']) {
				if (strpos($headers['User-Agent'], 'mozilla') !== false) {
					$endpoint = 'https://updates.push.services.mozilla.com/wpush/v1/'.$currentToken;
				} else {
					$endpoint = 'https://fcm.googleapis.com/fcm/send/' . $currentToken ; 
				}
			
			} else {
				$endpoint = 'https://fcm.googleapis.com/fcm/send/' . $currentToken ; 
			}
			if ($getuser) {
				$this->db->query("UPDATE " . DB_PREFIX . "preorder_push SET token = '" . $currentToken . "', endpoint = '" .  $endpoint . "' WHERE  user_id = '" . $user_id . "'");
			} else {
				$this->db->query("INSERT INTO " . DB_PREFIX . "preorder_push SET user_id = '" . $user_id . "', token = '" . $currentToken . "', endpoint = '" . $endpoint .  "'");
			}
			
		}
		
	}
}
?>
