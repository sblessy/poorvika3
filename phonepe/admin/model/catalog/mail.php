<?php
class ModelCatalogMail extends Model {

  public function addMail($data) {

    if($data['mail_id'])
			$this->db->query("UPDATE " . DB_PREFIX . "preorder_mail SET name = '" . $this->db->escape($data['name']) . "', message = '" . $this->db->escape($data['message']) . "', subject = '" . $this->db->escape($data['subject']) . "' WHERE id='".(int)$data['mail_id']."'");
		else
			$this->db->query("INSERT INTO " . DB_PREFIX . "preorder_mail SET name = '" . $this->db->escape($data['name']) . "', message = '" . $this->db->escape($data['message']) . "', subject = '" . $this->db->escape($data['subject']) . "'");
	}

  public function getMailData($id) {

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "preorder_mail WHERE id='".(int)$id."'");
		return $query->row;
	}
  public function deleteentry($id){
		$this->db->query("DELETE FROM " . DB_PREFIX . "preorder_mail WHERE id='".(int)$id."'");
	}
  public function gettotal() {

		$sql = "SELECT * FROM " . DB_PREFIX . "preorder_mail WHERE 1 ";
		$result = $this->db->query($sql);
		return $result->rows;
	}

  public function viewtotal($data){

		$sql ="SELECT * FROM " . DB_PREFIX . "preorder_mail WHERE 1 ";

		if (!empty($data['filter_id'])) {
			$sql .= " AND id = '" . (float)$this->db->escape($data['filter_id']) . "'";
		}

		if (!empty($data['filter_name'])) {
			$sql .= " AND LCASE(name) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
		}

		if (!empty($data['filter_subject'])) {
			$sql .= " AND LCASE(subject) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_subject'])) . "%'";
		}

		if (!empty($data['filter_message'])) {
			$sql .= " AND LCASE(message) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_message'])) . "%'";
		}

		$sort_data = array(
			'id',
			'name',
			'subject',
			'message',
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY id";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$result=$this->db->query($sql);

		return $result->rows;
	}

	public function viewtotalentry($data){

		$sql ="SELECT * FROM " . DB_PREFIX . "preorder_mail WHERE 1 ";

		if (!empty($data['filter_id'])) {
			$sql .= " AND id = '" . (float)$this->db->escape($data['filter_id']) . "'";
		}

		if (!empty($data['filter_name'])) {
			$sql .= " AND LCASE(name) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
		}

		if (!empty($data['filter_subject'])) {
			$sql .= " AND LCASE(subject) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_subject'])) . "%'";
		}

		if (!empty($data['filter_message'])) {
			$sql .= " AND LCASE(message) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_message'])) . "%'";
		}

		$result = $this->db->query($sql);

		return count($result->rows);
	}

  public function mail($details){

    $mail_id = $this->config->get('module_wk_preorder_pro_mail_notify_customer');
    $mail_details = $this->getMailData($mail_id);
    $mail_to = $details['customer']['email'];
    $mail_from = $this->config->get('config_email');
    $value_index = array();
    foreach($details['products'] as $product){
        $value_index = array(
          'product_name' => $product['name'],
          'product_image' => '<img src="'.HTTPS_CATALOG.'image/' .$product['image'].'">',
          'product_model' => $product['model'],
          'product_quantity' => $product['quantity'],
          'product_id' => $product['product_id']
        );
    }

		if($mail_details){

			$find = array(
        '{config_logo}',
        '{config_icon}',
        '{config_currency}',
        '{config_name}',
        '{config_owner}',
        '{config_address}',
        '{config_geocode}',
        '{config_email}',
        '{config_telephone}',
        '{product_link}',
        '{customer_name}',
        '{product_name}',
        '{product_image}',
        '{product_model}',
        '{product_quantity}'
				);
        $replace = array(
          'config_logo' => '<img src="'.HTTPS_CATALOG.'image/' . $this->config->get('config_logo').'">',
  				'config_icon' => '<img src="'.HTTPS_CATALOG.'image/' . $this->config->get('config_icon').'">',
  				'config_currency' => $this->config->get('config_currency'),
  				'config_name' => $this->config->get('config_name'),
  				'config_owner' => $this->config->get('config_owner'),
  				'config_address' => $this->config->get('config_address'),
  				'config_geocode' => $this->config->get('config_geocode'),
  				'config_email' => $this->config->get('config_email'),
  				'config_telephone' => $this->config->get('config_telephone'),
          'product_link'  => $details['product_link'].$value_index['product_id'],
          'customer_name' => $details['customer']['name'],
			   );

      $replace = array_merge($replace,$value_index);

      $mail_details['message'] = trim(str_replace($find, $replace, $mail_details['message']));

			$data['subject'] = $mail_details['subject'];
			$data['message'] = html_entity_decode($mail_details['message']);


      $html = $this->load->view('catalog/mail', $data);

      if (preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $mail_to) AND preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $mail_from) ) {


				if(VERSION == '2.0.0.0' || VERSION == '2.0.1.0' || VERSION == '2.0.1.1') {

					$mail = new Mail($this->config->get('config_mail'));
					$mail->setTo($mail_to);
					$mail->setFrom($mail_from);
					$mail->setSender( $this->config->get('config_name'));
					$mail->setSubject($mail_details['subject']);
					$mail->setHtml($html);
					$mail->setText(strip_tags($html));
					$mail->send();
				} else {
					$mail = new Mail($this->config->get('config_mail_engine'));
          			$mail->protocol = $this->config->get('config_mail_protocol');
					$mail->parameter = $this->config->get('config_mail_parameter');
					$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
					$mail->smtp_username = $this->config->get('config_mail_smtp_username');
					$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
					$mail->smtp_port = $this->config->get('config_mail_smtp_port');
					$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
					$mail->setTo($mail_to);
					$mail->setFrom($mail_from);
					$mail->setSender($this->config->get('config_name'));
					$mail->setSubject($mail_details['subject']);
					$mail->setHtml($html);
					$mail->setText(strip_tags($html));
					$mail->send();
				}
      }
    }
  }
}
