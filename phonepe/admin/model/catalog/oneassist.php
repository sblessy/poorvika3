<?php

class ModelCatalogOneassist extends Model {

	public function addOneAssistTC($data) {
		$created_dt = date("Y-m-d");

		$this->db->query("DELETE FROM " . DB_PREFIX . "oneassist_tc");

		$this->db->query("INSERT INTO " . DB_PREFIX . "oneassist_tc SET adldplantc = '" . $data['adldplantc'] . "', `ewplantc` = '" . $data['ewplantc'] . "',created_date='".$created_dt  . "'");
	    $tc_id = $this->db->getLastId();

		return $tc_id;

	}
	public function addOneAssistPlan($data) {

		$cat_name = $this->getOaCategory($data['category_id']);
		$category_name = $cat_name['name'];

		$created_dt = date("Y-m-d H:i:s");

		$this->db->query("INSERT INTO " . DB_PREFIX . "oneassist_plans SET plan_code = '" . $data['plan_code'] . "', `category` = '" . $category_name. "', `category_id` = '" . $data['category_id'] . "', `type` = '" . $data['type'] . "', `brand` = '" . $data['brand'] . "', `plan_name` = '" . $data['plan_name'] . "', `from_price` = '" . $data['from_price'] . "', `to_price` = '" . $data['to_price'] . "', `poorvika_mrp` = '" . $data['poorvika_mrp'] . "', `apx_itemcode` = '" . $data['apx_itemcode'] . "',created_date='".$created_dt  . "'");
	    $plan_id = $this->db->getLastId();

		return $plan_id;

	}
	public function getOaCategory($category_id) {

		$query = $this->db->query("SELECT DISTINCT *, (SELECT GROUP_CONCAT(cd1.name ORDER BY level SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;') FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "category_description cd1 ON (cp.path_id = cd1.category_id AND cp.category_id != cp.path_id) WHERE cp.category_id = c.category_id AND cd1.language_id = '" . (int)$this->config->get('config_language_id') . "' GROUP BY cp.category_id) AS path, (SELECT DISTINCT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'category_id=" . (int)$category_id . "') AS keyword FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd2 ON (c.category_id = cd2.category_id) WHERE c.category_id = '" . (int)$category_id . "' AND cd2.language_id = '" . (int)$this->config->get('config_language_id') . "'");



		return $query->row;

	}
	public function getOneAssistPlan() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "oneassist_tc");
		$res = $query->row;
		return $res;
	}
	public function getPlanDetail($planid) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "oneassist_plans WHERE oa_plan_id = ".$planid);
		$res = $query->row;
		return $res;
	}
	public function getTotalOneassist() {

		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "oneassist_plans");

		return $query->row['total'];

	}
	public function editOneAssistPlan($planid,$data) {

		$cat_name = $this->getOaCategory($data['category_id']);
		$category_name = $cat_name['name'];

		$query = $this->db->query("UPDATE " . DB_PREFIX . "oneassist_plans SET plan_code = '" . $data['plan_code'] . "', `category` = '" . $category_name . "', `category_id` = '" . $data['category_id'] . "', `type` = '" . $data['type'] . "', `brand` = '" . $data['brand'] . "', `plan_name` = '" . $data['plan_name'] . "', `from_price` = '" . $data['from_price'] . "', `to_price` = '" . $data['to_price'] . "', `poorvika_mrp` = '" . $data['poorvika_mrp'] . "', `apx_itemcode` = '" . $data['apx_itemcode'] . "' WHERE oa_plan_id = ".$planid);

		return $query;

	}	
	public function getOneAssistPlans($data = array()) {
//Array ( [sort] => name [order] => ASC [start] => 0 [limit] => 30 ) 
		$sql = "SELECT * FROM " . DB_PREFIX . "oneassist_plans " ." WHERE oa_plan_id != ''";



		if (!empty($data['filter_name'])) {

			$sql .= " AND plan_name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";

		}



		$sql .= " GROUP BY oa_plan_id";



		$sort_data = array(

			'plan_name',

			'oa_plan_id'

		);



		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {

			$sql .= " ORDER BY " . $data['sort'];

		} else {

			$sql .= " ORDER BY oa_plan_id";

		}



		if (isset($data['order']) && ($data['order'] == 'DESC')) {

			$sql .= " DESC";

		} else {

			$sql .= " ASC";

		}



		if (isset($data['start']) || isset($data['limit'])) {

			if ($data['start'] < 0) {

				$data['start'] = 0;

			}



			if ($data['limit'] < 1) {

				$data['limit'] = 20;

			}



			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];

		}



		$query = $this->db->query($sql);



		return $query->rows;

	}

	public function deleteOneAssistPlan($planid) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "oneassist_plans WHERE oa_plan_id = '" . (int)$planid . "'");

	}

	public function getAllCategories($data = array()) {
		$sql = "SELECT cp.category_id AS category_id, GROUP_CONCAT(cd1.name ORDER BY cp.level SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;') AS name, c1.parent_id, c1.sort_order FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "category c1 ON (cp.category_id = c1.category_id) LEFT JOIN " . DB_PREFIX . "category c2 ON (cp.path_id = c2.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd1 ON (cp.path_id = cd1.category_id) LEFT JOIN " . DB_PREFIX . "category_description cd2 ON (cp.category_id = cd2.category_id) WHERE cd1.language_id = '" . (int)$this->config->get('config_language_id') . "' AND c1.parent_id = 0 AND cd2.language_id = '" . (int)$this->config->get('config_language_id') . "'";



		if (!empty($data['filter_name'])) {

			$sql .= " AND cd2.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";

		}



		$sql .= " GROUP BY cp.category_id";



		$sort_data = array(

			'name',

			'sort_order'

		);



		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {

			$sql .= " ORDER BY " . $data['sort'];

		} else {

			$sql .= " ORDER BY sort_order";

		}



		if (isset($data['order']) && ($data['order'] == 'DESC')) {

			$sql .= " DESC";

		} else {

			$sql .= " ASC";

		}



		if (isset($data['start']) || isset($data['limit'])) {

			if ($data['start'] < 0) {

				$data['start'] = 0;

			}



			if ($data['limit'] < 1) {

				$data['limit'] = 20;

			}



			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];

		}



		$query = $this->db->query($sql);



		return $query->rows;

	}

}

