<?php

class Controlleraccountwkenquirylist extends Controller {

    public function index() {

      if(!$this->config->get('module_wk_preorder_pro_status')){
        $this->response->redirect($this->url->link('account/account', '', true));
      }
        if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/account', '', true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}

        $data = array();
        $data = array_merge($data, $this->language->load('account/wk_preorder'), $this->language->load('checkout/precart'));

		$this->document->setTitle($this->language->get('heading_title_enquiry'));

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'pe.enquiry_id';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$filter = array();
		$url = '';
		if($this->request->get) {
			foreach ($this->request->get as $key => $value) {
				if (strpos($key, 'filter_') !== false) {
					$filter[$key] = $data[$key] = $this->request->get[$key];
					$url .= '&'.$key.'='.$this->request->get[$key];
				}
			}
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);

        $data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title_enquiry'),
			'href' => $this->url->link('account/wk_enquiry_list', '' . $url, true)
		);

		$filter_data = array();
        $filter_data = $filter;
        $filter_data['sort']  = $sort;
        $filter_data['order']  = $order;
        $filter_data['start']  = ($page - 1) * $this->config->get('config_limit_admin');
        $filter_data['limit']  = $this->config->get('config_limit_admin');
		$filter_data['customer_id'] = $this->customer->getId();

        $this->load->model('account/wk_pre_order');
		$data['preorder_products'] = $this->model_account_wk_pre_order->getProOrderProducts();
        $enquires = $this->model_account_wk_pre_order->getEnquiries($filter_data);
        $enquires_total = $this->model_account_wk_pre_order->getEnquiriesTotal($filter_data);
        $data['enquires'] = array();
        if($enquires) {
            foreach ($enquires as $key => $enquiry) {
                $data['enquires'][] = array(
                    'enquiry_id' => '#'.$enquiry['enquiry_id'],
                    'customer_name' => $enquiry['customer_name'],
                    'email' => $enquiry['email'],
                    'product_name' => $enquiry['name'],
                    'subject' => $enquiry['subject'],
                    'total_thread' => $enquiry['total_thread'],
                    'status' => $enquiry['status'],
                    'view' => $this->url->link('account/wk_enquiry_list/view', '&id='.$enquiry['enquiry_id'], true),
                );
            }
        }

		$data['customer_id'] = $this->customer->getId();
		$data['customer_email'] = $this->customer->getEmail();
		$data['customer_name'] = $this->customer->getFirstname().' '.$this->customer->getlastname();
		$data['module_wk_preorder_pro_site_key'] = $this->config->get('module_wk_preorder_pro_site_key');

		$url = '';

		if($this->request->get) {
			foreach ($this->request->get as $key => $value) {
				if (strpos($key, 'filter_') !== false) {
					$url .= '&'.$key.'='.$this->request->get[$key];
				}
			}
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_enquiry_id'] = $this->url->link('account/wk_enquiry_list', '' . $url . '&sort=pe.enquiry_id', true);
		$data['sort_customer_name'] = $this->url->link('account/wk_enquiry_list', '' . $url . '&sort=pe.customer_name', true);
		$data['sort_email'] = $this->url->link('account/wk_enquiry_list', '' . $url . '&sort=pe.email' , true);
		$data['sort_name'] = $this->url->link('account/wk_enquiry_list', '' . $url . '&sort=pd.name' , true);
		$data['sort_subject'] = $this->url->link('account/wk_enquiry_list', '' . $url . '&sort=pe.subject' , true);
		$data['sort_status'] = $this->url->link('account/wk_enquiry_list', '' . $url . '&sort=pe.status' , true);

		$url = '';

		if($this->request->get) {
			foreach ($this->request->get as $key => $value) {
				if (strpos($key, 'filter_') !== false) {
					$url .= '&'.$key.'='.$this->request->get[$key];
				}
			}
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $enquires_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('account/wk_enquiry_list', $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($enquires_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($enquires_total - $this->config->get('config_limit_admin'))) ? $enquires_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $enquires_total, ceil($enquires_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['clear_filter'] = $this->url->link('account/wk_enquiry_list', '', true);
        $data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('account/wk_enquiry_list', $data));
    }

    public function view() {
      if(!$this->config->get('module_wk_preorder_pro_status')){
        $this->response->redirect($this->url->link('account/account', '', true));
      }
        if (!$this->customer->isLogged()) {
          $this->session->data['redirect'] = $this->url->link('account/account', '', true);
          $this->response->redirect($this->url->link('account/login', '', true));
        }

        $data = array();
        $data = array_merge($data, $this->language->load('account/wk_preorder'));
		    $this->document->setTitle($this->language->get('heading_title_enquiry_detail'));
        if(isset($this->request->get['id']) && $this->request->get['id']) {
            $enquiry_id = $this->request->get['id'];
        } else {
            $enquiry_id = 0;
        }
        $this->load->model('account/wk_pre_order');
        $data['enquiry'] = array();
        $enquiry = $this->model_account_wk_pre_order->getEnquiry($enquiry_id);
        if($enquiry) {
            $data['enquiry'] = $enquiry;
            $enquiry_threads = $this->model_account_wk_pre_order->getEnquiryThreads($enquiry_id);
            $data['threads'] = array();
            if($enquiry_threads) {
                foreach ($enquiry_threads as $key => $enquiry_thread) {
                    $data['threads'][] = array(
                        'thread_id' => $enquiry_thread['thread_id'],
                        'enquiry_id' => $enquiry_thread['enquiry_id'],
                        'posted_by' => $enquiry_thread['posted_by'],
                        'query' => $enquiry_thread['query'],
                        'date_added' => $enquiry_thread['date_added'],
                        'status' => $enquiry_thread['status'],
                    );
                }
            }
        } else {
            $this->session->data['error_warning'] = $this->language->get('no_enquiry_exist');
            $this->response->redirect($this->url->link('account/wk_enquiry_list', '', true));
        }

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);

        $data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title_enquiry'),
			'href' => $this->url->link('account/wk_enquiry_list', '', true)
		);


    $data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title_enquiry_detail'),
			'href' => $this->url->link('account/wk_enquiry_list/view&id='.$enquiry_id, '', true)
		);

        if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

        $data['back'] = $this->url->link('account/wk_enquiry_list', '', true);

        $data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('account/wk_enquiry_detail', $data));

    }

    public function addThread() {
        $this->load->language('account/wk_preorder');
		$json = array();
		if($this->request->server['REQUEST_METHOD'] == 'POST' && $this->request->post) {
			if(isset($this->request->post['enquiry_id']) && !$this->request->post['enquiry_id']) {
				$json['success'] = false;
				$json['msg'] = $this->language->get('error_no_enquiry_id');
			} else if(!isset($this->request->post['enquiry_id'])) {
				$json['success'] = false;
				$json['msg'] = $this->language->get('error_in_general');
			}
			if(!isset($this->request->post['query']) || !$this->request->post['query'] || strlen(trim($this->request->post['query'])) < 10 || strlen(trim($this->request->post['query'])) > 500 ) {
				$json['success'] = false;
				$json['msg'] = $this->language->get('error_query_length');
			}
			if(!$json) {
				$this->load->model('extension/module/wk_preorder_pro');
				$this->model_extension_module_wk_preorder_pro->addThread($this->request->post);
				$json['success'] = true;
				$json['msg'] = $this->language->get('success_thread_added');
			}
		} else {
			$json['success'] = false;
			$json['msg'] = $this->language->get('error_in_general');
		}
		$this->response->setOutput(json_encode($json));
    }

}

?>
