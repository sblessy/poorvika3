<?php

// heading
$_['heading_title']                 =       'Pre-Order list';
$_['heading_title_enquiry']         =       'Enquiry list';
$_['heading_title_enquiry_detail']  =       'Enquiry detail';

// button
$_['button_clear_filter']           =       'Clear filter';

// text
$_['text_preorder']                 =       'Pre-Order';
$_['text_preorder_list']            =       'Pre-Order list';
$_['text_enquiry_list']             =       'Enquiry list';
$_['text_account']                  =       'Account';
$_['text_open']                     =       '<span class="text-success">Open</span>';
$_['text_resolved']                 =       '<span class="text-danger">Resolved</span>';
$_['text_query']                    =       'Query';
$_['text_current_status']           =       'Current status';
$_['text_account']                  =       'Account';
$_['text_admin']                    =       'Admin';
$_['text_posted_by']                =       'Posted By';
$_['text_no_product_to_enquiry']    =       'No product is for preorder to enquired about!';
$_['text_order_total_detail']       =       'Order Total Detail';
$_['text_change_currency']          =       'Change currency';
$_['text_partial_paid']             =       '<span class="text-warning">Partial paid</span>';
$_['text_complete_paid']            =       '<span class="text-success">Complete</span>';
$_['text_currency_info']            =       'Paid amount will be displayed in the same currency in which orders are placed. If you want to see all of them in current currency then click here ';
$_['text_no_record']                =       'No record found!';

// column
$_['column_enquiry_id']             =       'Enquiry Id';
$_['column_order_id']               =       'Order Id';
$_['column_customer_name']          =       'Customer name';
$_['column_email']                  =       'Email';
$_['column_product_name']           =       'Product name';
$_['column_quantity']               =       'Quantity';
$_['column_total_threads']          =       'Total threads';
$_['column_subject']                =       'Subject';
$_['column_actual_price']           =       'Actual price';
$_['column_rem_amount']             =       'Remaining amount';
$_['column_paid_amount']            =       'Paid amount';
$_['column_date_added']             =       'Date Added';
$_['column_status']                 =       'Status';
$_['column_action']                 =       'Action';

// Mail text
$_['mail_text_preorder_product']    =       'Enquiry for product - %s';
$_['mail_text_salutation']          =       'Hi, %s';
$_['mail_text_query_id']            =       'Query id : #%d';
$_['mail_text_subject']             =       'Subject : ';
$_['mail_text_enquire_by']          =       'Enquired by : %s , Email id : %s';
$_['mail_text_query']               =       'Query : ';
$_['mail_text_customer_query']      =       'Customer Query : ';
$_['mail_text_thank_you']           =       'Thank you';
$_['mail_text_check_ur_enq']        =       'Check your enquiry';
$_['mail_text_subject_to_customer'] =       'Pre Order Enquiry for %s';
$_['mail_text_subject_admin']       =       'New enquiry arrived for %s';
$_['greeting_to_customer']          =       'Thank you for contacting us, we will revert back you very soon. If you have any other query then click on below given link and comment for your query.';

// Error
$_['error_in_general']              =       'Warning: There is some issue, please try again later!';
$_['no_enquiry_exist']              =       'Warning: There no such enquiry made by you, please check and try again!';
$_['error_no_enquiry_id']           =       'Some issue found with enquiry id, please try again after refreshing the page!';
$_['text_no_thread']                =       'No thread found for this enquiry!';
$_['error_query_length']            =       'Query length must be in between 10 to 1000 characters!';
// Success
$_['success_thread_added']          =       'Success: Thread is added successfully.';
?>
