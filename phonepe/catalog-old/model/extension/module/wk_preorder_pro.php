<?php
##################################################################
#  Preorder Pro Opencart 2.x.x.x From Webkul  http://webkul.com  #
##################################################################
class ModelExtensionModulewkPreorderPro extends Model {

    public function getPreorderDetailsByOrderId($order_id) {
        $result = $this->db->query("SELECT * FROM ".DB_PREFIX."preordred_product_detail ppd WHERE order_id = '".(int)$order_id."' && customer_id = '".(int)$this->customer->getId()."' ")->row;
        if($result) {
            return $result;
        } else {
            return false;
        }
    }

    public function sendEmailNotification($data) {
        $mail = new Mail();
        $mail->protocol = $this->config->get('config_mail_protocol');
        $mail->parameter = $this->config->get('config_mail_parameter');
        $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
        $mail->smtp_username = $this->config->get('config_mail_smtp_username');
        $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
        $mail->smtp_port = $this->config->get('config_mail_smtp_port');
        $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
        $mail->setTo($data['email_to']);
        $mail->setFrom($this->config->get('config_email'));
        $mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
        $mail->setSubject(html_entity_decode($data['subject'], ENT_QUOTES, 'UTF-8'));
        $mail->setHtml($this->load->view('account/wk_enquiry_email', $data));
        $mail->setText($data['enquiry_query']);
        $mail->send();
    }

    public function addPreOrderEnquiry($data) {
        $this->db->query("INSERT INTO ".DB_PREFIX."preorder_enquiry SET product_id = '".(int)$data['preorder_product_id']."', customer_id = '".(int)$data['preorder_customer_id']."', customer_name = '".$this->db->escape($data['preorder_name'])."', email = '".$this->db->escape($data['preorder_email'])."', subject = '".$this->db->escape($data['preorder_subject'])."', query = '".$this->db->escape($data['preorder_query'])."', status = '1' ");
        $enquiry_id = $this->db->getLastId();
        // Email notification
        $product = $this->db->query("SELECT * FROM ".DB_PREFIX."product_description pd WHERE pd.product_id = '".(int)$data['preorder_product_id']."' && pd.language_id = '".(int)$this->config->get('config_language_id')."' ")->row;
        // Email to customer for enquiry
        $data['product_detail'] = $product;
        $this->language->load('account/wk_preorder');

        $data['mail_type'] = 'enquiry';
        $data['query_id'] = sprintf($this->language->get('mail_text_query_id'), $enquiry_id);
        $data['logo'] = $this->config->get('config_url') . 'image/' . $this->config->get('config_logo');
        $data['store_name'] = $this->config->get('config_name');
        $data['store_url'] = $this->config->get('config_url');
        $data['customer_id'] = $this->customer->getId();
        $data['heading'] = sprintf($this->language->get('mail_text_preorder_product'), $product['name']);
        $data['mail_text_salutation'] = sprintf($this->language->get('mail_text_salutation'), $data['preorder_name']);
        $data['mail_text_subject'] = $this->language->get('mail_text_subject');
        $data['mail_text_query'] = $this->language->get('mail_text_query');
        $data['greeting_to_customer'] = $this->language->get('greeting_to_customer');
        $data['mail_text_check_ur_enq'] = $this->language->get('mail_text_check_ur_enq');
        $data['mail_text_thank_you'] = $this->language->get('mail_text_thank_you');
        $data['enquiry_subject'] = $data['preorder_subject'];
        $data['enquiry_query'] = $data['preorder_query'];
        $data['enquiry_link'] = $this->url->link('account/wk_enquiry_list/view', '&id='.$enquiry_id, true);
        $data['enquiry_mail'] = 'customer_enquiry';
        $data['email_to'] = $data['preorder_email'];
        $data['send_to_name'] = $data['preorder_name'];
        $data['subject'] = sprintf($this->language->get('mail_text_subject_to_customer'), $product['name']);
        $this->sendEmailNotification($data);

        // Email to admin for enquiry
        $data['email_to'] = $this->config->get('config_email');
        $data['subject'] = sprintf($this->language->get('mail_text_subject_admin'), $product['name']);
        $data['enquiry_mail'] = 'to_admin';
        $data['mail_text_query'] = $this->language->get('mail_text_customer_query');
        $data['mail_text_salutation'] = sprintf($this->language->get('mail_text_enquire_by'), $data['preorder_name'], $data['preorder_email']);
        $this->sendEmailNotification($data);

        return true;
    }

    public function addThread($data) {
        $sql = "INSERT INTO ".DB_PREFIX."preorder_enquiry_threads SET enquiry_id = '".(int)$data['enquiry_id']."', posted_by = '".(int)$this->customer->getId()."', query = '".$this->db->escape($data['query'])."', date_added = NOW(), status = '".(int)$data['status']."' ";
        $this->db->query($sql);
        $this->db->query("UPDATE ".DB_PREFIX."preorder_enquiry SET status = '".(int)$data['status']."' WHERE enquiry_id = '".(int)$data['enquiry_id']."' ");

        $enquiry = $this->db->query("SELECT * FROM ".DB_PREFIX."preorder_enquiry WHERE enquiry_id = '".(int)$data['enquiry_id']."' ")->row;
        $product = $this->db->query("SELECT * FROM ".DB_PREFIX."product_description pd WHERE pd.product_id = '".(int)$enquiry['product_id']."' && pd.language_id = '".(int)$this->config->get('config_language_id')."' ")->row;

        $this->language->load('account/wk_preorder');

        $data['mail_type'] = 'enquiry';
        $data['query_id'] = sprintf($this->language->get('mail_text_query_id'), $data['enquiry_id']);
        $data['logo'] = $this->config->get('config_url') . 'image/' . $this->config->get('config_logo');
        $data['store_name'] = $this->config->get('config_name');
        $data['store_url'] = $this->config->get('config_url');
        $data['customer_id'] = $this->customer->getId();
        $data['heading'] = sprintf($this->language->get('mail_text_preorder_product'), $product['name']);
        $data['mail_text_salutation'] = sprintf($this->language->get('mail_text_salutation'), $enquiry['customer_name']);
        $data['mail_text_subject'] = $this->language->get('mail_text_subject');
        $data['mail_text_query'] = $this->language->get('mail_text_query');
        $data['greeting_to_customer'] = $this->language->get('greeting_to_customer');
        $data['mail_text_check_ur_enq'] = $this->language->get('mail_text_check_ur_enq');
        $data['mail_text_thank_you'] = $this->language->get('mail_text_thank_you');
        $data['enquiry_subject'] = $enquiry['subject'];
        $data['enquiry_query'] = $enquiry['query'];
        $data['enquiry_link'] = $this->url->link('account/wk_enquiry_list/view', '&id='.$data['enquiry_id'], true);
        $data['enquiry_mail'] = 'customer_enquiry';
        $data['email_to'] = $enquiry['email'];
        $data['send_to_name'] = $enquiry['customer_name'];
        $data['subject'] = sprintf($this->language->get('mail_text_subject_to_customer'), $product['name']);
        $this->sendEmailNotification($data);

        return true;
    }

    public function checkCustomerEligibility($details, $customer_id) {
        $current_ip = $this->request->server['REMOTE_ADDR'];
        $product_id = $details['product_id'];
        $json = array();
        if($details['one_ip_one_order']) {
            if($this->checkOrderFromThisIpForProductId($customer_id,$current_ip, $product_id)) {
                $json['success'] = false;
                $json['type'] = 'one_ip_one_order';
            }
        }

    //   $quantity_left_to_be_ordered = $this->db->query("SELECT SUM(quantity) as quantity FROM ".DB_PREFIX."preordred_product_detail ppd LEFT JOIN ".DB_PREFIX."preorder_products pp ON (ppd.preorder_product_id=pp.id) WHERE ppd.preorder_product_id = '".$details['id']."' && ppd.product_id = '".(int)$product_id."' ")->row;

        if(!$json && $details['preorder_quantity'] <= 0) {
            $json['success'] = false;
            $json['type'] = 'no_more_preorder';
        }

        $total_order_by_customer = $this->db->query("SELECT quantity FROM ".DB_PREFIX."preordred_product_detail ppd LEFT JOIN ".DB_PREFIX."preorder_products pp ON (ppd.preorder_product_id=ppd.id) WHERE ppd.preorder_product_id = '".$details['id']."' && ppd.product_id = '".(int)$product_id."' && ppd.customer_id = '" . $this->customer->getId() . "'")->rows;
        
        if(!$json && $total_order_by_customer && count($total_order_by_customer) >= $details['customer_order']) {
            $json['success'] = false;
            $json['type'] = 'order_per_customer';
        }

        if($json) {
            return $json;
        } else {
            $json['success'] = true;
            return $json;
        }
    }

    public function checkOrderFromThisIpForProductId($customer_id,$current_ip, $product_id){
        $sql = "SELECT ppd.order_id FROM ".DB_PREFIX."order o LEFT JOIN ".DB_PREFIX."preordred_product_detail ppd ON (ppd.order_id=o.order_id) LEFT JOIN ".DB_PREFIX."preorder_products pp ON (ppd.preorder_product_id=ppd.id) WHERE o.ip = '".$this->db->escape($current_ip)."' AND o.customer_id = '".(int)$customer_id."' AND ppd.product_id = '".(int)$product_id."' AND o.date_added <= pp.wk_available_date  ";
        $result = $this->db->query($sql)->row;
        if(isset($result['order_id']) && $result['order_id']) {
            return true;
        } else{
            return false;
        }
    }

    public function getStockStatuses($data = array()) {
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "stock_status WHERE language_id = '" . (int)$this->config->get('config_language_id') . "'";

			$sql .= " ORDER BY name";

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->db->query($sql);

			return $query->rows;
		} else {
			$stock_status_data = $this->cache->get('stock_status.' . (int)$this->config->get('config_language_id'));

			if (!$stock_status_data) {
				$query = $this->db->query("SELECT stock_status_id, name FROM " . DB_PREFIX . "stock_status WHERE language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY name");

				$stock_status_data = $query->rows;

				$this->cache->set('stock_status.' . (int)$this->config->get('config_language_id'), $stock_status_data);
			}

			return $stock_status_data;
		}
	}

    public function get_total_productcount($filter_data){
        $sql = "SELECT ppd.id,ppd.order_id,ppd.notified,ppd.product_id,ppd.status,c.firstname,c.lastname,c.email,o.total,pd.name FROM `".DB_PREFIX."preordred_product_detail` ppd LEFT JOIN `".DB_PREFIX."customer` c ON c.customer_id=ppd.customer_id LEFT JOIN `".DB_PREFIX."order` o ON o.order_id=ppd.order_id LEFT JOIN `".DB_PREFIX."product_description` pd ON pd.product_id=ppd.product_id WHERE pd.language_id='".$this->config->get('config_language_id')."' ";

        if(!empty($filter_data['pname'])) {
            $sql .= " AND LCASE(pd.name) like '".$this->db->escape(utf8_strtolower($filter_data['pname']))."%' ";
        }

        if(!empty($filter_data['name'])) {
            $sql .= " AND ( LCASE(c.firstname) like '".$this->db->escape(utf8_strtolower($filter_data['name']))."%' || LCASE(c.firstname) like '".$this->db->escape(utf8_strtolower($filter_data['name']))."%' || LCASE(CONCAT(c.firstname,' ',c.lastname)) like '".$this->db->escape(utf8_strtolower($filter_data['name']))."%' )";
        }

        if(!empty($filter_data['customer_mail'])) {
            $sql .= " AND LCASE(c.email) like '".$this->db->escape(utf8_strtolower($filter_data['customer_mail']))."%' ";
        }

        if(!empty($filter_data['notified'])) {
            if($filter_data['notified'] == 'yes'){
                $sql .= " AND ppd.notified = '1' ";
            }else if($filter_data['notified'] == 'no') {
                $sql .= " AND ppd.notified = '0' ";
            }
        }

        if(!empty($filter_data['status'])) {
            if($filter_data['status'] == 'yes'){
                $sql .= " AND ppd.status = '0' ";
            }else if($filter_data['status'] == 'no') {
                $sql .= " AND ppd.status = '1' ";
            }
        }

        $result = $this->db->query($sql)->rows;
        return count($result);
    }

    public function isPreorder($id) {
        $result = $this->db->query("SELECT * FROM `" .DB_PREFIX. "preorder_products` WHERE product_id = '".(int)$id."' AND status = 1 AND preorder_start_date <= '".date("Y-m-d")."' ")->row;
        if($result) {
            return $result;
        } else {
            return false;
        }
    }

    public function get_Preordered_list($filter_data){
        $sql = "SELECT ppd.id,ppd.order_id,ppd.notified,ppd.product_id,ppd.status,c.firstname,c.lastname,c.email,o.total,pd.name FROM `".DB_PREFIX."preordred_product_detail` ppd LEFT JOIN `".DB_PREFIX."customer` c ON c.customer_id=ppd.customer_id LEFT JOIN `".DB_PREFIX."order` o ON o.order_id=ppd.order_id LEFT JOIN `".DB_PREFIX."product_description` pd ON pd.product_id=ppd.product_id WHERE pd.language_id='".$this->config->get('config_language_id')."' ";

        if(!empty($filter_data['pname'])) {
            $sql .= " AND LCASE(pd.name) like '".$this->db->escape(utf8_strtolower($filter_data['pname']))."%' ";
        }

        if(!empty($filter_data['name'])) {
            $sql .= " AND ( LCASE(c.firstname) like '".$this->db->escape(utf8_strtolower($filter_data['name']))."%' || LCASE(c.firstname) like '".$this->db->escape(utf8_strtolower($filter_data['name']))."%' || LCASE(CONCAT(c.firstname,' ',c.lastname)) like '".$this->db->escape(utf8_strtolower($filter_data['name']))."%' )";
        }

        if(!empty($filter_data['customer_mail'])) {
            $sql .= " AND LCASE(c.email) like '".$this->db->escape(utf8_strtolower($filter_data['customer_mail']))."%' ";
        }

        if(!empty($filter_data['notified'])) {
            if($filter_data['notified'] == 'yes'){
                $sql .= " AND ppd.notified = '1' ";
            }else if($filter_data['notified'] == 'no') {
                $sql .= " AND ppd.notified = '0' ";
            }
        }

        if(!empty($filter_data['status'])) {
            if($filter_data['status'] == 'yes'){
                $sql .= " AND ppd.status = '1' ";
            }else if($filter_data['status'] == 'no') {
                $sql .= " AND ppd.status = '0' ";
            }
        }

        if(!empty($filter_data['sort'])) {
            $sql .= " ";
        }

        if(!empty($filter_data['sort'])) {
            $sql .= " ORDER BY '".$filter_data['sort']."', '".$filter_data['order']."' LIMIT '".$filter_data['start']."','".$filter_data['limit'];
        }else{
            $sql .= " ORDER BY ppd.order_id DESC LIMIT ".$filter_data['start'].",".$filter_data['limit'];
        }

        $result = $this->db->query($sql)->rows;
        return $result;
    }

    public function delete_preorder_productct($id){
        $result = $this->db->query("SELECT product_id,base_price,cur_available_date,cur_quantity FROM `".DB_PREFIX."preorder_products` WHERE id = '".(int)$id."' ")->row;

        $this->db->query("UPDATE `".DB_PREFIX."product` SET price = '".(float)$result['base_price']."', quantity = '".(int)$result['cur_quantity']."', date_available = '".$this->db->escape(utf8_strtolower($result['cur_available_date']))."', subtract = '1'   WHERE product_id = '".(int)$result['product_id']."' ");

        $this->db->query("DELETE FROM `".DB_PREFIX."preorder_products` WHERE id = '".(int)$id."' ");

    }


    public function getTotalProducts($filter_data) {

        $sql = "SELECT COUNT(*) AS total FROM `".DB_PREFIX."preorder_products` pp LEFT JOIN `".DB_PREFIX."product` p ON p.product_id = pp.product_id LEFT JOIN `".DB_PREFIX."product_description` pd ON pp.product_id = pd.product_id WHERE pd.language_id='".$this->config->get('config_language_id')."' ";

        if($filter_data['name']){
            $sql .= " AND LCASE(pd.name) like '".$this->db->escape(utf8_strtolower($filter_data['name']))."%' ";
        }

        if($filter_data['model']){
            $sql .= " AND LCASE(p.model) like '".$this->db->escape(utf8_strtolower($filter_data['model']))."%' ";
        }

        if($filter_data['preorder_price']){
            $sql .= " AND pp.initial_price = ".(float)$filter_data['preorder_price'];
        }

        if($filter_data['deduction_type']){
            $sql .= " AND LCASE(pp.deduction_type) = '".$this->db->escape(utf8_strtolower($filter_data['deduction_type']))."' ";
        }

        if(isset($filter_data['status'])){
            $sql .= " AND pp.status = '".(int)$filter_data['status']."' ";
        }

        $result = $this->db->query($sql);

        return $result->row['total'];
    }

    public function getproducts($filter_data,$start = 0, $limit = 2){

        $sql = "SELECT pp.*,p.model,pd.name FROM `".DB_PREFIX."preorder_products` pp LEFT JOIN `".DB_PREFIX."product` p ON p.product_id = pp.product_id LEFT JOIN `".DB_PREFIX."product_description` pd ON pp.product_id = pd.product_id WHERE pd.language_id='".$this->config->get('config_language_id')."' ";

        if($filter_data['name']){
            $sql .= " AND LCASE(pd.name) like '".$this->db->escape(utf8_strtolower($filter_data['name']))."%' ";
        }

        if($filter_data['model']){
            $sql .= " AND LCASE(p.model) like '".$this->db->escape(utf8_strtolower($filter_data['model']))."%' ";
        }

        if($filter_data['preorder_price']){
            $sql .= " AND pp.initial_price = ".(float)$filter_data['preorder_price'];
        }

        if($filter_data['deduction_type']){
            $sql .= " AND LCASE(pp.deduction_type) = '".$this->db->escape(utf8_strtolower($filter_data['deduction_type']))."' ";
        }

        $sql .= " LIMIT " . (int)$start . "," . (int)$limit;
        $result = $this->db->query($sql)->rows;

        if($result){
            return $result;
        }else{
            false;
        }
    }

    private function isExist($id){
        $result = $this->db->query("SELECT product_id FROM `" .DB_PREFIX. "preorder_products` WHERE product_id = '".(int)$id."' AND status = 1 ")->row;
        return $result;
    }

    public function cretaePreOrderProduct($data){
        $products = explode(',',$data['wk_preorder_products_id']);
        $this->load->model('catalog/product');
        $faultId = '';
        $currectId = '';
        $alreadyAdded = '';
        $details = array();
        foreach ($products as $key => $value) {
            $details[$value] = $this->model_catalog_product->getProduct($value);
            if(empty($details[$value])){
                $faultId .= $value.",";
            }else{
                $chkId = $this->isExist($value);
                if(isset($chkId['product_id'])){
                    $alreadyAdded .= $value.",";
                    continue;
                }
                $currectId .= $value.",";
                $product_id = $value;
                $cur_price = $details[$value]['price'];
                if($data['wk_deduction_method'] == 'pc'){
                    $preorder_price =  round(( $details[$value]['price'] * $data['wk_percentage_price'] ) / 100);
                    $rem_price = round($details[$value]['price'] - $preorder_price);
                }else{
                        $preorder_price =  round($details[$value]['price'] - 0);
                        $rem_price = round($details[$value]['price'] - $preorder_price);
                }

                $cur_available_date = $details[$value]['date_available'];
                $cur_quantity = $details[$value]['quantity'];
                $sql = "INSERT INTO `" .DB_PREFIX. "preorder_products` VALUES ('', '".(int)$product_id."', '".(float)$cur_price."', '".(float)$preorder_price."', '".(float)$rem_price."', '".$this->db->escape(utf8_strtolower($data['wk_deduction_method']))."', '".(int)$cur_quantity."', '".$this->db->escape(($cur_available_date))."', '".$this->db->escape(($data['wk_available_date']))."', '".(int)$data['wk_percentage_price']."', '0','0', 1 ) ";

                $this->db->query($sql);

                $this->db->query("UPDATE `".DB_PREFIX."product` SET stock_status_id = 5, subtract = '1'  WHERE product_id = '".(int)$product_id."' ");
            }
        }

        $success = array(
            'failure' => $faultId,
            'success' => $currectId,
            'alreadyAdded' => $alreadyAdded,
        );
        return $success;
    }

    public function getPreorderProduct($product_id) {
        $data = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product_preorder_pro` WHERE product_id = '" . (int)$product_id . "'")->row;
        if($data AND isset($data['date']))
            return $data['date'];
        else
            return '' ;
    }

    public function getPreorderProductDetail($preorder_product_id){
        $result = $this->db->query("SELECT * FROM `".DB_PREFIX."preorder_products` WHERE id = '".(int)$preorder_product_id."' ")->row;
        return $result;
    }

    public function finalizePreOrder($details){
        $preorder_info = $this->db->query("SELECT * FROM `".DB_PREFIX."preordred_product_detail` WHERE order_id = '".(int)$details['order_id']."' && product_id = '".(int)$details['product_id']."' && customer_id = '".(int)$details['customer_id']."'  ")->row;
        $price = $preorder_info['paid_amount'] + $details['price'];
        $this->db->query("UPDATE `".DB_PREFIX."preordred_product_detail` SET status = 1 , paid_amount = '" . (int)$price . "' WHERE order_id = '".(int)$details['order_id']."' && product_id = '".(int)$details['product_id']."' && customer_id = '".(int)$details['customer_id']."'");
        $preorder_product_info = $this->getPreorderProductDetail($preorder_info['preorder_product_id']);
        if($preorder_product_info && $preorder_product_info['deduction_type'] == 'pc') {
            foreach($this->cart->getProducts() as $product) {
                if ($product['product_id'] == $details['product_id']) {
                    $cart = $product;
                    $total = 0;
                    $sub_total = 0;
                    $base_price = 0;
                    $total = $cart['total'];
    
                    $this->db->query("UPDATE `".DB_PREFIX."order_product` SET price = ".(float)$cart['price']." + price, total = ".(float)$cart['total']." + total WHERE order_id = '".(int)$details['order_id']."' AND product_id = '" . (int)$details['product_id'] . "'" );
    
                    $this->db->query("UPDATE `".DB_PREFIX."order_total` SET value = ".(float)$total." + value WHERE order_id = '".(int)$details['order_id']."' && code = 'total' ");
    
                    $this->db->query("UPDATE `".DB_PREFIX."order_total` SET value = ".(float)$cart['price']." + value WHERE order_id = '".(int)$details['order_id']."' && code = 'sub_total' ");
    
                    $this->db->query("UPDATE `".DB_PREFIX."order` SET total = ".(float)$total." + total WHERE order_id = '".(int)$details['order_id']."' ");
                }
                
            }
            
        }
   
        $msg = "As you have completed your order, it will be delivered soon to your doorstep.";
        $this->load->model('checkout/order');
        $this->model_checkout_order->addOrderHistory($details['order_id'],$this->config->get('module_wk_preorder_pro_order_status'),$msg,$this->config->get('module_wk_preorder_pro_mail_status'));

    }

    public function getOrderProducts($order_id){
        $result = $this->db->query("SELECT product_id,quantity FROM `".DB_PREFIX."order_product` WHERE order_id = ".(int)$order_id." ")->rows;
        return $result;
    }

    public function getProductByOrderId($order_id){
        $result = $this->db->query("SELECT ppd.*,p.stock_status_id,pp.rem_price FROM `".DB_PREFIX."preordred_product_detail` ppd LEFT JOIN `".DB_PREFIX."product` p ON p.product_id=ppd.product_id LEFT JOIN `".DB_PREFIX."preorder_products` pp ON pp.product_id=ppd.product_id WHERE ppd.order_id = '".(int)$order_id."' && ppd.customer_id = '".(int)$this->customer->getId()."' ")->rows;
        return $result;
    }

    public function checkPreorderQuantity($order_id,$product_id,$quantity){
        $checkQuantity = $this->db->query("SELECT * FROM `".DB_PREFIX."preordred_product_detail` WHERE product_id = '".(int)$product_id."' && order_id = '".(int)$order_id."' && customer_id = '".$this->customer->getId()."' && status = 0 ")->row;
        if(isset($checkQuantity['quantity']) && $checkQuantity['quantity'] == $quantity){
            return false;
        }else{
            return true;
        }
    }

    public function addPreorderProduct($product_id,$date) {
        $this->db->query("DELETE FROM `" . DB_PREFIX . "product_preorder_pro` WHERE product_id = '".(int)$product_id."'");
        $this->db->query("INSERT INTO `" . DB_PREFIX . "product_preorder_pro` SET product_id = '".(int)$product_id."' ,date = '".$this->db->escape($date)."'");
    }

    public function getPreorderOrder($order_id,$product_id = false) {
        $add = '';
        if($product_id)
            $add = "AND product_id = '" . (int)$product_id . "'";

        $data = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_preorder` WHERE order_id ='".(int)$order_id."'" . $add ."")->row;
        if($data AND isset($data['id']))
            return true;
        else
            return false ;
    }

    public function viewCustomers($data) {

        $sql = "SELECT * FROM `" . DB_PREFIX . "order_preorder_customer` opc WHERE 1 ";

        $implode = array();

        if (!empty($data['filter_name'])) {
            $implode[] = "LCASE(opc.name) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
        }

        if (!empty($data['filter_email'])) {
            $implode[] = "LCASE(opc.email) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_email'])) . "%'";
        }

        if (!empty($data['filter_query'])) {
            $implode[] = "LCASE(opc.query) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_query'])) . "%'";
        }

        if ($implode) {
            $sql .= " AND " . implode(" AND ", $implode);
        }

        $sort_data = array(
            'name',
            'email',
            'query',
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY opc.id";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function viewtotalCustomers($data) {

        $sql = "SELECT * FROM `" . DB_PREFIX . "order_preorder_customer` opc WHERE 1 ";

        $implode = array();

        if (!empty($data['filter_name'])) {
            $implode[] = "LCASE(opc.name) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
        }

        if (!empty($data['filter_email'])) {
            $implode[] = "LCASE(opc.email) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_email'])) . "%'";
        }

        if (!empty($data['filter_query'])) {
            $implode[] = "LCASE(opc.query) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_query'])) . "%'";
        }

        if ($implode) {
            $sql .= " AND " . implode(" AND ", $implode);
        }

        $sort_data = array(
            'name',
            'email',
            'query',
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY opc.id";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        $query = $this->db->query($sql);

        return count($query->rows);
    }

    public function viewProductsCustomer($c_id) {

        $data = $this->db->query("SELECT p.model,ppd.quantity,pd.name,p.image,ppd.product_id FROM `" . DB_PREFIX . "preorder_products` pp LEFT JOIN `" . DB_PREFIX . "preordred_product_detail` ppd ON (pp.id = ppd.preorder_product_id) LEFT JOIN `" . DB_PREFIX . "product` p ON (pp.product_id = p.product_id) LEFT JOIN `" . DB_PREFIX . "product_description` pd ON (p.product_id = pd.product_id) WHERE pp.c_id ='".$c_id."' AND pd.language_id = '".$this->config->get('config_language_id')."'")->rows;
        return $data;
    }

    public function deleteCustomerEntry($c_id) {

        $this->db->query("DELETE FROM `" . DB_PREFIX . "order_preorder` WHERE c_id = '$c_id'");
        $this->db->query("DELETE FROM `" . DB_PREFIX . "order_preorder_customer` WHERE id = '$c_id'");
    }

    public function viewOrders($data) {

        $sql = "SELECT DISTINCT o.order_id,o.firstname,o.lastname,o.date_added FROM `" . DB_PREFIX . "order_preorder` op LEFT JOIN `" . DB_PREFIX . "order` o ON (op.order_id = o.order_id) WHERE op.order_id > 0  ";

        $implode = array();

        if (!empty($data['filter_id'])) {
            $implode[] = "op.order_id = '" . (int)$data['filter_id'] . "'";
        }

        if (!empty($data['filter_name'])) {
            $implode[] = "LCASE(CONCAT(o.firstname,' ',o.lastname)) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
        }

        if (!empty($data['filter_date'])) {
            $implode[] = "LCASE(o.date_added) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_date'])) . "%'";
        }

        if ($implode) {
            $sql .= " AND " . implode(" AND ", $implode);
        }

        $sort_data = array(
            'o.firstname',
            'o.date_added',
            'op.order_id',
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY op.id";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function viewtotalOrders($data) {

         $sql = "SELECT DISTINCT o.order_id,o.firstname,o.lastname,o.date_added FROM `" . DB_PREFIX . "order_preorder` op LEFT JOIN `" . DB_PREFIX . "order` o ON (op.order_id = o.order_id) WHERE op.order_id > 0  ";

        $implode = array();

        if (!empty($data['filter_id'])) {
            $implode[] = "op.order_id = '" . (int)$data['filter_id'] . "'";
        }

        if (!empty($data['filter_name'])) {
            $implode[] = "LCASE(CONCAT(o.firstname,' ',o.lastname)) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
        }

        if (!empty($data['filter_date'])) {
            $implode[] = "LCASE(o.date_added) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_date'])) . "%'";
        }

        if ($implode) {
            $sql .= " AND " . implode(" AND ", $implode);
        }

        $sort_data = array(
            'o.firstname',
            'o.date_added',
            'op.order_id',
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY op.id";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        $query = $this->db->query($sql);

        return count($query->rows);
    }

    public function viewProductsOrder($o_id) {

        $data = $this->db->query("SELECT p.model,op.quantity,pd.name,p.image,p.product_id FROM `" . DB_PREFIX . "order_preorder` op LEFT JOIN `" . DB_PREFIX . "product` p ON (op.product_id = p.product_id) LEFT JOIN `" . DB_PREFIX . "product_description` pd ON (p.product_id = pd.product_id) WHERE op.order_id ='".$o_id."' AND pd.language_id = '".$this->config->get('config_language_id')."'")->rows;
        return $data;
    }

    public function getCustomerPreorderProduct($id) {

        $data = $this->db->query("SELECT p.model,ppd.quantity,pd.name,p.image,p.product_id FROM `" . DB_PREFIX . "preordred_product_detail` ppd LEFT JOIN `" . DB_PREFIX . "product` p ON (ppd.product_id = p.product_id) LEFT JOIN `" . DB_PREFIX . "product_description` pd ON (p.product_id = pd.product_id) WHERE ppd.order_id ='".$id."' AND ppd.notified = '0' AND pd.language_id = '".$this->config->get('config_language_id')."'")->rows;
        return $data;
    }

    public function deleteOrderEntry($o_id) {
        $this->db->query("DELETE FROM `" . DB_PREFIX . "order_preorder` WHERE order_id = '$o_id'");
    }


    public function deleteOrder($id){

    $this->db->query("DELETE FROM `".DB_PREFIX."preordred_product_detail` WHERE id = '".$id."' ");
    $this->db->query("DELETE FROM `".DB_PREFIX."order` WHERE order_id = '".$id."' ");
    }


    public function emailToCustomer($id,$type){

        // Send out mail to admin about preorder
        $language = new Language('catalog/');

        $language->load('catalog/precartmail');

        // $template = new Template();

        $data['text_hello'] = $language->get('text_hello');
        $data['text_name'] = $this->config->get('config_name');
        $data['text_cname'] = $language->get('name');
        $data['text_subject'] = $language->get('text_subject');
        $data['text_email'] = $language->get('email');
        $data['text_query'] = $language->get('query');
        $data['text_pname'] = $language->get('text_pname');
        $data['text_pqty'] = $language->get('text_pqty');
        $data['text_pimage'] = $language->get('text_pimage');
        $data['text_pmodel'] = $language->get('text_pmodel');
        $data['text_thanksadmin'] = $language->get('text_thanksadmin');

        if ($type=="order") {
            $customer = $this->db->query("SELECT email,firstname FROM `".DB_PREFIX."customer` c LEFT JOIN `".DB_PREFIX."preordred_product_detail` ppd ON (ppd.customer_id=c.customer_id) WHERE ppd.order_id = '".(int)$id."'")->row;
            $data['customer']['email'] = $customer['email'];
            $data['customer']['name'] = $customer['firstname'];
            $data['products'] = $this->getCustomerPreorderProduct($id);
        } else {
            $data['customer'] = $this->db->query("SELECT * FROM `".DB_PREFIX."order_preorder_customer` WHERE order_id = '".(int)$id."'")->row;
            $data['products'] = $this->viewProductsCustomer($id);
        }

        $data['store_name'] = $this->config->get('store_name');
        $data['store_url'] = HTTP_CATALOG;
        $data['product_link'] = HTTP_CATALOG.'index.php?route=product/product&product_id=';
        $data['logo'] = HTTP_CATALOG.'image/' . $this->config->get('config_logo');
        $html = $this->load->view('catalog/precartmail',$data);

        $tocustomer=array();
        $tocustomer['emailto']=$data['customer']['email'];
        $tocustomer['message']=$html;
        $tocustomer['mailfrom']=$this->config->get('config_email');
        $tocustomer['subject']=$data['text_subject'];
        $tocustomer['name']=$data['customer']['name'];

        $this->sendMail($tocustomer);

        if($type="order"){
            $this->db->query("UPDATE `".DB_PREFIX."preordred_product_detail` SET notified = 1 WHERE order_id = '$id'");
        }else{
            $this->db->query("UPDATE `".DB_PREFIX."order_preorder_customer` SET notify = 1 WHERE id = '$id'");
        }

    }


    public function sendMail($data){

        $text = $data['message'];

        if(VERSION == '2.0.0.0' || VERSION == '2.0.1.0' || VERSION == '2.0.1.1'  ) {
            /*Old mail code*/
            $mail = new Mail($this->config->get('config_mail'));
            $mail->setTo($data['emailto']);
            $mail->setFrom($data['mailfrom']);
            $mail->setSender($data['name']);
            $mail->setSubject(html_entity_decode($data['subject'], ENT_QUOTES, 'UTF-8'));
            $mail->setHtml(html_entity_decode($text, ENT_QUOTES, 'UTF-8'));
            $mail->send();
        } else {
            $mail = new Mail();
            $mail->protocol = $this->config->get('config_mail_protocol');
            $mail->parameter = $this->config->get('config_mail_parameter');
            $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
            $mail->smtp_username = $this->config->get('config_mail_smtp_username');
            $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
            $mail->smtp_port = $this->config->get('config_mail_smtp_port');
            $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
            $mail->setTo($data['emailto']);
            $mail->setFrom($data['mailfrom']);
            $mail->setSender($data['name']);
            $mail->setSubject(html_entity_decode($data['subject'], ENT_QUOTES, 'UTF-8'));
            $mail->setHtml(html_entity_decode($text, ENT_QUOTES, 'UTF-8'));
            $mail->send();
        }
    }

    public function checkPreordered($product_id,$customer_id){
        $result = $this->db->query("SELECT id FROM `".DB_PREFIX."preordred_product_detail` WHERE product_id = '".(int)$product_id."' && customer_id = '".(int)$customer_id."' && status = 0 ")->row;
        if(isset($result['id'])){
            return true;
        }else{
            return false;
        }
    }

    public function addPreorderOrder($details, $order_status_id = 0){

      $this->db->query("UPDATE`" .DB_PREFIX. "preorder_products` SET preorder_quantity = (preorder_quantity - " . (int)$details['quantity'] . ") WHERE product_id = '".(int)$details['product_id']."' ");

      $this->db->query("INSERT INTO `".DB_PREFIX."preordred_product_detail` VALUES ('', '".(int)$details['order_id']."' , '".(int)$details['product_id']."', '".$details['preorder_product_id']."', '".(int)$details['customer_id']."', '".(int)$details['quantity']."', '0', '".(int)$details['status']."', '".(int)$details['price']."') ");

      if ($order_status_id) {
        $this->db->query("UPDATE`" .DB_PREFIX. "order` SET order_status_id = '" . (int)$order_status_id . "', date_modified = NOW() WHERE order_id = '" . (int)$details['order_id'] . "'");
      }
    }   

    public function addPreorderOrderprecart($data,$c_id,$order_id = 0) {
        $this->db->query("INSERT INTO `" . DB_PREFIX . "order_preorder` SET product_id='".$data['product_id']."',quantity='".$data['quantity']."',c_id = '".$c_id."',order_id = '".$order_id."' ");
    }

    public function addPreorderOrderCustomer($data) {
        $cid = 0;
        if($this->customer->getId())
            $cid = $this->customer->getId();

        $this->db->query("INSERT INTO `" . DB_PREFIX . "order_preorder_customer` SET customer_id='".$cid."',name='".$this->db->escape(utf8_strtolower($data['name']))."',email='".$this->db->escape(utf8_strtolower($data['email']))."',query='".$this->db->escape(utf8_strtolower($data['text']))."' ");

        return $this->db->getLastId();
    }

    public function SendPrecartMail($products,$data){

        // Send out mail to admin about preorder
        $language = new Language('catalog/');

        $language->load('checkout/precartmail');

        // $template = new Template();

        $data['text_hello'] = $language->get('text_hello');
        $data['text_name'] = $this->config->get('config_name');
        $data['text_cname'] = $language->get('name');
        $data['text_subject'] = $language->get('text_subject');
        $data['text_email'] = $language->get('email');
        $data['text_query'] = $language->get('query');
        $data['text_pname'] = $language->get('text_pname');
        $data['text_pqty'] = $language->get('text_pqty');
        $data['text_pimage'] = $language->get('text_pimage');
        $data['text_pmodel'] = $language->get('text_pmodel');

        $data['customer'] = $data;
         $data['products'] = $products;

        $data['store_name'] = $this->config->get('store_name');
        $data['store_url'] = HTTP_SERVER;
        $data['product_link'] = HTTP_SERVER.'index.php?route=product/product&product_id=';
        $data['logo'] = HTTP_SERVER.'image/' . $this->config->get('config_logo');

        if (version_compare(VERSION, '2.2.0.0', '>=')) {
            $html = $this->load->view('checkout/precartmail',$data);
        } else {
           $html = $this->load->view('default/template/checkout/precartmail.tpl',$data);
        }

        $tocustomer=array();
        $tocustomer['emailto']=$this->config->get('config_email');
        $tocustomer['message']=$html;
        $tocustomer['mailfrom']=$data['email'];
        $tocustomer['subject']=$data['text_subject'];
        $tocustomer['name']=$this->config->get('config_name');
        $this->sendMail($tocustomer);
    }

    public function customer_preordered($product_id){
        $result = $this->db->query("SELECT id FROM `".DB_PREFIX."preordred_product_detail` WHERE customer_id = '".$this->customer->getId()."' AND status = '0' AND product_id = '".(int)$product_id."' ")->row;
        if(isset($result['id'])){
            return true;
        }else{
            return false;
        }
    }

    public function getInitialPrice($id){
        $result = $this->db->query("SELECT initial_price FROM `".DB_PREFIX."preorder_products` WHERE product_id = '".(int)$id."' ")->row;
        if(isset($result['initial-price'])){
            $price = $result['initial-price'];
        }else{
            $price = 0;
        }
        return $price;
    }
    public function sendNotification($order_id = 0) {
        $results = $this->db->query("SELECT * FROM " . DB_PREFIX . "preorder_push")->rows;
        foreach($results as $result) {
            if (strpos($result['token'], 'mozilla') !== false) {
               $this->dispatchToMozilla($result['token'], $order_id);
            } else {
               $this->dispatchToChrome($result['token'], $order_id);
            }
        }
    }
    public function dispatchToMozilla($cus_id, $order_id) {
        
        $this->log->write("dispatchToMozilla");
       
		$url = 'https://updates.push.services.mozilla.com/wpush/v1/'.$cus_id;

	    $headers = array(
	        'Content-Type: application/json',
	        'TTL: 600000',
	    );

	    $ch = curl_init();

	    // Set the url, number of POST vars, POST data
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_POST, true);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    // curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $response = curl_exec($ch);
        
        $this->log->write( $response);
      
	    curl_close($ch);
	    return $response;
	}

    public function dispatchToChrome($order_id = 0) {

        $this->log->write("dispatchToMozilla");
        $this->load->language('checkout/precart');
		$url = 'https://fcm.googleapis.com/fcm/send';
		$headers = array(
			'Authorization: key=' . $this->config->get('module_wk_preorder_pro_server_key'),
			'Content-Type: application/json'
		);
		$results = $this->db->query("SELECT * FROM " . DB_PREFIX . "preorder_push")->rows;
		foreach ($results as $key => $value) {

			$message = array(
				'title'		=> 'Pre Order Product',
				'content'	=> 'Pre Order',
				'subTitle'	=> 'Pre Order',
				'body'		=> $this->language->get('text_msg'),
			);
			$fields = array(
				'to'			=> $value['token'],
				'data'			=> $message,
				'notification'	=> $message,
				'time_to_live'	=> 30,
				'delay_while_idle'	=> true,
				'priority'          => 'high',
				'content_available' => true,
				'mutable_content' =>  true
			);
		
					// Open connection
					$ch = curl_init();
			
					// Set the url, number of POST vars, POST data
					curl_setopt($ch, CURLOPT_URL, $url);
			
					curl_setopt($ch, CURLOPT_POST, true);
					curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			
					// Disabling SSL Certificate support temporarly
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			
					curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
			
					// Execute post
					$result = curl_exec($ch);
				
					$this->log->write($result);
					
			
		}
	}
}
?>
