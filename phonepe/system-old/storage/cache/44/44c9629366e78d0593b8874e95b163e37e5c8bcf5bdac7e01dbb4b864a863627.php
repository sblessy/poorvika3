<?php

/* default/template/account/wk_preorder_list.twig */
class __TwigTemplate_89f5c40ac94f85ded031d3337bd1371462d3d36eb6b333c09094d115b7540afa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo " 
<div class=\"container\">
  <ul class=\"breadcrumb\">
    ";
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            echo " 
    <li><a href=\"";
            // line 5
            echo $this->getAttribute($context["breadcrumb"], "href", array(), "array");
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array(), "array");
            echo "</a></li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 6
        echo " 
  </ul>
  ";
        // line 8
        if ((isset($context["success"]) ? $context["success"] : null)) {
            echo " 
  <div class=\"alert alert-success\"><i class=\"fa fa-check-circle\"></i> ";
            // line 9
            echo (isset($context["success"]) ? $context["success"] : null);
            echo "</div>
  ";
        }
        // line 10
        echo " 
  <div class=\"row\">";
        // line 11
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo " 
    ";
        // line 12
        if (((isset($context["column_left"]) ? $context["column_left"] : null) && (isset($context["column_right"]) ? $context["column_right"] : null))) {
            echo " 
    ";
            // line 13
            $context["class"] = "col-sm-6";
            echo " 
    ";
        } elseif ((        // line 14
(isset($context["column_left"]) ? $context["column_left"] : null) || (isset($context["column_right"]) ? $context["column_right"] : null))) {
            echo " 
    ";
            // line 15
            $context["class"] = "col-sm-9";
            echo " 
    ";
        } else {
            // line 16
            echo " 
    ";
            // line 17
            $context["class"] = "col-sm-12";
            echo " 
    ";
        }
        // line 18
        echo " 
    <div id=\"content\" class=\"";
        // line 19
        echo (isset($context["class"]) ? $context["class"] : null);
        echo "\">";
        echo (isset($context["content_top"]) ? $context["content_top"] : null);
        echo " 
      <h1>";
        // line 20
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
      <div class=\"alert alert-info\">
        <i class=\"fa fa-info-circle\"></i>
        ";
        // line 23
        echo (isset($context["text_currency_info"]) ? $context["text_currency_info"] : null);
        echo " 
        <a href=\"";
        // line 24
        echo (isset($context["currency_change_url"]) ? $context["currency_change_url"] : null);
        echo "\" >";
        echo (isset($context["text_change_currency"]) ? $context["text_change_currency"] : null);
        echo "</a>
      </div>
      <div class=\"table-reponsive\">
        <table class=\"table table-hover table-bordered\"
          <thead>
            <tr>
              <td class=\"text-left\">";
        // line 30
        echo (isset($context["column_order_id"]) ? $context["column_order_id"] : null);
        echo "</td>
              <td class=\"text-left\">";
        // line 31
        echo (isset($context["column_customer_name"]) ? $context["column_customer_name"] : null);
        echo "</td>
              <td class=\"text-left\">";
        // line 32
        echo (isset($context["column_product_name"]) ? $context["column_product_name"] : null);
        echo "</td>
              <td class=\"text-left\">";
        // line 33
        echo (isset($context["column_quantity"]) ? $context["column_quantity"] : null);
        echo "</td>
              <td class=\"text-left\">";
        // line 34
        echo (isset($context["column_actual_price"]) ? $context["column_actual_price"] : null);
        echo "</td>
              <td class=\"text-left\">";
        // line 35
        echo (isset($context["column_rem_amount"]) ? $context["column_rem_amount"] : null);
        echo "</td>
              <td class=\"text-left\">";
        // line 36
        echo (isset($context["column_paid_amount"]) ? $context["column_paid_amount"] : null);
        echo "</td>
              <td class=\"text-left\">";
        // line 37
        echo (isset($context["column_status"]) ? $context["column_status"] : null);
        echo "</td>
              <td class=\"text-left\">";
        // line 38
        echo (isset($context["column_date_added"]) ? $context["column_date_added"] : null);
        echo "</td>
              <td class=\"text-center\">";
        // line 39
        echo (isset($context["column_action"]) ? $context["column_action"] : null);
        echo "</td>
            </tr>
          </thead>
          <tbody>
            ";
        // line 43
        if ((array_key_exists("preorders", $context) && (isset($context["preorders"]) ? $context["preorders"] : null))) {
            echo " 
              ";
            // line 44
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["preorders"]) ? $context["preorders"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["preorder"]) {
                echo " 
                <tr>
                  <td class=\"text-left\">";
                // line 46
                echo ("#" . $this->getAttribute($context["preorder"], "order_id", array(), "array"));
                echo "</td>
                  <td class=\"text-left\">";
                // line 47
                echo $this->getAttribute($context["preorder"], "name", array(), "array");
                echo "</td>
                  <td class=\"text-left\">";
                // line 48
                echo $this->getAttribute($context["preorder"], "product_name", array(), "array");
                echo "</td>
                  <td class=\"text-left\">";
                // line 49
                echo $this->getAttribute($context["preorder"], "quantity", array(), "array");
                echo "</td>
                  <td class=\"text-left\">";
                // line 50
                echo $this->getAttribute($context["preorder"], "base_price", array(), "array");
                echo "</td>
                  <td class=\"text-left\">";
                // line 51
                echo $this->getAttribute($context["preorder"], "rem_price", array(), "array");
                echo "</td>
                  <td class=\"text-left\">
                    <div class=\"wkpopover text-info\" data-toggle=\"popover\" title=\"";
                // line 53
                echo (isset($context["text_order_total_detail"]) ? $context["text_order_total_detail"] : null);
                echo "\" data-content=\"
                      ";
                // line 54
                echo ("Product Price : " . $this->getAttribute($context["preorder"], "product_price", array(), "array"));
                echo "</br>
                      ";
                // line 55
                if (($this->getAttribute($context["preorder"], "extra_total", array(), "array", true, true) && $this->getAttribute($context["preorder"], "extra_total", array(), "array"))) {
                    echo " 
                        <table>
                        ";
                    // line 57
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["preorder"], "extra_total", array(), "array"));
                    foreach ($context['_seq'] as $context["_key"] => $context["extra_total"]) {
                        echo " 
                          ";
                        // line 58
                        echo ((($this->getAttribute($context["extra_total"], "title", array(), "array") . " : ") . $this->getAttribute($context["extra_total"], "text", array(), "array")) . "</br>");
                        echo " 
                        ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['extra_total'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 59
                    echo " 
                        </table>
                      ";
                }
                // line 61
                echo " 
                    \">
                      ";
                // line 63
                echo $this->getAttribute($context["preorder"], "total", array(), "array");
                echo " 
                    </div>
                  </td>
                  <td class=\"text-left\">";
                // line 66
                echo (($this->getAttribute($context["preorder"], "status", array(), "array")) ? ((isset($context["text_complete_paid"]) ? $context["text_complete_paid"] : null)) : ((isset($context["text_partial_paid"]) ? $context["text_partial_paid"] : null)));
                echo "</td>
                  <td class=\"text-left\">";
                // line 67
                echo $this->getAttribute($context["preorder"], "date_added", array(), "array");
                echo "</td>
                  <td class=\"text-center\">
                    <input type=\"hidden\" name=\"product_id\" value=\"";
                // line 69
                echo $this->getAttribute($context["preorder"], "product_id", array(), "array");
                echo "\" >
                    <input type=\"hidden\" name=\"quantity\" value=\"";
                // line 70
                echo $this->getAttribute($context["preorder"], "quantity", array(), "array");
                echo "\" >
                    ";
                // line 71
                if (($this->getAttribute($context["preorder"], "options", array(), "array") && $this->getAttribute($context["preorder"], "options", array(), "array"))) {
                    echo " 
                      ";
                    // line 72
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["preorder"], "options", array(), "array"));
                    foreach ($context['_seq'] as $context["_key"] => $context["option"]) {
                        echo " 
                        ";
                        // line 73
                        if (($this->getAttribute($context["option"], "type", array(), "array") == "checkbox")) {
                            echo " 
                          <input type=\"hidden\" class=\"options\" name=\"option[";
                            // line 74
                            echo $this->getAttribute($context["option"], "product_option_id", array(), "array");
                            echo "][]\" value=\"";
                            echo $this->getAttribute($context["option"], "product_option_value_id", array(), "array");
                            echo "\" >
                        ";
                        } elseif ((((($this->getAttribute(                        // line 75
$context["option"], "type", array(), "array") == "date") || ($this->getAttribute($context["option"], "type", array(), "array") == "datetime")) || ($this->getAttribute($context["option"], "type", array(), "array") == "text")) || ($this->getAttribute($context["option"], "type", array(), "array") == "textarea"))) {
                            echo " 
                          <input type=\"hidden\" class=\"options\" name=\"option[";
                            // line 76
                            echo $this->getAttribute($context["option"], "product_option_id", array(), "array");
                            echo "]\" value=\"";
                            echo $this->getAttribute($context["option"], "value", array(), "array");
                            echo "\" >
                        ";
                        } else {
                            // line 77
                            echo " 
                          <input type=\"hidden\" class=\"options\" name=\"option[";
                            // line 78
                            echo $this->getAttribute($context["option"], "product_option_id", array(), "array");
                            echo "]\" value=\"";
                            echo $this->getAttribute($context["option"], "product_option_value_id", array(), "array");
                            echo "\" >
                        ";
                        }
                        // line 79
                        echo " 
                      ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 80
                    echo " 
                    ";
                }
                // line 81
                echo " 
                    ";
                // line 82
                if (( !$this->getAttribute($context["preorder"], "status", array(), "array") && ($this->getAttribute($context["preorder"], "stock_status_id", array(), "array") == (isset($context["module_wk_preorder_pro_notification_status"]) ? $context["module_wk_preorder_pro_notification_status"] : null)))) {
                    echo " 
                      <button class=\"btn btn-primary add-to-cart\" data-wk-o-d=\"";
                    // line 83
                    echo $this->getAttribute($context["preorder"], "order_id", array(), "array");
                    echo "\" type=\"button\" >
                        <i class=\"fa fa-shopping-cart\"></i>
                      </button>
                    ";
                } else {
                    // line 86
                    echo " 
                      <button class=\"btn btn-primary\" disabled type=\"button\" >
                        <i class=\"fa fa-shopping-cart\"></i>
                      </button>
                    ";
                }
                // line 90
                echo " 
                  </td>
                </tr>
              ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['preorder'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 93
            echo " 
            ";
        } else {
            // line 94
            echo " 
              <tr>
                  <td class=\"text-center\" colspan=\"10\">";
            // line 96
            echo (isset($context["text_no_record"]) ? $context["text_no_record"] : null);
            echo "</td>
              </tr>
            ";
        }
        // line 98
        echo " 
          </tbody>
        </table>
      </div>
      <div class=\"row\">
        <div class=\"col-sm-6 text-left\">";
        // line 103
        echo (isset($context["pagination"]) ? $context["pagination"] : null);
        echo "</div>
        <div class=\"col-sm-6 text-right\">";
        // line 104
        echo (isset($context["results"]) ? $context["results"] : null);
        echo "</div>
      </div>
      ";
        // line 106
        echo (isset($context["content_bottom"]) ? $context["content_bottom"] : null);
        echo "</div>
    ";
        // line 107
        echo (isset($context["column_right"]) ? $context["column_right"] : null);
        echo "</div>
</div>
<script>

  \$('.wkpopover').popover({
    html: true,
    placement: 'top',
    trigger: 'hover',
  });

  var enableCompletionPreorder = function(order_id) {
    \$.ajax({
      url:'index.php?route=checkout/precart/set_session_to_confirm_order&order_id=' + order_id,
      type:'post',
      dataType:'json',
      success:function(response) {
        if(response.success) {
          \$('.breadcrumb').after('<div class=\"alert alert-success preorder-alert\"><i class=\"fa fa-check-circle\"></i> '+response.msg+'<button class=\"close\" data-dismiss=\"alert\" type=\"button\">&times;</button></div>');
                \$('html, body').animate({ scrollTop: 0 }, 'slow');
        } else {
          \$('.breadcrumb').after('<div class=\"alert alert-danger preorder-alert\"><i class=\"fa fa-exclamation-circle\"></i> '+response.msg+'<button class=\"close\" data-dismiss=\"alert\" type=\"button\">&times;</button></div>');
                \$('html, body').animate({ scrollTop: 0 }, 'slow');
        }
      }
    })
  }

  \$('.add-to-cart').on('click', function() {
    \$this = \$(this);
    order_id = \$(this).data('wk-o-d');
    data = new Object();
    data.product_id = \$(this).prevAll('input[name=\"product_id\"]').val();
    data.quantity = \$(this).prevAll('input[name=\"quantity\"]').val();
    options = \$(this).prevAll('.options');
    \$.each(options, function(index, value) {
      data[this.name] = this.value;
    })
    \$.ajax({
        url:'index.php?route=checkout/precart/customer_preordered',
        data: {
            product_id: data.product_id,
        },
        type:'post',
        dataType: 'json',
        beforeSend: function() {
          \$('.alert').remove();
        },
        success:function(response) {
            if(response.success) {
                enableCompletionPreorder(order_id);
                addToCart(data, \$this);
            } else {
                \$('.breadcrumb').after('<div class=\"alert alert-danger preorder-alert\"><i class=\"fa fa-exclamation-circle\"></i>'+response.msg+'<button class=\"close\" data-dismiss=\"alert\" type=\"button\">&times;</button></div>');
                \$('html, body').animate({ scrollTop: 0 }, 'slow');
            }
        }
    });
  });

  function addToCart(data, \$this) {
    \$.ajax({
      url: 'index.php?route=checkout/cart/add',
      data: data,
      type: 'post',
      dataType: 'json',
      beforeSend: function() {
        \$this.button('loading');
      },
      complete: function() {
        \$this.button('reset');
      },
      success: function(json) {
        
        if (json['success']) {
          \$('.breadcrumb').after('<div class=\"alert alert-success\">' + json['success'] + '<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>');

          \$('#cart > button').html('<span id=\"cart-total\"><i class=\"fa fa-shopping-cart\"></i> ' + json['total'] + '</span>');

          \$('html, body').animate({ scrollTop: 0 }, 'slow');

          \$('#cart > ul').load('index.php?route=common/cart/info ul li');
        }
         if (json['error']) {
           \$('.alert').remove();
          \$('.breadcrumb').after('<div class=\"alert alert-danger\">' + json['error'] + '<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>');
           \$('html, body').animate({ scrollTop: 0 }, 'slow');
         }

      }
    });
  }

</script>
";
        // line 200
        echo (isset($context["footer"]) ? $context["footer"] : null);
        echo " 
";
    }

    public function getTemplateName()
    {
        return "default/template/account/wk_preorder_list.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  466 => 200,  370 => 107,  366 => 106,  361 => 104,  357 => 103,  350 => 98,  344 => 96,  340 => 94,  336 => 93,  327 => 90,  320 => 86,  313 => 83,  309 => 82,  306 => 81,  302 => 80,  295 => 79,  288 => 78,  285 => 77,  278 => 76,  274 => 75,  268 => 74,  264 => 73,  258 => 72,  254 => 71,  250 => 70,  246 => 69,  241 => 67,  237 => 66,  231 => 63,  227 => 61,  222 => 59,  214 => 58,  208 => 57,  203 => 55,  199 => 54,  195 => 53,  190 => 51,  186 => 50,  182 => 49,  178 => 48,  174 => 47,  170 => 46,  163 => 44,  159 => 43,  152 => 39,  148 => 38,  144 => 37,  140 => 36,  136 => 35,  132 => 34,  128 => 33,  124 => 32,  120 => 31,  116 => 30,  105 => 24,  101 => 23,  95 => 20,  89 => 19,  86 => 18,  81 => 17,  78 => 16,  73 => 15,  69 => 14,  65 => 13,  61 => 12,  57 => 11,  54 => 10,  49 => 9,  45 => 8,  41 => 6,  31 => 5,  25 => 4,  19 => 1,);
    }
}
/* {{ header }} */
/* <div class="container">*/
/*   <ul class="breadcrumb">*/
/*     {% for breadcrumb in breadcrumbs %} */
/*     <li><a href="{{ breadcrumb['href'] }}">{{ breadcrumb['text'] }}</a></li>*/
/*     {% endfor %} */
/*   </ul>*/
/*   {% if (success) %} */
/*   <div class="alert alert-success"><i class="fa fa-check-circle"></i> {{ success }}</div>*/
/*   {% endif %} */
/*   <div class="row">{{ column_left }} */
/*     {% if (column_left and column_right) %} */
/*     {% set class = 'col-sm-6' %} */
/*     {% elseif (column_left or column_right) %} */
/*     {% set class = 'col-sm-9' %} */
/*     {% else %} */
/*     {% set class = 'col-sm-12' %} */
/*     {% endif %} */
/*     <div id="content" class="{{ class }}">{{ content_top }} */
/*       <h1>{{ heading_title }}</h1>*/
/*       <div class="alert alert-info">*/
/*         <i class="fa fa-info-circle"></i>*/
/*         {{ text_currency_info }} */
/*         <a href="{{ currency_change_url }}" >{{ text_change_currency }}</a>*/
/*       </div>*/
/*       <div class="table-reponsive">*/
/*         <table class="table table-hover table-bordered"*/
/*           <thead>*/
/*             <tr>*/
/*               <td class="text-left">{{ column_order_id }}</td>*/
/*               <td class="text-left">{{ column_customer_name }}</td>*/
/*               <td class="text-left">{{ column_product_name }}</td>*/
/*               <td class="text-left">{{ column_quantity }}</td>*/
/*               <td class="text-left">{{ column_actual_price }}</td>*/
/*               <td class="text-left">{{ column_rem_amount }}</td>*/
/*               <td class="text-left">{{ column_paid_amount }}</td>*/
/*               <td class="text-left">{{ column_status }}</td>*/
/*               <td class="text-left">{{ column_date_added }}</td>*/
/*               <td class="text-center">{{ column_action }}</td>*/
/*             </tr>*/
/*           </thead>*/
/*           <tbody>*/
/*             {% if (preorders is defined and preorders) %} */
/*               {% for preorder in preorders %} */
/*                 <tr>*/
/*                   <td class="text-left">{{ "#"~preorder['order_id'] }}</td>*/
/*                   <td class="text-left">{{ preorder['name'] }}</td>*/
/*                   <td class="text-left">{{ preorder['product_name'] }}</td>*/
/*                   <td class="text-left">{{ preorder['quantity'] }}</td>*/
/*                   <td class="text-left">{{ preorder['base_price'] }}</td>*/
/*                   <td class="text-left">{{ preorder['rem_price'] }}</td>*/
/*                   <td class="text-left">*/
/*                     <div class="wkpopover text-info" data-toggle="popover" title="{{ text_order_total_detail }}" data-content="*/
/*                       {{ 'Product Price : '~preorder['product_price'] }}</br>*/
/*                       {% if (preorder['extra_total'] is defined and preorder['extra_total']) %} */
/*                         <table>*/
/*                         {% for extra_total in preorder['extra_total'] %} */
/*                           {{ extra_total['title']~" : "~extra_total['text']~"</br>" }} */
/*                         {% endfor %} */
/*                         </table>*/
/*                       {% endif %} */
/*                     ">*/
/*                       {{ preorder['total'] }} */
/*                     </div>*/
/*                   </td>*/
/*                   <td class="text-left">{{ preorder['status'] ? text_complete_paid : text_partial_paid }}</td>*/
/*                   <td class="text-left">{{ preorder['date_added'] }}</td>*/
/*                   <td class="text-center">*/
/*                     <input type="hidden" name="product_id" value="{{ preorder['product_id'] }}" >*/
/*                     <input type="hidden" name="quantity" value="{{ preorder['quantity'] }}" >*/
/*                     {% if (preorder['options'] and preorder['options']) %} */
/*                       {% for option in preorder['options'] %} */
/*                         {% if (option['type'] == 'checkbox') %} */
/*                           <input type="hidden" class="options" name="option[{{ option['product_option_id'] }}][]" value="{{ option['product_option_value_id'] }}" >*/
/*                         {% elseif (option['type'] == 'date' or option['type'] == 'datetime' or option['type'] == 'text' or option['type'] == 'textarea') %} */
/*                           <input type="hidden" class="options" name="option[{{ option['product_option_id'] }}]" value="{{ option['value'] }}" >*/
/*                         {% else %} */
/*                           <input type="hidden" class="options" name="option[{{ option['product_option_id'] }}]" value="{{ option['product_option_value_id'] }}" >*/
/*                         {% endif %} */
/*                       {% endfor %} */
/*                     {% endif %} */
/*                     {% if (not preorder['status'] and preorder['stock_status_id'] == module_wk_preorder_pro_notification_status) %} */
/*                       <button class="btn btn-primary add-to-cart" data-wk-o-d="{{ preorder['order_id'] }}" type="button" >*/
/*                         <i class="fa fa-shopping-cart"></i>*/
/*                       </button>*/
/*                     {% else %} */
/*                       <button class="btn btn-primary" disabled type="button" >*/
/*                         <i class="fa fa-shopping-cart"></i>*/
/*                       </button>*/
/*                     {% endif %} */
/*                   </td>*/
/*                 </tr>*/
/*               {% endfor %} */
/*             {% else %} */
/*               <tr>*/
/*                   <td class="text-center" colspan="10">{{ text_no_record }}</td>*/
/*               </tr>*/
/*             {% endif %} */
/*           </tbody>*/
/*         </table>*/
/*       </div>*/
/*       <div class="row">*/
/*         <div class="col-sm-6 text-left">{{ pagination }}</div>*/
/*         <div class="col-sm-6 text-right">{{ results }}</div>*/
/*       </div>*/
/*       {{ content_bottom }}</div>*/
/*     {{ column_right }}</div>*/
/* </div>*/
/* <script>*/
/* */
/*   $('.wkpopover').popover({*/
/*     html: true,*/
/*     placement: 'top',*/
/*     trigger: 'hover',*/
/*   });*/
/* */
/*   var enableCompletionPreorder = function(order_id) {*/
/*     $.ajax({*/
/*       url:'index.php?route=checkout/precart/set_session_to_confirm_order&order_id=' + order_id,*/
/*       type:'post',*/
/*       dataType:'json',*/
/*       success:function(response) {*/
/*         if(response.success) {*/
/*           $('.breadcrumb').after('<div class="alert alert-success preorder-alert"><i class="fa fa-check-circle"></i> '+response.msg+'<button class="close" data-dismiss="alert" type="button">&times;</button></div>');*/
/*                 $('html, body').animate({ scrollTop: 0 }, 'slow');*/
/*         } else {*/
/*           $('.breadcrumb').after('<div class="alert alert-danger preorder-alert"><i class="fa fa-exclamation-circle"></i> '+response.msg+'<button class="close" data-dismiss="alert" type="button">&times;</button></div>');*/
/*                 $('html, body').animate({ scrollTop: 0 }, 'slow');*/
/*         }*/
/*       }*/
/*     })*/
/*   }*/
/* */
/*   $('.add-to-cart').on('click', function() {*/
/*     $this = $(this);*/
/*     order_id = $(this).data('wk-o-d');*/
/*     data = new Object();*/
/*     data.product_id = $(this).prevAll('input[name="product_id"]').val();*/
/*     data.quantity = $(this).prevAll('input[name="quantity"]').val();*/
/*     options = $(this).prevAll('.options');*/
/*     $.each(options, function(index, value) {*/
/*       data[this.name] = this.value;*/
/*     })*/
/*     $.ajax({*/
/*         url:'index.php?route=checkout/precart/customer_preordered',*/
/*         data: {*/
/*             product_id: data.product_id,*/
/*         },*/
/*         type:'post',*/
/*         dataType: 'json',*/
/*         beforeSend: function() {*/
/*           $('.alert').remove();*/
/*         },*/
/*         success:function(response) {*/
/*             if(response.success) {*/
/*                 enableCompletionPreorder(order_id);*/
/*                 addToCart(data, $this);*/
/*             } else {*/
/*                 $('.breadcrumb').after('<div class="alert alert-danger preorder-alert"><i class="fa fa-exclamation-circle"></i>'+response.msg+'<button class="close" data-dismiss="alert" type="button">&times;</button></div>');*/
/*                 $('html, body').animate({ scrollTop: 0 }, 'slow');*/
/*             }*/
/*         }*/
/*     });*/
/*   });*/
/* */
/*   function addToCart(data, $this) {*/
/*     $.ajax({*/
/*       url: 'index.php?route=checkout/cart/add',*/
/*       data: data,*/
/*       type: 'post',*/
/*       dataType: 'json',*/
/*       beforeSend: function() {*/
/*         $this.button('loading');*/
/*       },*/
/*       complete: function() {*/
/*         $this.button('reset');*/
/*       },*/
/*       success: function(json) {*/
/*         */
/*         if (json['success']) {*/
/*           $('.breadcrumb').after('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');*/
/* */
/*           $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');*/
/* */
/*           $('html, body').animate({ scrollTop: 0 }, 'slow');*/
/* */
/*           $('#cart > ul').load('index.php?route=common/cart/info ul li');*/
/*         }*/
/*          if (json['error']) {*/
/*            $('.alert').remove();*/
/*           $('.breadcrumb').after('<div class="alert alert-danger">' + json['error'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');*/
/*            $('html, body').animate({ scrollTop: 0 }, 'slow');*/
/*          }*/
/* */
/*       }*/
/*     });*/
/*   }*/
/* */
/* </script>*/
/* {{ footer }} */
/* */
