<?php

/* catalog/precartmail.twig */
class __TwigTemplate_6fef8aa7115da35cc8f7165d2c18a2561eb88b3f80d8a7b117aaab09d88b4819 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd\">
<html>
<head>
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">
<title>";
        // line 5
        echo (isset($context["text_subject"]) ? $context["text_subject"] : null);
        echo "</title>
</head>
<body style=\"font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;background:#f1f1f1; padding: 10px 0px;
width: 99%;\">

<div class=\"msgdiv\" style=\"font-size:12px;\twidth:100%;\tpadding:20px; background:#fff;border:1px solid #ccc;width: 93%;\">
<a href=\"";
        // line 11
        echo (isset($context["store_url"]) ? $context["store_url"] : null);
        echo "\" title=\"";
        echo (isset($context["store_name"]) ? $context["store_name"] : null);
        echo "\"><img src=\"";
        echo (isset($context["logo"]) ? $context["logo"] : null);
        echo "\" alt=\"";
        echo (isset($context["store_name"]) ? $context["store_name"] : null);
        echo "\" id=\"logo\"  style=\"max-width:200px; max-height:200px;margin-bottom: 10px;\"/></a><br>

\t\t<span class=\"hi\" style=\"font-size:22px;color:#004274;display: block;\">";
        // line 13
        echo (isset($context["text_hello"]) ? $context["text_hello"] : null);
        echo " ";
        echo $this->getAttribute((isset($context["customer"]) ? $context["customer"] : null), "name", array());
        echo ",</span><br>
\t\t<span style=\" display: block;margin: 2px 0px;color:#004274;font-size:14px; \">";
        // line 14
        echo (isset($context["text_subject"]) ? $context["text_subject"] : null);
        echo "</span>
</div>
\t<table style=\"width: 100%;margin-bottom: 15px;border-collapse: collapse;border-top: 1px solid #DDDDDD;border-left: 1px solid #DDDDDD;border-right: 1px solid #DDDDDD;\">
        <thead>
          <tr>
            <td class=\"image\" style=\"text-align: center;color: #4D4D4D;font-weight: bold;background-color: #F7F7F7;border-bottom: 1px solid #DDDDDD;padding: 7px;\">";
        // line 19
        echo (isset($context["text_pimage"]) ? $context["text_pimage"] : null);
        echo "</td>
            <td class=\"name\" style=\"text-align: center;color: #4D4D4D;font-weight: bold;background-color: #F7F7F7;border-bottom: 1px solid #DDDDDD;padding: 7px;\">";
        // line 20
        echo (isset($context["text_pname"]) ? $context["text_pname"] : null);
        echo "</td>
            <td class=\"model\" style=\"text-align: center;color: #4D4D4D;font-weight: bold;background-color: #F7F7F7;border-bottom: 1px solid #DDDDDD;padding: 7px;\">";
        // line 21
        echo (isset($context["text_pmodel"]) ? $context["text_pmodel"] : null);
        echo "</td>
            <td class=\"quantity\" style=\"text-align: center;color: #4D4D4D;font-weight: bold;background-color: #F7F7F7;border-bottom: 1px solid #DDDDDD;padding: 7px;\">";
        // line 22
        echo (isset($context["text_pqty"]) ? $context["text_pqty"] : null);
        echo "</td>
          </tr>
        </thead>
        <tbody>
          ";
        // line 26
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 27
            echo "          <tr>
            <td class=\"image\" style=\"text-align: center;color: #4D4D4D;border-bottom: 1px solid #DDDDDD;vertical-align: top;\">";
            // line 28
            if ($this->getAttribute($context["product"], "image", array())) {
                // line 29
                echo "              <a href=\"";
                echo ((isset($context["product_link"]) ? $context["product_link"] : null) . $this->getAttribute($context["product"], "product_id", array()));
                echo "\"><img src=\"";
                echo (isset($context["HTTP_SERVER"]) ? $context["HTTP_SERVER"] : null);
                echo "../image/";
                echo $this->getAttribute($context["product"], "image", array());
                echo "\" alt=\"";
                echo $this->getAttribute($context["product"], "name", array());
                echo "\" title=\"";
                echo $this->getAttribute($context["product"], "name", array());
                echo "\" style=\"width:100px;height:100px;\"/></a>
              ";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "</td>
            <td class=\"name\" style=\"text-align: center;color: #4D4D4D;border-bottom: 1px solid #DDDDDD;vertical-align: top;\"><a href=\"";
        // line 31
        echo (isset($context["product_link"]) ? $context["product_link"] : null);
        echo $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "product_id", array());
        echo "\">";
        echo $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "name", array());
        echo "</a>
              </td>
            <td class=\"model\" style=\"text-align: center;color: #4D4D4D;border-bottom: 1px solid #DDDDDD;vertical-align: top;\">";
        // line 33
        echo $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "model", array());
        echo "</td>
            <td class=\"quantity\" style=\"text-align: center;color: #4D4D4D;border-bottom: 1px solid #DDDDDD;vertical-align: top;\">";
        // line 34
        echo $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "quantity", array());
        echo "
              &nbsp;
            </td>
          </tr>
        </tbody>
      </table>
      <span style=\" display: block;margin: 2px 0px;font-size:14px;text-align: center; \"><b>";
        // line 40
        echo (isset($context["text_thanksadmin"]) ? $context["text_thanksadmin"] : null);
        echo "</b></span>
</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "catalog/precartmail.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  129 => 40,  120 => 34,  116 => 33,  108 => 31,  105 => 30,  87 => 29,  85 => 28,  82 => 27,  78 => 26,  71 => 22,  67 => 21,  63 => 20,  59 => 19,  51 => 14,  45 => 13,  34 => 11,  25 => 5,  19 => 1,);
    }
}
/* <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd">*/
/* <html>*/
/* <head>*/
/* <meta http-equiv="Content-Type" content="text/html; charset=utf-8">*/
/* <title>{{ text_subject }}</title>*/
/* </head>*/
/* <body style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #000000;background:#f1f1f1; padding: 10px 0px;*/
/* width: 99%;">*/
/* */
/* <div class="msgdiv" style="font-size:12px;	width:100%;	padding:20px; background:#fff;border:1px solid #ccc;width: 93%;">*/
/* <a href="{{ store_url }}" title="{{ store_name }}"><img src="{{ logo }}" alt="{{ store_name }}" id="logo"  style="max-width:200px; max-height:200px;margin-bottom: 10px;"/></a><br>*/
/* */
/* 		<span class="hi" style="font-size:22px;color:#004274;display: block;">{{ text_hello }} {{ customer.name }},</span><br>*/
/* 		<span style=" display: block;margin: 2px 0px;color:#004274;font-size:14px; ">{{ text_subject }}</span>*/
/* </div>*/
/* 	<table style="width: 100%;margin-bottom: 15px;border-collapse: collapse;border-top: 1px solid #DDDDDD;border-left: 1px solid #DDDDDD;border-right: 1px solid #DDDDDD;">*/
/*         <thead>*/
/*           <tr>*/
/*             <td class="image" style="text-align: center;color: #4D4D4D;font-weight: bold;background-color: #F7F7F7;border-bottom: 1px solid #DDDDDD;padding: 7px;">{{ text_pimage }}</td>*/
/*             <td class="name" style="text-align: center;color: #4D4D4D;font-weight: bold;background-color: #F7F7F7;border-bottom: 1px solid #DDDDDD;padding: 7px;">{{ text_pname }}</td>*/
/*             <td class="model" style="text-align: center;color: #4D4D4D;font-weight: bold;background-color: #F7F7F7;border-bottom: 1px solid #DDDDDD;padding: 7px;">{{ text_pmodel }}</td>*/
/*             <td class="quantity" style="text-align: center;color: #4D4D4D;font-weight: bold;background-color: #F7F7F7;border-bottom: 1px solid #DDDDDD;padding: 7px;">{{ text_pqty }}</td>*/
/*           </tr>*/
/*         </thead>*/
/*         <tbody>*/
/*           {% for product in products %}*/
/*           <tr>*/
/*             <td class="image" style="text-align: center;color: #4D4D4D;border-bottom: 1px solid #DDDDDD;vertical-align: top;">{% if product.image %}*/
/*               <a href="{{ product_link ~ product.product_id }}"><img src="{{ HTTP_SERVER }}../image/{{ product.image }}" alt="{{ product.name }}" title="{{ product.name }}" style="width:100px;height:100px;"/></a>*/
/*               {% endif %}{% endfor %}</td>*/
/*             <td class="name" style="text-align: center;color: #4D4D4D;border-bottom: 1px solid #DDDDDD;vertical-align: top;"><a href="{{ product_link }}{{ product.product_id }}">{{ product.name }}</a>*/
/*               </td>*/
/*             <td class="model" style="text-align: center;color: #4D4D4D;border-bottom: 1px solid #DDDDDD;vertical-align: top;">{{ product.model }}</td>*/
/*             <td class="quantity" style="text-align: center;color: #4D4D4D;border-bottom: 1px solid #DDDDDD;vertical-align: top;">{{ product.quantity }}*/
/*               &nbsp;*/
/*             </td>*/
/*           </tr>*/
/*         </tbody>*/
/*       </table>*/
/*       <span style=" display: block;margin: 2px 0px;font-size:14px;text-align: center; "><b>{{ text_thanksadmin }}</b></span>*/
/* </body>*/
/* </html>*/
/* */
