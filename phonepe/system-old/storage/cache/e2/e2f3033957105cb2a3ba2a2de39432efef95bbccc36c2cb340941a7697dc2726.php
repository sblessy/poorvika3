<?php

/* so-destino/template/extension/module/so_onepagecheckout/default.twig */
class __TwigTemplate_7e52b880021f52042bd3ba2f5a23be5235d9dd9c798fd48d71de984bb7254aa6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo "
<div class=\"container\">
  \t<ul class=\"breadcrumb\">
    \t";
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 5
            echo "    \t\t<li><a href=\"";
            echo $this->getAttribute($context["breadcrumb"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array());
            echo "</a></li>
    \t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 7
        echo "  \t</ul>
  \t";
        // line 8
        if ((isset($context["error_warning"]) ? $context["error_warning"] : null)) {
            // line 9
            echo "  \t\t<div class=\"alert alert-danger\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo (isset($context["error_warning"]) ? $context["error_warning"] : null);
            echo "
    \t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
  \t\t</div>
  \t";
        }
        // line 13
        echo "  \t<div class=\"row\">
  \t\t";
        // line 14
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "
    \t";
        // line 15
        if (((isset($context["column_left"]) ? $context["column_left"] : null) && (isset($context["column_right"]) ? $context["column_right"] : null))) {
            // line 16
            echo "    \t\t";
            $context["class"] = "col-sm-6";
            // line 17
            echo "    \t";
        } elseif (((isset($context["column_left"]) ? $context["column_left"] : null) || (isset($context["column_right"]) ? $context["column_right"] : null))) {
            // line 18
            echo "    \t\t";
            $context["class"] = "col-sm-9";
            // line 19
            echo "    \t";
        } else {
            // line 20
            echo "    \t\t";
            $context["class"] = "col-sm-12";
            // line 21
            echo "    \t";
        }
        // line 22
        echo "    \t<div id=\"content\" class=\"";
        echo (isset($context["class"]) ? $context["class"] : null);
        echo "\">
    \t\t";
        // line 23
        echo (isset($context["content_top"]) ? $context["content_top"] : null);
        echo "
    \t\t<h1>";
        // line 24
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
    \t\t<div class=\"so-onepagecheckout layout1 ";
        // line 25
        if ((isset($context["is_logged_in"]) ? $context["is_logged_in"] : null)) {
            echo " is_customer ";
        }
        echo "\">
    \t\t\t<div class=\"col-left col-lg-6 col-md-6 col-sm-6 col-xs-12\">
    \t\t\t\t";
        // line 27
        if ( !(isset($context["is_logged_in"]) ? $context["is_logged_in"] : null)) {
            // line 28
            echo "\t    \t\t\t\t<div class=\"checkout-content login-box\">
\t    \t\t\t\t\t<h2 class=\"secondary-title\"><i class=\"fa fa-user\"></i>";
            // line 29
            echo (isset($context["text_checkout_create_account_login"]) ? $context["text_checkout_create_account_login"] : null);
            echo "</h2>
                            <div class=\"box-inner\">
                                ";
            // line 31
            if (($this->getAttribute((isset($context["setting_so_onepagecheckout_layout_setting"]) ? $context["setting_so_onepagecheckout_layout_setting"] : null), "so_onepagecheckout_register_checkout", array()) == 1)) {
                // line 32
                echo "        \t    \t\t\t\t\t<div class=\"radio\">
        \t\t\t\t\t\t\t\t<label><input type=\"radio\" name=\"account\" value=\"register\" ";
                // line 33
                if (((isset($context["default_auth"]) ? $context["default_auth"] : null) == "register")) {
                    echo " checked=\"checked\" ";
                }
                echo ">";
                echo (isset($context["text_register"]) ? $context["text_register"] : null);
                echo "</label>
        \t\t\t\t\t\t\t</div>
                                ";
            }
            // line 36
            echo "
    \t\t\t\t\t\t\t";
            // line 37
            if (((isset($context["allow_guest_checkout"]) ? $context["allow_guest_checkout"] : null) && ($this->getAttribute((isset($context["setting_so_onepagecheckout_layout_setting"]) ? $context["setting_so_onepagecheckout_layout_setting"] : null), "so_onepagecheckout_guest_checkout", array()) == 1))) {
                // line 38
                echo "        \t                        <div class=\"radio\">
        \t                            <label><input type=\"radio\" name=\"account\" value=\"guest\" ";
                // line 39
                if (((isset($context["default_auth"]) ? $context["default_auth"] : null) == "guest")) {
                    echo " checked=\"checked\" ";
                }
                echo " />";
                echo (isset($context["text_guest"]) ? $context["text_guest"] : null);
                echo "</label>
        \t                        </div>
    \t                        ";
            }
            // line 42
            echo "
                                ";
            // line 43
            if (($this->getAttribute((isset($context["setting_so_onepagecheckout_layout_setting"]) ? $context["setting_so_onepagecheckout_layout_setting"] : null), "so_onepagecheckout_enable_login", array()) == 1)) {
                // line 44
                echo "        \t\t\t\t\t\t\t<div class=\"radio\">
        \t\t\t\t\t\t\t\t<label><input type=\"radio\" name=\"account\" value=\"login\" ";
                // line 45
                if (((isset($context["default_auth"]) ? $context["default_auth"] : null) == "login")) {
                    echo " checked=\"checked\" ";
                }
                echo ">";
                echo (isset($context["text_returning_customer"]) ? $context["text_returning_customer"] : null);
                echo "</label>
                                    </div>
                                ";
            }
            // line 48
            echo "                            </div>
\t\t\t\t\t\t</div>
                        <script type=\"text/javascript\">
                            \$(document).delegate('.so-onepagecheckout input[name=\"shipping_address\"]', 'change', function() {
                                var \$this = \$(this);

                                if (\$this.is(':checked')) {
                                    \$('.so-onepagecheckout #shipping-address').hide();
                                    \$this.val(1);
                                    \$(document).trigger('so_checkout_address_changed', 'payment');
                                } else {
                                    \$('.so-onepagecheckout #shipping-address').show().find('input[type=\"text\"]').val('');
                                    \$(document).trigger('so_checkout_address_changed', 'payment');
                                    \$(document).trigger('so_checkout_address_changed', 'shipping');
                                    \$this.val(0);
                                }
                            });

                            \$(document).delegate('.so-onepagecheckout input[name=\"account\"]', 'change', function() {
                                if (this.value === 'login') {
                                    \$('.so-onepagecheckout .checkout-login').slideDown(300);
                                    \$('.so-onepagecheckout .checkout-register').parent().addClass('login-mobile');
                                } else {
                                    \$('.so-onepagecheckout .checkout-login').slideUp(300);
                                    \$('.so-onepagecheckout .checkout-register').parent().removeClass('login-mobile');
                                    if (this.value === 'register') {
                                        \$('.so-onepagecheckout #password').slideDown(300);
                                    } else {
                                        \$('.so-onepagecheckout #password').slideUp(300);
                                    }
                                }

                                \$('html').removeClass('checkout-type-login checkout-type-register checkout-type-guest').addClass('checkout-type-' + this.value);
                            });
                        </script>
\t\t\t\t\t";
        }
        // line 84
        echo "
\t\t\t\t\t";
        // line 85
        if (( !(isset($context["is_logged_in"]) ? $context["is_logged_in"] : null) && ($this->getAttribute((isset($context["setting_so_onepagecheckout_layout_setting"]) ? $context["setting_so_onepagecheckout_layout_setting"] : null), "so_onepagecheckout_enable_login", array()) == 1))) {
            // line 86
            echo "\t                    <div class=\"checkout-content checkout-login\">
\t                        <fieldset>
\t                            <h2 class=\"secondary-title\"><i class=\"fa fa-unlock\"></i>";
            // line 88
            echo (isset($context["text_returning_customer"]) ? $context["text_returning_customer"] : null);
            echo "</h2>
                                <div class=\"box-inner\">
    \t                            <div class=\"form-group\">
    \t                                <input type=\"text\" name=\"login_email\" value=\"\" placeholder=\"";
            // line 91
            echo (isset($context["entry_email"]) ? $context["entry_email"] : null);
            echo "\" id=\"input-login_email\" class=\"form-control\" />
    \t                            </div>
    \t                            <div class=\"form-group\">
    \t                                <input type=\"password\" name=\"login_password\" value=\"\" placeholder=\"";
            // line 94
            echo (isset($context["entry_password"]) ? $context["entry_password"] : null);
            echo "\" id=\"input-login_password\" class=\"form-control\" />
    \t                                <a href=\"";
            // line 95
            echo (isset($context["forgotten"]) ? $context["forgotten"] : null);
            echo "\">";
            echo (isset($context["text_forgotten"]) ? $context["text_forgotten"] : null);
            echo "</a>
    \t                            </div>
    \t                            <div class=\"form-group\">
    \t                                <input type=\"button\" value=\"";
            // line 98
            echo (isset($context["button_login"]) ? $context["button_login"] : null);
            echo "\" id=\"button-login\" data-loading-text=\"";
            echo (isset($context["text_loading"]) ? $context["text_loading"] : null);
            echo "\" class=\"btn-primary button\" />
    \t                            </div>
                                </div>
\t                        </fieldset>
\t                    </div>\t                    
                    ";
        }
        // line 104
        echo "
                    ";
        // line 105
        echo (isset($context["register_form"]) ? $context["register_form"] : null);
        echo "
    \t\t\t</div>
                
    \t\t\t<div class=\"col-right col-lg-6 col-md-6 col-sm-6 col-xs-12\">
    \t\t\t\t<section class=\"section-left\">
    \t\t\t\t\t";
        // line 110
        if ((isset($context["is_logged_in"]) ? $context["is_logged_in"] : null)) {
            // line 111
            echo "                        \t";
            echo (isset($context["payment_address"]) ? $context["payment_address"] : null);
            echo "
                        \t";
            // line 112
            if ((isset($context["is_shipping_required"]) ? $context["is_shipping_required"] : null)) {
                // line 113
                echo "                        \t\t";
                echo (isset($context["shipping_address"]) ? $context["shipping_address"] : null);
                echo "
                        \t";
            }
            // line 115
            echo "                        ";
        }
        // line 116
        echo "                        <div class=\"ship-payment\">
                            ";
        // line 117
        if ((isset($context["is_shipping_required"]) ? $context["is_shipping_required"] : null)) {
            // line 118
            echo "                            \t";
            echo (isset($context["shipping_methods"]) ? $context["shipping_methods"] : null);
            echo "
                            ";
        }
        // line 120
        echo "
                            ";
        // line 121
        echo (isset($context["payment_methods"]) ? $context["payment_methods"] : null);
        echo "
                        </div>
    \t\t\t\t</section>
    \t\t\t\t<section class=\"section-right\">
                        <div id=\"coupon_voucher_reward\">
    \t\t\t\t\t   ";
        // line 126
        echo (isset($context["coupon_voucher_reward"]) ? $context["coupon_voucher_reward"] : null);
        echo "
                        </div>

                        ";
        // line 129
        echo (isset($context["cart"]) ? $context["cart"] : null);
        echo "
                        
    \t\t\t\t\t<div class=\"checkout-content confirm-section\">
                            ";
        // line 132
        if ($this->getAttribute((isset($context["setting_so_onepagecheckout_layout_setting"]) ? $context["setting_so_onepagecheckout_layout_setting"] : null), "comment_status", array())) {
            // line 133
            echo "                                <div>
                                    <h2 class=\"secondary-title\"><i class=\"fa fa-comment\"></i>";
            // line 134
            echo (isset($context["text_comments"]) ? $context["text_comments"] : null);
            echo "</h2>
                                    <label>
                                        <textarea name=\"comment\" rows=\"8\" class=\"form-control ";
            // line 136
            if ($this->getAttribute((isset($context["setting_so_onepagecheckout_layout_setting"]) ? $context["setting_so_onepagecheckout_layout_setting"] : null), "require_comment_status", array())) {
                echo " requried ";
            }
            echo "\">";
            echo (isset($context["comment"]) ? $context["comment"] : null);
            echo "</textarea>
                                    </label>
                                </div>
                            ";
        }
        // line 140
        echo "
                            ";
        // line 141
        if (((isset($context["entry_newsletter"]) ? $context["entry_newsletter"] : null) && $this->getAttribute((isset($context["setting_so_onepagecheckout_layout_setting"]) ? $context["setting_so_onepagecheckout_layout_setting"] : null), "show_newsletter", array()))) {
            // line 142
            echo "                                <div class=\"checkbox check-newsletter\">
                                    <label for=\"newsletter\">
                                        <input type=\"checkbox\" name=\"newsletter\" value=\"1\" id=\"newsletter\" />
                                        ";
            // line 145
            echo (isset($context["entry_newsletter"]) ? $context["entry_newsletter"] : null);
            echo "
                                    </label>
                                </div>
                            ";
        }
        // line 149
        echo "
                            ";
        // line 150
        if (((isset($context["text_privacy"]) ? $context["text_privacy"] : null) && $this->getAttribute((isset($context["setting_so_onepagecheckout_layout_setting"]) ? $context["setting_so_onepagecheckout_layout_setting"] : null), "show_privacy", array()))) {
            // line 151
            echo "                                <div class=\"checkbox check-privacy\">
                                    <label>
                                        <input type=\"checkbox\" name=\"privacy\" value=\"1\" />
                                        ";
            // line 154
            echo (isset($context["text_privacy"]) ? $context["text_privacy"] : null);
            echo "
                                    </label>
                                </div>
                            ";
        }
        // line 158
        echo "
                            ";
        // line 159
        if (((isset($context["text_agree"]) ? $context["text_agree"] : null) && $this->getAttribute((isset($context["setting_so_onepagecheckout_layout_setting"]) ? $context["setting_so_onepagecheckout_layout_setting"] : null), "show_term", array()))) {
            // line 160
            echo "                                <div class=\"checkbox check-terms\">
                                    <label>
                                        <input type=\"checkbox\" name=\"agree\" value=\"1\" />
                                        ";
            // line 163
            echo (isset($context["text_agree"]) ? $context["text_agree"] : null);
            echo "
                                    </label>
                                </div>
                            ";
        }
        // line 167
        echo "                            <div class=\"confirm-order\">
                                <button id=\"so-checkout-confirm-button\" data-loading-text=\"";
        // line 168
        echo (isset($context["text_loading"]) ? $context["text_loading"] : null);
        echo "\" class=\"btn btn-primary button confirm-button\">";
        echo (isset($context["text_confirm_order"]) ? $context["text_confirm_order"] : null);
        echo "</button>
                            </div>                            
                        </div>
    \t\t\t\t</section>
    \t\t\t</div>
    \t\t</div>
    \t\t";
        // line 174
        echo (isset($context["content_bottom"]) ? $context["content_bottom"] : null);
        echo "
    \t</div>
    </div>
</div>
<script type=\"text/javascript\">
    \$(document).delegate('.so-onepagecheckout input[name=\"shipping_method\"]', 'change', function() {
        \$(document).trigger('so_checkout_shipping_changed', this.value);
    });

    \$(document).delegate('input[name=\"payment_method\"]', 'change', function() {
        \$(document).trigger('so_checkout_payment_changed', this.value);
    });

    \$(document).delegate('.so-onepagecheckout #input-login_email, .so-onepagecheckout #input-login_password, .so-onepagecheckout #button-login', 'keydown', function(e) {
        if (e.keyCode == 13) {
            so_ajax_login();
        }
    });

    \$(document).delegate('.so-onepagecheckout #button-login', 'click', function() {
        so_ajax_login();
    });

    var count_loading = 0;
    function ajaxLoadingOn(){
        \$('body > .tooltip').remove();
        count_loading++;
        if(count_loading===1){
            \$('.so-onepagecheckout #so-checkout-confirm-button').button('loading');
            \$('.so-onepagecheckout #so-checkout-confirm-button, .so-onepagecheckout .checkout-register, .so-onepagecheckout .checkout-payment-form, .so-onepagecheckout .checkout-shipping-form, .so-onepagecheckout .checkout-cart, .so-onepagecheckout .confirm-section, .so-onepagecheckout .checkout-shipping-methods, .so-onepagecheckout .checkout-payment-methods, .so-onepagecheckout .coupon-voucher').addClass('checkout-loading');
        }
    }

    function ajaxLoadingOff(){
        count_loading--;
        if(count_loading===0){
            \$('.so-onepagecheckout #so-checkout-confirm-button').button('reset');
            \$('.so-onepagecheckout #so-checkout-confirm-button, .so-onepagecheckout .checkout-register, .so-onepagecheckout .checkout-payment-form, .so-onepagecheckout .checkout-shipping-form, .so-onepagecheckout .checkout-cart, .so-onepagecheckout .confirm-section, .so-onepagecheckout .checkout-shipping-methods, .so-onepagecheckout .checkout-payment-methods, .so-onepagecheckout .coupon-voucher').removeClass('checkout-loading');
        }
    }

    function so_ajax_login(){
        \$.ajax({
            url:'index.php?route=checkout/checkout/login',
            type:'post',
            cache:false,
            data:{email: \$('.so-onepagecheckout input[name=\"login_email\"]').val(),password: \$('.so-onepagecheckout input[name=\"login_password\"]').val()},
            dataType:'json',
            beforeSend:function(){
                ajaxLoadingOn();
                \$('.so-onepagecheckout #button-login').button('loading');
            },
            complete:function(){
                ajaxLoadingOff();
                \$('.so-onepagecheckout #button-login').button('reset');
            },
            success:function(json){
                if(json['error']&&json['error']['warning']){
                    alert(json['error']['warning']);
                }
                if(json['redirect']){
                    location=json['redirect'];
                }
            },
            error:function(xhr,ajaxOptions,thrownError){
                alert(thrownError+\"\\r\\n\"+xhr.statusText+\"\\r\\n\"+xhr.responseText);
            }
        });
    }

    \$(document).delegate('.so-onepagecheckout .confirm-button', 'click', function(){
        var data={};
        \$('.so-onepagecheckout input[type=\"text\"], .so-onepagecheckout input[type=\"number\"], .so-onepagecheckout input[type=\"password\"], .so-onepagecheckout select, .so-onepagecheckout input:checked, .so-onepagecheckout textarea[name=\"comment\"]').each(function(){
            data[\$(this).attr('name')]=\$(this).val();
        });

        \$.ajax({
            url:'index.php?route=checkout/checkout/confirm',
            type:'post',
            cache:false,
            data:data,
            dataType:'json',
            beforeSend:function(){
                ajaxLoadingOn();
            },
            success:function(json){
                \$('.so-onepagecheckout .text-danger').remove();
                \$('.so-onepagecheckout .has-error').removeClass('has-error');
                if(json['redirect_cart']){
                    location=json['redirect_cart'];return;
                }
                
                if(json['errors']){
                    \$.each(json['errors'],function(k,v){
                        if(k==='shipping_method'||k==='payment_method'){
                            return;
                        }
                        if(\$.inArray(k,['payment_country','payment_zone','shipping_country','shipping_zone'])!==-1){
                            k+='_id';
                        }else if(k.indexOf('custom_field')===0){
                            k=k.replace('custom_field','');
                            k='custom_field['+k+']';
                        }else if(k.indexOf('payment_custom_field')===0){
                            k=k.replace('payment_custom_field','');
                            k='payment_custom_field['+k+']';
                        }else if(k.indexOf('shipping_custom_field')===0){
                            k=k.replace('shipping_custom_field','');
                            k='shipping_custom_field['+k+']';
                        }
                        
                        var \$element=\$('.so-onepagecheckout [name=\"'+k+'\"]');
                        \$element.closest('.form-group').addClass('has-error');
                        if (\$element.closest('label').length)
                            \$element.closest('label').after('<div class=\"text-danger\">'+v+'</div>');
                        else
                            \$element.after('<div class=\"text-danger\">'+v+'</div>');
                    });
                    
                    ajaxLoadingOff();
                    
                    try{
                        \$('html, body').animate({scrollTop:\$('.has-error').offset().top},'slow');
                    }catch(e){
                        if (json['errors']['account'][0]) alert(json['errors']['account'][0]);
                    }
                    
                    return false;
                }
                else if(json['redirect']){
                    location=json['redirect'];
                }else{
                    var \$btn=\$('.so-onepagecheckout #payment-confirm-button input[type=\"button\"], .so-onepagecheckout #payment-confirm-button input[type=\"submit\"], .so-onepagecheckout #payment-confirm-button .pull-right a, .so-onepagecheckout #payment-confirm-button .right a, .so-onepagecheckout #payment-confirm-button a.button, .so-onepagecheckout #button-confirm, .so-onepagecheckout #button-pay, .so-onepagecheckout #payment-confirm-button.payment-iyzico_checkout_installment .submitButton, .so-onepagecheckout #stripe-confirm').first();
                    if(\$btn.attr('href')){
                        location=\$btn.attr('href');
                    }else{
                        \$btn.trigger('click');
                    }
                }
            },
            error:function(xhr,ajaxOptions,thrownError){
                alert(thrownError+\"\\r\\n\"+xhr.statusText+\"\\r\\n\"+xhr.responseText);
            }
        });
    });

    \$(document).on('so_checkout_customer_group_changed', function(e, value){
        \$.ajax({
            url:'index.php?route=checkout/checkout',
            type:'get',
            cache:false,
            data:{customer_group_id:value},
            beforeSend:function(){
                ajaxLoadingOn();
                \$('.so-onepagecheckout #account, .so-onepagecheckout #address').addClass('checkout-loading');
            },
            complete:function(){
                ajaxLoadingOff();
                \$('.so-onepagecheckout #account, .so-onepagecheckout #address').removeClass('checkout-loading');
            },
            success:function(html){
                var \$html=\$(html);
                \$('.so-onepagecheckout #account').html(\$html.find('#account'));
                \$('.so-onepagecheckout #address').html(\$html.find('#address'));
                \$('.so-onepagecheckout #password').html(\$html.find('#password'));
                \$('.so-onepagecheckout #account .form-group[data-sort]').detach().each(function(){
                    if(\$(this).attr('data-sort')>=0 && \$(this).attr('data-sort')<=\$('.so-onepagecheckout #account .form-group').length){
                        \$('.so-onepagecheckout #account .form-group').eq(\$(this).attr('data-sort')).before(this);
                    }
                    if(\$(this).attr('data-sort')>\$('#account .form-group').length){
                        \$('.so-onepagecheckout #account .form-group:last').after(this);
                    }
                    if(\$(this).attr('data-sort')<-\$('#account .form-group').length){
                        \$('.so-onepagecheckout #account .form-group:first').before(this);
                    }
                });
                
                \$(document).trigger('so_checkout_reload_payment');

                if(\$('.so-onepagecheckout input[name=\"shipping_address\"]').is(':checked')){
                    \$(document).trigger('so_checkout_reload_shipping');
                }
            },
            error:function(xhr,ajaxOptions,thrownError){
                alert(thrownError+\"\\r\\n\"+xhr.statusText+\"\\r\\n\"+xhr.responseText);
            }
        });
    });

    \$(document).on('so_checkout_address_changed', function(e, type){
        var data={};
        if(\$('.so-onepagecheckout input[name=\"'+type+'_address\"]:checked').val()==='existing'){
            data[type+'_address_id']=\$('select[name=\"'+type+'_address_id\"]').val();
        }else{
            data[type+'_country_id']=\$('select[name=\"'+type+'_country_id\"]').val();
            data[type+'_postcode']=\$('input[name=\"'+type+'_postcode\"]').val();
            data[type+'_zone_id']=\$('select[name=\"'+type+'_zone_id\"]').val();
            ";
        // line 370
        if ( !(isset($context["is_logged_in"]) ? $context["is_logged_in"] : null)) {
            // line 371
            echo "                if(type==='payment'&&\$('input[name=\"shipping_address\"]').is(\":checked\")){
                    data['shipping_country_id']=\$('select[name=\"'+type+'_country_id\"]').val();
                    data['shipping_postcode']=\$('input[name=\"'+type+'_postcode\"]').val();
                    data['shipping_zone_id']=\$('select[name=\"'+type+'_zone_id\"]').val();
                }
            ";
        }
        // line 377
        echo "        }

        \$.ajax({
            url:'index.php?route=checkout/checkout/save',
            type:'post',
            cache:false,
            data:data,
            dataType:'json',
            success:function(json){
                \$(document).trigger('so_checkout_reload_'+type);
                ";
        // line 387
        if ( !(isset($context["is_logged_in"]) ? $context["is_logged_in"] : null)) {
            // line 388
            echo "                    if(type==='payment'&&\$('input[name=\"shipping_address\"]').is(':checked')){
                        \$(document).trigger('so_checkout_reload_shipping');
                    }
                ";
        }
        // line 392
        echo "            },
            error:function(xhr,ajaxOptions,thrownError){
                alert(thrownError+\"\\r\\n\"+xhr.statusText+\"\\r\\n\"+xhr.responseText);
            }
        });
    });

    \$(document).on('so_checkout_shipping_changed', function (e, value) {
        \$.ajax({
            url: 'index.php?route=checkout/checkout/save',
            type: 'post',
            data: {
                shipping_method: value
            },
            dataType: 'json',
            success: function() {
                \$(document).trigger('so_checkout_reload_payment');
                \$(document).trigger('so_checkout_reload_cart');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
        });
    });

    \$(document).on('so_checkout_payment_changed', function (e, value) {
        \$.ajax({
            url: 'index.php?route=checkout/checkout/save',
            type: 'post',
            data: {
                payment_method: value
            },
            dataType: 'json',
            success: function() {
                \$(document).trigger('so_checkout_reload_cart');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
        });
    });

    \$(document).on('so_checkout_reload_shipping', function () {
        \$.ajax({
            url: 'index.php?route=checkout/checkout/shipping',
            type: 'get',
            dataType: 'html',
            beforeSend: function() {
                ajaxLoadingOn();
                \$('.checkout-shipping-methods').addClass('checkout-loading');
            },
            complete: function() {
                ajaxLoadingOff();
                \$('.checkout-shipping-methods').removeClass('checkout-loading');
            },
            success: function(html) {
                \$('.checkout-shipping-methods').replaceWith(html);
                \$(document).trigger('so_checkout_reload_cart');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
        });
    });

    \$(document).on('so_checkout_reload_payment', function () {
        \$.ajax({
            url: 'index.php?route=checkout/checkout/payment',
            type: 'get',
            dataType: 'html',
            beforeSend: function() {
                ajaxLoadingOn();
                \$('.checkout-payment-methods').addClass('checkout-loading');
            },
            complete: function() {
                ajaxLoadingOff();
                \$('.checkout-payment-methods').removeClass('checkout-loading');
            },
            success: function(html) {
                \$('.checkout-payment-methods').replaceWith(html);
                \$(document).trigger('so_checkout_reload_cart');
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
        });
    });

    \$(document).on('so_checkout_reload_cart', function (e, first) {
        \$.ajax({
            url: 'index.php?route=checkout/checkout/cart',
            type: 'get',
            dataType: 'html',
            beforeSend: function() {
                if (!first) {
                    ajaxLoadingOn();
                    \$('.so-onepagecheckout .checkout-cart').addClass('checkout-loading');
                }
            },
            complete: function() {
                if (!first) {
                    ajaxLoadingOff();
                    \$('.so-onepagecheckout .checkout-cart').removeClass('checkout-loading');
                }
            },
            success: function(html) {
                \$('.so-onepagecheckout .checkout-cart').replaceWith(html);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
        });
    });

    \$(document).delegate('.so-onepagecheckout .checkout-product .input-group .btn-update', 'click', function () {
        var key = \$(this).attr('data-product-key');
        var qty  = \$('input[name=\"quantity[' + key + ']\"]').val();
        \$.ajax({
            url: 'index.php?route=checkout/checkout/cart_update',
            type: 'post',
            data: {
                key: key,
                quantity: qty
            },
            dataType: 'json',
            beforeSend: function() {
                ajaxLoadingOn();
                \$('#cart > button > a > span').button('loading');
                \$('.so-onepagecheckout .checkout-cart').addClass('checkout-loading');
            },
            complete: function() {
                ajaxLoadingOff();
                \$('#cart > button > a > span').button('reset');
            },
            success: function(json) {
                setTimeout(function () {
                    \$('#cart-total').html(json['total']);
                }, 100);

                if (json['redirect']) {
                    location = json['redirect'];
                } else {
                    \$('#cart ul').load('index.php?route=common/cart/info ul li');

                    \$(document).trigger('so_checkout_reload_payment');
                    \$(document).trigger('so_checkout_reload_shipping');
                }
            }
        });
    });

    \$(document).delegate('.so-onepagecheckout .checkout-product .input-group .btn-delete', 'click', function () {
        var key = \$(this).attr('data-product-key');
        \$.ajax({
            url: 'index.php?route=checkout/checkout/cart_delete',
            type: 'post',
            data: {
                key: key
            },
            dataType: 'json',
            beforeSend: function() {
                ajaxLoadingOn();
                \$('#cart > button > a > span').button('loading');
                \$('.so-onepagecheckout .checkout-cart').addClass('checkout-loading');
            },
            complete: function() {
                ajaxLoadingOff();
                \$('#cart > button > a > span').button('reset');
            },
            success: function(json) {
                setTimeout(function () {
                    \$('#cart-total').html(json['total']);
                }, 100);

                if (json['redirect']) {
                    location = json['redirect'];
                } else {
                    \$('#cart ul').load('index.php?route=common/cart/info ul li');

                    \$(document).trigger('so_checkout_reload_payment');
                    \$(document).trigger('so_checkout_reload_shipping');
                }
            }
        });
    });

    \$(document).delegate('#button-voucher', 'click', function() {
        \$.ajax({
            url:'index.php?route=extension/total/voucher/voucher',
            type: 'post',
            data: 'voucher=' + encodeURIComponent(\$('input[name=\\'voucher\\']').val()),
            dataType: 'json',
            beforeSend: function() {
                ajaxLoadingOn();
                \$('.so-onepagecheckout #button-voucher').button('loading');
            },
            complete: function() {
                ajaxLoadingOff();
                \$('.so-onepagecheckout #button-voucher').button('reset');
            },
            success: function(json) {
                if (json['error']) {
                    alert(json['error']);
                } else {
                    \$('#cart ul').load('index.php?route=common/cart/info ul li');

                    \$(document).trigger('so_checkout_reload_payment');
                    \$(document).trigger('so_checkout_reload_shipping');
                }
            }
        });
    });

    \$(document).delegate('#button-coupon', 'click', function() {
        \$.ajax({
            url:'index.php?route=extension/total/coupon/coupon',
            type: 'post',
            data: 'coupon=' + encodeURIComponent(\$('input[name=\\'coupon\\']').val()),
            dataType: 'json',
            beforeSend: function() {
                ajaxLoadingOn();
                \$('.so-onepagecheckout #button-coupon').button('loading');
            },
            complete: function() {
                ajaxLoadingOff();
                \$('.so-onepagecheckout #button-coupon').button('reset');
            },
            success: function(json) {
                if (json['error']) {
                    alert(json['error']);
                } else {
                    \$('#cart ul').load('index.php?route=common/cart/info ul li');

                    \$(document).trigger('so_checkout_reload_payment');
                    \$(document).trigger('so_checkout_reload_shipping');
                }
            }
        });
    });

    \$(document).delegate('#button-reward', 'click', function() {
        \$.ajax({
            url:'index.php?route=extension/total/reward/reward',
            type: 'post',
            data: 'reward=' + encodeURIComponent(\$('input[name=\\'reward\\']').val()),
            dataType: 'json',
            beforeSend: function() {
                ajaxLoadingOn();
                \$('.so-onepagecheckout #button-reward').button('loading');
            },
            complete: function() {
                ajaxLoadingOff();
                \$('.so-onepagecheckout #button-reward').button('reset');
            },
            success: function(json) {
                if (json['error']) {
                    alert(json['error']);
                } else {
                    \$('#cart ul').load('index.php?route=common/cart/info ul li');

                    \$(document).trigger('so_checkout_reload_payment');
                    \$(document).trigger('so_checkout_reload_shipping');
                }
            }
        });
    });

    ";
        // line 659
        if ((isset($context["is_logged_in"]) ? $context["is_logged_in"] : null)) {
            // line 660
            echo "        \$('.so-onepagecheckout [value=\"existing\"]').trigger('change');
    ";
        } else {
            // line 662
            echo "        \$('.so-onepagecheckout input[name=\"account\"]:checked').trigger('change');
    ";
        }
        // line 664
        echo "
    \$(document).trigger('so_checkout_reload_cart', true);
</script>
";
        // line 667
        echo (isset($context["footer"]) ? $context["footer"] : null);
    }

    public function getTemplateName()
    {
        return "so-destino/template/extension/module/so_onepagecheckout/default.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  905 => 667,  900 => 664,  896 => 662,  892 => 660,  890 => 659,  621 => 392,  615 => 388,  613 => 387,  601 => 377,  593 => 371,  591 => 370,  392 => 174,  381 => 168,  378 => 167,  371 => 163,  366 => 160,  364 => 159,  361 => 158,  354 => 154,  349 => 151,  347 => 150,  344 => 149,  337 => 145,  332 => 142,  330 => 141,  327 => 140,  316 => 136,  311 => 134,  308 => 133,  306 => 132,  300 => 129,  294 => 126,  286 => 121,  283 => 120,  277 => 118,  275 => 117,  272 => 116,  269 => 115,  263 => 113,  261 => 112,  256 => 111,  254 => 110,  246 => 105,  243 => 104,  232 => 98,  224 => 95,  220 => 94,  214 => 91,  208 => 88,  204 => 86,  202 => 85,  199 => 84,  161 => 48,  151 => 45,  148 => 44,  146 => 43,  143 => 42,  133 => 39,  130 => 38,  128 => 37,  125 => 36,  115 => 33,  112 => 32,  110 => 31,  105 => 29,  102 => 28,  100 => 27,  93 => 25,  89 => 24,  85 => 23,  80 => 22,  77 => 21,  74 => 20,  71 => 19,  68 => 18,  65 => 17,  62 => 16,  60 => 15,  56 => 14,  53 => 13,  45 => 9,  43 => 8,  40 => 7,  29 => 5,  25 => 4,  19 => 1,);
    }
}
/* {{ header }}*/
/* <div class="container">*/
/*   	<ul class="breadcrumb">*/
/*     	{% for breadcrumb in breadcrumbs %}*/
/*     		<li><a href="{{ breadcrumb.href }}">{{ breadcrumb.text }}</a></li>*/
/*     	{% endfor %}*/
/*   	</ul>*/
/*   	{% if error_warning %}*/
/*   		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> {{ error_warning }}*/
/*     		<button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*   		</div>*/
/*   	{% endif %}*/
/*   	<div class="row">*/
/*   		{{ column_left }}*/
/*     	{% if column_left and column_right %}*/
/*     		{% set class = 'col-sm-6' %}*/
/*     	{% elseif column_left or column_right %}*/
/*     		{% set class = 'col-sm-9' %}*/
/*     	{% else %}*/
/*     		{% set class = 'col-sm-12' %}*/
/*     	{% endif %}*/
/*     	<div id="content" class="{{ class }}">*/
/*     		{{ content_top }}*/
/*     		<h1>{{ heading_title }}</h1>*/
/*     		<div class="so-onepagecheckout layout1 {% if is_logged_in %} is_customer {% endif %}">*/
/*     			<div class="col-left col-lg-6 col-md-6 col-sm-6 col-xs-12">*/
/*     				{% if not is_logged_in %}*/
/* 	    				<div class="checkout-content login-box">*/
/* 	    					<h2 class="secondary-title"><i class="fa fa-user"></i>{{ text_checkout_create_account_login }}</h2>*/
/*                             <div class="box-inner">*/
/*                                 {% if setting_so_onepagecheckout_layout_setting.so_onepagecheckout_register_checkout == 1 %}*/
/*         	    					<div class="radio">*/
/*         								<label><input type="radio" name="account" value="register" {% if default_auth == 'register' %} checked="checked" {% endif %}>{{ text_register }}</label>*/
/*         							</div>*/
/*                                 {% endif %}*/
/* */
/*     							{% if allow_guest_checkout and setting_so_onepagecheckout_layout_setting.so_onepagecheckout_guest_checkout == 1 %}*/
/*         	                        <div class="radio">*/
/*         	                            <label><input type="radio" name="account" value="guest" {% if default_auth == 'guest' %} checked="checked" {% endif %} />{{ text_guest }}</label>*/
/*         	                        </div>*/
/*     	                        {% endif %}*/
/* */
/*                                 {% if setting_so_onepagecheckout_layout_setting.so_onepagecheckout_enable_login == 1 %}*/
/*         							<div class="radio">*/
/*         								<label><input type="radio" name="account" value="login" {% if default_auth == 'login' %} checked="checked" {% endif %}>{{ text_returning_customer }}</label>*/
/*                                     </div>*/
/*                                 {% endif %}*/
/*                             </div>*/
/* 						</div>*/
/*                         <script type="text/javascript">*/
/*                             $(document).delegate('.so-onepagecheckout input[name="shipping_address"]', 'change', function() {*/
/*                                 var $this = $(this);*/
/* */
/*                                 if ($this.is(':checked')) {*/
/*                                     $('.so-onepagecheckout #shipping-address').hide();*/
/*                                     $this.val(1);*/
/*                                     $(document).trigger('so_checkout_address_changed', 'payment');*/
/*                                 } else {*/
/*                                     $('.so-onepagecheckout #shipping-address').show().find('input[type="text"]').val('');*/
/*                                     $(document).trigger('so_checkout_address_changed', 'payment');*/
/*                                     $(document).trigger('so_checkout_address_changed', 'shipping');*/
/*                                     $this.val(0);*/
/*                                 }*/
/*                             });*/
/* */
/*                             $(document).delegate('.so-onepagecheckout input[name="account"]', 'change', function() {*/
/*                                 if (this.value === 'login') {*/
/*                                     $('.so-onepagecheckout .checkout-login').slideDown(300);*/
/*                                     $('.so-onepagecheckout .checkout-register').parent().addClass('login-mobile');*/
/*                                 } else {*/
/*                                     $('.so-onepagecheckout .checkout-login').slideUp(300);*/
/*                                     $('.so-onepagecheckout .checkout-register').parent().removeClass('login-mobile');*/
/*                                     if (this.value === 'register') {*/
/*                                         $('.so-onepagecheckout #password').slideDown(300);*/
/*                                     } else {*/
/*                                         $('.so-onepagecheckout #password').slideUp(300);*/
/*                                     }*/
/*                                 }*/
/* */
/*                                 $('html').removeClass('checkout-type-login checkout-type-register checkout-type-guest').addClass('checkout-type-' + this.value);*/
/*                             });*/
/*                         </script>*/
/* 					{% endif %}*/
/* */
/* 					{% if not is_logged_in and setting_so_onepagecheckout_layout_setting.so_onepagecheckout_enable_login == 1 %}*/
/* 	                    <div class="checkout-content checkout-login">*/
/* 	                        <fieldset>*/
/* 	                            <h2 class="secondary-title"><i class="fa fa-unlock"></i>{{ text_returning_customer }}</h2>*/
/*                                 <div class="box-inner">*/
/*     	                            <div class="form-group">*/
/*     	                                <input type="text" name="login_email" value="" placeholder="{{ entry_email }}" id="input-login_email" class="form-control" />*/
/*     	                            </div>*/
/*     	                            <div class="form-group">*/
/*     	                                <input type="password" name="login_password" value="" placeholder="{{ entry_password }}" id="input-login_password" class="form-control" />*/
/*     	                                <a href="{{ forgotten }}">{{ text_forgotten }}</a>*/
/*     	                            </div>*/
/*     	                            <div class="form-group">*/
/*     	                                <input type="button" value="{{ button_login }}" id="button-login" data-loading-text="{{ text_loading }}" class="btn-primary button" />*/
/*     	                            </div>*/
/*                                 </div>*/
/* 	                        </fieldset>*/
/* 	                    </div>	                    */
/*                     {% endif %}*/
/* */
/*                     {{ register_form }}*/
/*     			</div>*/
/*                 */
/*     			<div class="col-right col-lg-6 col-md-6 col-sm-6 col-xs-12">*/
/*     				<section class="section-left">*/
/*     					{% if is_logged_in %}*/
/*                         	{{ payment_address }}*/
/*                         	{% if is_shipping_required %}*/
/*                         		{{ shipping_address }}*/
/*                         	{% endif %}*/
/*                         {% endif %}*/
/*                         <div class="ship-payment">*/
/*                             {% if is_shipping_required %}*/
/*                             	{{ shipping_methods }}*/
/*                             {% endif %}*/
/* */
/*                             {{ payment_methods }}*/
/*                         </div>*/
/*     				</section>*/
/*     				<section class="section-right">*/
/*                         <div id="coupon_voucher_reward">*/
/*     					   {{ coupon_voucher_reward }}*/
/*                         </div>*/
/* */
/*                         {{ cart }}*/
/*                         */
/*     					<div class="checkout-content confirm-section">*/
/*                             {% if setting_so_onepagecheckout_layout_setting.comment_status %}*/
/*                                 <div>*/
/*                                     <h2 class="secondary-title"><i class="fa fa-comment"></i>{{ text_comments }}</h2>*/
/*                                     <label>*/
/*                                         <textarea name="comment" rows="8" class="form-control {% if setting_so_onepagecheckout_layout_setting.require_comment_status %} requried {% endif %}">{{ comment }}</textarea>*/
/*                                     </label>*/
/*                                 </div>*/
/*                             {% endif %}*/
/* */
/*                             {% if entry_newsletter and setting_so_onepagecheckout_layout_setting.show_newsletter %}*/
/*                                 <div class="checkbox check-newsletter">*/
/*                                     <label for="newsletter">*/
/*                                         <input type="checkbox" name="newsletter" value="1" id="newsletter" />*/
/*                                         {{ entry_newsletter }}*/
/*                                     </label>*/
/*                                 </div>*/
/*                             {% endif %}*/
/* */
/*                             {% if text_privacy and setting_so_onepagecheckout_layout_setting.show_privacy %}*/
/*                                 <div class="checkbox check-privacy">*/
/*                                     <label>*/
/*                                         <input type="checkbox" name="privacy" value="1" />*/
/*                                         {{ text_privacy }}*/
/*                                     </label>*/
/*                                 </div>*/
/*                             {% endif %}*/
/* */
/*                             {% if text_agree and setting_so_onepagecheckout_layout_setting.show_term %}*/
/*                                 <div class="checkbox check-terms">*/
/*                                     <label>*/
/*                                         <input type="checkbox" name="agree" value="1" />*/
/*                                         {{ text_agree }}*/
/*                                     </label>*/
/*                                 </div>*/
/*                             {% endif %}*/
/*                             <div class="confirm-order">*/
/*                                 <button id="so-checkout-confirm-button" data-loading-text="{{ text_loading }}" class="btn btn-primary button confirm-button">{{ text_confirm_order }}</button>*/
/*                             </div>                            */
/*                         </div>*/
/*     				</section>*/
/*     			</div>*/
/*     		</div>*/
/*     		{{ content_bottom }}*/
/*     	</div>*/
/*     </div>*/
/* </div>*/
/* <script type="text/javascript">*/
/*     $(document).delegate('.so-onepagecheckout input[name="shipping_method"]', 'change', function() {*/
/*         $(document).trigger('so_checkout_shipping_changed', this.value);*/
/*     });*/
/* */
/*     $(document).delegate('input[name="payment_method"]', 'change', function() {*/
/*         $(document).trigger('so_checkout_payment_changed', this.value);*/
/*     });*/
/* */
/*     $(document).delegate('.so-onepagecheckout #input-login_email, .so-onepagecheckout #input-login_password, .so-onepagecheckout #button-login', 'keydown', function(e) {*/
/*         if (e.keyCode == 13) {*/
/*             so_ajax_login();*/
/*         }*/
/*     });*/
/* */
/*     $(document).delegate('.so-onepagecheckout #button-login', 'click', function() {*/
/*         so_ajax_login();*/
/*     });*/
/* */
/*     var count_loading = 0;*/
/*     function ajaxLoadingOn(){*/
/*         $('body > .tooltip').remove();*/
/*         count_loading++;*/
/*         if(count_loading===1){*/
/*             $('.so-onepagecheckout #so-checkout-confirm-button').button('loading');*/
/*             $('.so-onepagecheckout #so-checkout-confirm-button, .so-onepagecheckout .checkout-register, .so-onepagecheckout .checkout-payment-form, .so-onepagecheckout .checkout-shipping-form, .so-onepagecheckout .checkout-cart, .so-onepagecheckout .confirm-section, .so-onepagecheckout .checkout-shipping-methods, .so-onepagecheckout .checkout-payment-methods, .so-onepagecheckout .coupon-voucher').addClass('checkout-loading');*/
/*         }*/
/*     }*/
/* */
/*     function ajaxLoadingOff(){*/
/*         count_loading--;*/
/*         if(count_loading===0){*/
/*             $('.so-onepagecheckout #so-checkout-confirm-button').button('reset');*/
/*             $('.so-onepagecheckout #so-checkout-confirm-button, .so-onepagecheckout .checkout-register, .so-onepagecheckout .checkout-payment-form, .so-onepagecheckout .checkout-shipping-form, .so-onepagecheckout .checkout-cart, .so-onepagecheckout .confirm-section, .so-onepagecheckout .checkout-shipping-methods, .so-onepagecheckout .checkout-payment-methods, .so-onepagecheckout .coupon-voucher').removeClass('checkout-loading');*/
/*         }*/
/*     }*/
/* */
/*     function so_ajax_login(){*/
/*         $.ajax({*/
/*             url:'index.php?route=checkout/checkout/login',*/
/*             type:'post',*/
/*             cache:false,*/
/*             data:{email: $('.so-onepagecheckout input[name="login_email"]').val(),password: $('.so-onepagecheckout input[name="login_password"]').val()},*/
/*             dataType:'json',*/
/*             beforeSend:function(){*/
/*                 ajaxLoadingOn();*/
/*                 $('.so-onepagecheckout #button-login').button('loading');*/
/*             },*/
/*             complete:function(){*/
/*                 ajaxLoadingOff();*/
/*                 $('.so-onepagecheckout #button-login').button('reset');*/
/*             },*/
/*             success:function(json){*/
/*                 if(json['error']&&json['error']['warning']){*/
/*                     alert(json['error']['warning']);*/
/*                 }*/
/*                 if(json['redirect']){*/
/*                     location=json['redirect'];*/
/*                 }*/
/*             },*/
/*             error:function(xhr,ajaxOptions,thrownError){*/
/*                 alert(thrownError+"\r\n"+xhr.statusText+"\r\n"+xhr.responseText);*/
/*             }*/
/*         });*/
/*     }*/
/* */
/*     $(document).delegate('.so-onepagecheckout .confirm-button', 'click', function(){*/
/*         var data={};*/
/*         $('.so-onepagecheckout input[type="text"], .so-onepagecheckout input[type="number"], .so-onepagecheckout input[type="password"], .so-onepagecheckout select, .so-onepagecheckout input:checked, .so-onepagecheckout textarea[name="comment"]').each(function(){*/
/*             data[$(this).attr('name')]=$(this).val();*/
/*         });*/
/* */
/*         $.ajax({*/
/*             url:'index.php?route=checkout/checkout/confirm',*/
/*             type:'post',*/
/*             cache:false,*/
/*             data:data,*/
/*             dataType:'json',*/
/*             beforeSend:function(){*/
/*                 ajaxLoadingOn();*/
/*             },*/
/*             success:function(json){*/
/*                 $('.so-onepagecheckout .text-danger').remove();*/
/*                 $('.so-onepagecheckout .has-error').removeClass('has-error');*/
/*                 if(json['redirect_cart']){*/
/*                     location=json['redirect_cart'];return;*/
/*                 }*/
/*                 */
/*                 if(json['errors']){*/
/*                     $.each(json['errors'],function(k,v){*/
/*                         if(k==='shipping_method'||k==='payment_method'){*/
/*                             return;*/
/*                         }*/
/*                         if($.inArray(k,['payment_country','payment_zone','shipping_country','shipping_zone'])!==-1){*/
/*                             k+='_id';*/
/*                         }else if(k.indexOf('custom_field')===0){*/
/*                             k=k.replace('custom_field','');*/
/*                             k='custom_field['+k+']';*/
/*                         }else if(k.indexOf('payment_custom_field')===0){*/
/*                             k=k.replace('payment_custom_field','');*/
/*                             k='payment_custom_field['+k+']';*/
/*                         }else if(k.indexOf('shipping_custom_field')===0){*/
/*                             k=k.replace('shipping_custom_field','');*/
/*                             k='shipping_custom_field['+k+']';*/
/*                         }*/
/*                         */
/*                         var $element=$('.so-onepagecheckout [name="'+k+'"]');*/
/*                         $element.closest('.form-group').addClass('has-error');*/
/*                         if ($element.closest('label').length)*/
/*                             $element.closest('label').after('<div class="text-danger">'+v+'</div>');*/
/*                         else*/
/*                             $element.after('<div class="text-danger">'+v+'</div>');*/
/*                     });*/
/*                     */
/*                     ajaxLoadingOff();*/
/*                     */
/*                     try{*/
/*                         $('html, body').animate({scrollTop:$('.has-error').offset().top},'slow');*/
/*                     }catch(e){*/
/*                         if (json['errors']['account'][0]) alert(json['errors']['account'][0]);*/
/*                     }*/
/*                     */
/*                     return false;*/
/*                 }*/
/*                 else if(json['redirect']){*/
/*                     location=json['redirect'];*/
/*                 }else{*/
/*                     var $btn=$('.so-onepagecheckout #payment-confirm-button input[type="button"], .so-onepagecheckout #payment-confirm-button input[type="submit"], .so-onepagecheckout #payment-confirm-button .pull-right a, .so-onepagecheckout #payment-confirm-button .right a, .so-onepagecheckout #payment-confirm-button a.button, .so-onepagecheckout #button-confirm, .so-onepagecheckout #button-pay, .so-onepagecheckout #payment-confirm-button.payment-iyzico_checkout_installment .submitButton, .so-onepagecheckout #stripe-confirm').first();*/
/*                     if($btn.attr('href')){*/
/*                         location=$btn.attr('href');*/
/*                     }else{*/
/*                         $btn.trigger('click');*/
/*                     }*/
/*                 }*/
/*             },*/
/*             error:function(xhr,ajaxOptions,thrownError){*/
/*                 alert(thrownError+"\r\n"+xhr.statusText+"\r\n"+xhr.responseText);*/
/*             }*/
/*         });*/
/*     });*/
/* */
/*     $(document).on('so_checkout_customer_group_changed', function(e, value){*/
/*         $.ajax({*/
/*             url:'index.php?route=checkout/checkout',*/
/*             type:'get',*/
/*             cache:false,*/
/*             data:{customer_group_id:value},*/
/*             beforeSend:function(){*/
/*                 ajaxLoadingOn();*/
/*                 $('.so-onepagecheckout #account, .so-onepagecheckout #address').addClass('checkout-loading');*/
/*             },*/
/*             complete:function(){*/
/*                 ajaxLoadingOff();*/
/*                 $('.so-onepagecheckout #account, .so-onepagecheckout #address').removeClass('checkout-loading');*/
/*             },*/
/*             success:function(html){*/
/*                 var $html=$(html);*/
/*                 $('.so-onepagecheckout #account').html($html.find('#account'));*/
/*                 $('.so-onepagecheckout #address').html($html.find('#address'));*/
/*                 $('.so-onepagecheckout #password').html($html.find('#password'));*/
/*                 $('.so-onepagecheckout #account .form-group[data-sort]').detach().each(function(){*/
/*                     if($(this).attr('data-sort')>=0 && $(this).attr('data-sort')<=$('.so-onepagecheckout #account .form-group').length){*/
/*                         $('.so-onepagecheckout #account .form-group').eq($(this).attr('data-sort')).before(this);*/
/*                     }*/
/*                     if($(this).attr('data-sort')>$('#account .form-group').length){*/
/*                         $('.so-onepagecheckout #account .form-group:last').after(this);*/
/*                     }*/
/*                     if($(this).attr('data-sort')<-$('#account .form-group').length){*/
/*                         $('.so-onepagecheckout #account .form-group:first').before(this);*/
/*                     }*/
/*                 });*/
/*                 */
/*                 $(document).trigger('so_checkout_reload_payment');*/
/* */
/*                 if($('.so-onepagecheckout input[name="shipping_address"]').is(':checked')){*/
/*                     $(document).trigger('so_checkout_reload_shipping');*/
/*                 }*/
/*             },*/
/*             error:function(xhr,ajaxOptions,thrownError){*/
/*                 alert(thrownError+"\r\n"+xhr.statusText+"\r\n"+xhr.responseText);*/
/*             }*/
/*         });*/
/*     });*/
/* */
/*     $(document).on('so_checkout_address_changed', function(e, type){*/
/*         var data={};*/
/*         if($('.so-onepagecheckout input[name="'+type+'_address"]:checked').val()==='existing'){*/
/*             data[type+'_address_id']=$('select[name="'+type+'_address_id"]').val();*/
/*         }else{*/
/*             data[type+'_country_id']=$('select[name="'+type+'_country_id"]').val();*/
/*             data[type+'_postcode']=$('input[name="'+type+'_postcode"]').val();*/
/*             data[type+'_zone_id']=$('select[name="'+type+'_zone_id"]').val();*/
/*             {% if not is_logged_in %}*/
/*                 if(type==='payment'&&$('input[name="shipping_address"]').is(":checked")){*/
/*                     data['shipping_country_id']=$('select[name="'+type+'_country_id"]').val();*/
/*                     data['shipping_postcode']=$('input[name="'+type+'_postcode"]').val();*/
/*                     data['shipping_zone_id']=$('select[name="'+type+'_zone_id"]').val();*/
/*                 }*/
/*             {% endif %}*/
/*         }*/
/* */
/*         $.ajax({*/
/*             url:'index.php?route=checkout/checkout/save',*/
/*             type:'post',*/
/*             cache:false,*/
/*             data:data,*/
/*             dataType:'json',*/
/*             success:function(json){*/
/*                 $(document).trigger('so_checkout_reload_'+type);*/
/*                 {% if not is_logged_in %}*/
/*                     if(type==='payment'&&$('input[name="shipping_address"]').is(':checked')){*/
/*                         $(document).trigger('so_checkout_reload_shipping');*/
/*                     }*/
/*                 {% endif %}*/
/*             },*/
/*             error:function(xhr,ajaxOptions,thrownError){*/
/*                 alert(thrownError+"\r\n"+xhr.statusText+"\r\n"+xhr.responseText);*/
/*             }*/
/*         });*/
/*     });*/
/* */
/*     $(document).on('so_checkout_shipping_changed', function (e, value) {*/
/*         $.ajax({*/
/*             url: 'index.php?route=checkout/checkout/save',*/
/*             type: 'post',*/
/*             data: {*/
/*                 shipping_method: value*/
/*             },*/
/*             dataType: 'json',*/
/*             success: function() {*/
/*                 $(document).trigger('so_checkout_reload_payment');*/
/*                 $(document).trigger('so_checkout_reload_cart');*/
/*             },*/
/*             error: function(xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*         });*/
/*     });*/
/* */
/*     $(document).on('so_checkout_payment_changed', function (e, value) {*/
/*         $.ajax({*/
/*             url: 'index.php?route=checkout/checkout/save',*/
/*             type: 'post',*/
/*             data: {*/
/*                 payment_method: value*/
/*             },*/
/*             dataType: 'json',*/
/*             success: function() {*/
/*                 $(document).trigger('so_checkout_reload_cart');*/
/*             },*/
/*             error: function(xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*         });*/
/*     });*/
/* */
/*     $(document).on('so_checkout_reload_shipping', function () {*/
/*         $.ajax({*/
/*             url: 'index.php?route=checkout/checkout/shipping',*/
/*             type: 'get',*/
/*             dataType: 'html',*/
/*             beforeSend: function() {*/
/*                 ajaxLoadingOn();*/
/*                 $('.checkout-shipping-methods').addClass('checkout-loading');*/
/*             },*/
/*             complete: function() {*/
/*                 ajaxLoadingOff();*/
/*                 $('.checkout-shipping-methods').removeClass('checkout-loading');*/
/*             },*/
/*             success: function(html) {*/
/*                 $('.checkout-shipping-methods').replaceWith(html);*/
/*                 $(document).trigger('so_checkout_reload_cart');*/
/*             },*/
/*             error: function(xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*         });*/
/*     });*/
/* */
/*     $(document).on('so_checkout_reload_payment', function () {*/
/*         $.ajax({*/
/*             url: 'index.php?route=checkout/checkout/payment',*/
/*             type: 'get',*/
/*             dataType: 'html',*/
/*             beforeSend: function() {*/
/*                 ajaxLoadingOn();*/
/*                 $('.checkout-payment-methods').addClass('checkout-loading');*/
/*             },*/
/*             complete: function() {*/
/*                 ajaxLoadingOff();*/
/*                 $('.checkout-payment-methods').removeClass('checkout-loading');*/
/*             },*/
/*             success: function(html) {*/
/*                 $('.checkout-payment-methods').replaceWith(html);*/
/*                 $(document).trigger('so_checkout_reload_cart');*/
/*             },*/
/*             error: function(xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*         });*/
/*     });*/
/* */
/*     $(document).on('so_checkout_reload_cart', function (e, first) {*/
/*         $.ajax({*/
/*             url: 'index.php?route=checkout/checkout/cart',*/
/*             type: 'get',*/
/*             dataType: 'html',*/
/*             beforeSend: function() {*/
/*                 if (!first) {*/
/*                     ajaxLoadingOn();*/
/*                     $('.so-onepagecheckout .checkout-cart').addClass('checkout-loading');*/
/*                 }*/
/*             },*/
/*             complete: function() {*/
/*                 if (!first) {*/
/*                     ajaxLoadingOff();*/
/*                     $('.so-onepagecheckout .checkout-cart').removeClass('checkout-loading');*/
/*                 }*/
/*             },*/
/*             success: function(html) {*/
/*                 $('.so-onepagecheckout .checkout-cart').replaceWith(html);*/
/*             },*/
/*             error: function(xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*         });*/
/*     });*/
/* */
/*     $(document).delegate('.so-onepagecheckout .checkout-product .input-group .btn-update', 'click', function () {*/
/*         var key = $(this).attr('data-product-key');*/
/*         var qty  = $('input[name="quantity[' + key + ']"]').val();*/
/*         $.ajax({*/
/*             url: 'index.php?route=checkout/checkout/cart_update',*/
/*             type: 'post',*/
/*             data: {*/
/*                 key: key,*/
/*                 quantity: qty*/
/*             },*/
/*             dataType: 'json',*/
/*             beforeSend: function() {*/
/*                 ajaxLoadingOn();*/
/*                 $('#cart > button > a > span').button('loading');*/
/*                 $('.so-onepagecheckout .checkout-cart').addClass('checkout-loading');*/
/*             },*/
/*             complete: function() {*/
/*                 ajaxLoadingOff();*/
/*                 $('#cart > button > a > span').button('reset');*/
/*             },*/
/*             success: function(json) {*/
/*                 setTimeout(function () {*/
/*                     $('#cart-total').html(json['total']);*/
/*                 }, 100);*/
/* */
/*                 if (json['redirect']) {*/
/*                     location = json['redirect'];*/
/*                 } else {*/
/*                     $('#cart ul').load('index.php?route=common/cart/info ul li');*/
/* */
/*                     $(document).trigger('so_checkout_reload_payment');*/
/*                     $(document).trigger('so_checkout_reload_shipping');*/
/*                 }*/
/*             }*/
/*         });*/
/*     });*/
/* */
/*     $(document).delegate('.so-onepagecheckout .checkout-product .input-group .btn-delete', 'click', function () {*/
/*         var key = $(this).attr('data-product-key');*/
/*         $.ajax({*/
/*             url: 'index.php?route=checkout/checkout/cart_delete',*/
/*             type: 'post',*/
/*             data: {*/
/*                 key: key*/
/*             },*/
/*             dataType: 'json',*/
/*             beforeSend: function() {*/
/*                 ajaxLoadingOn();*/
/*                 $('#cart > button > a > span').button('loading');*/
/*                 $('.so-onepagecheckout .checkout-cart').addClass('checkout-loading');*/
/*             },*/
/*             complete: function() {*/
/*                 ajaxLoadingOff();*/
/*                 $('#cart > button > a > span').button('reset');*/
/*             },*/
/*             success: function(json) {*/
/*                 setTimeout(function () {*/
/*                     $('#cart-total').html(json['total']);*/
/*                 }, 100);*/
/* */
/*                 if (json['redirect']) {*/
/*                     location = json['redirect'];*/
/*                 } else {*/
/*                     $('#cart ul').load('index.php?route=common/cart/info ul li');*/
/* */
/*                     $(document).trigger('so_checkout_reload_payment');*/
/*                     $(document).trigger('so_checkout_reload_shipping');*/
/*                 }*/
/*             }*/
/*         });*/
/*     });*/
/* */
/*     $(document).delegate('#button-voucher', 'click', function() {*/
/*         $.ajax({*/
/*             url:'index.php?route=extension/total/voucher/voucher',*/
/*             type: 'post',*/
/*             data: 'voucher=' + encodeURIComponent($('input[name=\'voucher\']').val()),*/
/*             dataType: 'json',*/
/*             beforeSend: function() {*/
/*                 ajaxLoadingOn();*/
/*                 $('.so-onepagecheckout #button-voucher').button('loading');*/
/*             },*/
/*             complete: function() {*/
/*                 ajaxLoadingOff();*/
/*                 $('.so-onepagecheckout #button-voucher').button('reset');*/
/*             },*/
/*             success: function(json) {*/
/*                 if (json['error']) {*/
/*                     alert(json['error']);*/
/*                 } else {*/
/*                     $('#cart ul').load('index.php?route=common/cart/info ul li');*/
/* */
/*                     $(document).trigger('so_checkout_reload_payment');*/
/*                     $(document).trigger('so_checkout_reload_shipping');*/
/*                 }*/
/*             }*/
/*         });*/
/*     });*/
/* */
/*     $(document).delegate('#button-coupon', 'click', function() {*/
/*         $.ajax({*/
/*             url:'index.php?route=extension/total/coupon/coupon',*/
/*             type: 'post',*/
/*             data: 'coupon=' + encodeURIComponent($('input[name=\'coupon\']').val()),*/
/*             dataType: 'json',*/
/*             beforeSend: function() {*/
/*                 ajaxLoadingOn();*/
/*                 $('.so-onepagecheckout #button-coupon').button('loading');*/
/*             },*/
/*             complete: function() {*/
/*                 ajaxLoadingOff();*/
/*                 $('.so-onepagecheckout #button-coupon').button('reset');*/
/*             },*/
/*             success: function(json) {*/
/*                 if (json['error']) {*/
/*                     alert(json['error']);*/
/*                 } else {*/
/*                     $('#cart ul').load('index.php?route=common/cart/info ul li');*/
/* */
/*                     $(document).trigger('so_checkout_reload_payment');*/
/*                     $(document).trigger('so_checkout_reload_shipping');*/
/*                 }*/
/*             }*/
/*         });*/
/*     });*/
/* */
/*     $(document).delegate('#button-reward', 'click', function() {*/
/*         $.ajax({*/
/*             url:'index.php?route=extension/total/reward/reward',*/
/*             type: 'post',*/
/*             data: 'reward=' + encodeURIComponent($('input[name=\'reward\']').val()),*/
/*             dataType: 'json',*/
/*             beforeSend: function() {*/
/*                 ajaxLoadingOn();*/
/*                 $('.so-onepagecheckout #button-reward').button('loading');*/
/*             },*/
/*             complete: function() {*/
/*                 ajaxLoadingOff();*/
/*                 $('.so-onepagecheckout #button-reward').button('reset');*/
/*             },*/
/*             success: function(json) {*/
/*                 if (json['error']) {*/
/*                     alert(json['error']);*/
/*                 } else {*/
/*                     $('#cart ul').load('index.php?route=common/cart/info ul li');*/
/* */
/*                     $(document).trigger('so_checkout_reload_payment');*/
/*                     $(document).trigger('so_checkout_reload_shipping');*/
/*                 }*/
/*             }*/
/*         });*/
/*     });*/
/* */
/*     {% if is_logged_in %}*/
/*         $('.so-onepagecheckout [value="existing"]').trigger('change');*/
/*     {% else %}*/
/*         $('.so-onepagecheckout input[name="account"]:checked').trigger('change');*/
/*     {% endif %}*/
/* */
/*     $(document).trigger('so_checkout_reload_cart', true);*/
/* </script>*/
/* {{ footer }}*/
