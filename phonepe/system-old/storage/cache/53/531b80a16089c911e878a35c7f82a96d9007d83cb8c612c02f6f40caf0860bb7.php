<?php

/* extension/module/so_quickview.twig */
class __TwigTemplate_a8a02dc6ec2e0491bb2414e9bd62ecb93a0c5dcecef3a3c4578249f8dd1147fb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo " ";
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "
<div id=\"content\">
\t<div class=\"page-header\">
\t\t<div class=\"container-fluid\">
\t\t\t<div class=\"pull-right\">
\t\t\t\t<button type=\"submit\" form=\"form-featured\" data-toggle=\"tooltip\" title=\"";
        // line 6
        echo (isset($context["entry_button_save"]) ? $context["entry_button_save"] : null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i> ";
        echo (isset($context["entry_button_save"]) ? $context["entry_button_save"] : null);
        echo "</button>
\t\t\t\t<a class=\"btn btn-success\" onclick=\"\$('#action').val('save_edit');\$('#form-featured').submit();\" data-toggle=\"tooltip\" title=\"";
        // line 7
        echo (isset($context["entry_button_save_and_edit"]) ? $context["entry_button_save_and_edit"] : null);
        echo "\" ><i class=\"fa fa-pencil-square-o\"></i> ";
        echo (isset($context["entry_button_save_and_edit"]) ? $context["entry_button_save_and_edit"] : null);
        echo "</a>
\t\t\t\t<a class=\"btn btn-info\" onclick=\"\$('#action').val('save_new');\$('#form-featured').submit();\" data-toggle=\"tooltip\" title=\"";
        // line 8
        echo (isset($context["entry_button_save_and_new"]) ? $context["entry_button_save_and_new"] : null);
        echo "\" ><i class=\"fa fa-book\"></i> ";
        echo (isset($context["entry_button_save_and_new"]) ? $context["entry_button_save_and_new"] : null);
        echo "</a>
\t\t\t\t<a href=\"";
        // line 9
        echo (isset($context["cancel"]) ? $context["cancel"] : null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo (isset($context["entry_button_cancel"]) ? $context["entry_button_cancel"] : null);
        echo "\" class=\"btn btn-danger\"><i class=\"fa fa-reply\"></i> ";
        echo (isset($context["entry_button_cancel"]) ? $context["entry_button_cancel"] : null);
        echo "</a>
\t\t\t</div>
\t\t\t<h1>";
        // line 11
        echo (isset($context["heading_title_so"]) ? $context["heading_title_so"] : null);
        echo "</h1>
\t\t\t<ul class=\"breadcrumb\">
\t\t\t\t";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 14
            echo "\t\t\t\t<li><a href=\"";
            echo $this->getAttribute($context["breadcrumb"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array());
            echo "</a></li>
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 16
        echo "\t\t\t</ul>
\t\t</div>
\t</div>
\t<div class=\"container-fluid\">
\t\t";
        // line 20
        if ($this->getAttribute((isset($context["error"]) ? $context["error"] : null), "warning", array(), "any", true, true)) {
            // line 21
            echo "\t\t<div class=\"alert alert-danger\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo $this->getAttribute((isset($context["error"]) ? $context["error"] : null), "warning", array());
            echo "
\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
\t\t</div>
\t\t";
        }
        // line 25
        echo "\t\t";
        if ((array_key_exists("success", $context) && (isset($context["success"]) ? $context["success"] : null))) {
            // line 26
            echo "\t\t<div class=\"alert alert-success\"><i class=\"fa fa-check-circle\"></i> ";
            echo (isset($context["success"]) ? $context["success"] : null);
            echo "
\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
\t\t</div>
\t\t<div class=\"alert alert-info\"><i class=\"fa fa-info-circle\"></i> ";
            // line 29
            echo (isset($context["text_layout"]) ? $context["text_layout"] : null);
            echo "
\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
\t\t</div>
\t\t";
        }
        // line 33
        echo "\t    <div class=\"panel panel-default\">
\t\t\t<div class=\"panel-heading\">
\t\t\t\t<h3 class=\"panel-title\"><i class=\"fa fa-pencil\"></i> ";
        // line 35
        echo (isset($context["subheading"]) ? $context["subheading"] : null);
        echo "</h3>
\t\t\t</div>
\t\t\t<div class=\"panel-body\">
\t\t\t\t<form action=\"";
        // line 38
        echo (isset($context["action"]) ? $context["action"] : null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-featured\" class=\"form-horizontal\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<ul class=\"nav nav-tabs\" role=\"tablist\">
\t\t\t\t\t\t\t<li ";
        // line 41
        if (((isset($context["selectedid"]) ? $context["selectedid"] : null) == 0)) {
            echo " class=\"active\" ";
        }
        echo "> <a href=\"";
        echo (isset($context["link"]) ? $context["link"] : null);
        echo "\"> <span class=\"fa fa-plus\"></span> ";
        echo (isset($context["button_add_module"]) ? $context["button_add_module"] : null);
        echo "</a></li>
\t\t\t\t\t\t\t";
        // line 42
        $context["i"] = 1;
        // line 43
        echo "\t\t\t\t\t\t\t";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["moduletabs"]) ? $context["moduletabs"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["module"]) {
            // line 44
            echo "\t\t\t\t\t\t\t\t<li role=\"presentation\" ";
            if (($this->getAttribute($context["module"], "module_id", array()) == (isset($context["selectedid"]) ? $context["selectedid"] : null))) {
                echo " class=\"active\" ";
            }
            echo ">
\t\t\t\t\t\t\t\t\t<a href=\"";
            // line 45
            echo (isset($context["link"]) ? $context["link"] : null);
            echo "&module_id=";
            echo $this->getAttribute($context["module"], "module_id", array());
            echo "\" aria-controls=\"bannermodule-";
            echo $context["key"];
            echo "\"  >
\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-pencil\"></span> ";
            // line 46
            echo $this->getAttribute($context["module"], "name", array());
            echo "
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t";
            // line 49
            $context["i"] = ((isset($context["i"]) ? $context["i"] : null) + 1);
            // line 50
            echo "\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['module'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 51
        echo "\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<div class=\"col-sm-12\">
\t\t\t\t\t\t";
        // line 55
        $context["module_row"] = 1;
        // line 56
        echo "\t\t\t\t\t\t";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["modules"]) ? $context["modules"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
            // line 57
            echo "\t\t\t\t\t\t";
            if ((isset($context["selectedid"]) ? $context["selectedid"] : null)) {
                // line 58
                echo "\t\t\t\t\t\t\t<div class=\"pull-right\">
\t\t\t\t\t\t\t\t<a href=\"";
                // line 59
                echo (isset($context["action"]) ? $context["action"] : null);
                echo "&delete=1\" class=\"remove btn btn-danger\" ><span><i class=\"fa fa-remove\"></i> ";
                echo (isset($context["entry_button_delete"]) ? $context["entry_button_delete"] : null);
                echo "</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
            }
            // line 62
            echo "\t\t\t\t\t\t\t<div  id=\"tab-module";
            echo (isset($context["module_row"]) ? $context["module_row"] : null);
            echo "\" class=\"col-sm-12\">
\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"action\" id=\"action\" value=\"\"/>
\t\t\t\t\t\t\t\t\t<label class=\"col-sm-3 control-label\" for=\"input-name\"> <b style=\"font-weight:bold; color:#f00\">*</b> <span data-toggle=\"tooltip\" title=\"";
            // line 65
            echo (isset($context["entry_name_desc"]) ? $context["entry_name_desc"] : null);
            echo "\">";
            echo (isset($context["entry_name"]) ? $context["entry_name"] : null);
            echo " </span></label>
\t\t\t\t\t\t\t\t\t<div class=\"col-sm-9\">
\t\t\t\t\t\t\t\t\t\t<div class=\"col-sm-5\">
\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" name=\"name\" value=\"";
            // line 68
            echo $this->getAttribute($context["module"], "name", array());
            echo "\" placeholder=\"";
            echo (isset($context["entry_name"]) ? $context["entry_name"] : null);
            echo "\" id=\"input-name\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t";
            // line 70
            if ($this->getAttribute((isset($context["error"]) ? $context["error"] : null), "name", array(), "any", true, true)) {
                // line 71
                echo "\t\t\t\t\t\t\t\t\t\t<div class=\"text-danger col-sm-12\">";
                echo $this->getAttribute((isset($context["error"]) ? $context["error"] : null), "name", array());
                echo "</div>
\t\t\t\t\t\t\t\t\t\t";
            }
            // line 73
            echo "\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t<label class=\"col-sm-3 control-label\" for=\"input-status\"><span data-toggle=\"tooltip\" title=\"";
            // line 76
            echo (isset($context["entry_status_desc"]) ? $context["entry_status_desc"] : null);
            echo "\">";
            echo (isset($context["entry_status"]) ? $context["entry_status"] : null);
            echo " </span></label>
\t\t\t\t\t\t\t\t\t<div class=\"col-sm-9\">
\t\t\t\t\t\t\t\t\t\t<div class=\"col-sm-5\">
\t\t\t\t\t\t\t\t\t\t\t<select name=\"status\" id=\"input-status\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t\t\t\t";
            // line 80
            if (($this->getAttribute($context["module"], "status", array()) == 1)) {
                // line 81
                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"1\" selected=\"selected\">";
                echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
                echo "</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"0\">";
                // line 82
                echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
                echo "</option>
\t\t\t\t\t\t\t\t\t\t\t\t";
            } else {
                // line 84
                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"1\">";
                echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
                echo "</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"0\" selected=\"selected\">";
                // line 85
                echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
                echo "</option>
\t\t\t\t\t\t\t\t\t\t\t\t";
            }
            // line 87
            echo "\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"tab-pane\">
\t\t\t\t\t\t\t\t<ul class=\"nav nav-tabs\" id=\"so_youtech\">
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<a href=\"#so_module_module\" data-toggle=\"tab\">
\t\t\t\t\t\t\t\t\t\t\t";
            // line 96
            echo (isset($context["entry_module"]) ? $context["entry_module"] : null);
            echo "
\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t<div class=\"tab-content\">\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t<div class=\"tab-pane\" id=\"so_module_module\">
\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\"> 
\t\t\t\t\t\t\t\t\t\t\t<label class=\"col-sm-3 control-label\" for=\"input-class_suffix\">
\t\t\t\t\t\t\t\t\t\t\t\t<span data-toggle=\"tooltip\" title=\"";
            // line 104
            echo (isset($context["entry_class_suffix_desc"]) ? $context["entry_class_suffix_desc"] : null);
            echo "\">";
            echo (isset($context["entry_class_suffix"]) ? $context["entry_class_suffix"] : null);
            echo " </span>
\t\t\t\t\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-sm-9\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-sm-5\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<textarea name=\"class_suffix\" rows=\"5\" class=\"form-control\" id=\"input-class_suffix\">";
            // line 108
            echo $this->getAttribute($context["module"], "class_suffix", array());
            echo "</textarea>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t\t\t<label class=\"col-sm-3 control-label\" for=\"input-head_name\"> <span data-toggle=\"tooltip\" title=\"";
            // line 113
            echo (isset($context["entry_label_button_desc"]) ? $context["entry_label_button_desc"] : null);
            echo "\">";
            echo (isset($context["entry_label_button"]) ? $context["entry_label_button"] : null);
            echo " </span></label>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-sm-9\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-sm-5\">
\t\t\t\t\t\t\t\t\t\t\t\t\t";
            // line 116
            $context["i"] = 0;
            // line 117
            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
                // line 118
                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                $context["i"] = ((isset($context["i"]) ? $context["i"] : null) + 1);
                // line 119
                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" name=\"label_button[";
                echo $this->getAttribute($context["language"], "language_id", array());
                echo "][label_text]\" placeholder=\"";
                echo (isset($context["entry_label_button"]) ? $context["entry_label_button"] : null);
                echo "\" id=\"input-label-text-";
                echo $this->getAttribute($context["language"], "language_id", array());
                echo "\" value=\"";
                echo (($this->getAttribute($this->getAttribute((isset($context["label_button"]) ? $context["label_button"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array", false, true), "label_text", array(), "array", true, true)) ? ($this->getAttribute($this->getAttribute((isset($context["label_button"]) ? $context["label_button"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "label_text", array(), "array")) : (""));
                echo "\" class=\"form-control ";
                echo ((((isset($context["i"]) ? $context["i"] : null) > 1)) ? (" hide ") : (" first-name"));
                echo "\" />
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 120
                if (((isset($context["i"]) ? $context["i"] : null) == 1)) {
                    // line 121
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" class=\"form-control\" id=\"input-label_text\" placeholder=\"";
                    echo (isset($context["entry_label_button"]) ? $context["entry_label_button"] : null);
                    echo "\" value=\"";
                    echo (($this->getAttribute($this->getAttribute((isset($context["label_button"]) ? $context["label_button"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array", false, true), "label_text", array(), "array", true, true)) ? ($this->getAttribute($this->getAttribute((isset($context["label_button"]) ? $context["label_button"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "label_text", array(), "array")) : (""));
                    echo "\" name=\"label_text\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 123
                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 124
            echo "\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-sm-3\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<select  class=\"form-control\" id=\"language\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
            // line 127
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
                // line 128
                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"";
                echo $this->getAttribute($context["language"], "language_id", array());
                echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 129
                echo $this->getAttribute($context["language"], "name", array());
                echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 132
            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t";
            // line 134
            if ($this->getAttribute((isset($context["error"]) ? $context["error"] : null), "label_text", array(), "any", true, true)) {
                // line 135
                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"text-danger col-sm-12\">";
                echo $this->getAttribute((isset($context["error"]) ? $context["error"] : null), "label_text", array());
                echo "</div>
\t\t\t\t\t\t\t\t\t\t\t\t";
            }
            // line 137
            echo "\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t";
            // line 142
            $context["module_row"] = ((isset($context["module_row"]) ? $context["module_row"] : null) + 1);
            // line 143
            echo "\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 144
        echo "\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t        \t</form>
\t        </div>
\t    </div>\t
  \t</div>
\t<script type=\"text/javascript\"><!--
\t\t\$('#so_youtech a:first').tab('show');
\t\t\$('#language').change(function(){
\t\t\tvar that = \$(this), opt_select = \$('option:selected', that).val() , _input = \$('#input-label-text-'+opt_select);
\t\t\t\$('[id^=\"input-label-text-\"]').addClass('hide');
\t\t\t_input.removeClass('hide');
\t\t});

\t\t\$('.first-name').change(function(){
\t\t\t\$('input[name=\"head_name\"]').val(\$(this).val());
\t\t});
//--></script>
</div>
";
        // line 163
        echo (isset($context["footer"]) ? $context["footer"] : null);
    }

    public function getTemplateName()
    {
        return "extension/module/so_quickview.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  421 => 163,  400 => 144,  394 => 143,  392 => 142,  385 => 137,  379 => 135,  377 => 134,  373 => 132,  364 => 129,  359 => 128,  355 => 127,  350 => 124,  344 => 123,  336 => 121,  334 => 120,  321 => 119,  318 => 118,  313 => 117,  311 => 116,  303 => 113,  295 => 108,  286 => 104,  275 => 96,  264 => 87,  259 => 85,  254 => 84,  249 => 82,  244 => 81,  242 => 80,  233 => 76,  228 => 73,  222 => 71,  220 => 70,  213 => 68,  205 => 65,  198 => 62,  190 => 59,  187 => 58,  184 => 57,  179 => 56,  177 => 55,  171 => 51,  165 => 50,  163 => 49,  157 => 46,  149 => 45,  142 => 44,  137 => 43,  135 => 42,  125 => 41,  119 => 38,  113 => 35,  109 => 33,  102 => 29,  95 => 26,  92 => 25,  84 => 21,  82 => 20,  76 => 16,  65 => 14,  61 => 13,  56 => 11,  47 => 9,  41 => 8,  35 => 7,  29 => 6,  19 => 1,);
    }
}
/* {{ header }} {{ column_left }}*/
/* <div id="content">*/
/* 	<div class="page-header">*/
/* 		<div class="container-fluid">*/
/* 			<div class="pull-right">*/
/* 				<button type="submit" form="form-featured" data-toggle="tooltip" title="{{ entry_button_save }}" class="btn btn-primary"><i class="fa fa-save"></i> {{ entry_button_save }}</button>*/
/* 				<a class="btn btn-success" onclick="$('#action').val('save_edit');$('#form-featured').submit();" data-toggle="tooltip" title="{{ entry_button_save_and_edit }}" ><i class="fa fa-pencil-square-o"></i> {{ entry_button_save_and_edit }}</a>*/
/* 				<a class="btn btn-info" onclick="$('#action').val('save_new');$('#form-featured').submit();" data-toggle="tooltip" title="{{ entry_button_save_and_new }}" ><i class="fa fa-book"></i> {{ entry_button_save_and_new }}</a>*/
/* 				<a href="{{ cancel }}" data-toggle="tooltip" title="{{ entry_button_cancel }}" class="btn btn-danger"><i class="fa fa-reply"></i> {{ entry_button_cancel }}</a>*/
/* 			</div>*/
/* 			<h1>{{ heading_title_so }}</h1>*/
/* 			<ul class="breadcrumb">*/
/* 				{% for breadcrumb in breadcrumbs %}*/
/* 				<li><a href="{{ breadcrumb.href }}">{{ breadcrumb.text }}</a></li>*/
/* 				{% endfor %}*/
/* 			</ul>*/
/* 		</div>*/
/* 	</div>*/
/* 	<div class="container-fluid">*/
/* 		{% if error.warning is defined %}*/
/* 		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> {{ error.warning }}*/
/* 			<button type="button" class="close" data-dismiss="alert">&times;</button>*/
/* 		</div>*/
/* 		{% endif %}*/
/* 		{% if success is defined and success %}*/
/* 		<div class="alert alert-success"><i class="fa fa-check-circle"></i> {{ success }}*/
/* 			<button type="button" class="close" data-dismiss="alert">&times;</button>*/
/* 		</div>*/
/* 		<div class="alert alert-info"><i class="fa fa-info-circle"></i> {{ text_layout }}*/
/* 			<button type="button" class="close" data-dismiss="alert">&times;</button>*/
/* 		</div>*/
/* 		{% endif %}*/
/* 	    <div class="panel panel-default">*/
/* 			<div class="panel-heading">*/
/* 				<h3 class="panel-title"><i class="fa fa-pencil"></i> {{ subheading }}</h3>*/
/* 			</div>*/
/* 			<div class="panel-body">*/
/* 				<form action="{{ action }}" method="post" enctype="multipart/form-data" id="form-featured" class="form-horizontal">*/
/* 					<div class="row">*/
/* 						<ul class="nav nav-tabs" role="tablist">*/
/* 							<li {% if selectedid ==0 %} class="active" {% endif %}> <a href="{{ link }}"> <span class="fa fa-plus"></span> {{ button_add_module }}</a></li>*/
/* 							{% set i = 1 %}*/
/* 							{% for key, module in moduletabs %}*/
/* 								<li role="presentation" {% if module.module_id == selectedid %} class="active" {% endif %}>*/
/* 									<a href="{{ link }}&module_id={{ module.module_id }}" aria-controls="bannermodule-{{ key }}"  >*/
/* 										<span class="fa fa-pencil"></span> {{ module.name }}*/
/* 									</a>*/
/* 								</li>*/
/* 								{% set i = i + 1 %}*/
/* 							{% endfor %}*/
/* 						</ul>*/
/* 					</div>*/
/* 					<div class="row">*/
/* 						<div class="col-sm-12">*/
/* 						{% set module_row = 1 %}*/
/* 						{% for module in modules %}*/
/* 						{% if selectedid %}*/
/* 							<div class="pull-right">*/
/* 								<a href="{{ action }}&delete=1" class="remove btn btn-danger" ><span><i class="fa fa-remove"></i> {{ entry_button_delete }}</span></a>*/
/* 							</div>*/
/* 						{% endif %}*/
/* 							<div  id="tab-module{{ module_row }}" class="col-sm-12">*/
/* 								<div class="form-group">*/
/* 									<input type="hidden" name="action" id="action" value=""/>*/
/* 									<label class="col-sm-3 control-label" for="input-name"> <b style="font-weight:bold; color:#f00">*</b> <span data-toggle="tooltip" title="{{ entry_name_desc }}">{{ entry_name }} </span></label>*/
/* 									<div class="col-sm-9">*/
/* 										<div class="col-sm-5">*/
/* 											<input type="text" name="name" value="{{ module.name }}" placeholder="{{ entry_name }}" id="input-name" class="form-control" />*/
/* 										</div>*/
/* 										{% if error.name is defined %}*/
/* 										<div class="text-danger col-sm-12">{{ error.name }}</div>*/
/* 										{% endif %}*/
/* 									</div>*/
/* 								</div>*/
/* 								<div class="form-group">*/
/* 									<label class="col-sm-3 control-label" for="input-status"><span data-toggle="tooltip" title="{{ entry_status_desc }}">{{ entry_status }} </span></label>*/
/* 									<div class="col-sm-9">*/
/* 										<div class="col-sm-5">*/
/* 											<select name="status" id="input-status" class="form-control">*/
/* 												{% if module.status == 1 %}*/
/* 													<option value="1" selected="selected">{{ text_enabled }}</option>*/
/* 													<option value="0">{{ text_disabled }}</option>*/
/* 												{% else %}*/
/* 													<option value="1">{{ text_enabled }}</option>*/
/* 													<option value="0" selected="selected">{{ text_disabled }}</option>*/
/* 												{% endif %}*/
/* 											</select>*/
/* 										</div>*/
/* 									</div>*/
/* 								</div>*/
/* 							</div>*/
/* 							<div class="tab-pane">*/
/* 								<ul class="nav nav-tabs" id="so_youtech">*/
/* 									<li>*/
/* 										<a href="#so_module_module" data-toggle="tab">*/
/* 											{{ entry_module }}*/
/* 										</a>*/
/* 									</li>*/
/* 								</ul>*/
/* 								<div class="tab-content">							*/
/* 									<div class="tab-pane" id="so_module_module">*/
/* 										<div class="form-group"> */
/* 											<label class="col-sm-3 control-label" for="input-class_suffix">*/
/* 												<span data-toggle="tooltip" title="{{ entry_class_suffix_desc }}">{{ entry_class_suffix }} </span>*/
/* 											</label>*/
/* 											<div class="col-sm-9">*/
/* 												<div class="col-sm-5">*/
/* 													<textarea name="class_suffix" rows="5" class="form-control" id="input-class_suffix">{{ module.class_suffix }}</textarea>*/
/* 												</div>*/
/* 											</div>*/
/* 										</div>*/
/* 										<div class="form-group">*/
/* 											<label class="col-sm-3 control-label" for="input-head_name"> <span data-toggle="tooltip" title="{{ entry_label_button_desc }}">{{ entry_label_button }} </span></label>*/
/* 											<div class="col-sm-9">*/
/* 												<div class="col-sm-5">*/
/* 													{% set i = 0 %}*/
/* 													{% for language in languages %}*/
/* 														{% set i = i + 1 %}*/
/* 														<input type="text" name="label_button[{{ language.language_id }}][label_text]" placeholder="{{ entry_label_button }}" id="input-label-text-{{ language.language_id }}" value="{{ label_button[language.language_id]['label_text'] is defined ? label_button[language.language_id]['label_text'] : '' }}" class="form-control {{ i>1 ? ' hide ' : ' first-name' }}" />*/
/* 														{% if i == 1 %}*/
/* 															<input type="hidden" class="form-control" id="input-label_text" placeholder="{{ entry_label_button }}" value="{{ label_button[language.language_id]['label_text'] is defined ? label_button[language.language_id]['label_text'] : '' }}" name="label_text">*/
/* 														{% endif %}*/
/* 													{% endfor %}*/
/* 												</div>*/
/* 												<div class="col-sm-3">*/
/* 													<select  class="form-control" id="language">*/
/* 														{% for language in languages %}*/
/* 														<option value="{{ language.language_id }}">*/
/* 															{{ language.name }}*/
/* 														</option>*/
/* 														{% endfor %}*/
/* 													</select>*/
/* 												</div>*/
/* 												{% if error.label_text is defined %}*/
/* 													<div class="text-danger col-sm-12">{{ error.label_text }}</div>*/
/* 												{% endif %}*/
/* 											</div>*/
/* 										</div>*/
/* 									</div>*/
/* 								</div>*/
/* 							</div>*/
/* 							{% set module_row = module_row + 1 %}*/
/* 						{% endfor %}*/
/* 						</div>*/
/* 					</div>*/
/* 	        	</form>*/
/* 	        </div>*/
/* 	    </div>	*/
/*   	</div>*/
/* 	<script type="text/javascript"><!--*/
/* 		$('#so_youtech a:first').tab('show');*/
/* 		$('#language').change(function(){*/
/* 			var that = $(this), opt_select = $('option:selected', that).val() , _input = $('#input-label-text-'+opt_select);*/
/* 			$('[id^="input-label-text-"]').addClass('hide');*/
/* 			_input.removeClass('hide');*/
/* 		});*/
/* */
/* 		$('.first-name').change(function(){*/
/* 			$('input[name="head_name"]').val($(this).val());*/
/* 		});*/
/* //--></script>*/
/* </div>*/
/* {{ footer }}*/
