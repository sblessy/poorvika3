<?php

/* catalog/wk_precustomer_list.twig */
class __TwigTemplate_3b9e52104db0a2f413dd695a65dbe080ccf77c1da95a28797ad59e1662b1ceec extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo " 
";
        // line 2
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo " 
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\">
        <button id=\"delete\"  class=\"btn btn-danger\" title=\"";
        // line 7
        echo (isset($context["button_delete"]) ? $context["button_delete"] : null);
        echo "\">
          <i class=\"fa fa-trash\"></i>
        </button>
      </div>
      <h1>";
        // line 11
        echo (isset($context["heading_title_enquiry_list"]) ? $context["heading_title_enquiry_list"] : null);
        echo "</h1>
      <ul class=\"breadcrumb\">
        ";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            echo " 
          <li><a href=\"";
            // line 14
            echo $this->getAttribute($context["breadcrumb"], "href", array(), "array");
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array(), "array");
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 15
        echo " 
      </ul>
    </div>
  </div>
  <div class=\"container-fluid\">
    ";
        // line 20
        if ((isset($context["error"]) ? $context["error"] : null)) {
            echo " 
      <div class=\"alert alert-danger\">";
            // line 21
            echo (isset($context["error"]) ? $context["error"] : null);
            echo "</div>
    ";
        }
        // line 22
        echo " 
    ";
        // line 23
        if ((isset($context["error_warning"]) ? $context["error_warning"] : null)) {
            echo " 
      <div class=\"alert alert-danger\">";
            // line 24
            echo (isset($context["error_warning"]) ? $context["error_warning"] : null);
            echo "</div>
    ";
        }
        // line 25
        echo " 
    ";
        // line 26
        if ((isset($context["success"]) ? $context["success"] : null)) {
            echo " 
      <div class=\"alert alert-success\">";
            // line 27
            echo (isset($context["success"]) ? $context["success"] : null);
            echo "</div>
    ";
        }
        // line 28
        echo " 
    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <h3 class=\"panel-title\">";
        // line 31
        echo (isset($context["heading_title_enquiry_list"]) ? $context["heading_title_enquiry_list"] : null);
        echo "</h3>
      </div>
      <div class=\"panel-body\">
        <div class=\"well\">
          <div class=\"row\">
            <div class=\"col-sm-4\">
              <div class=\"form-group\">
                <label class=\"control-label\">
                  ";
        // line 39
        echo (isset($context["text_name"]) ? $context["text_name"] : null);
        echo " 
                </label>
                <input type=\"text\" class=\"form-control\" name=\"filter_name\" value=\"";
        // line 41
        if (array_key_exists("filter_name", $context)) {
            echo (isset($context["filter_name"]) ? $context["filter_name"] : null);
        }
        echo "\" />
              </div>
              <div class=\"form-group\">
                <label class=\"control-label\">
                  ";
        // line 45
        echo (isset($context["text_subject"]) ? $context["text_subject"] : null);
        echo " 
                </label>
                <input type=\"text\" class=\"form-control\" name=\"filter_subject\" value=\"";
        // line 47
        if (array_key_exists("filter_subject", $context)) {
            echo (isset($context["filter_subject"]) ? $context["filter_subject"] : null);
        }
        echo "\" />
              </div>
            </div>
            <div class=\"col-sm-4\">
              <div class=\"form-group\">
                <label class=\"control-label\">
                  ";
        // line 53
        echo (isset($context["text_email"]) ? $context["text_email"] : null);
        echo " 
                </label>
                <input type=\"text\" class=\"form-control\" name=\"filter_email\" value=\"";
        // line 55
        if (array_key_exists("filter_email", $context)) {
            echo (isset($context["filter_email"]) ? $context["filter_email"] : null);
        }
        echo "\" />
              </div>
              <div class=\"form-group\">
                <label class=\"control-label\">
                  ";
        // line 59
        echo (isset($context["text_product_name"]) ? $context["text_product_name"] : null);
        echo " 
                </label>
                <input type=\"text\" class=\"form-control\" name=\"filter_product_name\" value=\"";
        // line 61
        if (array_key_exists("filter_product_name", $context)) {
            echo (isset($context["filter_product_name"]) ? $context["filter_product_name"] : null);
        }
        echo "\" />
              </div>
            </div>
            <div class=\"col-sm-4\">
              <div class=\"form-group\">
                <label class=\"control-label\">
                  ";
        // line 67
        echo (isset($context["text_status"]) ? $context["text_status"] : null);
        echo " 
                </label>
                <select class=\"form-control\" name=\"filter_status\">
                  <option value=\"\"></option>
                  <option value=\"1\" ";
        // line 71
        if ((array_key_exists("filter_status", $context) && (isset($context["filter_status"]) ? $context["filter_status"] : null))) {
            echo " ";
            echo "selected";
        }
        echo " >";
        echo (isset($context["text_open"]) ? $context["text_open"] : null);
        echo "</option>
                  <option value=\"0\" ";
        // line 72
        if ((array_key_exists("filter_status", $context) && ((isset($context["filter_status"]) ? $context["filter_status"] : null) == "0"))) {
            echo " ";
            echo "selected";
        }
        echo " >";
        echo (isset($context["text_resolved"]) ? $context["text_resolved"] : null);
        echo "</option>
                </select>
              </div>
              <div class=\"btn-group pull-right\">
                <button class=\"btn btn-primary \" onclick=\"filter();\" data-toggle=\"tooltip\" title=\"";
        // line 76
        echo (isset($context["button_filter"]) ? $context["button_filter"] : null);
        echo "\" >
                  <i class=\"fa fa-filter\"></i>
                </button>
                <a class=\"btn btn-danger\" onclick=\"clearfilter();\" title=\"";
        // line 79
        echo (isset($context["button_clrfilter"]) ? $context["button_clrfilter"] : null);
        echo "\">
                  <i class=\"fa fa-eraser\"></i>
                </a>
              </div>
            </div>
          </div>
        </div>
        <form action=\"";
        // line 86
        echo (isset($context["delete"]) ? $context["delete"] : null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form\" class=\"form-horizontal\">
          <table class=\"table table-bordered table-hover\">
            <thead>
              <tr>
                <td width=\"1\" style=\"text-align: center;\"><input type=\"checkbox\" onclick=\"\$('input[name*=\\'selected\\']').prop('checked', this.checked);\" /></td>
                <td class=\"text-left\">
                  ";
        // line 92
        if (((isset($context["sort"]) ? $context["sort"] : null) == "pe.customer_name")) {
            echo " 
                    <a href=\"";
            // line 93
            echo (isset($context["sort_name"]) ? $context["sort_name"] : null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, (isset($context["order"]) ? $context["order"] : null));
            echo "\">";
            echo (isset($context["text_name"]) ? $context["text_name"] : null);
            echo "</a>
                  ";
        } else {
            // line 94
            echo " 
                    <a href=\"";
            // line 95
            echo (isset($context["sort_name"]) ? $context["sort_name"] : null);
            echo "\" > ";
            echo (isset($context["text_name"]) ? $context["text_name"] : null);
            echo " </a>
                  ";
        }
        // line 96
        echo " 
                </td>
                <td class=\"text-left\">
                  ";
        // line 99
        if (((isset($context["sort"]) ? $context["sort"] : null) == "pe.email")) {
            echo " 
                    <a href=\"";
            // line 100
            echo (isset($context["sort_email"]) ? $context["sort_email"] : null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, (isset($context["order"]) ? $context["order"] : null));
            echo "\">";
            echo (isset($context["text_email"]) ? $context["text_email"] : null);
            echo " 
                  ";
        } else {
            // line 101
            echo " 
                    <a href=\"";
            // line 102
            echo (isset($context["sort_email"]) ? $context["sort_email"] : null);
            echo "\" > ";
            echo (isset($context["text_email"]) ? $context["text_email"] : null);
            echo " </a>
                  ";
        }
        // line 103
        echo " 
                </td>
                <td class=\"text-left\">
                  ";
        // line 106
        if (((isset($context["sort"]) ? $context["sort"] : null) == "pe.subject")) {
            echo " 
                    <a href=\"";
            // line 107
            echo (isset($context["sort_subject"]) ? $context["sort_subject"] : null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, (isset($context["order"]) ? $context["order"] : null));
            echo "\">";
            echo (isset($context["text_subject"]) ? $context["text_subject"] : null);
            echo " 
                  ";
        } else {
            // line 108
            echo " 
                    <a href=\"";
            // line 109
            echo (isset($context["sort_subject"]) ? $context["sort_subject"] : null);
            echo "\" > ";
            echo (isset($context["text_subject"]) ? $context["text_subject"] : null);
            echo " </a>
                  ";
        }
        // line 110
        echo " 
                </td>
                <td class=\"text-left\">
                  ";
        // line 113
        if (((isset($context["sort"]) ? $context["sort"] : null) == "pd.name")) {
            echo " 
                    <a href=\"";
            // line 114
            echo (isset($context["sort_product_name"]) ? $context["sort_product_name"] : null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, (isset($context["order"]) ? $context["order"] : null));
            echo "\">";
            echo (isset($context["text_product_name"]) ? $context["text_product_name"] : null);
            echo " 
                  ";
        } else {
            // line 115
            echo " 
                    <a href=\"";
            // line 116
            echo (isset($context["sort_product_name"]) ? $context["sort_product_name"] : null);
            echo "\" >";
            echo (isset($context["text_product_name"]) ? $context["text_product_name"] : null);
            echo " </a>
                  ";
        }
        // line 117
        echo " 
                </td>
                <td class=\"text-center\">";
        // line 119
        echo (isset($context["text_total_thread"]) ? $context["text_total_thread"] : null);
        echo "</td>
                <td class=\"text-center\">
                  ";
        // line 121
        if (((isset($context["sort"]) ? $context["sort"] : null) == "pe.status")) {
            echo " 
                    <a href=\"";
            // line 122
            echo (isset($context["sort_status"]) ? $context["sort_status"] : null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, (isset($context["order"]) ? $context["order"] : null));
            echo "\">";
            echo (isset($context["text_status"]) ? $context["text_status"] : null);
            echo " 
                  ";
        } else {
            // line 123
            echo " 
                    <a href=\"";
            // line 124
            echo (isset($context["sort_status"]) ? $context["sort_status"] : null);
            echo "\" >";
            echo (isset($context["text_status"]) ? $context["text_status"] : null);
            echo " </a>
                  ";
        }
        // line 125
        echo " 
                </td>
                <td class=\"text-center\">";
        // line 127
        echo (isset($context["text_action"]) ? $context["text_action"] : null);
        echo "</td>
              </tr>
            </thead>
            <tbody>
              ";
        // line 131
        if ((array_key_exists("enquiries", $context) && (isset($context["enquiries"]) ? $context["enquiries"] : null))) {
            echo " 
                ";
            // line 132
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["enquiries"]) ? $context["enquiries"] : null));
            foreach ($context['_seq'] as $context["index"] => $context["enquiry"]) {
                echo " 
                  <tr>
                    <td style=\"text-align: center;\">
                      <input type=\"checkbox\" name=\"selected[]\" value=\"";
                // line 135
                echo $this->getAttribute($context["enquiry"], "enquiry_id", array(), "array");
                echo "\" />
                    </td>
                    <td class=\"text-left\">";
                // line 137
                echo $this->getAttribute($context["enquiry"], "customer_name", array(), "array");
                echo "</td>
                    <td class=\"text-left\">";
                // line 138
                echo $this->getAttribute($context["enquiry"], "email", array(), "array");
                echo "</td>
                    <td class=\"text-left\">";
                // line 139
                echo $this->getAttribute($context["enquiry"], "subject", array(), "array");
                echo "</td>
                    <td class=\"text-left\">";
                // line 140
                echo $this->getAttribute($context["enquiry"], "name", array(), "array");
                echo "</td>
                    <td class=\"text-center\"><span class=\"badge\">";
                // line 141
                echo $this->getAttribute($context["enquiry"], "total_thread", array(), "array");
                echo "</span></td>
                    <td class=\"text-center\">
                      ";
                // line 143
                echo (($this->getAttribute($context["enquiry"], "status", array(), "array")) ? ((isset($context["text_open"]) ? $context["text_open"] : null)) : ((isset($context["text_resolved"]) ? $context["text_resolved"] : null)));
                echo " 
                    </td>
                    <td class=\"text-center\">
                      <a href=\"";
                // line 146
                echo $this->getAttribute($context["enquiry"], "view", array(), "array");
                echo "\" data-toggle=\"tooltip\" title=\"";
                echo (isset($context["button_view"]) ? $context["button_view"] : null);
                echo "\" class=\"btn btn-warning\">
                        <i class=\"fa fa-eye\"></i>
                      </a>
                    </td>
                  </tr>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['index'], $context['enquiry'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 151
            echo " 
              ";
        } else {
            // line 152
            echo " 
                <tr>
                  <td class=\"text-center\" colspan=\"12\">";
            // line 154
            echo (isset($context["text_no_results"]) ? $context["text_no_results"] : null);
            echo "</td>
                </tr>
              ";
        }
        // line 156
        echo " 
            </tbody>
          </table>
        <div class=\"row\">
          <div class=\"col-sm-6 text-left\">";
        // line 160
        echo (isset($context["pagination"]) ? $context["pagination"] : null);
        echo "</div>
          <div class=\"col-sm-6 text-right\">";
        // line 161
        echo (isset($context["results"]) ? $context["results"] : null);
        echo "</div>
        </div>
      </form>
    </div>
  </div>
</div>
<script type=\"text/javascript\"><!--
\$('body').on('click','#delete',function(){
  var flag = false;
  \$('input[type=\\'checkbox\\']').each(function(){
    if (\$(this).is(':checked')) {
      flag = true;
    }
  });
  if (flag) {
      confirm('";
        // line 176
        echo (isset($context["text_confirmation"]) ? $context["text_confirmation"] : null);
        echo "') ? \$('form').submit() : false
  } else {
     alert('Please select first');
  }
});


function clearfilter() {
  url = 'index.php?route=catalog/wk_preorder&user_token=";
        // line 184
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "';
  location = url;
}

function filter() {
  url = 'index.php?route=catalog/wk_preorder&user_token=";
        // line 189
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "';

  var filter_name = \$('input[name=\\'filter_name\\']').val();
  if (filter_name) {
    url += '&filter_name=' + encodeURIComponent(filter_name);
  }

  var filter_email = \$('input[name=\\'filter_email\\']').val();
  if (filter_email) {
    url += '&filter_email=' + encodeURIComponent(filter_email);
  }

  var filter_subject = \$('input[name=\\'filter_subject\\']').val();
  if (filter_subject) {
    url += '&filter_subject=' + encodeURIComponent(filter_subject);
  }

  var filter_product_name = \$('input[name=\\'filter_product_name\\']').val();
  if (filter_product_name) {
    url += '&filter_product_name=' + encodeURIComponent(filter_product_name);
  }

  var filter_status = \$('select[name=\\'filter_status\\']').val();
  if (filter_status) {
    url += '&filter_status=' + encodeURIComponent(filter_status);
  }

  ";
        // line 216
        if ((array_key_exists("sort", $context) && (isset($context["sort"]) ? $context["sort"] : null))) {
            // line 217
            echo "    url += '&sort=";
            echo (isset($context["sort"]) ? $context["sort"] : null);
            echo "';
  ";
        }
        // line 218
        echo " 

  ";
        // line 220
        if ((array_key_exists("order", $context) && (isset($context["order"]) ? $context["order"] : null))) {
            // line 221
            echo "    url += '&order=";
            echo (isset($context["order"]) ? $context["order"] : null);
            echo "';
  ";
        }
        // line 222
        echo " 
  ";
        // line 223
        if ((array_key_exists("page", $context) && (isset($context["page"]) ? $context["page"] : null))) {
            // line 224
            echo "    url += '&page=";
            echo (isset($context["page"]) ? $context["page"] : null);
            echo "';
  ";
        }
        // line 225
        echo " 
  location = url;
}
jQuery('.pvisible').click(function(){
   \$(this).parent().children('.viewProducts').css('display','block');
})
jQuery('.close').click(function(){
  \$(this).parent().css('display','none');
})
//--></script>
<script>
\$('input[name=\\'filter_name\\']').autocomplete({

\t'source': function(request, response) {
\t\t\$.ajax({
\t\t\turl: 'index.php?route=catalog/wk_preorder/autocompleteFilter&user_token=";
        // line 240
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "&filter_name=' +  encodeURIComponent(request),
\t\t\tdataType: 'json',
\t\t\tsuccess: function(json) {
\t\t\t\tresponse(\$.map(json, function(item) {
\t\t\t\t\treturn {
\t\t\t\t\t\tlabel: item['customer_name'],
\t\t\t\t\t\tvalue: item['product_id']
\t\t\t\t\t}
\t\t\t\t}));
\t\t\t}
\t\t});
\t},
\t'select': function(item) {
\t\t\$('input[name=\\'filter_name\\']').val(item['label']);
\t}
});
\$('input[name=\\'filter_email\\']').autocomplete({

\t'source': function(request, response) {
\t\t\$.ajax({
\t\t\turl: 'index.php?route=catalog/wk_preorder/autocompleteFilter&user_token=";
        // line 260
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "&filter_email=' +  encodeURIComponent(request),
\t\t\tdataType: 'json',
\t\t\tsuccess: function(json) {
\t\t\t\tresponse(\$.map(json, function(item) {
\t\t\t\t\treturn {
\t\t\t\t\t\tlabel: item['email'],
\t\t\t\t\t\tvalue: item['product_id']
\t\t\t\t\t}
\t\t\t\t}));
\t\t\t}
\t\t});
\t},
\t'select': function(item) {
\t\t\$('input[name=\\'filter_email\\']').val(item['label']);
\t}
});
\$('input[name=\\'filter_product_name\\']').autocomplete({

\t'source': function(request, response) {
\t\t\$.ajax({
\t\t\turl: 'index.php?route=catalog/wk_preorder/autocompleteFilter&user_token=";
        // line 280
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "&filter_product_name=' +  encodeURIComponent(request),
\t\t\tdataType: 'json',
\t\t\tsuccess: function(json) {
\t\t\t\tresponse(\$.map(json, function(item) {
\t\t\t\t\treturn {
\t\t\t\t\t\tlabel: item['name'],
\t\t\t\t\t\tvalue: item['product_id']
\t\t\t\t\t}
\t\t\t\t}));
\t\t\t}
\t\t});
\t},
\t'select': function(item) {
\t\t\$('input[name=\\'filter_product_name\\']').val(item['label']);
\t}
});
\$('input[name=\\'filter_subject\\']').autocomplete({

\t'source': function(request, response) {
\t\t\$.ajax({
\t\t\turl: 'index.php?route=catalog/wk_preorder/autocompleteFilter&user_token=";
        // line 300
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "&filter_subject=' +  encodeURIComponent(request),
\t\t\tdataType: 'json',
\t\t\tsuccess: function(json) {
\t\t\t\tresponse(\$.map(json, function(item) {
\t\t\t\t\treturn {
\t\t\t\t\t\tlabel: item['subject'],
\t\t\t\t\t\tvalue: item['product_id']
\t\t\t\t\t}
\t\t\t\t}));
\t\t\t}
\t\t});
\t},
\t'select': function(item) {
\t\t\$('input[name=\\'filter_subject\\']').val(item['label']);
\t}
});
</script>
";
        // line 317
        echo (isset($context["footer"]) ? $context["footer"] : null);
        echo " 
";
    }

    public function getTemplateName()
    {
        return "catalog/wk_precustomer_list.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  664 => 317,  644 => 300,  621 => 280,  598 => 260,  575 => 240,  558 => 225,  552 => 224,  550 => 223,  547 => 222,  541 => 221,  539 => 220,  535 => 218,  529 => 217,  527 => 216,  497 => 189,  489 => 184,  478 => 176,  460 => 161,  456 => 160,  450 => 156,  444 => 154,  440 => 152,  436 => 151,  422 => 146,  416 => 143,  411 => 141,  407 => 140,  403 => 139,  399 => 138,  395 => 137,  390 => 135,  382 => 132,  378 => 131,  371 => 127,  367 => 125,  360 => 124,  357 => 123,  348 => 122,  344 => 121,  339 => 119,  335 => 117,  328 => 116,  325 => 115,  316 => 114,  312 => 113,  307 => 110,  300 => 109,  297 => 108,  288 => 107,  284 => 106,  279 => 103,  272 => 102,  269 => 101,  260 => 100,  256 => 99,  251 => 96,  244 => 95,  241 => 94,  232 => 93,  228 => 92,  219 => 86,  209 => 79,  203 => 76,  191 => 72,  182 => 71,  175 => 67,  164 => 61,  159 => 59,  150 => 55,  145 => 53,  134 => 47,  129 => 45,  120 => 41,  115 => 39,  104 => 31,  99 => 28,  94 => 27,  90 => 26,  87 => 25,  82 => 24,  78 => 23,  75 => 22,  70 => 21,  66 => 20,  59 => 15,  49 => 14,  43 => 13,  38 => 11,  31 => 7,  23 => 2,  19 => 1,);
    }
}
/* {{ header }} */
/* {{ column_left }} */
/* <div id="content">*/
/*   <div class="page-header">*/
/*     <div class="container-fluid">*/
/*       <div class="pull-right">*/
/*         <button id="delete"  class="btn btn-danger" title="{{ button_delete }}">*/
/*           <i class="fa fa-trash"></i>*/
/*         </button>*/
/*       </div>*/
/*       <h1>{{ heading_title_enquiry_list }}</h1>*/
/*       <ul class="breadcrumb">*/
/*         {% for breadcrumb in breadcrumbs %} */
/*           <li><a href="{{ breadcrumb['href'] }}">{{ breadcrumb['text'] }}</a></li>*/
/*         {% endfor %} */
/*       </ul>*/
/*     </div>*/
/*   </div>*/
/*   <div class="container-fluid">*/
/*     {% if (error) %} */
/*       <div class="alert alert-danger">{{ error }}</div>*/
/*     {% endif %} */
/*     {% if (error_warning) %} */
/*       <div class="alert alert-danger">{{ error_warning }}</div>*/
/*     {% endif %} */
/*     {% if (success) %} */
/*       <div class="alert alert-success">{{ success }}</div>*/
/*     {% endif %} */
/*     <div class="panel panel-default">*/
/*       <div class="panel-heading">*/
/*         <h3 class="panel-title">{{ heading_title_enquiry_list }}</h3>*/
/*       </div>*/
/*       <div class="panel-body">*/
/*         <div class="well">*/
/*           <div class="row">*/
/*             <div class="col-sm-4">*/
/*               <div class="form-group">*/
/*                 <label class="control-label">*/
/*                   {{ text_name }} */
/*                 </label>*/
/*                 <input type="text" class="form-control" name="filter_name" value="{% if (filter_name is defined) %}{{ filter_name }}{% endif %}" />*/
/*               </div>*/
/*               <div class="form-group">*/
/*                 <label class="control-label">*/
/*                   {{ text_subject }} */
/*                 </label>*/
/*                 <input type="text" class="form-control" name="filter_subject" value="{% if (filter_subject is defined) %}{{ filter_subject }}{% endif %}" />*/
/*               </div>*/
/*             </div>*/
/*             <div class="col-sm-4">*/
/*               <div class="form-group">*/
/*                 <label class="control-label">*/
/*                   {{ text_email }} */
/*                 </label>*/
/*                 <input type="text" class="form-control" name="filter_email" value="{% if (filter_email is defined) %}{{ filter_email }}{% endif %}" />*/
/*               </div>*/
/*               <div class="form-group">*/
/*                 <label class="control-label">*/
/*                   {{ text_product_name }} */
/*                 </label>*/
/*                 <input type="text" class="form-control" name="filter_product_name" value="{% if (filter_product_name is defined) %}{{ filter_product_name }}{% endif %}" />*/
/*               </div>*/
/*             </div>*/
/*             <div class="col-sm-4">*/
/*               <div class="form-group">*/
/*                 <label class="control-label">*/
/*                   {{ text_status }} */
/*                 </label>*/
/*                 <select class="form-control" name="filter_status">*/
/*                   <option value=""></option>*/
/*                   <option value="1" {% if (filter_status is defined and filter_status) %} {{ "selected" }}{% endif %} >{{ text_open }}</option>*/
/*                   <option value="0" {% if (filter_status is defined and filter_status == '0') %} {{ "selected" }}{% endif %} >{{ text_resolved }}</option>*/
/*                 </select>*/
/*               </div>*/
/*               <div class="btn-group pull-right">*/
/*                 <button class="btn btn-primary " onclick="filter();" data-toggle="tooltip" title="{{ button_filter }}" >*/
/*                   <i class="fa fa-filter"></i>*/
/*                 </button>*/
/*                 <a class="btn btn-danger" onclick="clearfilter();" title="{{ button_clrfilter }}">*/
/*                   <i class="fa fa-eraser"></i>*/
/*                 </a>*/
/*               </div>*/
/*             </div>*/
/*           </div>*/
/*         </div>*/
/*         <form action="{{ delete }}" method="post" enctype="multipart/form-data" id="form" class="form-horizontal">*/
/*           <table class="table table-bordered table-hover">*/
/*             <thead>*/
/*               <tr>*/
/*                 <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>*/
/*                 <td class="text-left">*/
/*                   {% if (sort == 'pe.customer_name') %} */
/*                     <a href="{{ sort_name }}" class="{{ order | lower }}">{{ text_name }}</a>*/
/*                   {% else %} */
/*                     <a href="{{ sort_name }}" > {{ text_name }} </a>*/
/*                   {% endif %} */
/*                 </td>*/
/*                 <td class="text-left">*/
/*                   {% if (sort == 'pe.email') %} */
/*                     <a href="{{ sort_email }}" class="{{ order | lower }}">{{ text_email }} */
/*                   {% else %} */
/*                     <a href="{{ sort_email }}" > {{ text_email }} </a>*/
/*                   {% endif %} */
/*                 </td>*/
/*                 <td class="text-left">*/
/*                   {% if (sort == 'pe.subject') %} */
/*                     <a href="{{ sort_subject }}" class="{{ order | lower }}">{{ text_subject }} */
/*                   {% else %} */
/*                     <a href="{{ sort_subject }}" > {{ text_subject }} </a>*/
/*                   {% endif %} */
/*                 </td>*/
/*                 <td class="text-left">*/
/*                   {% if (sort == 'pd.name') %} */
/*                     <a href="{{ sort_product_name }}" class="{{ order | lower }}">{{ text_product_name }} */
/*                   {% else %} */
/*                     <a href="{{ sort_product_name }}" >{{ text_product_name }} </a>*/
/*                   {% endif %} */
/*                 </td>*/
/*                 <td class="text-center">{{ text_total_thread }}</td>*/
/*                 <td class="text-center">*/
/*                   {% if (sort == 'pe.status') %} */
/*                     <a href="{{ sort_status }}" class="{{ order | lower }}">{{ text_status }} */
/*                   {% else %} */
/*                     <a href="{{ sort_status }}" >{{ text_status }} </a>*/
/*                   {% endif %} */
/*                 </td>*/
/*                 <td class="text-center">{{ text_action }}</td>*/
/*               </tr>*/
/*             </thead>*/
/*             <tbody>*/
/*               {% if (enquiries is defined and enquiries) %} */
/*                 {% for index,enquiry in enquiries %} */
/*                   <tr>*/
/*                     <td style="text-align: center;">*/
/*                       <input type="checkbox" name="selected[]" value="{{ enquiry['enquiry_id'] }}" />*/
/*                     </td>*/
/*                     <td class="text-left">{{ enquiry['customer_name'] }}</td>*/
/*                     <td class="text-left">{{ enquiry['email'] }}</td>*/
/*                     <td class="text-left">{{ enquiry['subject'] }}</td>*/
/*                     <td class="text-left">{{ enquiry['name'] }}</td>*/
/*                     <td class="text-center"><span class="badge">{{ enquiry['total_thread'] }}</span></td>*/
/*                     <td class="text-center">*/
/*                       {{ enquiry['status'] ? text_open : text_resolved }} */
/*                     </td>*/
/*                     <td class="text-center">*/
/*                       <a href="{{ enquiry['view'] }}" data-toggle="tooltip" title="{{ button_view }}" class="btn btn-warning">*/
/*                         <i class="fa fa-eye"></i>*/
/*                       </a>*/
/*                     </td>*/
/*                   </tr>*/
/*                 {% endfor %} */
/*               {% else %} */
/*                 <tr>*/
/*                   <td class="text-center" colspan="12">{{ text_no_results }}</td>*/
/*                 </tr>*/
/*               {% endif %} */
/*             </tbody>*/
/*           </table>*/
/*         <div class="row">*/
/*           <div class="col-sm-6 text-left">{{ pagination }}</div>*/
/*           <div class="col-sm-6 text-right">{{ results }}</div>*/
/*         </div>*/
/*       </form>*/
/*     </div>*/
/*   </div>*/
/* </div>*/
/* <script type="text/javascript"><!--*/
/* $('body').on('click','#delete',function(){*/
/*   var flag = false;*/
/*   $('input[type=\'checkbox\']').each(function(){*/
/*     if ($(this).is(':checked')) {*/
/*       flag = true;*/
/*     }*/
/*   });*/
/*   if (flag) {*/
/*       confirm('{{ text_confirmation }}') ? $('form').submit() : false*/
/*   } else {*/
/*      alert('Please select first');*/
/*   }*/
/* });*/
/* */
/* */
/* function clearfilter() {*/
/*   url = 'index.php?route=catalog/wk_preorder&user_token={{ user_token }}';*/
/*   location = url;*/
/* }*/
/* */
/* function filter() {*/
/*   url = 'index.php?route=catalog/wk_preorder&user_token={{ user_token }}';*/
/* */
/*   var filter_name = $('input[name=\'filter_name\']').val();*/
/*   if (filter_name) {*/
/*     url += '&filter_name=' + encodeURIComponent(filter_name);*/
/*   }*/
/* */
/*   var filter_email = $('input[name=\'filter_email\']').val();*/
/*   if (filter_email) {*/
/*     url += '&filter_email=' + encodeURIComponent(filter_email);*/
/*   }*/
/* */
/*   var filter_subject = $('input[name=\'filter_subject\']').val();*/
/*   if (filter_subject) {*/
/*     url += '&filter_subject=' + encodeURIComponent(filter_subject);*/
/*   }*/
/* */
/*   var filter_product_name = $('input[name=\'filter_product_name\']').val();*/
/*   if (filter_product_name) {*/
/*     url += '&filter_product_name=' + encodeURIComponent(filter_product_name);*/
/*   }*/
/* */
/*   var filter_status = $('select[name=\'filter_status\']').val();*/
/*   if (filter_status) {*/
/*     url += '&filter_status=' + encodeURIComponent(filter_status);*/
/*   }*/
/* */
/*   {% if (sort is defined and sort) %}*/
/*     url += '&sort={{ sort }}';*/
/*   {% endif %} */
/* */
/*   {% if (order is defined and order) %}*/
/*     url += '&order={{ order }}';*/
/*   {% endif %} */
/*   {% if (page is defined and page) %}*/
/*     url += '&page={{ page }}';*/
/*   {% endif %} */
/*   location = url;*/
/* }*/
/* jQuery('.pvisible').click(function(){*/
/*    $(this).parent().children('.viewProducts').css('display','block');*/
/* })*/
/* jQuery('.close').click(function(){*/
/*   $(this).parent().css('display','none');*/
/* })*/
/* //--></script>*/
/* <script>*/
/* $('input[name=\'filter_name\']').autocomplete({*/
/* */
/* 	'source': function(request, response) {*/
/* 		$.ajax({*/
/* 			url: 'index.php?route=catalog/wk_preorder/autocompleteFilter&user_token={{ user_token }}&filter_name=' +  encodeURIComponent(request),*/
/* 			dataType: 'json',*/
/* 			success: function(json) {*/
/* 				response($.map(json, function(item) {*/
/* 					return {*/
/* 						label: item['customer_name'],*/
/* 						value: item['product_id']*/
/* 					}*/
/* 				}));*/
/* 			}*/
/* 		});*/
/* 	},*/
/* 	'select': function(item) {*/
/* 		$('input[name=\'filter_name\']').val(item['label']);*/
/* 	}*/
/* });*/
/* $('input[name=\'filter_email\']').autocomplete({*/
/* */
/* 	'source': function(request, response) {*/
/* 		$.ajax({*/
/* 			url: 'index.php?route=catalog/wk_preorder/autocompleteFilter&user_token={{ user_token }}&filter_email=' +  encodeURIComponent(request),*/
/* 			dataType: 'json',*/
/* 			success: function(json) {*/
/* 				response($.map(json, function(item) {*/
/* 					return {*/
/* 						label: item['email'],*/
/* 						value: item['product_id']*/
/* 					}*/
/* 				}));*/
/* 			}*/
/* 		});*/
/* 	},*/
/* 	'select': function(item) {*/
/* 		$('input[name=\'filter_email\']').val(item['label']);*/
/* 	}*/
/* });*/
/* $('input[name=\'filter_product_name\']').autocomplete({*/
/* */
/* 	'source': function(request, response) {*/
/* 		$.ajax({*/
/* 			url: 'index.php?route=catalog/wk_preorder/autocompleteFilter&user_token={{ user_token }}&filter_product_name=' +  encodeURIComponent(request),*/
/* 			dataType: 'json',*/
/* 			success: function(json) {*/
/* 				response($.map(json, function(item) {*/
/* 					return {*/
/* 						label: item['name'],*/
/* 						value: item['product_id']*/
/* 					}*/
/* 				}));*/
/* 			}*/
/* 		});*/
/* 	},*/
/* 	'select': function(item) {*/
/* 		$('input[name=\'filter_product_name\']').val(item['label']);*/
/* 	}*/
/* });*/
/* $('input[name=\'filter_subject\']').autocomplete({*/
/* */
/* 	'source': function(request, response) {*/
/* 		$.ajax({*/
/* 			url: 'index.php?route=catalog/wk_preorder/autocompleteFilter&user_token={{ user_token }}&filter_subject=' +  encodeURIComponent(request),*/
/* 			dataType: 'json',*/
/* 			success: function(json) {*/
/* 				response($.map(json, function(item) {*/
/* 					return {*/
/* 						label: item['subject'],*/
/* 						value: item['product_id']*/
/* 					}*/
/* 				}));*/
/* 			}*/
/* 		});*/
/* 	},*/
/* 	'select': function(item) {*/
/* 		$('input[name=\'filter_subject\']').val(item['label']);*/
/* 	}*/
/* });*/
/* </script>*/
/* {{ footer }} */
/* */
