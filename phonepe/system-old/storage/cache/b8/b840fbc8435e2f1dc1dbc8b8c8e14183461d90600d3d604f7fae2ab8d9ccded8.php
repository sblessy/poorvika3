<?php

/* catalog/mail.twig */
class __TwigTemplate_ac9ffb3795a96e069c4a2d2ae33d6bd0a1faeb42b7749de8b2084c964eefafdc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd\">
<html>
<head>
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
<title>";
        // line 5
        echo (isset($context["subject"]) ? $context["subject"] : null);
        echo "</title>
</head>
<body>
<div class=\"content\" >
\t";
        // line 9
        echo (isset($context["message"]) ? $context["message"] : null);
        echo "
</div>
</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "catalog/mail.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  32 => 9,  25 => 5,  19 => 1,);
    }
}
/* <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd">*/
/* <html>*/
/* <head>*/
/* <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">*/
/* <title>{{ subject}}</title>*/
/* </head>*/
/* <body>*/
/* <div class="content" >*/
/* 	{{ message | raw }}*/
/* </div>*/
/* </body>*/
/* </html>*/
/* */
