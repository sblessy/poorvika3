<?php

/* so-destino/template/header/header5.twig */
class __TwigTemplate_b03d1d6f5d5a3291421a3f4bf84c83a987fa5602b2298d8e03692f8455d953c9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $context["hidden_headercenter"] = ((($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "toppanel_type"), "method") == "2")) ? ("hidden-compact") : (""));
        // line 3
        $context["hidden_headerbottom"] = ((($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "toppanel_type"), "method") == "1")) ? ("hidden-compact") : (""));
        // line 4
        echo "
<header id=\"header\" class=\" variant typeheader-";
        // line 5
        echo (((isset($context["typeheader"]) ? $context["typeheader"] : null)) ? ((isset($context["typeheader"]) ? $context["typeheader"] : null)) : ("1"));
        echo "\">
   
\t<!-- HEADER TOP -->
\t<div class=\"header-top compact-hidden custom\">
\t     <!-- HEADER TOP TITLE BAR Antony-->
  <!-- <div class=\"header-top-title-bar\">
\t<div class=\"row top-row\">
\t<div class=\"col-12 col-sm-12 col-md-4 col-xs-4\">
\t\t<i class=\"fa fa-pie-chart\" aria-hidden=\"true\" class=\"chart\"></i>

\t\t<i class=\"fa fa-comment-o\" aria-hidden=\"true\" class=\"comment\"></i>

        </div>
        <div class=\"col-12 col-sm-12 col-md-4 col-xs-4\">
        \t<p class=\"title-bar\">New Website Layout</p>
        </div>
        <div class=\"col-12 col-sm-12 col-md-4 col-xs-4 button-section\">
        \t<i class=\"fa fa-question\" aria-hidden=\"true\"></i>
        \t<button class=\"btn btn-info question\" type=\"submit\"><span>Login or Create Account </span></button>

        </div>
    </div>
    </div> -->
      <!-- HEADER TOP TITLE BAR Antony-->
\t\t<div class=\"inner\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"header-top-left col-lg-1 col-md-1 col-sm-1 col-xs-6\">
\t\t\t\t\t<!-- LANGUAGE CURENCY -->
\t\t\t\t<!--\t";
        // line 33
        if ($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "lang_status"), "method")) {
            // line 34
            echo "\t\t\t\t\t<ul class=\"top-link list-inline lang-curr\">
\t\t\t\t\t\t";
            // line 35
            if ((isset($context["currency"]) ? $context["currency"] : null)) {
                echo "<li class=\"currency\"> ";
                echo (isset($context["currency"]) ? $context["currency"] : null);
                echo "  </li> ";
            }
            // line 36
            echo "\t\t\t\t\t\t";
            if ((isset($context["language"]) ? $context["language"] : null)) {
                echo " <li class=\"language\">";
                echo (isset($context["language"]) ? $context["language"] : null);
                echo " </li>\t";
            }
            echo "\t\t\t
\t\t\t\t\t</ul>\t\t\t\t
\t\t\t\t\t";
        }
        // line 38
        echo " 
\t\t\t\t\t
\t\t\t\t\t";
        // line 40
        if (($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "phone_status"), "method") && $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "contact_number"), "method"))) {
            // line 41
            echo "\t\t\t\t\t<div class=\"telephone hidden-xs hidden-sm hidden-md\" >
\t\t\t\t\t\t";
            // line 42
            echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "decode_entities", array(0 => $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "contact_number"), "method")), "method");
            echo "
\t\t\t\t\t</div>
\t\t\t\t\t";
        }
        // line 44
        echo "-->
\t\t\t\t\t<div id=\"wrapper\">
        <div class=\"overlay\"></div>
    
        <!-- Sidebar -->
        <nav class=\"navbar navbar-inverse navbar-fixed-top\" id=\"sidebar-wrapper\" role=\"navigation\">
            <ul class=\"nav sidebar-nav\">
                <li class=\"sidebar-brand\">
                    <a href=\"#\">
                       Sign In & Sign Up
                    </a>
                </li>
                <li>
                    <a href=\"#\">Home</a>
                </li>
                <li>Categories</li>
                <li class=\"dropdown\">
                  <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Mobiles & Tablets<span class=\"caret\"></span></a>
                  <ul class=\"dropdown-menu\" role=\"menu\">
                    <li><a href=\"#\">Smartphones</a></li>
                    <li><a href=\"#\">Wearable Tech</a></li>
                    <li><a href=\"#\">Mobile, Tablet Accessories</a></li>
                    <li><a href=\"#\">Headphones & Headsets</a></li>
                    <li><a href=\"#\">Tablets & eReaders</a></li>
                    <li><a href=\"#\">Power Banks</a></li>
                    <li><a href=\"#\">Mobile Cases & Protectors</a></li>
                  </ul>
                </li>
                <li>
                    <a href=\"#\">Computers</a>
                </li>
                <li>
                    <a href=\"#\">Cameras</a>
                </li>
                <li>
                    <a href=\"#\">Television & Audio</a>
                </li>
                <li>
                    <a href=\"#\">Accessories</a>
                </li>
            </ul>
        </nav>
        <!-- /#sidebar-wrapper -->
        <!-- Page Content -->
        <div id=\"page-content-wrapper\">
            <button type=\"button\" class=\"hamburger is-closed\" data-toggle=\"offcanvas\">
                <span class=\"hamb-top\"></span>
          <span class=\"hamb-middle\"></span>
        <span class=\"hamb-bottom\"></span>
            </button>
        </div>
        <!-- /#page-content-wrapper -->


    </div>
    <!-- /#wrapper -->
\t\t\t\t\t
\t\t\t\t</div>
\t\t\t\t<div class=\"navbar-logo col-lg-2 col-md-2  col-sm-4 col-xs-12\">
\t\t\t\t\t<div class=\"logo\">
\t\t\t\t   \t\t";
        // line 104
        echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_logo", array(), "method");
        echo " 
\t\t\t\t   \t<!--<a href=\"http://poorvikabeta.webindia.com/index.php?route=common/home\">
\t\t\t\t   \t    <img class=\"lazyautosizes lazyloaded\" data-sizes=\"auto\" src=\"https://s1.poorvikamobile.com/image/data/poorvika_mobile_five/poorvika-4-logo.png\" data-src=\"https://s1.poorvikamobile.com/image/data/poorvika_mobile_five/poorvika-4-logo.png\" title=\"Poorvika\" alt=\"Poorvika\" sizes=\"194px\">
\t\t\t\t   \t    </a>-->
\t\t\t\t   \t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-lg-4 col-md-4  col-sm-3 col-xs-12 cata-parent\">
\t\t\t\t   
\t\t\t\t\t<div class=\"search-header-w cata-child\">
\t\t\t\t\t    <div class=\"choose-cata\">
\t\t\t\t\t<select class=\"form-control option cata-child-opn\">
\t\t\t\t\t  <option value=\"\" disabled selected>All Categories</option>
\t\t\t\t\t  <option>Mobiles</option>
\t\t\t\t\t  <option>Mobile Accessories</option>
\t\t\t\t\t  <option>Laptop and Tablets</option>
\t\t\t\t\t  <option>Computer Accessories</option>
\t\t\t\t\t  <option>Television</option>
\t\t\t\t\t  <option>Other Home Entertainment</option>
\t\t\t\t\t  <option>TV Accessories</option>
\t\t\t\t\t  <option>Audio</option>
\t\t\t\t\t  <option>Voice Assistence Devices</option>
\t\t\t\t\t  <option>Cameras</option>
\t\t\t\t\t  <option>Smart Waches</option>
\t\t\t\t\t  <option>Smart Home Devices</option>
\t\t\t\t\t  <option>Deals</option>
\t\t\t\t\t  <option>Discounts</option>
\t\t\t\t\t  <option>Combo Offers</option>
\t\t\t\t\t  <option>Buy Back</option>
\t\t\t\t\t  <option>Under Rs.5000 to Rs.10000</option>
\t\t\t\t\t  <option>Under Rs.10001 to Rs.20000</option>
\t\t\t\t\t  <option>Under Rs.20000 to Rs.35000</option>
\t\t\t\t\t</select>
\t\t\t\t\t</div>\t
\t\t\t\t\t";
        // line 137
        echo (isset($context["search_block"]) ? $context["search_block"] : null);
        echo "
\t\t\t\t    </div>
\t\t\t\t</div>
\t\t\t\t<div class=\"header-top-right collapsed-block col-lg-5 col-md-5  col-sm-4 col-xs-6 \">
\t\t\t\t\t<ul class=\"top-link list-inline\">
\t\t\t\t\t\t";
        // line 142
        if ($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "welcome_message_status"), "method")) {
            // line 143
            echo "\t\t\t\t\t\t\t<li class=\"hidden-sm hidden-xs welcome-msg\">
\t\t\t\t\t\t\t\t";
            // line 144
            if ( !twig_test_empty($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "welcome_message"), "method"))) {
                // line 145
                echo "\t\t\t\t\t\t\t\t\t";
                echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "decode_entities", array(0 => $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "welcome_message"), "method")), "method");
                echo "
\t\t\t\t\t\t\t\t";
            }
            // line 146
            echo " 
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t";
        }
        // line 149
        echo "\t\t\t\t\t\t
\t\t\t\t\t\t<li class=\"cart-bag col-lg-4 col-md-4  col-sm-4 col-xs-12\">
    \t\t\t\t\t\t<div class=\"shopping_cart\">\t\t\t\t\t\t\t
            \t\t\t\t \t";
        // line 152
        echo (isset($context["cart"]) ? $context["cart"] : null);
        echo "
            \t\t\t\t</div>
        \t\t\t\t</li>
        \t\t\t\t<!-- <li><a class=\"stores\">Stores</a></li> -->
                        <li class=\"dropdown locate col-lg-4 col-md-4  col-sm-4 col-xs-12\">
          <a href=\"#\" class=\"dropdown-toggle stores\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\"> Locate <br><h5>Stores</h5><span class=\"caret\"></span></a>
          <ul class=\"dropdown-menu locator\">
           
            <li class=\"pincode\">
                        <span>
                                    <input type=\"text\" placeholder=\"Enter Pincode, City or State\" value=\"\" class=\"form-control\" id=\"pincode\" name=\"txtemail\" size=\"55\">
                               
                                 <button class=\"check\" type=\"submit\" name=\"submit\">Check</button></span>
                             </li>
                             <li class=\"search-bar\">
                               
                                    <select class=\"form-control option\">
                         <option value=\"\" disabled selected>Select State</option>
                      <option value=\"Andhra Pradesh\">Andhra Pradesh</option>
<option value=\"Andaman and Nicobar Islands\">Andaman and Nicobar Islands</option>
<option value=\"Arunachal Pradesh\">Arunachal Pradesh</option>
<option value=\"Assam\">Assam</option>
<option value=\"Bihar\">Bihar</option>
<option value=\"Chandigarh\">Chandigarh</option>
<option value=\"Chhattisgarh\">Chhattisgarh</option>
<option value=\"Dadar and Nagar Haveli\">Dadar and Nagar Haveli</option>
<option value=\"Daman and Diu\">Daman and Diu</option>
<option value=\"Delhi\">Delhi</option>
<option value=\"Lakshadweep\">Lakshadweep</option>
<option value=\"Puducherry\">Puducherry</option>
<option value=\"Goa\">Goa</option>
<option value=\"Gujarat\">Gujarat</option>
<option value=\"Haryana\">Haryana</option>
<option value=\"Himachal Pradesh\">Himachal Pradesh</option>
<option value=\"Jammu and Kashmir\">Jammu and Kashmir</option>
<option value=\"Jharkhand\">Jharkhand</option>
<option value=\"Karnataka\">Karnataka</option>
<option value=\"Kerala\">Kerala</option>
<option value=\"Madhya Pradesh\">Madhya Pradesh</option>
<option value=\"Maharashtra\">Maharashtra</option>
<option value=\"Manipur\">Manipur</option>
<option value=\"Meghalaya\">Meghalaya</option>
<option value=\"Mizoram\">Mizoram</option>
<option value=\"Nagaland\">Nagaland</option>
<option value=\"Odisha\">Odisha</option>
<option value=\"Punjab\">Punjab</option>
<option value=\"Rajasthan\">Rajasthan</option>
<option value=\"Sikkim\">Sikkim</option>
<option value=\"Tamil Nadu\">Tamil Nadu</option>
<option value=\"Telangana\">Telangana</option>
<option value=\"Tripura\">Tripura</option>
<option value=\"Uttar Pradesh\">Uttar Pradesh</option>
<option value=\"Uttarakhand\">Uttarakhand</option>
<option value=\"West Bengal\">West Bengal</option>
                     
                    </select>
                      </li>
                             <li>
                            
                                    <select class=\"form-control option\">
                         <option value=\"\" disabled selected>Select City</option>
                      <option value=\"Andhra Pradesh\">Chennai</option>
                                        </select>

                                 </li>
                             <li>
                                        <select class=\"form-control option\">
                         <option value=\"\" disabled selected>Select Area</option>
                      <option value=\"Andhra Pradesh\">Anna Nagar</option>
                                        </select>
                                          </li>
                             
                               
                        </li>
          </ul>
        </li>
        <li class=\"dropdown account col-lg-4 col-md-4  col-sm-4 col-xs-12\">
          <a href=\"#\" class=\"dropdown-toggle account\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">Account <span class=\"caret\"></span></a>
          <ul class=\"dropdown-menu my-account\">
            <li><a href=\"#\" class=\"order\">My Order</a></li>
                                        <li><a href=\"#\" class=\"wishlist\">Wishlist & Saved</a></li>
                                        <li><a href=\"#\" class=\"address\">Saved Address</a></li>
                                        <li><a href=\"#\" class=\"gift\">Gift Card</a></li>
                                        <li class=\"border\"><a href=\"#\" class=\"coupon\">Coupons</a></li>
                                        <hr class=\"divider\">
                                        <li><a href=\"#\" class=\"contact\">Contact Us</a></li>
                                        <li><a href=\"#\" class=\"profile\">My Profile</a></li>
                                        <li><a href=\"#\" class=\"notification\">Notification</a></li>
                                        <li><a href=\"#\" class=\"logout\">Logout</a></li>
          </ul>
        </li>
    </div>
                         \t\t
                         \t
                         \t
                         \t</div>
                         \t
                         \t
                         </div>

        \t\t\t\t
\t\t\t\t\t\t<!--";
        // line 253
        if ((isset($context["logged"]) ? $context["logged"] : null)) {
            echo " 
\t\t\t\t\t\t\t <li><a href=\"";
            // line 254
            echo (isset($context["logout"]) ? $context["logout"] : null);
            echo "\">";
            echo (isset($context["text_logout"]) ? $context["text_logout"] : null);
            echo "</a></li>\t\t\t\t\t\t\t
\t\t\t\t\t\t\t  ";
        } else {
            // line 256
            echo "\t\t\t\t\t\t\t<li><a href=\"";
            echo (isset($context["login"]) ? $context["login"] : null);
            echo "\">";
            echo (isset($context["text_login"]) ? $context["text_login"] : null);
            echo "</a></li>
\t\t\t\t\t\t\t<li><a href=\"";
            // line 257
            echo (isset($context["register"]) ? $context["register"] : null);
            echo "\">";
            echo (isset($context["text_register"]) ? $context["text_register"] : null);
            echo "</a></li> \t\t\t\t\t\t\t
\t\t\t\t\t\t";
        }
        // line 258
        echo "-->

\t\t\t\t\t\t<!-- WISHLIST  -->
\t\t\t\t\t\t";
        // line 261
        if ((isset($context["wishlist_status"]) ? $context["wishlist_status"] : null)) {
            // line 262
            echo "\t\t\t\t\t\t\t<li class=\"wishlist\"><a id=\"wishlist-total\" class=\"btn-link\" href=\"";
            echo (isset($context["wishlist"]) ? $context["wishlist"] : null);
            echo "\"  title=\"";
            echo (isset($context["text_wishlist"]) ? $context["text_wishlist"] : null);
            echo "\">";
            echo (isset($context["text_wishlist"]) ? $context["text_wishlist"] : null);
            echo "</a></li>
\t\t\t\t\t\t";
        }
        // line 263
        echo "\t
\t\t\t\t\t\t<!-- checkout -->
\t\t\t\t\t\t";
        // line 265
        if ($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "checkout_status"), "method")) {
            // line 266
            echo "\t\t\t\t\t\t\t<li class=\"checkout hidden-xs\"><a href=\"";
            echo (isset($context["checkout"]) ? $context["checkout"] : null);
            echo " \" class=\"btn-link\" title=\"";
            echo (isset($context["text_checkout"]) ? $context["text_checkout"] : null);
            echo " \"><span >";
            echo (isset($context["text_checkout"]) ? $context["text_checkout"] : null);
            echo " </span></a></li>
\t\t\t\t\t\t";
        }
        // line 267
        echo " \t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t</ul>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
\t<!-- HEADER CENTER -->
\t<div class=\"header-center ";
        // line 274
        echo (isset($context["hidden_headercenter"]) ? $context["hidden_headercenter"] : null);
        echo "\">
\t\t<div class=\"container\">

\t\t\t<div class=\"main-menu-w\">
\t\t\t<!-- Main menu -->\t\t\t\t
\t\t\t  ";
        // line 279
        echo (isset($context["content_menu1"]) ? $context["content_menu1"] : null);
        echo "
\t\t\t</div>
\t\t\t<div class=\"cart-search\">
\t\t\t\t

\t\t\t
\t\t\t</div>
\t\t</div>
\t</div>
\t\t<script type=\"text/javascript\">
  \$(document).ready(function () {
  var trigger = \$('.hamburger'),
      overlay = \$('.overlay'),
     isClosed = false;

    trigger.click(function () {
      hamburger_cross();      
    });

    function hamburger_cross() {

      if (isClosed == true) {          
        overlay.hide();
        trigger.removeClass('is-open');
        trigger.addClass('is-closed');
        isClosed = false;
      } else {   
        overlay.show();
        trigger.removeClass('is-closed');
        trigger.addClass('is-open');
        isClosed = true;
      }
  }
  
  \$('[data-toggle=\"offcanvas\"]').click(function () {
        \$('#wrapper').toggleClass('toggled');
  });  
});
</script>
</header>";
    }

    public function getTemplateName()
    {
        return "so-destino/template/header/header5.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  402 => 279,  394 => 274,  385 => 267,  375 => 266,  373 => 265,  369 => 263,  359 => 262,  357 => 261,  352 => 258,  345 => 257,  338 => 256,  331 => 254,  327 => 253,  223 => 152,  218 => 149,  213 => 146,  207 => 145,  205 => 144,  202 => 143,  200 => 142,  192 => 137,  156 => 104,  94 => 44,  88 => 42,  85 => 41,  83 => 40,  79 => 38,  68 => 36,  62 => 35,  59 => 34,  57 => 33,  26 => 5,  23 => 4,  21 => 3,  19 => 2,);
    }
}
/* {#=====Get variable : Config Select Block on header=====#}*/
/* {% set hidden_headercenter = soconfig.get_settings('toppanel_type') =='2'? 'hidden-compact' : '' %}*/
/* {% set hidden_headerbottom = soconfig.get_settings('toppanel_type') =='1'? 'hidden-compact' : '' %}*/
/* */
/* <header id="header" class=" variant typeheader-{{ typeheader ? typeheader : '1'}}">*/
/*    */
/* 	<!-- HEADER TOP -->*/
/* 	<div class="header-top compact-hidden custom">*/
/* 	     <!-- HEADER TOP TITLE BAR Antony-->*/
/*   <!-- <div class="header-top-title-bar">*/
/* 	<div class="row top-row">*/
/* 	<div class="col-12 col-sm-12 col-md-4 col-xs-4">*/
/* 		<i class="fa fa-pie-chart" aria-hidden="true" class="chart"></i>*/
/* */
/* 		<i class="fa fa-comment-o" aria-hidden="true" class="comment"></i>*/
/* */
/*         </div>*/
/*         <div class="col-12 col-sm-12 col-md-4 col-xs-4">*/
/*         	<p class="title-bar">New Website Layout</p>*/
/*         </div>*/
/*         <div class="col-12 col-sm-12 col-md-4 col-xs-4 button-section">*/
/*         	<i class="fa fa-question" aria-hidden="true"></i>*/
/*         	<button class="btn btn-info question" type="submit"><span>Login or Create Account </span></button>*/
/* */
/*         </div>*/
/*     </div>*/
/*     </div> -->*/
/*       <!-- HEADER TOP TITLE BAR Antony-->*/
/* 		<div class="inner">*/
/* 			<div class="row">*/
/* 				<div class="header-top-left col-lg-1 col-md-1 col-sm-1 col-xs-6">*/
/* 					<!-- LANGUAGE CURENCY -->*/
/* 				<!--	{% if soconfig.get_settings('lang_status') %}*/
/* 					<ul class="top-link list-inline lang-curr">*/
/* 						{% if currency %}<li class="currency"> {{ currency }}  </li> {% endif %}*/
/* 						{% if language %} <li class="language">{{ language }} </li>	{% endif %}			*/
/* 					</ul>				*/
/* 					{% endif %} */
/* 					*/
/* 					{% if soconfig.get_settings('phone_status') and soconfig.get_settings('contact_number') %}*/
/* 					<div class="telephone hidden-xs hidden-sm hidden-md" >*/
/* 						{{ soconfig.decode_entities( soconfig.get_settings('contact_number') ) }}*/
/* 					</div>*/
/* 					{% endif %}-->*/
/* 					<div id="wrapper">*/
/*         <div class="overlay"></div>*/
/*     */
/*         <!-- Sidebar -->*/
/*         <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">*/
/*             <ul class="nav sidebar-nav">*/
/*                 <li class="sidebar-brand">*/
/*                     <a href="#">*/
/*                        Sign In & Sign Up*/
/*                     </a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="#">Home</a>*/
/*                 </li>*/
/*                 <li>Categories</li>*/
/*                 <li class="dropdown">*/
/*                   <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mobiles & Tablets<span class="caret"></span></a>*/
/*                   <ul class="dropdown-menu" role="menu">*/
/*                     <li><a href="#">Smartphones</a></li>*/
/*                     <li><a href="#">Wearable Tech</a></li>*/
/*                     <li><a href="#">Mobile, Tablet Accessories</a></li>*/
/*                     <li><a href="#">Headphones & Headsets</a></li>*/
/*                     <li><a href="#">Tablets & eReaders</a></li>*/
/*                     <li><a href="#">Power Banks</a></li>*/
/*                     <li><a href="#">Mobile Cases & Protectors</a></li>*/
/*                   </ul>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="#">Computers</a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="#">Cameras</a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="#">Television & Audio</a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="#">Accessories</a>*/
/*                 </li>*/
/*             </ul>*/
/*         </nav>*/
/*         <!-- /#sidebar-wrapper -->*/
/*         <!-- Page Content -->*/
/*         <div id="page-content-wrapper">*/
/*             <button type="button" class="hamburger is-closed" data-toggle="offcanvas">*/
/*                 <span class="hamb-top"></span>*/
/*           <span class="hamb-middle"></span>*/
/*         <span class="hamb-bottom"></span>*/
/*             </button>*/
/*         </div>*/
/*         <!-- /#page-content-wrapper -->*/
/* */
/* */
/*     </div>*/
/*     <!-- /#wrapper -->*/
/* 					*/
/* 				</div>*/
/* 				<div class="navbar-logo col-lg-2 col-md-2  col-sm-4 col-xs-12">*/
/* 					<div class="logo">*/
/* 				   		{{soconfig.get_logo()}} */
/* 				   	<!--<a href="http://poorvikabeta.webindia.com/index.php?route=common/home">*/
/* 				   	    <img class="lazyautosizes lazyloaded" data-sizes="auto" src="https://s1.poorvikamobile.com/image/data/poorvika_mobile_five/poorvika-4-logo.png" data-src="https://s1.poorvikamobile.com/image/data/poorvika_mobile_five/poorvika-4-logo.png" title="Poorvika" alt="Poorvika" sizes="194px">*/
/* 				   	    </a>-->*/
/* 				   	</div>*/
/* 				</div>*/
/* 				<div class="col-lg-4 col-md-4  col-sm-3 col-xs-12 cata-parent">*/
/* 				   */
/* 					<div class="search-header-w cata-child">*/
/* 					    <div class="choose-cata">*/
/* 					<select class="form-control option cata-child-opn">*/
/* 					  <option value="" disabled selected>All Categories</option>*/
/* 					  <option>Mobiles</option>*/
/* 					  <option>Mobile Accessories</option>*/
/* 					  <option>Laptop and Tablets</option>*/
/* 					  <option>Computer Accessories</option>*/
/* 					  <option>Television</option>*/
/* 					  <option>Other Home Entertainment</option>*/
/* 					  <option>TV Accessories</option>*/
/* 					  <option>Audio</option>*/
/* 					  <option>Voice Assistence Devices</option>*/
/* 					  <option>Cameras</option>*/
/* 					  <option>Smart Waches</option>*/
/* 					  <option>Smart Home Devices</option>*/
/* 					  <option>Deals</option>*/
/* 					  <option>Discounts</option>*/
/* 					  <option>Combo Offers</option>*/
/* 					  <option>Buy Back</option>*/
/* 					  <option>Under Rs.5000 to Rs.10000</option>*/
/* 					  <option>Under Rs.10001 to Rs.20000</option>*/
/* 					  <option>Under Rs.20000 to Rs.35000</option>*/
/* 					</select>*/
/* 					</div>	*/
/* 					{{ search_block }}*/
/* 				    </div>*/
/* 				</div>*/
/* 				<div class="header-top-right collapsed-block col-lg-5 col-md-5  col-sm-4 col-xs-6 ">*/
/* 					<ul class="top-link list-inline">*/
/* 						{% if soconfig.get_settings('welcome_message_status') %}*/
/* 							<li class="hidden-sm hidden-xs welcome-msg">*/
/* 								{% if soconfig.get_settings('welcome_message') is not empty %}*/
/* 									{{ soconfig.decode_entities( soconfig.get_settings('welcome_message') ) }}*/
/* 								{% endif %} */
/* 							</li>*/
/* 						{% endif %}*/
/* 						*/
/* 						<li class="cart-bag col-lg-4 col-md-4  col-sm-4 col-xs-12">*/
/*     						<div class="shopping_cart">							*/
/*             				 	{{ cart }}*/
/*             				</div>*/
/*         				</li>*/
/*         				<!-- <li><a class="stores">Stores</a></li> -->*/
/*                         <li class="dropdown locate col-lg-4 col-md-4  col-sm-4 col-xs-12">*/
/*           <a href="#" class="dropdown-toggle stores" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Locate <br><h5>Stores</h5><span class="caret"></span></a>*/
/*           <ul class="dropdown-menu locator">*/
/*            */
/*             <li class="pincode">*/
/*                         <span>*/
/*                                     <input type="text" placeholder="Enter Pincode, City or State" value="" class="form-control" id="pincode" name="txtemail" size="55">*/
/*                                */
/*                                  <button class="check" type="submit" name="submit">Check</button></span>*/
/*                              </li>*/
/*                              <li class="search-bar">*/
/*                                */
/*                                     <select class="form-control option">*/
/*                          <option value="" disabled selected>Select State</option>*/
/*                       <option value="Andhra Pradesh">Andhra Pradesh</option>*/
/* <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>*/
/* <option value="Arunachal Pradesh">Arunachal Pradesh</option>*/
/* <option value="Assam">Assam</option>*/
/* <option value="Bihar">Bihar</option>*/
/* <option value="Chandigarh">Chandigarh</option>*/
/* <option value="Chhattisgarh">Chhattisgarh</option>*/
/* <option value="Dadar and Nagar Haveli">Dadar and Nagar Haveli</option>*/
/* <option value="Daman and Diu">Daman and Diu</option>*/
/* <option value="Delhi">Delhi</option>*/
/* <option value="Lakshadweep">Lakshadweep</option>*/
/* <option value="Puducherry">Puducherry</option>*/
/* <option value="Goa">Goa</option>*/
/* <option value="Gujarat">Gujarat</option>*/
/* <option value="Haryana">Haryana</option>*/
/* <option value="Himachal Pradesh">Himachal Pradesh</option>*/
/* <option value="Jammu and Kashmir">Jammu and Kashmir</option>*/
/* <option value="Jharkhand">Jharkhand</option>*/
/* <option value="Karnataka">Karnataka</option>*/
/* <option value="Kerala">Kerala</option>*/
/* <option value="Madhya Pradesh">Madhya Pradesh</option>*/
/* <option value="Maharashtra">Maharashtra</option>*/
/* <option value="Manipur">Manipur</option>*/
/* <option value="Meghalaya">Meghalaya</option>*/
/* <option value="Mizoram">Mizoram</option>*/
/* <option value="Nagaland">Nagaland</option>*/
/* <option value="Odisha">Odisha</option>*/
/* <option value="Punjab">Punjab</option>*/
/* <option value="Rajasthan">Rajasthan</option>*/
/* <option value="Sikkim">Sikkim</option>*/
/* <option value="Tamil Nadu">Tamil Nadu</option>*/
/* <option value="Telangana">Telangana</option>*/
/* <option value="Tripura">Tripura</option>*/
/* <option value="Uttar Pradesh">Uttar Pradesh</option>*/
/* <option value="Uttarakhand">Uttarakhand</option>*/
/* <option value="West Bengal">West Bengal</option>*/
/*                      */
/*                     </select>*/
/*                       </li>*/
/*                              <li>*/
/*                             */
/*                                     <select class="form-control option">*/
/*                          <option value="" disabled selected>Select City</option>*/
/*                       <option value="Andhra Pradesh">Chennai</option>*/
/*                                         </select>*/
/* */
/*                                  </li>*/
/*                              <li>*/
/*                                         <select class="form-control option">*/
/*                          <option value="" disabled selected>Select Area</option>*/
/*                       <option value="Andhra Pradesh">Anna Nagar</option>*/
/*                                         </select>*/
/*                                           </li>*/
/*                              */
/*                                */
/*                         </li>*/
/*           </ul>*/
/*         </li>*/
/*         <li class="dropdown account col-lg-4 col-md-4  col-sm-4 col-xs-12">*/
/*           <a href="#" class="dropdown-toggle account" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Account <span class="caret"></span></a>*/
/*           <ul class="dropdown-menu my-account">*/
/*             <li><a href="#" class="order">My Order</a></li>*/
/*                                         <li><a href="#" class="wishlist">Wishlist & Saved</a></li>*/
/*                                         <li><a href="#" class="address">Saved Address</a></li>*/
/*                                         <li><a href="#" class="gift">Gift Card</a></li>*/
/*                                         <li class="border"><a href="#" class="coupon">Coupons</a></li>*/
/*                                         <hr class="divider">*/
/*                                         <li><a href="#" class="contact">Contact Us</a></li>*/
/*                                         <li><a href="#" class="profile">My Profile</a></li>*/
/*                                         <li><a href="#" class="notification">Notification</a></li>*/
/*                                         <li><a href="#" class="logout">Logout</a></li>*/
/*           </ul>*/
/*         </li>*/
/*     </div>*/
/*                          		*/
/*                          	*/
/*                          	*/
/*                          	</div>*/
/*                          	*/
/*                          	*/
/*                          </div>*/
/* */
/*         				*/
/* 						<!--{% if logged %} */
/* 							 <li><a href="{{ logout }}">{{ text_logout }}</a></li>							*/
/* 							  {% else %}*/
/* 							<li><a href="{{ login }}">{{ text_login }}</a></li>*/
/* 							<li><a href="{{ register }}">{{ text_register }}</a></li> 							*/
/* 						{% endif %}-->*/
/* */
/* 						<!-- WISHLIST  -->*/
/* 						{% if wishlist_status %}*/
/* 							<li class="wishlist"><a id="wishlist-total" class="btn-link" href="{{ wishlist }}"  title="{{ text_wishlist }}">{{ text_wishlist }}</a></li>*/
/* 						{% endif %}	*/
/* 						<!-- checkout -->*/
/* 						{% if soconfig.get_settings('checkout_status') %}*/
/* 							<li class="checkout hidden-xs"><a href="{{ checkout }} " class="btn-link" title="{{ text_checkout }} "><span >{{ text_checkout }} </span></a></li>*/
/* 						{% endif %} 											*/
/* 					</ul>*/
/* 				</div>*/
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/* 	<!-- HEADER CENTER -->*/
/* 	<div class="header-center {{hidden_headercenter}}">*/
/* 		<div class="container">*/
/* */
/* 			<div class="main-menu-w">*/
/* 			<!-- Main menu -->				*/
/* 			  {{ content_menu1 }}*/
/* 			</div>*/
/* 			<div class="cart-search">*/
/* 				*/
/* */
/* 			*/
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/* 		<script type="text/javascript">*/
/*   $(document).ready(function () {*/
/*   var trigger = $('.hamburger'),*/
/*       overlay = $('.overlay'),*/
/*      isClosed = false;*/
/* */
/*     trigger.click(function () {*/
/*       hamburger_cross();      */
/*     });*/
/* */
/*     function hamburger_cross() {*/
/* */
/*       if (isClosed == true) {          */
/*         overlay.hide();*/
/*         trigger.removeClass('is-open');*/
/*         trigger.addClass('is-closed');*/
/*         isClosed = false;*/
/*       } else {   */
/*         overlay.show();*/
/*         trigger.removeClass('is-closed');*/
/*         trigger.addClass('is-open');*/
/*         isClosed = true;*/
/*       }*/
/*   }*/
/*   */
/*   $('[data-toggle="offcanvas"]').click(function () {*/
/*         $('#wrapper').toggleClass('toggled');*/
/*   });  */
/* });*/
/* </script>*/
/* </header>*/
