<?php

/* extension/module/rest_api.twig */
class __TwigTemplate_f48a46e20b4795d81d7848e48b97205dafc583ef455c22096ca93bcdec4f17bb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "
<div id=\"content\">
\t<div class=\"page-header\">
\t\t<div class=\"container-fluid\">
\t\t\t<div class=\"pull-right\"><a href=\"";
        // line 5
        echo (isset($context["add"]) ? $context["add"] : null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo (isset($context["button_add"]) ? $context["button_add"] : null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-plus\"></i></a>
\t\t\t\t<button type=\"button\" data-toggle=\"tooltip\" title=\"";
        // line 6
        echo (isset($context["button_delete"]) ? $context["button_delete"] : null);
        echo "\" class=\"btn btn-danger\"
\t\t\t\t\t\tonclick=\"confirm('";
        // line 7
        echo (isset($context["text_confirm"]) ? $context["text_confirm"] : null);
        echo "') ? \$('#form-user').submit() : false;\"><i class=\"fa fa-trash-o\"></i></button>
\t\t\t</div>
\t\t\t<h1>";
        // line 9
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
\t\t\t<ul class=\"breadcrumb\">
\t\t\t\t";
        // line 11
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 12
            echo "\t\t\t\t<li><a href=\"";
            echo $this->getAttribute($context["breadcrumb"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array());
            echo "</a></li>
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 14
        echo "\t\t\t</ul>
\t\t</div>
\t</div>
\t<div class=\"container-fluid\">
\t\t";
        // line 18
        if ((isset($context["error_warning"]) ? $context["error_warning"] : null)) {
            // line 19
            echo "\t\t<div class=\"alert alert-danger\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo (isset($context["error_warning"]) ? $context["error_warning"] : null);
            echo "
\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
\t\t</div>
\t\t";
        }
        // line 23
        echo "\t\t";
        if ((isset($context["success"]) ? $context["success"] : null)) {
            // line 24
            echo "\t\t<div class=\"alert alert-success\"><i class=\"fa fa-check-circle\"></i> ";
            echo (isset($context["success"]) ? $context["success"] : null);
            echo "
\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
\t\t</div>
\t\t";
        }
        // line 28
        echo "\t\t<div class=\"panel panel-default\">
\t\t\t<div class=\"panel-heading\">
\t\t\t\t<h3 class=\"panel-title\"><i class=\"fa fa-list\"></i> ";
        // line 30
        echo (isset($context["text_list"]) ? $context["text_list"] : null);
        echo "</h3>
\t\t\t</div>
\t\t\t<div class=\"panel-body\">
\t\t\t\t<form action=\"";
        // line 33
        echo (isset($context["delete"]) ? $context["delete"] : null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-user\">
\t\t\t\t\t<div class=\"table-responsive\">
\t\t\t\t\t\t<table class=\"table table-bordered table-hover\">
\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<td style=\"width: 1px;\" class=\"text-center\"><input type=\"checkbox\" onclick=\"\$('input[name*=\\'selected\\').prop('checked', this.checked);\"/></td>
\t\t\t\t\t\t\t\t<td class=\"text-left\">";
        // line 39
        if (((isset($context["sort"]) ? $context["sort"] : null) == "username")) {
            // line 40
            echo "\t\t\t\t\t\t\t\t\t<a href=\"";
            echo (isset($context["sort_username"]) ? $context["sort_username"] : null);
            echo "\" class=\"";
            echo (isset($context["order"]) ? $context["order"] : null);
            echo "\">";
            echo (isset($context["column_title"]) ? $context["column_title"] : null);
            echo "</a>
\t\t\t\t\t\t\t\t\t";
        } else {
            // line 42
            echo "\t\t\t\t\t\t\t\t\t<a href=\"";
            echo (isset($context["sort_username"]) ? $context["sort_username"] : null);
            echo "\">";
            echo (isset($context["column_title"]) ? $context["column_title"] : null);
            echo "</a>
\t\t\t\t\t\t\t\t\t";
        }
        // line 43
        echo "</td>
\t\t\t\t\t\t\t\t<td class=\"text-left\">";
        // line 44
        if (((isset($context["sort"]) ? $context["sort"] : null) == "status")) {
            // line 45
            echo "\t\t\t\t\t\t\t\t\t<a href=\"";
            echo (isset($context["sort_status"]) ? $context["sort_status"] : null);
            echo "\" class=\"";
            echo (isset($context["order"]) ? $context["order"] : null);
            echo "\">";
            echo (isset($context["column_public"]) ? $context["column_public"] : null);
            echo "</a>
\t\t\t\t\t\t\t\t\t";
        } else {
            // line 47
            echo "\t\t\t\t\t\t\t\t\t<a href=\"";
            echo (isset($context["sort_status"]) ? $context["sort_status"] : null);
            echo "\">";
            echo (isset($context["column_public"]) ? $context["column_public"] : null);
            echo "</a>
\t\t\t\t\t\t\t\t\t";
        }
        // line 48
        echo "</td>
\t\t\t\t\t\t\t\t<td class=\"text-left\">";
        // line 49
        if (((isset($context["sort"]) ? $context["sort"] : null) == "created_at")) {
            // line 50
            echo "\t\t\t\t\t\t\t\t\t<a href=\"";
            echo (isset($context["sort_date_added"]) ? $context["sort_date_added"] : null);
            echo "\" class=\"";
            echo (isset($context["order"]) ? $context["order"] : null);
            echo "\">";
            echo (isset($context["column_date_added"]) ? $context["column_date_added"] : null);
            echo "</a>
\t\t\t\t\t\t\t\t\t";
        } else {
            // line 52
            echo "\t\t\t\t\t\t\t\t\t<a href=\"";
            echo (isset($context["sort_date_added"]) ? $context["sort_date_added"] : null);
            echo "\">";
            echo (isset($context["column_date_added"]) ? $context["column_date_added"] : null);
            echo "</a>
\t\t\t\t\t\t\t\t\t";
        }
        // line 53
        echo "</td>
\t\t\t\t\t\t\t\t<td class=\"text-right\">";
        // line 54
        echo (isset($context["column_action"]) ? $context["column_action"] : null);
        echo "</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t<tbody>


\t\t\t\t\t\t\t";
        // line 60
        if ((isset($context["apis"]) ? $context["apis"] : null)) {
            // line 61
            echo "\t\t\t\t\t\t\t";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["apis"]) ? $context["apis"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["api"]) {
                // line 62
                echo "\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<td class=\"text-center\">";
                // line 63
                if (twig_in_filter($this->getAttribute($context["api"], "id", array()), (isset($context["selected"]) ? $context["selected"] : null))) {
                    // line 64
                    echo "\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" name=\"selected[]\" value=\"";
                    echo $this->getAttribute($context["api"], "id", array());
                    echo "\" checked=\"checked\"/>
\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 66
                    echo "\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" name=\"selected[]\" value=\"";
                    echo $this->getAttribute($context["api"], "id", array());
                    echo "\"/>
\t\t\t\t\t\t\t\t\t";
                }
                // line 67
                echo "</td>
\t\t\t\t\t\t\t\t<td class=\"text-left\">";
                // line 68
                echo $this->getAttribute($context["api"], "title", array());
                echo "</td>
\t\t\t\t\t\t\t\t<td class=\"text-left\">";
                // line 69
                echo $this->getAttribute($context["api"], "public", array());
                echo "</td>
\t\t\t\t\t\t\t\t<td class=\"text-left\">";
                // line 70
                echo $this->getAttribute($context["api"], "date_added", array());
                echo "</td>
\t\t\t\t\t\t\t\t<td class=\"text-right\"><a href=\"";
                // line 71
                echo $this->getAttribute($context["api"], "edit", array());
                echo "\" data-toggle=\"tooltip\" title=\"";
                echo (isset($context["button_edit"]) ? $context["button_edit"] : null);
                echo "\" class=\"btn btn-primary\"><i
\t\t\t\t\t\t\t\t\t\t\t\tclass=\"fa fa-pencil\"></i></a></td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['api'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 75
            echo "\t\t\t\t\t\t\t
\t\t\t\t\t\t\t";
        } else {
            // line 77
            echo "\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<td class=\"text-center\" colspan=\"5\">";
            // line 78
            echo (isset($context["text_no_results"]) ? $context["text_no_results"] : null);
            echo "</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t";
        }
        // line 81
        echo "\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t</table>
\t\t\t\t\t</div>
\t\t\t\t</form>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-sm-6 text-left\">";
        // line 86
        echo (isset($context["pagination"]) ? $context["pagination"] : null);
        echo "</div>
\t\t\t\t\t<div class=\"col-sm-6 text-right\">";
        // line 87
        echo (isset($context["results"]) ? $context["results"] : null);
        echo "</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
</div>
";
        // line 93
        echo (isset($context["footer"]) ? $context["footer"] : null);
    }

    public function getTemplateName()
    {
        return "extension/module/rest_api.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  270 => 93,  261 => 87,  257 => 86,  250 => 81,  244 => 78,  241 => 77,  237 => 75,  225 => 71,  221 => 70,  217 => 69,  213 => 68,  210 => 67,  204 => 66,  198 => 64,  196 => 63,  193 => 62,  188 => 61,  186 => 60,  177 => 54,  174 => 53,  166 => 52,  156 => 50,  154 => 49,  151 => 48,  143 => 47,  133 => 45,  131 => 44,  128 => 43,  120 => 42,  110 => 40,  108 => 39,  99 => 33,  93 => 30,  89 => 28,  81 => 24,  78 => 23,  70 => 19,  68 => 18,  62 => 14,  51 => 12,  47 => 11,  42 => 9,  37 => 7,  33 => 6,  27 => 5,  19 => 1,);
    }
}
/* {{ header }}{{ column_left }}*/
/* <div id="content">*/
/* 	<div class="page-header">*/
/* 		<div class="container-fluid">*/
/* 			<div class="pull-right"><a href="{{ add }}" data-toggle="tooltip" title="{{ button_add }}" class="btn btn-primary"><i class="fa fa-plus"></i></a>*/
/* 				<button type="button" data-toggle="tooltip" title="{{ button_delete }}" class="btn btn-danger"*/
/* 						onclick="confirm('{{ text_confirm }}') ? $('#form-user').submit() : false;"><i class="fa fa-trash-o"></i></button>*/
/* 			</div>*/
/* 			<h1>{{ heading_title }}</h1>*/
/* 			<ul class="breadcrumb">*/
/* 				{% for breadcrumb in breadcrumbs %}*/
/* 				<li><a href="{{ breadcrumb.href }}">{{ breadcrumb.text }}</a></li>*/
/* 				{% endfor %}*/
/* 			</ul>*/
/* 		</div>*/
/* 	</div>*/
/* 	<div class="container-fluid">*/
/* 		{% if error_warning %}*/
/* 		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> {{ error_warning }}*/
/* 			<button type="button" class="close" data-dismiss="alert">&times;</button>*/
/* 		</div>*/
/* 		{% endif %}*/
/* 		{% if success %}*/
/* 		<div class="alert alert-success"><i class="fa fa-check-circle"></i> {{ success }}*/
/* 			<button type="button" class="close" data-dismiss="alert">&times;</button>*/
/* 		</div>*/
/* 		{% endif %}*/
/* 		<div class="panel panel-default">*/
/* 			<div class="panel-heading">*/
/* 				<h3 class="panel-title"><i class="fa fa-list"></i> {{ text_list }}</h3>*/
/* 			</div>*/
/* 			<div class="panel-body">*/
/* 				<form action="{{ delete }}" method="post" enctype="multipart/form-data" id="form-user">*/
/* 					<div class="table-responsive">*/
/* 						<table class="table table-bordered table-hover">*/
/* 							<thead>*/
/* 							<tr>*/
/* 								<td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\').prop('checked', this.checked);"/></td>*/
/* 								<td class="text-left">{% if sort == 'username' %}*/
/* 									<a href="{{ sort_username }}" class="{{ order }}">{{ column_title }}</a>*/
/* 									{% else %}*/
/* 									<a href="{{ sort_username }}">{{ column_title }}</a>*/
/* 									{% endif %}</td>*/
/* 								<td class="text-left">{% if sort == 'status' %}*/
/* 									<a href="{{ sort_status }}" class="{{ order }}">{{ column_public }}</a>*/
/* 									{% else %}*/
/* 									<a href="{{ sort_status }}">{{ column_public }}</a>*/
/* 									{% endif %}</td>*/
/* 								<td class="text-left">{% if sort == 'created_at' %}*/
/* 									<a href="{{ sort_date_added }}" class="{{ order }}">{{ column_date_added }}</a>*/
/* 									{% else %}*/
/* 									<a href="{{ sort_date_added }}">{{ column_date_added }}</a>*/
/* 									{% endif %}</td>*/
/* 								<td class="text-right">{{ column_action }}</td>*/
/* 							</tr>*/
/* 							</thead>*/
/* 							<tbody>*/
/* */
/* */
/* 							{% if apis %}*/
/* 							{% for api in apis %}*/
/* 							<tr>*/
/* 								<td class="text-center">{% if api.id in selected %}*/
/* 									<input type="checkbox" name="selected[]" value="{{ api.id }}" checked="checked"/>*/
/* 									{% else %}*/
/* 									<input type="checkbox" name="selected[]" value="{{ api.id }}"/>*/
/* 									{% endif %}</td>*/
/* 								<td class="text-left">{{ api.title }}</td>*/
/* 								<td class="text-left">{{ api.public }}</td>*/
/* 								<td class="text-left">{{ api.date_added }}</td>*/
/* 								<td class="text-right"><a href="{{ api.edit }}" data-toggle="tooltip" title="{{ button_edit }}" class="btn btn-primary"><i*/
/* 												class="fa fa-pencil"></i></a></td>*/
/* 							</tr>*/
/* 							{% endfor %}*/
/* 							*/
/* 							{% else %}*/
/* 							<tr>*/
/* 								<td class="text-center" colspan="5">{{ text_no_results }}</td>*/
/* 							</tr>*/
/* 							{% endif %}*/
/* 							</tbody>*/
/* 						</table>*/
/* 					</div>*/
/* 				</form>*/
/* 				<div class="row">*/
/* 					<div class="col-sm-6 text-left">{{ pagination }}</div>*/
/* 					<div class="col-sm-6 text-right">{{ results }}</div>*/
/* 				</div>*/
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/* </div>*/
/* {{ footer }}*/
