<?php

/* extension/module/rest_api_edit.twig */
class __TwigTemplate_bdabf720ec61126e40a0725483becda729ecf5343f0ab57642321b94adc91938 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "
<div id=\"content\">
   <div class=\"page-header\">
      <div class=\"container-fluid\">
         <div class=\"pull-right\">
            <button type=\"submit\" form=\"form-user\" data-toggle=\"tooltip\" title=\"";
        // line 6
        echo (isset($context["button_save"]) ? $context["button_save"] : null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i></button>
            <a href=\"";
        // line 7
        echo (isset($context["cancel"]) ? $context["cancel"] : null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo (isset($context["button_cancel"]) ? $context["button_cancel"] : null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
            <h1>";
        // line 8
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
            <ul class=\"breadcrumb\">
               ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 11
            echo "                  <li><a href=\"";
            echo $this->getAttribute($context["breadcrumb"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array());
            echo "</a></li>
               ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "            </ul>
         </div>
      </div>
      <div class=\"container-fluid\">
         ";
        // line 17
        if ((isset($context["error_warning"]) ? $context["error_warning"] : null)) {
            // line 18
            echo "         <div class=\"alert alert-danger\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo (isset($context["error_warning"]) ? $context["error_warning"] : null);
            echo "
            <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
         </div>
         ";
        }
        // line 22
        echo "         <div class=\"panel panel-default\">
            <div class=\"panel-heading\">
               <h3 class=\"panel-title\"><i class=\"fa fa-pencil\"></i> ";
        // line 24
        echo (isset($context["text_form"]) ? $context["text_form"] : null);
        echo "</h3>
            </div>
            <div class=\"panel-body\">
               <form action=\"";
        // line 27
        echo (isset($context["action"]) ? $context["action"] : null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-user\" class=\"form-horizontal\">
                  <div class=\"form-group required\">
                     <label class=\"col-sm-2 control-label\" for=\"input-username\">";
        // line 29
        echo (isset($context["entry_title"]) ? $context["entry_title"] : null);
        echo "</label>
                     <div class=\"col-sm-10\">
                        <input type=\"text\" name=\"title\" value=\"";
        // line 31
        echo (isset($context["title"]) ? $context["title"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_title"]) ? $context["entry_title"] : null);
        echo "\" id=\"input-username\" class=\"form-control\" />
                        ";
        // line 32
        if ((isset($context["error_title"]) ? $context["error_title"] : null)) {
            // line 33
            echo "                        <div class=\"text-danger\">";
            echo (isset($context["error_title"]) ? $context["error_title"] : null);
            echo "</div>
                        ";
        }
        // line 35
        echo "                     </div>
                  </div>
                  <div class=\"form-group required\">
                     <label class=\"col-sm-2 control-label\" for=\"input-firstname\">";
        // line 38
        echo (isset($context["entry_public"]) ? $context["entry_public"] : null);
        echo "</label>
                     <div class=\"col-sm-10\">
                        <input type=\"text\" name=\"public\" value=\"";
        // line 40
        echo (isset($context["public"]) ? $context["public"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_public"]) ? $context["entry_public"] : null);
        echo "\" id=\"input-firstname\" class=\"form-control\" />
                        ";
        // line 41
        if ((isset($context["error_public"]) ? $context["error_public"] : null)) {
            // line 42
            echo "                        <div class=\"text-danger\">";
            echo (isset($context["error_public"]) ? $context["error_public"] : null);
            echo "</div>
                        ";
        }
        // line 44
        echo "                     </div>
                  </div>
                  <div class=\"form-group required\">
                     <label class=\"col-sm-2 control-label\" for=\"input-lastname\">";
        // line 47
        echo (isset($context["entry_private"]) ? $context["entry_private"] : null);
        echo "</label>
                     <div class=\"col-sm-10\">
                        <input type=\"text\" name=\"private\" value=\"";
        // line 49
        echo (isset($context["private"]) ? $context["private"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_private"]) ? $context["entry_private"] : null);
        echo "\" id=\"input-lastname\" class=\"form-control\" />
                        ";
        // line 50
        if ((isset($context["error_private"]) ? $context["error_private"] : null)) {
            // line 51
            echo "                        <div class=\"text-danger\">";
            echo (isset($context["error_private"]) ? $context["error_private"] : null);
            echo "</div>
                        ";
        }
        // line 53
        echo "                        ";
        if ((isset($context["id"]) ? $context["id"] : null)) {
            // line 54
            echo "                        <input type=\"hidden\" name=\"id\" value=\"";
            echo (isset($context["id"]) ? $context["id"] : null);
            echo "\">
                        ";
        }
        // line 56
        echo "                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
";
        // line 63
        echo (isset($context["footer"]) ? $context["footer"] : null);
    }

    public function getTemplateName()
    {
        return "extension/module/rest_api_edit.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  174 => 63,  165 => 56,  159 => 54,  156 => 53,  150 => 51,  148 => 50,  142 => 49,  137 => 47,  132 => 44,  126 => 42,  124 => 41,  118 => 40,  113 => 38,  108 => 35,  102 => 33,  100 => 32,  94 => 31,  89 => 29,  84 => 27,  78 => 24,  74 => 22,  66 => 18,  64 => 17,  58 => 13,  47 => 11,  43 => 10,  38 => 8,  32 => 7,  28 => 6,  19 => 1,);
    }
}
/* {{ header }}{{ column_left }}*/
/* <div id="content">*/
/*    <div class="page-header">*/
/*       <div class="container-fluid">*/
/*          <div class="pull-right">*/
/*             <button type="submit" form="form-user" data-toggle="tooltip" title="{{ button_save }}" class="btn btn-primary"><i class="fa fa-save"></i></button>*/
/*             <a href="{{ cancel }}" data-toggle="tooltip" title="{{ button_cancel }}" class="btn btn-default"><i class="fa fa-reply"></i></a></div>*/
/*             <h1>{{ heading_title }}</h1>*/
/*             <ul class="breadcrumb">*/
/*                {% for breadcrumb in breadcrumbs %}*/
/*                   <li><a href="{{ breadcrumb.href }}">{{ breadcrumb.text }}</a></li>*/
/*                {% endfor %}*/
/*             </ul>*/
/*          </div>*/
/*       </div>*/
/*       <div class="container-fluid">*/
/*          {% if error_warning %}*/
/*          <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> {{ error_warning }}*/
/*             <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*          </div>*/
/*          {% endif %}*/
/*          <div class="panel panel-default">*/
/*             <div class="panel-heading">*/
/*                <h3 class="panel-title"><i class="fa fa-pencil"></i> {{ text_form }}</h3>*/
/*             </div>*/
/*             <div class="panel-body">*/
/*                <form action="{{ action }}" method="post" enctype="multipart/form-data" id="form-user" class="form-horizontal">*/
/*                   <div class="form-group required">*/
/*                      <label class="col-sm-2 control-label" for="input-username">{{ entry_title }}</label>*/
/*                      <div class="col-sm-10">*/
/*                         <input type="text" name="title" value="{{ title }}" placeholder="{{ entry_title }}" id="input-username" class="form-control" />*/
/*                         {% if error_title %}*/
/*                         <div class="text-danger">{{ error_title }}</div>*/
/*                         {% endif %}*/
/*                      </div>*/
/*                   </div>*/
/*                   <div class="form-group required">*/
/*                      <label class="col-sm-2 control-label" for="input-firstname">{{ entry_public }}</label>*/
/*                      <div class="col-sm-10">*/
/*                         <input type="text" name="public" value="{{ public }}" placeholder="{{ entry_public }}" id="input-firstname" class="form-control" />*/
/*                         {% if error_public %}*/
/*                         <div class="text-danger">{{ error_public }}</div>*/
/*                         {% endif %}*/
/*                      </div>*/
/*                   </div>*/
/*                   <div class="form-group required">*/
/*                      <label class="col-sm-2 control-label" for="input-lastname">{{ entry_private }}</label>*/
/*                      <div class="col-sm-10">*/
/*                         <input type="text" name="private" value="{{ private }}" placeholder="{{ entry_private }}" id="input-lastname" class="form-control" />*/
/*                         {% if error_private %}*/
/*                         <div class="text-danger">{{ error_private }}</div>*/
/*                         {% endif %}*/
/*                         {% if id %}*/
/*                         <input type="hidden" name="id" value="{{ id }}">*/
/*                         {% endif %}*/
/*                      </div>*/
/*                   </div>*/
/*                </form>*/
/*             </div>*/
/*          </div>*/
/*       </div>*/
/*    </div>*/
/* {{ footer }}*/
