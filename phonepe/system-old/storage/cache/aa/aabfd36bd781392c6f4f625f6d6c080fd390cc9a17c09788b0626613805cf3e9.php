<?php

/* extension/module/whatsapp.twig */
class __TwigTemplate_e2d84eb27a900931e8883e5799ce70545f745bfc68d50f8740d702ff272bed56 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\">
        <button type=\"submit\" form=\"form-whtsapp\" data-toggle=\"tooltip\" title=\"";
        // line 6
        echo (isset($context["button_save"]) ? $context["button_save"] : null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i></button>
        <a href=\"";
        // line 7
        echo (isset($context["cancel"]) ? $context["cancel"] : null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo (isset($context["button_cancel"]) ? $context["button_cancel"] : null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
      <h1>";
        // line 8
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
      <ul class=\"breadcrumb\">
       ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 11
            echo "        <li><a href=\"";
            echo $this->getAttribute($context["breadcrumb"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array());
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "      </ul>
    </div>
  </div>
  <div class=\"container-fluid\">
    ";
        // line 17
        if ((isset($context["error_warning"]) ? $context["error_warning"] : null)) {
            // line 18
            echo "    <div class=\"alert alert-danger\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo (isset($context["error_warning"]) ? $context["error_warning"] : null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
     ";
        }
        // line 22
        echo "    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <h3 class=\"panel-title\"><i class=\"fa fa-pencil\"></i> ";
        // line 24
        echo (isset($context["text_edit"]) ? $context["text_edit"] : null);
        echo "</h3>
      </div>
      <div class=\"panel-body\">
        <form action=\"";
        // line 27
        echo (isset($context["action"]) ? $context["action"] : null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-whatsapp\" class=\"form-horizontal\">
\t\t<div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-name\">";
        // line 29
        echo (isset($context["entry_name"]) ? $context["entry_name"] : null);
        echo "</label>
            <div class=\"col-sm-10\">
              <input type=\"text\" name=\"name\" value=\"";
        // line 31
        echo (isset($context["name"]) ? $context["name"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_name"]) ? $context["entry_name"] : null);
        echo "\" id=\"input-name\" class=\"form-control\" />
              ";
        // line 32
        if ((isset($context["error_name"]) ? $context["error_name"] : null)) {
            // line 33
            echo "              <div class=\"text-danger\">";
            echo (isset($context["error_name"]) ? $context["error_name"] : null);
            echo "</div>
              ";
        }
        // line 35
        echo "            </div>
          </div>
\t\t <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\">";
        // line 38
        echo (isset($context["entry_share"]) ? $context["entry_share"] : null);
        echo " </label>
            <div class=\"col-sm-10\">
              <label class=\"radio-inline\">
                ";
        // line 41
        if ((isset($context["whatsapp_share"]) ? $context["whatsapp_share"] : null)) {
            // line 42
            echo "                <input type=\"radio\" name=\"whatsapp_share\" value=\"1\" checked=\"checked\" />
                ";
            // line 43
            echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
            echo "
                ";
        } else {
            // line 45
            echo "                <input type=\"radio\" name=\"whatsapp_share\" value=\"1\" />
                ";
            // line 46
            echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
            echo "
               ";
        }
        // line 48
        echo "              </label>
              <label class=\"radio-inline\">
\t\t\t   ";
        // line 50
        if ( !(isset($context["whatsapp_share"]) ? $context["whatsapp_share"] : null)) {
            // line 51
            echo "               
                <input type=\"radio\" name=\"whatsapp_share\" value=\"0\" checked=\"checked\" />
                 ";
            // line 53
            echo (isset($context["text_no"]) ? $context["text_no"] : null);
            echo "
               ";
        } else {
            // line 55
            echo "                <input type=\"radio\" name=\"whatsapp_share\" value=\"0\" />
                ";
            // line 56
            echo (isset($context["text_no"]) ? $context["text_no"] : null);
            echo "
                ";
        }
        // line 58
        echo "              </label>
            </div>
          </div>
\t\t<div class=\"form-group\">
            <label class=\"col-sm-2 control-label\"> ";
        // line 62
        echo (isset($context["entry_Chat"]) ? $context["entry_Chat"] : null);
        echo " </label>
            <div class=\"col-sm-10\">
              <label class=\"radio-inline\">
\t\t\t  ";
        // line 65
        if ((isset($context["whatsapp_chat"]) ? $context["whatsapp_chat"] : null)) {
            echo "           
                <input type=\"radio\" name=\"whatsapp_chat\" value=\"1\" checked=\"checked\" />
               ";
            // line 67
            echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
            echo "
                ";
        } else {
            // line 69
            echo "                <input type=\"radio\" name=\"whatsapp_chat\" value=\"1\" />
                ";
            // line 70
            echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
            echo "
               ";
        }
        // line 72
        echo "              </label>
              <label class=\"radio-inline\">
\t\t\t  ";
        // line 74
        if ( !(isset($context["whatsapp_chat"]) ? $context["whatsapp_chat"] : null)) {
            // line 75
            echo "            
                <input type=\"radio\" name=\"whatsapp_chat\" value=\"0\" checked=\"checked\" />
                ";
            // line 77
            echo (isset($context["text_no"]) ? $context["text_no"] : null);
            echo "
                 ";
        } else {
            // line 79
            echo "                <input type=\"radio\" name=\"whatsapp_chat\" value=\"0\" />
                ";
            // line 80
            echo (isset($context["text_no"]) ? $context["text_no"] : null);
            echo "
                ";
        }
        // line 82
        echo "              </label>
            </div>
          </div>
\t\t  <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-limit\">";
        // line 86
        echo (isset($context["entry_cphone"]) ? $context["entry_cphone"] : null);
        echo "</label>
            <div class=\"col-sm-10\">
              <input type=\"text\" name=\"whatsapp_cphone\" value=\"";
        // line 88
        echo (isset($context["whatsapp_cphone"]) ? $context["whatsapp_cphone"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_cphone"]) ? $context["entry_cphone"] : null);
        echo "\" id=\"input-limit\" class=\"form-control\" />
\t\t\t </div>
          </div>
         <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-text\">";
        // line 92
        echo (isset($context["entry_chatmsg"]) ? $context["entry_chatmsg"] : null);
        echo "</label>
            <div class=\"col-sm-10\">
              <input type=\"text\" name=\"whatsapp_cmsg\" value=\"";
        // line 94
        echo (isset($context["whatsapp_cmsg"]) ? $context["whatsapp_cmsg"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_chatmsg"]) ? $context["entry_chatmsg"] : null);
        echo "\" id=\"input-limit\" class=\"form-control\" />
           </div>
          </div>
\t\t  <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\">";
        // line 98
        echo (isset($context["entry_order"]) ? $context["entry_order"] : null);
        echo " </label>
            <div class=\"col-sm-10\">
              <label class=\"radio-inline\">
\t\t\t  ";
        // line 101
        if ((isset($context["whatsapp_order"]) ? $context["whatsapp_order"] : null)) {
            // line 102
            echo "                
                <input type=\"radio\" name=\"whatsapp_order\" value=\"1\" checked=\"checked\" />
                ";
            // line 104
            echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
            echo "
                 ";
        } else {
            // line 106
            echo "                <input type=\"radio\" name=\"whatsapp_order\" value=\"1\" />
                ";
            // line 107
            echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
            echo "
                ";
        }
        // line 109
        echo "              </label>
              <label class=\"radio-inline\">
\t\t\t  ";
        // line 111
        if ( !(isset($context["whatsapp_order"]) ? $context["whatsapp_order"] : null)) {
            // line 112
            echo "                
                <input type=\"radio\" name=\"whatsapp_order\" value=\"0\" checked=\"checked\" />
                ";
            // line 114
            echo (isset($context["text_no"]) ? $context["text_no"] : null);
            echo " 
                 ";
        } else {
            // line 116
            echo "                <input type=\"radio\" name=\"whatsapp_order\" value=\"0\" />
               ";
            // line 117
            echo (isset($context["text_no"]) ? $context["text_no"] : null);
            echo " 
                ";
        }
        // line 119
        echo "              </label>
            </div>
          </div>
\t\t <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-limit\"> ";
        // line 123
        echo (isset($context["entry_ophone"]) ? $context["entry_ophone"] : null);
        echo "</label>
            <div class=\"col-sm-10\">
              <input type=\"text\" name=\"whatsapp_ophone\" value=\"";
        // line 125
        echo (isset($context["whatsapp_ophone"]) ? $context["whatsapp_ophone"] : null);
        echo "\" placeholder=\" ";
        echo (isset($context["entry_ophone"]) ? $context["entry_ophone"] : null);
        echo "\" id=\"input-limit\" class=\"form-control\" />
\t\t\t </div>
          </div>
         <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-text\">";
        // line 129
        echo (isset($context["entry_ordermsg"]) ? $context["entry_ordermsg"] : null);
        echo "</label>
            <div class=\"col-sm-10\">
              <input type=\"text\" name=\"whatsapp_omsg\" value=\"";
        // line 131
        echo (isset($context["whatsapp_omsg"]) ? $context["whatsapp_omsg"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_ordermsg"]) ? $context["entry_ordermsg"] : null);
        echo "\" id=\"input-limit\" class=\"form-control\" />
           </div>
          </div>
\t\t  <div class=\"form-group\">
            <label class=\"col-sm-2 control-label\" for=\"input-status\">";
        // line 135
        echo (isset($context["entry_status"]) ? $context["entry_status"] : null);
        echo "</label>
            <div class=\"col-sm-10\">
              <select name=\"status\" id=\"input-status\" class=\"form-control\">
               ";
        // line 138
        if ((isset($context["status"]) ? $context["status"] : null)) {
            // line 139
            echo "                <option value=\"1\" selected=\"selected\">";
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "</option>
                <option value=\"0\">";
            // line 140
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "</option>
                 ";
        } else {
            // line 142
            echo "                <option value=\"1\">";
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "</option>
                <option value=\"0\" selected=\"selected\">";
            // line 143
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "</option>
                ";
        }
        // line 145
        echo "              </select>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
 </div>
";
        // line 153
        echo (isset($context["footer"]) ? $context["footer"] : null);
        echo "
";
    }

    public function getTemplateName()
    {
        return "extension/module/whatsapp.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  367 => 153,  357 => 145,  352 => 143,  347 => 142,  342 => 140,  337 => 139,  335 => 138,  329 => 135,  320 => 131,  315 => 129,  306 => 125,  301 => 123,  295 => 119,  290 => 117,  287 => 116,  282 => 114,  278 => 112,  276 => 111,  272 => 109,  267 => 107,  264 => 106,  259 => 104,  255 => 102,  253 => 101,  247 => 98,  238 => 94,  233 => 92,  224 => 88,  219 => 86,  213 => 82,  208 => 80,  205 => 79,  200 => 77,  196 => 75,  194 => 74,  190 => 72,  185 => 70,  182 => 69,  177 => 67,  172 => 65,  166 => 62,  160 => 58,  155 => 56,  152 => 55,  147 => 53,  143 => 51,  141 => 50,  137 => 48,  132 => 46,  129 => 45,  124 => 43,  121 => 42,  119 => 41,  113 => 38,  108 => 35,  102 => 33,  100 => 32,  94 => 31,  89 => 29,  84 => 27,  78 => 24,  74 => 22,  66 => 18,  64 => 17,  58 => 13,  47 => 11,  43 => 10,  38 => 8,  32 => 7,  28 => 6,  19 => 1,);
    }
}
/* {{ header }}{{ column_left }}*/
/* <div id="content">*/
/*   <div class="page-header">*/
/*     <div class="container-fluid">*/
/*       <div class="pull-right">*/
/*         <button type="submit" form="form-whtsapp" data-toggle="tooltip" title="{{ button_save }}" class="btn btn-primary"><i class="fa fa-save"></i></button>*/
/*         <a href="{{ cancel }}" data-toggle="tooltip" title="{{ button_cancel }}" class="btn btn-default"><i class="fa fa-reply"></i></a></div>*/
/*       <h1>{{ heading_title }}</h1>*/
/*       <ul class="breadcrumb">*/
/*        {% for breadcrumb in breadcrumbs %}*/
/*         <li><a href="{{ breadcrumb.href }}">{{ breadcrumb.text }}</a></li>*/
/*         {% endfor %}*/
/*       </ul>*/
/*     </div>*/
/*   </div>*/
/*   <div class="container-fluid">*/
/*     {% if error_warning %}*/
/*     <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> {{ error_warning }}*/
/*       <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*     </div>*/
/*      {% endif %}*/
/*     <div class="panel panel-default">*/
/*       <div class="panel-heading">*/
/*         <h3 class="panel-title"><i class="fa fa-pencil"></i> {{ text_edit }}</h3>*/
/*       </div>*/
/*       <div class="panel-body">*/
/*         <form action="{{ action }}" method="post" enctype="multipart/form-data" id="form-whatsapp" class="form-horizontal">*/
/* 		<div class="form-group">*/
/*             <label class="col-sm-2 control-label" for="input-name">{{ entry_name }}</label>*/
/*             <div class="col-sm-10">*/
/*               <input type="text" name="name" value="{{ name }}" placeholder="{{ entry_name }}" id="input-name" class="form-control" />*/
/*               {% if error_name %}*/
/*               <div class="text-danger">{{ error_name }}</div>*/
/*               {% endif %}*/
/*             </div>*/
/*           </div>*/
/* 		 <div class="form-group">*/
/*             <label class="col-sm-2 control-label">{{ entry_share }} </label>*/
/*             <div class="col-sm-10">*/
/*               <label class="radio-inline">*/
/*                 {% if whatsapp_share %}*/
/*                 <input type="radio" name="whatsapp_share" value="1" checked="checked" />*/
/*                 {{ text_yes }}*/
/*                 {% else %}*/
/*                 <input type="radio" name="whatsapp_share" value="1" />*/
/*                 {{ text_yes }}*/
/*                {% endif %}*/
/*               </label>*/
/*               <label class="radio-inline">*/
/* 			   {% if not whatsapp_share %}*/
/*                */
/*                 <input type="radio" name="whatsapp_share" value="0" checked="checked" />*/
/*                  {{ text_no }}*/
/*                {% else %}*/
/*                 <input type="radio" name="whatsapp_share" value="0" />*/
/*                 {{ text_no }}*/
/*                 {% endif %}*/
/*               </label>*/
/*             </div>*/
/*           </div>*/
/* 		<div class="form-group">*/
/*             <label class="col-sm-2 control-label"> {{ entry_Chat }} </label>*/
/*             <div class="col-sm-10">*/
/*               <label class="radio-inline">*/
/* 			  {% if whatsapp_chat %}           */
/*                 <input type="radio" name="whatsapp_chat" value="1" checked="checked" />*/
/*                {{ text_yes }}*/
/*                 {% else %}*/
/*                 <input type="radio" name="whatsapp_chat" value="1" />*/
/*                 {{ text_yes }}*/
/*                {% endif %}*/
/*               </label>*/
/*               <label class="radio-inline">*/
/* 			  {% if not whatsapp_chat %}*/
/*             */
/*                 <input type="radio" name="whatsapp_chat" value="0" checked="checked" />*/
/*                 {{ text_no }}*/
/*                  {% else %}*/
/*                 <input type="radio" name="whatsapp_chat" value="0" />*/
/*                 {{ text_no }}*/
/*                 {% endif %}*/
/*               </label>*/
/*             </div>*/
/*           </div>*/
/* 		  <div class="form-group">*/
/*             <label class="col-sm-2 control-label" for="input-limit">{{ entry_cphone }}</label>*/
/*             <div class="col-sm-10">*/
/*               <input type="text" name="whatsapp_cphone" value="{{ whatsapp_cphone }}" placeholder="{{ entry_cphone }}" id="input-limit" class="form-control" />*/
/* 			 </div>*/
/*           </div>*/
/*          <div class="form-group">*/
/*             <label class="col-sm-2 control-label" for="input-text">{{ entry_chatmsg }}</label>*/
/*             <div class="col-sm-10">*/
/*               <input type="text" name="whatsapp_cmsg" value="{{ whatsapp_cmsg }}" placeholder="{{ entry_chatmsg }}" id="input-limit" class="form-control" />*/
/*            </div>*/
/*           </div>*/
/* 		  <div class="form-group">*/
/*             <label class="col-sm-2 control-label">{{ entry_order }} </label>*/
/*             <div class="col-sm-10">*/
/*               <label class="radio-inline">*/
/* 			  {% if whatsapp_order %}*/
/*                 */
/*                 <input type="radio" name="whatsapp_order" value="1" checked="checked" />*/
/*                 {{ text_yes }}*/
/*                  {% else %}*/
/*                 <input type="radio" name="whatsapp_order" value="1" />*/
/*                 {{ text_yes }}*/
/*                 {% endif %}*/
/*               </label>*/
/*               <label class="radio-inline">*/
/* 			  {% if not whatsapp_order %}*/
/*                 */
/*                 <input type="radio" name="whatsapp_order" value="0" checked="checked" />*/
/*                 {{ text_no }} */
/*                  {% else %}*/
/*                 <input type="radio" name="whatsapp_order" value="0" />*/
/*                {{ text_no }} */
/*                 {% endif %}*/
/*               </label>*/
/*             </div>*/
/*           </div>*/
/* 		 <div class="form-group">*/
/*             <label class="col-sm-2 control-label" for="input-limit"> {{ entry_ophone }}</label>*/
/*             <div class="col-sm-10">*/
/*               <input type="text" name="whatsapp_ophone" value="{{ whatsapp_ophone }}" placeholder=" {{ entry_ophone }}" id="input-limit" class="form-control" />*/
/* 			 </div>*/
/*           </div>*/
/*          <div class="form-group">*/
/*             <label class="col-sm-2 control-label" for="input-text">{{ entry_ordermsg }}</label>*/
/*             <div class="col-sm-10">*/
/*               <input type="text" name="whatsapp_omsg" value="{{ whatsapp_omsg }}" placeholder="{{ entry_ordermsg }}" id="input-limit" class="form-control" />*/
/*            </div>*/
/*           </div>*/
/* 		  <div class="form-group">*/
/*             <label class="col-sm-2 control-label" for="input-status">{{ entry_status }}</label>*/
/*             <div class="col-sm-10">*/
/*               <select name="status" id="input-status" class="form-control">*/
/*                {% if status %}*/
/*                 <option value="1" selected="selected">{{ text_enabled }}</option>*/
/*                 <option value="0">{{ text_disabled }}</option>*/
/*                  {% else %}*/
/*                 <option value="1">{{ text_enabled }}</option>*/
/*                 <option value="0" selected="selected">{{ text_disabled }}</option>*/
/*                 {% endif %}*/
/*               </select>*/
/*             </div>*/
/*           </div>*/
/*         </form>*/
/*       </div>*/
/*     </div>*/
/*   </div>*/
/*  </div>*/
/* {{ footer }}*/
/* */
