<?php

/* default/template/extension/payment/razorpay.twig */
class __TwigTemplate_c8f15586cf2ffdc0ef777de7024ab47e62ebb2dee5ce311e49f95ee5bb8ee179 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((isset($context["is_hosted"]) ? $context["is_hosted"] : null)) {
            // line 2
            echo "
<form action=\"";
            // line 3
            echo (isset($context["api_url"]) ? $context["api_url"] : null);
            echo "checkout/embedded\" method=\"post\" name=\"embedded_checkout_form\">
    <input type=\"hidden\" name=\"key_id\" value=\"";
            // line 4
            echo (isset($context["key_id"]) ? $context["key_id"] : null);
            echo "\">
    <input type=\"hidden\" name=\"order_id\" value=\"";
            // line 5
            echo (isset($context["razorpay_order_id"]) ? $context["razorpay_order_id"] : null);
            echo "\">
    <input type=\"hidden\" name=\"image\" value=\"";
            // line 6
            echo (isset($context["image"]) ? $context["image"] : null);
            echo "\">
    <input type=\"hidden\" name=\"name\" value=\"";
            // line 7
            echo (isset($context["name"]) ? $context["name"] : null);
            echo "\">
    <input type=\"hidden\" name=\"description\" value=\"Order # ";
            // line 8
            echo (isset($context["merchant_order_id"]) ? $context["merchant_order_id"] : null);
            echo "\">
    
    <input type=\"hidden\" name=\"prefill[name]\" value=\"";
            // line 10
            echo (isset($context["card_holder_name"]) ? $context["card_holder_name"] : null);
            echo "\"> 
    <input type=\"hidden\" name=\"prefill[contact]\" value=\"";
            // line 11
            echo (isset($context["phone"]) ? $context["phone"] : null);
            echo "\">  
    <input type=\"hidden\" name=\"prefill[email]\" value=\"";
            // line 12
            echo (isset($context["email"]) ? $context["email"] : null);
            echo "\">
    
    <input type=\"hidden\" name=\"notes[opencart_order_id]\" value=\"";
            // line 14
            echo (isset($context["merchant_order_id"]) ? $context["merchant_order_id"] : null);
            echo "\">

    <input type=\"hidden\" name=\"_[integration]\" value=\"opencart\"> 
    <input type=\"hidden\" name=\"_[integration_version]\" value=\"";
            // line 17
            echo (isset($context["version"]) ? $context["version"] : null);
            echo "\">  
    <input type=\"hidden\" name=\"_[integration_parent_version]\" value=\"";
            // line 18
            echo (isset($context["oc_version"]) ? $context["oc_version"] : null);
            echo "\">

    <input type=\"hidden\" name=\"callback_url\" value=\"";
            // line 20
            echo (isset($context["return_url"]) ? $context["return_url"] : null);
            echo "\">
    <input type=\"hidden\" name=\"cancel_url\" value=\"";
            // line 21
            echo (isset($context["cancel_url"]) ? $context["cancel_url"] : null);
            echo "\">
    <div class=\"buttons\">
      <div class=\"pull-right\">
        <input type=\"submit\" value=\"";
            // line 24
            echo (isset($context["button_confirm"]) ? $context["button_confirm"] : null);
            echo "\" class=\"btn btn-primary\" />
      </div>
    </div>
  </form>

";
        } else {
            // line 30
            echo "
  <script data-cfasync='false' type='text/javascript' src=\"https://checkout.razorpay.com/v1/checkout.js\"></script>
  <script data-cfasync='false' type='text/javascript'>
    var razorpay_options = {
      key: \"";
            // line 34
            echo (isset($context["key_id"]) ? $context["key_id"] : null);
            echo "\",
      amount: \"";
            // line 35
            echo (isset($context["total"]) ? $context["total"] : null);
            echo "\",
      name: \"";
            // line 36
            echo (isset($context["name"]) ? $context["name"] : null);
            echo "\",
      description: \"Order # ";
            // line 37
            echo (isset($context["merchant_order_id"]) ? $context["merchant_order_id"] : null);
            echo "\",
      netbanking: true,
      currency: \"";
            // line 39
            echo (isset($context["currency_code"]) ? $context["currency_code"] : null);
            echo "\",
      prefill: {
        name:\"";
            // line 41
            echo (isset($context["card_holder_name"]) ? $context["card_holder_name"] : null);
            echo "\",
        email: \"";
            // line 42
            echo (isset($context["email"]) ? $context["email"] : null);
            echo "\",
        contact: \"";
            // line 43
            echo (isset($context["phone"]) ? $context["phone"] : null);
            echo "\"
      },
      notes: {
        opencart_order_id: \"";
            // line 46
            echo (isset($context["merchant_order_id"]) ? $context["merchant_order_id"] : null);
            echo "\"
      },
      _: {
          integration: 'opencart',
          integration_version: '";
            // line 50
            echo (isset($context["version"]) ? $context["version"] : null);
            echo "',
          integration_parent_version: '";
            // line 51
            echo (isset($context["oc_version"]) ? $context["oc_version"] : null);
            echo "'
      },
      callback_url: \"";
            // line 53
            echo (isset($context["return_url"]) ? $context["return_url"] : null);
            echo "\",
      order_id: \"";
            // line 54
            echo (isset($context["razorpay_order_id"]) ? $context["razorpay_order_id"] : null);
            echo "\",
      handler: function (transaction) {
          document.getElementById('razorpay_payment_id').value = transaction.razorpay_payment_id;
          document.getElementById('razorpay_signature').value = transaction.razorpay_signature;
          document.getElementById('razorpay-form').submit();
      }
    };
    var razorpay_submit_btn, razorpay_instance;

    function razorpaySubmit(el){
      if(typeof Razorpay == 'undefined'){
        setTimeout(razorpaySubmit, 200);
        if(!razorpay_submit_btn && el){
          razorpay_submit_btn = el;
          el.disabled = true;
          el.value = 'Please wait...';  
        }
      } else {
        if(!razorpay_instance){
          razorpay_instance = new Razorpay(razorpay_options);
          if(razorpay_submit_btn){
            razorpay_submit_btn.disabled = false;
            razorpay_submit_btn.value = \"";
            // line 76
            echo (isset($context["button_confirm"]) ? $context["button_confirm"] : null);
            echo "\";
          }
        }
        razorpay_instance.open();
      }
    }

  </script>
  <form name=\"razorpay-form\" id=\"razorpay-form\" action=\"";
            // line 84
            echo (isset($context["return_url"]) ? $context["return_url"] : null);
            echo "\" method=\"POST\">
    <input type=\"hidden\" name=\"razorpay_payment_id\" id=\"razorpay_payment_id\" />
    <input type=\"hidden\" name=\"razorpay_signature\" id=\"razorpay_signature\" />
  </form>
  <div class=\"buttons\">
    <div class=\"pull-right\">
      <input type=\"submit\" onclick=\"razorpaySubmit(this);\" value=\"";
            // line 90
            echo (isset($context["button_confirm"]) ? $context["button_confirm"] : null);
            echo "\" class=\"btn btn-primary\" />
    </div>
  </div>

";
        }
    }

    public function getTemplateName()
    {
        return "default/template/extension/payment/razorpay.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  203 => 90,  194 => 84,  183 => 76,  158 => 54,  154 => 53,  149 => 51,  145 => 50,  138 => 46,  132 => 43,  128 => 42,  124 => 41,  119 => 39,  114 => 37,  110 => 36,  106 => 35,  102 => 34,  96 => 30,  87 => 24,  81 => 21,  77 => 20,  72 => 18,  68 => 17,  62 => 14,  57 => 12,  53 => 11,  49 => 10,  44 => 8,  40 => 7,  36 => 6,  32 => 5,  28 => 4,  24 => 3,  21 => 2,  19 => 1,);
    }
}
/* {% if is_hosted %}*/
/* */
/* <form action="{{ api_url }}checkout/embedded" method="post" name="embedded_checkout_form">*/
/*     <input type="hidden" name="key_id" value="{{ key_id }}">*/
/*     <input type="hidden" name="order_id" value="{{ razorpay_order_id }}">*/
/*     <input type="hidden" name="image" value="{{ image }}">*/
/*     <input type="hidden" name="name" value="{{ name }}">*/
/*     <input type="hidden" name="description" value="Order # {{ merchant_order_id }}">*/
/*     */
/*     <input type="hidden" name="prefill[name]" value="{{ card_holder_name }}"> */
/*     <input type="hidden" name="prefill[contact]" value="{{ phone }}">  */
/*     <input type="hidden" name="prefill[email]" value="{{ email }}">*/
/*     */
/*     <input type="hidden" name="notes[opencart_order_id]" value="{{ merchant_order_id }}">*/
/* */
/*     <input type="hidden" name="_[integration]" value="opencart"> */
/*     <input type="hidden" name="_[integration_version]" value="{{ version }}">  */
/*     <input type="hidden" name="_[integration_parent_version]" value="{{ oc_version }}">*/
/* */
/*     <input type="hidden" name="callback_url" value="{{ return_url }}">*/
/*     <input type="hidden" name="cancel_url" value="{{ cancel_url }}">*/
/*     <div class="buttons">*/
/*       <div class="pull-right">*/
/*         <input type="submit" value="{{ button_confirm }}" class="btn btn-primary" />*/
/*       </div>*/
/*     </div>*/
/*   </form>*/
/* */
/* {% else %}*/
/* */
/*   <script data-cfasync='false' type='text/javascript' src="https://checkout.razorpay.com/v1/checkout.js"></script>*/
/*   <script data-cfasync='false' type='text/javascript'>*/
/*     var razorpay_options = {*/
/*       key: "{{ key_id }}",*/
/*       amount: "{{ total }}",*/
/*       name: "{{ name }}",*/
/*       description: "Order # {{ merchant_order_id }}",*/
/*       netbanking: true,*/
/*       currency: "{{ currency_code }}",*/
/*       prefill: {*/
/*         name:"{{ card_holder_name }}",*/
/*         email: "{{ email }}",*/
/*         contact: "{{ phone }}"*/
/*       },*/
/*       notes: {*/
/*         opencart_order_id: "{{ merchant_order_id }}"*/
/*       },*/
/*       _: {*/
/*           integration: 'opencart',*/
/*           integration_version: '{{ version }}',*/
/*           integration_parent_version: '{{ oc_version }}'*/
/*       },*/
/*       callback_url: "{{ return_url }}",*/
/*       order_id: "{{ razorpay_order_id }}",*/
/*       handler: function (transaction) {*/
/*           document.getElementById('razorpay_payment_id').value = transaction.razorpay_payment_id;*/
/*           document.getElementById('razorpay_signature').value = transaction.razorpay_signature;*/
/*           document.getElementById('razorpay-form').submit();*/
/*       }*/
/*     };*/
/*     var razorpay_submit_btn, razorpay_instance;*/
/* */
/*     function razorpaySubmit(el){*/
/*       if(typeof Razorpay == 'undefined'){*/
/*         setTimeout(razorpaySubmit, 200);*/
/*         if(!razorpay_submit_btn && el){*/
/*           razorpay_submit_btn = el;*/
/*           el.disabled = true;*/
/*           el.value = 'Please wait...';  */
/*         }*/
/*       } else {*/
/*         if(!razorpay_instance){*/
/*           razorpay_instance = new Razorpay(razorpay_options);*/
/*           if(razorpay_submit_btn){*/
/*             razorpay_submit_btn.disabled = false;*/
/*             razorpay_submit_btn.value = "{{ button_confirm }}";*/
/*           }*/
/*         }*/
/*         razorpay_instance.open();*/
/*       }*/
/*     }*/
/* */
/*   </script>*/
/*   <form name="razorpay-form" id="razorpay-form" action="{{ return_url }}" method="POST">*/
/*     <input type="hidden" name="razorpay_payment_id" id="razorpay_payment_id" />*/
/*     <input type="hidden" name="razorpay_signature" id="razorpay_signature" />*/
/*   </form>*/
/*   <div class="buttons">*/
/*     <div class="pull-right">*/
/*       <input type="submit" onclick="razorpaySubmit(this);" value="{{ button_confirm }}" class="btn btn-primary" />*/
/*     </div>*/
/*   </div>*/
/* */
/* {% endif %}*/
/* */
