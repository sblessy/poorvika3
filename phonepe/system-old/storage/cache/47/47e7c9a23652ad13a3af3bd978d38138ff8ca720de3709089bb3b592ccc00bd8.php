<?php

/* so-mobile/template/soconfig/related_product.twig */
class __TwigTemplate_9fa2d7f2c7bfb7b6ab9a322f1d085e4c2a53dd82589d8881f805b66da0a6091d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 9
        echo "
";
        // line 11
        echo "<div class=\"clearfix module related-horizontal col-xs-12\">
\t<h3 class=\"modtitle\"><span>";
        // line 12
        echo (isset($context["text_related"]) ? $context["text_related"] : null);
        echo " </span></h3>
\t
    <div class=\"related-products products-list  contentslider\" data-rtl=\"";
        // line 14
        echo (isset($context["direction"]) ? $context["direction"] : null);
        echo "\" data-autoplay=\"no\"  data-pagination=\"no\" data-delay=\"4\" data-speed=\"0.6\" data-margin=\"10\"  data-items_column0=\"2\" data-items_column1=\"2\" data-items_column2=\"2\"
\t\t\tdata-items_column3=\"2\" data-items_column4=\"2\" data-arrows=\"yes\" data-lazyload=\"yes\" data-loop=\"no\" data-hoverpause=\"yes\">
\t\t<!-- Products list -->
\t\t";
        // line 17
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            echo " 
                <div class=\"product-layout product-grid\">
\t\t\t\t  \t<div class=\"product-item-container\">
\t\t\t\t\t\t<div class=\"left-block\">
\t\t\t\t\t\t\t<div class=\"product-image-container\">
\t\t\t\t\t\t\t\t<a href=\"";
            // line 22
            echo $this->getAttribute($context["product"], "href", array());
            echo " \" title=\"";
            echo $this->getAttribute($context["product"], "name", array());
            echo " \">
\t\t\t\t\t\t\t\t\t<img  src=\"";
            // line 23
            echo $this->getAttribute($context["product"], "thumb", array());
            echo " \"  title=\"";
            echo $this->getAttribute($context["product"], "name", array());
            echo " \" class=\"img-1 img-responsive\" />
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"box-label\">
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t";
            // line 30
            echo "\t\t\t\t\t\t\t\t";
            if ($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "discount_status"), "method")) {
                echo " 
\t\t\t\t\t\t\t\t";
                // line 31
                if (($this->getAttribute($context["product"], "price", array()) && $this->getAttribute($context["product"], "special", array()))) {
                    echo " 
\t\t\t\t\t\t\t\t\t<span class=\"label-product label-sale\">
\t\t\t\t\t\t\t\t\t\t ";
                    // line 33
                    echo $this->getAttribute($context["product"], "discount", array());
                    echo "
\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t";
                }
                // line 35
                echo " 
\t\t\t\t\t\t\t\t";
            }
            // line 36
            echo " 
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t</div>
\t\t\t\t\t\t 
\t\t\t\t\t\t<div class=\"right-block\">
\t\t\t\t\t\t\t<div class=\"caption\">
\t\t\t\t\t\t\t\t";
            // line 43
            if ($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "rating_status"), "method")) {
                echo " 
\t\t\t\t\t\t\t\t<div class=\"ratings\">
\t\t\t\t\t\t\t\t\t<div class=\"rating-box\">
\t\t\t\t\t\t\t\t\t";
                // line 46
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(range(1, 5));
                foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                    // line 47
                    echo "\t\t\t\t\t\t\t\t\t";
                    if (($this->getAttribute($context["product"], "rating", array()) < $context["i"])) {
                        echo " 
\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>
\t\t\t\t\t\t\t\t\t";
                    } else {
                        // line 49
                        echo "   
\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>
\t\t\t\t\t\t\t\t\t";
                    }
                    // line 51
                    echo " 
\t\t\t\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 53
                echo "
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
            }
            // line 56
            echo " 
\t\t\t\t\t\t\t\t<h4><a href=\"";
            // line 57
            echo $this->getAttribute($context["product"], "href", array());
            echo " \">";
            echo $this->getAttribute($context["product"], "name", array());
            echo " </a></h4>
\t\t\t\t\t\t\t\t";
            // line 58
            if ($this->getAttribute($context["product"], "price", array())) {
                echo " 
\t\t\t\t\t\t\t\t<div class=\"price\">
\t\t\t\t\t\t\t\t\t";
                // line 60
                if ( !$this->getAttribute($context["product"], "special", array())) {
                    echo " 
\t\t\t\t\t\t\t\t\t\t<span class=\"price-new\">";
                    // line 61
                    echo $this->getAttribute($context["product"], "price", array());
                    echo " </span>
\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 62
                    echo "   
\t\t\t\t\t\t\t\t\t\t<span class=\"price-new\">";
                    // line 63
                    echo $this->getAttribute($context["product"], "special", array());
                    echo " </span> <span class=\"price-old\">";
                    echo $this->getAttribute($context["product"], "price", array());
                    echo " </span>
\t\t\t\t\t\t\t\t\t";
                }
                // line 64
                echo " 
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
            }
            // line 66
            echo " 
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
                </div>
     ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 74
        echo " 
    </div>
\t
</div>

";
    }

    public function getTemplateName()
    {
        return "so-mobile/template/soconfig/related_product.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  179 => 74,  165 => 66,  160 => 64,  153 => 63,  150 => 62,  145 => 61,  141 => 60,  136 => 58,  130 => 57,  127 => 56,  121 => 53,  114 => 51,  109 => 49,  102 => 47,  98 => 46,  92 => 43,  83 => 36,  79 => 35,  73 => 33,  68 => 31,  63 => 30,  52 => 23,  46 => 22,  36 => 17,  30 => 14,  25 => 12,  22 => 11,  19 => 9,);
    }
}
/* {#*/
/* ****************************************************** */
/*  * @package	SO Framework for Opencart 3.x*/
/*  * @author	http://www.opencartworks.com*/
/*  * @license	GNU General Public License*/
/*  * @copyright(C) 2008-2017 opencartworks.com. All rights reserved.*/
/*  *******************************************************/
/* #}*/
/* */
/* {#========== Product Detail - Releate Horizontal ============#}*/
/* <div class="clearfix module related-horizontal col-xs-12">*/
/* 	<h3 class="modtitle"><span>{{ text_related }} </span></h3>*/
/* 	*/
/*     <div class="related-products products-list  contentslider" data-rtl="{{direction}}" data-autoplay="no"  data-pagination="no" data-delay="4" data-speed="0.6" data-margin="10"  data-items_column0="2" data-items_column1="2" data-items_column2="2"*/
/* 			data-items_column3="2" data-items_column4="2" data-arrows="yes" data-lazyload="yes" data-loop="no" data-hoverpause="yes">*/
/* 		<!-- Products list -->*/
/* 		{% for product in products %} */
/*                 <div class="product-layout product-grid">*/
/* 				  	<div class="product-item-container">*/
/* 						<div class="left-block">*/
/* 							<div class="product-image-container">*/
/* 								<a href="{{ product.href }} " title="{{ product.name }} ">*/
/* 									<img  src="{{ product.thumb }} "  title="{{ product.name }} " class="img-1 img-responsive" />*/
/* 								</a>*/
/* 							</div>*/
/* 							*/
/* 							<div class="box-label">*/
/* 								*/
/* 								{#=======Discount Label======= #}*/
/* 								{% if soconfig.get_settings('discount_status')  %} */
/* 								{% if product.price  and  product.special  %} */
/* 									<span class="label-product label-sale">*/
/* 										 {{ product.discount }}*/
/* 									</span>*/
/* 								{% endif %} */
/* 								{% endif %} */
/* 							</div>*/
/* */
/* 						</div>*/
/* 						 */
/* 						<div class="right-block">*/
/* 							<div class="caption">*/
/* 								{% if soconfig.get_settings('rating_status') %} */
/* 								<div class="ratings">*/
/* 									<div class="rating-box">*/
/* 									{% for i in 1..5 %}*/
/* 									{% if product.rating < i %} */
/* 										<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>*/
/* 									{% else %}   */
/* 										<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>*/
/* 									{% endif %} */
/* 									{% endfor %}*/
/* */
/* 									</div>*/
/* 								</div>*/
/* 								{% endif %} */
/* 								<h4><a href="{{ product.href }} ">{{ product.name }} </a></h4>*/
/* 								{% if product.price %} */
/* 								<div class="price">*/
/* 									{% if not product.special %} */
/* 										<span class="price-new">{{ product.price }} </span>*/
/* 									{% else %}   */
/* 										<span class="price-new">{{ product.special }} </span> <span class="price-old">{{ product.price }} </span>*/
/* 									{% endif %} */
/* 								</div>*/
/* 								{% endif %} */
/* 								*/
/* 							*/
/* 							</div>*/
/* 							*/
/* 						</div>*/
/* 					</div>*/
/*                 </div>*/
/*      {% endfor %} */
/*     </div>*/
/* 	*/
/* </div>*/
/* */
/* */
