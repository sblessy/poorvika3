<?php

/* extension/module/so_onepagecheckout.twig */
class __TwigTemplate_e319ad3956a77f655ceda8e76d6a106e2c83572b76f9851efae3f28257fdb7a8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "
<div id=\"content\">
  \t<div class=\"page-header\">
    \t<div class=\"container-fluid\">
      \t\t<div class=\"pull-right\">
        \t\t<button type=\"submit\" form=\"form-so-onepagecheckout\" data-toggle=\"tooltip\" title=\"";
        // line 6
        echo (isset($context["button_save"]) ? $context["button_save"] : null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i> ";
        echo (isset($context["button_save"]) ? $context["button_save"] : null);
        echo "</button>
        \t\t<a class=\"btn btn-success1\" onclick=\"\$('#action').val('save_edit');\$('#form-so-onepagecheckout').submit();\" data-toggle=\"tooltip\" title=\"";
        // line 7
        echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "entry_button_save_and_edit"), "method");
        echo "\" ><i class=\"fa fa-pencil-square-o\"></i> ";
        echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "entry_button_save_and_edit"), "method");
        echo "</a>
\t\t\t\t<a class=\"btn btn-info\" onclick=\"\$('#action').val('save_new');\$('#form-so-onepagecheckout').submit();\" data-toggle=\"tooltip\" title=\"";
        // line 8
        echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "entry_button_save_and_new"), "method");
        echo "\" ><i class=\"fa fa-book\"></i>  ";
        echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "entry_button_save_and_new"), "method");
        echo "</a>
        \t\t<a href=\"";
        // line 9
        echo (isset($context["cancel"]) ? $context["cancel"] : null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo (isset($context["button_cancel"]) ? $context["button_cancel"] : null);
        echo "\" class=\"btn btn-danger\"><i class=\"fa fa-reply\"></i> ";
        echo (isset($context["button_cancel"]) ? $context["button_cancel"] : null);
        echo "</a>
        \t</div>
\t\t\t<h1>";
        // line 11
        echo (isset($context["heading_title_so"]) ? $context["heading_title_so"] : null);
        echo "</h1>
      \t\t<ul class=\"breadcrumb\">
        \t\t";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 14
            echo "        \t\t\t<li><a href=\"";
            echo $this->getAttribute($context["breadcrumb"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array());
            echo "</a></li>
        \t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 16
        echo "      \t\t</ul>
    \t</div>
  \t</div>
  \t<div class=\"container-fluid\">
    \t";
        // line 20
        if ((isset($context["error_warning"]) ? $context["error_warning"] : null)) {
            // line 21
            echo "    \t\t<div class=\"alert alert-danger\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo (isset($context["error_warning"]) ? $context["error_warning"] : null);
            echo "
      \t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    \t\t</div>
    \t";
        }
        // line 25
        echo "    \t";
        if ((isset($context["success"]) ? $context["success"] : null)) {
            // line 26
            echo "    \t\t<div class=\"alert alert-success\"><i class=\"fa fa-check-circle\"></i> ";
            echo (isset($context["success"]) ? $context["success"] : null);
            echo "
      \t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    \t\t</div>
    \t";
        }
        // line 30
        echo "    \t<div class=\"panel panel-default\">
      \t\t<div class=\"panel-heading\">
        \t\t<h3 class=\"panel-title\"><i class=\"fa fa-pencil\"></i> ";
        // line 32
        echo (isset($context["text_edit"]) ? $context["text_edit"] : null);
        echo "</h3>
      \t\t</div>
      \t\t<div class=\"panel-body\">
\t   \t\t\t<form action=\"";
        // line 35
        echo (isset($context["action"]) ? $context["action"] : null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-so-onepagecheckout\" class=\"form-horizontal\">
    \t\t\t\t<input type=\"hidden\" name=\"action\" id=\"action\" value=\"\"/>        
\t\t\t\t\t<ul class=\"nav nav-tabs\" id=\"CheckoutTab\">
\t\t\t\t\t\t<li class=\"active\"><a href=\"#tab-general\" data-toggle=\"tab\"><i class=\"fa fa-cog fa-fw\"></i> ";
        // line 38
        echo (isset($context["tab_general"]) ? $context["tab_general"] : null);
        echo "</a></li>
\t\t\t\t\t\t<li class=\"dropdown\">
\t\t\t\t\t\t\t<a href=\"#\" data-toggle=\"dropdown\"><i class=\"fa fa-eye\"></i> ";
        // line 40
        echo (isset($context["tab_layout_setting"]) ? $context["tab_layout_setting"] : null);
        echo "<span class=\"caret\"></span></a>
\t\t\t\t\t\t\t<ul class=\"dropdown-menu\">
\t\t\t\t\t\t\t\t<li><a href=\"#tab-layout\" data-toggle=\"tab\"><i class=\"fa fa-eye\"></i> ";
        // line 42
        echo (isset($context["tab_account_setting"]) ? $context["tab_account_setting"] : null);
        echo "</a></li>
\t\t\t\t\t\t\t\t<li><a href=\"#tab-shopping-cart-setting\" data-toggle=\"tab\"><i class=\"fa fa-shopping-cart\"></i> ";
        // line 43
        echo (isset($context["tab_shipping_cart"]) ? $context["tab_shipping_cart"] : null);
        echo "</a></li>
\t\t\t\t\t\t\t\t<li><a href=\"#tab-delivery_method-setting\" data-toggle=\"tab\"><i class=\"fa fa-truck\" aria-hidden=\"true\"></i> ";
        // line 44
        echo (isset($context["tab_delivery_methods"]) ? $context["tab_delivery_methods"] : null);
        echo "</a></li>
\t\t\t\t\t\t\t\t<li><a href=\"#tab-payment_method-setting\" data-toggle=\"tab\"><i class=\"fa fa-credit-card\" aria-hidden=\"true\"></i> ";
        // line 45
        echo (isset($context["tab_payment_methods"]) ? $context["tab_payment_methods"] : null);
        echo "</a></li>
\t\t\t\t\t\t\t\t<li><a href=\"#tab-confirm_order-setting\" data-toggle=\"tab\"><i class=\"fa fa-cart-plus\" aria-hidden=\"true\"></i> ";
        // line 46
        echo (isset($context["tab_confirm_order"]) ? $context["tab_confirm_order"] : null);
        echo "</a></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</li>
\t\t\t\t\t\t<li class=\"\">
\t\t\t\t\t\t\t<a href=\"#tab-help\" data-toggle=\"tab\"><i class=\"fa fa-question-circle\"></i> ";
        // line 50
        echo (isset($context["tab_help"]) ? $context["tab_help"] : null);
        echo "</a>
\t\t\t\t\t\t</li>
\t\t\t\t\t</ul>
\t\t\t\t\t<div class=\"tab-content\">
\t\t\t\t\t\t<div class=\"tab-pane active in\" id=\"tab-general\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"input-status\"><span data-toggle=\"tooltip\" title=\"\" data-original-title=\"";
        // line 56
        echo (isset($context["entry_status_title"]) ? $context["entry_status_title"] : null);
        echo "\">";
        echo (isset($context["entry_status"]) ? $context["entry_status"] : null);
        echo "</span></label>
\t\t\t\t\t\t\t\t<div class=\"col-sm-3\">
\t\t\t\t\t\t\t\t\t<div class=\"btn-group\" data-toggle=\"buttons\">
\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm  ";
        // line 59
        echo ((((isset($context["so_onepagecheckout_enabled"]) ? $context["so_onepagecheckout_enabled"] : null) == 1)) ? ("active") : (""));
        echo "\"><input type=\"radio\"  name=\"so_onepagecheckout[so_onepagecheckout_general][so_onepagecheckout_enabled]\"  ";
        echo ((((isset($context["so_onepagecheckout_enabled"]) ? $context["so_onepagecheckout_enabled"] : null) == 1)) ? ("checked=\"checked\"") : (""));
        echo "  value=\"1\" autocomplete=\"off\">";
        echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm ";
        // line 60
        echo ((((isset($context["so_onepagecheckout_enabled"]) ? $context["so_onepagecheckout_enabled"] : null) == 0)) ? ("active") : (""));
        echo "\"><input type=\"radio\"  name=\"so_onepagecheckout[so_onepagecheckout_general][so_onepagecheckout_enabled]\" ";
        echo ((((isset($context["so_onepagecheckout_enabled"]) ? $context["so_onepagecheckout_enabled"] : null) == 0)) ? ("checked=\"checked\"") : (""));
        echo " value=\"0\" autocomplete=\"off\">";
        echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t  \t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"input-name\"><span data-toggle=\"tooltip\" title=\"\" data-original-title=\"";
        // line 65
        echo (isset($context["entry_name_title"]) ? $context["entry_name_title"] : null);
        echo "\">";
        echo (isset($context["entry_name"]) ? $context["entry_name"] : null);
        echo "</span></label>
\t\t\t\t\t\t\t\t<div class=\"col-sm-3\">
\t\t\t\t  \t\t\t\t\t<input type=\"text\" name=\"so_onepagecheckout[so_onepagecheckout_general][so_onepagecheckout_name]\" value=\"";
        // line 67
        echo (isset($context["so_onepagecheckout_name"]) ? $context["so_onepagecheckout_name"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_name"]) ? $context["entry_name"] : null);
        echo "\" id=\"input-name\" class=\"form-control\" />
\t\t\t\t  \t\t\t\t\t";
        // line 68
        if ((isset($context["error_name"]) ? $context["error_name"] : null)) {
            // line 69
            echo "\t\t\t\t  \t\t\t\t\t\t<div class=\"text-danger\">";
            echo (isset($context["error_name"]) ? $context["error_name"] : null);
            echo "</div>
\t\t\t\t  \t\t\t\t\t";
        }
        // line 71
        echo "\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"input-status\"><span data-toggle=\"tooltip\" title=\"\" data-original-title=\"";
        // line 74
        echo (isset($context["text_layout_title"]) ? $context["text_layout_title"] : null);
        echo "\">";
        echo (isset($context["text_layout"]) ? $context["text_layout"] : null);
        echo "</span></label>
\t\t\t\t\t\t\t\t<div class=\"col-sm-3\">
\t\t\t\t\t\t\t\t\t<select name=\"so_onepagecheckout[so_onepagecheckout_general][so_onepagecheckout_layout]\" id=\"input-country\" class=\"form-control\">
\t\t\t                \t\t\t<option value=\"1\" ";
        // line 77
        echo ((((isset($context["so_onepagecheckout_layout"]) ? $context["so_onepagecheckout_layout"] : null) == 1)) ? ("selected=\"selected\"") : (""));
        echo ">";
        echo (isset($context["text_layout_one"]) ? $context["text_layout_one"] : null);
        echo "</option>
\t\t\t                \t\t\t<option value=\"2\" ";
        // line 78
        echo ((((isset($context["so_onepagecheckout_layout"]) ? $context["so_onepagecheckout_layout"] : null) == 2)) ? ("selected=\"selected\"") : (""));
        echo ">";
        echo (isset($context["text_layout_two"]) ? $context["text_layout_two"] : null);
        echo "</option>
\t\t\t                \t\t</select>
\t\t\t\t  \t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t                \t<label class=\"col-sm-2 control-label\" for=\"input-country\"><span data-toggle=\"tooltip\" title=\"\" data-original-title=\"Set Default Country\">";
        // line 83
        echo (isset($context["text_default_country"]) ? $context["text_default_country"] : null);
        echo "</span></label>
\t\t\t                \t<div class=\"col-sm-3\">
\t\t\t                \t\t<select name=\"so_onepagecheckout[so_onepagecheckout_general][so_onepagecheckout_country_id]\" id=\"input-country\" class=\"form-control\">
\t\t\t                \t\t\t";
        // line 86
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["countries"]) ? $context["countries"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["country"]) {
            // line 87
            echo "\t\t\t                \t\t\t\t<option value=\"";
            echo $this->getAttribute($context["country"], "country_id", array());
            echo "\" ";
            echo ((($this->getAttribute($context["country"], "country_id", array()) == (isset($context["so_onepagecheckout_country_id"]) ? $context["so_onepagecheckout_country_id"] : null))) ? ("selected=\"selected\"") : (""));
            echo ">";
            echo $this->getAttribute($context["country"], "name", array());
            echo "</option>
\t\t\t                \t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['country'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 89
        echo "\t\t\t                \t\t</select>
\t\t\t                \t</div>
\t\t\t                </div>
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"input-zone\"><span data-toggle=\"tooltip\" title=\"\" data-original-title=\"Set Default zone\">";
        // line 93
        echo (isset($context["text_default_zone"]) ? $context["text_default_zone"] : null);
        echo "</span></label>
\t\t\t\t\t\t\t\t<div class=\"col-sm-3\">
\t\t\t\t\t\t\t\t\t<select name=\"so_onepagecheckout[so_onepagecheckout_general][so_onepagecheckout_zone_id]\" id=\"input-zone\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"tab-pane\" id=\"tab-layout\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"input-status\"><span  data-toggle=\"tooltip\" title=\"Disabled Register Checkout \">";
        // line 102
        echo (isset($context["text_register_account"]) ? $context["text_register_account"] : null);
        echo "</span></label>
\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t\t<div class=\"btn-group\" data-toggle=\"buttons\">
\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm button-account-type ";
        // line 105
        echo ((((isset($context["so_onepagecheckout_register_checkout"]) ? $context["so_onepagecheckout_register_checkout"] : null) == 1)) ? ("active") : (""));
        echo "\"><input type=\"radio\" name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][so_onepagecheckout_register_checkout]\" ";
        echo ((((isset($context["so_onepagecheckout_register_checkout"]) ? $context["so_onepagecheckout_register_checkout"] : null) == 1)) ? ("checked=\"checked\"") : (""));
        echo " value=\"1\" autocomplete=\"off\">";
        echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm button-account-type ";
        // line 106
        echo ((((isset($context["so_onepagecheckout_register_checkout"]) ? $context["so_onepagecheckout_register_checkout"] : null) == 0)) ? ("active") : (""));
        echo "\"><input type=\"radio\" name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][so_onepagecheckout_register_checkout]\" ";
        echo ((((isset($context["so_onepagecheckout_register_checkout"]) ? $context["so_onepagecheckout_register_checkout"] : null) == 0)) ? ("checked=\"checked\"") : (""));
        echo " value=\"0\" autocomplete=\"off\">";
        echo (isset($context["text_no"]) ? $context["text_no"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"input-status\"><span  data-toggle=\"tooltip\" title=\"Disabled Guest Checkout \">";
        // line 111
        echo (isset($context["text_guest_checkout"]) ? $context["text_guest_checkout"] : null);
        echo "</span></label>
\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t\t<div class=\"btn-group\" data-toggle=\"buttons\">
\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm ";
        // line 114
        echo ((((isset($context["so_onepagecheckout_guest_checkout"]) ? $context["so_onepagecheckout_guest_checkout"] : null) == 1)) ? ("active") : (""));
        echo "\"><input type=\"radio\"  name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][so_onepagecheckout_guest_checkout]\" ";
        echo ((((isset($context["so_onepagecheckout_guest_checkout"]) ? $context["so_onepagecheckout_guest_checkout"] : null) == 1)) ? ("checked=\"checked\"") : (""));
        echo " value=\"1\" autocomplete=\"off\">";
        echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm ";
        // line 115
        echo ((((isset($context["so_onepagecheckout_guest_checkout"]) ? $context["so_onepagecheckout_guest_checkout"] : null) == 0)) ? ("active") : (""));
        echo "\"><input type=\"radio\" name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][so_onepagecheckout_guest_checkout]\"  ";
        echo ((((isset($context["so_onepagecheckout_guest_checkout"]) ? $context["so_onepagecheckout_guest_checkout"] : null) == 0)) ? ("checked=\"checked\"") : (""));
        echo " value=\"0\" autocomplete=\"off\">";
        echo (isset($context["text_no"]) ? $context["text_no"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"input-status\"><span  data-toggle=\"tooltip\" title=\"Disabled Login \">";
        // line 120
        echo (isset($context["text_login_checkout"]) ? $context["text_login_checkout"] : null);
        echo "</span></label>
\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t\t<div class=\"btn-group\" data-toggle=\"buttons\">
\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm ";
        // line 123
        echo ((((isset($context["so_onepagecheckout_enable_login"]) ? $context["so_onepagecheckout_enable_login"] : null) == 1)) ? ("active") : (""));
        echo "\"><input type=\"radio\"  name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][so_onepagecheckout_enable_login]\" ";
        echo ((((isset($context["so_onepagecheckout_enable_login"]) ? $context["so_onepagecheckout_enable_login"] : null) == 1)) ? ("checked=\"checked\"") : (""));
        echo " value=\"1\" autocomplete=\"off\">";
        echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm ";
        // line 124
        echo ((((isset($context["so_onepagecheckout_enable_login"]) ? $context["so_onepagecheckout_enable_login"] : null) == 0)) ? ("active") : (""));
        echo "\"><input type=\"radio\"  name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][so_onepagecheckout_enable_login]\" ";
        echo ((((isset($context["so_onepagecheckout_enable_login"]) ? $context["so_onepagecheckout_enable_login"] : null) == 0)) ? ("checked=\"checked\"") : (""));
        echo " value=\"0\" autocomplete=\"off\">";
        echo (isset($context["text_no"]) ? $context["text_no"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"form-group\">\t
\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"input-account_open\"><span data-toggle=\"tooltip\" title=\"\" data-original-title=\"Default select section\">";
        // line 129
        echo (isset($context["text_default_display"]) ? $context["text_default_display"] : null);
        echo "</span></label>
\t\t\t\t\t\t\t\t<div class=\"col-sm-3\">
\t\t\t\t\t\t\t\t\t<div class=\"btn-group btn-group-justified\" data-toggle=\"buttons\">\t
\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm ";
        // line 132
        echo ((((isset($context["so_onepagecheckout_account_open"]) ? $context["so_onepagecheckout_account_open"] : null) == "register")) ? ("active") : (""));
        echo "\"><input type=\"radio\" name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][so_onepagecheckout_account_open]\" ";
        echo ((((isset($context["so_onepagecheckout_account_open"]) ? $context["so_onepagecheckout_account_open"] : null) == "register")) ? ("checked=\"checked\"") : (""));
        echo " value=\"register\" autocomplete=\"off\" />";
        echo (isset($context["text_register"]) ? $context["text_register"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm ";
        // line 133
        echo ((((isset($context["so_onepagecheckout_account_open"]) ? $context["so_onepagecheckout_account_open"] : null) == "guest")) ? ("active") : (""));
        echo "\"><input type=\"radio\" name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][so_onepagecheckout_account_open]\" ";
        echo ((((isset($context["so_onepagecheckout_account_open"]) ? $context["so_onepagecheckout_account_open"] : null) == "guest")) ? ("checked=\"checked\"") : (""));
        echo " value=\"guest\" autocomplete=\"off\" />";
        echo (isset($context["text_guest"]) ? $context["text_guest"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm ";
        // line 134
        echo ((((isset($context["so_onepagecheckout_account_open"]) ? $context["so_onepagecheckout_account_open"] : null) == "login")) ? ("active") : (""));
        echo "\"><input type=\"radio\" name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][so_onepagecheckout_account_open]\" ";
        echo ((((isset($context["so_onepagecheckout_account_open"]) ? $context["so_onepagecheckout_account_open"] : null) == "login")) ? ("checked=\"checked\"") : (""));
        echo " value=\"login\" autocomplete=\"off\" />";
        echo (isset($context["text_login"]) ? $context["text_login"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t</div>\t 
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div id=\"tab-shopping-cart-setting\" class=\"tab-pane\">
\t\t\t\t\t\t\t<fieldset>
\t\t\t\t\t\t\t\t<legend>";
        // line 141
        echo (isset($context["text_shopping_cart"]) ? $context["text_shopping_cart"] : null);
        echo "</legend>
\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"input-status\"><span  data-toggle=\"tooltip\" title=\"Shopping Cart Status\" >";
        // line 143
        echo (isset($context["text_shopping_cart_status"]) ? $context["text_shopping_cart_status"] : null);
        echo "</span></label>
\t\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t\t\t<div class=\"btn-group\" data-toggle=\"buttons\">
\t\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm ";
        // line 146
        echo ((((isset($context["shopping_cart_status"]) ? $context["shopping_cart_status"] : null) == 1)) ? ("active") : (""));
        echo "\"><input type=\"radio\"  name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][shopping_cart_status]\" ";
        echo ((((isset($context["shopping_cart_status"]) ? $context["shopping_cart_status"] : null) == 1)) ? ("checked=\"checked\"") : (""));
        echo " value=\"1\" autocomplete=\"off\">";
        echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm ";
        // line 147
        echo ((((isset($context["shopping_cart_status"]) ? $context["shopping_cart_status"] : null) == 0)) ? ("active") : (""));
        echo "\"><input type=\"radio\"  name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][shopping_cart_status]\" ";
        echo ((((isset($context["shopping_cart_status"]) ? $context["shopping_cart_status"] : null) == 0)) ? ("checked=\"checked\"") : (""));
        echo " value=\"0\" autocomplete=\"off\">";
        echo (isset($context["text_no"]) ? $context["text_no"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"input-weight\"><span  data-toggle=\"tooltip\" title=\"Show Weight\" >";
        // line 152
        echo (isset($context["text_show_weight"]) ? $context["text_show_weight"] : null);
        echo "</span></label>
\t\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t\t\t<div class=\"btn-group\" data-toggle=\"buttons\">
\t\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm  ";
        // line 155
        echo ((((isset($context["show_product_weight"]) ? $context["show_product_weight"] : null) == 1)) ? ("active") : (""));
        echo "\"><input type=\"radio\"  name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][show_product_weight]\" ";
        echo ((((isset($context["show_product_weight"]) ? $context["show_product_weight"] : null) == 1)) ? ("checked=\"checked\"") : (""));
        echo " value=\"1\" autocomplete=\"off\">";
        echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm  ";
        // line 156
        echo ((((isset($context["show_product_weight"]) ? $context["show_product_weight"] : null) == 0)) ? ("active") : (""));
        echo "\"><input type=\"radio\"  name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][show_product_weight]\" ";
        echo ((((isset($context["show_product_weight"]) ? $context["show_product_weight"] : null) == 0)) ? ("checked=\"checked\"") : (""));
        echo " value=\"0\" autocomplete=\"off\">";
        echo (isset($context["text_no"]) ? $context["text_no"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"input-qnty_update\"><span  data-toggle=\"tooltip\" title=\"Show product Quantity update on cart\">";
        // line 161
        echo (isset($context["text_quantity_update_permission"]) ? $context["text_quantity_update_permission"] : null);
        echo "</span></label>
\t\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t\t\t<div class=\"btn-group\" data-toggle=\"buttons\">
\t\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm  ";
        // line 164
        echo ((((isset($context["show_product_qnty_update"]) ? $context["show_product_qnty_update"] : null) == 1)) ? ("active") : (""));
        echo "\"><input type=\"radio\"  name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][show_product_qnty_update]\" ";
        echo ((((isset($context["show_product_qnty_update"]) ? $context["show_product_qnty_update"] : null) == 1)) ? ("checked=\"checked\"") : (""));
        echo " value=\"1\" autocomplete=\"off\">";
        echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm  ";
        // line 165
        echo ((((isset($context["show_product_qnty_update"]) ? $context["show_product_qnty_update"] : null) == 0)) ? ("active") : (""));
        echo "\"><input type=\"radio\"  name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][show_product_qnty_update]\" ";
        echo ((((isset($context["show_product_qnty_update"]) ? $context["show_product_qnty_update"] : null) == 0)) ? ("checked=\"checked\"") : (""));
        echo " value=\"0\" autocomplete=\"off\">";
        echo (isset($context["text_no"]) ? $context["text_no"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"input-qnty_update\"><span  data-toggle=\"tooltip\" title=\"Show remove cart button\">";
        // line 170
        echo (isset($context["text_show_removecart"]) ? $context["text_show_removecart"] : null);
        echo "</span></label>
\t\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t\t\t<div class=\"btn-group\" data-toggle=\"buttons\">
\t\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm  ";
        // line 173
        echo ((((isset($context["show_product_removecart"]) ? $context["show_product_removecart"] : null) == 1)) ? ("active") : (""));
        echo "\"><input type=\"radio\"  name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][show_product_removecart]\" ";
        echo ((((isset($context["show_product_removecart"]) ? $context["show_product_removecart"] : null) == 1)) ? ("checked=\"checked\"") : (""));
        echo " value=\"1\" autocomplete=\"off\">";
        echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm  ";
        // line 174
        echo ((((isset($context["show_product_removecart"]) ? $context["show_product_removecart"] : null) == 0)) ? ("active") : (""));
        echo "\"><input type=\"radio\"  name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][show_product_removecart]\" ";
        echo ((((isset($context["show_product_removecart"]) ? $context["show_product_removecart"] : null) == 0)) ? ("checked=\"checked\"") : (""));
        echo " value=\"0\" autocomplete=\"off\">";
        echo (isset($context["text_no"]) ? $context["text_no"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"input-image_width\">";
        // line 179
        echo (isset($context["text_product_image_size"]) ? $context["text_product_image_size"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t<div class=\"col-sm-3\">
\t\t\t\t\t\t\t\t\t\t<input type=\"text\" value=\"";
        // line 181
        echo (isset($context["show_product_image_width"]) ? $context["show_product_image_width"] : null);
        echo "\" name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][show_product_image_width]\" class=\"form-control\" placeholder=\"Width\">
\t\t\t\t\t\t\t\t\t\t";
        // line 182
        if ((isset($context["error_product_image_width"]) ? $context["error_product_image_width"] : null)) {
            // line 183
            echo "\t\t\t\t\t  \t\t\t\t\t\t<div class=\"text-danger\">";
            echo (isset($context["error_product_image_width"]) ? $context["error_product_image_width"] : null);
            echo "</div>
\t\t\t\t\t  \t\t\t\t\t";
        }
        // line 185
        echo "\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"col-sm-3\">
\t\t\t\t\t\t\t\t\t\t<input type=\"text\" value=\"";
        // line 187
        echo (isset($context["show_product_image_height"]) ? $context["show_product_image_height"] : null);
        echo "\" name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][show_product_image_height]\" class=\"form-control\" placeholder=\"Height\">
\t\t\t\t\t\t\t\t\t\t";
        // line 188
        if ((isset($context["error_product_image_height"]) ? $context["error_product_image_height"] : null)) {
            // line 189
            echo "\t\t\t\t\t  \t\t\t\t\t\t<div class=\"text-danger\">";
            echo (isset($context["error_product_image_height"]) ? $context["error_product_image_height"] : null);
            echo "</div>
\t\t\t\t\t  \t\t\t\t\t";
        }
        // line 191
        echo "\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</fieldset>
\t\t\t\t\t\t\t<fieldset>
\t\t\t\t\t\t\t\t<legend>";
        // line 195
        echo (isset($context["text_coupon_voucher"]) ? $context["text_coupon_voucher"] : null);
        echo "</legend>
\t\t\t\t\t\t\t\t<table class=\"table table-bordered\">
\t\t\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t<td class=\"text-left\">";
        // line 199
        echo (isset($context["text_show_module_name"]) ? $context["text_show_module_name"] : null);
        echo "</td>
\t\t\t\t\t\t\t\t\t\t\t<th class=\"text-center\">";
        // line 200
        echo (isset($context["text_login_account"]) ? $context["text_login_account"] : null);
        echo "</th>
\t\t\t\t\t\t\t\t\t\t\t<th class=\"text-center\">";
        // line 201
        echo (isset($context["text_register_account"]) ? $context["text_register_account"] : null);
        echo "</th>
\t\t\t\t\t\t\t\t\t\t\t<th class=\"text-right\">";
        // line 202
        echo (isset($context["text_guest_checkout"]) ? $context["text_guest_checkout"] : null);
        echo "</th>
\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t<td class=\"text-left\">";
        // line 207
        echo (isset($context["text_coupon"]) ? $context["text_coupon"] : null);
        echo "</td>
\t\t\t\t\t\t\t\t\t\t\t<td class=\"text-center\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-sm-12\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"btn-group\" data-toggle=\"buttons\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm  ";
        // line 211
        echo ((((isset($context["coupon_login_status"]) ? $context["coupon_login_status"] : null) == 1)) ? ("active") : (""));
        echo "\"><input type=\"radio\" name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][coupon_login_status]\" ";
        echo ((((isset($context["coupon_login_status"]) ? $context["coupon_login_status"] : null) == 1)) ? ("checked=\"checked\"") : (""));
        echo " value=\"1\" autocomplete=\"off\">";
        echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm  ";
        // line 212
        echo ((((isset($context["coupon_login_status"]) ? $context["coupon_login_status"] : null) == 0)) ? ("active") : (""));
        echo "\"><input type=\"radio\" name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][coupon_login_status]\" ";
        echo ((((isset($context["coupon_login_status"]) ? $context["coupon_login_status"] : null) == 0)) ? ("checked=\"checked\"") : (""));
        echo " value=\"0\" autocomplete=\"off\">";
        echo (isset($context["text_no"]) ? $context["text_no"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</td>\t
\t\t\t\t\t\t\t\t\t\t\t<td class=\"text-center\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-sm-12\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"btn-group\" data-toggle=\"buttons\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm  ";
        // line 219
        echo ((((isset($context["coupon_register_status"]) ? $context["coupon_register_status"] : null) == 1)) ? ("active") : (""));
        echo "\"><input type=\"radio\" name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][coupon_register_status]\" ";
        echo ((((isset($context["coupon_register_status"]) ? $context["coupon_register_status"] : null) == 1)) ? ("checked=\"checked\"") : (""));
        echo " value=\"1\" autocomplete=\"off\">";
        echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm  ";
        // line 220
        echo ((((isset($context["coupon_register_status"]) ? $context["coupon_register_status"] : null) == 0)) ? ("active") : (""));
        echo "\"><input type=\"radio\" name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][coupon_register_status]\" ";
        echo ((((isset($context["coupon_register_status"]) ? $context["coupon_register_status"] : null) == 0)) ? ("checked=\"checked\"") : (""));
        echo " value=\"0\" autocomplete=\"off\">";
        echo (isset($context["text_no"]) ? $context["text_no"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t<td class=\"text-right\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-sm-12\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"btn-group\" data-toggle=\"buttons\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm  ";
        // line 227
        echo ((((isset($context["coupon_guest_status"]) ? $context["coupon_guest_status"] : null) == 1)) ? ("active") : (""));
        echo "\"><input type=\"radio\" name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][coupon_guest_status]\" ";
        echo ((((isset($context["coupon_guest_status"]) ? $context["coupon_guest_status"] : null) == 1)) ? ("checked=\"checked\"") : (""));
        echo " value=\"1\" autocomplete=\"off\">";
        echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm  ";
        // line 228
        echo ((((isset($context["coupon_guest_status"]) ? $context["coupon_guest_status"] : null) == 0)) ? ("active") : (""));
        echo "\"><input type=\"radio\" name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][coupon_guest_status]\" ";
        echo ((((isset($context["coupon_guest_status"]) ? $context["coupon_guest_status"] : null) == 0)) ? ("checked=\"checked\"") : (""));
        echo " value=\"0\" autocomplete=\"off\">";
        echo (isset($context["text_no"]) ? $context["text_no"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t<td class=\"text-left\">";
        // line 234
        echo (isset($context["text_reward"]) ? $context["text_reward"] : null);
        echo "</td>
\t\t\t\t\t\t\t\t\t\t\t<td class=\"text-center\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-sm-12\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"btn-group\" data-toggle=\"buttons\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm  ";
        // line 238
        echo ((((isset($context["reward_login_status"]) ? $context["reward_login_status"] : null) == 1)) ? ("active") : (""));
        echo "\"><input type=\"radio\" name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][reward_login_status]\" ";
        echo ((((isset($context["reward_login_status"]) ? $context["reward_login_status"] : null) == 1)) ? ("checked=\"checked\"") : (""));
        echo " value=\"1\" autocomplete=\"off\">";
        echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm  ";
        // line 239
        echo ((((isset($context["reward_login_status"]) ? $context["reward_login_status"] : null) == 0)) ? ("active") : (""));
        echo "\"><input type=\"radio\" name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][reward_login_status]\" ";
        echo ((((isset($context["reward_login_status"]) ? $context["reward_login_status"] : null) == 0)) ? ("checked=\"checked\"") : (""));
        echo " value=\"0\" autocomplete=\"off\">";
        echo (isset($context["text_no"]) ? $context["text_no"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</td>\t
\t\t\t\t\t\t\t\t\t\t\t<td class=\"text-center\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-sm-12\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"btn-group\" data-toggle=\"buttons\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm  ";
        // line 246
        echo ((((isset($context["reward_register_status"]) ? $context["reward_register_status"] : null) == 1)) ? ("active") : (""));
        echo "\"><input type=\"radio\" name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][reward_register_status]\" ";
        echo ((((isset($context["reward_register_status"]) ? $context["reward_register_status"] : null) == 1)) ? ("checked=\"checked\"") : (""));
        echo " value=\"1\" autocomplete=\"off\">";
        echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm  ";
        // line 247
        echo ((((isset($context["reward_register_status"]) ? $context["reward_register_status"] : null) == 0)) ? ("active") : (""));
        echo "\"><input type=\"radio\" name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][reward_register_status]\" ";
        echo ((((isset($context["reward_register_status"]) ? $context["reward_register_status"] : null) == 0)) ? ("checked=\"checked\"") : (""));
        echo " value=\"0\" autocomplete=\"off\">";
        echo (isset($context["text_no"]) ? $context["text_no"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t<td class=\"text-right\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-sm-12\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"btn-group\" data-toggle=\"buttons\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm  ";
        // line 254
        echo ((((isset($context["reward_guest_status"]) ? $context["reward_guest_status"] : null) == 1)) ? ("active") : (""));
        echo "\"><input type=\"radio\" name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][reward_guest_status]\" ";
        echo ((((isset($context["reward_guest_status"]) ? $context["reward_guest_status"] : null) == 1)) ? ("checked=\"checked\"") : (""));
        echo " value=\"1\" autocomplete=\"off\">";
        echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm  ";
        // line 255
        echo ((((isset($context["reward_guest_status"]) ? $context["reward_guest_status"] : null) == 0)) ? ("active") : (""));
        echo "\"><input type=\"radio\" name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][reward_guest_status]\" ";
        echo ((((isset($context["reward_guest_status"]) ? $context["reward_guest_status"] : null) == 0)) ? ("checked=\"checked\"") : (""));
        echo " value=\"0\" autocomplete=\"off\">";
        echo (isset($context["text_no"]) ? $context["text_no"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t<td class=\"text-left\">";
        // line 261
        echo (isset($context["text_voucher"]) ? $context["text_voucher"] : null);
        echo "</td>
\t\t\t\t\t\t\t\t\t\t\t<td class=\"text-center\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-sm-12\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"btn-group\" data-toggle=\"buttons\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm  ";
        // line 265
        echo ((((isset($context["voucher_login_status"]) ? $context["voucher_login_status"] : null) == 1)) ? ("active") : (""));
        echo "\"><input type=\"radio\" name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][voucher_login_status]\" ";
        echo ((((isset($context["voucher_login_status"]) ? $context["voucher_login_status"] : null) == 1)) ? ("checked=\"checked\"") : (""));
        echo " value=\"1\" autocomplete=\"off\">";
        echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm  ";
        // line 266
        echo ((((isset($context["voucher_login_status"]) ? $context["voucher_login_status"] : null) == 0)) ? ("active") : (""));
        echo "\"><input type=\"radio\" name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][voucher_login_status]\" ";
        echo ((((isset($context["voucher_login_status"]) ? $context["voucher_login_status"] : null) == 0)) ? ("checked=\"checked\"") : (""));
        echo " value=\"0\" autocomplete=\"off\">";
        echo (isset($context["text_no"]) ? $context["text_no"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</td>\t
\t\t\t\t\t\t\t\t\t\t\t<td class=\"text-center\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-sm-12\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"btn-group\" data-toggle=\"buttons\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm  ";
        // line 273
        echo ((((isset($context["voucher_register_status"]) ? $context["voucher_register_status"] : null) == 1)) ? ("active") : (""));
        echo "\"><input type=\"radio\" name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][voucher_register_status]\" ";
        echo ((((isset($context["voucher_register_status"]) ? $context["voucher_register_status"] : null) == 1)) ? ("checked=\"checked\"") : (""));
        echo " value=\"1\" autocomplete=\"off\">";
        echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm  ";
        // line 274
        echo ((((isset($context["voucher_register_status"]) ? $context["voucher_register_status"] : null) == 0)) ? ("active") : (""));
        echo "\"><input type=\"radio\" name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][voucher_register_status]\" ";
        echo ((((isset($context["voucher_register_status"]) ? $context["voucher_register_status"] : null) == 0)) ? ("checked=\"checked\"") : (""));
        echo " value=\"0\" autocomplete=\"off\">";
        echo (isset($context["text_no"]) ? $context["text_no"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t<td class=\"text-right\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-sm-12\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"btn-group\" data-toggle=\"buttons\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm  ";
        // line 281
        echo ((((isset($context["voucher_guest_status"]) ? $context["voucher_guest_status"] : null) == 1)) ? ("active") : (""));
        echo "\"><input type=\"radio\" name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][voucher_guest_status]\" ";
        echo ((((isset($context["voucher_guest_status"]) ? $context["voucher_guest_status"] : null) == 1)) ? ("checked=\"checked\"") : (""));
        echo " value=\"1\" autocomplete=\"off\">";
        echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm  ";
        // line 282
        echo ((((isset($context["voucher_guest_status"]) ? $context["voucher_guest_status"] : null) == 0)) ? ("active") : (""));
        echo "\"><input type=\"radio\" name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][voucher_guest_status]\" ";
        echo ((((isset($context["voucher_guest_status"]) ? $context["voucher_guest_status"] : null) == 0)) ? ("checked=\"checked\"") : (""));
        echo " value=\"0\" autocomplete=\"off\">";
        echo (isset($context["text_no"]) ? $context["text_no"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t</fieldset>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div id=\"tab-delivery_method-setting\" class=\"tab-pane\">
\t\t\t\t\t\t\t";
        // line 292
        if (twig_length_filter($this->env, (isset($context["shipping_methods"]) ? $context["shipping_methods"] : null))) {
            // line 293
            echo "\t\t\t\t\t\t\t<fieldset>
\t\t\t\t\t\t\t\t<legend>";
            // line 294
            echo (isset($context["text_delivery_methods"]) ? $context["text_delivery_methods"] : null);
            echo "</legend>
\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"input-status\"><span  data-toggle=\"tooltip\" title=\"Status\" >";
            // line 296
            echo (isset($context["text_delivery_methods_status"]) ? $context["text_delivery_methods_status"] : null);
            echo "</span></label>
\t\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t\t\t<div class=\"btn-group\" data-toggle=\"buttons\">
\t\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm ";
            // line 299
            echo ((((isset($context["delivery_method_status"]) ? $context["delivery_method_status"] : null) == 1)) ? ("active") : (""));
            echo "\"><input type=\"radio\"  name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][delivery_method_status]\" ";
            echo ((((isset($context["delivery_method_status"]) ? $context["delivery_method_status"] : null) == 1)) ? ("checked=\"checked\"") : (""));
            echo " value=\"1\" autocomplete=\"off\">";
            echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
            echo "</label>
\t\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm ";
            // line 300
            echo ((((isset($context["delivery_method_status"]) ? $context["delivery_method_status"] : null) == 0)) ? ("active") : (""));
            echo "\"><input type=\"radio\"  name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][delivery_method_status]\" ";
            echo ((((isset($context["delivery_method_status"]) ? $context["delivery_method_status"] : null) == 0)) ? ("checked=\"checked\"") : (""));
            echo " value=\"0\" autocomplete=\"off\">";
            echo (isset($context["text_no"]) ? $context["text_no"] : null);
            echo "</label>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"input-customer-group\"><span data-toggle=\"tooltip\" title=\"\" data-original-title=\"Default selected Shipping Method\">";
            // line 305
            echo (isset($context["text_shipping_methods"]) ? $context["text_shipping_methods"] : null);
            echo "</span></label>
\t\t\t\t\t\t\t\t\t<div class=\"col-md-5\">
\t\t\t\t\t\t\t\t\t\t<select class=\"form-control\" name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][so_onepagecheckout_default_shipping]\">
\t\t\t\t\t\t\t\t\t\t\t";
            // line 308
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["shipping_methods"]) ? $context["shipping_methods"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["shipping_method"]) {
                // line 309
                echo "\t\t\t\t\t\t\t\t\t\t  \t\t<option value=\"";
                echo $this->getAttribute($context["shipping_method"], "code", array());
                echo "\" ";
                echo ((($this->getAttribute($context["shipping_method"], "code", array()) == (isset($context["so_onepagecheckout_default_shipping"]) ? $context["so_onepagecheckout_default_shipping"] : null))) ? ("selected=\"selected\"") : (""));
                echo ">";
                echo $this->getAttribute($context["shipping_method"], "title", array());
                echo "</option>
\t\t\t\t\t\t\t\t\t\t  \t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['shipping_method'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 311
            echo "\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<table class=\"table table-bordered\">
\t\t\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t<th class=\"text-left\">";
            // line 317
            echo (isset($context["text_shipping_methods"]) ? $context["text_shipping_methods"] : null);
            echo "</th>
\t\t\t\t\t\t\t\t\t\t\t<th class=\"text-center\">";
            // line 318
            echo (isset($context["text_status"]) ? $context["text_status"] : null);
            echo "</th>
\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t\t\t";
            // line 322
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["shipping_methods"]) ? $context["shipping_methods"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["shipping_method"]) {
                // line 323
                echo "\t\t\t\t\t\t\t\t\t\t";
                $context["shipping_status"] = ($this->getAttribute($context["shipping_method"], "code", array()) . "_status");
                // line 324
                echo "\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t<td class=\"text-left\">";
                // line 325
                echo $this->getAttribute($context["shipping_method"], "title", array());
                echo "</td>
\t\t\t\t\t\t\t\t\t\t\t<td class=\"text-center\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"btn-group\" data-toggle=\"buttons\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm ";
                // line 330
                echo ((($this->getAttribute((isset($context["setting_so_onepagecheckout_layout_setting"]) ? $context["setting_so_onepagecheckout_layout_setting"] : null), (isset($context["shipping_status"]) ? $context["shipping_status"] : null), array(), "array", true, true) && ($this->getAttribute((isset($context["setting_so_onepagecheckout_layout_setting"]) ? $context["setting_so_onepagecheckout_layout_setting"] : null), (isset($context["shipping_status"]) ? $context["shipping_status"] : null), array(), "array") == 1))) ? ("active") : (""));
                echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"radio\" name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][";
                // line 331
                echo $this->getAttribute($context["shipping_method"], "code", array());
                echo "_status]\" ";
                echo ((($this->getAttribute((isset($context["setting_so_onepagecheckout_layout_setting"]) ? $context["setting_so_onepagecheckout_layout_setting"] : null), (isset($context["shipping_status"]) ? $context["shipping_status"] : null), array(), "array", true, true) && ($this->getAttribute((isset($context["setting_so_onepagecheckout_layout_setting"]) ? $context["setting_so_onepagecheckout_layout_setting"] : null), (isset($context["shipping_status"]) ? $context["shipping_status"] : null), array(), "array") == 1))) ? ("checked=\"checked\"") : (""));
                echo " value=\"1\" autocomplete=\"off\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 332
                echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
                echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm ";
                // line 334
                echo (((twig_test_empty($this->getAttribute((isset($context["setting_so_onepagecheckout_layout_setting"]) ? $context["setting_so_onepagecheckout_layout_setting"] : null), (isset($context["shipping_status"]) ? $context["shipping_status"] : null), array(), "array")) || ($this->getAttribute((isset($context["setting_so_onepagecheckout_layout_setting"]) ? $context["setting_so_onepagecheckout_layout_setting"] : null), (isset($context["shipping_status"]) ? $context["shipping_status"] : null), array(), "array", true, true) && ($this->getAttribute((isset($context["setting_so_onepagecheckout_layout_setting"]) ? $context["setting_so_onepagecheckout_layout_setting"] : null), (isset($context["shipping_status"]) ? $context["shipping_status"] : null), array(), "array") == 0)))) ? ("active") : (""));
                echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"radio\" name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][";
                // line 335
                echo $this->getAttribute($context["shipping_method"], "code", array());
                echo "_status]\" ";
                echo ((($this->getAttribute((isset($context["setting_so_onepagecheckout_layout_setting"]) ? $context["setting_so_onepagecheckout_layout_setting"] : null), (isset($context["shipping_status"]) ? $context["shipping_status"] : null), array(), "array", true, true) && ($this->getAttribute((isset($context["setting_so_onepagecheckout_layout_setting"]) ? $context["setting_so_onepagecheckout_layout_setting"] : null), (isset($context["shipping_status"]) ? $context["shipping_status"] : null), array(), "array") == 0))) ? ("checked=\"checked\"") : (""));
                echo " value=\"0\" autocomplete=\"off\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 336
                echo (isset($context["text_no"]) ? $context["text_no"] : null);
                echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['shipping_method'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 344
            echo "\t\t\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t</fieldset>
\t\t\t\t\t\t\t";
        }
        // line 348
        echo "\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div id=\"tab-payment_method-setting\" class=\"tab-pane\">
\t\t\t\t\t\t\t";
        // line 350
        if (twig_length_filter($this->env, (isset($context["payment_methods"]) ? $context["payment_methods"] : null))) {
            // line 351
            echo "\t\t\t\t\t\t\t<fieldset>
\t\t\t\t\t\t\t\t<legend>";
            // line 352
            echo (isset($context["text_payment_methods"]) ? $context["text_payment_methods"] : null);
            echo "</legend>
\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"input-status\"><span  data-toggle=\"tooltip\" title=\"Status\" >";
            // line 354
            echo (isset($context["text_payment_methods_status"]) ? $context["text_payment_methods_status"] : null);
            echo "</span></label>
\t\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t\t\t<div class=\"btn-group\" data-toggle=\"buttons\">
\t\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm ";
            // line 357
            echo (((array_key_exists("payment_method_status", $context) && ((isset($context["payment_method_status"]) ? $context["payment_method_status"] : null) == 1))) ? ("active") : (""));
            echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"radio\"  name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][payment_method_status]\" ";
            // line 358
            echo (((array_key_exists("payment_method_status", $context) && ((isset($context["payment_method_status"]) ? $context["payment_method_status"] : null) == 1))) ? ("checked=\"checked\"") : (""));
            echo " value=\"1\" autocomplete=\"off\">
\t\t\t\t\t\t\t\t\t\t\t\t";
            // line 359
            echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
            echo "
\t\t\t\t\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm ";
            // line 361
            echo (((array_key_exists("payment_method_status", $context) && ((isset($context["payment_method_status"]) ? $context["payment_method_status"] : null) == 0))) ? ("active") : (""));
            echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"radio\"  name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][payment_method_status]\" ";
            // line 362
            echo (((array_key_exists("payment_method_status", $context) && ((isset($context["payment_method_status"]) ? $context["payment_method_status"] : null) == 0))) ? ("checked=\"checked\"") : (""));
            echo " value=\"0\" autocomplete=\"off\">
\t\t\t\t\t\t\t\t\t\t\t\t";
            // line 363
            echo (isset($context["text_no"]) ? $context["text_no"] : null);
            echo "
\t\t\t\t\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"input-customer-group\"><span data-toggle=\"tooltip\" title=\"\" data-original-title=\"Default selected Payment Method\">";
            // line 369
            echo (isset($context["text_payment_methods"]) ? $context["text_payment_methods"] : null);
            echo "</span></label>
\t\t\t\t\t\t\t\t\t<div class=\"col-md-5\">
\t\t\t\t\t\t\t\t\t\t<select class=\"form-control\" name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][so_onepagecheckout_default_payment]\">
\t\t\t\t\t\t\t\t\t\t\t";
            // line 372
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["payment_methods"]) ? $context["payment_methods"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["payment_method"]) {
                // line 373
                echo "\t\t\t\t\t\t\t\t\t\t  \t\t<option value=\"";
                echo $this->getAttribute($context["payment_method"], "code", array());
                echo "\" ";
                echo ((($this->getAttribute($context["payment_method"], "code", array()) == (isset($context["so_onepagecheckout_default_payment"]) ? $context["so_onepagecheckout_default_payment"] : null))) ? ("selected=\"selected\"") : (""));
                echo ">";
                echo $this->getAttribute($context["payment_method"], "title", array());
                echo "</option>
\t\t\t\t\t\t\t\t\t\t  \t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['payment_method'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 375
            echo "\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<table class=\"table table-bordered\">
\t\t\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t<th class=\"text-left\">";
            // line 381
            echo (isset($context["text_payment_methods"]) ? $context["text_payment_methods"] : null);
            echo "</th>
\t\t\t\t\t\t\t\t\t\t\t<th class=\"text-center\">";
            // line 382
            echo (isset($context["text_status"]) ? $context["text_status"] : null);
            echo "</th>
\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t\t\t";
            // line 386
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["payment_methods"]) ? $context["payment_methods"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["payment_method"]) {
                // line 387
                echo "\t\t\t\t\t\t\t\t\t\t";
                $context["payment_status"] = ($this->getAttribute($context["payment_method"], "code", array()) . "_status");
                // line 388
                echo "\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t<td class=\"text-left\">";
                // line 389
                echo $this->getAttribute($context["payment_method"], "title", array());
                echo "</td>
\t\t\t\t\t\t\t\t\t\t\t<td class=\"text-center\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"btn-group\" data-toggle=\"buttons\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm ";
                // line 394
                echo ((($this->getAttribute((isset($context["setting_so_onepagecheckout_layout_setting"]) ? $context["setting_so_onepagecheckout_layout_setting"] : null), (isset($context["payment_status"]) ? $context["payment_status"] : null), array(), "array", true, true) && ($this->getAttribute((isset($context["setting_so_onepagecheckout_layout_setting"]) ? $context["setting_so_onepagecheckout_layout_setting"] : null), (isset($context["payment_status"]) ? $context["payment_status"] : null), array(), "array") == 1))) ? ("active") : (""));
                echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"radio\" name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][";
                // line 395
                echo $this->getAttribute($context["payment_method"], "code", array());
                echo "_status]\" ";
                echo ((($this->getAttribute((isset($context["setting_so_onepagecheckout_layout_setting"]) ? $context["setting_so_onepagecheckout_layout_setting"] : null), (isset($context["payment_status"]) ? $context["payment_status"] : null), array(), "array", true, true) && ($this->getAttribute((isset($context["setting_so_onepagecheckout_layout_setting"]) ? $context["setting_so_onepagecheckout_layout_setting"] : null), (isset($context["payment_status"]) ? $context["payment_status"] : null), array(), "array") == 1))) ? ("checked=\"checked\"") : (""));
                echo " value=\"1\" autocomplete=\"off\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 396
                echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
                echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm ";
                // line 398
                echo ((($this->getAttribute((isset($context["setting_so_onepagecheckout_layout_setting"]) ? $context["setting_so_onepagecheckout_layout_setting"] : null), (isset($context["payment_status"]) ? $context["payment_status"] : null), array(), "array", true, true) && ($this->getAttribute((isset($context["setting_so_onepagecheckout_layout_setting"]) ? $context["setting_so_onepagecheckout_layout_setting"] : null), (isset($context["payment_status"]) ? $context["payment_status"] : null), array(), "array") == 0))) ? ("active") : (""));
                echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"radio\" name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][";
                // line 399
                echo $this->getAttribute($context["payment_method"], "code", array());
                echo "_status]\" ";
                echo ((($this->getAttribute((isset($context["setting_so_onepagecheckout_layout_setting"]) ? $context["setting_so_onepagecheckout_layout_setting"] : null), (isset($context["payment_status"]) ? $context["payment_status"] : null), array(), "array", true, true) && ($this->getAttribute((isset($context["setting_so_onepagecheckout_layout_setting"]) ? $context["setting_so_onepagecheckout_layout_setting"] : null), (isset($context["payment_status"]) ? $context["payment_status"] : null), array(), "array") == 0))) ? ("checked=\"checked\"") : (""));
                echo " value=\"0\" autocomplete=\"off\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 400
                echo (isset($context["text_no"]) ? $context["text_no"] : null);
                echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['payment_method'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 408
            echo "\t\t\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t</fieldset>
\t\t\t\t\t\t\t";
        }
        // line 412
        echo "\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div id=\"tab-confirm_order-setting\" class=\"tab-pane\">
\t\t\t\t\t\t\t<fieldset>
\t\t\t\t\t\t\t\t<legend>";
        // line 415
        echo (isset($context["text_confirm_order"]) ? $context["text_confirm_order"] : null);
        echo "</legend>
\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"input-status\">";
        // line 417
        echo (isset($context["text_add_comments"]) ? $context["text_add_comments"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t\t\t<div class=\"btn-group\" data-toggle=\"buttons\">
\t\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm ";
        // line 420
        echo ((((isset($context["comment_status"]) ? $context["comment_status"] : null) == 1)) ? ("active") : (""));
        echo "\"><input type=\"radio\" name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][comment_status]\" ";
        echo ((((isset($context["comment_status"]) ? $context["comment_status"] : null) == 1)) ? ("checked=\"checked\"") : (""));
        echo " value=\"1\" autocomplete=\"off\">";
        echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm ";
        // line 421
        echo ((((isset($context["comment_status"]) ? $context["comment_status"] : null) == 0)) ? ("active") : (""));
        echo "\"><input type=\"radio\" name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][comment_status]\" ";
        echo ((((isset($context["comment_status"]) ? $context["comment_status"] : null) == 0)) ? ("checked=\"checked\"") : (""));
        echo " value=\"0\" autocomplete=\"off\">";
        echo (isset($context["text_no"]) ? $context["text_no"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"input-status\">";
        // line 426
        echo (isset($context["text_require_comment"]) ? $context["text_require_comment"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t\t\t<div class=\"btn-group\" data-toggle=\"buttons\">
\t\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm ";
        // line 429
        echo ((((isset($context["require_comment_status"]) ? $context["require_comment_status"] : null) == 1)) ? ("active") : (""));
        echo "\"><input type=\"radio\" name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][require_comment_status]\" ";
        echo ((((isset($context["require_comment_status"]) ? $context["require_comment_status"] : null) == 1)) ? ("checked=\"checked\"") : (""));
        echo " value=\"1\" autocomplete=\"off\">";
        echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
        echo "</label>\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm ";
        // line 430
        echo ((((isset($context["require_comment_status"]) ? $context["require_comment_status"] : null) == 0)) ? ("active") : (""));
        echo "\"><input type=\"radio\" name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][require_comment_status]\" ";
        echo ((((isset($context["require_comment_status"]) ? $context["require_comment_status"] : null) == 0)) ? ("checked=\"checked\"") : (""));
        echo " value=\"0\" autocomplete=\"off\">";
        echo (isset($context["text_no"]) ? $context["text_no"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"input-status\">";
        // line 435
        echo (isset($context["text_show_newsletter"]) ? $context["text_show_newsletter"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t\t\t<div class=\"btn-group\" data-toggle=\"buttons\">
\t\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm ";
        // line 438
        echo ((((isset($context["show_newsletter"]) ? $context["show_newsletter"] : null) == 1)) ? ("active") : (""));
        echo "\"><input type=\"radio\" name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][show_newsletter]\" ";
        echo ((((isset($context["show_newsletter"]) ? $context["show_newsletter"] : null) == 1)) ? ("checked=\"checked\"") : (""));
        echo " value=\"1\" autocomplete=\"off\">";
        echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm ";
        // line 439
        echo ((((isset($context["show_newsletter"]) ? $context["show_newsletter"] : null) == 0)) ? ("active") : (""));
        echo "\"><input type=\"radio\" name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][show_newsletter]\" ";
        echo ((((isset($context["show_newsletter"]) ? $context["show_newsletter"] : null) == 0)) ? ("checked=\"checked\"") : (""));
        echo " value=\"0\" autocomplete=\"off\">";
        echo (isset($context["text_no"]) ? $context["text_no"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"input-status\">";
        // line 444
        echo (isset($context["text_show_privacy"]) ? $context["text_show_privacy"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t\t\t<div class=\"btn-group\" data-toggle=\"buttons\">
\t\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm ";
        // line 447
        echo ((((isset($context["show_privacy"]) ? $context["show_privacy"] : null) == 1)) ? ("active") : (""));
        echo "\"><input type=\"radio\" name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][show_privacy]\" ";
        echo ((((isset($context["show_privacy"]) ? $context["show_privacy"] : null) == 1)) ? ("checked=\"checked\"") : (""));
        echo " value=\"1\" autocomplete=\"off\">";
        echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm ";
        // line 448
        echo ((((isset($context["show_privacy"]) ? $context["show_privacy"] : null) == 0)) ? ("active") : (""));
        echo "\"><input type=\"radio\" name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][show_privacy]\" ";
        echo ((((isset($context["show_privacy"]) ? $context["show_privacy"] : null) == 0)) ? ("checked=\"checked\"") : (""));
        echo " value=\"0\" autocomplete=\"off\">";
        echo (isset($context["text_no"]) ? $context["text_no"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\" for=\"input-status\">";
        // line 453
        echo (isset($context["text_show_term"]) ? $context["text_show_term"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t\t\t<div class=\"btn-group\" data-toggle=\"buttons\">
\t\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm ";
        // line 456
        echo ((((isset($context["show_term"]) ? $context["show_term"] : null) == 1)) ? ("active") : (""));
        echo "\"><input type=\"radio\" name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][show_term]\" ";
        echo ((((isset($context["show_term"]) ? $context["show_term"] : null) == 1)) ? ("checked=\"checked\"") : (""));
        echo " value=\"1\" autocomplete=\"off\">";
        echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t\t\t<label class=\"btn btn-success btn-sm ";
        // line 457
        echo ((((isset($context["show_term"]) ? $context["show_term"] : null) == 0)) ? ("active") : (""));
        echo "\"><input type=\"radio\" name=\"so_onepagecheckout[so_onepagecheckout_layout_setting][show_term]\" ";
        echo ((((isset($context["show_term"]) ? $context["show_term"] : null) == 0)) ? ("checked=\"checked\"") : (""));
        echo " value=\"0\" autocomplete=\"off\">";
        echo (isset($context["text_no"]) ? $context["text_no"] : null);
        echo "</label>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</fieldset>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div id=\"tab-help\" class=\"tab-pane\">
\t\t\t\t\t\t\t<div class=\"tab-body\">
\t\t\t\t\t\t\t\t<ol>
\t\t\t\t\t\t\t\t\t<li><b>Setting your FTP credentials</b></li>
\t\t\t\t\t\t\t\t\t<p>In order to set your FTP credentials, navigate to System -> Settings -> Store Settings.</p>
\t\t\t\t\t\t\t\t\t<p><img src=\"view/javascript/so_onepagecheckout/images/help/1.png\" class=\"img-thumbnail img-responsive\"></p>
\t\t\t\t\t\t\t\t\t<p>Then, choose the FTP tab and fill in your FTP credentials. Make sure you check the Yes radio button for the Enable FTP field and click the blue Save button in the upper right corner of the page as shown:</p>
\t\t\t\t\t\t\t\t\t<p><img src=\"view/javascript/so_onepagecheckout/images/help/2.png\" class=\"img-thumbnail img-responsive\"></p>
\t\t\t\t\t\t\t\t\t<p>Now the Extension Installer will know how to access your FTP and you will be able to install extensions with ease.</p>
\t\t\t\t\t\t\t\t\t<li><b>Installing an extension</b></li>
\t\t\t\t\t\t\t\t\t<p>In order to upload an extension, navigate to Extensions -> Extension Installer. Then click the blue Upload button and provide the route to your extension' s\"*.ocmod.zip\" archive.</p>
\t\t\t\t\t\t\t\t\t<p><img src=\"view/javascript/so_onepagecheckout/images/help/3.png\" class=\"img-thumbnail img-responsive\"></p>
\t\t\t\t\t\t\t\t\t<li><b>Clear Modification</b></li>
\t\t\t\t\t\t\t\t\t<p>In order to upload an extension, navigate to Extensions -> Modifications. Then click the blue Refresh button.</p>
\t\t\t\t\t\t\t\t\t<p><img src=\"view/javascript/so_onepagecheckout/images/help/4.png\" class=\"img-thumbnail img-responsive\"></p>
\t\t\t\t\t\t\t\t</ol>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</form>
\t\t\t</div>
      \t</div>
\t</div>
</div>
<script type=\"text/javascript\"><!--
jQuery(document).ready(function(\$) {
\t\$('select[name=\\'so_onepagecheckout[so_onepagecheckout_general][so_onepagecheckout_country_id]\\']').on('change', function() {
\t\t\$.ajax({
\t\t\turl: 'index.php?route=localisation/country/country&user_token=";
        // line 491
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "&country_id=' + this.value,
\t\t\tdataType: 'json',
\t\t\tbeforeSend: function() {
\t\t\t\t\$('select[name=\\'so_onepagecheckout[so_onepagecheckout_general][so_onepagecheckout_country_id]\\']').after(' <i class=\"fa fa-circle-o-notch fa-spin\"></i>');
\t\t\t},
\t\t\tcomplete: function() {
\t\t\t\t\$('.fa-spin').remove();
\t\t\t},
\t\t\tsuccess: function(json) {
\t\t\t\thtml = '<option value=\"\"> --- Please Select --- </option>';

\t\t\t\tif (json['zone'] && json['zone'] != '') {
\t\t\t\t\tfor (i = 0; i < json['zone'].length; i++) {
\t          \t\t\thtml += '<option value=\"' + json['zone'][i]['zone_id'] + '\"';

\t\t\t\t\t\tif (json['zone'][i]['zone_id'] == '";
        // line 506
        echo (isset($context["so_onepagecheckout_zone_id"]) ? $context["so_onepagecheckout_zone_id"] : null);
        echo "') {
\t            \t\t\thtml += ' selected=\"selected\"';
\t\t\t\t\t\t}

\t\t\t\t\t\thtml += '>' + json['zone'][i]['name'] + '</option>';
\t\t\t\t\t}
\t\t\t\t} else {
\t\t\t\t\thtml += '<option value=\"0\" selected=\"selected\"> --- None --- </option>';
\t\t\t\t}

\t\t\t\t\$('select[name=\\'so_onepagecheckout[so_onepagecheckout_general][so_onepagecheckout_zone_id]\\']').html(html);
\t\t\t\t
\t\t\t\t\$('#button-save').prop('disabled', false);
\t\t\t},
\t\t\terror: function(xhr, ajaxOptions, thrownError) {
\t\t\t\talert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
\t\t\t}
\t\t});
\t});

\t\$('select[name=\\'so_onepagecheckout[so_onepagecheckout_general][so_onepagecheckout_country_id]\\']').trigger('change');
\t
\t// for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
    \$('a[data-toggle=\"tab\"]').on('shown.bs.tab', function (e) {
        // save the latest tab; use cookies if you like 'em better:
        localStorage.setItem('lastTab', \$(this).attr('href'));
    });

    // go to the latest tab, if it exists:
    var lastTab = localStorage.getItem('lastTab');
    if (lastTab) {
        \$('[href=\"' + lastTab + '\"]').tab('show');
    }
    
\t\$('#CheckoutTab a').click(function(e) {
\t  e.preventDefault();
\t  \$(this).tab('show');
\t});

\t// store the currently selected tab in the hash value
\t// \$(\"ul.nav-tabs li a, ul.nav-tabs li ul li a\").on(\"shown.bs.tab\", function(e) {
\t//   var id = \$(e.target).attr(\"href\").substr(1);
\t//   window.location.hash = id;
\t// });

\t// // on load of the page: switch to the currently selected tab
\t// var hash = window.location.hash;
\t// \$('#CheckoutTab a[href=\"' + hash + '\"]').tab('show');
});
//--></script>
";
        // line 556
        echo (isset($context["footer"]) ? $context["footer"] : null);
    }

    public function getTemplateName()
    {
        return "extension/module/so_onepagecheckout.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1324 => 556,  1271 => 506,  1253 => 491,  1212 => 457,  1204 => 456,  1198 => 453,  1186 => 448,  1178 => 447,  1172 => 444,  1160 => 439,  1152 => 438,  1146 => 435,  1134 => 430,  1126 => 429,  1120 => 426,  1108 => 421,  1100 => 420,  1094 => 417,  1089 => 415,  1084 => 412,  1078 => 408,  1064 => 400,  1058 => 399,  1054 => 398,  1049 => 396,  1043 => 395,  1039 => 394,  1031 => 389,  1028 => 388,  1025 => 387,  1021 => 386,  1014 => 382,  1010 => 381,  1002 => 375,  989 => 373,  985 => 372,  979 => 369,  970 => 363,  966 => 362,  962 => 361,  957 => 359,  953 => 358,  949 => 357,  943 => 354,  938 => 352,  935 => 351,  933 => 350,  929 => 348,  923 => 344,  909 => 336,  903 => 335,  899 => 334,  894 => 332,  888 => 331,  884 => 330,  876 => 325,  873 => 324,  870 => 323,  866 => 322,  859 => 318,  855 => 317,  847 => 311,  834 => 309,  830 => 308,  824 => 305,  812 => 300,  804 => 299,  798 => 296,  793 => 294,  790 => 293,  788 => 292,  771 => 282,  763 => 281,  749 => 274,  741 => 273,  727 => 266,  719 => 265,  712 => 261,  699 => 255,  691 => 254,  677 => 247,  669 => 246,  655 => 239,  647 => 238,  640 => 234,  627 => 228,  619 => 227,  605 => 220,  597 => 219,  583 => 212,  575 => 211,  568 => 207,  560 => 202,  556 => 201,  552 => 200,  548 => 199,  541 => 195,  535 => 191,  529 => 189,  527 => 188,  523 => 187,  519 => 185,  513 => 183,  511 => 182,  507 => 181,  502 => 179,  490 => 174,  482 => 173,  476 => 170,  464 => 165,  456 => 164,  450 => 161,  438 => 156,  430 => 155,  424 => 152,  412 => 147,  404 => 146,  398 => 143,  393 => 141,  379 => 134,  371 => 133,  363 => 132,  357 => 129,  345 => 124,  337 => 123,  331 => 120,  319 => 115,  311 => 114,  305 => 111,  293 => 106,  285 => 105,  279 => 102,  267 => 93,  261 => 89,  248 => 87,  244 => 86,  238 => 83,  228 => 78,  222 => 77,  214 => 74,  209 => 71,  203 => 69,  201 => 68,  195 => 67,  188 => 65,  176 => 60,  168 => 59,  160 => 56,  151 => 50,  144 => 46,  140 => 45,  136 => 44,  132 => 43,  128 => 42,  123 => 40,  118 => 38,  112 => 35,  106 => 32,  102 => 30,  94 => 26,  91 => 25,  83 => 21,  81 => 20,  75 => 16,  64 => 14,  60 => 13,  55 => 11,  46 => 9,  40 => 8,  34 => 7,  28 => 6,  19 => 1,);
    }
}
/* {{ header }}{{ column_left }}*/
/* <div id="content">*/
/*   	<div class="page-header">*/
/*     	<div class="container-fluid">*/
/*       		<div class="pull-right">*/
/*         		<button type="submit" form="form-so-onepagecheckout" data-toggle="tooltip" title="{{ button_save }}" class="btn btn-primary"><i class="fa fa-save"></i> {{ button_save }}</button>*/
/*         		<a class="btn btn-success1" onclick="$('#action').val('save_edit');$('#form-so-onepagecheckout').submit();" data-toggle="tooltip" title="{{ objlang.get('entry_button_save_and_edit') }}" ><i class="fa fa-pencil-square-o"></i> {{ objlang.get('entry_button_save_and_edit') }}</a>*/
/* 				<a class="btn btn-info" onclick="$('#action').val('save_new');$('#form-so-onepagecheckout').submit();" data-toggle="tooltip" title="{{ objlang.get('entry_button_save_and_new') }}" ><i class="fa fa-book"></i>  {{ objlang.get('entry_button_save_and_new') }}</a>*/
/*         		<a href="{{ cancel }}" data-toggle="tooltip" title="{{ button_cancel }}" class="btn btn-danger"><i class="fa fa-reply"></i> {{ button_cancel }}</a>*/
/*         	</div>*/
/* 			<h1>{{ heading_title_so }}</h1>*/
/*       		<ul class="breadcrumb">*/
/*         		{% for breadcrumb in breadcrumbs %}*/
/*         			<li><a href="{{ breadcrumb.href }}">{{ breadcrumb.text }}</a></li>*/
/*         		{% endfor %}*/
/*       		</ul>*/
/*     	</div>*/
/*   	</div>*/
/*   	<div class="container-fluid">*/
/*     	{% if error_warning %}*/
/*     		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> {{ error_warning }}*/
/*       			<button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*     		</div>*/
/*     	{% endif %}*/
/*     	{% if success %}*/
/*     		<div class="alert alert-success"><i class="fa fa-check-circle"></i> {{ success }}*/
/*       			<button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*     		</div>*/
/*     	{% endif %}*/
/*     	<div class="panel panel-default">*/
/*       		<div class="panel-heading">*/
/*         		<h3 class="panel-title"><i class="fa fa-pencil"></i> {{ text_edit }}</h3>*/
/*       		</div>*/
/*       		<div class="panel-body">*/
/* 	   			<form action="{{ action }}" method="post" enctype="multipart/form-data" id="form-so-onepagecheckout" class="form-horizontal">*/
/*     				<input type="hidden" name="action" id="action" value=""/>        */
/* 					<ul class="nav nav-tabs" id="CheckoutTab">*/
/* 						<li class="active"><a href="#tab-general" data-toggle="tab"><i class="fa fa-cog fa-fw"></i> {{ tab_general }}</a></li>*/
/* 						<li class="dropdown">*/
/* 							<a href="#" data-toggle="dropdown"><i class="fa fa-eye"></i> {{ tab_layout_setting }}<span class="caret"></span></a>*/
/* 							<ul class="dropdown-menu">*/
/* 								<li><a href="#tab-layout" data-toggle="tab"><i class="fa fa-eye"></i> {{ tab_account_setting }}</a></li>*/
/* 								<li><a href="#tab-shopping-cart-setting" data-toggle="tab"><i class="fa fa-shopping-cart"></i> {{ tab_shipping_cart }}</a></li>*/
/* 								<li><a href="#tab-delivery_method-setting" data-toggle="tab"><i class="fa fa-truck" aria-hidden="true"></i> {{ tab_delivery_methods }}</a></li>*/
/* 								<li><a href="#tab-payment_method-setting" data-toggle="tab"><i class="fa fa-credit-card" aria-hidden="true"></i> {{ tab_payment_methods }}</a></li>*/
/* 								<li><a href="#tab-confirm_order-setting" data-toggle="tab"><i class="fa fa-cart-plus" aria-hidden="true"></i> {{ tab_confirm_order }}</a></li>*/
/* 							</ul>*/
/* 						</li>*/
/* 						<li class="">*/
/* 							<a href="#tab-help" data-toggle="tab"><i class="fa fa-question-circle"></i> {{ tab_help }}</a>*/
/* 						</li>*/
/* 					</ul>*/
/* 					<div class="tab-content">*/
/* 						<div class="tab-pane active in" id="tab-general">*/
/* 							<div class="form-group">*/
/* 								<label class="col-sm-2 control-label" for="input-status"><span data-toggle="tooltip" title="" data-original-title="{{ entry_status_title }}">{{ entry_status }}</span></label>*/
/* 								<div class="col-sm-3">*/
/* 									<div class="btn-group" data-toggle="buttons">*/
/* 										<label class="btn btn-success btn-sm  {{ so_onepagecheckout_enabled == 1 ? 'active' : '' }}"><input type="radio"  name="so_onepagecheckout[so_onepagecheckout_general][so_onepagecheckout_enabled]"  {{ so_onepagecheckout_enabled == 1 ? 'checked="checked"' : '' }}  value="1" autocomplete="off">{{ text_enabled }}</label>*/
/* 										<label class="btn btn-success btn-sm {{ so_onepagecheckout_enabled == 0 ? 'active' : '' }}"><input type="radio"  name="so_onepagecheckout[so_onepagecheckout_general][so_onepagecheckout_enabled]" {{ so_onepagecheckout_enabled == 0 ? 'checked="checked"' : '' }} value="0" autocomplete="off">{{ text_disabled }}</label>*/
/* 									</div>*/
/* 				  				</div>*/
/* 							</div>*/
/* 							<div class="form-group">*/
/* 								<label class="col-sm-2 control-label" for="input-name"><span data-toggle="tooltip" title="" data-original-title="{{ entry_name_title }}">{{ entry_name }}</span></label>*/
/* 								<div class="col-sm-3">*/
/* 				  					<input type="text" name="so_onepagecheckout[so_onepagecheckout_general][so_onepagecheckout_name]" value="{{ so_onepagecheckout_name }}" placeholder="{{ entry_name }}" id="input-name" class="form-control" />*/
/* 				  					{% if error_name %}*/
/* 				  						<div class="text-danger">{{ error_name }}</div>*/
/* 				  					{% endif %}*/
/* 								</div>*/
/* 							</div>*/
/* 							<div class="form-group">*/
/* 								<label class="col-sm-2 control-label" for="input-status"><span data-toggle="tooltip" title="" data-original-title="{{ text_layout_title }}">{{ text_layout }}</span></label>*/
/* 								<div class="col-sm-3">*/
/* 									<select name="so_onepagecheckout[so_onepagecheckout_general][so_onepagecheckout_layout]" id="input-country" class="form-control">*/
/* 			                			<option value="1" {{ so_onepagecheckout_layout == 1 ? 'selected="selected"' : '' }}>{{ text_layout_one }}</option>*/
/* 			                			<option value="2" {{ so_onepagecheckout_layout == 2 ? 'selected="selected"' : '' }}>{{ text_layout_two }}</option>*/
/* 			                		</select>*/
/* 				  				</div>*/
/* 							</div>*/
/* 							<div class="form-group">*/
/* 			                	<label class="col-sm-2 control-label" for="input-country"><span data-toggle="tooltip" title="" data-original-title="Set Default Country">{{ text_default_country }}</span></label>*/
/* 			                	<div class="col-sm-3">*/
/* 			                		<select name="so_onepagecheckout[so_onepagecheckout_general][so_onepagecheckout_country_id]" id="input-country" class="form-control">*/
/* 			                			{% for country in countries %}*/
/* 			                				<option value="{{ country.country_id }}" {{ country.country_id == so_onepagecheckout_country_id ? 'selected="selected"' : '' }}>{{ country.name }}</option>*/
/* 			                			{% endfor %}*/
/* 			                		</select>*/
/* 			                	</div>*/
/* 			                </div>*/
/* 							<div class="form-group">*/
/* 								<label class="col-sm-2 control-label" for="input-zone"><span data-toggle="tooltip" title="" data-original-title="Set Default zone">{{ text_default_zone }}</span></label>*/
/* 								<div class="col-sm-3">*/
/* 									<select name="so_onepagecheckout[so_onepagecheckout_general][so_onepagecheckout_zone_id]" id="input-zone" class="form-control">*/
/* 									</select>*/
/* 								</div>*/
/* 							</div>*/
/* 						</div>*/
/* 						<div class="tab-pane" id="tab-layout">*/
/* 							<div class="form-group">*/
/* 								<label class="col-sm-2 control-label" for="input-status"><span  data-toggle="tooltip" title="Disabled Register Checkout ">{{ text_register_account }}</span></label>*/
/* 								<div class="col-sm-10">*/
/* 									<div class="btn-group" data-toggle="buttons">*/
/* 										<label class="btn btn-success btn-sm button-account-type {{ so_onepagecheckout_register_checkout == 1 ? 'active' : '' }}"><input type="radio" name="so_onepagecheckout[so_onepagecheckout_layout_setting][so_onepagecheckout_register_checkout]" {{ so_onepagecheckout_register_checkout == 1 ? 'checked="checked"' : '' }} value="1" autocomplete="off">{{ text_yes }}</label>*/
/* 										<label class="btn btn-success btn-sm button-account-type {{ so_onepagecheckout_register_checkout == 0 ? 'active' : '' }}"><input type="radio" name="so_onepagecheckout[so_onepagecheckout_layout_setting][so_onepagecheckout_register_checkout]" {{ so_onepagecheckout_register_checkout == 0 ? 'checked="checked"' : '' }} value="0" autocomplete="off">{{ text_no }}</label>*/
/* 									</div>*/
/* 								</div>*/
/* 							</div>*/
/* 							<div class="form-group">*/
/* 								<label class="col-sm-2 control-label" for="input-status"><span  data-toggle="tooltip" title="Disabled Guest Checkout ">{{ text_guest_checkout }}</span></label>*/
/* 								<div class="col-sm-10">*/
/* 									<div class="btn-group" data-toggle="buttons">*/
/* 										<label class="btn btn-success btn-sm {{ so_onepagecheckout_guest_checkout == 1 ? 'active' : '' }}"><input type="radio"  name="so_onepagecheckout[so_onepagecheckout_layout_setting][so_onepagecheckout_guest_checkout]" {{ so_onepagecheckout_guest_checkout == 1 ? 'checked="checked"' : '' }} value="1" autocomplete="off">{{ text_yes }}</label>*/
/* 										<label class="btn btn-success btn-sm {{ so_onepagecheckout_guest_checkout == 0 ? 'active' : '' }}"><input type="radio" name="so_onepagecheckout[so_onepagecheckout_layout_setting][so_onepagecheckout_guest_checkout]"  {{ so_onepagecheckout_guest_checkout == 0 ? 'checked="checked"' : '' }} value="0" autocomplete="off">{{ text_no }}</label>*/
/* 									</div>*/
/* 								</div>*/
/* 							</div>*/
/* 							<div class="form-group">*/
/* 								<label class="col-sm-2 control-label" for="input-status"><span  data-toggle="tooltip" title="Disabled Login ">{{ text_login_checkout }}</span></label>*/
/* 								<div class="col-sm-10">*/
/* 									<div class="btn-group" data-toggle="buttons">*/
/* 										<label class="btn btn-success btn-sm {{ so_onepagecheckout_enable_login == 1 ? 'active' : '' }}"><input type="radio"  name="so_onepagecheckout[so_onepagecheckout_layout_setting][so_onepagecheckout_enable_login]" {{ so_onepagecheckout_enable_login == 1 ? 'checked="checked"' : '' }} value="1" autocomplete="off">{{ text_yes }}</label>*/
/* 										<label class="btn btn-success btn-sm {{ so_onepagecheckout_enable_login == 0 ? 'active' : '' }}"><input type="radio"  name="so_onepagecheckout[so_onepagecheckout_layout_setting][so_onepagecheckout_enable_login]" {{ so_onepagecheckout_enable_login == 0 ? 'checked="checked"' : '' }} value="0" autocomplete="off">{{ text_no }}</label>*/
/* 									</div>*/
/* 								</div>*/
/* 							</div>*/
/* 							<div class="form-group">	*/
/* 								<label class="col-sm-2 control-label" for="input-account_open"><span data-toggle="tooltip" title="" data-original-title="Default select section">{{ text_default_display }}</span></label>*/
/* 								<div class="col-sm-3">*/
/* 									<div class="btn-group btn-group-justified" data-toggle="buttons">	*/
/* 										<label class="btn btn-success btn-sm {{ so_onepagecheckout_account_open == 'register' ? 'active' : '' }}"><input type="radio" name="so_onepagecheckout[so_onepagecheckout_layout_setting][so_onepagecheckout_account_open]" {{ so_onepagecheckout_account_open == 'register' ? 'checked="checked"' : '' }} value="register" autocomplete="off" />{{ text_register }}</label>*/
/* 										<label class="btn btn-success btn-sm {{ so_onepagecheckout_account_open == 'guest' ? 'active' : '' }}"><input type="radio" name="so_onepagecheckout[so_onepagecheckout_layout_setting][so_onepagecheckout_account_open]" {{ so_onepagecheckout_account_open == 'guest' ? 'checked="checked"' : '' }} value="guest" autocomplete="off" />{{ text_guest }}</label>*/
/* 										<label class="btn btn-success btn-sm {{ so_onepagecheckout_account_open == 'login' ? 'active' : '' }}"><input type="radio" name="so_onepagecheckout[so_onepagecheckout_layout_setting][so_onepagecheckout_account_open]" {{ so_onepagecheckout_account_open == 'login' ? 'checked="checked"' : '' }} value="login" autocomplete="off" />{{ text_login }}</label>*/
/* 									</div>	 */
/* 								</div>*/
/* 							</div>*/
/* 						</div>*/
/* 						<div id="tab-shopping-cart-setting" class="tab-pane">*/
/* 							<fieldset>*/
/* 								<legend>{{ text_shopping_cart }}</legend>*/
/* 								<div class="form-group">*/
/* 									<label class="col-sm-2 control-label" for="input-status"><span  data-toggle="tooltip" title="Shopping Cart Status" >{{ text_shopping_cart_status }}</span></label>*/
/* 									<div class="col-sm-10">*/
/* 										<div class="btn-group" data-toggle="buttons">*/
/* 											<label class="btn btn-success btn-sm {{ shopping_cart_status == 1 ? 'active' : '' }}"><input type="radio"  name="so_onepagecheckout[so_onepagecheckout_layout_setting][shopping_cart_status]" {{ shopping_cart_status == 1 ? 'checked="checked"' : '' }} value="1" autocomplete="off">{{ text_yes }}</label>*/
/* 											<label class="btn btn-success btn-sm {{ shopping_cart_status == 0 ? 'active' : '' }}"><input type="radio"  name="so_onepagecheckout[so_onepagecheckout_layout_setting][shopping_cart_status]" {{ shopping_cart_status == 0 ? 'checked="checked"' : '' }} value="0" autocomplete="off">{{ text_no }}</label>*/
/* 										</div>*/
/* 									</div>*/
/* 								</div>*/
/* 								<div class="form-group">*/
/* 									<label class="col-sm-2 control-label" for="input-weight"><span  data-toggle="tooltip" title="Show Weight" >{{ text_show_weight }}</span></label>*/
/* 									<div class="col-sm-10">*/
/* 										<div class="btn-group" data-toggle="buttons">*/
/* 											<label class="btn btn-success btn-sm  {{ show_product_weight == 1 ? 'active' : '' }}"><input type="radio"  name="so_onepagecheckout[so_onepagecheckout_layout_setting][show_product_weight]" {{ show_product_weight == 1 ? 'checked="checked"' : '' }} value="1" autocomplete="off">{{ text_yes }}</label>*/
/* 											<label class="btn btn-success btn-sm  {{ show_product_weight == 0 ? 'active' : '' }}"><input type="radio"  name="so_onepagecheckout[so_onepagecheckout_layout_setting][show_product_weight]" {{ show_product_weight == 0 ? 'checked="checked"' : '' }} value="0" autocomplete="off">{{ text_no }}</label>*/
/* 										</div>*/
/* 									</div>*/
/* 								</div>*/
/* 								<div class="form-group">*/
/* 									<label class="col-sm-2 control-label" for="input-qnty_update"><span  data-toggle="tooltip" title="Show product Quantity update on cart">{{ text_quantity_update_permission }}</span></label>*/
/* 									<div class="col-sm-10">*/
/* 										<div class="btn-group" data-toggle="buttons">*/
/* 											<label class="btn btn-success btn-sm  {{ show_product_qnty_update == 1 ? 'active' : '' }}"><input type="radio"  name="so_onepagecheckout[so_onepagecheckout_layout_setting][show_product_qnty_update]" {{ show_product_qnty_update == 1 ? 'checked="checked"' : '' }} value="1" autocomplete="off">{{ text_yes }}</label>*/
/* 											<label class="btn btn-success btn-sm  {{ show_product_qnty_update == 0 ? 'active' : '' }}"><input type="radio"  name="so_onepagecheckout[so_onepagecheckout_layout_setting][show_product_qnty_update]" {{ show_product_qnty_update == 0 ? 'checked="checked"' : '' }} value="0" autocomplete="off">{{ text_no }}</label>*/
/* 										</div>*/
/* 									</div>*/
/* 								</div>*/
/* 								<div class="form-group">*/
/* 									<label class="col-sm-2 control-label" for="input-qnty_update"><span  data-toggle="tooltip" title="Show remove cart button">{{ text_show_removecart }}</span></label>*/
/* 									<div class="col-sm-10">*/
/* 										<div class="btn-group" data-toggle="buttons">*/
/* 											<label class="btn btn-success btn-sm  {{ show_product_removecart == 1 ? 'active' : '' }}"><input type="radio"  name="so_onepagecheckout[so_onepagecheckout_layout_setting][show_product_removecart]" {{ show_product_removecart == 1 ? 'checked="checked"' : '' }} value="1" autocomplete="off">{{ text_yes }}</label>*/
/* 											<label class="btn btn-success btn-sm  {{ show_product_removecart == 0 ? 'active' : '' }}"><input type="radio"  name="so_onepagecheckout[so_onepagecheckout_layout_setting][show_product_removecart]" {{ show_product_removecart == 0 ? 'checked="checked"' : '' }} value="0" autocomplete="off">{{ text_no }}</label>*/
/* 										</div>*/
/* 									</div>*/
/* 								</div>*/
/* 								<div class="form-group">*/
/* 									<label class="col-sm-2 control-label" for="input-image_width">{{ text_product_image_size }}</label>*/
/* 									<div class="col-sm-3">*/
/* 										<input type="text" value="{{ show_product_image_width }}" name="so_onepagecheckout[so_onepagecheckout_layout_setting][show_product_image_width]" class="form-control" placeholder="Width">*/
/* 										{% if error_product_image_width %}*/
/* 					  						<div class="text-danger">{{ error_product_image_width }}</div>*/
/* 					  					{% endif %}*/
/* 									</div>*/
/* 									<div class="col-sm-3">*/
/* 										<input type="text" value="{{ show_product_image_height }}" name="so_onepagecheckout[so_onepagecheckout_layout_setting][show_product_image_height]" class="form-control" placeholder="Height">*/
/* 										{% if error_product_image_height %}*/
/* 					  						<div class="text-danger">{{ error_product_image_height }}</div>*/
/* 					  					{% endif %}*/
/* 									</div>*/
/* 								</div>*/
/* 							</fieldset>*/
/* 							<fieldset>*/
/* 								<legend>{{ text_coupon_voucher }}</legend>*/
/* 								<table class="table table-bordered">*/
/* 									<thead>*/
/* 										<tr>*/
/* 											<td class="text-left">{{ text_show_module_name }}</td>*/
/* 											<th class="text-center">{{ text_login_account }}</th>*/
/* 											<th class="text-center">{{ text_register_account }}</th>*/
/* 											<th class="text-right">{{ text_guest_checkout }}</th>*/
/* 										</tr>*/
/* 									</thead>*/
/* 									<tbody>*/
/* 										<tr>*/
/* 											<td class="text-left">{{ text_coupon }}</td>*/
/* 											<td class="text-center">*/
/* 												<div class="col-sm-12">*/
/* 													<div class="btn-group" data-toggle="buttons">*/
/* 														<label class="btn btn-success btn-sm  {{ coupon_login_status == 1 ? 'active': '' }}"><input type="radio" name="so_onepagecheckout[so_onepagecheckout_layout_setting][coupon_login_status]" {{ coupon_login_status == 1 ? 'checked="checked"': '' }} value="1" autocomplete="off">{{ text_yes }}</label>*/
/* 														<label class="btn btn-success btn-sm  {{ coupon_login_status == 0 ? 'active': '' }}"><input type="radio" name="so_onepagecheckout[so_onepagecheckout_layout_setting][coupon_login_status]" {{ coupon_login_status == 0 ? 'checked="checked"': '' }} value="0" autocomplete="off">{{ text_no }}</label>*/
/* 													</div>*/
/* 												</div>*/
/* 											</td>	*/
/* 											<td class="text-center">*/
/* 												<div class="col-sm-12">*/
/* 													<div class="btn-group" data-toggle="buttons">*/
/* 														<label class="btn btn-success btn-sm  {{ coupon_register_status == 1 ? 'active': '' }}"><input type="radio" name="so_onepagecheckout[so_onepagecheckout_layout_setting][coupon_register_status]" {{ coupon_register_status == 1 ? 'checked="checked"': '' }} value="1" autocomplete="off">{{ text_yes }}</label>*/
/* 														<label class="btn btn-success btn-sm  {{ coupon_register_status == 0 ? 'active': '' }}"><input type="radio" name="so_onepagecheckout[so_onepagecheckout_layout_setting][coupon_register_status]" {{ coupon_register_status == 0 ? 'checked="checked"': '' }} value="0" autocomplete="off">{{ text_no }}</label>*/
/* 													</div>*/
/* 												</div>*/
/* 											</td>*/
/* 											<td class="text-right">*/
/* 												<div class="col-sm-12">*/
/* 													<div class="btn-group" data-toggle="buttons">*/
/* 														<label class="btn btn-success btn-sm  {{ coupon_guest_status == 1 ? 'active': '' }}"><input type="radio" name="so_onepagecheckout[so_onepagecheckout_layout_setting][coupon_guest_status]" {{ coupon_guest_status == 1 ? 'checked="checked"': '' }} value="1" autocomplete="off">{{ text_yes }}</label>*/
/* 														<label class="btn btn-success btn-sm  {{ coupon_guest_status == 0 ? 'active': '' }}"><input type="radio" name="so_onepagecheckout[so_onepagecheckout_layout_setting][coupon_guest_status]" {{ coupon_guest_status == 0 ? 'checked="checked"': '' }} value="0" autocomplete="off">{{ text_no }}</label>*/
/* 													</div>*/
/* 												</div>*/
/* 											</td>*/
/* 										</tr>*/
/* 										<tr>*/
/* 											<td class="text-left">{{ text_reward }}</td>*/
/* 											<td class="text-center">*/
/* 												<div class="col-sm-12">*/
/* 													<div class="btn-group" data-toggle="buttons">*/
/* 														<label class="btn btn-success btn-sm  {{ reward_login_status == 1 ? 'active': '' }}"><input type="radio" name="so_onepagecheckout[so_onepagecheckout_layout_setting][reward_login_status]" {{ reward_login_status == 1 ? 'checked="checked"': '' }} value="1" autocomplete="off">{{ text_yes }}</label>*/
/* 														<label class="btn btn-success btn-sm  {{ reward_login_status == 0 ? 'active': '' }}"><input type="radio" name="so_onepagecheckout[so_onepagecheckout_layout_setting][reward_login_status]" {{ reward_login_status == 0 ? 'checked="checked"': '' }} value="0" autocomplete="off">{{ text_no }}</label>*/
/* 													</div>*/
/* 												</div>*/
/* 											</td>	*/
/* 											<td class="text-center">*/
/* 												<div class="col-sm-12">*/
/* 													<div class="btn-group" data-toggle="buttons">*/
/* 														<label class="btn btn-success btn-sm  {{ reward_register_status == 1 ? 'active': '' }}"><input type="radio" name="so_onepagecheckout[so_onepagecheckout_layout_setting][reward_register_status]" {{ reward_register_status == 1 ? 'checked="checked"': '' }} value="1" autocomplete="off">{{ text_yes }}</label>*/
/* 														<label class="btn btn-success btn-sm  {{ reward_register_status == 0 ? 'active': '' }}"><input type="radio" name="so_onepagecheckout[so_onepagecheckout_layout_setting][reward_register_status]" {{ reward_register_status == 0 ? 'checked="checked"': '' }} value="0" autocomplete="off">{{ text_no }}</label>*/
/* 													</div>*/
/* 												</div>*/
/* 											</td>*/
/* 											<td class="text-right">*/
/* 												<div class="col-sm-12">*/
/* 													<div class="btn-group" data-toggle="buttons">*/
/* 														<label class="btn btn-success btn-sm  {{ reward_guest_status == 1 ? 'active': '' }}"><input type="radio" name="so_onepagecheckout[so_onepagecheckout_layout_setting][reward_guest_status]" {{ reward_guest_status == 1 ? 'checked="checked"': '' }} value="1" autocomplete="off">{{ text_yes }}</label>*/
/* 														<label class="btn btn-success btn-sm  {{ reward_guest_status == 0 ? 'active': '' }}"><input type="radio" name="so_onepagecheckout[so_onepagecheckout_layout_setting][reward_guest_status]" {{ reward_guest_status == 0 ? 'checked="checked"': '' }} value="0" autocomplete="off">{{ text_no }}</label>*/
/* 													</div>*/
/* 												</div>*/
/* 											</td>*/
/* 										</tr>*/
/* 										<tr>*/
/* 											<td class="text-left">{{ text_voucher }}</td>*/
/* 											<td class="text-center">*/
/* 												<div class="col-sm-12">*/
/* 													<div class="btn-group" data-toggle="buttons">*/
/* 														<label class="btn btn-success btn-sm  {{ voucher_login_status == 1 ? 'active': '' }}"><input type="radio" name="so_onepagecheckout[so_onepagecheckout_layout_setting][voucher_login_status]" {{ voucher_login_status == 1 ? 'checked="checked"': '' }} value="1" autocomplete="off">{{ text_yes }}</label>*/
/* 														<label class="btn btn-success btn-sm  {{ voucher_login_status == 0 ? 'active': '' }}"><input type="radio" name="so_onepagecheckout[so_onepagecheckout_layout_setting][voucher_login_status]" {{ voucher_login_status == 0 ? 'checked="checked"': '' }} value="0" autocomplete="off">{{ text_no }}</label>*/
/* 													</div>*/
/* 												</div>*/
/* 											</td>	*/
/* 											<td class="text-center">*/
/* 												<div class="col-sm-12">*/
/* 													<div class="btn-group" data-toggle="buttons">*/
/* 														<label class="btn btn-success btn-sm  {{ voucher_register_status == 1 ? 'active': '' }}"><input type="radio" name="so_onepagecheckout[so_onepagecheckout_layout_setting][voucher_register_status]" {{ voucher_register_status == 1 ? 'checked="checked"': '' }} value="1" autocomplete="off">{{ text_yes }}</label>*/
/* 														<label class="btn btn-success btn-sm  {{ voucher_register_status == 0 ? 'active': '' }}"><input type="radio" name="so_onepagecheckout[so_onepagecheckout_layout_setting][voucher_register_status]" {{ voucher_register_status == 0 ? 'checked="checked"': '' }} value="0" autocomplete="off">{{ text_no }}</label>*/
/* 													</div>*/
/* 												</div>*/
/* 											</td>*/
/* 											<td class="text-right">*/
/* 												<div class="col-sm-12">*/
/* 													<div class="btn-group" data-toggle="buttons">*/
/* 														<label class="btn btn-success btn-sm  {{ voucher_guest_status == 1 ? 'active': '' }}"><input type="radio" name="so_onepagecheckout[so_onepagecheckout_layout_setting][voucher_guest_status]" {{ voucher_guest_status == 1 ? 'checked="checked"': '' }} value="1" autocomplete="off">{{ text_yes }}</label>*/
/* 														<label class="btn btn-success btn-sm  {{ voucher_guest_status == 0 ? 'active': '' }}"><input type="radio" name="so_onepagecheckout[so_onepagecheckout_layout_setting][voucher_guest_status]" {{ voucher_guest_status == 0 ? 'checked="checked"': '' }} value="0" autocomplete="off">{{ text_no }}</label>*/
/* 													</div>*/
/* 												</div>*/
/* 											</td>*/
/* 										</tr>*/
/* 									</tbody>*/
/* 								</table>*/
/* 							</fieldset>*/
/* 						</div>*/
/* 						<div id="tab-delivery_method-setting" class="tab-pane">*/
/* 							{% if shipping_methods|length %}*/
/* 							<fieldset>*/
/* 								<legend>{{ text_delivery_methods }}</legend>*/
/* 								<div class="form-group">*/
/* 									<label class="col-sm-2 control-label" for="input-status"><span  data-toggle="tooltip" title="Status" >{{ text_delivery_methods_status }}</span></label>*/
/* 									<div class="col-sm-10">*/
/* 										<div class="btn-group" data-toggle="buttons">*/
/* 											<label class="btn btn-success btn-sm {{ delivery_method_status == 1 ? 'active' : '' }}"><input type="radio"  name="so_onepagecheckout[so_onepagecheckout_layout_setting][delivery_method_status]" {{ delivery_method_status == 1 ? 'checked="checked"' : '' }} value="1" autocomplete="off">{{ text_yes }}</label>*/
/* 											<label class="btn btn-success btn-sm {{ delivery_method_status == 0 ? 'active' : '' }}"><input type="radio"  name="so_onepagecheckout[so_onepagecheckout_layout_setting][delivery_method_status]" {{ delivery_method_status == 0 ? 'checked="checked"' : '' }} value="0" autocomplete="off">{{ text_no }}</label>*/
/* 										</div>*/
/* 									</div>*/
/* 								</div>*/
/* 								<div class="form-group">*/
/* 									<label class="col-sm-2 control-label" for="input-customer-group"><span data-toggle="tooltip" title="" data-original-title="Default selected Shipping Method">{{ text_shipping_methods }}</span></label>*/
/* 									<div class="col-md-5">*/
/* 										<select class="form-control" name="so_onepagecheckout[so_onepagecheckout_layout_setting][so_onepagecheckout_default_shipping]">*/
/* 											{% for shipping_method in shipping_methods %}*/
/* 										  		<option value="{{ shipping_method.code }}" {{ shipping_method.code == so_onepagecheckout_default_shipping ? 'selected="selected"' : '' }}>{{ shipping_method.title }}</option>*/
/* 										  	{% endfor %}*/
/* 										</select>*/
/* 									</div>*/
/* 								</div>*/
/* 								<table class="table table-bordered">*/
/* 									<thead>*/
/* 										<tr>*/
/* 											<th class="text-left">{{ text_shipping_methods }}</th>*/
/* 											<th class="text-center">{{ text_status }}</th>*/
/* 										</tr>*/
/* 									</thead>*/
/* 									<tbody>*/
/* 										{% for shipping_method in shipping_methods %}*/
/* 										{% set shipping_status = shipping_method.code~'_status' %}*/
/* 										<tr>*/
/* 											<td class="text-left">{{ shipping_method.title }}</td>*/
/* 											<td class="text-center">*/
/* 												<div class="col-md-12">*/
/* 													<div class="form-group">*/
/* 														<div class="btn-group" data-toggle="buttons">*/
/* 															<label class="btn btn-success btn-sm {{ setting_so_onepagecheckout_layout_setting[shipping_status] is defined and setting_so_onepagecheckout_layout_setting[shipping_status] == 1 ? 'active' : '' }}">*/
/* 																<input type="radio" name="so_onepagecheckout[so_onepagecheckout_layout_setting][{{ shipping_method.code }}_status]" {{ setting_so_onepagecheckout_layout_setting[shipping_status] is defined and setting_so_onepagecheckout_layout_setting[shipping_status] == 1 ? 'checked="checked"' : '' }} value="1" autocomplete="off">*/
/* 																{{ text_yes }}*/
/* 															</label>*/
/* 															<label class="btn btn-success btn-sm {{ setting_so_onepagecheckout_layout_setting[shipping_status] is empty or (setting_so_onepagecheckout_layout_setting[shipping_status] is defined and setting_so_onepagecheckout_layout_setting[shipping_status] == 0) ? 'active' : '' }}">*/
/* 																<input type="radio" name="so_onepagecheckout[so_onepagecheckout_layout_setting][{{ shipping_method.code }}_status]" {{ setting_so_onepagecheckout_layout_setting[shipping_status] is defined and setting_so_onepagecheckout_layout_setting[shipping_status] == 0 ? 'checked="checked"' : '' }} value="0" autocomplete="off">*/
/* 																{{ text_no }}*/
/* 															</label>*/
/* 														</div>*/
/* 													</div>*/
/* 												</div>*/
/* 											</td>*/
/* 										</tr>*/
/* 										{% endfor %}*/
/* 									</tbody>*/
/* 								</table>*/
/* 							</fieldset>*/
/* 							{% endif %}*/
/* 						</div>*/
/* 						<div id="tab-payment_method-setting" class="tab-pane">*/
/* 							{% if payment_methods|length %}*/
/* 							<fieldset>*/
/* 								<legend>{{ text_payment_methods }}</legend>*/
/* 								<div class="form-group">*/
/* 									<label class="col-sm-2 control-label" for="input-status"><span  data-toggle="tooltip" title="Status" >{{ text_payment_methods_status }}</span></label>*/
/* 									<div class="col-sm-10">*/
/* 										<div class="btn-group" data-toggle="buttons">*/
/* 											<label class="btn btn-success btn-sm {{ payment_method_status is defined and payment_method_status == 1 ? 'active' : '' }}">*/
/* 												<input type="radio"  name="so_onepagecheckout[so_onepagecheckout_layout_setting][payment_method_status]" {{ payment_method_status is defined and payment_method_status == 1 ? 'checked="checked"' : '' }} value="1" autocomplete="off">*/
/* 												{{ text_yes }}*/
/* 											</label>*/
/* 											<label class="btn btn-success btn-sm {{ payment_method_status is defined and payment_method_status == 0 ? 'active' : '' }}">*/
/* 												<input type="radio"  name="so_onepagecheckout[so_onepagecheckout_layout_setting][payment_method_status]" {{ payment_method_status is defined and payment_method_status == 0 ? 'checked="checked"' : '' }} value="0" autocomplete="off">*/
/* 												{{ text_no }}*/
/* 											</label>*/
/* 										</div>*/
/* 									</div>*/
/* 								</div>*/
/* 								<div class="form-group">*/
/* 									<label class="col-sm-2 control-label" for="input-customer-group"><span data-toggle="tooltip" title="" data-original-title="Default selected Payment Method">{{ text_payment_methods }}</span></label>*/
/* 									<div class="col-md-5">*/
/* 										<select class="form-control" name="so_onepagecheckout[so_onepagecheckout_layout_setting][so_onepagecheckout_default_payment]">*/
/* 											{% for payment_method in payment_methods %}*/
/* 										  		<option value="{{ payment_method.code }}" {{ payment_method.code == so_onepagecheckout_default_payment ? 'selected="selected"' : '' }}>{{ payment_method.title }}</option>*/
/* 										  	{% endfor %}*/
/* 										</select>*/
/* 									</div>*/
/* 								</div>*/
/* 								<table class="table table-bordered">*/
/* 									<thead>*/
/* 										<tr>*/
/* 											<th class="text-left">{{ text_payment_methods }}</th>*/
/* 											<th class="text-center">{{ text_status }}</th>*/
/* 										</tr>*/
/* 									</thead>*/
/* 									<tbody>*/
/* 										{% for payment_method in payment_methods %}*/
/* 										{% set payment_status = payment_method.code~'_status' %}*/
/* 										<tr>*/
/* 											<td class="text-left">{{ payment_method.title }}</td>*/
/* 											<td class="text-center">*/
/* 												<div class="col-md-12">*/
/* 													<div class="form-group">*/
/* 														<div class="btn-group" data-toggle="buttons">*/
/* 															<label class="btn btn-success btn-sm {{ setting_so_onepagecheckout_layout_setting[payment_status] is defined and setting_so_onepagecheckout_layout_setting[payment_status] == 1 ? 'active' : '' }}">*/
/* 																<input type="radio" name="so_onepagecheckout[so_onepagecheckout_layout_setting][{{ payment_method.code }}_status]" {{ setting_so_onepagecheckout_layout_setting[payment_status] is defined and setting_so_onepagecheckout_layout_setting[payment_status] == 1 ? 'checked="checked"' : '' }} value="1" autocomplete="off">*/
/* 																{{ text_yes }}*/
/* 															</label>*/
/* 															<label class="btn btn-success btn-sm {{ setting_so_onepagecheckout_layout_setting[payment_status] is defined and setting_so_onepagecheckout_layout_setting[payment_status] == 0 ? 'active' : '' }}">*/
/* 																<input type="radio" name="so_onepagecheckout[so_onepagecheckout_layout_setting][{{ payment_method.code }}_status]" {{ setting_so_onepagecheckout_layout_setting[payment_status] is defined and setting_so_onepagecheckout_layout_setting[payment_status] == 0 ? 'checked="checked"' : '' }} value="0" autocomplete="off">*/
/* 																{{ text_no }}*/
/* 															</label>*/
/* 														</div>*/
/* 													</div>*/
/* 												</div>*/
/* 											</td>*/
/* 										</tr>*/
/* 										{% endfor %}*/
/* 									</tbody>*/
/* 								</table>*/
/* 							</fieldset>*/
/* 							{% endif %}*/
/* 						</div>*/
/* 						<div id="tab-confirm_order-setting" class="tab-pane">*/
/* 							<fieldset>*/
/* 								<legend>{{ text_confirm_order }}</legend>*/
/* 								<div class="form-group">*/
/* 									<label class="col-sm-2 control-label" for="input-status">{{ text_add_comments }}</label>*/
/* 									<div class="col-sm-10">*/
/* 										<div class="btn-group" data-toggle="buttons">*/
/* 											<label class="btn btn-success btn-sm {{ comment_status == 1 ? 'active' : '' }}"><input type="radio" name="so_onepagecheckout[so_onepagecheckout_layout_setting][comment_status]" {{ comment_status == 1 ? 'checked="checked"' : '' }} value="1" autocomplete="off">{{ text_yes }}</label>*/
/* 											<label class="btn btn-success btn-sm {{ comment_status == 0 ? 'active' : '' }}"><input type="radio" name="so_onepagecheckout[so_onepagecheckout_layout_setting][comment_status]" {{ comment_status == 0 ? 'checked="checked"' : '' }} value="0" autocomplete="off">{{ text_no }}</label>*/
/* 										</div>*/
/* 									</div>*/
/* 								</div>*/
/* 								<div class="form-group">*/
/* 									<label class="col-sm-2 control-label" for="input-status">{{ text_require_comment }}</label>*/
/* 									<div class="col-sm-10">*/
/* 										<div class="btn-group" data-toggle="buttons">*/
/* 											<label class="btn btn-success btn-sm {{ require_comment_status == 1 ? 'active' : '' }}"><input type="radio" name="so_onepagecheckout[so_onepagecheckout_layout_setting][require_comment_status]" {{ require_comment_status == 1 ? 'checked="checked"' : '' }} value="1" autocomplete="off">{{ text_yes }}</label>										*/
/* 											<label class="btn btn-success btn-sm {{ require_comment_status == 0 ? 'active' : '' }}"><input type="radio" name="so_onepagecheckout[so_onepagecheckout_layout_setting][require_comment_status]" {{ require_comment_status == 0 ? 'checked="checked"' : '' }} value="0" autocomplete="off">{{ text_no }}</label>*/
/* 										</div>*/
/* 									</div>*/
/* 								</div>*/
/* 								<div class="form-group">*/
/* 									<label class="col-sm-2 control-label" for="input-status">{{ text_show_newsletter }}</label>*/
/* 									<div class="col-sm-10">*/
/* 										<div class="btn-group" data-toggle="buttons">*/
/* 											<label class="btn btn-success btn-sm {{ show_newsletter == 1 ? 'active' : '' }}"><input type="radio" name="so_onepagecheckout[so_onepagecheckout_layout_setting][show_newsletter]" {{ show_newsletter == 1 ? 'checked="checked"' : '' }} value="1" autocomplete="off">{{ text_yes }}</label>*/
/* 											<label class="btn btn-success btn-sm {{ show_newsletter == 0 ? 'active' : '' }}"><input type="radio" name="so_onepagecheckout[so_onepagecheckout_layout_setting][show_newsletter]" {{ show_newsletter == 0 ? 'checked="checked"' : '' }} value="0" autocomplete="off">{{ text_no }}</label>*/
/* 										</div>*/
/* 									</div>*/
/* 								</div>*/
/* 								<div class="form-group">*/
/* 									<label class="col-sm-2 control-label" for="input-status">{{ text_show_privacy }}</label>*/
/* 									<div class="col-sm-10">*/
/* 										<div class="btn-group" data-toggle="buttons">*/
/* 											<label class="btn btn-success btn-sm {{ show_privacy == 1 ? 'active' : '' }}"><input type="radio" name="so_onepagecheckout[so_onepagecheckout_layout_setting][show_privacy]" {{ show_privacy == 1 ? 'checked="checked"' : '' }} value="1" autocomplete="off">{{ text_yes }}</label>*/
/* 											<label class="btn btn-success btn-sm {{ show_privacy == 0 ? 'active' : '' }}"><input type="radio" name="so_onepagecheckout[so_onepagecheckout_layout_setting][show_privacy]" {{ show_privacy == 0 ? 'checked="checked"' : '' }} value="0" autocomplete="off">{{ text_no }}</label>*/
/* 										</div>*/
/* 									</div>*/
/* 								</div>*/
/* 								<div class="form-group">*/
/* 									<label class="col-sm-2 control-label" for="input-status">{{ text_show_term }}</label>*/
/* 									<div class="col-sm-10">*/
/* 										<div class="btn-group" data-toggle="buttons">*/
/* 											<label class="btn btn-success btn-sm {{ show_term == 1 ? 'active' : '' }}"><input type="radio" name="so_onepagecheckout[so_onepagecheckout_layout_setting][show_term]" {{ show_term == 1 ? 'checked="checked"' : '' }} value="1" autocomplete="off">{{ text_yes }}</label>*/
/* 											<label class="btn btn-success btn-sm {{ show_term == 0 ? 'active' : '' }}"><input type="radio" name="so_onepagecheckout[so_onepagecheckout_layout_setting][show_term]" {{ show_term == 0 ? 'checked="checked"' : '' }} value="0" autocomplete="off">{{ text_no }}</label>*/
/* 										</div>*/
/* 									</div>*/
/* 								</div>*/
/* 							</fieldset>*/
/* 						</div>*/
/* 						<div id="tab-help" class="tab-pane">*/
/* 							<div class="tab-body">*/
/* 								<ol>*/
/* 									<li><b>Setting your FTP credentials</b></li>*/
/* 									<p>In order to set your FTP credentials, navigate to System -> Settings -> Store Settings.</p>*/
/* 									<p><img src="view/javascript/so_onepagecheckout/images/help/1.png" class="img-thumbnail img-responsive"></p>*/
/* 									<p>Then, choose the FTP tab and fill in your FTP credentials. Make sure you check the Yes radio button for the Enable FTP field and click the blue Save button in the upper right corner of the page as shown:</p>*/
/* 									<p><img src="view/javascript/so_onepagecheckout/images/help/2.png" class="img-thumbnail img-responsive"></p>*/
/* 									<p>Now the Extension Installer will know how to access your FTP and you will be able to install extensions with ease.</p>*/
/* 									<li><b>Installing an extension</b></li>*/
/* 									<p>In order to upload an extension, navigate to Extensions -> Extension Installer. Then click the blue Upload button and provide the route to your extension' s"*.ocmod.zip" archive.</p>*/
/* 									<p><img src="view/javascript/so_onepagecheckout/images/help/3.png" class="img-thumbnail img-responsive"></p>*/
/* 									<li><b>Clear Modification</b></li>*/
/* 									<p>In order to upload an extension, navigate to Extensions -> Modifications. Then click the blue Refresh button.</p>*/
/* 									<p><img src="view/javascript/so_onepagecheckout/images/help/4.png" class="img-thumbnail img-responsive"></p>*/
/* 								</ol>*/
/* 							</div>*/
/* 						</div>*/
/* 					</div>*/
/* 				</form>*/
/* 			</div>*/
/*       	</div>*/
/* 	</div>*/
/* </div>*/
/* <script type="text/javascript"><!--*/
/* jQuery(document).ready(function($) {*/
/* 	$('select[name=\'so_onepagecheckout[so_onepagecheckout_general][so_onepagecheckout_country_id]\']').on('change', function() {*/
/* 		$.ajax({*/
/* 			url: 'index.php?route=localisation/country/country&user_token={{ user_token }}&country_id=' + this.value,*/
/* 			dataType: 'json',*/
/* 			beforeSend: function() {*/
/* 				$('select[name=\'so_onepagecheckout[so_onepagecheckout_general][so_onepagecheckout_country_id]\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');*/
/* 			},*/
/* 			complete: function() {*/
/* 				$('.fa-spin').remove();*/
/* 			},*/
/* 			success: function(json) {*/
/* 				html = '<option value=""> --- Please Select --- </option>';*/
/* */
/* 				if (json['zone'] && json['zone'] != '') {*/
/* 					for (i = 0; i < json['zone'].length; i++) {*/
/* 	          			html += '<option value="' + json['zone'][i]['zone_id'] + '"';*/
/* */
/* 						if (json['zone'][i]['zone_id'] == '{{ so_onepagecheckout_zone_id }}') {*/
/* 	            			html += ' selected="selected"';*/
/* 						}*/
/* */
/* 						html += '>' + json['zone'][i]['name'] + '</option>';*/
/* 					}*/
/* 				} else {*/
/* 					html += '<option value="0" selected="selected"> --- None --- </option>';*/
/* 				}*/
/* */
/* 				$('select[name=\'so_onepagecheckout[so_onepagecheckout_general][so_onepagecheckout_zone_id]\']').html(html);*/
/* 				*/
/* 				$('#button-save').prop('disabled', false);*/
/* 			},*/
/* 			error: function(xhr, ajaxOptions, thrownError) {*/
/* 				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/* 			}*/
/* 		});*/
/* 	});*/
/* */
/* 	$('select[name=\'so_onepagecheckout[so_onepagecheckout_general][so_onepagecheckout_country_id]\']').trigger('change');*/
/* 	*/
/* 	// for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line*/
/*     $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {*/
/*         // save the latest tab; use cookies if you like 'em better:*/
/*         localStorage.setItem('lastTab', $(this).attr('href'));*/
/*     });*/
/* */
/*     // go to the latest tab, if it exists:*/
/*     var lastTab = localStorage.getItem('lastTab');*/
/*     if (lastTab) {*/
/*         $('[href="' + lastTab + '"]').tab('show');*/
/*     }*/
/*     */
/* 	$('#CheckoutTab a').click(function(e) {*/
/* 	  e.preventDefault();*/
/* 	  $(this).tab('show');*/
/* 	});*/
/* */
/* 	// store the currently selected tab in the hash value*/
/* 	// $("ul.nav-tabs li a, ul.nav-tabs li ul li a").on("shown.bs.tab", function(e) {*/
/* 	//   var id = $(e.target).attr("href").substr(1);*/
/* 	//   window.location.hash = id;*/
/* 	// });*/
/* */
/* 	// // on load of the page: switch to the currently selected tab*/
/* 	// var hash = window.location.hash;*/
/* 	// $('#CheckoutTab a[href="' + hash + '"]').tab('show');*/
/* });*/
/* //--></script>*/
/* {{ footer }}*/
