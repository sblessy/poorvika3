<?php

/* so-destino/template/product/product.twig */
class __TwigTemplate_378bba38aa50e5acec5b8923110f9c48a9239aad83af4652e3ab78f4e3e2ef8a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo "


\t<link rel=\"stylesheet\" type=\"text/css\" href=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/css/owl.carousel.min.css\" />
<link rel=\"stylesheet\" type=\"text/css\" href=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/css/owl.theme.default.min.css\" />
";
        // line 7
        $this->loadTemplate(((isset($context["theme_directory"]) ? $context["theme_directory"] : null) . "/template/soconfig/breadcrumbs.twig"), "so-destino/template/product/product.twig", 7)->display($context);
        // line 8
        echo "
";
        // line 10
        if ((isset($context["url_asidePosition"]) ? $context["url_asidePosition"] : null)) {
            $context["col_position"] = (isset($context["url_asidePosition"]) ? $context["url_asidePosition"] : null);
        } else {
            // line 11
            $context["col_position"] = $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "catalog_col_position"), "method");
        }
        // line 12
        echo "
";
        // line 13
        if ((isset($context["url_asideType"]) ? $context["url_asideType"] : null)) {
            echo " ";
            $context["col_canvas"] = (isset($context["url_asideType"]) ? $context["url_asideType"] : null);
        } else {
            // line 14
            $context["col_canvas"] = $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "catalog_col_type"), "method");
        }
        // line 15
        echo "
";
        // line 16
        if ((isset($context["url_productGallery"]) ? $context["url_productGallery"] : null)) {
            echo " ";
            $context["productGallery"] = (isset($context["url_productGallery"]) ? $context["url_productGallery"] : null);
        } else {
            // line 17
            $context["productGallery"] = $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "thumbnails_position"), "method");
        }
        // line 18
        echo "
";
        // line 19
        if ((isset($context["url_sidebarsticky"]) ? $context["url_sidebarsticky"] : null)) {
            echo " ";
            $context["sidebar_sticky"] = (isset($context["url_sidebarsticky"]) ? $context["url_sidebarsticky"] : null);
        } else {
            // line 20
            echo " ";
            $context["sidebar_sticky"] = $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "catalog_sidebar_sticky"), "method");
        }
        // line 21
        echo "
";
        // line 22
        $context["desktop_canvas"] = ((((isset($context["col_canvas"]) ? $context["col_canvas"] : null) == "off_canvas")) ? ("desktop-offcanvas") : (""));
        // line 23
        echo "
<div class=\"content-main container product-detail  ";
        // line 24
        echo (isset($context["desktop_canvas"]) ? $context["desktop_canvas"] : null);
        echo "\">
\t<div class=\"row\">
\t\t
\t\t";
        // line 28
        echo "
\t\t";
        // line 29
        if (((isset($context["col_position"]) ? $context["col_position"] : null) == "outside")) {
            // line 30
            echo "\t\t\t";
            echo (isset($context["column_left"]) ? $context["column_left"] : null);
            echo "
\t\t\t
\t\t\t";
            // line 32
            if (((isset($context["col_canvas"]) ? $context["col_canvas"] : null) == "off_canvas")) {
                // line 33
                echo "\t\t\t\t";
                $context["class_pos"] = "col-sm-12";
                // line 34
                echo "\t    \t";
            } elseif (((isset($context["column_left"]) ? $context["column_left"] : null) && (isset($context["column_right"]) ? $context["column_right"] : null))) {
                // line 35
                echo "\t    \t\t";
                $context["class_pos"] = "col-md-6 col-xs-12 fluid-allsidebar";
                // line 36
                echo "\t\t    ";
            } elseif (((isset($context["column_left"]) ? $context["column_left"] : null) || (isset($context["column_right"]) ? $context["column_right"] : null))) {
                // line 37
                echo "\t\t    \t";
                $context["class_pos"] = "col-md-9 col-sm-12 col-xs-12 fluid-sidebar";
                // line 38
                echo "\t\t    ";
            } else {
                // line 39
                echo "\t\t    \t";
                $context["class_pos"] = "col-sm-12";
                // line 40
                echo "\t\t    ";
            }
            // line 41
            echo "\t\t";
        } else {
            // line 42
            echo "\t\t\t";
            $context["class_pos"] = "col-sm-12";
            // line 43
            echo "\t\t";
        }
        // line 44
        echo "\t\t";
        // line 45
        echo "    \t
\t\t<div id=\"content\" class=\"product-view ";
        // line 46
        echo (isset($context["class_pos"]) ? $context["class_pos"] : null);
        echo "\"> 
\t\t
\t\t";
        // line 49
        echo "\t\t";
        if (((isset($context["productGallery"]) ? $context["productGallery"] : null) == "grid")) {
            // line 50
            echo "\t\t\t";
            $context["class_left_gallery"] = "col-md-6 col-sm-12 col-xs-12";
            // line 51
            echo "\t\t\t";
            $context["class_right_gallery"] = "col-md-6 col-sm-12 col-xs-12";
            // line 52
            echo "\t\t";
        } elseif (((isset($context["productGallery"]) ? $context["productGallery"] : null) == "list")) {
            // line 53
            echo "\t\t\t";
            $context["class_left_gallery"] = "col-md-5 col-sm-12 col-xs-12";
            // line 54
            echo "\t\t\t";
            $context["class_right_gallery"] = "col-md-7 col-sm-12 col-xs-12";
            // line 55
            echo "\t\t";
        } elseif (((isset($context["productGallery"]) ? $context["productGallery"] : null) == "left")) {
            // line 56
            echo "\t\t\t";
            $context["class_left_gallery"] = "col-md-6 col-sm-12 col-xs-12";
            // line 57
            echo "\t\t\t";
            $context["class_right_gallery"] = "col-md-6 col-sm-12 col-xs-12";
            // line 58
            echo "\t\t\t";
        } elseif (((isset($context["productGallery"]) ? $context["productGallery"] : null) == "bottom")) {
            // line 59
            echo "\t\t";
            $context["class_left_gallery"] = "col-md-5 col-sm-12 col-xs-12";
            // line 60
            echo "\t\t\t";
            $context["class_right_gallery"] = "col-md-7 col-sm-12 col-xs-12";
            // line 61
            echo "\t\t";
        } else {
            // line 62
            echo "\t\t\t";
            $context["class_left_gallery"] = "col-md-12 col-sm-12 col-xs-12";
            // line 63
            echo "\t\t\t";
            $context["class_right_gallery"] = "col-md-12 col-sm-12 col-xs-12 col-gallery-slider";
            // line 64
            echo "\t\t";
        }
        // line 65
        echo "
\t\t";
        // line 67
        echo "\t\t";
        if (((isset($context["column_left"]) ? $context["column_left"] : null) || (isset($context["column_right"]) ? $context["column_right"] : null))) {
            // line 68
            echo "\t\t\t";
            $context["class_canvas"] = ((((isset($context["col_canvas"]) ? $context["col_canvas"] : null) == "off_canvas")) ? ("") : ("hidden-lg hidden-md"));
            // line 69
            echo "\t\t\t<a href=\"javascript:void(0)\" class=\" open-sidebar ";
            echo (isset($context["class_canvas"]) ? $context["class_canvas"] : null);
            echo "\"><i class=\"fa fa-bars\"></i>";
            echo (isset($context["text_sidebar"]) ? $context["text_sidebar"] : null);
            echo "</a>
\t\t\t<div class=\"sidebar-overlay \"></div>
\t\t";
        }
        // line 72
        echo "

\t\t<div class=\"content-product-mainheader clearfix\"> 
\t\t\t<div class=\"row\">\t
\t\t\t";
        // line 77
        echo "\t\t\t<div class=\"content-product-left  ";
        echo (isset($context["class_left_gallery"]) ? $context["class_left_gallery"] : null);
        echo "\" >
\t\t\t\t";
        // line 78
        if ((isset($context["images"]) ? $context["images"] : null)) {
            // line 79
            echo "\t\t\t\t\t<div class=\"so-loadeding\" ></div>
\t\t\t\t\t";
            // line 81
            echo "\t\t\t\t\t";
            if (((isset($context["productGallery"]) ? $context["productGallery"] : null) == "left")) {
                // line 82
                echo "\t\t\t\t\t \t";
                $this->loadTemplate(((isset($context["theme_directory"]) ? $context["theme_directory"] : null) . "/template/product/gallery/gallery-left.twig"), "so-destino/template/product/product.twig", 82)->display($context);
                // line 83
                echo "
\t\t\t\t\t";
            } elseif ((            // line 84
(isset($context["productGallery"]) ? $context["productGallery"] : null) == "bottom")) {
                // line 85
                echo "\t\t\t\t\t\t";
                $this->loadTemplate(((isset($context["theme_directory"]) ? $context["theme_directory"] : null) . "/template/product/gallery/gallery-bottom.twig"), "so-destino/template/product/product.twig", 85)->display($context);
                // line 86
                echo "
\t\t\t\t\t";
            } elseif ((            // line 87
(isset($context["productGallery"]) ? $context["productGallery"] : null) == "grid")) {
                // line 88
                echo "\t\t\t\t\t\t";
                $this->loadTemplate(((isset($context["theme_directory"]) ? $context["theme_directory"] : null) . "/template/product/gallery/gallery-grid.twig"), "so-destino/template/product/product.twig", 88)->display($context);
                // line 89
                echo "
\t\t\t\t\t";
            } elseif ((            // line 90
(isset($context["productGallery"]) ? $context["productGallery"] : null) == "list")) {
                // line 91
                echo "\t\t\t\t\t\t";
                $this->loadTemplate(((isset($context["theme_directory"]) ? $context["theme_directory"] : null) . "/template/product/gallery/gallery-list.twig"), "so-destino/template/product/product.twig", 91)->display($context);
                // line 92
                echo "
\t\t\t\t\t";
            } elseif ((            // line 93
(isset($context["productGallery"]) ? $context["productGallery"] : null) == "slider")) {
                // line 94
                echo "\t\t\t\t\t\t";
                $this->loadTemplate(((isset($context["theme_directory"]) ? $context["theme_directory"] : null) . "/template/product/gallery/gallery-slider.twig"), "so-destino/template/product/product.twig", 94)->display($context);
                // line 95
                echo "\t\t\t\t\t";
            }
            // line 96
            echo "\t\t\t\t";
        }
        // line 97
        echo "\t\t\t\t
\t\t\t\t\t<div class=\"col-md-12 pl-0 pr-0\">  
\t\t\t<div class=\"cart\">
\t\t\t    <input type=\"button\" value=\"";
        // line 100
        echo (isset($context["button_cart"]) ? $context["button_cart"] : null);
        echo "\" data-loading-text=\"";
        echo (isset($context["text_loading"]) ? $context["text_loading"] : null);
        echo "\" id=\"button-cart\" class=\"btn btn-mega btn-lg btn-product-page\">
\t\t\t </div>
\t\t\t \t<div class=\"buynow\">
\t\t\t    <input type=\"button\" value=\"Buy Now\" id=\"button-cart\" class=\"btn btn-mega btn-lg btn-product-buy\">
\t\t\t </div>
\t\t\t </div>
\t\t\t</div>
\t\t
        \t";
        // line 109
        echo "
\t\t\t";
        // line 111
        echo "\t\t\t<div class=\"content-product-right ";
        echo (isset($context["class_right_gallery"]) ? $context["class_right_gallery"] : null);
        echo "\" itemprop=\"offerDetails\" itemscope itemtype=\"http://schema.org/Product\">
\t\t\t    <div class=\"cont-right\">
\t\t\t    <div class=\"tabs-nav\">
\t\t\t        <ul>
\t\t\t            <li><i class=\"fa fa-eye\"></i>Overview</li>
\t\t\t            <li><i class=\"fa fa-gears\"></i>Specs</li>
\t\t\t            <li><i class=\"fa fa-video-camera\"></i>Video</li>
\t\t\t            <li><i class=\"fa fa-star\"></i>Reviews</li>
\t\t\t            <li><i class=\"fa fa-cube\"></i>Related product</li>
\t\t\t        </ul>
\t\t\t    </div>
\t\t\t    <div class=\"product-breadcrumb\">
\t\t\t    <ul>
                <li><a href=\"http://poorvikabeta.webindia.com/index.php?route=common/home\"><i class=\"fa fa-home\"></i></a></li>
                <li><a href=\"http://poorvikabeta.webindia.com/index.php?route=product/product&amp;product_id=30\">Canon EOS 5D</a></li>
              </ul>
              <span>Add to compare <input type=\"checkbox\" id=\"product-compare\"></span>
              </div>

\t\t\t\t<div class=\"title-product\">
\t\t\t\t\t\t <h1 itemprop=\"name\">";
        // line 131
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
\t\t\t\t\t\t <ul class=\"product-share-links\">
\t\t\t\t\t\t     <li><span>Wishlist</span><a onclick=\"wishlist.add(";
        // line 133
        echo (isset($context["product_id"]) ? $context["product_id"] : null);
        echo ");\"><i class=\"fa fa-heart\"></i></a></li>
\t\t\t\t\t\t     <li><a onclick=\"compare.add(";
        // line 134
        echo (isset($context["product_id"]) ? $context["product_id"] : null);
        echo ");\"><i class=\"fa fa-share-alt\"></i></a></li>
\t\t\t\t\t\t     </ul>
\t\t\t\t\t</div>
\t\t\t\t
\t\t\t\t";
        // line 138
        if ((isset($context["review_status"]) ? $context["review_status"] : null)) {
            // line 139
            echo "\t\t\t\t\t";
            // line 140
            echo "\t\t\t\t\t<div class=\"box-review\"  itemprop=\"aggregateRating\" itemscope itemtype=\"http://schema.org/AggregateRating\">
\t\t\t\t\t\t";
            // line 141
            if ((isset($context["count_reviews"]) ? $context["count_reviews"] : null)) {
                // line 142
                echo "\t\t\t\t\t\t\t\t<meta itemprop=\"ratingValue\" content=\"";
                echo (isset($context["rating"]) ? $context["rating"] : null);
                echo "\">
\t\t\t\t\t\t\t\t<meta itemprop=\"ratingCount\" content=\"";
                // line 143
                echo (isset($context["count_reviews"]) ? $context["count_reviews"] : null);
                echo "\">
\t\t\t\t\t\t\t\t<meta itemprop=\"reviewCount\" content=\"";
                // line 144
                echo (isset($context["count_reviews"]) ? $context["count_reviews"] : null);
                echo "\">
\t\t\t\t\t\t";
            }
            // line 146
            echo "\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"rating\">
\t\t\t\t\t\t    <div class=\"rating-show\">
\t\t\t\t\t\t        <h6>4.5</h6>
\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t<div class=\"rating-box\">
\t\t\t\t\t\t\t";
            // line 152
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(1, 5));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 153
                echo "\t\t\t\t\t\t\t\t";
                if (((isset($context["rating"]) ? $context["rating"] : null) < $context["i"])) {
                    echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
                } else {
                    echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
                }
                // line 154
                echo "\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 155
            echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<a class=\"reviews_button\" href=\"\" onclick=\"\$('a[href=\\'#tab-review\\']').trigger('click'); return false;\">";
            // line 157
            echo (isset($context["reviews"]) ? $context["reviews"] : null);
            echo "</a>
\t\t\t\t\t\t";
            // line 158
            if ($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "product_order"), "method")) {
                // line 159
                echo "\t\t\t\t\t\t\t\t\t<span class=\"order-num\">";
                echo (isset($context["orders"]) ? $context["orders"] : null);
                echo "</span>
\t\t\t\t\t\t";
            }
            // line 161
            echo "\t\t\t\t\t
\t\t\t\t\t</div>
\t\t\t\t\t";
        }
        // line 164
        echo "
\t\t\t\t";
        // line 165
        if ((isset($context["price"]) ? $context["price"] : null)) {
            // line 166
            echo "
                ";
            // line 167
            if ((array_key_exists("text_discount_applied", $context) && (isset($context["text_discount_applied"]) ? $context["text_discount_applied"] : null))) {
                // line 168
                echo "                <h4><span class=\"bg-warning text-warning\">";
                echo (isset($context["text_discount_applied"]) ? $context["text_discount_applied"] : null);
                echo "</span></h4>
                ";
            }
            // line 170
            echo "                
                
\t\t\t\t\t";
            // line 173
            echo "\t\t\t\t\t<div class=\"product_page_price price\" itemprop=\"offers\" itemscope itemtype=\"http://schema.org/Offer\">
\t\t\t\t\t\t";
            // line 174
            if ( !(isset($context["special"]) ? $context["special"] : null)) {
                // line 175
                echo "\t\t\t\t\t\t\t<span class=\"price-new\">
\t\t\t\t\t\t\t\t<span itemprop=\"price\" content=\"";
                // line 176
                echo (isset($context["price_value"]) ? $context["price_value"] : null);
                echo "\" id=\"price-old\"> 
 ";
                // line 177
                if (($this->getAttribute((isset($context["cfp_setting"]) ? $context["cfp_setting"] : null), "module_so_call_for_price_status", array()) && ((isset($context["price_0"]) ? $context["price_0"] : null) <= 0))) {
                    echo " 
 ";
                    // line 178
                    if (($this->getAttribute((isset($context["cfp_setting"]) ? $context["cfp_setting"] : null), "module_so_call_for_price_replace_cart", array(), "any", true, true) && ($this->getAttribute((isset($context["cfp_setting"]) ? $context["cfp_setting"] : null), "module_so_call_for_price_replace_cart", array()) == "0"))) {
                        echo " 
 <a data-fancybox data-type=\"ajax\" data-src=\"";
                        // line 179
                        echo (isset($context["base"]) ? $context["base"] : null);
                        echo "index.php?route=extension/module/so_call_for_price&product_id=";
                        echo $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "product_id", array());
                        echo "\" href=\"javascript:;\" class=\"callforprice\" style=\"color: #ff0000; font-weight: bold; font-size: 18px;\"><i class=\"fa fa-phone\" style=\"font-size: 18px;\"></i> ";
                        echo (isset($context["text_price_0"]) ? $context["text_price_0"] : null);
                        echo "</a> 
 ";
                    }
                    // line 180
                    echo " 
 ";
                } else {
                    // line 181
                    echo " 
  
 ";
                    // line 183
                    if (($this->getAttribute((isset($context["cfp_setting"]) ? $context["cfp_setting"] : null), "module_so_call_for_price_status", array()) && ((isset($context["price_0"]) ? $context["price_0"] : null) <= 0))) {
                        echo " 
 ";
                        // line 184
                        if (($this->getAttribute((isset($context["cfp_setting"]) ? $context["cfp_setting"] : null), "module_so_call_for_price_replace_cart", array(), "any", true, true) && ($this->getAttribute((isset($context["cfp_setting"]) ? $context["cfp_setting"] : null), "module_so_call_for_price_replace_cart", array()) == "0"))) {
                            echo " 
 <a data-fancybox data-type=\"ajax\" data-src=\"";
                            // line 185
                            echo (isset($context["base"]) ? $context["base"] : null);
                            echo "index.php?route=extension/module/so_call_for_price&product_id=";
                            echo $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "product_id", array());
                            echo "\" href=\"javascript:;\" class=\"callforprice\" style=\"color: #ff0000; font-weight: bold;\"><i class=\"fa fa-phone\"></i> ";
                            echo (isset($context["text_price_0"]) ? $context["text_price_0"] : null);
                            echo "</a> 
 ";
                        }
                        // line 186
                        echo " 
 ";
                    } else {
                        // line 187
                        echo " 
 ";
                        // line 188
                        echo (isset($context["price"]) ? $context["price"] : null);
                        echo " 
 ";
                    }
                    // line 189
                    echo " 
  
 ";
                }
                // line 191
                echo " 
 </span>
\t\t\t\t\t\t\t\t<meta itemprop=\"priceCurrency\" content=\"";
                // line 193
                echo (isset($context["currency"]) ? $context["currency"] : null);
                echo "\" />
\t\t\t\t\t\t\t</span>

\t\t\t\t\t\t";
            } else {
                // line 197
                echo "\t\t\t\t\t\t
\t\t\t\t\t\t\t<span class=\"price-new\">
\t\t\t\t\t\t\t\t<span itemprop=\"price\" content=\"";
                // line 199
                echo (isset($context["special_value"]) ? $context["special_value"] : null);
                echo "\" id=\"price-special\">";
                echo (isset($context["special"]) ? $context["special"] : null);
                echo "</span>
\t\t\t\t\t\t\t\t<meta itemprop=\"priceCurrency\" content=\"";
                // line 200
                echo (isset($context["currency"]) ? $context["currency"] : null);
                echo "\" />
\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t   <span class=\"price-old\" id=\"price-old\"> 
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t   </span>
\t\t\t\t\t\t   
\t\t\t\t\t\t";
            }
            // line 207
            echo "\t\t\t\t\t\t
\t\t\t\t\t\t";
            // line 208
            if (((isset($context["special"]) ? $context["special"] : null) && $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "discount_status"), "method"))) {
                echo " 
\t\t\t\t\t\t";
                // line 210
                echo "\t\t\t\t\t\t<span class=\"label-product label-sale\">
\t\t\t\t\t\t\t ";
                // line 211
                echo (isset($context["discount"]) ? $context["discount"] : null);
                echo "
\t\t\t\t\t\t</span>
\t\t\t\t\t\t";
            }
            // line 213
            echo " 

\t\t\t\t\t\t 
 ";
            // line 216
            if ((((isset($context["tax"]) ? $context["tax"] : null) && $this->getAttribute((isset($context["cfp_setting"]) ? $context["cfp_setting"] : null), "module_so_call_for_price_status", array())) && ((isset($context["price_0"]) ? $context["price_0"] : null) > 0))) {
                echo " 
 
\t\t\t\t\t\t\t<div class=\"price-tax\"><span>";
                // line 218
                echo (isset($context["text_tax"]) ? $context["text_tax"] : null);
                echo "</span> <span id=\"price-tax\"> ";
                echo (isset($context["tax"]) ? $context["tax"] : null);
                echo " </span></div>
\t\t\t\t\t\t";
            }
            // line 220
            echo "\t\t\t\t\t 
\t\t\t\t\t</div>
\t\t\t\t\t";
        }
        // line 223
        echo "\t\t\t\t\t

\t\t\t\t";
        // line 225
        if ((isset($context["discounts"]) ? $context["discounts"] : null)) {
            echo " 
\t\t\t\t\t<ul class=\"list-unstyled text-success\">
\t\t\t\t\t";
            // line 227
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["discounts"]) ? $context["discounts"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["discount"]) {
                echo " 
\t\t\t\t\t\t<li><strong>";
                // line 228
                echo $this->getAttribute($context["discount"], "quantity", array());
                echo " ";
                echo (isset($context["text_discount"]) ? $context["text_discount"] : null);
                echo " ";
                echo $this->getAttribute($context["discount"], "price", array());
                echo "</strong> </li>
\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['discount'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 230
            echo "\t\t\t\t\t</ul>
\t\t\t\t";
        }
        // line 231
        echo " \t

\t\t\t\t<div class=\"col-md-6 pl-0\">
\t\t\t\t\t    <div class=\"product-color\">
\t\t\t\t\t        <h3>Color:</h3>
\t\t\t\t\t        <div id=\"color-1\" class=\"product-color-change tabcontent product-color-show\">
\t\t\t\t\t            <span>White</span>
\t\t\t\t\t        </div>
\t\t\t\t\t        <div id=\"color-2\" class=\"product-color-change tabcontent color2\">
\t\t\t\t\t            <span>Black</span>
\t\t\t\t\t        </div>
\t\t\t\t\t        <div id=\"color-3\" class=\"product-color-change tabcontent color3\">
\t\t\t\t\t            <span>Green</span>
\t\t\t\t\t        </div>
\t\t\t\t\t        <div id=\"color-4\" class=\"product-color-change tabcontent color4\">
\t\t\t\t\t            <span>Blue</span>
\t\t\t\t\t        </div>
\t\t\t\t\t        <ul>
\t\t\t\t\t            <li><div class=\"hover-color hover-color1\"><span class=\"product-hover-color\">White</span></div><button class=\"tablinks\"  onclick=\"openCity(event, 'color-1')\"><img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/product-color1.jpg\"></button></li>
\t\t\t\t\t            <li><div class=\"hover-color hover-color2\"><span class=\"product-hover-color\">Black</span></div><button  class=\"tablinks\" onclick=\"openCity(event, 'color-2')\"><img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/product-color2.jpg\"></button></li>
\t\t\t\t\t            <li><div class=\"hover-color hover-color3\"><span class=\"product-hover-color\">Green</span></div><button  class=\"tablinks\" onclick=\"openCity(event, 'color-3')\"><img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/product-color3.jpg\"></button></li>
\t\t\t\t\t            <li><div class=\"hover-color hover-color4\"><span class=\"product-hover-color\">Blue</span></div><button  class=\"tablinks\" onclick=\"openCity(event, 'color-4')\"><img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/product-color4.jpg\"></button></li>
\t\t\t\t\t        </ul>
\t\t\t\t\t    </div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t    <div class=\"product-storage\">
\t\t\t\t\t        <h3>Storage</h3>
\t\t\t\t\t        <ul>
\t\t\t\t\t            <li><strong>8GB+128GB</strong><span>RAM</span><small>Storage</small></li>
\t\t\t\t\t            <li><strong>8GB+256GB</strong><span>RAM</span><small>Storage</small></li>
\t\t\t\t\t            <li><strong>8GB+512GB</strong><span>RAM</span><small>Storage</small></li>
\t\t\t\t\t        </ul>
\t\t\t\t\t    </div>
\t\t\t\t\t</div>
\t\t\t\t";
        // line 266
        if ((isset($context["description_short"]) ? $context["description_short"] : null)) {
            // line 267
            echo "\t\t\t\t\t <div class=\"short_description form-group\" itemprop=\"description\">
\t\t\t\t\t\t<h3>";
            // line 268
            echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "text_overview"), "method");
            echo "</h3>
\t\t                ";
            // line 269
            echo (isset($context["description_short"]) ? $context["description_short"] : null);
            echo "       
\t\t\t\t\t</div>
\t\t\t\t\t";
        }
        // line 272
        echo "\t\t\t\t\t
\t\t\t\t\t<div class=\"Product-offers\">
\t\t\t\t\t    <h3>Available Offers</h3>
\t\t\t\t\t    <ul>
\t\t\t\t\t        <li><i class=\"fa fa-percent\"></i><strong>Bank Offer</strong>flat 30% discount</li>
\t\t\t\t\t        <li><i class=\"fa fa-percent\"></i><strong>Bank Offer</strong>5% offer in Axis bank</li>
\t\t\t\t\t    </ul>
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t\t<div class=\"product-delivery\">
\t\t\t\t\t    <h3>Delivery</h3>
\t\t\t\t\t   
\t\t\t\t\t    <div class=\"pincode\">
\t\t\t\t\t        <span><i class=\"fa fa-map-marker\"></i></span> 
\t\t\t\t\t        <input type=\"number\" id=\"fname\" name=\"fname\">
\t\t\t\t\t        <strong><a href=\"#\">change</strong></a>
\t\t\t\t\t   
\t\t\t\t\t   </div>
\t\t\t\t\t   
\t\t\t\t\t    <ul>
\t\t\t\t\t        
\t\t\t\t\t        <li><img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/deliver-img1.png\"><span>2 hours</span></li>
\t\t\t\t\t        <li><img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/deliver-img2.png\"><span>Regular</span></li>
\t\t\t\t\t        <li><img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/deliver-img3.png\"><span>Pickup@store</span></li>
\t\t\t\t\t   </ul>
\t\t\t\t\t    
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t    <div class=\"product-exchange\">
\t\t\t\t\t        <a href=\"#\">
\t\t\t\t\t        <i class=\"fa fa-exchange\"></i>
\t\t\t\t\t        <span>With exchange<strong>Up to &#x20B9; 14000</strong> <i class=\"fa fa-angle-right\"></i></span>
\t\t\t\t\t        
\t\t\t\t\t        </a>
\t\t\t\t\t    </div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t    <div class=\"product-exchange product-emi\">
\t\t\t\t\t        <a href=\"#\">
\t\t\t\t\t        <i class=\"fa fa-percent\"></i>
\t\t\t\t\t        <span>No cost EMI @<strong> &#x20B9; 4000/month</strong> <i class=\"fa fa-angle-right\"></i></span>
\t\t\t\t\t        
\t\t\t\t\t        </a>
\t\t\t\t\t    </div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-12 pr-0 pl-0\">
\t\t\t\t\t    <div class=\"specs-block\">
\t\t\t\t\t    <div class=\"product-specification\">
\t\t\t\t\t         <h2><img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/product-spec.png\">Specifications</h2>
\t\t\t\t\t    </div>
\t\t\t\t\t    
\t\t\t\t\t<div class=\"product-box-desc\">
\t\t\t\t\t<div class=\"inner-box-desc\">

\t\t\t\t\t\t";
        // line 326
        if ((isset($context["manufacturer"]) ? $context["manufacturer"] : null)) {
            // line 327
            echo "\t\t\t\t\t\t\t        <div class=\"brand ptb-10\" itemprop=\"brand\" itemscope itemtype=\"http://schema.org/Brand\">
\t\t\t\t\t\t\t        <span>";
            // line 328
            echo (isset($context["text_manufacturer"]) ? $context["text_manufacturer"] : null);
            echo " </span><a href=\"";
            echo (isset($context["manufacturers"]) ? $context["manufacturers"] : null);
            echo "\" itemprop=\"url\"><span itemprop=\"name\">";
            echo (isset($context["manufacturer"]) ? $context["manufacturer"] : null);
            echo " </span></a></div>
\t\t\t\t\t\t\t";
        }
        // line 330
        echo "\t\t\t\t\t\t
\t\t\t\t\t\t";
        // line 331
        if ((isset($context["model"]) ? $context["model"] : null)) {
            // line 332
            echo "\t\t\t\t\t\t<div class=\"model ptb-10\"><span>";
            echo (isset($context["text_model"]) ? $context["text_model"] : null);
            echo " </span> ";
            echo (isset($context["model"]) ? $context["model"] : null);
            echo "</div>
\t\t\t\t\t\t";
        }
        // line 334
        echo "\t\t\t\t\t\t
\t\t\t\t\t\t";
        // line 335
        if ((isset($context["reward"]) ? $context["reward"] : null)) {
            // line 336
            echo "\t\t\t\t\t\t\t<div class=\"reward ptb-10\"><span>";
            echo (isset($context["text_reward"]) ? $context["text_reward"] : null);
            echo "</span> ";
            echo (isset($context["reward"]) ? $context["reward"] : null);
            echo "</div>
\t\t\t\t\t\t";
        }
        // line 338
        echo "\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"stock ptb-10\"><span>";
        // line 339
        echo (isset($context["text_stock"]) ? $context["text_stock"] : null);
        echo "</span> <i class=\"fa fa-check-square-o\"></i> ";
        echo (isset($context["stock"]) ? $context["stock"] : null);
        echo "</div>\t
\t\t\t\t\t</div>\t
\t\t\t\t\t
\t\t\t\t\t

\t\t\t\t\t";
        // line 344
        if ($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "product_enablesold"), "method")) {
            // line 345
            echo "\t\t\t\t\t<div class=\"inner-box-sold \">
\t\t\t\t\t\t<div class=\"viewed\"><span>";
            // line 346
            echo (isset($context["text_viewed"]) ? $context["text_viewed"] : null);
            echo "</span> <span class=\"label label-primary\">";
            echo (isset($context["viewed"]) ? $context["viewed"] : null);
            echo "</span></div>\t
\t\t\t\t\t\t";
            // line 347
            if ((isset($context["sold"]) ? $context["sold"] : null)) {
                // line 348
                echo "\t\t\t\t\t\t<div class=\"sold\"><span>";
                echo (isset($context["text_sold_ready"]) ? $context["text_sold_ready"] : null);
                echo "</span> <span class=\"label label-success\"> ";
                echo (isset($context["sold"]) ? $context["sold"] : null);
                echo " </span></div>\t
\t\t\t\t\t\t";
            }
            // line 350
            echo "\t\t\t\t\t</div>\t
\t\t\t\t\t";
        }
        // line 352
        echo "\t\t\t\t\t
\t\t\t\t\t";
        // line 353
        if ($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "product_enablesizechart"), "method")) {
            // line 354
            echo "\t\t\t\t\t\t<a class=\"image-popup-sizechart\" href=\"image/";
            echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "img_sizechart"), "method");
            echo "\" >";
            echo (isset($context["text_size_chart"]) ? $context["text_size_chart"] : null);
            echo " </a>\t
\t\t\t\t    ";
        }
        // line 356
        echo "
\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t
\t\t\t\t</div>
\t\t\t\t
\t\t\t\t\t<div class=\"col-md-12 pr-0 pl-0\">
\t\t\t    <div class=\"product-combo\">
\t\t\t        <div class=\"combo-title\">
\t\t\t            <h2>Buy Together Combo Offer</h2>
\t\t\t        </div>
\t\t\t        <ul>
\t\t\t            <li><div class=\"plus-ico\"><img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/plus-ico.png\"></div>
\t\t\t                <div class=\"combo-checked-box\"><input type=\"checkbox\" id=\"combo-box\"></div>
\t\t\t                <div class=\"combo-offer-img\"><img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/combo-offer-img.jpg\"></div>
\t\t\t                <div class=\"combo-list\"><img src=\"http://poorvikabeta.webindia.com/image/cache/catalog/demo/canon_eos_5d_1-355x470.jpg\"></div>
\t\t\t                <div class=\"combo-content\">
\t\t\t                    <h5>Canon EOS 5D</h5>
\t\t\t                    <strong>4.5
\t\t\t                    ";
        // line 375
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 5));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 376
            echo "\t\t\t\t\t\t\t\t";
            if (((isset($context["rating"]) ? $context["rating"] : null) < $context["i"])) {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            } else {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            }
            // line 377
            echo "\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 378
        echo "\t\t\t\t\t\t\t</strong> 
\t\t\t\t\t\t\t<small class=\"price\">&#8377; 45,000<strike class=\"old-price\">25,000</strike></small>
\t\t\t\t\t\t\t</div>
\t\t\t            
\t\t\t            </li>
\t\t\t             <li><div class=\"plus-ico\"><img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/plus-ico.png\"></div>
\t\t\t                   <div class=\"combo-checked-box\"><input type=\"checkbox\" id=\"combo-box\"></div>
\t\t\t                <div class=\"combo-offer-img\"><img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/combo-offer-img.jpg\"></div>
\t\t\t                <div class=\"combo-list\"><img src=\"http://poorvikabeta.webindia.com/image/cache/catalog/demo/canon_eos_5d_1-355x470.jpg\"></div>
\t\t\t                <div class=\"combo-content\">
\t\t\t                    <h5>Canon EOS 5D</h5>
\t\t\t                    <strong>4.5
\t\t\t                    ";
        // line 390
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 5));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 391
            echo "\t\t\t\t\t\t\t\t";
            if (((isset($context["rating"]) ? $context["rating"] : null) < $context["i"])) {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            } else {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            }
            // line 392
            echo "\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 393
        echo "\t\t\t\t\t\t\t</strong> 
\t\t\t\t\t\t\t<small class=\"price\">&#8377; 45,000<strike class=\"old-price\">25,000</strike></small>
\t\t\t\t\t\t\t</div>
\t\t\t            
\t\t\t            </li>
\t\t\t             <li><div class=\"plus-ico\"><img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/equal-ico.png\"></div>
\t\t\t                   <div class=\"combo-checked-box\"><input type=\"checkbox\" id=\"combo-box\"></div>
\t\t\t                <div class=\"combo-offer-img\"><img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/combo-offer-img.jpg\"></div>
\t\t\t                <div class=\"combo-list\"><img src=\"http://poorvikabeta.webindia.com/image/cache/catalog/demo/canon_eos_5d_1-355x470.jpg\"></div>
\t\t\t                <div class=\"combo-content\">
\t\t\t                    <h5>Canon EOS 5D</h5>
\t\t\t                    <strong>4.5
\t\t\t                    ";
        // line 405
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 5));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 406
            echo "\t\t\t\t\t\t\t\t";
            if (((isset($context["rating"]) ? $context["rating"] : null) < $context["i"])) {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            } else {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            }
            // line 407
            echo "\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 408
        echo "\t\t\t\t\t\t\t</strong> 
\t\t\t\t\t\t\t<small class=\"price\">&#8377; 45,000<strike class=\"old-price\">25,000</strike></small>
\t\t\t\t\t\t\t</div>
\t\t\t            
\t\t\t            </li>
\t\t\t             <li class=\"combo-offer-bg\">
\t\t\t                <h4>Offer Summary</h4>
\t\t\t                <strike class=\"old-price\">25,000</strike>
\t\t\t                \t<small class=\"price\">&#8377; 45,000</small>
\t\t\t               <strong>You save 20,600<span>on 2 items</span></strong>
\t\t\t               
\t\t\t            <div class=\"cart\"><input type=\"button\" value=\"";
        // line 419
        echo (isset($context["button_cart"]) ? $context["button_cart"] : null);
        echo "\" data-loading-text=\"";
        echo (isset($context["text_loading"]) ? $context["text_loading"] : null);
        echo "\" id=\"button-cart\" class=\"btn btn-mega btn-lg btn-offer\"></div>
\t\t\t            </li>
\t\t\t        </ul>
\t\t\t    </div>
\t\t\t</div>

\t\t\t\t";
        // line 426
        echo "\t\t\t\t";
        if (($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "countdown_status"), "method") && (isset($context["special_end_date"]) ? $context["special_end_date"] : null))) {
            // line 427
            echo "\t\t\t\t\t";
            $this->loadTemplate(((isset($context["theme_directory"]) ? $context["theme_directory"] : null) . "/template/soconfig/countdown.twig"), "so-destino/template/product/product.twig", 427)->display(array_merge($context, array("product" => (isset($context["product"]) ? $context["product"] : null), "special_end_date" => (isset($context["special_end_date"]) ? $context["special_end_date"] : null))));
            // line 428
            echo "\t\t\t\t";
        }
        // line 429
        echo "\t\t\t\t
\t\t\t\t
\t\t\t\t<div id=\"product\">\t
\t\t\t\t\t";
        // line 432
        if ((isset($context["options"]) ? $context["options"] : null)) {
            echo " 
\t\t\t\t\t<h3>";
            // line 433
            echo (isset($context["text_option"]) ? $context["text_option"] : null);
            echo "</h3>
 
 ";
            // line 435
            if ((((isset($context["option_data"]) ? $context["option_data"] : null) && $this->getAttribute((isset($context["option_data"]) ? $context["option_data"] : null), "product_option_value", array(), "any", true, true)) && $this->getAttribute((isset($context["option_data"]) ? $context["option_data"] : null), "product_option_value", array()))) {
                echo " 
 <ul id=\"so-colorswatch-selector-";
                // line 436
                echo (isset($context["product_id"]) ? $context["product_id"] : null);
                echo "\" class='so-colorswatch-productpage-icons'> 
 ";
                // line 437
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["option_data"]) ? $context["option_data"] : null), "product_option_value", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["option_value"]) {
                    echo " 
 <li class=\"option-item\"> 
 <a class=\"\" 
 data-product-option-value-id=\"";
                    // line 440
                    echo $this->getAttribute($context["option_value"], "product_option_value_id", array());
                    echo "\" 
 data-option-value-id=\"";
                    // line 441
                    echo $this->getAttribute($context["option_value"], "option_value_id", array());
                    echo "\" 
 data-color-image=\"";
                    // line 442
                    echo $this->getAttribute($context["option_value"], "color_image", array());
                    echo "\" 
 data-color-thumb-image=\"";
                    // line 443
                    echo $this->getAttribute($context["option_value"], "color_thumb_image", array());
                    echo "\" 
 style=\"width: ";
                    // line 444
                    echo (isset($context["width_product_page"]) ? $context["width_product_page"] : null);
                    echo "px; height: ";
                    echo (isset($context["height_product_page"]) ? $context["height_product_page"] : null);
                    echo "px; background-image: url('";
                    echo $this->getAttribute($context["option_value"], "image", array());
                    echo "')\"> 
 </a> 
 </li> 
 ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option_value'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 447
                echo " 
 <li class=\"selected-option\"><span></span></li> 
 </ul> 
 <script type=\"text/javascript\"> 
 var \$window_width = \$(window).width(); 
 var ProductOptionId = '";
                // line 452
                echo (isset($context["product_option_id"]) ? $context["product_option_id"] : null);
                echo "'; 
 var default_image = \$('.large-image img').attr('src'); 
 jQuery(document).ready(function(\$) { 
 \$('#input-option";
                // line 455
                echo (isset($context["product_option_id"]) ? $context["product_option_id"] : null);
                echo "').parent().hide(); 
 
 \$('#input-option";
                // line 457
                echo (isset($context["product_option_id"]) ? $context["product_option_id"] : null);
                echo " option').each(function(){ 
 var text = \$(this).text().replace(/\\s{2,}/g, ' '); 
 var val = \$(this).attr('value'); 
 \$('.so-colorswatch-productpage-icons li a').each(function(index, el){ 
 if(\$(el).data('product-option-value-id')== val){ 
 \$(el).attr('title', text); 
 } 
 }) 
 }) 
 
 ";
                // line 467
                if (((isset($context["colorswatch_type"]) ? $context["colorswatch_type"] : null) == "click")) {
                    echo " 
 \$(document).on('click', '.so-colorswatch-productpage-icons li.option-item', function(e){ 
 e.preventDefault(); 
 var option_value_id = \$(this).children('a').data('product-option-value-id'); 
 var option_id = \$(this).children('a').data('option-value-id'); 
 
 if (\$(this).hasClass('checked')) { 
 \$('.so-colorswatch-productpage-icons li.option-item').removeClass('checked'); 
 \$(this).removeClass('checked'); 
 \$('#input-option";
                    // line 476
                    echo (isset($context["product_option_id"]) ? $context["product_option_id"] : null);
                    echo "').val('').trigger('change'); 
 \$('.so-colorswatch-productpage-icons li.selected-option > span').html(''); 
 
 \$('.large-image img').attr('src', default_image); 
 } 
 else { 
 \$('.so-colorswatch-productpage-icons li.option-item').removeClass('checked'); 
 \$(this).removeClass('checked').addClass('checked'); 
 \$('#input-option";
                    // line 484
                    echo (isset($context["product_option_id"]) ? $context["product_option_id"] : null);
                    echo "').val(option_value_id).trigger('change'); 
 \$('.so-colorswatch-productpage-icons li.selected-option > span').html(\$(this).children('a').attr('title')); 
 
 if (\$(this).children('a').data('color-image') != '') { 
 \$('.large-image img').attr('src', \$(this).children('a').data('color-image')); 
 } 
 else { 
 \$('.large-image img').attr('src', default_image); 
 } 
 
 \$('#thumb-slider a.thumbnail').removeClass('active'); 
 } 
 }) 
 ";
                } else {
                    // line 497
                    echo " 
 if (\$window_width > 1199) { 
 \$('.so-colorswatch-productpage-icons li.option-item').hover(function(e){ 
 e.preventDefault(); 
 var option_value_id = \$(this).children('a').data('product-option-value-id'); 
 var option_id = \$(this).children('a').data('option-value-id'); 
 
 \$('.so-colorswatch-productpage-icons li.option-item').removeClass('checked'); 
 if (\$(this).hasClass('checked')) { 
 \$(this).removeClass('checked'); 
 \$('#input-option";
                    // line 507
                    echo (isset($context["product_option_id"]) ? $context["product_option_id"] : null);
                    echo "').val('').trigger('change'); 
 \$('.large-image img').attr('src', default_image); 
 
 } 
 else { 
 \$(this).removeClass('checked').addClass('checked'); 
 \$('#input-option";
                    // line 513
                    echo (isset($context["product_option_id"]) ? $context["product_option_id"] : null);
                    echo "').val(option_value_id).trigger('change'); 
 \$('.so-colorswatch-productpage-icons li.selected-option > span').html(\$(this).children('a').attr('title')); 
 
 if (\$(this).children('a').data('color-image') != '') { 
 \$('.large-image img').attr('src', \$(this).children('a').data('color-image')); 
 } 
 else { 
 \$('.large-image img').attr('src', default_image); 
 } 
 \$('#thumb-slider a.thumbnail').removeClass('active'); 
 } 
 }); 
 } 
 else { 
 \$(document).on('click', '.so-colorswatch-productpage-icons li.option-item', function(e){ 
 e.preventDefault(); 
 var option_value_id = \$(this).children('a').data('product-option-value-id'); 
 var option_id = \$(this).children('a').data('option-value-id'); 
 
 \$('.so-colorswatch-productpage-icons li.option-item').removeClass('checked'); 
 if (\$(this).hasClass('checked')) { 
 \$(this).removeClass('checked'); 
 \$('#input-option";
                    // line 535
                    echo (isset($context["product_option_id"]) ? $context["product_option_id"] : null);
                    echo "').val('').trigger('change'); 
 \$('.large-image img').attr('src', default_image); 
 
 } 
 else { 
 \$(this).removeClass('checked').addClass('checked'); 
 \$('#input-option";
                    // line 541
                    echo (isset($context["product_option_id"]) ? $context["product_option_id"] : null);
                    echo "').val(option_value_id).trigger('change'); 
 \$('.so-colorswatch-productpage-icons li.selected-option > span').html(\$(this).children('a').attr('title')); 
 
 if (\$(this).children('a').data('color-image') != '') { 
 \$('.large-image img').attr('src', \$(this).children('a').data('color-image')); 
 } 
 else { 
 \$('.large-image img').attr('src', default_image); 
 } 
 \$('#thumb-slider a.thumbnail').removeClass('active'); 
 } 
 }) 
 } 
 ";
                }
                // line 554
                echo " 
 }) 
 </script> 
 ";
            }
            // line 557
            echo " 
 
\t\t\t\t\t";
            // line 559
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["options"]) ? $context["options"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["option"]) {
                // line 560
                echo "\t\t\t\t\t\t
\t\t\t\t\t\t";
                // line 561
                if (($this->getAttribute($context["option"], "type", array()) == "select")) {
                    // line 562
                    echo "\t\t\t\t\t\t<div class=\"form-group";
                    if ($this->getAttribute($context["option"], "required", array())) {
                        echo " required ";
                    }
                    echo "\">
\t\t\t\t\t\t\t<label class=\"control-label\" for=\"input-option";
                    // line 563
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "\">";
                    echo $this->getAttribute($context["option"], "name", array());
                    echo "</label>
\t\t\t\t\t\t\t<select name=\"option[";
                    // line 564
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "]\" id=\"input-option";
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "\" class=\"form-control width50\">
\t\t\t\t\t\t\t\t<option value=\"\">";
                    // line 565
                    echo (isset($context["text_select"]) ? $context["text_select"] : null);
                    echo "</option>
\t\t\t\t\t\t\t";
                    // line 566
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["option"], "product_option_value", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["option_value"]) {
                        // line 567
                        echo "\t\t\t\t\t\t\t\t<option value=\"";
                        echo $this->getAttribute($context["option_value"], "product_option_value_id", array());
                        echo "\">";
                        echo $this->getAttribute($context["option_value"], "name", array());
                        echo "
\t\t\t\t\t\t\t\t";
                        // line 568
                        if ($this->getAttribute($context["option_value"], "price", array())) {
                            // line 569
                            echo "\t\t\t\t\t\t\t\t\t(";
                            echo $this->getAttribute($context["option_value"], "price_prefix", array());
                            echo $this->getAttribute($context["option_value"], "price", array());
                            echo ")
\t\t\t\t\t\t\t\t";
                        }
                        // line 571
                        echo "\t\t\t\t\t\t\t\t</option>
\t\t\t\t\t\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option_value'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 573
                    echo "\t\t\t\t\t\t  </select>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
                }
                // line 576
                echo "\t\t\t\t\t\t
\t\t\t\t\t\t";
                // line 577
                if (($this->getAttribute($context["option"], "type", array()) == "radio")) {
                    // line 578
                    echo "\t\t\t\t\t\t<div class=\"form-group";
                    if ($this->getAttribute($context["option"], "required", array())) {
                        echo " required ";
                    }
                    echo "\">
\t\t\t\t\t\t  \t<label class=\"control-label\">";
                    // line 579
                    echo $this->getAttribute($context["option"], "name", array());
                    echo "</label>
\t\t\t\t\t\t\t<div id=\"input-option";
                    // line 580
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "\">
\t\t\t\t\t\t\t\t";
                    // line 581
                    $context["radio_style"] = $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "radio_style"), "method");
                    // line 582
                    echo "\t\t\t\t\t\t\t\t";
                    $context["radio_type"] = (((isset($context["radio_style"]) ? $context["radio_style"] : null)) ? (" radio-type-button") : (""));
                    // line 583
                    echo "
\t\t\t\t\t\t\t\t";
                    // line 584
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["option"], "product_option_value", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["option_value"]) {
                        echo " 
\t\t\t\t\t\t\t\t";
                        // line 585
                        $context["radio_image"] = (($this->getAttribute($context["option_value"], "image", array())) ? ("option_image") : (""));
                        echo " 
\t\t\t\t\t\t\t\t";
                        // line 586
                        $context["radio_price"] = (((isset($context["radio_style"]) ? $context["radio_style"] : null)) ? (($this->getAttribute($context["option_value"], "price_prefix", array()) . $this->getAttribute($context["option_value"], "price", array()))) : (""));
                        echo " 
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t<div class=\"radio ";
                        // line 588
                        echo ((isset($context["radio_image"]) ? $context["radio_image"] : null) . (isset($context["radio_type"]) ? $context["radio_type"] : null));
                        echo "\">
\t\t\t\t\t\t\t\t\t\t<label>\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<input type=\"radio\" name=\"option[";
                        // line 590
                        echo $this->getAttribute($context["option"], "product_option_id", array());
                        echo "]\" value=\"";
                        echo $this->getAttribute($context["option_value"], "product_option_value_id", array());
                        echo "\" />
\t\t\t\t\t\t\t\t\t\t\t<span class=\"option-content-box\" data-title=\"";
                        // line 591
                        echo $this->getAttribute($context["option_value"], "name", array());
                        echo " ";
                        echo (isset($context["radio_price"]) ? $context["radio_price"] : null);
                        echo "\" data-toggle='tooltip'>
\t\t\t\t\t\t\t\t\t\t\t\t";
                        // line 592
                        if ($this->getAttribute($context["option_value"], "image", array())) {
                            echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"";
                            // line 593
                            echo $this->getAttribute($context["option_value"], "image", array());
                            echo " \" alt=\"";
                            echo $this->getAttribute($context["option_value"], "name", array());
                            echo "  ";
                            echo (isset($context["radio_price"]) ? $context["radio_price"] : null);
                            echo "\" /> 
\t\t\t\t\t\t\t\t\t\t\t\t";
                        }
                        // line 594
                        echo " 
\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"option-name\">";
                        // line 595
                        echo $this->getAttribute($context["option_value"], "name", array());
                        echo " </span>
\t\t\t\t\t\t\t\t\t\t\t\t";
                        // line 596
                        if (($this->getAttribute($context["option_value"], "price", array()) && ((isset($context["radio_style"]) ? $context["radio_style"] : null) != "1"))) {
                            echo " (";
                            echo $this->getAttribute($context["option_value"], "price_prefix", array());
                            echo " ";
                            echo $this->getAttribute($context["option_value"], "price", array());
                            echo " )";
                        }
                        echo " 
\t\t\t\t\t\t\t\t\t\t\t  
\t\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option_value'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 601
                    echo "\t
\t\t\t\t\t\t\t\t 
\t\t\t\t\t\t\t\t";
                    // line 603
                    if ((isset($context["radio_style"]) ? $context["radio_style"] : null)) {
                        echo " 
\t\t\t\t\t\t\t\t<script type=\"text/javascript\">
\t\t\t\t\t\t\t\t\t \$(document).ready(function(){
\t\t\t\t\t\t\t\t\t\t  \$('#input-option";
                        // line 606
                        echo $this->getAttribute($context["option"], "product_option_id", array());
                        echo " ').on('click', 'span', function () {
\t\t\t\t\t\t\t\t\t\t\t   \$('#input-option";
                        // line 607
                        echo $this->getAttribute($context["option"], "product_option_id", array());
                        echo "  span').removeClass(\"active\");
\t\t\t\t\t\t\t\t\t\t\t   \$(this).toggleClass(\"active\");
\t\t\t\t\t\t\t\t\t\t  });
\t\t\t\t\t\t\t\t\t });
\t\t\t\t\t\t\t\t</script>
\t\t\t\t\t\t\t\t";
                    }
                    // line 612
                    echo " 

\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
                }
                // line 617
                echo "
\t\t\t\t\t\t";
                // line 618
                if (($this->getAttribute($context["option"], "type", array()) == "checkbox")) {
                    // line 619
                    echo "\t\t\t\t\t\t<div class=\"form-group";
                    if ($this->getAttribute($context["option"], "required", array())) {
                        echo " required ";
                    }
                    echo "\">
\t\t\t\t\t\t  \t<label class=\"control-label\">";
                    // line 620
                    echo $this->getAttribute($context["option"], "name", array());
                    echo "</label>
\t\t\t\t\t\t  \t<div id=\"input-option";
                    // line 621
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "\">
\t\t\t\t\t\t\t\t";
                    // line 622
                    $context["radio_style"] = $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "radio_style"), "method");
                    // line 623
                    echo "\t\t\t\t\t\t\t\t";
                    $context["radio_type"] = (((isset($context["radio_style"]) ? $context["radio_style"] : null)) ? (" radio-type-button") : (""));
                    // line 624
                    echo "
\t\t\t\t\t\t\t\t";
                    // line 625
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["option"], "product_option_value", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["option_value"]) {
                        echo " 
\t\t\t\t\t\t\t\t";
                        // line 626
                        $context["radio_image"] = (($this->getAttribute($context["option_value"], "image", array())) ? ("option_image") : (""));
                        echo " 
\t\t\t\t\t\t\t\t";
                        // line 627
                        $context["radio_price"] = (((isset($context["radio_style"]) ? $context["radio_style"] : null)) ? (($this->getAttribute($context["option_value"], "price_prefix", array()) . $this->getAttribute($context["option_value"], "price", array()))) : (""));
                        echo " 
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t<div class=\"checkbox  ";
                        // line 629
                        echo ((isset($context["radio_image"]) ? $context["radio_image"] : null) . (isset($context["radio_type"]) ? $context["radio_type"] : null));
                        echo "\">
\t\t\t\t\t\t\t\t\t\t<label>
\t\t\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" name=\"option[";
                        // line 631
                        echo $this->getAttribute($context["option"], "product_option_id", array());
                        echo "][]\" value=\"";
                        echo $this->getAttribute($context["option_value"], "product_option_value_id", array());
                        echo "\" />
\t\t\t\t\t\t\t\t\t\t\t<span class=\"option-content-box\" data-title=\"";
                        // line 632
                        echo $this->getAttribute($context["option_value"], "name", array());
                        echo " ";
                        echo (isset($context["radio_price"]) ? $context["radio_price"] : null);
                        echo "\" data-toggle='tooltip'>
\t\t\t\t\t\t\t\t\t\t\t\t";
                        // line 633
                        if ($this->getAttribute($context["option_value"], "image", array())) {
                            echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"";
                            // line 634
                            echo $this->getAttribute($context["option_value"], "image", array());
                            echo " \" alt=\"";
                            echo $this->getAttribute($context["option_value"], "name", array());
                            echo "  ";
                            echo (isset($context["radio_price"]) ? $context["radio_price"] : null);
                            echo "\" /> 
\t\t\t\t\t\t\t\t\t\t\t\t";
                        }
                        // line 635
                        echo " 

\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"option-name\">";
                        // line 637
                        echo $this->getAttribute($context["option_value"], "name", array());
                        echo " </span>
\t\t\t\t\t\t\t\t\t\t\t\t";
                        // line 638
                        if (($this->getAttribute($context["option_value"], "price", array()) && ((isset($context["radio_style"]) ? $context["radio_style"] : null) != "1"))) {
                            echo " 
\t\t\t\t\t\t\t\t\t\t\t\t\t(";
                            // line 639
                            echo $this->getAttribute($context["option_value"], "price_prefix", array());
                            echo " ";
                            echo $this->getAttribute($context["option_value"], "price", array());
                            echo " )
\t\t\t\t\t\t\t\t\t\t\t\t";
                        }
                        // line 640
                        echo " 
\t\t\t\t\t\t\t\t\t\t\t  
\t\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option_value'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 645
                    echo "\t
\t\t\t\t\t\t\t\t 
\t\t\t\t\t\t\t\t";
                    // line 647
                    if ((isset($context["radio_style"]) ? $context["radio_style"] : null)) {
                        echo " 
\t\t\t\t\t\t\t\t<script type=\"text/javascript\">
\t\t\t\t\t\t\t\t\t \$(document).ready(function(){
\t\t\t\t\t\t\t\t\t\t  \$('#input-option";
                        // line 650
                        echo $this->getAttribute($context["option"], "product_option_id", array());
                        echo " ').on('click', 'span', function () {
\t\t\t\t\t\t\t\t\t\t\t   \$(this).toggleClass(\"active\");
\t\t\t\t\t\t\t\t\t\t  });
\t\t\t\t\t\t\t\t\t });
\t\t\t\t\t\t\t\t</script>
\t\t\t\t\t\t\t\t";
                    }
                    // line 655
                    echo " 

\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
                }
                // line 660
                echo "
\t\t\t\t\t\t";
                // line 661
                if (($this->getAttribute($context["option"], "type", array()) == "text")) {
                    // line 662
                    echo "\t\t\t\t\t\t<div class=\"form-group";
                    if ($this->getAttribute($context["option"], "required", array())) {
                        echo " required ";
                    }
                    echo "\">
\t\t\t\t\t\t  <label class=\"control-label\" for=\"input-option";
                    // line 663
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "\">";
                    echo $this->getAttribute($context["option"], "name", array());
                    echo "</label>
\t\t\t\t\t\t  <input type=\"text\" name=\"option[";
                    // line 664
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "]\" value=\"";
                    echo $this->getAttribute($context["option"], "value", array());
                    echo "\" placeholder=\"";
                    echo $this->getAttribute($context["option"], "name", array());
                    echo "\" id=\"input-option";
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "\" class=\"form-control\" />
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
                }
                // line 667
                echo "
\t\t\t\t\t\t";
                // line 668
                if (($this->getAttribute($context["option"], "type", array()) == "textarea")) {
                    // line 669
                    echo "\t\t\t\t\t\t<div class=\"form-group";
                    if ($this->getAttribute($context["option"], "required", array())) {
                        echo " required ";
                    }
                    echo "\">
\t\t\t\t\t\t  <label class=\"control-label\" for=\"input-option";
                    // line 670
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "\">";
                    echo $this->getAttribute($context["option"], "name", array());
                    echo "</label>
\t\t\t\t\t\t  <textarea name=\"option[";
                    // line 671
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "]\" rows=\"5\" placeholder=\"";
                    echo $this->getAttribute($context["option"], "name", array());
                    echo "\" id=\"input-option";
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "\" class=\"form-control\">";
                    echo $this->getAttribute($context["option"], "value", array());
                    echo "</textarea>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
                }
                // line 674
                echo "
\t\t\t\t\t\t";
                // line 675
                if (($this->getAttribute($context["option"], "type", array()) == "file")) {
                    // line 676
                    echo "\t\t\t\t\t\t<div class=\"form-group";
                    if ($this->getAttribute($context["option"], "required", array())) {
                        echo " required ";
                    }
                    echo "\">
\t\t\t\t\t\t  <label class=\"control-label\">";
                    // line 677
                    echo $this->getAttribute($context["option"], "name", array());
                    echo "</label>
\t\t\t\t\t\t  <button type=\"button\" id=\"button-upload";
                    // line 678
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "\" data-loading-text=\"";
                    echo (isset($context["text_loading"]) ? $context["text_loading"] : null);
                    echo "\" class=\"btn btn-default btn-block\"><i class=\"fa fa-upload\"></i> ";
                    echo (isset($context["button_upload"]) ? $context["button_upload"] : null);
                    echo "</button>
\t\t\t\t\t\t  <input type=\"hidden\" name=\"option[";
                    // line 679
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "]\" value=\"\" id=\"input-option";
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "\" />
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
                }
                // line 682
                echo "
\t\t\t\t\t\t";
                // line 683
                if (($this->getAttribute($context["option"], "type", array()) == "date")) {
                    // line 684
                    echo "\t\t\t\t\t\t<div class=\"form-group";
                    if ($this->getAttribute($context["option"], "required", array())) {
                        echo " required ";
                    }
                    echo "\">
\t\t\t\t\t\t  <label class=\"control-label\" for=\"input-option";
                    // line 685
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "\">";
                    echo $this->getAttribute($context["option"], "name", array());
                    echo "</label>
\t\t\t\t\t\t  <div class=\"input-group date\">
\t\t\t\t\t\t\t<input type=\"text\" name=\"option[";
                    // line 687
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "]\" value=\"";
                    echo $this->getAttribute($context["option"], "value", array());
                    echo "\" data-date-format=\"YYYY-MM-DD\" id=\"input-option";
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t<span class=\"input-group-btn\">
\t\t\t\t\t\t\t<button class=\"btn btn-default\" type=\"button\"><i class=\"fa fa-calendar\"></i></button>
\t\t\t\t\t\t\t</span></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
                }
                // line 693
                echo "
\t\t\t\t\t\t";
                // line 694
                if (($this->getAttribute($context["option"], "type", array()) == "datetime")) {
                    // line 695
                    echo "\t\t\t\t\t\t<div class=\"form-group";
                    if ($this->getAttribute($context["option"], "required", array())) {
                        echo " required ";
                    }
                    echo "\">
\t\t\t\t\t\t  <label class=\"control-label\" for=\"input-option";
                    // line 696
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "\">";
                    echo $this->getAttribute($context["option"], "name", array());
                    echo "</label>
\t\t\t\t\t\t  <div class=\"input-group datetime\">
\t\t\t\t\t\t\t<input type=\"text\" name=\"option[";
                    // line 698
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "]\" value=\"";
                    echo $this->getAttribute($context["option"], "value", array());
                    echo "\" data-date-format=\"YYYY-MM-DD HH:mm\" id=\"input-option";
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t<span class=\"input-group-btn\">
\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-default\"><i class=\"fa fa-calendar\"></i></button>
\t\t\t\t\t\t\t</span></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
                }
                // line 704
                echo "\t\t\t\t\t\t
\t\t\t\t\t\t";
                // line 705
                if (($this->getAttribute($context["option"], "type", array()) == "time")) {
                    // line 706
                    echo "\t\t\t\t\t\t<div class=\"form-group";
                    if ($this->getAttribute($context["option"], "required", array())) {
                        echo " required ";
                    }
                    echo "\">
\t\t\t\t\t\t\t<label class=\"control-label\" for=\"input-option";
                    // line 707
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "\">";
                    echo $this->getAttribute($context["option"], "name", array());
                    echo "</label>
\t\t\t\t\t\t\t<div class=\"input-group time\">
\t\t\t\t\t\t\t<input type=\"text\" name=\"option[";
                    // line 709
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "]\" value=\"";
                    echo $this->getAttribute($context["option"], "value", array());
                    echo "\" data-date-format=\"HH:mm\" id=\"input-option";
                    echo $this->getAttribute($context["option"], "product_option_id", array());
                    echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t<span class=\"input-group-btn\">
\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-default\"><i class=\"fa fa-calendar\"></i></button>
\t\t\t\t\t\t\t</span></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
                }
                // line 715
                echo "\t\t\t\t\t\t
\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 717
            echo "\t\t\t\t\t";
        }
        // line 718
        echo "
\t\t\t\t\t<div class=\"box-cart clearfix form-group\">
\t\t\t\t\t\t";
        // line 720
        if ((isset($context["recurrings"]) ? $context["recurrings"] : null)) {
            // line 721
            echo "\t\t\t\t\t\t<h3>";
            echo (isset($context["text_payment_recurring"]) ? $context["text_payment_recurring"] : null);
            echo "</h3>
\t\t\t\t\t\t<div class=\"form-group required\">
\t\t\t\t\t\t\t<select name=\"recurring_id\" class=\"form-control\">
\t\t\t\t\t\t\t<option value=\"\">";
            // line 724
            echo (isset($context["text_select"]) ? $context["text_select"] : null);
            echo "</option>
\t\t\t\t\t\t\t";
            // line 725
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["recurrings"]) ? $context["recurrings"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["recurring"]) {
                // line 726
                echo "\t\t\t\t\t\t\t<option value=\"";
                echo $this->getAttribute($context["recurring"], "recurring_id", array());
                echo "\">";
                echo $this->getAttribute($context["recurring"], "name", array());
                echo "</option>
\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['recurring'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 728
            echo "\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t  <div class=\"help-block\" id=\"recurring-description\"></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
        }
        // line 732
        echo "\t\t\t\t\t  
\t\t\t\t\t\t<div class=\"form-group box-info-product\">
\t\t\t\t\t\t\t<div class=\"option quantity\">
\t\t\t\t\t\t\t\t<div class=\"input-group quantity-control\">
\t\t\t\t\t\t\t\t\t  <span class=\"input-group-addon product_quantity_down fa fa-minus\"></span>
\t\t\t\t\t\t\t\t\t  <input class=\"form-control\" type=\"text\" name=\"quantity\" value=\"";
        // line 737
        echo (isset($context["minimum"]) ? $context["minimum"] : null);
        echo "\" />
\t\t\t\t\t\t\t\t\t  <input type=\"hidden\" name=\"product_id\" value=\"";
        // line 738
        echo (isset($context["product_id"]) ? $context["product_id"] : null);
        echo "\" />\t\t\t\t\t\t\t\t  
\t\t\t\t\t\t\t\t\t  <span class=\"input-group-addon product_quantity_up fa fa-plus\"></span>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"detail-action\">
\t\t\t\t\t\t\t\t";
        // line 744
        echo "\t\t\t\t\t\t\t\t<div class=\"cart\"> 
 ";
        // line 745
        if (($this->getAttribute((isset($context["cfp_setting"]) ? $context["cfp_setting"] : null), "module_so_call_for_price_status", array()) && ((isset($context["price_0"]) ? $context["price_0"] : null) <= 0))) {
            echo " 
 ";
            // line 746
            if (($this->getAttribute((isset($context["cfp_setting"]) ? $context["cfp_setting"] : null), "module_so_call_for_price_hide_cart", array(), "any", true, true) && ($this->getAttribute((isset($context["cfp_setting"]) ? $context["cfp_setting"] : null), "module_so_call_for_price_hide_cart", array()) == "0"))) {
                echo " 
 ";
                // line 747
                if (($this->getAttribute((isset($context["cfp_setting"]) ? $context["cfp_setting"] : null), "module_so_call_for_price_replace_cart", array(), "any", true, true) && ($this->getAttribute((isset($context["cfp_setting"]) ? $context["cfp_setting"] : null), "module_so_call_for_price_replace_cart", array()) == "1"))) {
                    echo " 
 <input type=\"button\" value=\"";
                    // line 748
                    echo (isset($context["text_price_0"]) ? $context["text_price_0"] : null);
                    echo "\" data-fancybox data-type=\"ajax\" data-src=\"";
                    echo (isset($context["base"]) ? $context["base"] : null);
                    echo "index.php?route=extension/module/so_call_for_price&product_id=";
                    echo $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "product_id", array());
                    echo "\" data-loading-text=\"";
                    echo (isset($context["text_loading"]) ? $context["text_loading"] : null);
                    echo "\" class=\"btn btn-mega btn-lg callforprice\"> 
 ";
                } else {
                    // line 749
                    echo " 
 <input type=\"button\" value=\"";
                    // line 750
                    echo (isset($context["button_cart"]) ? $context["button_cart"] : null);
                    echo "\" data-loading-text=\"";
                    echo (isset($context["text_loading"]) ? $context["text_loading"] : null);
                    echo "\" class=\"btn btn-mega btn-lg\" style=\"cursor: default; background: #eee; color: #ccc; border: 1px solid #eee; text-shadow: none; box-shadow: none;\"> 
 ";
                }
                // line 751
                echo " 
 ";
            } else {
                // line 752
                echo " 
 ";
                // line 753
                if (($this->getAttribute((isset($context["cfp_setting"]) ? $context["cfp_setting"] : null), "module_so_call_for_price_replace_cart", array(), "any", true, true) && ($this->getAttribute((isset($context["cfp_setting"]) ? $context["cfp_setting"] : null), "module_so_call_for_price_replace_cart", array()) == "1"))) {
                    echo " 
 <input type=\"button\" value=\"";
                    // line 754
                    echo (isset($context["text_price_0"]) ? $context["text_price_0"] : null);
                    echo "\" data-fancybox data-type=\"ajax\" data-src=\"";
                    echo (isset($context["base"]) ? $context["base"] : null);
                    echo "index.php?route=extension/module/so_call_for_price&product_id=";
                    echo $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "product_id", array());
                    echo "\" data-loading-text=\"";
                    echo (isset($context["text_loading"]) ? $context["text_loading"] : null);
                    echo "\" class=\"btn btn-mega btn-lg \"> 
 ";
                }
                // line 755
                echo " 
 ";
            }
            // line 756
            echo " 
 ";
        } else {
            // line 757
            echo " 
 <input type=\"button\" value=\"";
            // line 758
            echo (isset($context["button_cart"]) ? $context["button_cart"] : null);
            echo "\" data-loading-text=\"";
            echo (isset($context["text_loading"]) ? $context["text_loading"] : null);
            echo "\" id=\"button-cart\" class=\"btn btn-mega btn-lg\" /> 
 ";
        }
        // line 759
        echo " 
 </div>
\t\t\t\t\t\t\t\t<div class=\"add-to-links wish_comp\">
\t\t\t\t\t\t\t\t\t<ul class=\"blank\">
\t\t\t\t\t\t\t\t\t\t<li class=\"wishlist\">
\t\t\t\t\t\t\t\t\t\t\t<a onclick=\"wishlist.add(";
        // line 764
        echo (isset($context["product_id"]) ? $context["product_id"] : null);
        echo ");\"><i class=\"fa fa-heart\"></i></a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li class=\"compare\">
\t\t\t\t\t\t\t\t\t\t\t<a onclick=\"compare.add(";
        // line 767
        echo (isset($context["product_id"]) ? $context["product_id"] : null);
        echo ");\"><i class=\"fa fa-retweet\"></i></a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t\t\t\t";
        // line 776
        if (((isset($context["minimum"]) ? $context["minimum"] : null) > 1)) {
            // line 777
            echo "\t\t\t\t\t\t\t<div class=\"alert alert-info\"><i class=\"fa fa-info-circle\"></i> ";
            echo (isset($context["text_minimum"]) ? $context["text_minimum"] : null);
            echo "</div>
\t\t\t\t\t\t";
        }
        // line 779
        echo "\t\t\t\t\t</div>

\t\t\t\t\t";
        // line 781
        if (($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "product_page_button"), "method") && $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "product_socialshare"), "method"))) {
            // line 782
            echo "\t\t\t\t\t<div class=\"form-group social-share clearfix\">
\t\t\t\t\t\t";
            // line 783
            echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "decode_entities", array(0 => $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "product_socialshare"), "method")), "method");
            echo "
\t\t\t\t\t</div>
\t\t\t\t\t";
        }
        // line 786
        echo "\t\t\t\t\t<!-- Go to www.addthis.com/dashboard to customize your tools -->
\t\t\t\t\t<script type=\"text/javascript\" src=\"//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-529be2200cc72db5\"></script>
\t\t\t\t\t
\t\t\t\t\t ";
        // line 789
        if ((isset($context["tags"]) ? $context["tags"] : null)) {
            // line 790
            echo "\t\t\t\t\t<div id=\"tab-tags\">
\t\t\t\t\t\t";
            // line 791
            echo (isset($context["text_tags"]) ? $context["text_tags"] : null);
            echo "
\t\t\t\t\t\t";
            // line 792
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(0, twig_length_filter($this->env, (isset($context["tags"]) ? $context["tags"] : null))));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 793
                echo "\t\t\t\t\t\t";
                if (($context["i"] < (twig_length_filter($this->env, (isset($context["tags"]) ? $context["tags"] : null)) - 1))) {
                    echo " <a class=\"btn btn-primary btn-sm\" href=\"";
                    echo $this->getAttribute($this->getAttribute((isset($context["tags"]) ? $context["tags"] : null), $context["i"], array(), "array"), "href", array());
                    echo "\">";
                    echo $this->getAttribute($this->getAttribute((isset($context["tags"]) ? $context["tags"] : null), $context["i"], array(), "array"), "tag", array());
                    echo "</a>
\t\t\t\t\t\t";
                } else {
                    // line 794
                    echo " 
\t\t\t\t\t\t";
                    // line 795
                    if ( !twig_test_empty($this->getAttribute((isset($context["tags"]) ? $context["tags"] : null), $context["i"], array(), "array"))) {
                        // line 796
                        echo "\t\t\t\t\t\t<a class=\"btn btn-primary btn-sm 22\" href=\"";
                        echo $this->getAttribute($this->getAttribute((isset($context["tags"]) ? $context["tags"] : null), $context["i"], array(), "array"), "href", array());
                        echo "\">";
                        echo $this->getAttribute($this->getAttribute((isset($context["tags"]) ? $context["tags"] : null), $context["i"], array(), "array"), "tag", array());
                        echo "</a> ";
                    }
                    // line 797
                    echo "\t\t\t\t\t\t";
                }
                // line 798
                echo "\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo " 
\t\t\t\t\t\t
\t\t\t\t\t 
\t\t\t\t\t</div>
\t\t\t\t\t";
        }
        // line 803
        echo "
\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t</div>
\t\t
\t\t\t";
        // line 809
        echo "\t\t\t</div>
\t\t</div>

\t\t";
        // line 813
        echo "\t\t";
        if ((isset($context["content_top"]) ? $context["content_top"] : null)) {
            // line 814
            echo "\t\t<div class=\"content-product-maintop form-group clearfix\">
\t\t\t";
            // line 815
            echo (isset($context["content_top"]) ? $context["content_top"] : null);
            echo "
\t\t</div>
\t\t";
        }
        // line 818
        echo "\t\t<div class=\"content-product-mainbody clearfix row\">
\t\t\t
\t\t\t";
        // line 820
        if (((isset($context["col_position"]) ? $context["col_position"] : null) == "inside")) {
            // line 821
            echo "\t\t\t";
            // line 822
            echo "\t\t\t\t";
            echo (isset($context["column_left"]) ? $context["column_left"] : null);
            echo "
\t\t\t    ";
            // line 823
            if (((isset($context["col_canvas"]) ? $context["col_canvas"] : null) == "off_canvas")) {
                // line 824
                echo "\t\t\t\t\t";
                $context["class_left"] = "col-sm-12";
                // line 825
                echo "\t\t    \t";
            } elseif (((isset($context["column_left"]) ? $context["column_left"] : null) && (isset($context["column_right"]) ? $context["column_right"] : null))) {
                // line 826
                echo "\t\t    \t\t";
                $context["class_left"] = "col-md-6 col-column3";
                // line 827
                echo "\t\t\t    ";
            } elseif (((isset($context["column_left"]) ? $context["column_left"] : null) || (isset($context["column_right"]) ? $context["column_right"] : null))) {
                // line 828
                echo "\t\t\t    \t";
                $context["class_left"] = "col-md-9 col-sm-12 col-xs-12";
                // line 829
                echo "\t\t\t    ";
            } else {
                // line 830
                echo "\t\t\t    \t";
                $context["class_left"] = "col-sm-12";
                // line 831
                echo "\t\t\t    ";
            }
            // line 832
            echo "\t\t\t";
        } else {
            // line 833
            echo "\t\t\t\t";
            $context["class_left"] = "col-sm-12";
            // line 834
            echo "\t\t\t";
        }
        // line 835
        echo "
\t\t    <div class=\"content-product-content ";
        // line 836
        echo (isset($context["class_left"]) ? $context["class_left"] : null);
        echo "\">
\t\t\t\t<div class=\"content-product-midde clearfix\">
\t\t\t\t\t";
        // line 839
        echo "\t\t\t\t\t";
        $context["related_position"] = ((($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "tabs_position"), "method") == 1)) ? ("vertical-tabs") : (""));
        // line 840
        echo "\t\t\t\t\t";
        $context["tabs_position"] = $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "tabs_position"), "method");
        // line 841
        echo "\t\t\t\t\t";
        $context["showmore"] = $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "product_enableshowmore"), "method");
        // line 842
        echo "\t\t\t\t\t";
        if ((isset($context["showmore"]) ? $context["showmore"] : null)) {
            echo " ";
            $context["class_showmore"] = "showdown";
            // line 843
            echo "\t\t\t\t\t";
        } else {
            echo " ";
            $context["class_showmore"] = "showup";
            // line 844
            echo "\t\t\t\t\t";
        }
        // line 845
        echo "
\t\t\t\t\t<div class=\"producttab \">
\t\t\t\t\t\t<div class=\"tabsslider ";
        // line 847
        echo (isset($context["related_position"]) ? $context["related_position"] : null);
        echo " ";
        if (((isset($context["tabs_position"]) ? $context["tabs_position"] : null) == 1)) {
            echo " ";
            echo "vertical-tabs";
            echo " ";
        } else {
            echo " ";
            echo "horizontal-tabs";
            echo " ";
        }
        echo " col-xs-12\">
\t\t\t\t\t\t\t";
        // line 849
        echo "\t\t\t\t\t\t\t";
        if (((isset($context["tabs_position"]) ? $context["tabs_position"] : null) == 2)) {
            // line 850
            echo "\t\t\t\t\t\t\t<ul class=\"nav nav-tabs font-sn\">
\t\t\t\t\t\t\t\t<li class=\"active\"><a data-toggle=\"tab\" href=\"#tab-description\">";
            // line 851
            echo (isset($context["tab_description"]) ? $context["tab_description"] : null);
            echo "</a></li>
\t\t\t\t\t\t\t\t
\t\t\t\t\t         
\t\t\t\t\t            ";
            // line 854
            if ((isset($context["review_status"]) ? $context["review_status"] : null)) {
                // line 855
                echo "\t\t\t\t\t           \t <li><a href=\"#tab-review\" data-toggle=\"tab\">";
                echo (isset($context["tab_review"]) ? $context["tab_review"] : null);
                echo "</a></li>
\t\t\t\t\t            ";
            }
            // line 857
            echo "\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t";
            // line 858
            if ($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "product_enableshipping"), "method")) {
                // line 859
                echo "\t\t\t\t\t\t\t\t <li><a href=\"#tab-contentshipping\" data-toggle=\"tab\">";
                echo (isset($context["tab_shipping"]) ? $context["tab_shipping"] : null);
                echo "</a></li>
\t\t\t\t\t\t\t\t";
            }
            // line 861
            echo "
\t\t\t\t\t\t\t\t";
            // line 862
            if ((isset($context["product_tabtitle"]) ? $context["product_tabtitle"] : null)) {
                // line 863
                echo "\t\t\t\t\t           \t <li><a href=\"#tab-customhtml\" data-toggle=\"tab\">";
                echo (isset($context["product_tabtitle"]) ? $context["product_tabtitle"] : null);
                echo "</a></li>
\t\t\t\t\t            ";
            }
            // line 865
            echo "
\t\t\t\t\t\t\t\t";
            // line 866
            if ((isset($context["product_video"]) ? $context["product_video"] : null)) {
                // line 867
                echo "\t\t\t\t\t           \t <li><a class=\"thumb-video\" href=\"";
                echo (isset($context["product_video"]) ? $context["product_video"] : null);
                echo "\"><i class=\"fa fa-youtube-play fa-lg\"></i> ";
                echo (isset($context["tab_video"]) ? $context["tab_video"] : null);
                echo "</a></li>
\t\t\t\t\t            ";
            }
            // line 869
            echo "\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</ul>

\t\t\t\t\t\t\t";
            // line 875
            echo "\t\t\t\t\t\t\t";
        } elseif (((isset($context["tabs_position"]) ? $context["tabs_position"] : null) == 1)) {
            // line 876
            echo "\t\t\t\t\t\t\t\t<ul class=\"nav nav-tabs col-lg-3 col-sm-4\">
\t\t\t\t\t\t\t\t<li class=\"active\"><a data-toggle=\"tab\" href=\"#tab-description\">";
            // line 877
            echo (isset($context["tab_description"]) ? $context["tab_description"] : null);
            echo "</a></li>
\t\t\t\t\t\t\t\t
\t\t\t\t\t            ";
            // line 879
            if ((isset($context["review_status"]) ? $context["review_status"] : null)) {
                // line 880
                echo "\t\t\t\t\t           \t <li><a href=\"#tab-review\" data-toggle=\"tab\">";
                echo (isset($context["tab_review"]) ? $context["tab_review"] : null);
                echo "</a></li>
\t\t\t\t\t            ";
            }
            // line 882
            echo "\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t";
            // line 883
            if ($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "product_enableshipping"), "method")) {
                // line 884
                echo "\t\t\t\t\t\t\t\t <li><a href=\"#tab-contentshipping\" data-toggle=\"tab\">";
                echo (isset($context["tab_shipping"]) ? $context["tab_shipping"] : null);
                echo "</a></li>
\t\t\t\t\t\t\t\t";
            }
            // line 886
            echo "
\t\t\t\t\t\t\t\t";
            // line 887
            if ((isset($context["product_tabtitle"]) ? $context["product_tabtitle"] : null)) {
                // line 888
                echo "\t\t\t\t\t           \t <li><a href=\"#tab-customhtml\" data-toggle=\"tab\">";
                echo (isset($context["product_tabtitle"]) ? $context["product_tabtitle"] : null);
                echo "</a></li>
\t\t\t\t\t            ";
            }
            // line 890
            echo "\t\t\t\t\t            
\t\t\t\t\t\t\t\t";
            // line 891
            if ((isset($context["product_video"]) ? $context["product_video"] : null)) {
                // line 892
                echo "\t\t\t\t\t           \t <li><a class=\"thumb-video\" href=\"";
                echo (isset($context["product_video"]) ? $context["product_video"] : null);
                echo "\"><i class=\"fa fa-youtube-play fa-lg\"></i> ";
                echo (isset($context["tab_video"]) ? $context["tab_video"] : null);
                echo "</a></li>
\t\t\t\t\t            ";
            }
            // line 894
            echo "\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t";
        }
        // line 898
        echo "
\t\t\t\t\t\t\t<div class=\"tab-content ";
        // line 899
        if (((isset($context["tabs_position"]) ? $context["tabs_position"] : null) == 1)) {
            echo " ";
            echo "col-lg-9 col-sm-8";
            echo " ";
        }
        echo " col-xs-12\">
\t\t\t\t\t\t\t\t<div class=\"tab-pane active\" id=\"tab-description\">
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t";
        // line 902
        if ((isset($context["attribute_groups"]) ? $context["attribute_groups"] : null)) {
            // line 903
            echo "\t\t\t\t\t\t\t\t\t\t<h3 class=\"product-property-title\" > ";
            echo (isset($context["text_product_specifics"]) ? $context["text_product_specifics"] : null);
            echo "</h3>
\t\t\t\t\t\t              \t<ul class=\"product-property-list util-clearfix\">
\t\t\t\t\t\t\t                ";
            // line 905
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["attribute_groups"]) ? $context["attribute_groups"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["attribute_group"]) {
                // line 906
                echo "\t\t\t\t\t\t\t               
\t\t\t\t\t\t\t                \t
\t\t\t\t\t\t\t\t                ";
                // line 908
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["attribute_group"], "attribute", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["attribute"]) {
                    // line 909
                    echo "\t\t\t\t\t\t\t\t                <li class=\"property-item\">
\t\t\t\t\t\t\t\t                  <span class=\"propery-title\">";
                    // line 910
                    echo $this->getAttribute($context["attribute"], "name", array());
                    echo "</span>
\t\t\t\t\t\t\t\t                  <span class=\"propery-des\">";
                    // line 911
                    echo $this->getAttribute($context["attribute"], "text", array());
                    echo "</span>
\t\t\t\t\t\t\t\t                </li>
\t\t\t\t\t\t\t\t                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attribute'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 914
                echo "\t\t\t\t\t\t\t                 \t
\t\t\t\t\t\t\t                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attribute_group'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 916
            echo "\t\t\t\t\t\t              \t</ul>
\t\t\t\t\t\t            ";
        }
        // line 918
        echo "
\t\t\t\t\t\t            <h3 class=\"product-property-title\" > ";
        // line 919
        echo (isset($context["text_product_description"]) ? $context["text_product_description"] : null);
        echo "</h3>
\t\t\t\t\t\t            <div id=\"collapse-description\" class=\"desc-collapse ";
        // line 920
        echo (isset($context["class_showmore"]) ? $context["class_showmore"] : null);
        echo "\">
\t\t\t\t\t\t\t\t\t\t";
        // line 921
        echo (isset($context["description"]) ? $context["description"] : null);
        echo "
\t\t\t\t\t\t\t\t\t</div>\t

\t\t\t\t\t\t\t\t\t";
        // line 924
        if ((isset($context["showmore"]) ? $context["showmore"] : null)) {
            // line 925
            echo "\t\t\t\t\t\t\t\t\t<div class=\"button-toggle\">
\t\t\t\t\t\t\t\t         <a class=\"showmore\" data-toggle=\"collapse\" href=\"#\" aria-expanded=\"false\" aria-controls=\"collapse-footer\">
\t\t\t\t\t\t\t\t            <span class=\"toggle-more\">";
            // line 927
            echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "show_more"), "method");
            echo " <i class=\"fa fa-angle-down\"></i></span> 
\t\t\t\t\t\t\t\t            <span class=\"toggle-less\">";
            // line 928
            echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "show_less"), "method");
            echo " <i class=\"fa fa-angle-up\"></i></span>           
\t\t\t\t\t\t\t\t\t\t</a>        
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t";
        }
        // line 932
        echo "\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t

\t\t\t\t\t            ";
        // line 935
        if ((isset($context["review_status"]) ? $context["review_status"] : null)) {
            // line 936
            echo "\t\t\t\t\t            <div class=\"tab-pane\" id=\"tab-review\">
\t\t\t\t\t\t            <form class=\"form-horizontal\" id=\"form-review\">
\t\t\t\t\t\t                <div id=\"review\"></div>
\t\t\t\t\t\t                <h3>";
            // line 939
            echo (isset($context["text_write"]) ? $context["text_write"] : null);
            echo "</h3>
\t\t\t\t\t\t                ";
            // line 940
            if ((isset($context["review_guest"]) ? $context["review_guest"] : null)) {
                // line 941
                echo "\t\t\t\t\t\t                <div class=\"form-group required\">
\t\t\t\t\t\t                  <div class=\"col-sm-12\">
\t\t\t\t\t\t                    <label class=\"control-label\" for=\"input-name\">";
                // line 943
                echo (isset($context["entry_name"]) ? $context["entry_name"] : null);
                echo "</label>
\t\t\t\t\t\t                    <input type=\"text\" name=\"name\" value=\"";
                // line 944
                echo (isset($context["customer_name"]) ? $context["customer_name"] : null);
                echo "\" id=\"input-name\" class=\"form-control\" />
\t\t\t\t\t\t                  </div>
\t\t\t\t\t\t                </div>
\t\t\t\t\t\t                <div class=\"form-group required\">
\t\t\t\t\t\t                  <div class=\"col-sm-12\">
\t\t\t\t\t\t                    <label class=\"control-label\" for=\"input-review\">";
                // line 949
                echo (isset($context["entry_review"]) ? $context["entry_review"] : null);
                echo "</label>
\t\t\t\t\t\t                    <textarea name=\"text\" rows=\"5\" id=\"input-review\" class=\"form-control\"></textarea>
\t\t\t\t\t\t                    <div class=\"help-block\">";
                // line 951
                echo (isset($context["text_note"]) ? $context["text_note"] : null);
                echo "</div>
\t\t\t\t\t\t                  </div>
\t\t\t\t\t\t                </div>
\t\t\t\t\t\t                <div class=\"form-group required\">
\t\t\t\t\t\t                  <div class=\"col-sm-12\">
\t\t\t\t\t\t                    <label class=\"control-label\">";
                // line 956
                echo (isset($context["entry_rating"]) ? $context["entry_rating"] : null);
                echo "</label>
\t\t\t\t\t\t                    &nbsp;&nbsp;&nbsp; ";
                // line 957
                echo (isset($context["entry_bad"]) ? $context["entry_bad"] : null);
                echo "&nbsp;
\t\t\t\t\t\t                    <input type=\"radio\" name=\"rating\" value=\"1\" />
\t\t\t\t\t\t                    &nbsp;
\t\t\t\t\t\t                    <input type=\"radio\" name=\"rating\" value=\"2\" />
\t\t\t\t\t\t                    &nbsp;
\t\t\t\t\t\t                    <input type=\"radio\" name=\"rating\" value=\"3\" />
\t\t\t\t\t\t                    &nbsp;
\t\t\t\t\t\t                    <input type=\"radio\" name=\"rating\" value=\"4\" />
\t\t\t\t\t\t                    &nbsp;
\t\t\t\t\t\t                    <input type=\"radio\" name=\"rating\" value=\"5\" />
\t\t\t\t\t\t                    &nbsp;";
                // line 967
                echo (isset($context["entry_good"]) ? $context["entry_good"] : null);
                echo "</div>
\t\t\t\t\t\t                </div>
\t\t\t\t\t\t                ";
                // line 969
                echo (isset($context["captcha"]) ? $context["captcha"] : null);
                echo "
\t\t\t\t\t\t                
\t\t\t\t\t\t                  <div class=\"pull-right\">
\t\t\t\t\t\t                    <button type=\"button\" id=\"button-review\" data-loading-text=\"";
                // line 972
                echo (isset($context["text_loading"]) ? $context["text_loading"] : null);
                echo "\" class=\"btn btn-primary\">";
                echo (isset($context["button_continue"]) ? $context["button_continue"] : null);
                echo "</button>
\t\t\t\t\t\t                  </div>
\t\t\t\t\t\t               
\t\t\t\t\t\t                ";
            } else {
                // line 976
                echo "\t\t\t\t\t\t                ";
                echo (isset($context["text_login"]) ? $context["text_login"] : null);
                echo "
\t\t\t\t\t\t                ";
            }
            // line 978
            echo "\t\t\t\t\t\t            </form>
\t\t\t\t\t            </div>
\t\t\t\t\t            ";
        }
        // line 981
        echo "
\t\t\t\t\t            ";
        // line 982
        if (($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "product_enableshipping"), "method") && $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "product_contentshipping"), "method"))) {
            // line 983
            echo "\t\t\t\t\t\t\t\t<div class=\"tab-pane\" id=\"tab-contentshipping\">
\t\t\t\t\t\t\t\t\t";
            // line 984
            echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "decode_entities", array(0 => $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "product_contentshipping"), "method")), "method");
            echo "
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        // line 987
        echo "
\t\t\t\t\t\t\t\t";
        // line 988
        if ((isset($context["product_tabtitle"]) ? $context["product_tabtitle"] : null)) {
            // line 989
            echo "\t\t\t\t\t\t\t\t<div class=\"tab-pane \" id=\"tab-customhtml\">";
            echo (isset($context["product_tabcontent"]) ? $context["product_tabcontent"] : null);
            echo "</div>
\t\t\t\t\t\t\t\t";
        }
        // line 991
        echo "\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t
\t\t\t\t";
        // line 998
        echo "\t\t\t\t";
        if (((isset($context["products"]) ? $context["products"] : null) && $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "related_status"), "method"))) {
            // line 999
            echo "\t\t\t\t<div class=\"content-product-bottom clearfix\">
\t\t\t\t\t<ul class=\"nav nav-tabs\">
\t\t\t\t\t  <li class=\"active\"><a data-toggle=\"tab\" href=\"#product-related\">";
            // line 1001
            echo (isset($context["text_related"]) ? $context["text_related"] : null);
            echo "</a></li> 
\t\t\t\t\t  <li><a data-toggle=\"tab\" href=\"#product-upsell\">";
            // line 1002
            echo (isset($context["text_upsell"]) ? $context["text_upsell"] : null);
            echo "</a></li>
\t\t\t\t\t</ul>
\t\t\t\t\t<div class=\"tab-content\">
\t\t\t\t\t  \t<div id=\"product-related\" class=\"tab-pane fade in active\">
\t\t\t\t\t\t\t";
            // line 1006
            $this->loadTemplate(((isset($context["theme_directory"]) ? $context["theme_directory"] : null) . "/template/soconfig/related_product.twig"), "so-destino/template/product/product.twig", 1006)->display($context);
            // line 1007
            echo "\t\t\t\t\t  \t</div>
\t\t\t\t\t  \t<div id=\"product-upsell\" class=\"tab-pane fade\">
\t\t\t\t\t  \t\t";
            // line 1010
            echo "\t\t\t\t\t  \t\t";
            echo (isset($context["content_bottom"]) ? $context["content_bottom"] : null);
            echo "
\t\t\t\t\t  \t</div>
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t</div>
\t\t\t\t";
        }
        // line 1016
        echo "
\t\t\t\t
\t\t\t</div>
\t\t\t";
        // line 1020
        echo "\t\t\t";
        if (((isset($context["col_position"]) ? $context["col_position"] : null) == "inside")) {
            echo " ";
            echo (isset($context["column_right"]) ? $context["column_right"] : null);
            echo " ";
        }
        // line 1021
        echo "
\t\t</div>
\t\t<div class=\"content-product-main1\">
\t\t    ";
        // line 1025
        echo "\t\t    <div class=\"col-md-12\">
\t\t        <div class=\"product-combo compare-product\">
\t\t\t        <div class=\"combo-title\">
\t\t\t            <h2>Compate With similar Products</h2>
\t\t\t            <a href=\"\"><i class=\"fa fa-plus\"></i>Add Comparison</a>
\t\t\t        </div>
\t\t\t        <div id=\"collapse-description\" class=\"desc-collapse showdown compare\">
\t\t\t        <ul class=\"\">
\t\t\t            <li><div class=\"free-space\"></div>
\t\t\t            <div class=\"battery-power\">
\t\t\t                <strong>Battery</strong>
\t\t\t            </div>
\t\t\t            <div class=\"external-storage\">
\t\t\t                <strong>Expandable storage</strong>
\t\t\t            </div>
\t\t\t             <div class=\"external-storage\">
\t\t\t                <strong>Expandable storage</strong>
\t\t\t            </div>
\t\t\t            </li>
\t\t\t             <li><a href=\"\">
\t\t\t                <div class=\"combo-list\"><img src=\"http://poorvikabeta.webindia.com/image/cache/catalog/demo/canon_eos_5d_1-355x470.jpg\"></div>
\t\t\t                <div class=\"combo-content\">
\t\t\t                    <h5>Canon EOS 5D</h5>
\t\t\t                    <strong>4.5
\t\t\t                    ";
        // line 1049
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 5));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 1050
            echo "\t\t\t\t\t\t\t\t";
            if (((isset($context["rating"]) ? $context["rating"] : null) < $context["i"])) {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            } else {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            }
            // line 1051
            echo "\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1052
        echo "\t\t\t\t\t\t\t</strong> 
\t\t\t\t\t\t\t<small class=\"price\">&#8377; 45,000<strike class=\"old-price\">25,000</strike></small>
\t\t\t\t\t\t\t</div>
\t\t\t            </a>
\t\t\t             <div class=\"battery-power\">
\t\t\t                <strong>4000 mAH</strong>
\t\t\t            </div>
\t\t\t            <div class=\"external-storage\">
\t\t\t                <strong>128GB</strong>
\t\t\t            </div>
\t\t\t             <div class=\"external-storage\">
\t\t\t                <strong>128GB</strong>
\t\t\t            </div>
\t\t\t            </li>
\t\t\t             <li><a href=\"\">
\t\t\t                <div class=\"combo-list\"><img src=\"http://poorvikabeta.webindia.com/image/cache/catalog/demo/canon_eos_5d_1-355x470.jpg\"></div>
\t\t\t                <div class=\"combo-content\">
\t\t\t                    <h5>Canon EOS 5D</h5>
\t\t\t                    <strong>4.5
\t\t\t                    ";
        // line 1071
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 5));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 1072
            echo "\t\t\t\t\t\t\t\t";
            if (((isset($context["rating"]) ? $context["rating"] : null) < $context["i"])) {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            } else {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            }
            // line 1073
            echo "\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1074
        echo "\t\t\t\t\t\t\t</strong> 
\t\t\t\t\t\t\t<small class=\"price\">&#8377; 45,000<strike class=\"old-price\">25,000</strike></small>
\t\t\t\t\t\t\t</div>
\t\t\t            </a>
\t\t\t              <div class=\"battery-power\">
\t\t\t                <strong>4000 mAH</strong>
\t\t\t            </div>
\t\t\t            <div class=\"external-storage\">
\t\t\t                <strong>128GB</strong>
\t\t\t            </div>
\t\t\t             <div class=\"external-storage\">
\t\t\t                <strong>128GB</strong>
\t\t\t            </div>
\t\t\t            </li>
\t\t\t             <li><a href=\"\">
\t\t\t                <div class=\"combo-list\"><img src=\"http://poorvikabeta.webindia.com/image/cache/catalog/demo/canon_eos_5d_1-355x470.jpg\"></div>
\t\t\t                <div class=\"combo-content\">
\t\t\t                    <h5>Canon EOS 5D</h5>
\t\t\t                    <strong>4.5
\t\t\t                    ";
        // line 1093
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 5));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 1094
            echo "\t\t\t\t\t\t\t\t";
            if (((isset($context["rating"]) ? $context["rating"] : null) < $context["i"])) {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            } else {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            }
            // line 1095
            echo "\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1096
        echo "\t\t\t\t\t\t\t</strong> 
\t\t\t\t\t\t\t<small class=\"price\">&#8377; 45,000<strike class=\"old-price\">25,000</strike></small>
\t\t\t\t\t\t\t</div>
\t\t\t            </a>
\t\t\t             <div class=\"battery-power\">
\t\t\t                <strong>4000 mAH</strong>
\t\t\t            </div>
\t\t\t            <div class=\"external-storage\">
\t\t\t                <strong>128GB</strong>
\t\t\t            </div>
\t\t\t             <div class=\"external-storage\">
\t\t\t                <strong>128GB</strong>
\t\t\t            </div>
\t\t\t            </li>
\t\t\t             <li><a href=\"\">
\t\t\t                <div class=\"combo-list\"><img src=\"http://poorvikabeta.webindia.com/image/cache/catalog/demo/canon_eos_5d_1-355x470.jpg\"></div>
\t\t\t                <div class=\"combo-content\">
\t\t\t                    <h5>Canon EOS 5D</h5>
\t\t\t                    <strong>4.5
\t\t\t                    ";
        // line 1115
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 5));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 1116
            echo "\t\t\t\t\t\t\t\t";
            if (((isset($context["rating"]) ? $context["rating"] : null) < $context["i"])) {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            } else {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            }
            // line 1117
            echo "\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1118
        echo "\t\t\t\t\t\t\t</strong> 
\t\t\t\t\t\t\t<small class=\"price\">&#8377; 45,000<strike class=\"old-price\">25,000</strike></small>
\t\t\t\t\t\t\t</div>
\t\t\t            </a>
\t\t\t             <div class=\"battery-power\">
\t\t\t                <strong>4000 mAH</strong>
\t\t\t            </div>
\t\t\t            <div class=\"external-storage\">
\t\t\t                <strong>128GB</strong>
\t\t\t            </div>
\t\t\t             <div class=\"external-storage\">
\t\t\t                <strong>128GB</strong>
\t\t\t            </div>
\t\t\t            </li>
\t\t\t            
\t\t\t             
\t\t\t            
\t\t\t        </ul>
\t\t\t        </div>
\t\t\t       
\t\t\t       
\t\t\t\t\t\t\t\t\t<div class=\"gallery-button details-button\"><a href=\"\" class=\"btn btn-gallary btn-detail\">See Image Gallery</a></div>
\t\t\t    </div>
\t\t    </div>
\t\t    ";
        // line 1143
        echo "\t\t    ";
        // line 1144
        echo "\t\t    <div class=\"col-md-12\">
\t\t        <div class=\"product-combo product-video\">
\t\t       <div class=\"combo-title\">
\t\t\t            <h2><img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/video-product.png\">Video</h2>
\t\t\t             <a href=\"\"><i class=\"fa fa-youtube\"></i>Watch YouTube Reviews</a>
\t\t\t        </div>
\t\t\t        
\t\t\t        <ul class=\"video-carousel owl-carousel owl-theme\">
\t\t\t            <li>
\t\t\t                <div class=\"video-frame\">
\t\t\t                  <iframe width=\"200\" height=\"115\" src=\"https://www.youtube.com/embed/tgbNymZ7vqY\"></iframe>
                            </div>
                            <div class=\"video-content\">
                                <span>4.6<img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/video-green-arrow.png\"></span>
                                <strong>performance<small>Ultra fast</small></strong>
                            </div>
                       </li>
                        <li>
\t\t\t                <div class=\"video-frame\">
\t\t\t                  <iframe width=\"200\" height=\"115\" src=\"https://www.youtube.com/embed/tgbNymZ7vqY\"></iframe>
                            </div>
                             <div class=\"video-content\">
                                <span>4.6<img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/video-green-arrow.png\"></span>
                                <strong>performance<small>Ultra fast</small></strong>
                            </div>
                       </li>
                        <li>
\t\t\t                <div class=\"video-frame\">
\t\t\t                   <iframe width=\"200\" height=\"115\" src=\"https://www.youtube.com/embed/tgbNymZ7vqY\"></iframe>
                            </div>
                             <div class=\"video-content\">
                                <span>4.6<img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/video-green-arrow.png\"></span>
                                <strong>performance<small>Ultra fast</small></strong>
                            </div>
                       </li>
                        <li>
\t\t\t                <div class=\"video-frame\">
\t\t\t                   <iframe width=\"200\" height=\"115\" src=\"https://www.youtube.com/embed/tgbNymZ7vqY\"></iframe>
                            </div>
                             <div class=\"video-content\">
                                <span>4.6<img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/video-green-arrow.png\"></span>
                                <strong>performance<small>Ultra fast</small></strong>
                            </div>
                       </li>
                        <li>
\t\t\t                <div class=\"video-frame\">
\t\t\t                  <iframe width=\"200\" height=\"115\" src=\"https://www.youtube.com/embed/tgbNymZ7vqY\"></iframe>
                            </div>
                             <div class=\"video-content\">
                                <span>4.6<img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/video-green-arrow.png\"></span>
                                <strong>performance<small>Ultra fast</small></strong>
                            </div>
                       </li>
\t\t\t        </ul>
\t\t\t        
\t\t\t        </div>
\t\t    </div>
\t\t     ";
        // line 1202
        echo "\t\t      ";
        // line 1203
        echo "\t\t     <div class=\"col-md-12\">
\t\t         <div class=\"product-question review-product\">
\t\t              <div class=\"combo-title\">
\t\t\t            <h2><img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/video-product.png\">Customer Review</h2>
\t\t\t             <a href=\"\"><i class=\"fa fa-edit\"></i>Write Product Reviews</a>
\t\t\t          </div>
\t\t         </div>
\t\t     </div>
\t\t     <div class=\"col-md-5  pr-0\">
\t\t         <div class=\"reviwe-block\">
\t\t         <div class=\"review-card\">
\t\t             <div class=\"card-cont\">
\t\t             <span>Rating<strong>4.6<small>out of 5</small></strong></span>
\t\t             <small>35 Ratings 4 Reviews</small>
\t\t             </div>
\t\t             <div class=\"review-rating\">
\t\t                 <ul>
\t\t                     <li> ";
        // line 1220
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 5));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 1221
            echo "\t\t\t\t\t\t\t\t";
            if (((isset($context["rating"]) ? $context["rating"] : null) < $context["i"])) {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            } else {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            }
            // line 1222
            echo "\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo "<small>75%</small></li>
\t\t\t\t\t\t\t<li> ";
        // line 1223
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 4));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 1224
            echo "\t\t\t\t\t\t\t\t";
            if (((isset($context["rating"]) ? $context["rating"] : null) < $context["i"])) {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            } else {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            }
            // line 1225
            echo "\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo "<small>75%</small></li>
\t\t\t\t\t\t\t\t<li> ";
        // line 1226
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 3));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 1227
            echo "\t\t\t\t\t\t\t\t";
            if (((isset($context["rating"]) ? $context["rating"] : null) < $context["i"])) {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            } else {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            }
            // line 1228
            echo "\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo "<small>75%</small></li>
\t\t\t\t\t\t\t\t<li> ";
        // line 1229
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 2));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 1230
            echo "\t\t\t\t\t\t\t\t";
            if (((isset($context["rating"]) ? $context["rating"] : null) < $context["i"])) {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            } else {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            }
            // line 1231
            echo "\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo "<small>75%</small></li>
\t\t\t\t\t\t\t\t<li> ";
        // line 1232
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 1));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 1233
            echo "\t\t\t\t\t\t\t\t";
            if (((isset($context["rating"]) ? $context["rating"] : null) < $context["i"])) {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            } else {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            }
            // line 1234
            echo "\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo "<small>75%</small></li>
\t\t                 </ul>
\t\t             </div>
\t\t             
\t\t         </div>
\t\t         <div class=\"review-edit-box\">
\t\t             <strong>Write review for this product<span>share your feedback with other customer</span></strong>
\t\t             <div class=\"edit-button\"><a href=\"\"><i class=\"fa fa-edit\"></i>Write a Product review</a></div>
\t\t         </div>
\t\t         </div>
\t\t     </div>
\t\t     <div class=\"col-md-7 pl-0\">
\t\t        <div class=\"review-right-block\">
\t\t         <div class=\"review-mention\">
\t\t             <h3>Review Mention</h3>
\t\t             <span>battery life</span><span>value of money</span><span>Price range</span><span>best budget</span>
\t\t         </div>
\t\t         <div class=\"review-text\">
\t\t             <h3>Review</h3>
\t\t             <ul>
\t\t                 <li>
\t\t                     <div class=\"customer-block\">
\t\t                     <div class=\"customer-details\">
\t\t                         <div class=\"customer-profile\">
\t\t                         <img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/customer-review-img1.jpg\">
\t\t                         </div>
\t\t                         <strong>Nikil <br>
\t\t                          ";
        // line 1261
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 5));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 1262
            echo "\t\t\t\t\t\t\t\t";
            if (((isset($context["rating"]) ? $context["rating"] : null) < $context["i"])) {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            } else {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            }
            // line 1263
            echo "\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1264
        echo "\t\t\t\t\t\t\t<small>reviewed on 15 sep 2020</small>
\t\t                         </strong>
\t\t                     </div>
\t\t                     <div class=\"like-details\">
\t\t                         <small><i class=\"fa fa-thumbs-up\"></i></small>
\t\t                         <small><i class=\"fa fa-thumbs-down\"></i></small>
\t\t                     </div>
\t\t                     </div>
\t\t                     <div class=\"review-para\">
\t\t                         <span>More than 5 star, best budget mobile</span>
\t\t                         <p>Integer faucibus facilisis volutpat. Aliquam in suscipit orci. Nullam aliquam fermentum auctor. </p>
\t\t                     </div>
\t\t                 </li>
\t\t                  <li>
\t\t                     <div class=\"customer-block\">
\t\t                     <div class=\"customer-details\">
\t\t                         <div class=\"customer-profile\">
\t\t                         <img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/customer-review-img1.jpg\">
\t\t                         </div>
\t\t                         <strong>Nikil <br>
\t\t                          ";
        // line 1284
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 5));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 1285
            echo "\t\t\t\t\t\t\t\t";
            if (((isset($context["rating"]) ? $context["rating"] : null) < $context["i"])) {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            } else {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            }
            // line 1286
            echo "\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1287
        echo "\t\t\t\t\t\t\t<small>reviewed on 15 sep 2020</small>
\t\t                         </strong>
\t\t                     </div>
\t\t                     <div class=\"like-details\">
\t\t                         <small><i class=\"fa fa-thumbs-up\"></i></small>
\t\t                         <small><i class=\"fa fa-thumbs-down\"></i></small>
\t\t                     </div>
\t\t                     </div>
\t\t                     <div class=\"review-para\">
\t\t                         <span>More than 5 star, best budget mobile</span>
\t\t                         <p>Integer faucibus facilisis volutpat. Aliquam in suscipit orci. Nullam aliquam fermentum auctor..</p>
\t\t                     </div>
\t\t                 </li>
\t\t                  <li>
\t\t                     <div class=\"customer-block\">
\t\t                     <div class=\"customer-details\">
\t\t                         <div class=\"customer-profile\">
\t\t                         <img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/customer-review-img1.jpg\">
\t\t                         </div>
\t\t                         <strong>Nikil <br>
\t\t                          ";
        // line 1307
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 5));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 1308
            echo "\t\t\t\t\t\t\t\t";
            if (((isset($context["rating"]) ? $context["rating"] : null) < $context["i"])) {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            } else {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            }
            // line 1309
            echo "\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1310
        echo "\t\t\t\t\t\t\t<small>reviewed on 15 sep 2020</small>
\t\t                         </strong>
\t\t                     </div>
\t\t                     <div class=\"like-details\">
\t\t                         <small><i class=\"fa fa-thumbs-up\"></i></small>
\t\t                         <small><i class=\"fa fa-thumbs-down\"></i></small>
\t\t                     </div>
\t\t                     </div>
\t\t                     <div class=\"review-para\">
\t\t                         <span>More than 5 star, best budget mobile</span>
\t\t                         <p>Integer faucibus facilisis volutpat. Aliquam in suscipit orci. Nullam aliquam fermentum auctor..</p>
\t\t                     </div>
\t\t                 </li>
\t\t                 
\t\t             </ul>
\t\t         </div>
\t\t         </div>
\t\t     </div>
\t\t       ";
        // line 1329
        echo "\t\t     ";
        // line 1330
        echo "\t\t     <div class=\"col-md-8 pr-0\">
\t\t         <div class=\"product-question\">
\t\t               <div class=\"combo-title question-pro\">
\t\t\t            <h2><img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/related-product.png\">Question and Answer</h2>
\t\t\t            <span><input type=\"text\" placeholder=\"Search of Question and Answer..\"></span>
\t\t\t        </div>
\t\t\t        <div id=\"collapse-description\" class=\"desc-collapse showdown\">
\t\t\t        <ul>
\t\t\t            <li><div class=\"question-content\">
\t\t\t                <p class=\"question-cont\">Q : It is Quality Product?</p>
\t\t\t                <p class=\"answer-cont\">A : Yes it Quality Product.</p>
\t\t\t                </div>
\t\t\t                <div class=\"like-details\">
\t\t\t                    <span>by paratap</span>
\t\t\t                    <small><i class=\"fa fa-thumbs-up\"></i>930</small>
\t\t\t                    <small><i class=\"fa fa-thumbs-down\"></i>30</small>
\t\t\t                </div>
\t\t\t            </li>
\t\t\t             <li><div class=\"question-content\">
\t\t\t                <p class=\"question-cont\">Q : It is Quality Product?</p>
\t\t\t                <p class=\"answer-cont\">A : Yes it Quality Product.</p>
\t\t\t                </div>
\t\t\t                <div class=\"like-details\">
\t\t\t                    <span>by paratap</span>
\t\t\t                    <small><i class=\"fa fa-thumbs-up\"></i>930</small>
\t\t\t                    <small><i class=\"fa fa-thumbs-down\"></i>30</small>
\t\t\t                </div>
\t\t\t            </li>
\t\t\t             <li><div class=\"question-content\">
\t\t\t                <p class=\"question-cont\">Q : It is Quality Product?</p>
\t\t\t                <p class=\"answer-cont\">A : Yes it Quality Product.</p>
\t\t\t                </div>
\t\t\t                <div class=\"like-details\">
\t\t\t                    <span>by paratap</span>
\t\t\t                    <small><i class=\"fa fa-thumbs-up\"></i>930</small>
\t\t\t                    <small><i class=\"fa fa-thumbs-down\"></i>30</small>
\t\t\t                </div>
\t\t\t            </li>
\t\t\t             <li><div class=\"question-content\">
\t\t\t                <p class=\"question-cont\">Q : It is Quality Product?</p>
\t\t\t                <p class=\"answer-cont\">A : Yes it Quality Product.</p>
\t\t\t                </div>
\t\t\t                <div class=\"like-details\">
\t\t\t                    <span>by paratap</span>
\t\t\t                    <small><i class=\"fa fa-thumbs-up\"></i>930</small>
\t\t\t                    <small><i class=\"fa fa-thumbs-down\"></i>30</small>
\t\t\t                </div>
\t\t\t            </li>
\t\t\t        </ul>
\t\t\t        </div>
\t\t\t        <div class=\"button-toggle toggle1\">
\t\t\t\t\t\t\t\t         <a class=\"showmore\" data-toggle=\"collapse\" href=\"#\" aria-expanded=\"false\" aria-controls=\"collapse-footer\">
\t\t\t\t\t\t\t\t            <span class=\"toggle-more\">Show all answer question <i class=\"fa fa-angle-down\"></i></span> 
\t\t\t\t\t\t\t\t            <span class=\"toggle-less\">Show Less <i class=\"fa fa-angle-up\"></i></span>           
\t\t\t\t\t\t\t\t\t\t</a>      
\t\t\t\t\t\t\t\t\t\t<a href=\"\">
\t\t\t\t\t\t\t\t\t\t    <strong><i class=\"fa fa-edit\"></i>Ask Question</strong>
\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t</div>
\t\t         </div>
\t\t     </div>
\t\t     <div class=\"col-md-4 pl-0\">
\t\t         <div class=\"product-customer-image\">
\t\t              <div class=\"combo-title customer-img\">
\t\t\t            <h2>Customer Image</h2>
\t\t\t        </div>
\t\t\t        <ul>
\t\t\t            <li><a href=\"\"><img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/customer-img1.jpg\"></a></li>
\t\t\t            <li><a href=\"\"><img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/customer-img2.jpg\"></a></li>
\t\t\t            <li><a href=\"\"><img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/customer-img2.jpg\"></a></li>
\t\t\t            <li><a href=\"\"><img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/customer-img1.jpg\"></a></li>
\t\t\t        </ul>
\t\t\t        <div class=\"gallery-button\"><a href=\"\" class=\"btn btn-gallary\">See Image Gallery</a></div>
\t\t         </div>
\t\t     </div>
\t\t     ";
        // line 1406
        echo "\t\t    ";
        // line 1407
        echo "\t\t    <div class=\"col-md-12\">
\t\t        <div class=\"product-combo related-product\">
\t\t\t        <div class=\"combo-title\">
\t\t\t            <h2><img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/related-product.png\">Related Product</h2>
\t\t\t        </div>
\t\t\t        <ul class=\"related-carousel owl-carousel owl-theme\">
\t\t\t            <li><a href=\"\">
\t\t\t                <div class=\"combo-list\"><img src=\"http://poorvikabeta.webindia.com/image/cache/catalog/demo/canon_eos_5d_1-355x470.jpg\"></div>
\t\t\t                <div class=\"combo-content\">
\t\t\t                    <h5>Canon EOS 5D</h5>
\t\t\t                    <strong>4.5
\t\t\t                    ";
        // line 1418
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 5));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 1419
            echo "\t\t\t\t\t\t\t\t";
            if (((isset($context["rating"]) ? $context["rating"] : null) < $context["i"])) {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            } else {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            }
            // line 1420
            echo "\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1421
        echo "\t\t\t\t\t\t\t</strong> 
\t\t\t\t\t\t\t<small class=\"price\">&#8377; 45,000<strike class=\"old-price\">25,000</strike></small>
\t\t\t\t\t\t\t</div>
\t\t\t            </a>
\t\t\t            </li>
\t\t\t             <li><a href=\"\">
\t\t\t                <div class=\"combo-list\"><img src=\"http://poorvikabeta.webindia.com/image/cache/catalog/demo/canon_eos_5d_1-355x470.jpg\"></div>
\t\t\t                <div class=\"combo-content\">
\t\t\t                    <h5>Canon EOS 5D</h5>
\t\t\t                    <strong>4.5
\t\t\t                    ";
        // line 1431
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 5));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 1432
            echo "\t\t\t\t\t\t\t\t";
            if (((isset($context["rating"]) ? $context["rating"] : null) < $context["i"])) {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            } else {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            }
            // line 1433
            echo "\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1434
        echo "\t\t\t\t\t\t\t</strong> 
\t\t\t\t\t\t\t<small class=\"price\">&#8377; 45,000<strike class=\"old-price\">25,000</strike></small>
\t\t\t\t\t\t\t</div>
\t\t\t            </a>
\t\t\t            </li>
\t\t\t             <li><a href=\"\">
\t\t\t                <div class=\"combo-list\"><img src=\"http://poorvikabeta.webindia.com/image/cache/catalog/demo/canon_eos_5d_1-355x470.jpg\"></div>
\t\t\t                <div class=\"combo-content\">
\t\t\t                    <h5>Canon EOS 5D</h5>
\t\t\t                    <strong>4.5
\t\t\t                    ";
        // line 1444
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 5));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 1445
            echo "\t\t\t\t\t\t\t\t";
            if (((isset($context["rating"]) ? $context["rating"] : null) < $context["i"])) {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            } else {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            }
            // line 1446
            echo "\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1447
        echo "\t\t\t\t\t\t\t</strong> 
\t\t\t\t\t\t\t<small class=\"price\">&#8377; 45,000<strike class=\"old-price\">25,000</strike></small>
\t\t\t\t\t\t\t</div>
\t\t\t            </a>
\t\t\t            </li>
\t\t\t             <li><a href=\"\">
\t\t\t                <div class=\"combo-list\"><img src=\"http://poorvikabeta.webindia.com/image/cache/catalog/demo/canon_eos_5d_1-355x470.jpg\"></div>
\t\t\t                <div class=\"combo-content\">
\t\t\t                    <h5>Canon EOS 5D</h5>
\t\t\t                    <strong>4.5
\t\t\t                    ";
        // line 1457
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 5));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 1458
            echo "\t\t\t\t\t\t\t\t";
            if (((isset($context["rating"]) ? $context["rating"] : null) < $context["i"])) {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            } else {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            }
            // line 1459
            echo "\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1460
        echo "\t\t\t\t\t\t\t</strong> 
\t\t\t\t\t\t\t<small class=\"price\">&#8377; 45,000<strike class=\"old-price\">25,000</strike></small>
\t\t\t\t\t\t\t</div>
\t\t\t            </a>
\t\t\t            </li>
\t\t\t             <li><a href=\"\">
\t\t\t                <div class=\"combo-list\"><img src=\"http://poorvikabeta.webindia.com/image/cache/catalog/demo/canon_eos_5d_1-355x470.jpg\"></div>
\t\t\t                <div class=\"combo-content\">
\t\t\t                    <h5>Canon EOS 5D</h5>
\t\t\t                    <strong>4.5
\t\t\t                    ";
        // line 1470
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 5));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 1471
            echo "\t\t\t\t\t\t\t\t";
            if (((isset($context["rating"]) ? $context["rating"] : null) < $context["i"])) {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            } else {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            }
            // line 1472
            echo "\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1473
        echo "\t\t\t\t\t\t\t</strong> 
\t\t\t\t\t\t\t<small class=\"price\">&#8377; 45,000<strike class=\"old-price\">25,000</strike></small>
\t\t\t\t\t\t\t</div>
\t\t\t            </a>
\t\t\t            </li>
\t\t\t            
\t\t\t             
\t\t\t            
\t\t\t        </ul>
\t\t\t    </div>
\t\t    </div>
\t\t</div>
    \t";
        // line 1486
        echo "    </div>
    
    ";
        // line 1489
        echo "    ";
        if (((isset($context["col_position"]) ? $context["col_position"] : null) == "outside")) {
            echo " ";
            echo (isset($context["column_right"]) ? $context["column_right"] : null);
            echo " ";
        }
        // line 1490
        echo "    </div>
</div>

<script src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/js/owl.carousel.min.js\"></script> 
<script type=\"text/javascript\">
<!--
\$('select[name=\\'recurring_id\\'], input[name=\"quantity\"]').change(function(){
\t\$.ajax({
\t\turl: 'index.php?route=product/product/getRecurringDescription',
\t\ttype: 'post',
\t\tdata: \$('input[name=\\'product_id\\'], input[name=\\'quantity\\'], select[name=\\'recurring_id\\']'),
\t\tdataType: 'json',
\t\tbeforeSend: function() {
\t\t\t\$('#recurring-description').html('');
\t\t},
\t\tsuccess: function(json) {
\t\t\t\$('.alert-dismissible, .text-danger').remove();

\t\t\tif (json['success']) {
\t\t\t\t\$('#recurring-description').html(json['success']);
\t\t\t}
\t\t}
\t});
});
//--></script>

<script type=\"text/javascript\"><!--
\$('#button-cart').on('click', function() {
\t
\t\$.ajax({
\t\turl: 'index.php?route=extension/soconfig/cart/add',
\t\ttype: 'post',
\t\tdata: \$('#product input[type=\\'text\\'], #product input[type=\\'hidden\\'], #product input[type=\\'radio\\']:checked, #product input[type=\\'checkbox\\']:checked, #product select, #product textarea'),
\t\tdataType: 'json',
\t\tbeforeSend: function() {
\t\t\t\$('#button-cart').button('loading');
\t\t},
\t\tcomplete: function() {
\t\t\t\$('#button-cart').button('reset');
\t\t},
\t\tsuccess: function(json) {
\t\t\t\$('.alert').remove();
\t\t\t\$('.text-danger').remove();
\t\t\t\$('.form-group').removeClass('has-error');
\t\t\tif (json['error']) {
\t\t\t\tif (json['error']['option']) {
\t\t\t\t\tfor (i in json['error']['option']) {
\t\t\t\t\t\tvar element = \$('#input-option' + i.replace('_', '-'));
 
 ";
        // line 1539
        if ((isset($context["option_data"]) ? $context["option_data"] : null)) {
            echo " 
 if(ProductOptionId != undefined && ProductOptionId==i.replace('_', '-')){ 
 \$('.so-colorswatch-productpage-icons').after('<div class=\"text-danger\">' + json['error']['option'][i] + '</div>'); 
 } 
 ";
        }
        // line 1543
        echo " 
 
\t\t\t\t\t\t
\t\t\t\t\t\tif (element.parent().hasClass('input-group')) {
\t\t\t\t\t\t\telement.parent().after('<div class=\"text-danger\">' + json['error']['option'][i] + '</div>');
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\telement.after('<div class=\"text-danger\">' + json['error']['option'][i] + '</div>');
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t}
\t\t\t\t
\t\t\t\tif (json['error']['recurring']) {
\t\t\t\t\t\$('select[name=\\'recurring_id\\']').after('<div class=\"text-danger\">' + json['error']['recurring'] + '</div>');
\t\t\t\t}
\t\t\t\t
\t\t\t\t// Highlight any found errors
\t\t\t\t\$('.text-danger').parent().addClass('has-error');
\t\t\t}
\t\t\t
\t\t\tif (json['success']) {
\t\t\t\t\$('.text-danger').remove();
\t\t\t\t\$('#wrapper').before('<div class=\"alert alert-success\"><i class=\"fa fa-check-circle\"></i> ' + json['success'] + ' <button type=\"button\" class=\"fa fa-close close\" data-dismiss=\"alert\"></button></div>');
\t\t\t\t\$('#cart  .total-shopping-cart ').html(json['total'] );
\t\t\t\t\$('#cart > ul').load('index.php?route=common/cart/info ul li');
\t\t\t\t
\t\t\t\ttimer = setTimeout(function () {
\t\t\t\t\t\$('.alert').addClass('fadeOut');
\t\t\t\t}, 4000);
\t\t\t\t\$('.so-groups-sticky .popup-mycart .popup-content').load('index.php?route=extension/module/so_tools/info .popup-content .cart-header');
\t\t\t}
\t\t\t
\t\t
\t\t},
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
        }
\t});
});

//--></script> 

<script type=\"text/javascript\"><!--
\$('.date').datetimepicker({
\tlanguage: document.cookie.match(new RegExp('language=([^;]+)'))[1],
\tpickTime: false
});

\$('.datetime').datetimepicker({
\tlanguage: document.cookie.match(new RegExp('language=([^;]+)'))[1],
\tpickDate: true,
\tpickTime: true
});

\$('.time').datetimepicker({
\tlanguage: document.cookie.match(new RegExp('language=([^;]+)'))[1],
\tpickDate: false
});

\$('button[id^=\\'button-upload\\']').on('click', function() {
\tvar node = this;

\t\$('#form-upload').remove();

\t\$('body').prepend('<form enctype=\"multipart/form-data\" id=\"form-upload\" style=\"display: none;\"><input type=\"file\" name=\"file\" /></form>');

\t\$('#form-upload input[name=\\'file\\']').trigger('click');

\tif (typeof timer != 'undefined') {
\t\tclearInterval(timer);
\t}

\ttimer = setInterval(function() {
\t\tif (\$('#form-upload input[name=\\'file\\']').val() != '') {
\t\t\tclearInterval(timer);

\t\t\t\$.ajax({
\t\t\t\turl: 'index.php?route=tool/upload',
\t\t\t\ttype: 'post',
\t\t\t\tdataType: 'json',
\t\t\t\tdata: new FormData(\$('#form-upload')[0]),
\t\t\t\tcache: false,
\t\t\t\tcontentType: false,
\t\t\t\tprocessData: false,
\t\t\t\tbeforeSend: function() {
\t\t\t\t\t\$(node).button('loading');
\t\t\t\t},
\t\t\t\tcomplete: function() {
\t\t\t\t\t\$(node).button('reset');
\t\t\t\t},
\t\t\t\tsuccess: function(json) {
\t\t\t\t\t\$('.text-danger').remove();

\t\t\t\t\tif (json['error']) {
\t\t\t\t\t\t\$(node).parent().find('input').after('<div class=\"text-danger\">' + json['error'] + '</div>');
\t\t\t\t\t}

\t\t\t\t\tif (json['success']) {
\t\t\t\t\t\talert(json['success']);

\t\t\t\t\t\t\$(node).parent().find('input').val(json['code']);
\t\t\t\t\t}
\t\t\t\t},
\t\t\t\terror: function(xhr, ajaxOptions, thrownError) {
\t\t\t\t\talert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
\t\t\t\t}
\t\t\t});
\t\t}
\t}, 500);
});
//--></script> 
<script type=\"text/javascript\"><!--
\$('#review').delegate('.pagination a', 'click', function(e) {
    e.preventDefault();

    \$('#review').fadeOut('slow');
    \$('#review').load(this.href);
    \$('#review').fadeIn('slow');
});

\$('#review').load('index.php?route=product/product/review&product_id=";
        // line 1662
        echo (isset($context["product_id"]) ? $context["product_id"] : null);
        echo "');

\$('#button-review').on('click', function() {
\t\$.ajax({
\t\turl: 'index.php?route=product/product/write&product_id=";
        // line 1666
        echo (isset($context["product_id"]) ? $context["product_id"] : null);
        echo "',
\t\ttype: 'post',
\t\tdataType: 'json',
\t\tdata: \$(\"#form-review\").serialize(),
\t\tbeforeSend: function() {
\t\t\t\$('#button-review').button('loading');
\t\t},
\t\tcomplete: function() {
\t\t\t\$('#button-review').button('reset');
\t\t},
\t\tsuccess: function(json) {
\t\t\t\$('.alert-dismissible').remove();

\t\t\tif (json['error']) {
\t\t\t\t\$('#review').after('<div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ' + json['error'] + '</div>');
\t\t\t}

\t\t\tif (json['success']) {
\t\t\t\t\$('#review').after('<div class=\"alert alert-success alert-dismissible\"><i class=\"fa fa-check-circle\"></i> ' + json['success'] + '</div>');

\t\t\t\t\$('input[name=\\'name\\']').val('');
\t\t\t\t\$('textarea[name=\\'text\\']').val('');
\t\t\t\t\$('input[name=\\'rating\\']:checked').prop('checked', false);
\t\t\t}
\t\t}
\t});
});

//--></script>



<script type=\"text/javascript\"><!--
\t\$(document).ready(function() {
\t\t
\t\t// Initialize the sticky scrolling on an item 
\t\tsidebar_sticky = '";
        // line 1702
        echo (isset($context["sidebar_sticky"]) ? $context["sidebar_sticky"] : null);
        echo "';
\t\t
\t\tif(sidebar_sticky=='left'){
\t\t\t\$(\".left_column\").stick_in_parent({
\t\t\t    offset_top: 10,
\t\t\t    bottoming   : true
\t\t\t});
\t\t}else if (sidebar_sticky=='right'){
\t\t\t\$(\".right_column\").stick_in_parent({
\t\t\t    offset_top: 10,
\t\t\t    bottoming   : true
\t\t\t});
\t\t}else if (sidebar_sticky=='all'){
\t\t\t\$(\".content-aside\").stick_in_parent({
\t\t\t    offset_top: 10,
\t\t\t    bottoming   : true
\t\t\t});
\t\t}
\t\t

\t\t\$(\"#thumb-slider .image-additional\").each(function() {
\t\t\t\$(this).find(\"[data-index='0']\").addClass('active');
\t\t});
\t\t
\t\t\$('.product-options li.radio').click(function(){
\t\t\t\$(this).addClass(function() {
\t\t\t\tif(\$(this).hasClass(\"active\")) return \"\";
\t\t\t\treturn \"active\";
\t\t\t});
\t\t\t
\t\t\t\$(this).siblings(\"li\").removeClass(\"active\");
\t\t\t\$(this).parent().find('.selected-option').html('<span class=\"label label-success\">'+ \$(this).find('img').data('original-title') +'</span>');
\t\t})
\t\t
\t\t\$('.thumb-video').magnificPopup({
\t\t  type: 'iframe',
\t\t  iframe: {
\t\t\tpatterns: {
\t\t\t   youtube: {
\t\t\t\t  index: 'youtube.com/', // String that detects type of video (in this case YouTube). Simply via url.indexOf(index).
\t\t\t\t  id: 'v=', // String that splits URL in a two parts, second part should be %id%
\t\t\t\t  src: '//www.youtube.com/embed/%id%?autoplay=1' // URL that will be set as a source for iframe. 
\t\t\t\t\t},
\t\t\t\t}
\t\t\t}
\t\t});
\t});
//--></script>


<script type=\"text/javascript\">
var ajax_price = function() {
\t\$.ajax({
\t\ttype: 'POST',
\t\turl: 'index.php?route=extension/soconfig/liveprice/index',
\t\tdata: \$('.product-detail input[type=\\'text\\'], .product-detail input[type=\\'hidden\\'], .product-detail input[type=\\'radio\\']:checked, .product-detail input[type=\\'checkbox\\']:checked, .product-detail select, .product-detail textarea'),
\t\tdataType: 'json',
\t\t\tsuccess: function(json) {
\t\t\tif (json.success) {
\t\t\t\tchange_price('#price-special', json.new_price.special);
\t\t\t\tchange_price('#price-tax', json.new_price.tax);
\t\t\t\tchange_price('#price-old', json.new_price.price);
\t\t\t}
\t\t}
\t});
}

var change_price = function(id, new_price) {\$(id).html(new_price);}
\$('.product-detail input[type=\\'text\\'], .product-detail input[type=\\'hidden\\'], .product-detail input[type=\\'radio\\'], .product-detail input[type=\\'checkbox\\'], .product-detail select, .product-detail textarea, .product-detail input[name=\\'quantity\\']').on('change', function() {
\tajax_price();
});
</script>
<script>
function openColor(color) {
  var i;
  var x = document.getElementsByClassName(\"product-color-change\");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = \"none\";  
  }
  document.getElementById(color).style.display = \"block\";  
}
</script>


<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName(\"tabcontent\");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = \"none\";
  }
  tablinks = document.getElementsByClassName(\"tablinks\");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(\" active\", \"\");
  }
  document.getElementById(cityName).style.display = \"inline-block\";
  evt.currentTarget.className += \" active\";
}
</script>
<script>
 \$(document).ready(function(){
  \$('.related-carousel').owlCarousel({
    loop:true,
   autoplay:true,
    autoplayTimeout:3000,
    autoplayHoverPause:true,
    nav:true,
\tdots:false,
    responsive:{
        0:{
            items:1
        },
\t\t320:{
            items:1
        },
\t\t480:{
            items:1
        },
        600:{
            items:2
        },
\t\t767:{
            items:3
        },
\t\t991:{
            items:4
        },
        1200:{
            items:5
        }
    }
})
\$( \".owl-prev\").html('<i class=\"fa fa-lg fa-angle-left\"></i>');
 \$( \".owl-next\").html('<i class=\"fa fa-lg fa-angle-right\"></i>');
});\t

</script>
<script>
 \$(document).ready(function(){
  \$('.video-carousel').owlCarousel({
    loop:true,
   autoplay:true,
    autoplayTimeout:3000,
    autoplayHoverPause:true,
    nav:true,
\tdots:false,
    responsive:{
        0:{
            items:1
        },
\t\t320:{
            items:1
        },
\t\t480:{
            items:1
        },
        600:{
            items:2
        },
\t\t767:{
            items:2
        },
\t\t991:{
            items:3
        },
        1200:{
            items:4
        }
    }
})
\$( \".owl-prev\").html('<i class=\"fa fa-lg fa-arrow-left\"></i>');
 \$( \".owl-next\").html('<i class=\"fa fa-lg fa-arrow-right\"></i>');
});\t

</script>

";
        // line 1878
        echo (isset($context["footer"]) ? $context["footer"] : null);
        echo " 
";
    }

    public function getTemplateName()
    {
        return "so-destino/template/product/product.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  3563 => 1878,  3384 => 1702,  3345 => 1666,  3338 => 1662,  3217 => 1543,  3209 => 1539,  3158 => 1490,  3151 => 1489,  3147 => 1486,  3133 => 1473,  3127 => 1472,  3120 => 1471,  3116 => 1470,  3104 => 1460,  3098 => 1459,  3091 => 1458,  3087 => 1457,  3075 => 1447,  3069 => 1446,  3062 => 1445,  3058 => 1444,  3046 => 1434,  3040 => 1433,  3033 => 1432,  3029 => 1431,  3017 => 1421,  3011 => 1420,  3004 => 1419,  3000 => 1418,  2987 => 1407,  2985 => 1406,  2908 => 1330,  2906 => 1329,  2886 => 1310,  2880 => 1309,  2873 => 1308,  2869 => 1307,  2847 => 1287,  2841 => 1286,  2834 => 1285,  2830 => 1284,  2808 => 1264,  2802 => 1263,  2795 => 1262,  2791 => 1261,  2757 => 1234,  2750 => 1233,  2746 => 1232,  2738 => 1231,  2731 => 1230,  2727 => 1229,  2719 => 1228,  2712 => 1227,  2708 => 1226,  2700 => 1225,  2693 => 1224,  2689 => 1223,  2681 => 1222,  2674 => 1221,  2670 => 1220,  2651 => 1203,  2649 => 1202,  2590 => 1144,  2588 => 1143,  2562 => 1118,  2556 => 1117,  2549 => 1116,  2545 => 1115,  2524 => 1096,  2518 => 1095,  2511 => 1094,  2507 => 1093,  2486 => 1074,  2480 => 1073,  2473 => 1072,  2469 => 1071,  2448 => 1052,  2442 => 1051,  2435 => 1050,  2431 => 1049,  2405 => 1025,  2400 => 1021,  2393 => 1020,  2388 => 1016,  2378 => 1010,  2374 => 1007,  2372 => 1006,  2365 => 1002,  2361 => 1001,  2357 => 999,  2354 => 998,  2346 => 991,  2340 => 989,  2338 => 988,  2335 => 987,  2329 => 984,  2326 => 983,  2324 => 982,  2321 => 981,  2316 => 978,  2310 => 976,  2301 => 972,  2295 => 969,  2290 => 967,  2277 => 957,  2273 => 956,  2265 => 951,  2260 => 949,  2252 => 944,  2248 => 943,  2244 => 941,  2242 => 940,  2238 => 939,  2233 => 936,  2231 => 935,  2226 => 932,  2219 => 928,  2215 => 927,  2211 => 925,  2209 => 924,  2203 => 921,  2199 => 920,  2195 => 919,  2192 => 918,  2188 => 916,  2181 => 914,  2172 => 911,  2168 => 910,  2165 => 909,  2161 => 908,  2157 => 906,  2153 => 905,  2147 => 903,  2145 => 902,  2135 => 899,  2132 => 898,  2126 => 894,  2118 => 892,  2116 => 891,  2113 => 890,  2107 => 888,  2105 => 887,  2102 => 886,  2096 => 884,  2094 => 883,  2091 => 882,  2085 => 880,  2083 => 879,  2078 => 877,  2075 => 876,  2072 => 875,  2065 => 869,  2057 => 867,  2055 => 866,  2052 => 865,  2046 => 863,  2044 => 862,  2041 => 861,  2035 => 859,  2033 => 858,  2030 => 857,  2024 => 855,  2022 => 854,  2016 => 851,  2013 => 850,  2010 => 849,  1996 => 847,  1992 => 845,  1989 => 844,  1984 => 843,  1979 => 842,  1976 => 841,  1973 => 840,  1970 => 839,  1965 => 836,  1962 => 835,  1959 => 834,  1956 => 833,  1953 => 832,  1950 => 831,  1947 => 830,  1944 => 829,  1941 => 828,  1938 => 827,  1935 => 826,  1932 => 825,  1929 => 824,  1927 => 823,  1922 => 822,  1920 => 821,  1918 => 820,  1914 => 818,  1908 => 815,  1905 => 814,  1902 => 813,  1897 => 809,  1890 => 803,  1878 => 798,  1875 => 797,  1868 => 796,  1866 => 795,  1863 => 794,  1853 => 793,  1849 => 792,  1845 => 791,  1842 => 790,  1840 => 789,  1835 => 786,  1829 => 783,  1826 => 782,  1824 => 781,  1820 => 779,  1814 => 777,  1812 => 776,  1800 => 767,  1794 => 764,  1787 => 759,  1780 => 758,  1777 => 757,  1773 => 756,  1769 => 755,  1758 => 754,  1754 => 753,  1751 => 752,  1747 => 751,  1740 => 750,  1737 => 749,  1726 => 748,  1722 => 747,  1718 => 746,  1714 => 745,  1711 => 744,  1703 => 738,  1699 => 737,  1692 => 732,  1686 => 728,  1675 => 726,  1671 => 725,  1667 => 724,  1660 => 721,  1658 => 720,  1654 => 718,  1651 => 717,  1644 => 715,  1631 => 709,  1624 => 707,  1617 => 706,  1615 => 705,  1612 => 704,  1599 => 698,  1592 => 696,  1585 => 695,  1583 => 694,  1580 => 693,  1567 => 687,  1560 => 685,  1553 => 684,  1551 => 683,  1548 => 682,  1540 => 679,  1532 => 678,  1528 => 677,  1521 => 676,  1519 => 675,  1516 => 674,  1504 => 671,  1498 => 670,  1491 => 669,  1489 => 668,  1486 => 667,  1474 => 664,  1468 => 663,  1461 => 662,  1459 => 661,  1456 => 660,  1449 => 655,  1440 => 650,  1434 => 647,  1430 => 645,  1419 => 640,  1412 => 639,  1408 => 638,  1404 => 637,  1400 => 635,  1391 => 634,  1387 => 633,  1381 => 632,  1375 => 631,  1370 => 629,  1365 => 627,  1361 => 626,  1355 => 625,  1352 => 624,  1349 => 623,  1347 => 622,  1343 => 621,  1339 => 620,  1332 => 619,  1330 => 618,  1327 => 617,  1320 => 612,  1311 => 607,  1307 => 606,  1301 => 603,  1297 => 601,  1279 => 596,  1275 => 595,  1272 => 594,  1263 => 593,  1259 => 592,  1253 => 591,  1247 => 590,  1242 => 588,  1237 => 586,  1233 => 585,  1227 => 584,  1224 => 583,  1221 => 582,  1219 => 581,  1215 => 580,  1211 => 579,  1204 => 578,  1202 => 577,  1199 => 576,  1194 => 573,  1187 => 571,  1180 => 569,  1178 => 568,  1171 => 567,  1167 => 566,  1163 => 565,  1157 => 564,  1151 => 563,  1144 => 562,  1142 => 561,  1139 => 560,  1135 => 559,  1131 => 557,  1125 => 554,  1108 => 541,  1099 => 535,  1074 => 513,  1065 => 507,  1053 => 497,  1036 => 484,  1025 => 476,  1013 => 467,  1000 => 457,  995 => 455,  989 => 452,  982 => 447,  968 => 444,  964 => 443,  960 => 442,  956 => 441,  952 => 440,  944 => 437,  940 => 436,  936 => 435,  931 => 433,  927 => 432,  922 => 429,  919 => 428,  916 => 427,  913 => 426,  902 => 419,  889 => 408,  883 => 407,  876 => 406,  872 => 405,  858 => 393,  852 => 392,  845 => 391,  841 => 390,  827 => 378,  821 => 377,  814 => 376,  810 => 375,  789 => 356,  781 => 354,  779 => 353,  776 => 352,  772 => 350,  764 => 348,  762 => 347,  756 => 346,  753 => 345,  751 => 344,  741 => 339,  738 => 338,  730 => 336,  728 => 335,  725 => 334,  717 => 332,  715 => 331,  712 => 330,  703 => 328,  700 => 327,  698 => 326,  642 => 272,  636 => 269,  632 => 268,  629 => 267,  627 => 266,  590 => 231,  586 => 230,  574 => 228,  568 => 227,  563 => 225,  559 => 223,  554 => 220,  547 => 218,  542 => 216,  537 => 213,  531 => 211,  528 => 210,  524 => 208,  521 => 207,  511 => 200,  505 => 199,  501 => 197,  494 => 193,  490 => 191,  485 => 189,  480 => 188,  477 => 187,  473 => 186,  464 => 185,  460 => 184,  456 => 183,  452 => 181,  448 => 180,  439 => 179,  435 => 178,  431 => 177,  427 => 176,  424 => 175,  422 => 174,  419 => 173,  415 => 170,  409 => 168,  407 => 167,  404 => 166,  402 => 165,  399 => 164,  394 => 161,  388 => 159,  386 => 158,  382 => 157,  378 => 155,  372 => 154,  365 => 153,  361 => 152,  353 => 146,  348 => 144,  344 => 143,  339 => 142,  337 => 141,  334 => 140,  332 => 139,  330 => 138,  323 => 134,  319 => 133,  314 => 131,  290 => 111,  287 => 109,  274 => 100,  269 => 97,  266 => 96,  263 => 95,  260 => 94,  258 => 93,  255 => 92,  252 => 91,  250 => 90,  247 => 89,  244 => 88,  242 => 87,  239 => 86,  236 => 85,  234 => 84,  231 => 83,  228 => 82,  225 => 81,  222 => 79,  220 => 78,  215 => 77,  209 => 72,  200 => 69,  197 => 68,  194 => 67,  191 => 65,  188 => 64,  185 => 63,  182 => 62,  179 => 61,  176 => 60,  173 => 59,  170 => 58,  167 => 57,  164 => 56,  161 => 55,  158 => 54,  155 => 53,  152 => 52,  149 => 51,  146 => 50,  143 => 49,  138 => 46,  135 => 45,  133 => 44,  130 => 43,  127 => 42,  124 => 41,  121 => 40,  118 => 39,  115 => 38,  112 => 37,  109 => 36,  106 => 35,  103 => 34,  100 => 33,  98 => 32,  92 => 30,  90 => 29,  87 => 28,  81 => 24,  78 => 23,  76 => 22,  73 => 21,  69 => 20,  64 => 19,  61 => 18,  58 => 17,  53 => 16,  50 => 15,  47 => 14,  42 => 13,  39 => 12,  36 => 11,  32 => 10,  29 => 8,  27 => 7,  19 => 1,);
    }
}
/* {{ header }}*/
/* */
/* */
/* 	<link rel="stylesheet" type="text/css" href="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/css/owl.carousel.min.css" />*/
/* <link rel="stylesheet" type="text/css" href="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/css/owl.theme.default.min.css" />*/
/* {#====  Loader breadcrumbs ==== #}*/
/* {% include theme_directory~'/template/soconfig/breadcrumbs.twig' %}*/
/* */
/* {#====  Variables url parameter ==== #}*/
/* {% if url_asidePosition %}{% set col_position = url_asidePosition %}*/
/* {% else %}{% set col_position = soconfig.get_settings('catalog_col_position') %}{% endif %}*/
/* */
/* {% if url_asideType %} {% set col_canvas = url_asideType %}*/
/* {% else %}{% set col_canvas = soconfig.get_settings('catalog_col_type') %}{% endif %}*/
/* */
/* {% if url_productGallery %} {% set productGallery = url_productGallery %}*/
/* {% else %}{% set productGallery = soconfig.get_settings('thumbnails_position') %}{% endif %}*/
/* */
/* {% if url_sidebarsticky %} {% set sidebar_sticky = url_sidebarsticky %}*/
/* {% else %} {% set sidebar_sticky = soconfig.get_settings('catalog_sidebar_sticky') %}{% endif %}*/
/* */
/* {% set desktop_canvas = col_canvas =='off_canvas' ? 'desktop-offcanvas' : '' %}*/
/* */
/* <div class="content-main container product-detail  {{desktop_canvas}}">*/
/* 	<div class="row">*/
/* 		*/
/* 		{#==== Column Left Outside ==== #}*/
/* */
/* 		{% if col_position== 'outside' %}*/
/* 			{{ column_left }}*/
/* 			*/
/* 			{% if col_canvas =='off_canvas' %}*/
/* 				{% set class_pos = 'col-sm-12' %}*/
/* 	    	{% elseif column_left and column_right %}*/
/* 	    		{% set class_pos = 'col-md-6 col-xs-12 fluid-allsidebar' %}*/
/* 		    {% elseif column_left or column_right %}*/
/* 		    	{% set class_pos = 'col-md-9 col-sm-12 col-xs-12 fluid-sidebar' %}*/
/* 		    {% else %}*/
/* 		    	{% set class_pos = 'col-sm-12' %}*/
/* 		    {% endif %}*/
/* 		{% else %}*/
/* 			{% set class_pos = 'col-sm-12' %}*/
/* 		{% endif %}*/
/* 		{#==== End Column Outside ==== #}*/
/*     	*/
/* 		<div id="content" class="product-view {{class_pos}}"> */
/* 		*/
/* 		{#====  Product Gallery ==== #}*/
/* 		{% if productGallery =='grid' %}*/
/* 			{% set class_left_gallery  = 'col-md-6 col-sm-12 col-xs-12' %}*/
/* 			{% set class_right_gallery = 'col-md-6 col-sm-12 col-xs-12' %}*/
/* 		{% elseif productGallery =='list' %}*/
/* 			{% set class_left_gallery  = 'col-md-5 col-sm-12 col-xs-12' %}*/
/* 			{% set class_right_gallery = 'col-md-7 col-sm-12 col-xs-12' %}*/
/* 		{% elseif productGallery =='left' %}*/
/* 			{% set class_left_gallery  = 'col-md-6 col-sm-12 col-xs-12' %}*/
/* 			{% set class_right_gallery = 'col-md-6 col-sm-12 col-xs-12' %}*/
/* 			{% elseif productGallery =='bottom' %}*/
/* 		{% set class_left_gallery  = 'col-md-5 col-sm-12 col-xs-12' %}*/
/* 			{% set class_right_gallery = 'col-md-7 col-sm-12 col-xs-12' %}*/
/* 		{% else %}*/
/* 			{% set class_left_gallery  = 'col-md-12 col-sm-12 col-xs-12' %}*/
/* 			{% set class_right_gallery = 'col-md-12 col-sm-12 col-xs-12 col-gallery-slider' %}*/
/* 		{% endif %}*/
/* */
/* 		{#====  Button Sidebar canvas==== #}*/
/* 		{% if column_left or column_right %}*/
/* 			{% set class_canvas = col_canvas =='off_canvas' ? '' : 'hidden-lg hidden-md' %}*/
/* 			<a href="javascript:void(0)" class=" open-sidebar {{class_canvas}}"><i class="fa fa-bars"></i>{{ text_sidebar }}</a>*/
/* 			<div class="sidebar-overlay "></div>*/
/* 		{% endif %}*/
/* */
/* */
/* 		<div class="content-product-mainheader clearfix"> */
/* 			<div class="row">	*/
/* 			{#========== Product Left ============#}*/
/* 			<div class="content-product-left  {{ class_left_gallery }}" >*/
/* 				{% if images %}*/
/* 					<div class="so-loadeding" ></div>*/
/* 					{#==== Gallery -  Thumbnails ==== #}*/
/* 					{% if productGallery=='left' %}*/
/* 					 	{% include theme_directory~'/template/product/gallery/gallery-left.twig' %}*/
/* */
/* 					{% elseif productGallery=='bottom' %}*/
/* 						{% include theme_directory~'/template/product/gallery/gallery-bottom.twig' %}*/
/* */
/* 					{% elseif productGallery=='grid' %}*/
/* 						{% include theme_directory~'/template/product/gallery/gallery-grid.twig' %}*/
/* */
/* 					{% elseif productGallery=='list' %}*/
/* 						{% include theme_directory~'/template/product/gallery/gallery-list.twig' %}*/
/* */
/* 					{% elseif productGallery=='slider' %}*/
/* 						{% include theme_directory~'/template/product/gallery/gallery-slider.twig' %}*/
/* 					{% endif %}*/
/* 				{% endif %}*/
/* 				*/
/* 					<div class="col-md-12 pl-0 pr-0">  */
/* 			<div class="cart">*/
/* 			    <input type="button" value="{{ button_cart }}" data-loading-text="{{ text_loading }}" id="button-cart" class="btn btn-mega btn-lg btn-product-page">*/
/* 			 </div>*/
/* 			 	<div class="buynow">*/
/* 			    <input type="button" value="Buy Now" id="button-cart" class="btn btn-mega btn-lg btn-product-buy">*/
/* 			 </div>*/
/* 			 </div>*/
/* 			</div>*/
/* 		*/
/*         	{#========== //Product Left ============#}*/
/* */
/* 			{#========== Product Right ============#}*/
/* 			<div class="content-product-right {{ class_right_gallery }}" itemprop="offerDetails" itemscope itemtype="http://schema.org/Product">*/
/* 			    <div class="cont-right">*/
/* 			    <div class="tabs-nav">*/
/* 			        <ul>*/
/* 			            <li><i class="fa fa-eye"></i>Overview</li>*/
/* 			            <li><i class="fa fa-gears"></i>Specs</li>*/
/* 			            <li><i class="fa fa-video-camera"></i>Video</li>*/
/* 			            <li><i class="fa fa-star"></i>Reviews</li>*/
/* 			            <li><i class="fa fa-cube"></i>Related product</li>*/
/* 			        </ul>*/
/* 			    </div>*/
/* 			    <div class="product-breadcrumb">*/
/* 			    <ul>*/
/*                 <li><a href="http://poorvikabeta.webindia.com/index.php?route=common/home"><i class="fa fa-home"></i></a></li>*/
/*                 <li><a href="http://poorvikabeta.webindia.com/index.php?route=product/product&amp;product_id=30">Canon EOS 5D</a></li>*/
/*               </ul>*/
/*               <span>Add to compare <input type="checkbox" id="product-compare"></span>*/
/*               </div>*/
/* */
/* 				<div class="title-product">*/
/* 						 <h1 itemprop="name">{{heading_title}}</h1>*/
/* 						 <ul class="product-share-links">*/
/* 						     <li><span>Wishlist</span><a onclick="wishlist.add({{ product_id }});"><i class="fa fa-heart"></i></a></li>*/
/* 						     <li><a onclick="compare.add({{ product_id }});"><i class="fa fa-share-alt"></i></a></li>*/
/* 						     </ul>*/
/* 					</div>*/
/* 				*/
/* 				{% if review_status %}*/
/* 					{#======== Review - Rating ========== #}*/
/* 					<div class="box-review"  itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">*/
/* 						{% if count_reviews %}*/
/* 								<meta itemprop="ratingValue" content="{{rating}}">*/
/* 								<meta itemprop="ratingCount" content="{{count_reviews}}">*/
/* 								<meta itemprop="reviewCount" content="{{count_reviews}}">*/
/* 						{% endif %}*/
/* 						*/
/* 						<div class="rating">*/
/* 						    <div class="rating-show">*/
/* 						        <h6>4.5</h6>*/
/* 						    </div>*/
/* 							<div class="rating-box">*/
/* 							{% for i in 1..5 %}*/
/* 								{% if rating < i %}<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>{% else %}<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>{% endif %}*/
/* 							{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/* 						<a class="reviews_button" href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;">{{ reviews }}</a>*/
/* 						{% if soconfig.get_settings('product_order') %}*/
/* 									<span class="order-num">{{orders}}</span>*/
/* 						{% endif %}*/
/* 					*/
/* 					</div>*/
/* 					{% endif %}*/
/* */
/* 				{% if price %}*/
/* */
/*                 {% if (text_discount_applied is defined and text_discount_applied) %}*/
/*                 <h4><span class="bg-warning text-warning">{{ text_discount_applied }}</span></h4>*/
/*                 {% endif %}*/
/*                 */
/*                 */
/* 					{#========= Product - Price ========= #}*/
/* 					<div class="product_page_price price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">*/
/* 						{% if not special %}*/
/* 							<span class="price-new">*/
/* 								<span itemprop="price" content="{{ price_value }}" id="price-old"> */
/*  {% if (cfp_setting.module_so_call_for_price_status and price_0 <= 0) %} */
/*  {% if cfp_setting.module_so_call_for_price_replace_cart is defined and cfp_setting.module_so_call_for_price_replace_cart == '0' %} */
/*  <a data-fancybox data-type="ajax" data-src="{{ base }}index.php?route=extension/module/so_call_for_price&product_id={{ product.product_id }}" href="javascript:;" class="callforprice" style="color: #ff0000; font-weight: bold; font-size: 18px;"><i class="fa fa-phone" style="font-size: 18px;"></i> {{ text_price_0 }}</a> */
/*  {% endif %} */
/*  {% else %} */
/*   */
/*  {% if (cfp_setting.module_so_call_for_price_status and price_0 <= 0) %} */
/*  {% if cfp_setting.module_so_call_for_price_replace_cart is defined and cfp_setting.module_so_call_for_price_replace_cart == '0' %} */
/*  <a data-fancybox data-type="ajax" data-src="{{ base }}index.php?route=extension/module/so_call_for_price&product_id={{ product.product_id }}" href="javascript:;" class="callforprice" style="color: #ff0000; font-weight: bold;"><i class="fa fa-phone"></i> {{ text_price_0 }}</a> */
/*  {% endif %} */
/*  {% else %} */
/*  {{ price }} */
/*  {% endif %} */
/*   */
/*  {% endif %} */
/*  </span>*/
/* 								<meta itemprop="priceCurrency" content="{{currency}}" />*/
/* 							</span>*/
/* */
/* 						{% else %}*/
/* 						*/
/* 							<span class="price-new">*/
/* 								<span itemprop="price" content="{{special_value}}" id="price-special">{{ special }}</span>*/
/* 								<meta itemprop="priceCurrency" content="{{currency}}" />*/
/* 							</span>*/
/* 						   <span class="price-old" id="price-old"> */
/* 								*/
/* 						   </span>*/
/* 						   */
/* 						{% endif %}*/
/* 						*/
/* 						{% if special and soconfig.get_settings('discount_status')   %} */
/* 						{#=======Discount Label======= #}*/
/* 						<span class="label-product label-sale">*/
/* 							 {{ discount }}*/
/* 						</span>*/
/* 						{% endif %} */
/* */
/* 						 */
/*  {% if ((tax) and (cfp_setting.module_so_call_for_price_status) and (price_0 > 0)) %} */
/*  */
/* 							<div class="price-tax"><span>{{ text_tax }}</span> <span id="price-tax"> {{ tax }} </span></div>*/
/* 						{% endif %}*/
/* 					 */
/* 					</div>*/
/* 					{% endif %}*/
/* 					*/
/* */
/* 				{% if discounts %} */
/* 					<ul class="list-unstyled text-success">*/
/* 					{% for discount in discounts %} */
/* 						<li><strong>{{ discount.quantity }} {{ text_discount }} {{ discount.price }}</strong> </li>*/
/* 					{% endfor %}*/
/* 					</ul>*/
/* 				{% endif %} 	*/
/* */
/* 				<div class="col-md-6 pl-0">*/
/* 					    <div class="product-color">*/
/* 					        <h3>Color:</h3>*/
/* 					        <div id="color-1" class="product-color-change tabcontent product-color-show">*/
/* 					            <span>White</span>*/
/* 					        </div>*/
/* 					        <div id="color-2" class="product-color-change tabcontent color2">*/
/* 					            <span>Black</span>*/
/* 					        </div>*/
/* 					        <div id="color-3" class="product-color-change tabcontent color3">*/
/* 					            <span>Green</span>*/
/* 					        </div>*/
/* 					        <div id="color-4" class="product-color-change tabcontent color4">*/
/* 					            <span>Blue</span>*/
/* 					        </div>*/
/* 					        <ul>*/
/* 					            <li><div class="hover-color hover-color1"><span class="product-hover-color">White</span></div><button class="tablinks"  onclick="openCity(event, 'color-1')"><img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/product-color1.jpg"></button></li>*/
/* 					            <li><div class="hover-color hover-color2"><span class="product-hover-color">Black</span></div><button  class="tablinks" onclick="openCity(event, 'color-2')"><img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/product-color2.jpg"></button></li>*/
/* 					            <li><div class="hover-color hover-color3"><span class="product-hover-color">Green</span></div><button  class="tablinks" onclick="openCity(event, 'color-3')"><img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/product-color3.jpg"></button></li>*/
/* 					            <li><div class="hover-color hover-color4"><span class="product-hover-color">Blue</span></div><button  class="tablinks" onclick="openCity(event, 'color-4')"><img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/product-color4.jpg"></button></li>*/
/* 					        </ul>*/
/* 					    </div>*/
/* 					</div>*/
/* 					<div class="col-md-6">*/
/* 					    <div class="product-storage">*/
/* 					        <h3>Storage</h3>*/
/* 					        <ul>*/
/* 					            <li><strong>8GB+128GB</strong><span>RAM</span><small>Storage</small></li>*/
/* 					            <li><strong>8GB+256GB</strong><span>RAM</span><small>Storage</small></li>*/
/* 					            <li><strong>8GB+512GB</strong><span>RAM</span><small>Storage</small></li>*/
/* 					        </ul>*/
/* 					    </div>*/
/* 					</div>*/
/* 				{% if description_short %}*/
/* 					 <div class="short_description form-group" itemprop="description">*/
/* 						<h3>{{objlang.get('text_overview')}}</h3>*/
/* 		                {{ description_short }}       */
/* 					</div>*/
/* 					{% endif %}*/
/* 					*/
/* 					<div class="Product-offers">*/
/* 					    <h3>Available Offers</h3>*/
/* 					    <ul>*/
/* 					        <li><i class="fa fa-percent"></i><strong>Bank Offer</strong>flat 30% discount</li>*/
/* 					        <li><i class="fa fa-percent"></i><strong>Bank Offer</strong>5% offer in Axis bank</li>*/
/* 					    </ul>*/
/* 					</div>*/
/* 					*/
/* 					<div class="product-delivery">*/
/* 					    <h3>Delivery</h3>*/
/* 					   */
/* 					    <div class="pincode">*/
/* 					        <span><i class="fa fa-map-marker"></i></span> */
/* 					        <input type="number" id="fname" name="fname">*/
/* 					        <strong><a href="#">change</strong></a>*/
/* 					   */
/* 					   </div>*/
/* 					   */
/* 					    <ul>*/
/* 					        */
/* 					        <li><img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/deliver-img1.png"><span>2 hours</span></li>*/
/* 					        <li><img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/deliver-img2.png"><span>Regular</span></li>*/
/* 					        <li><img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/deliver-img3.png"><span>Pickup@store</span></li>*/
/* 					   </ul>*/
/* 					    */
/* 					</div>*/
/* 					<div class="col-md-6">*/
/* 					    <div class="product-exchange">*/
/* 					        <a href="#">*/
/* 					        <i class="fa fa-exchange"></i>*/
/* 					        <span>With exchange<strong>Up to &#x20B9; 14000</strong> <i class="fa fa-angle-right"></i></span>*/
/* 					        */
/* 					        </a>*/
/* 					    </div>*/
/* 					</div>*/
/* 					<div class="col-md-6">*/
/* 					    <div class="product-exchange product-emi">*/
/* 					        <a href="#">*/
/* 					        <i class="fa fa-percent"></i>*/
/* 					        <span>No cost EMI @<strong> &#x20B9; 4000/month</strong> <i class="fa fa-angle-right"></i></span>*/
/* 					        */
/* 					        </a>*/
/* 					    </div>*/
/* 					</div>*/
/* 					<div class="col-md-12 pr-0 pl-0">*/
/* 					    <div class="specs-block">*/
/* 					    <div class="product-specification">*/
/* 					         <h2><img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/product-spec.png">Specifications</h2>*/
/* 					    </div>*/
/* 					    */
/* 					<div class="product-box-desc">*/
/* 					<div class="inner-box-desc">*/
/* */
/* 						{% if manufacturer %}*/
/* 							        <div class="brand ptb-10" itemprop="brand" itemscope itemtype="http://schema.org/Brand">*/
/* 							        <span>{{ text_manufacturer }} </span><a href="{{ manufacturers }}" itemprop="url"><span itemprop="name">{{ manufacturer }} </span></a></div>*/
/* 							{% endif %}*/
/* 						*/
/* 						{% if model %}*/
/* 						<div class="model ptb-10"><span>{{ text_model }} </span> {{ model }}</div>*/
/* 						{% endif %}*/
/* 						*/
/* 						{% if reward %}*/
/* 							<div class="reward ptb-10"><span>{{ text_reward }}</span> {{ reward }}</div>*/
/* 						{% endif %}*/
/* 						*/
/* 						<div class="stock ptb-10"><span>{{ text_stock }}</span> <i class="fa fa-check-square-o"></i> {{ stock }}</div>	*/
/* 					</div>	*/
/* 					*/
/* 					*/
/* */
/* 					{% if soconfig.get_settings('product_enablesold')   %}*/
/* 					<div class="inner-box-sold ">*/
/* 						<div class="viewed"><span>{{ text_viewed }}</span> <span class="label label-primary">{{ viewed }}</span></div>	*/
/* 						{% if sold %}*/
/* 						<div class="sold"><span>{{ text_sold_ready }}</span> <span class="label label-success"> {{ sold }} </span></div>	*/
/* 						{% endif %}*/
/* 					</div>	*/
/* 					{% endif %}*/
/* 					*/
/* 					{% if soconfig.get_settings('product_enablesizechart') %}*/
/* 						<a class="image-popup-sizechart" href="image/{{soconfig.get_settings('img_sizechart')}}" >{{ text_size_chart }} </a>	*/
/* 				    {% endif %}*/
/* */
/* 				</div>*/
/* 				</div>*/
/* 				*/
/* 				</div>*/
/* 				*/
/* 					<div class="col-md-12 pr-0 pl-0">*/
/* 			    <div class="product-combo">*/
/* 			        <div class="combo-title">*/
/* 			            <h2>Buy Together Combo Offer</h2>*/
/* 			        </div>*/
/* 			        <ul>*/
/* 			            <li><div class="plus-ico"><img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/plus-ico.png"></div>*/
/* 			                <div class="combo-checked-box"><input type="checkbox" id="combo-box"></div>*/
/* 			                <div class="combo-offer-img"><img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/combo-offer-img.jpg"></div>*/
/* 			                <div class="combo-list"><img src="http://poorvikabeta.webindia.com/image/cache/catalog/demo/canon_eos_5d_1-355x470.jpg"></div>*/
/* 			                <div class="combo-content">*/
/* 			                    <h5>Canon EOS 5D</h5>*/
/* 			                    <strong>4.5*/
/* 			                    {% for i in 1..5 %}*/
/* 								{% if rating < i %}<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>{% else %}<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>{% endif %}*/
/* 							{% endfor %}*/
/* 							</strong> */
/* 							<small class="price">&#8377; 45,000<strike class="old-price">25,000</strike></small>*/
/* 							</div>*/
/* 			            */
/* 			            </li>*/
/* 			             <li><div class="plus-ico"><img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/plus-ico.png"></div>*/
/* 			                   <div class="combo-checked-box"><input type="checkbox" id="combo-box"></div>*/
/* 			                <div class="combo-offer-img"><img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/combo-offer-img.jpg"></div>*/
/* 			                <div class="combo-list"><img src="http://poorvikabeta.webindia.com/image/cache/catalog/demo/canon_eos_5d_1-355x470.jpg"></div>*/
/* 			                <div class="combo-content">*/
/* 			                    <h5>Canon EOS 5D</h5>*/
/* 			                    <strong>4.5*/
/* 			                    {% for i in 1..5 %}*/
/* 								{% if rating < i %}<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>{% else %}<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>{% endif %}*/
/* 							{% endfor %}*/
/* 							</strong> */
/* 							<small class="price">&#8377; 45,000<strike class="old-price">25,000</strike></small>*/
/* 							</div>*/
/* 			            */
/* 			            </li>*/
/* 			             <li><div class="plus-ico"><img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/equal-ico.png"></div>*/
/* 			                   <div class="combo-checked-box"><input type="checkbox" id="combo-box"></div>*/
/* 			                <div class="combo-offer-img"><img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/combo-offer-img.jpg"></div>*/
/* 			                <div class="combo-list"><img src="http://poorvikabeta.webindia.com/image/cache/catalog/demo/canon_eos_5d_1-355x470.jpg"></div>*/
/* 			                <div class="combo-content">*/
/* 			                    <h5>Canon EOS 5D</h5>*/
/* 			                    <strong>4.5*/
/* 			                    {% for i in 1..5 %}*/
/* 								{% if rating < i %}<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>{% else %}<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>{% endif %}*/
/* 							{% endfor %}*/
/* 							</strong> */
/* 							<small class="price">&#8377; 45,000<strike class="old-price">25,000</strike></small>*/
/* 							</div>*/
/* 			            */
/* 			            </li>*/
/* 			             <li class="combo-offer-bg">*/
/* 			                <h4>Offer Summary</h4>*/
/* 			                <strike class="old-price">25,000</strike>*/
/* 			                	<small class="price">&#8377; 45,000</small>*/
/* 			               <strong>You save 20,600<span>on 2 items</span></strong>*/
/* 			               */
/* 			            <div class="cart"><input type="button" value="{{ button_cart }}" data-loading-text="{{ text_loading }}" id="button-cart" class="btn btn-mega btn-lg btn-offer"></div>*/
/* 			            </li>*/
/* 			        </ul>*/
/* 			    </div>*/
/* 			</div>*/
/* */
/* 				{#===== Show CountDown Product =======#}*/
/* 				{% if soconfig.get_settings('countdown_status') and special_end_date %}*/
/* 					{% include theme_directory~'/template/soconfig/countdown.twig' with {product: product,special_end_date:special_end_date} %}*/
/* 				{% endif %}*/
/* 				*/
/* 				*/
/* 				<div id="product">	*/
/* 					{% if options %} */
/* 					<h3>{{ text_option }}</h3>*/
/*  */
/*  {% if option_data and option_data.product_option_value is defined and option_data.product_option_value %} */
/*  <ul id="so-colorswatch-selector-{{ product_id }}" class='so-colorswatch-productpage-icons'> */
/*  {% for option_value in option_data.product_option_value %} */
/*  <li class="option-item"> */
/*  <a class="" */
/*  data-product-option-value-id="{{ option_value.product_option_value_id }}" */
/*  data-option-value-id="{{ option_value.option_value_id }}" */
/*  data-color-image="{{ option_value.color_image }}" */
/*  data-color-thumb-image="{{ option_value.color_thumb_image }}" */
/*  style="width: {{ width_product_page }}px; height: {{ height_product_page }}px; background-image: url('{{ option_value.image }}')"> */
/*  </a> */
/*  </li> */
/*  {% endfor %} */
/*  <li class="selected-option"><span></span></li> */
/*  </ul> */
/*  <script type="text/javascript"> */
/*  var $window_width = $(window).width(); */
/*  var ProductOptionId = '{{ product_option_id }}'; */
/*  var default_image = $('.large-image img').attr('src'); */
/*  jQuery(document).ready(function($) { */
/*  $('#input-option{{ product_option_id }}').parent().hide(); */
/*  */
/*  $('#input-option{{ product_option_id}} option').each(function(){ */
/*  var text = $(this).text().replace(/\s{2,}/g, ' '); */
/*  var val = $(this).attr('value'); */
/*  $('.so-colorswatch-productpage-icons li a').each(function(index, el){ */
/*  if($(el).data('product-option-value-id')== val){ */
/*  $(el).attr('title', text); */
/*  } */
/*  }) */
/*  }) */
/*  */
/*  {% if colorswatch_type == 'click' %} */
/*  $(document).on('click', '.so-colorswatch-productpage-icons li.option-item', function(e){ */
/*  e.preventDefault(); */
/*  var option_value_id = $(this).children('a').data('product-option-value-id'); */
/*  var option_id = $(this).children('a').data('option-value-id'); */
/*  */
/*  if ($(this).hasClass('checked')) { */
/*  $('.so-colorswatch-productpage-icons li.option-item').removeClass('checked'); */
/*  $(this).removeClass('checked'); */
/*  $('#input-option{{ product_option_id }}').val('').trigger('change'); */
/*  $('.so-colorswatch-productpage-icons li.selected-option > span').html(''); */
/*  */
/*  $('.large-image img').attr('src', default_image); */
/*  } */
/*  else { */
/*  $('.so-colorswatch-productpage-icons li.option-item').removeClass('checked'); */
/*  $(this).removeClass('checked').addClass('checked'); */
/*  $('#input-option{{ product_option_id }}').val(option_value_id).trigger('change'); */
/*  $('.so-colorswatch-productpage-icons li.selected-option > span').html($(this).children('a').attr('title')); */
/*  */
/*  if ($(this).children('a').data('color-image') != '') { */
/*  $('.large-image img').attr('src', $(this).children('a').data('color-image')); */
/*  } */
/*  else { */
/*  $('.large-image img').attr('src', default_image); */
/*  } */
/*  */
/*  $('#thumb-slider a.thumbnail').removeClass('active'); */
/*  } */
/*  }) */
/*  {% else %} */
/*  if ($window_width > 1199) { */
/*  $('.so-colorswatch-productpage-icons li.option-item').hover(function(e){ */
/*  e.preventDefault(); */
/*  var option_value_id = $(this).children('a').data('product-option-value-id'); */
/*  var option_id = $(this).children('a').data('option-value-id'); */
/*  */
/*  $('.so-colorswatch-productpage-icons li.option-item').removeClass('checked'); */
/*  if ($(this).hasClass('checked')) { */
/*  $(this).removeClass('checked'); */
/*  $('#input-option{{ product_option_id }}').val('').trigger('change'); */
/*  $('.large-image img').attr('src', default_image); */
/*  */
/*  } */
/*  else { */
/*  $(this).removeClass('checked').addClass('checked'); */
/*  $('#input-option{{ product_option_id }}').val(option_value_id).trigger('change'); */
/*  $('.so-colorswatch-productpage-icons li.selected-option > span').html($(this).children('a').attr('title')); */
/*  */
/*  if ($(this).children('a').data('color-image') != '') { */
/*  $('.large-image img').attr('src', $(this).children('a').data('color-image')); */
/*  } */
/*  else { */
/*  $('.large-image img').attr('src', default_image); */
/*  } */
/*  $('#thumb-slider a.thumbnail').removeClass('active'); */
/*  } */
/*  }); */
/*  } */
/*  else { */
/*  $(document).on('click', '.so-colorswatch-productpage-icons li.option-item', function(e){ */
/*  e.preventDefault(); */
/*  var option_value_id = $(this).children('a').data('product-option-value-id'); */
/*  var option_id = $(this).children('a').data('option-value-id'); */
/*  */
/*  $('.so-colorswatch-productpage-icons li.option-item').removeClass('checked'); */
/*  if ($(this).hasClass('checked')) { */
/*  $(this).removeClass('checked'); */
/*  $('#input-option{{ product_option_id }}').val('').trigger('change'); */
/*  $('.large-image img').attr('src', default_image); */
/*  */
/*  } */
/*  else { */
/*  $(this).removeClass('checked').addClass('checked'); */
/*  $('#input-option{{ product_option_id }}').val(option_value_id).trigger('change'); */
/*  $('.so-colorswatch-productpage-icons li.selected-option > span').html($(this).children('a').attr('title')); */
/*  */
/*  if ($(this).children('a').data('color-image') != '') { */
/*  $('.large-image img').attr('src', $(this).children('a').data('color-image')); */
/*  } */
/*  else { */
/*  $('.large-image img').attr('src', default_image); */
/*  } */
/*  $('#thumb-slider a.thumbnail').removeClass('active'); */
/*  } */
/*  }) */
/*  } */
/*  {% endif %} */
/*  }) */
/*  </script> */
/*  {% endif %} */
/*  */
/* 					{% for option in options %}*/
/* 						*/
/* 						{% if option.type == 'select' %}*/
/* 						<div class="form-group{% if option.required %} required {% endif %}">*/
/* 							<label class="control-label" for="input-option{{ option.product_option_id }}">{{ option.name }}</label>*/
/* 							<select name="option[{{ option.product_option_id }}]" id="input-option{{ option.product_option_id }}" class="form-control width50">*/
/* 								<option value="">{{ text_select }}</option>*/
/* 							{% for option_value in option.product_option_value %}*/
/* 								<option value="{{ option_value.product_option_value_id }}">{{ option_value.name }}*/
/* 								{% if option_value.price %}*/
/* 									({{ option_value.price_prefix }}{{ option_value.price }})*/
/* 								{% endif %}*/
/* 								</option>*/
/* 							{% endfor %}*/
/* 						  </select>*/
/* 						</div>*/
/* 						{% endif %}*/
/* 						*/
/* 						{% if option.type == 'radio' %}*/
/* 						<div class="form-group{% if option.required %} required {% endif %}">*/
/* 						  	<label class="control-label">{{ option.name }}</label>*/
/* 							<div id="input-option{{ option.product_option_id }}">*/
/* 								{% set radio_style 	 = soconfig.get_settings('radio_style') %}*/
/* 								{% set radio_type 	 = radio_style ? ' radio-type-button':'' %}*/
/* */
/* 								{% for option_value in option.product_option_value %} */
/* 								{% set radio_image 	=  option_value.image ? 'option_image' : '' %} */
/* 								{% set radio_price 	=  radio_style ? option_value.price_prefix ~ option_value.price : '' %} */
/* 								*/
/* 									<div class="radio {{ radio_image ~ radio_type }}">*/
/* 										<label>							*/
/* 											<input type="radio" name="option[{{ option.product_option_id }}]" value="{{ option_value.product_option_value_id }}" />*/
/* 											<span class="option-content-box" data-title="{{ option_value.name}} {{ radio_price }}" data-toggle='tooltip'>*/
/* 												{% if option_value.image %} */
/* 													<img src="{{ option_value.image }} " alt="{{ option_value.name}}  {{radio_price}}" /> */
/* 												{% endif %} */
/* 												<span class="option-name">{{ option_value.name }} </span>*/
/* 												{% if option_value.price  and  radio_style  != '1' %} ({{ option_value.price_prefix }} {{ option_value.price }} ){% endif %} */
/* 											  */
/* 											</span>*/
/* 										</label>*/
/* 									</div>*/
/* 								{% endfor %}	*/
/* 								 */
/* 								{% if radio_style %} */
/* 								<script type="text/javascript">*/
/* 									 $(document).ready(function(){*/
/* 										  $('#input-option{{ option.product_option_id }} ').on('click', 'span', function () {*/
/* 											   $('#input-option{{ option.product_option_id }}  span').removeClass("active");*/
/* 											   $(this).toggleClass("active");*/
/* 										  });*/
/* 									 });*/
/* 								</script>*/
/* 								{% endif %} */
/* */
/* 							</div>*/
/* 						</div>*/
/* 						{% endif %}*/
/* */
/* 						{% if option.type == 'checkbox' %}*/
/* 						<div class="form-group{% if option.required %} required {% endif %}">*/
/* 						  	<label class="control-label">{{ option.name }}</label>*/
/* 						  	<div id="input-option{{ option.product_option_id }}">*/
/* 								{% set radio_style 	 = soconfig.get_settings('radio_style') %}*/
/* 								{% set radio_type 	 = radio_style ? ' radio-type-button':'' %}*/
/* */
/* 								{% for option_value in option.product_option_value %} */
/* 								{% set radio_image 	=  option_value.image ? 'option_image' : '' %} */
/* 								{% set radio_price 	=  radio_style ? option_value.price_prefix ~ option_value.price : '' %} */
/* 								*/
/* 									<div class="checkbox  {{ radio_image ~ radio_type }}">*/
/* 										<label>*/
/* 											<input type="checkbox" name="option[{{ option.product_option_id }}][]" value="{{ option_value.product_option_value_id }}" />*/
/* 											<span class="option-content-box" data-title="{{ option_value.name}} {{ radio_price }}" data-toggle='tooltip'>*/
/* 												{% if option_value.image %} */
/* 													<img src="{{ option_value.image }} " alt="{{ option_value.name}}  {{radio_price}}" /> */
/* 												{% endif %} */
/* */
/* 												<span class="option-name">{{ option_value.name }} </span>*/
/* 												{% if option_value.price  and  radio_style  != '1' %} */
/* 													({{ option_value.price_prefix }} {{ option_value.price }} )*/
/* 												{% endif %} */
/* 											  */
/* 											</span>*/
/* 										</label>*/
/* 									</div>*/
/* 								{% endfor %}	*/
/* 								 */
/* 								{% if radio_style %} */
/* 								<script type="text/javascript">*/
/* 									 $(document).ready(function(){*/
/* 										  $('#input-option{{ option.product_option_id }} ').on('click', 'span', function () {*/
/* 											   $(this).toggleClass("active");*/
/* 										  });*/
/* 									 });*/
/* 								</script>*/
/* 								{% endif %} */
/* */
/* 							</div>*/
/* 						</div>*/
/* 						{% endif %}*/
/* */
/* 						{% if option.type == 'text' %}*/
/* 						<div class="form-group{% if option.required %} required {% endif %}">*/
/* 						  <label class="control-label" for="input-option{{ option.product_option_id }}">{{ option.name }}</label>*/
/* 						  <input type="text" name="option[{{ option.product_option_id }}]" value="{{ option.value }}" placeholder="{{ option.name }}" id="input-option{{ option.product_option_id }}" class="form-control" />*/
/* 						</div>*/
/* 						{% endif %}*/
/* */
/* 						{% if option.type == 'textarea' %}*/
/* 						<div class="form-group{% if option.required %} required {% endif %}">*/
/* 						  <label class="control-label" for="input-option{{ option.product_option_id }}">{{ option.name }}</label>*/
/* 						  <textarea name="option[{{ option.product_option_id }}]" rows="5" placeholder="{{ option.name }}" id="input-option{{ option.product_option_id }}" class="form-control">{{ option.value }}</textarea>*/
/* 						</div>*/
/* 						{% endif %}*/
/* */
/* 						{% if option.type == 'file' %}*/
/* 						<div class="form-group{% if option.required %} required {% endif %}">*/
/* 						  <label class="control-label">{{ option.name }}</label>*/
/* 						  <button type="button" id="button-upload{{ option.product_option_id }}" data-loading-text="{{ text_loading }}" class="btn btn-default btn-block"><i class="fa fa-upload"></i> {{ button_upload }}</button>*/
/* 						  <input type="hidden" name="option[{{ option.product_option_id }}]" value="" id="input-option{{ option.product_option_id }}" />*/
/* 						</div>*/
/* 						{% endif %}*/
/* */
/* 						{% if option.type == 'date' %}*/
/* 						<div class="form-group{% if option.required %} required {% endif %}">*/
/* 						  <label class="control-label" for="input-option{{ option.product_option_id }}">{{ option.name }}</label>*/
/* 						  <div class="input-group date">*/
/* 							<input type="text" name="option[{{ option.product_option_id }}]" value="{{ option.value }}" data-date-format="YYYY-MM-DD" id="input-option{{ option.product_option_id }}" class="form-control" />*/
/* 							<span class="input-group-btn">*/
/* 							<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>*/
/* 							</span></div>*/
/* 						</div>*/
/* 						{% endif %}*/
/* */
/* 						{% if option.type == 'datetime' %}*/
/* 						<div class="form-group{% if option.required %} required {% endif %}">*/
/* 						  <label class="control-label" for="input-option{{ option.product_option_id }}">{{ option.name }}</label>*/
/* 						  <div class="input-group datetime">*/
/* 							<input type="text" name="option[{{ option.product_option_id }}]" value="{{ option.value }}" data-date-format="YYYY-MM-DD HH:mm" id="input-option{{ option.product_option_id }}" class="form-control" />*/
/* 							<span class="input-group-btn">*/
/* 							<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>*/
/* 							</span></div>*/
/* 						</div>*/
/* 						{% endif %}*/
/* 						*/
/* 						{% if option.type == 'time' %}*/
/* 						<div class="form-group{% if option.required %} required {% endif %}">*/
/* 							<label class="control-label" for="input-option{{ option.product_option_id }}">{{ option.name }}</label>*/
/* 							<div class="input-group time">*/
/* 							<input type="text" name="option[{{ option.product_option_id }}]" value="{{ option.value }}" data-date-format="HH:mm" id="input-option{{ option.product_option_id }}" class="form-control" />*/
/* 							<span class="input-group-btn">*/
/* 							<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>*/
/* 							</span></div>*/
/* 						</div>*/
/* 						{% endif %}*/
/* 						*/
/* 					{% endfor %}*/
/* 					{% endif %}*/
/* */
/* 					<div class="box-cart clearfix form-group">*/
/* 						{% if recurrings %}*/
/* 						<h3>{{ text_payment_recurring }}</h3>*/
/* 						<div class="form-group required">*/
/* 							<select name="recurring_id" class="form-control">*/
/* 							<option value="">{{ text_select }}</option>*/
/* 							{% for recurring in recurrings %}*/
/* 							<option value="{{ recurring.recurring_id }}">{{ recurring.name }}</option>*/
/* 							{% endfor %}*/
/* 							</select>*/
/* 						  <div class="help-block" id="recurring-description"></div>*/
/* 						</div>*/
/* 						{% endif %}*/
/* 					  */
/* 						<div class="form-group box-info-product">*/
/* 							<div class="option quantity">*/
/* 								<div class="input-group quantity-control">*/
/* 									  <span class="input-group-addon product_quantity_down fa fa-minus"></span>*/
/* 									  <input class="form-control" type="text" name="quantity" value="{{ minimum }}" />*/
/* 									  <input type="hidden" name="product_id" value="{{ product_id }}" />								  */
/* 									  <span class="input-group-addon product_quantity_up fa fa-plus"></span>*/
/* 								</div>*/
/* 							</div>*/
/* 							<div class="detail-action">*/
/* 								{# =========button Cart ======#}*/
/* 								<div class="cart"> */
/*  {% if (cfp_setting.module_so_call_for_price_status and price_0 <= 0) %} */
/*  {% if cfp_setting.module_so_call_for_price_hide_cart is defined and cfp_setting.module_so_call_for_price_hide_cart == '0' %} */
/*  {% if cfp_setting.module_so_call_for_price_replace_cart is defined and cfp_setting.module_so_call_for_price_replace_cart == '1' %} */
/*  <input type="button" value="{{ text_price_0 }}" data-fancybox data-type="ajax" data-src="{{ base }}index.php?route=extension/module/so_call_for_price&product_id={{ product.product_id }}" data-loading-text="{{ text_loading }}" class="btn btn-mega btn-lg callforprice"> */
/*  {% else %} */
/*  <input type="button" value="{{ button_cart }}" data-loading-text="{{ text_loading }}" class="btn btn-mega btn-lg" style="cursor: default; background: #eee; color: #ccc; border: 1px solid #eee; text-shadow: none; box-shadow: none;"> */
/*  {% endif %} */
/*  {% else %} */
/*  {% if cfp_setting.module_so_call_for_price_replace_cart is defined and cfp_setting.module_so_call_for_price_replace_cart == '1' %} */
/*  <input type="button" value="{{ text_price_0 }}" data-fancybox data-type="ajax" data-src="{{ base }}index.php?route=extension/module/so_call_for_price&product_id={{ product.product_id }}" data-loading-text="{{ text_loading }}" class="btn btn-mega btn-lg "> */
/*  {% endif %} */
/*  {% endif %} */
/*  {% else %} */
/*  <input type="button" value="{{ button_cart }}" data-loading-text="{{ text_loading }}" id="button-cart" class="btn btn-mega btn-lg" /> */
/*  {% endif %} */
/*  </div>*/
/* 								<div class="add-to-links wish_comp">*/
/* 									<ul class="blank">*/
/* 										<li class="wishlist">*/
/* 											<a onclick="wishlist.add({{ product_id }});"><i class="fa fa-heart"></i></a>*/
/* 										</li>*/
/* 										<li class="compare">*/
/* 											<a onclick="compare.add({{ product_id }});"><i class="fa fa-retweet"></i></a>*/
/* 										</li>*/
/* 										*/
/* 									</ul>*/
/* 								</div>*/
/* 							</div>*/
/* 						</div>*/
/* */
/* 						<div class="clearfix"></div>*/
/* 						{% if minimum > 1 %}*/
/* 							<div class="alert alert-info"><i class="fa fa-info-circle"></i> {{ text_minimum }}</div>*/
/* 						{% endif %}*/
/* 					</div>*/
/* */
/* 					{% if soconfig.get_settings('product_page_button') and soconfig.get_settings('product_socialshare') %}*/
/* 					<div class="form-group social-share clearfix">*/
/* 						{{ soconfig.decode_entities( soconfig.get_settings('product_socialshare') ) }}*/
/* 					</div>*/
/* 					{% endif %}*/
/* 					<!-- Go to www.addthis.com/dashboard to customize your tools -->*/
/* 					<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-529be2200cc72db5"></script>*/
/* 					*/
/* 					 {% if tags %}*/
/* 					<div id="tab-tags">*/
/* 						{{ text_tags }}*/
/* 						{% for i in 0..tags|length %}*/
/* 						{% if i < (tags|length - 1) %} <a class="btn btn-primary btn-sm" href="{{ tags[i].href }}">{{ tags[i].tag }}</a>*/
/* 						{% else %} */
/* 						{% if tags[i] is not empty  %}*/
/* 						<a class="btn btn-primary btn-sm 22" href="{{ tags[i].href }}">{{ tags[i].tag }}</a> {% endif %}*/
/* 						{% endif %}*/
/* 						{% endfor %} */
/* 						*/
/* 					 */
/* 					</div>*/
/* 					{% endif %}*/
/* */
/* 				</div>*/
/* 					</div>*/
/* 			</div>*/
/* 		*/
/* 			{#========== //Product Right ============#}*/
/* 			</div>*/
/* 		</div>*/
/* */
/* 		{#====  content_Top==== #}*/
/* 		{% if content_top %}*/
/* 		<div class="content-product-maintop form-group clearfix">*/
/* 			{{ content_top }}*/
/* 		</div>*/
/* 		{% endif %}*/
/* 		<div class="content-product-mainbody clearfix row">*/
/* 			*/
/* 			{% if col_position== 'inside' %}*/
/* 			{#====  Column left inside==== #}*/
/* 				{{ column_left }}*/
/* 			    {% if col_canvas =='off_canvas' %}*/
/* 					{% set class_left = 'col-sm-12' %}*/
/* 		    	{% elseif column_left and column_right %}*/
/* 		    		{% set class_left = 'col-md-6 col-column3' %}*/
/* 			    {% elseif column_left or column_right %}*/
/* 			    	{% set class_left = 'col-md-9 col-sm-12 col-xs-12' %}*/
/* 			    {% else %}*/
/* 			    	{% set class_left = 'col-sm-12' %}*/
/* 			    {% endif %}*/
/* 			{% else %}*/
/* 				{% set class_left = 'col-sm-12' %}*/
/* 			{% endif %}*/
/* */
/* 		    <div class="content-product-content {{ class_left }}">*/
/* 				<div class="content-product-midde clearfix">*/
/* 					{#========== TAB BLOCK ============#}*/
/* 					{% set related_position = soconfig.get_settings('tabs_position') == 1 ? 'vertical-tabs' : ''  %}*/
/* 					{% set tabs_position	= soconfig.get_settings('tabs_position')  %}*/
/* 					{% set showmore			= soconfig.get_settings('product_enableshowmore')  %}*/
/* 					{% if showmore %} {% set class_showmore = 'showdown' %}*/
/* 					{% else %} {% set class_showmore = 'showup' %}*/
/* 					{% endif %}*/
/* */
/* 					<div class="producttab ">*/
/* 						<div class="tabsslider {{related_position}} {% if tabs_position == 1 %} {{'vertical-tabs'}} {% else %} {{'horizontal-tabs'}} {% endif %} col-xs-12">*/
/* 							{#========= Tabs - Bottom horizontal =========#}*/
/* 							{% if tabs_position == 2 %}*/
/* 							<ul class="nav nav-tabs font-sn">*/
/* 								<li class="active"><a data-toggle="tab" href="#tab-description">{{ tab_description }}</a></li>*/
/* 								*/
/* 					         */
/* 					            {% if review_status %}*/
/* 					           	 <li><a href="#tab-review" data-toggle="tab">{{ tab_review }}</a></li>*/
/* 					            {% endif %}*/
/* 								*/
/* 								{% if soconfig.get_settings('product_enableshipping') %}*/
/* 								 <li><a href="#tab-contentshipping" data-toggle="tab">{{ tab_shipping}}</a></li>*/
/* 								{% endif %}*/
/* */
/* 								{% if product_tabtitle %}*/
/* 					           	 <li><a href="#tab-customhtml" data-toggle="tab">{{ product_tabtitle}}</a></li>*/
/* 					            {% endif %}*/
/* */
/* 								{% if product_video %}*/
/* 					           	 <li><a class="thumb-video" href="{{product_video}}"><i class="fa fa-youtube-play fa-lg"></i> {{ tab_video}}</a></li>*/
/* 					            {% endif %}*/
/* 								*/
/* 								*/
/* 								*/
/* 							</ul>*/
/* */
/* 							{#========= Tabs - Left vertical =========#}*/
/* 							{% elseif tabs_position == 1  %}*/
/* 								<ul class="nav nav-tabs col-lg-3 col-sm-4">*/
/* 								<li class="active"><a data-toggle="tab" href="#tab-description">{{ tab_description }}</a></li>*/
/* 								*/
/* 					            {% if review_status %}*/
/* 					           	 <li><a href="#tab-review" data-toggle="tab">{{ tab_review }}</a></li>*/
/* 					            {% endif %}*/
/* 								*/
/* 								{% if soconfig.get_settings('product_enableshipping')  %}*/
/* 								 <li><a href="#tab-contentshipping" data-toggle="tab">{{ tab_shipping}}</a></li>*/
/* 								{% endif %}*/
/* */
/* 								{% if product_tabtitle %}*/
/* 					           	 <li><a href="#tab-customhtml" data-toggle="tab">{{ product_tabtitle}}</a></li>*/
/* 					            {% endif %}*/
/* 					            */
/* 								{% if product_video %}*/
/* 					           	 <li><a class="thumb-video" href="{{product_video}}"><i class="fa fa-youtube-play fa-lg"></i> {{ tab_video}}</a></li>*/
/* 					            {% endif %}*/
/* 								*/
/* 								*/
/* 								</ul>*/
/* 							{% endif %}*/
/* */
/* 							<div class="tab-content {% if tabs_position == 1  %} {{ 'col-lg-9 col-sm-8' }} {% endif %} col-xs-12">*/
/* 								<div class="tab-pane active" id="tab-description">*/
/* 									*/
/* 									{% if attribute_groups %}*/
/* 										<h3 class="product-property-title" > {{text_product_specifics}}</h3>*/
/* 						              	<ul class="product-property-list util-clearfix">*/
/* 							                {% for attribute_group in attribute_groups %}*/
/* 							               */
/* 							                	*/
/* 								                {% for attribute in attribute_group.attribute %}*/
/* 								                <li class="property-item">*/
/* 								                  <span class="propery-title">{{ attribute.name }}</span>*/
/* 								                  <span class="propery-des">{{ attribute.text }}</span>*/
/* 								                </li>*/
/* 								                {% endfor %}*/
/* 							                 	*/
/* 							                {% endfor %}*/
/* 						              	</ul>*/
/* 						            {% endif %}*/
/* */
/* 						            <h3 class="product-property-title" > {{text_product_description}}</h3>*/
/* 						            <div id="collapse-description" class="desc-collapse {{class_showmore}}">*/
/* 										{{ description }}*/
/* 									</div>	*/
/* */
/* 									{% if showmore %}*/
/* 									<div class="button-toggle">*/
/* 								         <a class="showmore" data-toggle="collapse" href="#" aria-expanded="false" aria-controls="collapse-footer">*/
/* 								            <span class="toggle-more">{{ objlang.get('show_more') }} <i class="fa fa-angle-down"></i></span> */
/* 								            <span class="toggle-less">{{ objlang.get('show_less') }} <i class="fa fa-angle-up"></i></span>           */
/* 										</a>        */
/* 									</div>*/
/* 									{% endif %}*/
/* 								</div>*/
/* 								*/
/* */
/* 					            {% if review_status %}*/
/* 					            <div class="tab-pane" id="tab-review">*/
/* 						            <form class="form-horizontal" id="form-review">*/
/* 						                <div id="review"></div>*/
/* 						                <h3>{{ text_write }}</h3>*/
/* 						                {% if review_guest %}*/
/* 						                <div class="form-group required">*/
/* 						                  <div class="col-sm-12">*/
/* 						                    <label class="control-label" for="input-name">{{ entry_name }}</label>*/
/* 						                    <input type="text" name="name" value="{{ customer_name }}" id="input-name" class="form-control" />*/
/* 						                  </div>*/
/* 						                </div>*/
/* 						                <div class="form-group required">*/
/* 						                  <div class="col-sm-12">*/
/* 						                    <label class="control-label" for="input-review">{{ entry_review }}</label>*/
/* 						                    <textarea name="text" rows="5" id="input-review" class="form-control"></textarea>*/
/* 						                    <div class="help-block">{{ text_note }}</div>*/
/* 						                  </div>*/
/* 						                </div>*/
/* 						                <div class="form-group required">*/
/* 						                  <div class="col-sm-12">*/
/* 						                    <label class="control-label">{{ entry_rating }}</label>*/
/* 						                    &nbsp;&nbsp;&nbsp; {{ entry_bad }}&nbsp;*/
/* 						                    <input type="radio" name="rating" value="1" />*/
/* 						                    &nbsp;*/
/* 						                    <input type="radio" name="rating" value="2" />*/
/* 						                    &nbsp;*/
/* 						                    <input type="radio" name="rating" value="3" />*/
/* 						                    &nbsp;*/
/* 						                    <input type="radio" name="rating" value="4" />*/
/* 						                    &nbsp;*/
/* 						                    <input type="radio" name="rating" value="5" />*/
/* 						                    &nbsp;{{ entry_good }}</div>*/
/* 						                </div>*/
/* 						                {{ captcha }}*/
/* 						                */
/* 						                  <div class="pull-right">*/
/* 						                    <button type="button" id="button-review" data-loading-text="{{ text_loading }}" class="btn btn-primary">{{ button_continue }}</button>*/
/* 						                  </div>*/
/* 						               */
/* 						                {% else %}*/
/* 						                {{ text_login }}*/
/* 						                {% endif %}*/
/* 						            </form>*/
/* 					            </div>*/
/* 					            {% endif %}*/
/* */
/* 					            {% if soconfig.get_settings('product_enableshipping') and soconfig.get_settings('product_contentshipping') %}*/
/* 								<div class="tab-pane" id="tab-contentshipping">*/
/* 									{{ soconfig.decode_entities( soconfig.get_settings('product_contentshipping') ) }}*/
/* 								</div>*/
/* 								{% endif %}*/
/* */
/* 								{% if product_tabtitle %}*/
/* 								<div class="tab-pane " id="tab-customhtml">{{ product_tabcontent }}</div>*/
/* 								{% endif %}*/
/* 								*/
/* 							</div>*/
/* 						</div>*/
/* 					</div>*/
/* 				</div>*/
/* 				*/
/* 				{#====  Related_Product==== #}*/
/* 				{% if products and soconfig.get_settings('related_status') %}*/
/* 				<div class="content-product-bottom clearfix">*/
/* 					<ul class="nav nav-tabs">*/
/* 					  <li class="active"><a data-toggle="tab" href="#product-related">{{ text_related }}</a></li> */
/* 					  <li><a data-toggle="tab" href="#product-upsell">{{ text_upsell }}</a></li>*/
/* 					</ul>*/
/* 					<div class="tab-content">*/
/* 					  	<div id="product-related" class="tab-pane fade in active">*/
/* 							{% include theme_directory~'/template/soconfig/related_product.twig' %}*/
/* 					  	</div>*/
/* 					  	<div id="product-upsell" class="tab-pane fade">*/
/* 					  		{#====  content_bottom==== #}*/
/* 					  		{{ content_bottom }}*/
/* 					  	</div>*/
/* 					</div>*/
/* 					*/
/* 				</div>*/
/* 				{% endif %}*/
/* */
/* 				*/
/* 			</div>*/
/* 			{#====  Column Right inside==== #}*/
/* 			{% if col_position== 'inside' %} {{ column_right }} {% endif %}*/
/* */
/* 		</div>*/
/* 		<div class="content-product-main1">*/
/* 		    {#====  Comparision-product==== #}*/
/* 		    <div class="col-md-12">*/
/* 		        <div class="product-combo compare-product">*/
/* 			        <div class="combo-title">*/
/* 			            <h2>Compate With similar Products</h2>*/
/* 			            <a href=""><i class="fa fa-plus"></i>Add Comparison</a>*/
/* 			        </div>*/
/* 			        <div id="collapse-description" class="desc-collapse showdown compare">*/
/* 			        <ul class="">*/
/* 			            <li><div class="free-space"></div>*/
/* 			            <div class="battery-power">*/
/* 			                <strong>Battery</strong>*/
/* 			            </div>*/
/* 			            <div class="external-storage">*/
/* 			                <strong>Expandable storage</strong>*/
/* 			            </div>*/
/* 			             <div class="external-storage">*/
/* 			                <strong>Expandable storage</strong>*/
/* 			            </div>*/
/* 			            </li>*/
/* 			             <li><a href="">*/
/* 			                <div class="combo-list"><img src="http://poorvikabeta.webindia.com/image/cache/catalog/demo/canon_eos_5d_1-355x470.jpg"></div>*/
/* 			                <div class="combo-content">*/
/* 			                    <h5>Canon EOS 5D</h5>*/
/* 			                    <strong>4.5*/
/* 			                    {% for i in 1..5 %}*/
/* 								{% if rating < i %}<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>{% else %}<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>{% endif %}*/
/* 							{% endfor %}*/
/* 							</strong> */
/* 							<small class="price">&#8377; 45,000<strike class="old-price">25,000</strike></small>*/
/* 							</div>*/
/* 			            </a>*/
/* 			             <div class="battery-power">*/
/* 			                <strong>4000 mAH</strong>*/
/* 			            </div>*/
/* 			            <div class="external-storage">*/
/* 			                <strong>128GB</strong>*/
/* 			            </div>*/
/* 			             <div class="external-storage">*/
/* 			                <strong>128GB</strong>*/
/* 			            </div>*/
/* 			            </li>*/
/* 			             <li><a href="">*/
/* 			                <div class="combo-list"><img src="http://poorvikabeta.webindia.com/image/cache/catalog/demo/canon_eos_5d_1-355x470.jpg"></div>*/
/* 			                <div class="combo-content">*/
/* 			                    <h5>Canon EOS 5D</h5>*/
/* 			                    <strong>4.5*/
/* 			                    {% for i in 1..5 %}*/
/* 								{% if rating < i %}<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>{% else %}<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>{% endif %}*/
/* 							{% endfor %}*/
/* 							</strong> */
/* 							<small class="price">&#8377; 45,000<strike class="old-price">25,000</strike></small>*/
/* 							</div>*/
/* 			            </a>*/
/* 			              <div class="battery-power">*/
/* 			                <strong>4000 mAH</strong>*/
/* 			            </div>*/
/* 			            <div class="external-storage">*/
/* 			                <strong>128GB</strong>*/
/* 			            </div>*/
/* 			             <div class="external-storage">*/
/* 			                <strong>128GB</strong>*/
/* 			            </div>*/
/* 			            </li>*/
/* 			             <li><a href="">*/
/* 			                <div class="combo-list"><img src="http://poorvikabeta.webindia.com/image/cache/catalog/demo/canon_eos_5d_1-355x470.jpg"></div>*/
/* 			                <div class="combo-content">*/
/* 			                    <h5>Canon EOS 5D</h5>*/
/* 			                    <strong>4.5*/
/* 			                    {% for i in 1..5 %}*/
/* 								{% if rating < i %}<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>{% else %}<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>{% endif %}*/
/* 							{% endfor %}*/
/* 							</strong> */
/* 							<small class="price">&#8377; 45,000<strike class="old-price">25,000</strike></small>*/
/* 							</div>*/
/* 			            </a>*/
/* 			             <div class="battery-power">*/
/* 			                <strong>4000 mAH</strong>*/
/* 			            </div>*/
/* 			            <div class="external-storage">*/
/* 			                <strong>128GB</strong>*/
/* 			            </div>*/
/* 			             <div class="external-storage">*/
/* 			                <strong>128GB</strong>*/
/* 			            </div>*/
/* 			            </li>*/
/* 			             <li><a href="">*/
/* 			                <div class="combo-list"><img src="http://poorvikabeta.webindia.com/image/cache/catalog/demo/canon_eos_5d_1-355x470.jpg"></div>*/
/* 			                <div class="combo-content">*/
/* 			                    <h5>Canon EOS 5D</h5>*/
/* 			                    <strong>4.5*/
/* 			                    {% for i in 1..5 %}*/
/* 								{% if rating < i %}<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>{% else %}<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>{% endif %}*/
/* 							{% endfor %}*/
/* 							</strong> */
/* 							<small class="price">&#8377; 45,000<strike class="old-price">25,000</strike></small>*/
/* 							</div>*/
/* 			            </a>*/
/* 			             <div class="battery-power">*/
/* 			                <strong>4000 mAH</strong>*/
/* 			            </div>*/
/* 			            <div class="external-storage">*/
/* 			                <strong>128GB</strong>*/
/* 			            </div>*/
/* 			             <div class="external-storage">*/
/* 			                <strong>128GB</strong>*/
/* 			            </div>*/
/* 			            </li>*/
/* 			            */
/* 			             */
/* 			            */
/* 			        </ul>*/
/* 			        </div>*/
/* 			       */
/* 			       */
/* 									<div class="gallery-button details-button"><a href="" class="btn btn-gallary btn-detail">See Image Gallery</a></div>*/
/* 			    </div>*/
/* 		    </div>*/
/* 		    {#==== End-Comparision-product==== #}*/
/* 		    {#====product-video==== #}*/
/* 		    <div class="col-md-12">*/
/* 		        <div class="product-combo product-video">*/
/* 		       <div class="combo-title">*/
/* 			            <h2><img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/video-product.png">Video</h2>*/
/* 			             <a href=""><i class="fa fa-youtube"></i>Watch YouTube Reviews</a>*/
/* 			        </div>*/
/* 			        */
/* 			        <ul class="video-carousel owl-carousel owl-theme">*/
/* 			            <li>*/
/* 			                <div class="video-frame">*/
/* 			                  <iframe width="200" height="115" src="https://www.youtube.com/embed/tgbNymZ7vqY"></iframe>*/
/*                             </div>*/
/*                             <div class="video-content">*/
/*                                 <span>4.6<img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/video-green-arrow.png"></span>*/
/*                                 <strong>performance<small>Ultra fast</small></strong>*/
/*                             </div>*/
/*                        </li>*/
/*                         <li>*/
/* 			                <div class="video-frame">*/
/* 			                  <iframe width="200" height="115" src="https://www.youtube.com/embed/tgbNymZ7vqY"></iframe>*/
/*                             </div>*/
/*                              <div class="video-content">*/
/*                                 <span>4.6<img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/video-green-arrow.png"></span>*/
/*                                 <strong>performance<small>Ultra fast</small></strong>*/
/*                             </div>*/
/*                        </li>*/
/*                         <li>*/
/* 			                <div class="video-frame">*/
/* 			                   <iframe width="200" height="115" src="https://www.youtube.com/embed/tgbNymZ7vqY"></iframe>*/
/*                             </div>*/
/*                              <div class="video-content">*/
/*                                 <span>4.6<img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/video-green-arrow.png"></span>*/
/*                                 <strong>performance<small>Ultra fast</small></strong>*/
/*                             </div>*/
/*                        </li>*/
/*                         <li>*/
/* 			                <div class="video-frame">*/
/* 			                   <iframe width="200" height="115" src="https://www.youtube.com/embed/tgbNymZ7vqY"></iframe>*/
/*                             </div>*/
/*                              <div class="video-content">*/
/*                                 <span>4.6<img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/video-green-arrow.png"></span>*/
/*                                 <strong>performance<small>Ultra fast</small></strong>*/
/*                             </div>*/
/*                        </li>*/
/*                         <li>*/
/* 			                <div class="video-frame">*/
/* 			                  <iframe width="200" height="115" src="https://www.youtube.com/embed/tgbNymZ7vqY"></iframe>*/
/*                             </div>*/
/*                              <div class="video-content">*/
/*                                 <span>4.6<img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/video-green-arrow.png"></span>*/
/*                                 <strong>performance<small>Ultra fast</small></strong>*/
/*                             </div>*/
/*                        </li>*/
/* 			        </ul>*/
/* 			        */
/* 			        </div>*/
/* 		    </div>*/
/* 		     {#====End-product-video==== #}*/
/* 		      {#====product-review==== #}*/
/* 		     <div class="col-md-12">*/
/* 		         <div class="product-question review-product">*/
/* 		              <div class="combo-title">*/
/* 			            <h2><img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/video-product.png">Customer Review</h2>*/
/* 			             <a href=""><i class="fa fa-edit"></i>Write Product Reviews</a>*/
/* 			          </div>*/
/* 		         </div>*/
/* 		     </div>*/
/* 		     <div class="col-md-5  pr-0">*/
/* 		         <div class="reviwe-block">*/
/* 		         <div class="review-card">*/
/* 		             <div class="card-cont">*/
/* 		             <span>Rating<strong>4.6<small>out of 5</small></strong></span>*/
/* 		             <small>35 Ratings 4 Reviews</small>*/
/* 		             </div>*/
/* 		             <div class="review-rating">*/
/* 		                 <ul>*/
/* 		                     <li> {% for i in 1..5 %}*/
/* 								{% if rating < i %}<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>{% else %}<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>{% endif %}*/
/* 							{% endfor %}<small>75%</small></li>*/
/* 							<li> {% for i in 1..4 %}*/
/* 								{% if rating < i %}<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>{% else %}<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>{% endif %}*/
/* 							{% endfor %}<small>75%</small></li>*/
/* 								<li> {% for i in 1..3 %}*/
/* 								{% if rating < i %}<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>{% else %}<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>{% endif %}*/
/* 							{% endfor %}<small>75%</small></li>*/
/* 								<li> {% for i in 1..2 %}*/
/* 								{% if rating < i %}<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>{% else %}<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>{% endif %}*/
/* 							{% endfor %}<small>75%</small></li>*/
/* 								<li> {% for i in 1..1 %}*/
/* 								{% if rating < i %}<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>{% else %}<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>{% endif %}*/
/* 							{% endfor %}<small>75%</small></li>*/
/* 		                 </ul>*/
/* 		             </div>*/
/* 		             */
/* 		         </div>*/
/* 		         <div class="review-edit-box">*/
/* 		             <strong>Write review for this product<span>share your feedback with other customer</span></strong>*/
/* 		             <div class="edit-button"><a href=""><i class="fa fa-edit"></i>Write a Product review</a></div>*/
/* 		         </div>*/
/* 		         </div>*/
/* 		     </div>*/
/* 		     <div class="col-md-7 pl-0">*/
/* 		        <div class="review-right-block">*/
/* 		         <div class="review-mention">*/
/* 		             <h3>Review Mention</h3>*/
/* 		             <span>battery life</span><span>value of money</span><span>Price range</span><span>best budget</span>*/
/* 		         </div>*/
/* 		         <div class="review-text">*/
/* 		             <h3>Review</h3>*/
/* 		             <ul>*/
/* 		                 <li>*/
/* 		                     <div class="customer-block">*/
/* 		                     <div class="customer-details">*/
/* 		                         <div class="customer-profile">*/
/* 		                         <img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/customer-review-img1.jpg">*/
/* 		                         </div>*/
/* 		                         <strong>Nikil <br>*/
/* 		                          {% for i in 1..5 %}*/
/* 								{% if rating < i %}<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>{% else %}<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>{% endif %}*/
/* 							{% endfor %}*/
/* 							<small>reviewed on 15 sep 2020</small>*/
/* 		                         </strong>*/
/* 		                     </div>*/
/* 		                     <div class="like-details">*/
/* 		                         <small><i class="fa fa-thumbs-up"></i></small>*/
/* 		                         <small><i class="fa fa-thumbs-down"></i></small>*/
/* 		                     </div>*/
/* 		                     </div>*/
/* 		                     <div class="review-para">*/
/* 		                         <span>More than 5 star, best budget mobile</span>*/
/* 		                         <p>Integer faucibus facilisis volutpat. Aliquam in suscipit orci. Nullam aliquam fermentum auctor. </p>*/
/* 		                     </div>*/
/* 		                 </li>*/
/* 		                  <li>*/
/* 		                     <div class="customer-block">*/
/* 		                     <div class="customer-details">*/
/* 		                         <div class="customer-profile">*/
/* 		                         <img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/customer-review-img1.jpg">*/
/* 		                         </div>*/
/* 		                         <strong>Nikil <br>*/
/* 		                          {% for i in 1..5 %}*/
/* 								{% if rating < i %}<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>{% else %}<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>{% endif %}*/
/* 							{% endfor %}*/
/* 							<small>reviewed on 15 sep 2020</small>*/
/* 		                         </strong>*/
/* 		                     </div>*/
/* 		                     <div class="like-details">*/
/* 		                         <small><i class="fa fa-thumbs-up"></i></small>*/
/* 		                         <small><i class="fa fa-thumbs-down"></i></small>*/
/* 		                     </div>*/
/* 		                     </div>*/
/* 		                     <div class="review-para">*/
/* 		                         <span>More than 5 star, best budget mobile</span>*/
/* 		                         <p>Integer faucibus facilisis volutpat. Aliquam in suscipit orci. Nullam aliquam fermentum auctor..</p>*/
/* 		                     </div>*/
/* 		                 </li>*/
/* 		                  <li>*/
/* 		                     <div class="customer-block">*/
/* 		                     <div class="customer-details">*/
/* 		                         <div class="customer-profile">*/
/* 		                         <img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/customer-review-img1.jpg">*/
/* 		                         </div>*/
/* 		                         <strong>Nikil <br>*/
/* 		                          {% for i in 1..5 %}*/
/* 								{% if rating < i %}<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>{% else %}<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>{% endif %}*/
/* 							{% endfor %}*/
/* 							<small>reviewed on 15 sep 2020</small>*/
/* 		                         </strong>*/
/* 		                     </div>*/
/* 		                     <div class="like-details">*/
/* 		                         <small><i class="fa fa-thumbs-up"></i></small>*/
/* 		                         <small><i class="fa fa-thumbs-down"></i></small>*/
/* 		                     </div>*/
/* 		                     </div>*/
/* 		                     <div class="review-para">*/
/* 		                         <span>More than 5 star, best budget mobile</span>*/
/* 		                         <p>Integer faucibus facilisis volutpat. Aliquam in suscipit orci. Nullam aliquam fermentum auctor..</p>*/
/* 		                     </div>*/
/* 		                 </li>*/
/* 		                 */
/* 		             </ul>*/
/* 		         </div>*/
/* 		         </div>*/
/* 		     </div>*/
/* 		       {#====End-product-review==== #}*/
/* 		     {#====Question-product==== #}*/
/* 		     <div class="col-md-8 pr-0">*/
/* 		         <div class="product-question">*/
/* 		               <div class="combo-title question-pro">*/
/* 			            <h2><img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/related-product.png">Question and Answer</h2>*/
/* 			            <span><input type="text" placeholder="Search of Question and Answer.."></span>*/
/* 			        </div>*/
/* 			        <div id="collapse-description" class="desc-collapse showdown">*/
/* 			        <ul>*/
/* 			            <li><div class="question-content">*/
/* 			                <p class="question-cont">Q : It is Quality Product?</p>*/
/* 			                <p class="answer-cont">A : Yes it Quality Product.</p>*/
/* 			                </div>*/
/* 			                <div class="like-details">*/
/* 			                    <span>by paratap</span>*/
/* 			                    <small><i class="fa fa-thumbs-up"></i>930</small>*/
/* 			                    <small><i class="fa fa-thumbs-down"></i>30</small>*/
/* 			                </div>*/
/* 			            </li>*/
/* 			             <li><div class="question-content">*/
/* 			                <p class="question-cont">Q : It is Quality Product?</p>*/
/* 			                <p class="answer-cont">A : Yes it Quality Product.</p>*/
/* 			                </div>*/
/* 			                <div class="like-details">*/
/* 			                    <span>by paratap</span>*/
/* 			                    <small><i class="fa fa-thumbs-up"></i>930</small>*/
/* 			                    <small><i class="fa fa-thumbs-down"></i>30</small>*/
/* 			                </div>*/
/* 			            </li>*/
/* 			             <li><div class="question-content">*/
/* 			                <p class="question-cont">Q : It is Quality Product?</p>*/
/* 			                <p class="answer-cont">A : Yes it Quality Product.</p>*/
/* 			                </div>*/
/* 			                <div class="like-details">*/
/* 			                    <span>by paratap</span>*/
/* 			                    <small><i class="fa fa-thumbs-up"></i>930</small>*/
/* 			                    <small><i class="fa fa-thumbs-down"></i>30</small>*/
/* 			                </div>*/
/* 			            </li>*/
/* 			             <li><div class="question-content">*/
/* 			                <p class="question-cont">Q : It is Quality Product?</p>*/
/* 			                <p class="answer-cont">A : Yes it Quality Product.</p>*/
/* 			                </div>*/
/* 			                <div class="like-details">*/
/* 			                    <span>by paratap</span>*/
/* 			                    <small><i class="fa fa-thumbs-up"></i>930</small>*/
/* 			                    <small><i class="fa fa-thumbs-down"></i>30</small>*/
/* 			                </div>*/
/* 			            </li>*/
/* 			        </ul>*/
/* 			        </div>*/
/* 			        <div class="button-toggle toggle1">*/
/* 								         <a class="showmore" data-toggle="collapse" href="#" aria-expanded="false" aria-controls="collapse-footer">*/
/* 								            <span class="toggle-more">Show all answer question <i class="fa fa-angle-down"></i></span> */
/* 								            <span class="toggle-less">Show Less <i class="fa fa-angle-up"></i></span>           */
/* 										</a>      */
/* 										<a href="">*/
/* 										    <strong><i class="fa fa-edit"></i>Ask Question</strong>*/
/* 										</a>*/
/* 									</div>*/
/* 		         </div>*/
/* 		     </div>*/
/* 		     <div class="col-md-4 pl-0">*/
/* 		         <div class="product-customer-image">*/
/* 		              <div class="combo-title customer-img">*/
/* 			            <h2>Customer Image</h2>*/
/* 			        </div>*/
/* 			        <ul>*/
/* 			            <li><a href=""><img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/customer-img1.jpg"></a></li>*/
/* 			            <li><a href=""><img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/customer-img2.jpg"></a></li>*/
/* 			            <li><a href=""><img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/customer-img2.jpg"></a></li>*/
/* 			            <li><a href=""><img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/customer-img1.jpg"></a></li>*/
/* 			        </ul>*/
/* 			        <div class="gallery-button"><a href="" class="btn btn-gallary">See Image Gallery</a></div>*/
/* 		         </div>*/
/* 		     </div>*/
/* 		     {#====End-Question-product==== #}*/
/* 		    {#====related-product==== #}*/
/* 		    <div class="col-md-12">*/
/* 		        <div class="product-combo related-product">*/
/* 			        <div class="combo-title">*/
/* 			            <h2><img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/related-product.png">Related Product</h2>*/
/* 			        </div>*/
/* 			        <ul class="related-carousel owl-carousel owl-theme">*/
/* 			            <li><a href="">*/
/* 			                <div class="combo-list"><img src="http://poorvikabeta.webindia.com/image/cache/catalog/demo/canon_eos_5d_1-355x470.jpg"></div>*/
/* 			                <div class="combo-content">*/
/* 			                    <h5>Canon EOS 5D</h5>*/
/* 			                    <strong>4.5*/
/* 			                    {% for i in 1..5 %}*/
/* 								{% if rating < i %}<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>{% else %}<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>{% endif %}*/
/* 							{% endfor %}*/
/* 							</strong> */
/* 							<small class="price">&#8377; 45,000<strike class="old-price">25,000</strike></small>*/
/* 							</div>*/
/* 			            </a>*/
/* 			            </li>*/
/* 			             <li><a href="">*/
/* 			                <div class="combo-list"><img src="http://poorvikabeta.webindia.com/image/cache/catalog/demo/canon_eos_5d_1-355x470.jpg"></div>*/
/* 			                <div class="combo-content">*/
/* 			                    <h5>Canon EOS 5D</h5>*/
/* 			                    <strong>4.5*/
/* 			                    {% for i in 1..5 %}*/
/* 								{% if rating < i %}<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>{% else %}<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>{% endif %}*/
/* 							{% endfor %}*/
/* 							</strong> */
/* 							<small class="price">&#8377; 45,000<strike class="old-price">25,000</strike></small>*/
/* 							</div>*/
/* 			            </a>*/
/* 			            </li>*/
/* 			             <li><a href="">*/
/* 			                <div class="combo-list"><img src="http://poorvikabeta.webindia.com/image/cache/catalog/demo/canon_eos_5d_1-355x470.jpg"></div>*/
/* 			                <div class="combo-content">*/
/* 			                    <h5>Canon EOS 5D</h5>*/
/* 			                    <strong>4.5*/
/* 			                    {% for i in 1..5 %}*/
/* 								{% if rating < i %}<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>{% else %}<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>{% endif %}*/
/* 							{% endfor %}*/
/* 							</strong> */
/* 							<small class="price">&#8377; 45,000<strike class="old-price">25,000</strike></small>*/
/* 							</div>*/
/* 			            </a>*/
/* 			            </li>*/
/* 			             <li><a href="">*/
/* 			                <div class="combo-list"><img src="http://poorvikabeta.webindia.com/image/cache/catalog/demo/canon_eos_5d_1-355x470.jpg"></div>*/
/* 			                <div class="combo-content">*/
/* 			                    <h5>Canon EOS 5D</h5>*/
/* 			                    <strong>4.5*/
/* 			                    {% for i in 1..5 %}*/
/* 								{% if rating < i %}<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>{% else %}<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>{% endif %}*/
/* 							{% endfor %}*/
/* 							</strong> */
/* 							<small class="price">&#8377; 45,000<strike class="old-price">25,000</strike></small>*/
/* 							</div>*/
/* 			            </a>*/
/* 			            </li>*/
/* 			             <li><a href="">*/
/* 			                <div class="combo-list"><img src="http://poorvikabeta.webindia.com/image/cache/catalog/demo/canon_eos_5d_1-355x470.jpg"></div>*/
/* 			                <div class="combo-content">*/
/* 			                    <h5>Canon EOS 5D</h5>*/
/* 			                    <strong>4.5*/
/* 			                    {% for i in 1..5 %}*/
/* 								{% if rating < i %}<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>{% else %}<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>{% endif %}*/
/* 							{% endfor %}*/
/* 							</strong> */
/* 							<small class="price">&#8377; 45,000<strike class="old-price">25,000</strike></small>*/
/* 							</div>*/
/* 			            </a>*/
/* 			            </li>*/
/* 			            */
/* 			             */
/* 			            */
/* 			        </ul>*/
/* 			    </div>*/
/* 		    </div>*/
/* 		</div>*/
/*     	{#====  End-related-product==== #}*/
/*     </div>*/
/*     */
/*     {#====  Column Right outside==== #}*/
/*     {% if col_position== 'outside' %} {{ column_right }} {% endif %}*/
/*     </div>*/
/* </div>*/
/* */
/* <script src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/js/owl.carousel.min.js"></script> */
/* <script type="text/javascript">*/
/* <!--*/
/* $('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){*/
/* 	$.ajax({*/
/* 		url: 'index.php?route=product/product/getRecurringDescription',*/
/* 		type: 'post',*/
/* 		data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),*/
/* 		dataType: 'json',*/
/* 		beforeSend: function() {*/
/* 			$('#recurring-description').html('');*/
/* 		},*/
/* 		success: function(json) {*/
/* 			$('.alert-dismissible, .text-danger').remove();*/
/* */
/* 			if (json['success']) {*/
/* 				$('#recurring-description').html(json['success']);*/
/* 			}*/
/* 		}*/
/* 	});*/
/* });*/
/* //--></script>*/
/* */
/* <script type="text/javascript"><!--*/
/* $('#button-cart').on('click', function() {*/
/* 	*/
/* 	$.ajax({*/
/* 		url: 'index.php?route=extension/soconfig/cart/add',*/
/* 		type: 'post',*/
/* 		data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),*/
/* 		dataType: 'json',*/
/* 		beforeSend: function() {*/
/* 			$('#button-cart').button('loading');*/
/* 		},*/
/* 		complete: function() {*/
/* 			$('#button-cart').button('reset');*/
/* 		},*/
/* 		success: function(json) {*/
/* 			$('.alert').remove();*/
/* 			$('.text-danger').remove();*/
/* 			$('.form-group').removeClass('has-error');*/
/* 			if (json['error']) {*/
/* 				if (json['error']['option']) {*/
/* 					for (i in json['error']['option']) {*/
/* 						var element = $('#input-option' + i.replace('_', '-'));*/
/*  */
/*  {% if option_data %} */
/*  if(ProductOptionId != undefined && ProductOptionId==i.replace('_', '-')){ */
/*  $('.so-colorswatch-productpage-icons').after('<div class="text-danger">' + json['error']['option'][i] + '</div>'); */
/*  } */
/*  {% endif %} */
/*  */
/* 						*/
/* 						if (element.parent().hasClass('input-group')) {*/
/* 							element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');*/
/* 						} else {*/
/* 							element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');*/
/* 						}*/
/* 					}*/
/* 				}*/
/* 				*/
/* 				if (json['error']['recurring']) {*/
/* 					$('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');*/
/* 				}*/
/* 				*/
/* 				// Highlight any found errors*/
/* 				$('.text-danger').parent().addClass('has-error');*/
/* 			}*/
/* 			*/
/* 			if (json['success']) {*/
/* 				$('.text-danger').remove();*/
/* 				$('#wrapper').before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="fa fa-close close" data-dismiss="alert"></button></div>');*/
/* 				$('#cart  .total-shopping-cart ').html(json['total'] );*/
/* 				$('#cart > ul').load('index.php?route=common/cart/info ul li');*/
/* 				*/
/* 				timer = setTimeout(function () {*/
/* 					$('.alert').addClass('fadeOut');*/
/* 				}, 4000);*/
/* 				$('.so-groups-sticky .popup-mycart .popup-content').load('index.php?route=extension/module/so_tools/info .popup-content .cart-header');*/
/* 			}*/
/* 			*/
/* 		*/
/* 		},*/
/*         error: function(xhr, ajaxOptions, thrownError) {*/
/*             alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*         }*/
/* 	});*/
/* });*/
/* */
/* //--></script> */
/* */
/* <script type="text/javascript"><!--*/
/* $('.date').datetimepicker({*/
/* 	language: document.cookie.match(new RegExp('language=([^;]+)'))[1],*/
/* 	pickTime: false*/
/* });*/
/* */
/* $('.datetime').datetimepicker({*/
/* 	language: document.cookie.match(new RegExp('language=([^;]+)'))[1],*/
/* 	pickDate: true,*/
/* 	pickTime: true*/
/* });*/
/* */
/* $('.time').datetimepicker({*/
/* 	language: document.cookie.match(new RegExp('language=([^;]+)'))[1],*/
/* 	pickDate: false*/
/* });*/
/* */
/* $('button[id^=\'button-upload\']').on('click', function() {*/
/* 	var node = this;*/
/* */
/* 	$('#form-upload').remove();*/
/* */
/* 	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');*/
/* */
/* 	$('#form-upload input[name=\'file\']').trigger('click');*/
/* */
/* 	if (typeof timer != 'undefined') {*/
/* 		clearInterval(timer);*/
/* 	}*/
/* */
/* 	timer = setInterval(function() {*/
/* 		if ($('#form-upload input[name=\'file\']').val() != '') {*/
/* 			clearInterval(timer);*/
/* */
/* 			$.ajax({*/
/* 				url: 'index.php?route=tool/upload',*/
/* 				type: 'post',*/
/* 				dataType: 'json',*/
/* 				data: new FormData($('#form-upload')[0]),*/
/* 				cache: false,*/
/* 				contentType: false,*/
/* 				processData: false,*/
/* 				beforeSend: function() {*/
/* 					$(node).button('loading');*/
/* 				},*/
/* 				complete: function() {*/
/* 					$(node).button('reset');*/
/* 				},*/
/* 				success: function(json) {*/
/* 					$('.text-danger').remove();*/
/* */
/* 					if (json['error']) {*/
/* 						$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');*/
/* 					}*/
/* */
/* 					if (json['success']) {*/
/* 						alert(json['success']);*/
/* */
/* 						$(node).parent().find('input').val(json['code']);*/
/* 					}*/
/* 				},*/
/* 				error: function(xhr, ajaxOptions, thrownError) {*/
/* 					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/* 				}*/
/* 			});*/
/* 		}*/
/* 	}, 500);*/
/* });*/
/* //--></script> */
/* <script type="text/javascript"><!--*/
/* $('#review').delegate('.pagination a', 'click', function(e) {*/
/*     e.preventDefault();*/
/* */
/*     $('#review').fadeOut('slow');*/
/*     $('#review').load(this.href);*/
/*     $('#review').fadeIn('slow');*/
/* });*/
/* */
/* $('#review').load('index.php?route=product/product/review&product_id={{ product_id }}');*/
/* */
/* $('#button-review').on('click', function() {*/
/* 	$.ajax({*/
/* 		url: 'index.php?route=product/product/write&product_id={{ product_id }}',*/
/* 		type: 'post',*/
/* 		dataType: 'json',*/
/* 		data: $("#form-review").serialize(),*/
/* 		beforeSend: function() {*/
/* 			$('#button-review').button('loading');*/
/* 		},*/
/* 		complete: function() {*/
/* 			$('#button-review').button('reset');*/
/* 		},*/
/* 		success: function(json) {*/
/* 			$('.alert-dismissible').remove();*/
/* */
/* 			if (json['error']) {*/
/* 				$('#review').after('<div class="alert alert-danger alert-dismissible"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');*/
/* 			}*/
/* */
/* 			if (json['success']) {*/
/* 				$('#review').after('<div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');*/
/* */
/* 				$('input[name=\'name\']').val('');*/
/* 				$('textarea[name=\'text\']').val('');*/
/* 				$('input[name=\'rating\']:checked').prop('checked', false);*/
/* 			}*/
/* 		}*/
/* 	});*/
/* });*/
/* */
/* //--></script>*/
/* */
/* */
/* */
/* <script type="text/javascript"><!--*/
/* 	$(document).ready(function() {*/
/* 		*/
/* 		// Initialize the sticky scrolling on an item */
/* 		sidebar_sticky = '{{sidebar_sticky}}';*/
/* 		*/
/* 		if(sidebar_sticky=='left'){*/
/* 			$(".left_column").stick_in_parent({*/
/* 			    offset_top: 10,*/
/* 			    bottoming   : true*/
/* 			});*/
/* 		}else if (sidebar_sticky=='right'){*/
/* 			$(".right_column").stick_in_parent({*/
/* 			    offset_top: 10,*/
/* 			    bottoming   : true*/
/* 			});*/
/* 		}else if (sidebar_sticky=='all'){*/
/* 			$(".content-aside").stick_in_parent({*/
/* 			    offset_top: 10,*/
/* 			    bottoming   : true*/
/* 			});*/
/* 		}*/
/* 		*/
/* */
/* 		$("#thumb-slider .image-additional").each(function() {*/
/* 			$(this).find("[data-index='0']").addClass('active');*/
/* 		});*/
/* 		*/
/* 		$('.product-options li.radio').click(function(){*/
/* 			$(this).addClass(function() {*/
/* 				if($(this).hasClass("active")) return "";*/
/* 				return "active";*/
/* 			});*/
/* 			*/
/* 			$(this).siblings("li").removeClass("active");*/
/* 			$(this).parent().find('.selected-option').html('<span class="label label-success">'+ $(this).find('img').data('original-title') +'</span>');*/
/* 		})*/
/* 		*/
/* 		$('.thumb-video').magnificPopup({*/
/* 		  type: 'iframe',*/
/* 		  iframe: {*/
/* 			patterns: {*/
/* 			   youtube: {*/
/* 				  index: 'youtube.com/', // String that detects type of video (in this case YouTube). Simply via url.indexOf(index).*/
/* 				  id: 'v=', // String that splits URL in a two parts, second part should be %id%*/
/* 				  src: '//www.youtube.com/embed/%id%?autoplay=1' // URL that will be set as a source for iframe. */
/* 					},*/
/* 				}*/
/* 			}*/
/* 		});*/
/* 	});*/
/* //--></script>*/
/* */
/* */
/* <script type="text/javascript">*/
/* var ajax_price = function() {*/
/* 	$.ajax({*/
/* 		type: 'POST',*/
/* 		url: 'index.php?route=extension/soconfig/liveprice/index',*/
/* 		data: $('.product-detail input[type=\'text\'], .product-detail input[type=\'hidden\'], .product-detail input[type=\'radio\']:checked, .product-detail input[type=\'checkbox\']:checked, .product-detail select, .product-detail textarea'),*/
/* 		dataType: 'json',*/
/* 			success: function(json) {*/
/* 			if (json.success) {*/
/* 				change_price('#price-special', json.new_price.special);*/
/* 				change_price('#price-tax', json.new_price.tax);*/
/* 				change_price('#price-old', json.new_price.price);*/
/* 			}*/
/* 		}*/
/* 	});*/
/* }*/
/* */
/* var change_price = function(id, new_price) {$(id).html(new_price);}*/
/* $('.product-detail input[type=\'text\'], .product-detail input[type=\'hidden\'], .product-detail input[type=\'radio\'], .product-detail input[type=\'checkbox\'], .product-detail select, .product-detail textarea, .product-detail input[name=\'quantity\']').on('change', function() {*/
/* 	ajax_price();*/
/* });*/
/* </script>*/
/* <script>*/
/* function openColor(color) {*/
/*   var i;*/
/*   var x = document.getElementsByClassName("product-color-change");*/
/*   for (i = 0; i < x.length; i++) {*/
/*     x[i].style.display = "none";  */
/*   }*/
/*   document.getElementById(color).style.display = "block";  */
/* }*/
/* </script>*/
/* */
/* */
/* <script>*/
/* function openCity(evt, cityName) {*/
/*   var i, tabcontent, tablinks;*/
/*   tabcontent = document.getElementsByClassName("tabcontent");*/
/*   for (i = 0; i < tabcontent.length; i++) {*/
/*     tabcontent[i].style.display = "none";*/
/*   }*/
/*   tablinks = document.getElementsByClassName("tablinks");*/
/*   for (i = 0; i < tablinks.length; i++) {*/
/*     tablinks[i].className = tablinks[i].className.replace(" active", "");*/
/*   }*/
/*   document.getElementById(cityName).style.display = "inline-block";*/
/*   evt.currentTarget.className += " active";*/
/* }*/
/* </script>*/
/* <script>*/
/*  $(document).ready(function(){*/
/*   $('.related-carousel').owlCarousel({*/
/*     loop:true,*/
/*    autoplay:true,*/
/*     autoplayTimeout:3000,*/
/*     autoplayHoverPause:true,*/
/*     nav:true,*/
/* 	dots:false,*/
/*     responsive:{*/
/*         0:{*/
/*             items:1*/
/*         },*/
/* 		320:{*/
/*             items:1*/
/*         },*/
/* 		480:{*/
/*             items:1*/
/*         },*/
/*         600:{*/
/*             items:2*/
/*         },*/
/* 		767:{*/
/*             items:3*/
/*         },*/
/* 		991:{*/
/*             items:4*/
/*         },*/
/*         1200:{*/
/*             items:5*/
/*         }*/
/*     }*/
/* })*/
/* $( ".owl-prev").html('<i class="fa fa-lg fa-angle-left"></i>');*/
/*  $( ".owl-next").html('<i class="fa fa-lg fa-angle-right"></i>');*/
/* });	*/
/* */
/* </script>*/
/* <script>*/
/*  $(document).ready(function(){*/
/*   $('.video-carousel').owlCarousel({*/
/*     loop:true,*/
/*    autoplay:true,*/
/*     autoplayTimeout:3000,*/
/*     autoplayHoverPause:true,*/
/*     nav:true,*/
/* 	dots:false,*/
/*     responsive:{*/
/*         0:{*/
/*             items:1*/
/*         },*/
/* 		320:{*/
/*             items:1*/
/*         },*/
/* 		480:{*/
/*             items:1*/
/*         },*/
/*         600:{*/
/*             items:2*/
/*         },*/
/* 		767:{*/
/*             items:2*/
/*         },*/
/* 		991:{*/
/*             items:3*/
/*         },*/
/*         1200:{*/
/*             items:4*/
/*         }*/
/*     }*/
/* })*/
/* $( ".owl-prev").html('<i class="fa fa-lg fa-arrow-left"></i>');*/
/*  $( ".owl-next").html('<i class="fa fa-lg fa-arrow-right"></i>');*/
/* });	*/
/* */
/* </script>*/
/* */
/* {{ footer }} */
/* */
