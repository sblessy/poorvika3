<?php

/* catalog/wk_preorder_productadd.twig */
class __TwigTemplate_730b5d2c29d3fab4b1e30162494eeecc6afccf8d925460de925d7f4b3d006caa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo " 
<div id=\"content\">
    <div class=\"page-header\">
        <div class=\"container-fluid\">
            <div class=\"pull-right\">
                <button  onclick=\"\$('#form').submit();\" class=\"btn btn-primary\" title=\"";
        // line 6
        echo (isset($context["button_save"]) ? $context["button_save"] : null);
        echo "\">
                    <i class=\"fa fa-save\"></i>
                </button>
                <a href=\"";
        // line 9
        echo (isset($context["cancel"]) ? $context["cancel"] : null);
        echo "\" class=\"btn btn-default\" title=\"";
        echo (isset($context["button_cancel"]) ? $context["button_cancel"] : null);
        echo "\">
                    <i class=\"fa fa-reply\"></i>
                </a>
            </div>
            <h1>";
        // line 13
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
            <ul class=\"breadcrumb\">
                ";
        // line 15
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            echo " 
                    <li><a href=\"";
            // line 16
            echo $this->getAttribute($context["breadcrumb"], "href", array(), "array");
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array(), "array");
            echo "</a></li>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 17
        echo " 
            </ul>
        </div>
    </div>
    <div class=\"container-fluid\">
        ";
        // line 22
        if ((isset($context["error_warning"]) ? $context["error_warning"] : null)) {
            echo " 
            <div class=\"alert alert-danger\">
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
                ";
            // line 25
            echo (isset($context["error_warning"]) ? $context["error_warning"] : null);
            echo " 
            </div>
        ";
        }
        // line 27
        echo " 
        ";
        // line 28
        if ((array_key_exists("form_error_warning", $context) && (isset($context["form_error_warning"]) ? $context["form_error_warning"] : null))) {
            echo " 
            <div class=\"alert alert-danger\">
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
                ";
            // line 31
            echo (isset($context["form_error_warning"]) ? $context["form_error_warning"] : null);
            echo " 
            </div>
        ";
        }
        // line 33
        echo " 
        <div class=\"panel panel-default\">
            <div class=\"panel-heading\">
                <h3 class=\"panel-title\">
                    ";
        // line 37
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo " 
                </h3>
            </div>
            <div class=\"panel-body\">
                <form action=\"";
        // line 41
        echo (isset($context["add"]) ? $context["add"] : null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form\" class=\"form-horizontal\">
                    <div class=\"form-group required\">
                        <label class=\"col-sm-2 control-label\">
                            <span data-toggle=\"tooltip\" data-original-title=\"";
        // line 44
        echo (isset($context["text_add_preorder_product_info"]) ? $context["text_add_preorder_product_info"] : null);
        echo "\">
                                ";
        // line 45
        echo (isset($context["text_add_preorder_product"]) ? $context["text_add_preorder_product"] : null);
        echo " 
                            </span>
                        </label>
                        <div class=\"col-sm-10\">
                            ";
        // line 49
        if ((array_key_exists("id", $context) && (isset($context["id"]) ? $context["id"] : null))) {
            echo " 
                                <input type=\"hidden\" name=\"id\" value=\"";
            // line 50
            echo (isset($context["id"]) ? $context["id"] : null);
            echo "\" />
                            ";
        } else {
            // line 51
            echo " 
                                <input type=\"text\" class=\"form-control\" name=\"wk_preorder_products\" id=\"wk_preorder_products\" />
                            ";
        }
        // line 53
        echo " 
                            <div id=\"pre-order-product\" class=\"well well-sm\" style=\"height: 150px; overflow: auto;\">
                                ";
        // line 55
        if ((array_key_exists("pre_order_product", $context) && (isset($context["pre_order_product"]) ? $context["pre_order_product"] : null))) {
            echo " 
                                    ";
            // line 56
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["pre_order_product"]) ? $context["pre_order_product"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["value"]) {
                echo " 
                                        <div id=\"";
                // line 57
                echo $this->getAttribute($context["value"], "product_id", array(), "array");
                echo "\"><i class=\"fa fa-minus-circle\" onclick=\"\$(this).parent().remove()\" ></i> ";
                echo $this->getAttribute($context["value"], "name", array(), "array");
                echo "<input type=\"hidden\" name=\"pre_order_product[";
                echo $this->getAttribute($context["value"], "product_id", array(), "array");
                echo "][product_id]\" value=\"";
                echo $this->getAttribute($context["value"], "product_id", array(), "array");
                echo "\" /><input type=\"hidden\" name=\"pre_order_product[";
                echo $this->getAttribute($context["value"], "product_id", array(), "array");
                echo "][name]\" value=\"";
                echo $this->getAttribute($context["value"], "name", array(), "array");
                echo "\" /></div>
                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['value'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 58
            echo " 
                                ";
        }
        // line 59
        echo " 
                            </div>
                            ";
        // line 61
        if ((array_key_exists("form_error_no_product", $context) && (isset($context["form_error_no_product"]) ? $context["form_error_no_product"] : null))) {
            echo " 
                                <div class=\"text-danger\">
                                    ";
            // line 63
            echo (isset($context["form_error_no_product"]) ? $context["form_error_no_product"] : null);
            echo " 
                                </div>
                            ";
        }
        // line 65
        echo " 
                        </div>
                    </div>
                    <div class=\"form-group\">
                        <label class=\"col-sm-2 control-label\">
                            <span data-toggle=\"tooltip\" data-original-title=\"";
        // line 70
        echo (isset($context["text_deduction_method_info"]) ? $context["text_deduction_method_info"] : null);
        echo "\">
                                ";
        // line 71
        echo (isset($context["text_deduction_method"]) ? $context["text_deduction_method"] : null);
        echo " 
                            </span>
                        </label>
                        <div class=\"col-sm-10\">
                            <select class=\"form-control\" name=\"wk_deduction_method\" >
                                <option value=\"fp\" ";
        // line 76
        if ((array_key_exists("wk_deduction_method", $context) && ((isset($context["wk_deduction_method"]) ? $context["wk_deduction_method"] : null) == "fp"))) {
            echo " ";
            echo "selected";
            echo " ";
        }
        echo " >";
        echo (isset($context["text_fixed_rate"]) ? $context["text_fixed_rate"] : null);
        echo "</option>
                                <option value=\"pc\" ";
        // line 77
        if ((array_key_exists("wk_deduction_method", $context) && ((isset($context["wk_deduction_method"]) ? $context["wk_deduction_method"] : null) == "pc"))) {
            echo " ";
            echo "selected";
            echo " ";
        }
        echo ">";
        echo (isset($context["text_partial_rate"]) ? $context["text_partial_rate"] : null);
        echo "</option>
                            </select>
                            ";
        // line 79
        if ((array_key_exists("error_method", $context) && (isset($context["error_method"]) ? $context["error_method"] : null))) {
            // line 80
            echo "                            <div class = \"text-danger\">";
            echo (isset($context["error_method"]) ? $context["error_method"] : null);
            echo "</div>
                            ";
        }
        // line 81
        echo " 
                        </div>
                    </div>
                    <div class=\"form-group\" id=\"percentage_price_block\" ";
        // line 84
        if (( !array_key_exists("wk_deduction_method", $context) || ((isset($context["wk_deduction_method"]) ? $context["wk_deduction_method"] : null) != "pc"))) {
            echo " ";
            echo "style=\"display:none\"";
            echo " ";
        }
        echo " >
                        <label class=\"col-sm-2 control-label\">
                            <span data-toggle=\"tooltip\" data-original-title=\"";
        // line 86
        echo (isset($context["text_product_price_info"]) ? $context["text_product_price_info"] : null);
        echo "\">
                                ";
        // line 87
        echo (isset($context["text_product_price"]) ? $context["text_product_price"] : null);
        echo " 
                            </span>
                        </label>
                        <div class=\"col-sm-3\">
                            <div class=\"input-group\">
                                <span class=\"input-group-btn\">
                                    <button class=\"btn btn-danger\" type=\"button\" id=\"spinner_minus\">
                                        <i class=\"fa fa-minus\"></i>
                                    </button>
                                </span>
                                <input type=\"text\" class=\"form-control\" maxlength=\"5\" onkeyup=\"this.value=this.value.replace(/[^\\d.]/,'');\" name=\"wk_percentage_price\" id=\"percentage_spinner\" value=\"";
        // line 97
        if ((array_key_exists("wk_percentage_price", $context) && (isset($context["wk_percentage_price"]) ? $context["wk_percentage_price"] : null))) {
            echo " ";
            echo (isset($context["wk_percentage_price"]) ? $context["wk_percentage_price"] : null);
        } else {
            echo " ";
            echo "0";
        }
        echo "\" />
                                <span class=\"input-group-btn\">
                                    <button class=\"btn btn-success\" id=\"spinner_add\" type=\"button\">
                                        <i class=\"fa fa-plus\"></i>
                                    </button>
                                </span>
                            </div>
                            ";
        // line 104
        if ((array_key_exists("form_error_no_percentage", $context) && (isset($context["form_error_no_percentage"]) ? $context["form_error_no_percentage"] : null))) {
            echo " 
                                <div class=\"text-danger\">
                                    ";
            // line 106
            echo (isset($context["form_error_no_percentage"]) ? $context["form_error_no_percentage"] : null);
            echo " 
                                </div>
                            ";
        }
        // line 108
        echo " 
                        </div>
                    </div>
                    <div class=\"form-group required\">
                        <label class=\"col-sm-2 control-label\">
                            <span data-toggle=\"tooltip\" data-original-title=\"";
        // line 113
        echo (isset($context["text_preorder_quantity_info"]) ? $context["text_preorder_quantity_info"] : null);
        echo "\">
                                ";
        // line 114
        echo (isset($context["text_preorder_quantity"]) ? $context["text_preorder_quantity"] : null);
        echo " 
                            </span>
                        </label>
                        <div class=\"col-sm-10\">
                            <input type=\"text\" class=\"form-control\" name=\"wk_preorder_quantity\" onkeyup=\"this.value=this.value.replace(/[^\\d.]/,'');\" value=\"";
        // line 118
        if ((array_key_exists("wk_preorder_quantity", $context) && (isset($context["wk_preorder_quantity"]) ? $context["wk_preorder_quantity"] : null))) {
            echo " ";
            echo (isset($context["wk_preorder_quantity"]) ? $context["wk_preorder_quantity"] : null);
        }
        echo "\" />
                            ";
        // line 119
        if ((array_key_exists("quantity_error", $context) && (isset($context["quantity_error"]) ? $context["quantity_error"] : null))) {
            echo " 
                              <div class=\"text-danger\">
                                  ";
            // line 121
            echo (isset($context["quantity_error"]) ? $context["quantity_error"] : null);
            echo " 
                              </div>
                           ";
        }
        // line 123
        echo " 
                        </div>
                    </div>
                    <div class=\"form-group required\">
                        <label class=\"col-sm-2 control-label\">
                            <span data-toggle=\"tooltip\" data-original-title=\"";
        // line 128
        echo (isset($context["text_quantity_per_order_info"]) ? $context["text_quantity_per_order_info"] : null);
        echo "\">
                                ";
        // line 129
        echo (isset($context["text_quantity_per_order"]) ? $context["text_quantity_per_order"] : null);
        echo " 
                            </span>
                        </label>
                        <div class=\"col-sm-10\">
                            <input type=\"text\" class=\"form-control\" onkeyup=\"this.value=this.value.replace(/[^\\d.]/,'');\" name=\"wk_quantity_per_order\" value=\"";
        // line 133
        if ((array_key_exists("wk_quantity_per_order", $context) && (isset($context["wk_quantity_per_order"]) ? $context["wk_quantity_per_order"] : null))) {
            echo " ";
            echo (isset($context["wk_quantity_per_order"]) ? $context["wk_quantity_per_order"] : null);
        }
        echo "\" >
                            ";
        // line 134
        if ((array_key_exists("quantity_per_oerror", $context) && (isset($context["quantity_per_oerror"]) ? $context["quantity_per_oerror"] : null))) {
            echo " 
                                <div class=\"text-danger\">
                                    ";
            // line 136
            echo (isset($context["quantity_per_oerror"]) ? $context["quantity_per_oerror"] : null);
            echo " 
                                </div>
                            ";
        }
        // line 138
        echo " 
                        </div>
                    </div>
                    <div class=\"form-group required\">
                        <label class=\"col-sm-2 control-label\">
                            <span data-toggle=\"tooltip\" data-original-title=\"";
        // line 143
        echo (isset($context["text_order_per_customer_info"]) ? $context["text_order_per_customer_info"] : null);
        echo "\">
                                ";
        // line 144
        echo (isset($context["text_order_per_customer"]) ? $context["text_order_per_customer"] : null);
        echo " 
                            </span>
                        </label>
                        <div class=\"col-sm-10\">
                            <input type=\"text\" class=\"form-control\" name=\"wk_order_per_customer\" onkeyup=\"this.value=this.value.replace(/[^\\d.]/,'');\" value=\"";
        // line 148
        if ((array_key_exists("wk_order_per_customer", $context) && (isset($context["wk_order_per_customer"]) ? $context["wk_order_per_customer"] : null))) {
            echo " ";
            echo (isset($context["wk_order_per_customer"]) ? $context["wk_order_per_customer"] : null);
        }
        echo "\"  />
                            ";
        // line 149
        if ((array_key_exists("quantity_cust_error", $context) && (isset($context["quantity_cust_error"]) ? $context["quantity_cust_error"] : null))) {
            echo " 
                                <div class=\"text-danger\">
                                    ";
            // line 151
            echo (isset($context["quantity_cust_error"]) ? $context["quantity_cust_error"] : null);
            echo " 
                                </div>
                            ";
        }
        // line 153
        echo " 
                        </div>
                    </div>
                    ";
        // line 156
        if ((array_key_exists("module_wk_preorder_pro_discount", $context) && (isset($context["module_wk_preorder_pro_discount"]) ? $context["module_wk_preorder_pro_discount"] : null))) {
            echo " 
                    <div class=\"form-group\">
                        <label class=\"col-sm-2 control-label\">
                            <span data-toggle=\"tooltip\" data-original-title=\"";
            // line 159
            echo (isset($context["text_discount_info"]) ? $context["text_discount_info"] : null);
            echo "\">
                                ";
            // line 160
            echo (isset($context["text_discount"]) ? $context["text_discount"] : null);
            echo " 
                            </span>
                        </label>
                        <div class=\"col-sm-10\">
                            <div class = \"input-group\">
                                <input type=\"text\" class=\"form-control\" name=\"wk_preorder_discount\" onkeyup=\"this.value=this.value.replace(/[^\\d.]/,'');\" value=\"";
            // line 165
            if ((array_key_exists("wk_preorder_discount", $context) && (isset($context["wk_preorder_discount"]) ? $context["wk_preorder_discount"] : null))) {
                echo " ";
                echo (isset($context["wk_preorder_discount"]) ? $context["wk_preorder_discount"] : null);
            }
            echo "\"  /><span class = \"input-group-addon\">";
            echo (isset($context["type"]) ? $context["type"] : null);
            echo "</span>
                            </div>
                            ";
            // line 167
            if ((array_key_exists("error_discount", $context) && (isset($context["error_discount"]) ? $context["error_discount"] : null))) {
                echo " 
                                <div class=\"text-danger\">
                                    ";
                // line 169
                echo (isset($context["error_discount"]) ? $context["error_discount"] : null);
                echo " 
                                </div>
                            ";
            }
            // line 171
            echo " 
                        </div>
                    </div>
                    ";
        } else {
            // line 174
            echo " 
                        <input type=\"hidden\" name = \"wk_preorder_discount\" value = \"0\"/>
                        ";
        }
        // line 176
        echo " 
                    <div class=\"form-group\">
                        <label class=\"col-sm-2 control-label\">
                            <span data-toggle=\"tooltip\" data-original-title=\"";
        // line 179
        echo (isset($context["text_one_ip_one_order_info"]) ? $context["text_one_ip_one_order_info"] : null);
        echo "\">
                                ";
        // line 180
        echo (isset($context["text_one_ip_one_order"]) ? $context["text_one_ip_one_order"] : null);
        echo " 
                            </span>
                        </label>
                        <div class=\"col-sm-10\">
                            <select class=\"form-control\" name=\"wk_one_ip_one_order\">
                                <option value=\"0\" ";
        // line 185
        if ((array_key_exists("wk_one_ip_one_order", $context) &&  !(isset($context["wk_one_ip_one_order"]) ? $context["wk_one_ip_one_order"] : null))) {
            echo " ";
            echo "selected";
            echo " ";
        }
        echo " >";
        echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
        echo "</option>
                                <option value=\"1\" ";
        // line 186
        if ((array_key_exists("wk_one_ip_one_order", $context) && (isset($context["wk_one_ip_one_order"]) ? $context["wk_one_ip_one_order"] : null))) {
            echo " ";
            echo "selected";
            echo " ";
        }
        echo ">";
        echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
        echo "</option>
                            </select>
                            ";
        // line 188
        if ((array_key_exists("error_ip", $context) && (isset($context["error_ip"]) ? $context["error_ip"] : null))) {
            // line 189
            echo "                            <div class = \"text-danger\">";
            echo (isset($context["error_ip"]) ? $context["error_ip"] : null);
            echo "</div>
                            ";
        }
        // line 190
        echo " 
                        </div>
                    </div>
                    <div class=\"form-group required\">
                        <label class=\"col-sm-2 control-label\">
                            <span data-toggle=\"tooltip\" data-original-title=\"";
        // line 195
        echo (isset($context["text_preorder_start_date_info"]) ? $context["text_preorder_start_date_info"] : null);
        echo "\">
                                ";
        // line 196
        echo (isset($context["text_preorder_start_date"]) ? $context["text_preorder_start_date"] : null);
        echo " 
                            </span>
                        </label>
                        <div class=\"col-sm-3\">
                            <div class=\"input-group date\">
                                <input type=\"text\" class=\"form-control\" readonly name=\"wk_preorder_start_date\" value=\"";
        // line 201
        if (array_key_exists("wk_preorder_start_date", $context)) {
            echo " ";
            echo (isset($context["wk_preorder_start_date"]) ? $context["wk_preorder_start_date"] : null);
        }
        echo "\" />
                                <span class=\"input-group-btn\">
                                    <button type=\"button\" class=\"btn btn-default\">
                                        <i class=\"fa fa-calendar\"></i>
                                    </button>
                                </span>
                            </div>
                            ";
        // line 208
        if ((array_key_exists("form_error_start_date", $context) && (isset($context["form_error_start_date"]) ? $context["form_error_start_date"] : null))) {
            echo " 
                                <div class=\"text-danger\">
                                    ";
            // line 210
            echo (isset($context["form_error_start_date"]) ? $context["form_error_start_date"] : null);
            echo " 
                                </div>
                            ";
        }
        // line 212
        echo " 
                        </div>
                    </div>
                    <div class=\"form-group required\" id=\"date_selector\">
                        <label class=\"col-sm-2 control-label\">
                            <span data-toggle=\"tooltip\" data-original-title=\"";
        // line 217
        echo (isset($context["text_preorder_pro_available_date_info"]) ? $context["text_preorder_pro_available_date_info"] : null);
        echo "\">
                                ";
        // line 218
        echo (isset($context["text_preorder_date"]) ? $context["text_preorder_date"] : null);
        echo " 
                            </span>
                        </label>
                        <div class=\"col-sm-3\">
                            <div class=\"input-group date\">
                                <input type=\"text\" class=\"form-control\" readonly name=\"wk_available_date\" value=\"";
        // line 223
        if ((array_key_exists("wk_available_date", $context) && (isset($context["wk_available_date"]) ? $context["wk_available_date"] : null))) {
            echo " ";
            echo (isset($context["wk_available_date"]) ? $context["wk_available_date"] : null);
        }
        echo "\"  />
                                <span class=\"input-group-btn\">
                                    <button type=\"button\" class=\"btn btn-default\">
                                        <i class=\"fa fa-calendar\"></i>
                                    </button>
                                </span>
                            </div>
                            ";
        // line 230
        if ((array_key_exists("form_error_available_date", $context) && (isset($context["form_error_available_date"]) ? $context["form_error_available_date"] : null))) {
            echo " 
                                <div class=\"text-danger\">
                                    ";
            // line 232
            echo (isset($context["form_error_available_date"]) ? $context["form_error_available_date"] : null);
            echo " 
                                </div>
                            ";
        }
        // line 234
        echo " 
                        </div>
                    </div>
                    <div class=\"form-group\">
                        <label class=\"col-sm-2 control-label\">
                            ";
        // line 239
        echo (isset($context["text_status"]) ? $context["text_status"] : null);
        echo " 
                        </label>
                        <div class=\"col-sm-10\">
                            <select class=\"form-control\" name=\"wk_status\">
                                <option value=\"0\" ";
        // line 243
        if ((array_key_exists("wk_status", $context) &&  !(isset($context["wk_status"]) ? $context["wk_status"] : null))) {
            echo " ";
            echo "selected";
            echo " ";
        }
        echo " >";
        echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
        echo "</option>
                                <option value=\"1\" ";
        // line 244
        if ((array_key_exists("wk_status", $context) && (isset($context["wk_status"]) ? $context["wk_status"] : null))) {
            echo " ";
            echo "selected";
            echo " ";
        }
        echo ">";
        echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
        echo "</option>
                            </select>
                            ";
        // line 246
        if ((array_key_exists("error_preorder_status", $context) && (isset($context["error_preorder_status"]) ? $context["error_preorder_status"] : null))) {
            // line 247
            echo "                            <div class = \"text-danger\">";
            echo (isset($context["error_preorder_status"]) ? $context["error_preorder_status"] : null);
            echo "</div>
                            ";
        }
        // line 248
        echo " 
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type=\"text/javascript\">

\$('#wk_preorder_products').autocomplete({
  'source': function(request, response) {
    \$.ajax({
      url: 'index.php?route=catalog/wk_preordered_list/autocomplete&user_token=";
        // line 262
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "&filter_name=' +  encodeURIComponent(request),
      dataType: 'json',
      success: function(json) {
        response(\$.map(json, function(item) {
          return {
            label: item['name'],
            value: item['product_id']
          }
        }));
      }
    });
  },
  'select': function(item) {
    \$('#'+item['value']).remove();
    \$('input[name=\\'wk_preorder_products\\']').val(item['label']);
    \$('#pre-order-product').append('<div id=\"' + item['value'] + '\"><i class=\"fa fa-minus-circle\" onclick=\"\$(this).parent().remove()\" ></i> ' + item['label'] + '<input type=\"hidden\" name=\"pre_order_product[' + item['value'] + '][product_id]\" value=\"' + item['value'] + '\" /><input type=\"hidden\" name=\"pre_order_product[' + item['value'] + '][name]\" value=\"' + item['label'] + '\" /></div>');
    \$('#wk_preorder_products').val('');
  }
});

\$('select[name=\"wk_deduction_method\"]').on('change',function(){
  if(\$(this).val() == 'pc'){
    \$('#percentage_warning').removeClass('hide');
    \$('#percentage_price_block').slideDown();
    \$('#fixed_price_block').slideUp();
  }else{
    \$('#percentage_warning').addClass('hide');
    \$('#percentage_price_block').slideUp();
    \$('#fixed_price_block').slideDown();
  }
})

val = \$('#percentage_spinner').val();

\$('#percentage_spinner').on('blur',function(){
  val = \$('#percentage_spinner').val();
})

\$('#spinner_add').on('click', function() {
    if(parseInt(val) < 100) {
        \$('#percentage_spinner').val( parseInt(val)+1 );
        val = \$('#percentage_spinner').val();
    }
});

\$('#spinner_minus').on('click',function(){
  if(parseInt(val) > 0){
    \$('#percentage_spinner').val( val-1 );
    val = \$('#percentage_spinner').val();
  }
})

\$('.date').datetimepicker({
  pickTime: false,
  minDate:new Date(),
  format: 'YYYY-MM-DD',
})
  \$('.color-pick').ColorPicker({
      color: '#fcfcfc',
      onShow: function (colpkr) {
      \$(colpkr).fadeIn(500);
      return false;
      },
      onHide: function (colpkr) {
      \$(colpkr).fadeOut(500);
      return false;
      },
      onChange: function (hsb, hex, rgb) {
      \$('#backgroundth').val('#' + hex);
      \$('.color-pick').css('background-color','#'+hex);
      }
  });
</script>

";
        // line 336
        echo (isset($context["footer"]) ? $context["footer"] : null);
        echo " 
";
    }

    public function getTemplateName()
    {
        return "catalog/wk_preorder_productadd.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  730 => 336,  653 => 262,  637 => 248,  631 => 247,  629 => 246,  618 => 244,  608 => 243,  601 => 239,  594 => 234,  588 => 232,  583 => 230,  570 => 223,  562 => 218,  558 => 217,  551 => 212,  545 => 210,  540 => 208,  527 => 201,  519 => 196,  515 => 195,  508 => 190,  502 => 189,  500 => 188,  489 => 186,  479 => 185,  471 => 180,  467 => 179,  462 => 176,  457 => 174,  451 => 171,  445 => 169,  440 => 167,  430 => 165,  422 => 160,  418 => 159,  412 => 156,  407 => 153,  401 => 151,  396 => 149,  389 => 148,  382 => 144,  378 => 143,  371 => 138,  365 => 136,  360 => 134,  353 => 133,  346 => 129,  342 => 128,  335 => 123,  329 => 121,  324 => 119,  317 => 118,  310 => 114,  306 => 113,  299 => 108,  293 => 106,  288 => 104,  272 => 97,  259 => 87,  255 => 86,  246 => 84,  241 => 81,  235 => 80,  233 => 79,  222 => 77,  212 => 76,  204 => 71,  200 => 70,  193 => 65,  187 => 63,  182 => 61,  178 => 59,  174 => 58,  156 => 57,  150 => 56,  146 => 55,  142 => 53,  137 => 51,  132 => 50,  128 => 49,  121 => 45,  117 => 44,  111 => 41,  104 => 37,  98 => 33,  92 => 31,  86 => 28,  83 => 27,  77 => 25,  71 => 22,  64 => 17,  54 => 16,  48 => 15,  43 => 13,  34 => 9,  28 => 6,  19 => 1,);
    }
}
/* {{ header }}{{ column_left }} */
/* <div id="content">*/
/*     <div class="page-header">*/
/*         <div class="container-fluid">*/
/*             <div class="pull-right">*/
/*                 <button  onclick="$('#form').submit();" class="btn btn-primary" title="{{ button_save }}">*/
/*                     <i class="fa fa-save"></i>*/
/*                 </button>*/
/*                 <a href="{{ cancel }}" class="btn btn-default" title="{{ button_cancel }}">*/
/*                     <i class="fa fa-reply"></i>*/
/*                 </a>*/
/*             </div>*/
/*             <h1>{{ heading_title }}</h1>*/
/*             <ul class="breadcrumb">*/
/*                 {% for breadcrumb in breadcrumbs %} */
/*                     <li><a href="{{ breadcrumb['href'] }}">{{ breadcrumb['text'] }}</a></li>*/
/*                 {% endfor %} */
/*             </ul>*/
/*         </div>*/
/*     </div>*/
/*     <div class="container-fluid">*/
/*         {% if (error_warning) %} */
/*             <div class="alert alert-danger">*/
/*                 <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*                 {{ error_warning }} */
/*             </div>*/
/*         {% endif %} */
/*         {% if (form_error_warning is defined and form_error_warning) %} */
/*             <div class="alert alert-danger">*/
/*                 <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*                 {{ form_error_warning }} */
/*             </div>*/
/*         {% endif %} */
/*         <div class="panel panel-default">*/
/*             <div class="panel-heading">*/
/*                 <h3 class="panel-title">*/
/*                     {{ heading_title }} */
/*                 </h3>*/
/*             </div>*/
/*             <div class="panel-body">*/
/*                 <form action="{{ add }}" method="post" enctype="multipart/form-data" id="form" class="form-horizontal">*/
/*                     <div class="form-group required">*/
/*                         <label class="col-sm-2 control-label">*/
/*                             <span data-toggle="tooltip" data-original-title="{{ text_add_preorder_product_info }}">*/
/*                                 {{ text_add_preorder_product }} */
/*                             </span>*/
/*                         </label>*/
/*                         <div class="col-sm-10">*/
/*                             {% if (id is defined and id) %} */
/*                                 <input type="hidden" name="id" value="{{ id }}" />*/
/*                             {% else %} */
/*                                 <input type="text" class="form-control" name="wk_preorder_products" id="wk_preorder_products" />*/
/*                             {% endif %} */
/*                             <div id="pre-order-product" class="well well-sm" style="height: 150px; overflow: auto;">*/
/*                                 {% if (pre_order_product is defined and pre_order_product) %} */
/*                                     {% for value in pre_order_product %} */
/*                                         <div id="{{ value['product_id'] }}"><i class="fa fa-minus-circle" onclick="$(this).parent().remove()" ></i> {{ value['name'] }}<input type="hidden" name="pre_order_product[{{ value['product_id'] }}][product_id]" value="{{ value['product_id'] }}" /><input type="hidden" name="pre_order_product[{{ value['product_id'] }}][name]" value="{{ value['name'] }}" /></div>*/
/*                                     {% endfor %} */
/*                                 {% endif %} */
/*                             </div>*/
/*                             {% if (form_error_no_product is defined and form_error_no_product) %} */
/*                                 <div class="text-danger">*/
/*                                     {{ form_error_no_product }} */
/*                                 </div>*/
/*                             {% endif %} */
/*                         </div>*/
/*                     </div>*/
/*                     <div class="form-group">*/
/*                         <label class="col-sm-2 control-label">*/
/*                             <span data-toggle="tooltip" data-original-title="{{ text_deduction_method_info }}">*/
/*                                 {{ text_deduction_method }} */
/*                             </span>*/
/*                         </label>*/
/*                         <div class="col-sm-10">*/
/*                             <select class="form-control" name="wk_deduction_method" >*/
/*                                 <option value="fp" {% if (wk_deduction_method is defined and wk_deduction_method == 'fp') %} {{ 'selected' }} {% endif %} >{{ text_fixed_rate }}</option>*/
/*                                 <option value="pc" {% if (wk_deduction_method is defined and wk_deduction_method == 'pc') %} {{ 'selected' }} {% endif %}>{{ text_partial_rate }}</option>*/
/*                             </select>*/
/*                             {% if (error_method is defined and error_method) %}*/
/*                             <div class = "text-danger">{{ error_method }}</div>*/
/*                             {% endif %} */
/*                         </div>*/
/*                     </div>*/
/*                     <div class="form-group" id="percentage_price_block" {% if (wk_deduction_method is not defined or wk_deduction_method != 'pc') %} {{ 'style="display:none"' }} {% endif %} >*/
/*                         <label class="col-sm-2 control-label">*/
/*                             <span data-toggle="tooltip" data-original-title="{{ text_product_price_info }}">*/
/*                                 {{ text_product_price }} */
/*                             </span>*/
/*                         </label>*/
/*                         <div class="col-sm-3">*/
/*                             <div class="input-group">*/
/*                                 <span class="input-group-btn">*/
/*                                     <button class="btn btn-danger" type="button" id="spinner_minus">*/
/*                                         <i class="fa fa-minus"></i>*/
/*                                     </button>*/
/*                                 </span>*/
/*                                 <input type="text" class="form-control" maxlength="5" onkeyup="this.value=this.value.replace(/[^\d.]/,'');" name="wk_percentage_price" id="percentage_spinner" value="{% if (wk_percentage_price is defined and wk_percentage_price) %} {{ wk_percentage_price }}{% else %} {{ '0' }}{% endif %}" />*/
/*                                 <span class="input-group-btn">*/
/*                                     <button class="btn btn-success" id="spinner_add" type="button">*/
/*                                         <i class="fa fa-plus"></i>*/
/*                                     </button>*/
/*                                 </span>*/
/*                             </div>*/
/*                             {% if (form_error_no_percentage is defined and form_error_no_percentage) %} */
/*                                 <div class="text-danger">*/
/*                                     {{ form_error_no_percentage }} */
/*                                 </div>*/
/*                             {% endif %} */
/*                         </div>*/
/*                     </div>*/
/*                     <div class="form-group required">*/
/*                         <label class="col-sm-2 control-label">*/
/*                             <span data-toggle="tooltip" data-original-title="{{ text_preorder_quantity_info }}">*/
/*                                 {{ text_preorder_quantity }} */
/*                             </span>*/
/*                         </label>*/
/*                         <div class="col-sm-10">*/
/*                             <input type="text" class="form-control" name="wk_preorder_quantity" onkeyup="this.value=this.value.replace(/[^\d.]/,'');" value="{% if (wk_preorder_quantity is defined and wk_preorder_quantity) %} {{ wk_preorder_quantity }}{% endif %}" />*/
/*                             {% if (quantity_error is defined and quantity_error) %} */
/*                               <div class="text-danger">*/
/*                                   {{ quantity_error }} */
/*                               </div>*/
/*                            {% endif %} */
/*                         </div>*/
/*                     </div>*/
/*                     <div class="form-group required">*/
/*                         <label class="col-sm-2 control-label">*/
/*                             <span data-toggle="tooltip" data-original-title="{{ text_quantity_per_order_info }}">*/
/*                                 {{ text_quantity_per_order }} */
/*                             </span>*/
/*                         </label>*/
/*                         <div class="col-sm-10">*/
/*                             <input type="text" class="form-control" onkeyup="this.value=this.value.replace(/[^\d.]/,'');" name="wk_quantity_per_order" value="{% if (wk_quantity_per_order is defined and wk_quantity_per_order) %} {{ wk_quantity_per_order }}{% endif %}" >*/
/*                             {% if (quantity_per_oerror is defined and quantity_per_oerror) %} */
/*                                 <div class="text-danger">*/
/*                                     {{ quantity_per_oerror }} */
/*                                 </div>*/
/*                             {% endif %} */
/*                         </div>*/
/*                     </div>*/
/*                     <div class="form-group required">*/
/*                         <label class="col-sm-2 control-label">*/
/*                             <span data-toggle="tooltip" data-original-title="{{ text_order_per_customer_info }}">*/
/*                                 {{ text_order_per_customer }} */
/*                             </span>*/
/*                         </label>*/
/*                         <div class="col-sm-10">*/
/*                             <input type="text" class="form-control" name="wk_order_per_customer" onkeyup="this.value=this.value.replace(/[^\d.]/,'');" value="{% if (wk_order_per_customer is defined and wk_order_per_customer) %} {{ wk_order_per_customer }}{% endif %}"  />*/
/*                             {% if (quantity_cust_error is defined and quantity_cust_error) %} */
/*                                 <div class="text-danger">*/
/*                                     {{ quantity_cust_error }} */
/*                                 </div>*/
/*                             {% endif %} */
/*                         </div>*/
/*                     </div>*/
/*                     {% if (module_wk_preorder_pro_discount is defined and module_wk_preorder_pro_discount) %} */
/*                     <div class="form-group">*/
/*                         <label class="col-sm-2 control-label">*/
/*                             <span data-toggle="tooltip" data-original-title="{{ text_discount_info }}">*/
/*                                 {{ text_discount }} */
/*                             </span>*/
/*                         </label>*/
/*                         <div class="col-sm-10">*/
/*                             <div class = "input-group">*/
/*                                 <input type="text" class="form-control" name="wk_preorder_discount" onkeyup="this.value=this.value.replace(/[^\d.]/,'');" value="{% if (wk_preorder_discount is defined and wk_preorder_discount) %} {{ wk_preorder_discount }}{% endif %}"  /><span class = "input-group-addon">{{ type }}</span>*/
/*                             </div>*/
/*                             {% if (error_discount is defined and error_discount) %} */
/*                                 <div class="text-danger">*/
/*                                     {{ error_discount }} */
/*                                 </div>*/
/*                             {% endif %} */
/*                         </div>*/
/*                     </div>*/
/*                     {% else %} */
/*                         <input type="hidden" name = "wk_preorder_discount" value = "0"/>*/
/*                         {% endif %} */
/*                     <div class="form-group">*/
/*                         <label class="col-sm-2 control-label">*/
/*                             <span data-toggle="tooltip" data-original-title="{{ text_one_ip_one_order_info }}">*/
/*                                 {{ text_one_ip_one_order }} */
/*                             </span>*/
/*                         </label>*/
/*                         <div class="col-sm-10">*/
/*                             <select class="form-control" name="wk_one_ip_one_order">*/
/*                                 <option value="0" {% if (wk_one_ip_one_order is defined and not wk_one_ip_one_order) %} {{ "selected" }} {% endif %} >{{ text_disabled }}</option>*/
/*                                 <option value="1" {% if (wk_one_ip_one_order is defined and wk_one_ip_one_order) %} {{ "selected" }} {% endif %}>{{ text_enabled }}</option>*/
/*                             </select>*/
/*                             {% if (error_ip is defined and error_ip) %}*/
/*                             <div class = "text-danger">{{ error_ip }}</div>*/
/*                             {% endif %} */
/*                         </div>*/
/*                     </div>*/
/*                     <div class="form-group required">*/
/*                         <label class="col-sm-2 control-label">*/
/*                             <span data-toggle="tooltip" data-original-title="{{ text_preorder_start_date_info }}">*/
/*                                 {{ text_preorder_start_date }} */
/*                             </span>*/
/*                         </label>*/
/*                         <div class="col-sm-3">*/
/*                             <div class="input-group date">*/
/*                                 <input type="text" class="form-control" readonly name="wk_preorder_start_date" value="{% if (wk_preorder_start_date is defined) %} {{ wk_preorder_start_date }}{% endif %}" />*/
/*                                 <span class="input-group-btn">*/
/*                                     <button type="button" class="btn btn-default">*/
/*                                         <i class="fa fa-calendar"></i>*/
/*                                     </button>*/
/*                                 </span>*/
/*                             </div>*/
/*                             {% if (form_error_start_date is defined and form_error_start_date) %} */
/*                                 <div class="text-danger">*/
/*                                     {{ form_error_start_date }} */
/*                                 </div>*/
/*                             {% endif %} */
/*                         </div>*/
/*                     </div>*/
/*                     <div class="form-group required" id="date_selector">*/
/*                         <label class="col-sm-2 control-label">*/
/*                             <span data-toggle="tooltip" data-original-title="{{ text_preorder_pro_available_date_info }}">*/
/*                                 {{ text_preorder_date }} */
/*                             </span>*/
/*                         </label>*/
/*                         <div class="col-sm-3">*/
/*                             <div class="input-group date">*/
/*                                 <input type="text" class="form-control" readonly name="wk_available_date" value="{% if (wk_available_date is defined and wk_available_date) %} {{ wk_available_date }}{% endif %}"  />*/
/*                                 <span class="input-group-btn">*/
/*                                     <button type="button" class="btn btn-default">*/
/*                                         <i class="fa fa-calendar"></i>*/
/*                                     </button>*/
/*                                 </span>*/
/*                             </div>*/
/*                             {% if (form_error_available_date is defined and form_error_available_date) %} */
/*                                 <div class="text-danger">*/
/*                                     {{ form_error_available_date }} */
/*                                 </div>*/
/*                             {% endif %} */
/*                         </div>*/
/*                     </div>*/
/*                     <div class="form-group">*/
/*                         <label class="col-sm-2 control-label">*/
/*                             {{ text_status }} */
/*                         </label>*/
/*                         <div class="col-sm-10">*/
/*                             <select class="form-control" name="wk_status">*/
/*                                 <option value="0" {% if (wk_status is defined and not wk_status) %} {{ "selected" }} {% endif %} >{{ text_disabled }}</option>*/
/*                                 <option value="1" {% if (wk_status is defined and wk_status) %} {{ "selected" }} {% endif %}>{{ text_enabled }}</option>*/
/*                             </select>*/
/*                             {% if (error_preorder_status is defined and error_preorder_status) %}*/
/*                             <div class = "text-danger">{{ error_preorder_status }}</div>*/
/*                             {% endif %} */
/*                         </div>*/
/*                     </div>*/
/*                 </form>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* */
/* <script type="text/javascript">*/
/* */
/* $('#wk_preorder_products').autocomplete({*/
/*   'source': function(request, response) {*/
/*     $.ajax({*/
/*       url: 'index.php?route=catalog/wk_preordered_list/autocomplete&user_token={{ user_token }}&filter_name=' +  encodeURIComponent(request),*/
/*       dataType: 'json',*/
/*       success: function(json) {*/
/*         response($.map(json, function(item) {*/
/*           return {*/
/*             label: item['name'],*/
/*             value: item['product_id']*/
/*           }*/
/*         }));*/
/*       }*/
/*     });*/
/*   },*/
/*   'select': function(item) {*/
/*     $('#'+item['value']).remove();*/
/*     $('input[name=\'wk_preorder_products\']').val(item['label']);*/
/*     $('#pre-order-product').append('<div id="' + item['value'] + '"><i class="fa fa-minus-circle" onclick="$(this).parent().remove()" ></i> ' + item['label'] + '<input type="hidden" name="pre_order_product[' + item['value'] + '][product_id]" value="' + item['value'] + '" /><input type="hidden" name="pre_order_product[' + item['value'] + '][name]" value="' + item['label'] + '" /></div>');*/
/*     $('#wk_preorder_products').val('');*/
/*   }*/
/* });*/
/* */
/* $('select[name="wk_deduction_method"]').on('change',function(){*/
/*   if($(this).val() == 'pc'){*/
/*     $('#percentage_warning').removeClass('hide');*/
/*     $('#percentage_price_block').slideDown();*/
/*     $('#fixed_price_block').slideUp();*/
/*   }else{*/
/*     $('#percentage_warning').addClass('hide');*/
/*     $('#percentage_price_block').slideUp();*/
/*     $('#fixed_price_block').slideDown();*/
/*   }*/
/* })*/
/* */
/* val = $('#percentage_spinner').val();*/
/* */
/* $('#percentage_spinner').on('blur',function(){*/
/*   val = $('#percentage_spinner').val();*/
/* })*/
/* */
/* $('#spinner_add').on('click', function() {*/
/*     if(parseInt(val) < 100) {*/
/*         $('#percentage_spinner').val( parseInt(val)+1 );*/
/*         val = $('#percentage_spinner').val();*/
/*     }*/
/* });*/
/* */
/* $('#spinner_minus').on('click',function(){*/
/*   if(parseInt(val) > 0){*/
/*     $('#percentage_spinner').val( val-1 );*/
/*     val = $('#percentage_spinner').val();*/
/*   }*/
/* })*/
/* */
/* $('.date').datetimepicker({*/
/*   pickTime: false,*/
/*   minDate:new Date(),*/
/*   format: 'YYYY-MM-DD',*/
/* })*/
/*   $('.color-pick').ColorPicker({*/
/*       color: '#fcfcfc',*/
/*       onShow: function (colpkr) {*/
/*       $(colpkr).fadeIn(500);*/
/*       return false;*/
/*       },*/
/*       onHide: function (colpkr) {*/
/*       $(colpkr).fadeOut(500);*/
/*       return false;*/
/*       },*/
/*       onChange: function (hsb, hex, rgb) {*/
/*       $('#backgroundth').val('#' + hex);*/
/*       $('.color-pick').css('background-color','#'+hex);*/
/*       }*/
/*   });*/
/* </script>*/
/* */
/* {{ footer }} */
/* */
