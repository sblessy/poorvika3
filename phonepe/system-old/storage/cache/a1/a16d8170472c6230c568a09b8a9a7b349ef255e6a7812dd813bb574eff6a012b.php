<?php

/* extension/module/so_countdown/list.twig */
class __TwigTemplate_083cbfe85dd594e316dee5d49cadae3a68aad54ec9a3731e86b73318df6701e7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo "
";
        // line 2
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "
<div id=\"content\">
  \t<div class=\"page-header\">
    \t<div class=\"container-fluid\">
      \t\t<div class=\"pull-right\">
      \t\t\t<a href=\"";
        // line 7
        echo (isset($context["add"]) ? $context["add"] : null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo (isset($context["button_add"]) ? $context["button_add"] : null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-plus\"></i></a>
      \t\t\t<button type=\"button\" data-toggle=\"tooltip\" title=\"";
        // line 8
        echo (isset($context["button_delete"]) ? $context["button_delete"] : null);
        echo "\" class=\"btn btn-danger\" onclick=\"confirm('";
        echo (isset($context["text_confirm"]) ? $context["text_confirm"] : null);
        echo "') ? \$('#form-category').submit() : false;\"><i class=\"fa fa-trash-o\"></i></button>
\t      \t</div>
\t      \t<h1>";
        // line 10
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
\t      \t<ul class=\"breadcrumb\">
\t        \t";
        // line 12
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 13
            echo "\t        \t<li><a href=\"";
            echo $this->getAttribute($context["breadcrumb"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array());
            echo "</a></li>
\t        \t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 15
        echo "\t      \t</ul>
\t    </div>
    </div>
    <div class=\"container-fluid\">
    \t";
        // line 19
        if ((isset($context["error_warning"]) ? $context["error_warning"] : null)) {
            // line 20
            echo "\t    <div class=\"alert alert-danger\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo (isset($context["error_warning"]) ? $context["error_warning"] : null);
            echo "
\t      \t<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
\t    </div>
\t    ";
        }
        // line 24
        echo "\t    ";
        if ((isset($context["success"]) ? $context["success"] : null)) {
            // line 25
            echo "\t    <div class=\"alert alert-success\"><i class=\"fa fa-check-circle\"></i> ";
            echo (isset($context["success"]) ? $context["success"] : null);
            echo "
\t      \t<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
\t    </div>
\t    ";
        }
        // line 29
        echo "\t    <div class=\"panel panel-default\">
      \t\t<div class=\"panel-heading\">
        \t\t<h3 class=\"panel-title\"><i class=\"fa fa-list\"></i> ";
        // line 31
        echo (isset($context["text_list"]) ? $context["text_list"] : null);
        echo "</h3>
      \t\t</div>
      \t\t<div class=\"panel-body\">
        \t\t<form action=\"";
        // line 34
        echo (isset($context["delete"]) ? $context["delete"] : null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-category\">
        \t\t\t<div class=\"table-responsive\">
        \t\t\t\t<table class=\"table table-bordered table-hover\">
\t\t\t              \t<thead>
\t\t\t                \t<tr>
\t\t\t                  \t\t<td style=\"width: 1px;\" class=\"text-center\"><input type=\"checkbox\" onclick=\"\$('input[name*=\\'selected\\']').prop('checked', this.checked);\" /></td>
\t\t\t                  \t\t<td class=\"text-center\">";
        // line 40
        echo (isset($context["column_image"]) ? $context["column_image"] : null);
        echo "</td>
\t\t\t                  \t\t<td class=\"text-center\">
\t\t\t                  \t\t\t";
        // line 42
        if (((isset($context["sort"]) ? $context["sort"] : null) == "name")) {
            // line 43
            echo "\t\t\t\t\t                    \t<a href=\"";
            echo (isset($context["sort_name"]) ? $context["sort_name"] : null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, (isset($context["order"]) ? $context["order"] : null));
            echo "\">";
            echo (isset($context["column_name"]) ? $context["column_name"] : null);
            echo "</a>
\t\t\t\t\t                    ";
        } else {
            // line 45
            echo "\t\t\t\t\t                    \t<a href=\"";
            echo (isset($context["sort_name"]) ? $context["sort_name"] : null);
            echo "\">";
            echo (isset($context["column_name"]) ? $context["column_name"] : null);
            echo "</a>
\t\t\t\t\t                    ";
        }
        // line 47
        echo "\t\t\t\t\t                </td>
\t\t\t\t\t                <td class=\"text-center\">
\t\t\t\t\t                \t";
        // line 49
        if (((isset($context["sort"]) ? $context["sort"] : null) == "sort_order")) {
            // line 50
            echo "\t\t\t\t\t                    \t<a href=\"";
            echo (isset($context["sort_sort_order"]) ? $context["sort_sort_order"] : null);
            echo "\" class=\"";
            echo twig_lower_filter($this->env, (isset($context["order"]) ? $context["order"] : null));
            echo "\">";
            echo (isset($context["column_sort_order"]) ? $context["column_sort_order"] : null);
            echo "</a>
\t\t\t\t\t                    ";
        } else {
            // line 52
            echo "\t\t\t\t\t                    \t<a href=\"";
            echo (isset($context["sort_sort_order"]) ? $context["sort_sort_order"] : null);
            echo "\">";
            echo (isset($context["column_sort_order"]) ? $context["column_sort_order"] : null);
            echo "</a>
\t\t\t\t\t                    ";
        }
        // line 54
        echo "\t\t\t\t\t                </td>
\t\t\t\t\t                <td class=\"text-center\">";
        // line 55
        echo (isset($context["column_status"]) ? $context["column_status"] : null);
        echo "</td>
\t\t\t\t\t                <td class=\"text-center\">";
        // line 56
        echo (isset($context["column_date_start_expire"]) ? $context["column_date_start_expire"] : null);
        echo "</td>
\t\t\t\t\t                <td class=\"text-center\">";
        // line 57
        echo (isset($context["column_store"]) ? $context["column_store"] : null);
        echo "</td>
\t\t\t\t                  \t<td class=\"text-center\">";
        // line 58
        echo (isset($context["column_action"]) ? $context["column_action"] : null);
        echo "</td>
\t\t\t\t                </tr>
\t\t\t\t            </thead>
\t\t\t\t            <tbody>
\t\t\t\t            \t";
        // line 62
        if ((array_key_exists("lists", $context) && (isset($context["lists"]) ? $context["lists"] : null))) {
            // line 63
            echo "                \t\t\t\t\t";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["lists"]) ? $context["lists"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["list"]) {
                // line 64
                echo "                \t\t\t\t\t\t<tr>
\t                \t\t\t\t\t\t<td class=\"text-center\">
\t                \t\t\t\t\t\t\t";
                // line 66
                if (twig_in_filter($this->getAttribute($context["list"], "id", array()), (isset($context["selected"]) ? $context["selected"] : null))) {
                    // line 67
                    echo "\t\t\t\t\t\t\t                    \t<input type=\"checkbox\" name=\"selected[]\" value=\"";
                    echo $this->getAttribute($context["list"], "id", array());
                    echo "\" checked=\"checked\" />
\t\t\t\t\t\t\t                    ";
                } else {
                    // line 69
                    echo "\t\t\t\t\t\t\t                    \t<input type=\"checkbox\" name=\"selected[]\" value=\"";
                    echo $this->getAttribute($context["list"], "id", array());
                    echo "\" />
\t\t\t\t\t\t\t                    ";
                }
                // line 71
                echo "\t\t\t\t\t\t\t                </td>
\t\t\t\t\t\t\t                <td class=\"text-center\"><img src=\"";
                // line 72
                echo $this->getAttribute($context["list"], "image", array());
                echo "\" /></td>
\t\t\t\t\t\t\t                <td class=\"text-center\">";
                // line 73
                echo $this->getAttribute($context["list"], "name", array());
                echo "</td>
\t\t\t\t\t\t\t                <td class=\"text-center\">";
                // line 74
                echo $this->getAttribute($context["list"], "sort_order", array());
                echo "</td>
\t\t\t\t\t\t\t                <td class=\"text-center\">";
                // line 75
                echo ((($this->getAttribute($context["list"], "status", array()) == 1)) ? ((isset($context["text_enabled"]) ? $context["text_enabled"] : null)) : ((isset($context["text_disabled"]) ? $context["text_disabled"] : null)));
                echo "</td>
\t\t\t\t\t\t\t                <td class=\"text-center\">";
                // line 76
                echo $this->getAttribute($context["list"], "date_start", array());
                echo "<br/><span style=\"color:red;\">";
                echo $this->getAttribute($context["list"], "date_expire", array());
                echo "</span></td>
\t\t\t\t\t\t\t                <td class=\"text-center\" style=\"font-weight:bold;\">
\t\t\t\t\t\t\t                \t";
                // line 78
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["list"], "stores", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["store"]) {
                    // line 79
                    echo "\t\t\t\t\t\t                \t\t\t";
                    echo $context["store"];
                    echo "<br />
\t\t\t\t\t\t\t                \t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['store'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 81
                echo "\t\t\t\t\t\t\t                </td>
\t\t\t\t\t\t\t                <td class=\"text-center\"><a href=\"";
                // line 82
                echo $this->getAttribute($context["list"], "edit", array());
                echo "\" data-toggle=\"tooltip\" title=\"";
                echo (isset($context["button_edit"]) ? $context["button_edit"] : null);
                echo "\" class=\"btn btn-primary\"><i class=\"fa fa-pencil\"></i></a></td>
\t\t\t\t\t\t\t          \t</tr>
                \t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['list'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 85
            echo "\t\t\t\t                ";
        } else {
            // line 86
            echo "\t\t\t\t                <tr>
\t\t\t\t                  \t<td class=\"text-center\" colspan=\"8\">";
            // line 87
            echo (isset($context["text_no_results"]) ? $context["text_no_results"] : null);
            echo "</td>
\t\t\t\t                </tr>
\t\t\t\t                ";
        }
        // line 90
        echo "\t\t\t\t            </tbody>
\t\t\t\t        </table>
        \t\t\t</div>
        \t\t</form>
        \t</div>
        </div>
    </div>
</div>
";
        // line 98
        echo (isset($context["footer"]) ? $context["footer"] : null);
    }

    public function getTemplateName()
    {
        return "extension/module/so_countdown/list.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  281 => 98,  271 => 90,  265 => 87,  262 => 86,  259 => 85,  248 => 82,  245 => 81,  236 => 79,  232 => 78,  225 => 76,  221 => 75,  217 => 74,  213 => 73,  209 => 72,  206 => 71,  200 => 69,  194 => 67,  192 => 66,  188 => 64,  183 => 63,  181 => 62,  174 => 58,  170 => 57,  166 => 56,  162 => 55,  159 => 54,  151 => 52,  141 => 50,  139 => 49,  135 => 47,  127 => 45,  117 => 43,  115 => 42,  110 => 40,  101 => 34,  95 => 31,  91 => 29,  83 => 25,  80 => 24,  72 => 20,  70 => 19,  64 => 15,  53 => 13,  49 => 12,  44 => 10,  37 => 8,  31 => 7,  23 => 2,  19 => 1,);
    }
}
/* {{ header }}*/
/* {{ column_left }}*/
/* <div id="content">*/
/*   	<div class="page-header">*/
/*     	<div class="container-fluid">*/
/*       		<div class="pull-right">*/
/*       			<a href="{{ add }}" data-toggle="tooltip" title="{{ button_add }}" class="btn btn-primary"><i class="fa fa-plus"></i></a>*/
/*       			<button type="button" data-toggle="tooltip" title="{{ button_delete }}" class="btn btn-danger" onclick="confirm('{{ text_confirm }}') ? $('#form-category').submit() : false;"><i class="fa fa-trash-o"></i></button>*/
/* 	      	</div>*/
/* 	      	<h1>{{ heading_title }}</h1>*/
/* 	      	<ul class="breadcrumb">*/
/* 	        	{% for breadcrumb in breadcrumbs %}*/
/* 	        	<li><a href="{{ breadcrumb.href }}">{{ breadcrumb.text }}</a></li>*/
/* 	        	{% endfor %}*/
/* 	      	</ul>*/
/* 	    </div>*/
/*     </div>*/
/*     <div class="container-fluid">*/
/*     	{% if error_warning %}*/
/* 	    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> {{ error_warning }}*/
/* 	      	<button type="button" class="close" data-dismiss="alert">&times;</button>*/
/* 	    </div>*/
/* 	    {% endif %}*/
/* 	    {% if success %}*/
/* 	    <div class="alert alert-success"><i class="fa fa-check-circle"></i> {{ success }}*/
/* 	      	<button type="button" class="close" data-dismiss="alert">&times;</button>*/
/* 	    </div>*/
/* 	    {% endif %}*/
/* 	    <div class="panel panel-default">*/
/*       		<div class="panel-heading">*/
/*         		<h3 class="panel-title"><i class="fa fa-list"></i> {{ text_list }}</h3>*/
/*       		</div>*/
/*       		<div class="panel-body">*/
/*         		<form action="{{ delete }}" method="post" enctype="multipart/form-data" id="form-category">*/
/*         			<div class="table-responsive">*/
/*         				<table class="table table-bordered table-hover">*/
/* 			              	<thead>*/
/* 			                	<tr>*/
/* 			                  		<td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>*/
/* 			                  		<td class="text-center">{{ column_image }}</td>*/
/* 			                  		<td class="text-center">*/
/* 			                  			{% if sort == 'name' %}*/
/* 					                    	<a href="{{ sort_name }}" class="{{ order|lower }}">{{ column_name }}</a>*/
/* 					                    {% else %}*/
/* 					                    	<a href="{{ sort_name }}">{{ column_name }}</a>*/
/* 					                    {% endif %}*/
/* 					                </td>*/
/* 					                <td class="text-center">*/
/* 					                	{% if sort == 'sort_order' %}*/
/* 					                    	<a href="{{ sort_sort_order }}" class="{{ order|lower }}">{{ column_sort_order }}</a>*/
/* 					                    {% else %}*/
/* 					                    	<a href="{{ sort_sort_order }}">{{ column_sort_order }}</a>*/
/* 					                    {% endif %}*/
/* 					                </td>*/
/* 					                <td class="text-center">{{ column_status }}</td>*/
/* 					                <td class="text-center">{{ column_date_start_expire }}</td>*/
/* 					                <td class="text-center">{{ column_store }}</td>*/
/* 				                  	<td class="text-center">{{ column_action }}</td>*/
/* 				                </tr>*/
/* 				            </thead>*/
/* 				            <tbody>*/
/* 				            	{% if lists is defined and lists %}*/
/*                 					{% for list in lists %}*/
/*                 						<tr>*/
/* 	                						<td class="text-center">*/
/* 	                							{% if list.id in selected %}*/
/* 							                    	<input type="checkbox" name="selected[]" value="{{ list.id }}" checked="checked" />*/
/* 							                    {% else %}*/
/* 							                    	<input type="checkbox" name="selected[]" value="{{ list.id }}" />*/
/* 							                    {% endif %}*/
/* 							                </td>*/
/* 							                <td class="text-center"><img src="{{ list.image }}" /></td>*/
/* 							                <td class="text-center">{{ list.name }}</td>*/
/* 							                <td class="text-center">{{ list.sort_order }}</td>*/
/* 							                <td class="text-center">{{ list.status == 1 ? text_enabled : text_disabled }}</td>*/
/* 							                <td class="text-center">{{ list.date_start }}<br/><span style="color:red;">{{ list.date_expire }}</span></td>*/
/* 							                <td class="text-center" style="font-weight:bold;">*/
/* 							                	{% for store in list.stores %}*/
/* 						                			{{ store }}<br />*/
/* 							                	{% endfor %}*/
/* 							                </td>*/
/* 							                <td class="text-center"><a href="{{ list.edit }}" data-toggle="tooltip" title="{{ button_edit }}" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>*/
/* 							          	</tr>*/
/*                 					{% endfor %}*/
/* 				                {% else %}*/
/* 				                <tr>*/
/* 				                  	<td class="text-center" colspan="8">{{ text_no_results }}</td>*/
/* 				                </tr>*/
/* 				                {% endif %}*/
/* 				            </tbody>*/
/* 				        </table>*/
/*         			</div>*/
/*         		</form>*/
/*         	</div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* {{ footer }}*/
