<?php

/* default/template/checkout/cart.twig */
class __TwigTemplate_c30cd1a042898f67a24dbd7f2fcd599238fd30c1064abfb88cd26458d6ac96ff extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo "
<div id=\"checkout-cart\" class=\"container\">
  <ul class=\"breadcrumb\">
    ";
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 5
            echo "    <li><a href=\"";
            echo $this->getAttribute($context["breadcrumb"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array());
            echo "</a></li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 7
        echo "  </ul>
  ";
        // line 8
        if ((isset($context["attention"]) ? $context["attention"] : null)) {
            // line 9
            echo "  <div class=\"alert alert-info\"><i class=\"fa fa-info-circle\"></i> ";
            echo (isset($context["attention"]) ? $context["attention"] : null);
            echo "
    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
  </div>
  ";
        }
        // line 13
        echo "  ";
        if ((isset($context["success"]) ? $context["success"] : null)) {
            // line 14
            echo "  <div class=\"alert alert-success alert-dismissible\"><i class=\"fa fa-check-circle\"></i> ";
            echo (isset($context["success"]) ? $context["success"] : null);
            echo "
    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
  </div>
  ";
        }
        // line 18
        echo "  ";
        if ((isset($context["error_warning"]) ? $context["error_warning"] : null)) {
            // line 19
            echo "  <div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo (isset($context["error_warning"]) ? $context["error_warning"] : null);
            echo "
    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
  </div>
  ";
        }
        // line 23
        echo "  <div class=\"row\">";
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "
    ";
        // line 24
        if (((isset($context["column_left"]) ? $context["column_left"] : null) && (isset($context["column_right"]) ? $context["column_right"] : null))) {
            // line 25
            echo "    ";
            $context["class"] = "col-sm-6";
            // line 26
            echo "    ";
        } elseif (((isset($context["column_left"]) ? $context["column_left"] : null) || (isset($context["column_right"]) ? $context["column_right"] : null))) {
            // line 27
            echo "    ";
            $context["class"] = "col-sm-9";
            // line 28
            echo "    ";
        } else {
            // line 29
            echo "    ";
            $context["class"] = "col-sm-12";
            // line 30
            echo "    ";
        }
        // line 31
        echo "    <div id=\"content\" class=\"";
        echo (isset($context["class"]) ? $context["class"] : null);
        echo "\">";
        echo (isset($context["content_top"]) ? $context["content_top"] : null);
        echo "
      <h1>";
        // line 32
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "
        ";
        // line 33
        if ((isset($context["weight"]) ? $context["weight"] : null)) {
            // line 34
            echo "        &nbsp;(";
            echo (isset($context["weight"]) ? $context["weight"] : null);
            echo ")
        ";
        }
        // line 35
        echo " </h1>
      <form action=\"";
        // line 36
        echo (isset($context["action"]) ? $context["action"] : null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\">
        <div class=\"table-responsive\">
          <table class=\"table table-bordered\">
            <thead>
              <tr>
                <td class=\"text-center\">";
        // line 41
        echo (isset($context["column_image"]) ? $context["column_image"] : null);
        echo "</td>
                <td class=\"text-left\">";
        // line 42
        echo (isset($context["column_name"]) ? $context["column_name"] : null);
        echo "</td>
                <td class=\"text-left\">";
        // line 43
        echo (isset($context["column_model"]) ? $context["column_model"] : null);
        echo "</td>
                <td class=\"text-left\">";
        // line 44
        echo (isset($context["column_quantity"]) ? $context["column_quantity"] : null);
        echo "</td>
                <td class=\"text-right\">";
        // line 45
        echo (isset($context["column_price"]) ? $context["column_price"] : null);
        echo "</td>
                <td class=\"text-right\">";
        // line 46
        echo (isset($context["column_total"]) ? $context["column_total"] : null);
        echo "</td>
              </tr>
            </thead>
            <tbody>
            
            ";
        // line 51
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 52
            echo "            <tr>
              <td class=\"text-center\">";
            // line 53
            if ($this->getAttribute($context["product"], "thumb", array())) {
                echo " <a href=\"";
                echo $this->getAttribute($context["product"], "href", array());
                echo "\"><img src=\"";
                echo $this->getAttribute($context["product"], "thumb", array());
                echo "\" alt=\"";
                echo $this->getAttribute($context["product"], "name", array());
                echo "\" title=\"";
                echo $this->getAttribute($context["product"], "name", array());
                echo "\" class=\"img-thumbnail\" /></a> ";
            }
            echo "</td>
              <td class=\"text-left\"><a href=\"";
            // line 54
            echo $this->getAttribute($context["product"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["product"], "name", array());
            echo "</a> ";
            if ( !$this->getAttribute($context["product"], "stock", array())) {
                echo " <span class=\"text-danger\">***</span> ";
            }
            // line 55
            echo "                ";
            if ($this->getAttribute($context["product"], "option", array())) {
                // line 56
                echo "                ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["product"], "option", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["option"]) {
                    echo " <br />
                <small>";
                    // line 57
                    echo $this->getAttribute($context["option"], "name", array());
                    echo ": ";
                    echo $this->getAttribute($context["option"], "value", array());
                    echo "</small> ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 58
                echo "                ";
            }
            // line 59
            echo "                ";
            if ($this->getAttribute($context["product"], "reward", array())) {
                echo " <br />
                <small>";
                // line 60
                echo $this->getAttribute($context["product"], "reward", array());
                echo "</small> ";
            }
            // line 61
            echo "                ";
            if ($this->getAttribute($context["product"], "recurring", array())) {
                echo " <br />
                <span class=\"label label-info\">";
                // line 62
                echo (isset($context["text_recurring_item"]) ? $context["text_recurring_item"] : null);
                echo "</span> <small>";
                echo $this->getAttribute($context["product"], "recurring", array());
                echo "</small> ";
            }
            echo "</td>
              <td class=\"text-left\">";
            // line 63
            echo $this->getAttribute($context["product"], "model", array());
            echo "</td>
              <td class=\"text-left\"><div class=\"input-group btn-block\" style=\"max-width: 200px;\">
                  
                  ";
            // line 66
            if ((array_key_exists("quantity_status", $context) && (isset($context["quantity_status"]) ? $context["quantity_status"] : null))) {
                echo " 
                    <input type=\"text\" name=\"quantity[";
                // line 67
                echo $this->getAttribute($context["product"], "cart_id", array(), "array");
                echo "]\" value=\"";
                echo $this->getAttribute($context["product"], "quantity", array(), "array");
                echo "\" size=\"1\" class=\"form-control\" readonly/>
                  ";
            } else {
                // line 68
                echo " 
                      <input type=\"text\" name=\"quantity[";
                // line 69
                echo $this->getAttribute($context["product"], "cart_id", array(), "array");
                echo "]\" value=\"";
                echo $this->getAttribute($context["product"], "quantity", array(), "array");
                echo "\" size=\"1\" class=\"form-control\" />
                  ";
            }
            // line 71
            echo "                
                  <span class=\"input-group-btn\">
                  <button type=\"submit\" data-toggle=\"tooltip\" title=\"";
            // line 73
            echo (isset($context["button_update"]) ? $context["button_update"] : null);
            echo "\" class=\"btn btn-primary\"><i class=\"fa fa-refresh\"></i></button>
                  <button type=\"button\" data-toggle=\"tooltip\" title=\"";
            // line 74
            echo (isset($context["button_remove"]) ? $context["button_remove"] : null);
            echo "\" class=\"btn btn-danger\" onclick=\"cart.remove('";
            echo $this->getAttribute($context["product"], "cart_id", array());
            echo "');\"><i class=\"fa fa-times-circle\"></i></button>
                  </span></div></td>
              <td class=\"text-right\">";
            // line 76
            echo $this->getAttribute($context["product"], "price", array());
            echo "</td>
              <td class=\"text-right\">";
            // line 77
            echo $this->getAttribute($context["product"], "total", array());
            echo "</td>
            </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 80
        echo "            ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["vouchers"]) ? $context["vouchers"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["voucher"]) {
            // line 81
            echo "            <tr>
              <td></td>
              <td class=\"text-left\">";
            // line 83
            echo $this->getAttribute($context["voucher"], "description", array());
            echo "</td>
              <td class=\"text-left\"></td>
              <td class=\"text-left\"><div class=\"input-group btn-block\" style=\"max-width: 200px;\">
                  <input type=\"text\" name=\"\" value=\"1\" size=\"1\" disabled=\"disabled\" class=\"form-control\" />
                  <span class=\"input-group-btn\">
                  <button type=\"button\" data-toggle=\"tooltip\" title=\"";
            // line 88
            echo (isset($context["button_remove"]) ? $context["button_remove"] : null);
            echo "\" class=\"btn btn-danger\" onclick=\"voucher.remove('";
            echo $this->getAttribute($context["voucher"], "key", array());
            echo "');\"><i class=\"fa fa-times-circle\"></i></button>
                  </span></div></td>
              <td class=\"text-right\">";
            // line 90
            echo $this->getAttribute($context["voucher"], "amount", array());
            echo "</td>
              <td class=\"text-right\">";
            // line 91
            echo $this->getAttribute($context["voucher"], "amount", array());
            echo "</td>
            </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['voucher'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 94
        echo "              </tbody>
            
          </table>
        </div>
      </form>
      ";
        // line 99
        if ((isset($context["modules"]) ? $context["modules"] : null)) {
            // line 100
            echo "      <h2>";
            echo (isset($context["text_next"]) ? $context["text_next"] : null);
            echo "</h2>
      <p>";
            // line 101
            echo (isset($context["text_next_choice"]) ? $context["text_next_choice"] : null);
            echo "</p>
      <div class=\"panel-group\" id=\"accordion\"> ";
            // line 102
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["modules"]) ? $context["modules"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                // line 103
                echo "        ";
                echo $context["module"];
                echo "
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 104
            echo " </div>
      ";
        }
        // line 105
        echo " <br />
      <div class=\"row\">
        <div class=\"col-sm-4 col-sm-offset-8\">
          <table class=\"table table-bordered\">
            ";
        // line 109
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["totals"]) ? $context["totals"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["total"]) {
            // line 110
            echo "            <tr>
              <td class=\"text-right\"><strong>";
            // line 111
            echo $this->getAttribute($context["total"], "title", array());
            echo ":</strong></td>
              <td class=\"text-right\">";
            // line 112
            echo $this->getAttribute($context["total"], "text", array());
            echo "</td>
            </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['total'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 115
        echo "          </table>
        </div>
      </div>
      <div class=\"buttons clearfix\">
        <div class=\"pull-left\"><a href=\"";
        // line 119
        echo (isset($context["continue"]) ? $context["continue"] : null);
        echo "\" class=\"btn btn-default\">";
        echo (isset($context["button_shopping"]) ? $context["button_shopping"] : null);
        echo "</a></div>
        <div class=\"pull-right\"><a href=\"";
        // line 120
        echo (isset($context["checkout"]) ? $context["checkout"] : null);
        echo "\" class=\"btn btn-primary\">";
        echo (isset($context["button_checkout"]) ? $context["button_checkout"] : null);
        echo "</a></div>
      </div>
      ";
        // line 122
        echo (isset($context["content_bottom"]) ? $context["content_bottom"] : null);
        echo "</div>
    ";
        // line 123
        echo (isset($context["column_right"]) ? $context["column_right"] : null);
        echo "</div>
   
   
</div>
<div class=\"cart-page\">
<div class=\"container\">
 <div class=\"row\">
         
        <div class=\"col-md-8\">
            <div class=\"cart-block\">
                <h3><i class=\"fa fa-cart-arrow-down\"></i>My cart</h3>
                <ul>
                    <li>
                        <div class=\"cart-sec\">
                        <div class=\"cart-img\">
                           <a href=\"\"><img src=\"http://poorvikabeta.webindia.com/image/cache/catalog/demo/imac_1-90x120.jpg\"></a>
                        </div>
                        <div class=\"cart-cont\">
                            <div class=\"cart-det\">
                                <div class=\"cart-info\">
                                    <h6>Dell Laptop</h6>
                                    <small>8Gb RAM</small>
                                    <strong>&#8377;1,06,000</strong>
                                    <small class=\"available-off-cart\"><strike>&#8377;2,06,000</strike>(4% OFF) <span>9 offer available<i class=\"fa fa-info\"></i></span></small>
                                    
                                </div>
                                <div class=\"cart-delivery\">
                                    <select name=\"type\" class=\"custom-select\">
                                      <option value=\"\">Regulary Delivery</option>
                                      <option value=\"\">Fast Delivery</option>
                                     </select>
                                     <span><small>Free</small>delivery by Aug 15</span>
                                </div>
                            </div>
                             <div class=\"cart-edit\">
                                <div class=\"cart-quantity\">
                                  <a href=\"\"><i class=\"fa fa-minus-square-o\"></i></a> <span>1</span>  <a href=\"\"><i class=\"fa fa-plus-square-o\"></i></a>
                                </div>
                                <div class=\"cart-remove\">
                                   <span><a href=\"\">Move to whitelist</a></span>
                                    <strong><a href=\"\">Remove<i class=\"fa fa-trash\"></i></a></strong>
                                </div>
                            </div>
                        </div>
                        </div>
                        <div class=\"cart-protection\">
                            <div class=\"protection-img\">
                                <img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/protection-img.png\">
                            </div>
                            <div class=\"protection-cont\">
                                <h6>complete mobile protection</h6>
                                <strong>&#8377;1,06,000</strong>
                                    <small class=\"available-off-cart\"><strike>&#8377;2,06,000</strike> <span>(4% OFF)</span></small>
                                    <p>Brand authorised repair / replacement guarantee for 1 year.Special offer\"Get 6 month Google One trial with complete mobile protection\"<a href=\"\">Know More</a></p>
                                    <a href=\"\" class=\"btn btn-protect\">Add Protection</a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class=\"cart-sec\">
                        <div class=\"cart-img\">
                           <a href=\"\"><img src=\"http://poorvikabeta.webindia.com/image/cache/catalog/demo/imac_1-90x120.jpg\"></a>
                        </div>
                        <div class=\"cart-cont\">
                            <div class=\"cart-det\">
                                <div class=\"cart-info\">
                                    <h6>Dell Laptop</h6>
                                    <small>8Gb RAM</small>
                                    <strong>&#8377;1,06,000</strong>
                                    <small class=\"available-off-cart\"><strike>&#8377;2,06,000</strike>(4% OFF) <span>9 offer available<i class=\"fa fa-info\"></i></span></small>
                                    
                                </div>
                                <div class=\"cart-delivery\">
                                    <select name=\"type\" class=\"custom-select\">
                                      <option value=\"\">Regulary Delivery</option>
                                      <option value=\"\">Fast Delivery</option>
                                     </select>
                                     <span><small>Free</small>delivery by Aug 15</span>
                                </div>
                            </div>
                             <div class=\"cart-edit\">
                                <div class=\"cart-quantity\">
                                  <a href=\"\"><i class=\"fa fa-minus-square-o\"></i></a> <span>1</span>  <a href=\"\"><i class=\"fa fa-plus-square-o\"></i></a>
                                </div>
                                <div class=\"cart-remove\">
                                    <span><a href=\"\">Move to whitelist</a></span>
                                    <strong><a href=\"\">Remove<i class=\"fa fa-trash\"></i></a></strong>
                                </div>
                            </div>
                        </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class=\"cart-block whitelist-block\">
                <h3><i class=\"fa fa-heart\"></i>My whitelist</h3>
                <ul>
                    <li>
                        <div class=\"cart-sec\">
                        <div class=\"cart-img\">
                           <a href=\"\"><img src=\"http://poorvikabeta.webindia.com/image/cache/catalog/demo/imac_1-90x120.jpg\"></a>
                        </div>
                        <div class=\"cart-cont\">
                            <div class=\"cart-det whitelist-border\">
                                <div class=\"cart-info\">
                                    <h6>Dell Laptop</h6>
                                    \t<div class=\"rating\">
\t\t\t\t\t\t    <div class=\"rating-show whitelist-size\">
\t\t\t\t\t\t        <h6>4.5</h6>
\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t<div class=\"rating-box whitelist-rating\">
\t\t\t\t\t\t\t";
        // line 234
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 5));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 235
            echo "\t\t\t\t\t\t\t\t";
            if (((isset($context["rating"]) ? $context["rating"] : null) < $context["i"])) {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            } else {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            }
            // line 236
            echo "\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 237
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                                    <strong>&#8377;1,06,000</strong>
                                   
                                    
                                </div>
                                <div class=\"cart-delivery move-cart\">
                                   <a href=\"\" class=\"btn btn-move-cart\">Move to Cart</a>
                                   <span><a href=\"\">Remove<i class=\"fa fa-trash\"></i></a></span>
                                </div>
                            </div>
                             
                        </div>
                        </div>
                    </li>
                     <li>
                        <div class=\"cart-sec\">
                        <div class=\"cart-img\">
                           <a href=\"\"><img src=\"http://poorvikabeta.webindia.com/image/cache/catalog/demo/imac_1-90x120.jpg\"></a>
                        </div>
                        <div class=\"cart-cont\">
                            <div class=\"cart-det whitelist-border\">
                                <div class=\"cart-info\">
                                    <h6>Dell Laptop</h6>
                                    \t<div class=\"rating\">
\t\t\t\t\t\t    <div class=\"rating-show whitelist-size\">
\t\t\t\t\t\t        <h6>4.5</h6>
\t\t\t\t\t\t    </div>
\t\t\t\t\t\t\t<div class=\"rating-box whitelist-rating\">
\t\t\t\t\t\t\t";
        // line 266
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 5));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 267
            echo "\t\t\t\t\t\t\t\t";
            if (((isset($context["rating"]) ? $context["rating"] : null) < $context["i"])) {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            } else {
                echo "<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-1x\"></i><i class=\"fa fa-star-o fa-stack-1x\"></i></span>";
            }
            // line 268
            echo "\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 269
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                                    <strong>&#8377;1,06,000</strong>
                                   
                                    
                                </div>
                                <div class=\"cart-delivery move-cart\">
                                   <a href=\"\" class=\"btn btn-move-cart\">Move to Cart</a>
                                   <span><a href=\"\">Remove<i class=\"fa fa-trash\"></i></a></span>
                                </div>
                            </div>
                             
                        </div>
                        </div>
                    </li>
                   
                </ul>
            </div>
        </div>
        <div class=\"col-md-4\">
            <div class=\"cart-block summary\">
            <h3>Payment Summary</h3>
            <ul>
                <li>Price-2 items<span>&#8377;1,06,000</span></li>
                 <li>Delivery Fee<span class=\"green-color\">Free</span></li>
                  <li>Coupon Discount<span><a href=\"\">Apply Coupon</a></span></li>
                  <li class=\"total-amount\">Total Amount<span>&#8377;1,06,000</span></li>
                  <li class=\"save-offer\"><i class=\"fa fa-percent\"></i>You will save &#8377;6,000 on this Order</li>
            </ul>
            </div>
            <div class=\"gift-certify\">
                <h3><i class=\"fa fa-gift\"></i>Use Gift Certificate</h3>
                <input type=\"text\" id=\"fname\" name=\"fname\" placeholder=\"Enter Gift Code\">
                <a href=\"\" class=\"btn btn-protect btn-gift\">Apply Gift Certificate</a>
            </div>
            <div class=\"checkout-button\">
                <a href=\"\" class=\"btn btn-checkout\">Proceed To Checkout</a>
            </div>
        </div>
    </div>
</div>
</div>
<div class=\"address-page\">
    <div class=\"container\">
    <div class=\"row\">
        <div class=\"col-md-8\">
             <div class=\"cart-block address-block\">
                <h3><i class=\"fa fa-home\"></i>My Address<a href=\"\"><i class=\"fa fa-plus\"></i>Add New Address</a></h3>
                <ul>
                    <li>
                        <div class=\"address-det\">
                            <input type=\"radio\" id=\"address\" name=\"Address\">
                            <strong>Vivek Jose</strong>
                            <span>Home</span>
                            <small>COD Available</small>
                        </div>
                        <div class=\"address-cont\">
                            <p>3c, 3rd road cornival inforphase 2, kakkanna <br>Ernakulam,kerala-625454</p>
                            <span>Mobile:8565678456</span>
                        </div>
                        <div class=\"address-button\">
                            <a href=\"\" class=\"btn btn-add-remove\"><i class=\"fa fa-trash\"></i>Remove</a>
                            <a href=\"\" class=\"btn btn-add-edit\"><i class=\"fa fa-edit\"></i>edit</a>
                        </div>
                    </li>
                     <li>
                        <div class=\"address-det\">
                            <input type=\"radio\" id=\"address\" name=\"Address\">
                            <strong>Vivek Jose</strong>
                            <span>Office</span>
                            
                        </div>
                        <div class=\"address-cont\">
                            <p>3c, 3rd road cornival inforphase 2, kakkanna <br>Ernakulam,kerala-625454</p>
                            <span>Mobile:8565678456</span>
                        </div>
                        <div class=\"address-button\">
                            <a href=\"\" class=\"btn btn-add-remove\"><i class=\"fa fa-trash\"></i>Remove</a>
                            <a href=\"\" class=\"btn btn-add-edit\"><i class=\"fa fa-edit\"></i>edit</a>
                        </div>
                    </li>
                </ul>
                </div>
            
        </div>
        <div class=\"col-md-4\">
            <div class=\"cart-block summary\">
            <h3>Payment Summary</h3>
            <ul>
                <li>Price-2 items<span>&#8377;1,06,000</span></li>
                 <li>Delivery Fee<span class=\"green-color\">Free</span></li>
                  
                  <li class=\"total-amount\">Total Amount<span>&#8377;1,06,000</span></li>
                  <li class=\"save-offer\"><i class=\"fa fa-percent\"></i>You will save &#8377;6,000 on this Order</li>
            </ul>
            </div>
            <div class=\"gift-certify\">
                <h3><i class=\"fa fa-gift\"></i>Use Gift Certificate</h3>
                <input type=\"text\" id=\"fname\" name=\"fname\" placeholder=\"Enter Gift Code\">
                <a href=\"\" class=\"btn btn-protect btn-gift\">Apply Gift Certificate</a>
            </div>
            <div class=\"checkout-button\">
                <a href=\"\" class=\"btn btn-checkout\">Proceed To Checkout</a>
            </div>
        </div>
    </div>
    </div>
</div>
<div class=\"payment-page\">
    <div class=\"container\">
    <div class=\"row\">
        <div class=\"col-md-8\">
           <div class=\"cart-block payment-offers\">
                <h3><span><i class=\"fa fa-percent\"></i>payment offer</span></h3>
                <div class=\"offers-cont\">
                    <ul>
                        <li>Flat 0% discount on first prepaid transaction using Rupay debit card T&C</li>
                      
                    </ul>
                   <span>Show More<i class=\"fa fa-angle-down\"></i></span>
                   
                </div>
                <div class=\"pay-card\">
                    <h3><i class=\"fa fa-credit-card\"></i>Choose Payment Methods</h3>
                    <ul>
                        <li>
                            <div class=\"bank-det\">
                                <input type=\"radio\" id=\"address\" name=\"Address\">
                                <img src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/axis-logo.png\"><span>Axis Bank Debit card</span>
                                <strong><i class=\"fa fa-percent\"></i>&#8377;30 Instant discount Applicable.<a href=\"\">Details<i class=\"fa fa-angle-right\"></i></a></strong>
                            </div>
                            <span>5184 **** **** 4334</span>
                            <div class=\"bank-opt-cont\">
                                <strong>
                                <input type=\"number\" id=\"fname\" name=\"fname\" placeholder=\"CVV\"><i class=\"fa fa-question-circle\"></i>
                                </strong>
                                <a href=\"\" class=\"btn btn-checkout btn-opt-confirm\">Continue</a>
                                <p>This card is recommend for You <i class=\"fa fa-question-circle\"></i></p>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class=\"other-payment\">
                      <h3><i class=\"fa fa-credit-card\"></i>Other Payment Methods</h3>
                      <div class=\"payment-tabs\">
                          <div class=\"col-md-4 pr-0 pl-0\">
                      <ul>
                          <li> <button class=\"tablinks\" onclick=\"openCity(event, 'credit')\">Credit/Debit</button></li>
                          <li><button class=\"tablinks\" onclick=\"openCity(event, 'netbanking')\">Net Banking</button></li>
                          <li><button class=\"tablinks\" onclick=\"openCity(event, 'cashcard')\">Cash Card</button></li>
                          <li><button class=\"tablinks\" onclick=\"openCity(event, 'wallet')\">Wallet</button><span>Offers Available on PayTM Payzapp</span></li>
                          <li><button class=\"tablinks\" onclick=\"openCity(event, 'emi')\">EMI</button><span>Get EMI on HDFC Bank</span></li>
                      </ul>
                      </div>
                      <div class=\"col-md-8 pr-0 pl-0\">
                      <div class=\"payment-card tabcontent\" id=\"credit\">
                         <div class=\"input-block\">
                          <label>Number*</label>
                          <span><input type=\"number\" id=\"fname\" name=\"fname\"></span> 
                         </div>
                         <div class=\"input-block\">
                           <label>Name Of the Card*</label>
                          <span><input type=\"text\" id=\"fname\" name=\"fname\"></span> 
                         </div>
                         <div class=\"col-md-6 pr-0 pl-0\">
                          <div class=\"select-box\">
                           <label>Expiry Month*</label>
                         <select name=\"type\" class=\"custom-select\">
                                      <option value=\"\">Month</option>
                                      <option value=\"\">Jan</option>
                                      <option value=\"\">Feb</option>
                                     </select>
                         </div>
                         </div>
                         <div class=\"col-md-6 pr-0 pl-0\">
                         <div class=\"select-box\">
                           <label>Expiry Year*</label>
                         <select name=\"type\" class=\"custom-select\">
                                      <option value=\"\">Year</option>
                                      <option value=\"\">2023</option>
                                       <option value=\"\">2024</option>
                                     </select>
                                     </div>
                                     </div>
                                       <div class=\"col-md-3\">
                         <div class=\"input-block1\">
                                <label>CVV*</label>
                               <input type=\"number\" id=\"fname\" name=\"fname\" placeholder=\"CVV\">
                        </div>
                        </div>
                        <div class=\"col-md-9\">
                        <div class=\"input-para\">
                              <p>The last three digits printed on the back of the card</p>
                          </div>
                          </div>
                      </div>
                       <div class=\"payment-card tabcontent tab-hide\" id=\"netbanking\">
                         <div class=\"input-block\">
                          <label>Card Number*</label>
                          <span><input type=\"number\" id=\"fname\" name=\"fname\"></span> 
                         </div>
                         <div class=\"input-block\">
                           <label>Name Of the Card*</label>
                          <span><input type=\"text\" id=\"fname\" name=\"fname\"></span> 
                         </div>
                         <div class=\"col-md-6 pr-0 pl-0\">
                          <div class=\"select-box\">
                           <label>Expiry Month*</label>
                         <select name=\"type\" class=\"custom-select\">
                                      <option value=\"\">Month</option>
                                      <option value=\"\">Jan</option>
                                      <option value=\"\">Feb</option>
                                     </select>
                         </div>
                         </div>
                         <div class=\"col-md-6 pr-0 pl-0\">
                         <div class=\"select-box\">
                           <label>Expiry Year*</label>
                         <select name=\"type\" class=\"custom-select\">
                                      <option value=\"\">Year</option>
                                      <option value=\"\">2023</option>
                                       <option value=\"\">2024</option>
                                     </select>
                                     </div>
                                     </div>
                                       <div class=\"col-md-3\">
                         <div class=\"input-block1\">
                                <label>CVV*</label>
                               <input type=\"number\" id=\"fname\" name=\"fname\" placeholder=\"CVV\">
                        </div>
                        </div>
                        <div class=\"col-md-9\">
                        <div class=\"input-para\">
                              <p>The last three digits printed on the back of the card</p>
                          </div>
                          </div>
                      </div>
                      </div>
                      </div>
                </div>
            </div>
            
        </div>
        <div class=\"col-md-4\">
            <div class=\"cart-block summary payment\">
            <h3>Order Confirmation<a href=\"\">Edit Cart</a></h3>
            <ul class=\"none-boxshadow\">
               
                     <li>
                    <div class=\"payment-img\">
                        <img src=\"http://poorvikabeta.webindia.com/image/cache/catalog/demo/imac_1-90x120.jpg\">
                    </div>
                    <div class=\"payment-cont\">
                        <span>Dell Laptop<small>8GB RAM</small></span>
                        <strong>&#8377;1,06,000</strong>
                    </div>
                </li>
                  <li>
                    <div class=\"payment-img\">
                        <img src=\"http://poorvikabeta.webindia.com/image/cache/catalog/demo/imac_1-90x120.jpg\">
                    </div>
                    <div class=\"payment-cont\">
                        <span>Dell Laptop<small>8GB RAM</small></span>
                        <strong>&#8377;1,06,000</strong>
                    </div>
                </li>
                
            </ul>
            <ul>
               
               
                 <li>Delivery Fee<span class=\"green-color\">Free</span></li>
                  <li>Coupon Discount<span><a href=\"\">Apply Coupon</a></span></li>
                  <li class=\"total-amount\">Total Amount<span>&#8377;1,06,000</span></li>
                  <li class=\"save-offer\"><i class=\"fa fa-percent\"></i>You will save &#8377;6,000 on this Order</li>
            </ul>
            </div>
           
        </div>
    </div>
    </div>
</div>
<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName(\"tabcontent\");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = \"none\";
  }
  tablinks = document.getElementsByClassName(\"tablinks\");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(\" active\", \"\");
  }
  document.getElementById(cityName).style.display = \"block\";
  evt.currentTarget.className += \" active\";
}
</script>
";
        // line 566
        echo (isset($context["footer"]) ? $context["footer"] : null);
        echo " ";
    }

    public function getTemplateName()
    {
        return "default/template/checkout/cart.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  885 => 566,  586 => 269,  580 => 268,  573 => 267,  569 => 266,  538 => 237,  532 => 236,  525 => 235,  521 => 234,  407 => 123,  403 => 122,  396 => 120,  390 => 119,  384 => 115,  375 => 112,  371 => 111,  368 => 110,  364 => 109,  358 => 105,  354 => 104,  345 => 103,  341 => 102,  337 => 101,  332 => 100,  330 => 99,  323 => 94,  314 => 91,  310 => 90,  303 => 88,  295 => 83,  291 => 81,  286 => 80,  277 => 77,  273 => 76,  266 => 74,  262 => 73,  258 => 71,  251 => 69,  248 => 68,  241 => 67,  237 => 66,  231 => 63,  223 => 62,  218 => 61,  214 => 60,  209 => 59,  206 => 58,  197 => 57,  190 => 56,  187 => 55,  179 => 54,  165 => 53,  162 => 52,  158 => 51,  150 => 46,  146 => 45,  142 => 44,  138 => 43,  134 => 42,  130 => 41,  122 => 36,  119 => 35,  113 => 34,  111 => 33,  107 => 32,  100 => 31,  97 => 30,  94 => 29,  91 => 28,  88 => 27,  85 => 26,  82 => 25,  80 => 24,  75 => 23,  67 => 19,  64 => 18,  56 => 14,  53 => 13,  45 => 9,  43 => 8,  40 => 7,  29 => 5,  25 => 4,  19 => 1,);
    }
}
/* {{ header }}*/
/* <div id="checkout-cart" class="container">*/
/*   <ul class="breadcrumb">*/
/*     {% for breadcrumb in breadcrumbs %}*/
/*     <li><a href="{{ breadcrumb.href }}">{{ breadcrumb.text }}</a></li>*/
/*     {% endfor %}*/
/*   </ul>*/
/*   {% if attention %}*/
/*   <div class="alert alert-info"><i class="fa fa-info-circle"></i> {{ attention }}*/
/*     <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*   </div>*/
/*   {% endif %}*/
/*   {% if success %}*/
/*   <div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> {{ success }}*/
/*     <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*   </div>*/
/*   {% endif %}*/
/*   {% if error_warning %}*/
/*   <div class="alert alert-danger alert-dismissible"><i class="fa fa-exclamation-circle"></i> {{ error_warning }}*/
/*     <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*   </div>*/
/*   {% endif %}*/
/*   <div class="row">{{ column_left }}*/
/*     {% if column_left and column_right %}*/
/*     {% set class = 'col-sm-6' %}*/
/*     {% elseif column_left or column_right %}*/
/*     {% set class = 'col-sm-9' %}*/
/*     {% else %}*/
/*     {% set class = 'col-sm-12' %}*/
/*     {% endif %}*/
/*     <div id="content" class="{{ class }}">{{ content_top }}*/
/*       <h1>{{ heading_title }}*/
/*         {% if weight %}*/
/*         &nbsp;({{ weight }})*/
/*         {% endif %} </h1>*/
/*       <form action="{{ action }}" method="post" enctype="multipart/form-data">*/
/*         <div class="table-responsive">*/
/*           <table class="table table-bordered">*/
/*             <thead>*/
/*               <tr>*/
/*                 <td class="text-center">{{ column_image }}</td>*/
/*                 <td class="text-left">{{ column_name }}</td>*/
/*                 <td class="text-left">{{ column_model }}</td>*/
/*                 <td class="text-left">{{ column_quantity }}</td>*/
/*                 <td class="text-right">{{ column_price }}</td>*/
/*                 <td class="text-right">{{ column_total }}</td>*/
/*               </tr>*/
/*             </thead>*/
/*             <tbody>*/
/*             */
/*             {% for product in products %}*/
/*             <tr>*/
/*               <td class="text-center">{% if product.thumb %} <a href="{{ product.href }}"><img src="{{ product.thumb }}" alt="{{ product.name }}" title="{{ product.name }}" class="img-thumbnail" /></a> {% endif %}</td>*/
/*               <td class="text-left"><a href="{{ product.href }}">{{ product.name }}</a> {% if not product.stock %} <span class="text-danger">***</span> {% endif %}*/
/*                 {% if product.option %}*/
/*                 {% for option in product.option %} <br />*/
/*                 <small>{{ option.name }}: {{ option.value }}</small> {% endfor %}*/
/*                 {% endif %}*/
/*                 {% if product.reward %} <br />*/
/*                 <small>{{ product.reward }}</small> {% endif %}*/
/*                 {% if product.recurring %} <br />*/
/*                 <span class="label label-info">{{ text_recurring_item }}</span> <small>{{ product.recurring }}</small> {% endif %}</td>*/
/*               <td class="text-left">{{ product.model }}</td>*/
/*               <td class="text-left"><div class="input-group btn-block" style="max-width: 200px;">*/
/*                   */
/*                   {% if (quantity_status is defined and quantity_status) %} */
/*                     <input type="text" name="quantity[{{ product['cart_id'] }}]" value="{{ product['quantity'] }}" size="1" class="form-control" readonly/>*/
/*                   {% else %} */
/*                       <input type="text" name="quantity[{{ product['cart_id'] }}]" value="{{ product['quantity'] }}" size="1" class="form-control" />*/
/*                   {% endif %}*/
/*                 */
/*                   <span class="input-group-btn">*/
/*                   <button type="submit" data-toggle="tooltip" title="{{ button_update }}" class="btn btn-primary"><i class="fa fa-refresh"></i></button>*/
/*                   <button type="button" data-toggle="tooltip" title="{{ button_remove }}" class="btn btn-danger" onclick="cart.remove('{{ product.cart_id }}');"><i class="fa fa-times-circle"></i></button>*/
/*                   </span></div></td>*/
/*               <td class="text-right">{{ product.price }}</td>*/
/*               <td class="text-right">{{ product.total }}</td>*/
/*             </tr>*/
/*             {% endfor %}*/
/*             {% for voucher in vouchers %}*/
/*             <tr>*/
/*               <td></td>*/
/*               <td class="text-left">{{ voucher.description }}</td>*/
/*               <td class="text-left"></td>*/
/*               <td class="text-left"><div class="input-group btn-block" style="max-width: 200px;">*/
/*                   <input type="text" name="" value="1" size="1" disabled="disabled" class="form-control" />*/
/*                   <span class="input-group-btn">*/
/*                   <button type="button" data-toggle="tooltip" title="{{ button_remove }}" class="btn btn-danger" onclick="voucher.remove('{{ voucher.key }}');"><i class="fa fa-times-circle"></i></button>*/
/*                   </span></div></td>*/
/*               <td class="text-right">{{ voucher.amount }}</td>*/
/*               <td class="text-right">{{ voucher.amount }}</td>*/
/*             </tr>*/
/*             {% endfor %}*/
/*               </tbody>*/
/*             */
/*           </table>*/
/*         </div>*/
/*       </form>*/
/*       {% if modules %}*/
/*       <h2>{{ text_next }}</h2>*/
/*       <p>{{ text_next_choice }}</p>*/
/*       <div class="panel-group" id="accordion"> {% for module in modules %}*/
/*         {{ module }}*/
/*         {% endfor %} </div>*/
/*       {% endif %} <br />*/
/*       <div class="row">*/
/*         <div class="col-sm-4 col-sm-offset-8">*/
/*           <table class="table table-bordered">*/
/*             {% for total in totals %}*/
/*             <tr>*/
/*               <td class="text-right"><strong>{{ total.title }}:</strong></td>*/
/*               <td class="text-right">{{ total.text }}</td>*/
/*             </tr>*/
/*             {% endfor %}*/
/*           </table>*/
/*         </div>*/
/*       </div>*/
/*       <div class="buttons clearfix">*/
/*         <div class="pull-left"><a href="{{ continue }}" class="btn btn-default">{{ button_shopping }}</a></div>*/
/*         <div class="pull-right"><a href="{{ checkout }}" class="btn btn-primary">{{ button_checkout }}</a></div>*/
/*       </div>*/
/*       {{ content_bottom }}</div>*/
/*     {{ column_right }}</div>*/
/*    */
/*    */
/* </div>*/
/* <div class="cart-page">*/
/* <div class="container">*/
/*  <div class="row">*/
/*          */
/*         <div class="col-md-8">*/
/*             <div class="cart-block">*/
/*                 <h3><i class="fa fa-cart-arrow-down"></i>My cart</h3>*/
/*                 <ul>*/
/*                     <li>*/
/*                         <div class="cart-sec">*/
/*                         <div class="cart-img">*/
/*                            <a href=""><img src="http://poorvikabeta.webindia.com/image/cache/catalog/demo/imac_1-90x120.jpg"></a>*/
/*                         </div>*/
/*                         <div class="cart-cont">*/
/*                             <div class="cart-det">*/
/*                                 <div class="cart-info">*/
/*                                     <h6>Dell Laptop</h6>*/
/*                                     <small>8Gb RAM</small>*/
/*                                     <strong>&#8377;1,06,000</strong>*/
/*                                     <small class="available-off-cart"><strike>&#8377;2,06,000</strike>(4% OFF) <span>9 offer available<i class="fa fa-info"></i></span></small>*/
/*                                     */
/*                                 </div>*/
/*                                 <div class="cart-delivery">*/
/*                                     <select name="type" class="custom-select">*/
/*                                       <option value="">Regulary Delivery</option>*/
/*                                       <option value="">Fast Delivery</option>*/
/*                                      </select>*/
/*                                      <span><small>Free</small>delivery by Aug 15</span>*/
/*                                 </div>*/
/*                             </div>*/
/*                              <div class="cart-edit">*/
/*                                 <div class="cart-quantity">*/
/*                                   <a href=""><i class="fa fa-minus-square-o"></i></a> <span>1</span>  <a href=""><i class="fa fa-plus-square-o"></i></a>*/
/*                                 </div>*/
/*                                 <div class="cart-remove">*/
/*                                    <span><a href="">Move to whitelist</a></span>*/
/*                                     <strong><a href="">Remove<i class="fa fa-trash"></i></a></strong>*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                         </div>*/
/*                         <div class="cart-protection">*/
/*                             <div class="protection-img">*/
/*                                 <img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/protection-img.png">*/
/*                             </div>*/
/*                             <div class="protection-cont">*/
/*                                 <h6>complete mobile protection</h6>*/
/*                                 <strong>&#8377;1,06,000</strong>*/
/*                                     <small class="available-off-cart"><strike>&#8377;2,06,000</strike> <span>(4% OFF)</span></small>*/
/*                                     <p>Brand authorised repair / replacement guarantee for 1 year.Special offer"Get 6 month Google One trial with complete mobile protection"<a href="">Know More</a></p>*/
/*                                     <a href="" class="btn btn-protect">Add Protection</a>*/
/*                             </div>*/
/*                         </div>*/
/*                     </li>*/
/*                     <li>*/
/*                         <div class="cart-sec">*/
/*                         <div class="cart-img">*/
/*                            <a href=""><img src="http://poorvikabeta.webindia.com/image/cache/catalog/demo/imac_1-90x120.jpg"></a>*/
/*                         </div>*/
/*                         <div class="cart-cont">*/
/*                             <div class="cart-det">*/
/*                                 <div class="cart-info">*/
/*                                     <h6>Dell Laptop</h6>*/
/*                                     <small>8Gb RAM</small>*/
/*                                     <strong>&#8377;1,06,000</strong>*/
/*                                     <small class="available-off-cart"><strike>&#8377;2,06,000</strike>(4% OFF) <span>9 offer available<i class="fa fa-info"></i></span></small>*/
/*                                     */
/*                                 </div>*/
/*                                 <div class="cart-delivery">*/
/*                                     <select name="type" class="custom-select">*/
/*                                       <option value="">Regulary Delivery</option>*/
/*                                       <option value="">Fast Delivery</option>*/
/*                                      </select>*/
/*                                      <span><small>Free</small>delivery by Aug 15</span>*/
/*                                 </div>*/
/*                             </div>*/
/*                              <div class="cart-edit">*/
/*                                 <div class="cart-quantity">*/
/*                                   <a href=""><i class="fa fa-minus-square-o"></i></a> <span>1</span>  <a href=""><i class="fa fa-plus-square-o"></i></a>*/
/*                                 </div>*/
/*                                 <div class="cart-remove">*/
/*                                     <span><a href="">Move to whitelist</a></span>*/
/*                                     <strong><a href="">Remove<i class="fa fa-trash"></i></a></strong>*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/*                         </div>*/
/*                     </li>*/
/*                 </ul>*/
/*             </div>*/
/*             <div class="cart-block whitelist-block">*/
/*                 <h3><i class="fa fa-heart"></i>My whitelist</h3>*/
/*                 <ul>*/
/*                     <li>*/
/*                         <div class="cart-sec">*/
/*                         <div class="cart-img">*/
/*                            <a href=""><img src="http://poorvikabeta.webindia.com/image/cache/catalog/demo/imac_1-90x120.jpg"></a>*/
/*                         </div>*/
/*                         <div class="cart-cont">*/
/*                             <div class="cart-det whitelist-border">*/
/*                                 <div class="cart-info">*/
/*                                     <h6>Dell Laptop</h6>*/
/*                                     	<div class="rating">*/
/* 						    <div class="rating-show whitelist-size">*/
/* 						        <h6>4.5</h6>*/
/* 						    </div>*/
/* 							<div class="rating-box whitelist-rating">*/
/* 							{% for i in 1..5 %}*/
/* 								{% if rating < i %}<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>{% else %}<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>{% endif %}*/
/* 							{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                                     <strong>&#8377;1,06,000</strong>*/
/*                                    */
/*                                     */
/*                                 </div>*/
/*                                 <div class="cart-delivery move-cart">*/
/*                                    <a href="" class="btn btn-move-cart">Move to Cart</a>*/
/*                                    <span><a href="">Remove<i class="fa fa-trash"></i></a></span>*/
/*                                 </div>*/
/*                             </div>*/
/*                              */
/*                         </div>*/
/*                         </div>*/
/*                     </li>*/
/*                      <li>*/
/*                         <div class="cart-sec">*/
/*                         <div class="cart-img">*/
/*                            <a href=""><img src="http://poorvikabeta.webindia.com/image/cache/catalog/demo/imac_1-90x120.jpg"></a>*/
/*                         </div>*/
/*                         <div class="cart-cont">*/
/*                             <div class="cart-det whitelist-border">*/
/*                                 <div class="cart-info">*/
/*                                     <h6>Dell Laptop</h6>*/
/*                                     	<div class="rating">*/
/* 						    <div class="rating-show whitelist-size">*/
/* 						        <h6>4.5</h6>*/
/* 						    </div>*/
/* 							<div class="rating-box whitelist-rating">*/
/* 							{% for i in 1..5 %}*/
/* 								{% if rating < i %}<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>{% else %}<span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>{% endif %}*/
/* 							{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                                     <strong>&#8377;1,06,000</strong>*/
/*                                    */
/*                                     */
/*                                 </div>*/
/*                                 <div class="cart-delivery move-cart">*/
/*                                    <a href="" class="btn btn-move-cart">Move to Cart</a>*/
/*                                    <span><a href="">Remove<i class="fa fa-trash"></i></a></span>*/
/*                                 </div>*/
/*                             </div>*/
/*                              */
/*                         </div>*/
/*                         </div>*/
/*                     </li>*/
/*                    */
/*                 </ul>*/
/*             </div>*/
/*         </div>*/
/*         <div class="col-md-4">*/
/*             <div class="cart-block summary">*/
/*             <h3>Payment Summary</h3>*/
/*             <ul>*/
/*                 <li>Price-2 items<span>&#8377;1,06,000</span></li>*/
/*                  <li>Delivery Fee<span class="green-color">Free</span></li>*/
/*                   <li>Coupon Discount<span><a href="">Apply Coupon</a></span></li>*/
/*                   <li class="total-amount">Total Amount<span>&#8377;1,06,000</span></li>*/
/*                   <li class="save-offer"><i class="fa fa-percent"></i>You will save &#8377;6,000 on this Order</li>*/
/*             </ul>*/
/*             </div>*/
/*             <div class="gift-certify">*/
/*                 <h3><i class="fa fa-gift"></i>Use Gift Certificate</h3>*/
/*                 <input type="text" id="fname" name="fname" placeholder="Enter Gift Code">*/
/*                 <a href="" class="btn btn-protect btn-gift">Apply Gift Certificate</a>*/
/*             </div>*/
/*             <div class="checkout-button">*/
/*                 <a href="" class="btn btn-checkout">Proceed To Checkout</a>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* </div>*/
/* <div class="address-page">*/
/*     <div class="container">*/
/*     <div class="row">*/
/*         <div class="col-md-8">*/
/*              <div class="cart-block address-block">*/
/*                 <h3><i class="fa fa-home"></i>My Address<a href=""><i class="fa fa-plus"></i>Add New Address</a></h3>*/
/*                 <ul>*/
/*                     <li>*/
/*                         <div class="address-det">*/
/*                             <input type="radio" id="address" name="Address">*/
/*                             <strong>Vivek Jose</strong>*/
/*                             <span>Home</span>*/
/*                             <small>COD Available</small>*/
/*                         </div>*/
/*                         <div class="address-cont">*/
/*                             <p>3c, 3rd road cornival inforphase 2, kakkanna <br>Ernakulam,kerala-625454</p>*/
/*                             <span>Mobile:8565678456</span>*/
/*                         </div>*/
/*                         <div class="address-button">*/
/*                             <a href="" class="btn btn-add-remove"><i class="fa fa-trash"></i>Remove</a>*/
/*                             <a href="" class="btn btn-add-edit"><i class="fa fa-edit"></i>edit</a>*/
/*                         </div>*/
/*                     </li>*/
/*                      <li>*/
/*                         <div class="address-det">*/
/*                             <input type="radio" id="address" name="Address">*/
/*                             <strong>Vivek Jose</strong>*/
/*                             <span>Office</span>*/
/*                             */
/*                         </div>*/
/*                         <div class="address-cont">*/
/*                             <p>3c, 3rd road cornival inforphase 2, kakkanna <br>Ernakulam,kerala-625454</p>*/
/*                             <span>Mobile:8565678456</span>*/
/*                         </div>*/
/*                         <div class="address-button">*/
/*                             <a href="" class="btn btn-add-remove"><i class="fa fa-trash"></i>Remove</a>*/
/*                             <a href="" class="btn btn-add-edit"><i class="fa fa-edit"></i>edit</a>*/
/*                         </div>*/
/*                     </li>*/
/*                 </ul>*/
/*                 </div>*/
/*             */
/*         </div>*/
/*         <div class="col-md-4">*/
/*             <div class="cart-block summary">*/
/*             <h3>Payment Summary</h3>*/
/*             <ul>*/
/*                 <li>Price-2 items<span>&#8377;1,06,000</span></li>*/
/*                  <li>Delivery Fee<span class="green-color">Free</span></li>*/
/*                   */
/*                   <li class="total-amount">Total Amount<span>&#8377;1,06,000</span></li>*/
/*                   <li class="save-offer"><i class="fa fa-percent"></i>You will save &#8377;6,000 on this Order</li>*/
/*             </ul>*/
/*             </div>*/
/*             <div class="gift-certify">*/
/*                 <h3><i class="fa fa-gift"></i>Use Gift Certificate</h3>*/
/*                 <input type="text" id="fname" name="fname" placeholder="Enter Gift Code">*/
/*                 <a href="" class="btn btn-protect btn-gift">Apply Gift Certificate</a>*/
/*             </div>*/
/*             <div class="checkout-button">*/
/*                 <a href="" class="btn btn-checkout">Proceed To Checkout</a>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/*     </div>*/
/* </div>*/
/* <div class="payment-page">*/
/*     <div class="container">*/
/*     <div class="row">*/
/*         <div class="col-md-8">*/
/*            <div class="cart-block payment-offers">*/
/*                 <h3><span><i class="fa fa-percent"></i>payment offer</span></h3>*/
/*                 <div class="offers-cont">*/
/*                     <ul>*/
/*                         <li>Flat 0% discount on first prepaid transaction using Rupay debit card T&C</li>*/
/*                       */
/*                     </ul>*/
/*                    <span>Show More<i class="fa fa-angle-down"></i></span>*/
/*                    */
/*                 </div>*/
/*                 <div class="pay-card">*/
/*                     <h3><i class="fa fa-credit-card"></i>Choose Payment Methods</h3>*/
/*                     <ul>*/
/*                         <li>*/
/*                             <div class="bank-det">*/
/*                                 <input type="radio" id="address" name="Address">*/
/*                                 <img src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/images/axis-logo.png"><span>Axis Bank Debit card</span>*/
/*                                 <strong><i class="fa fa-percent"></i>&#8377;30 Instant discount Applicable.<a href="">Details<i class="fa fa-angle-right"></i></a></strong>*/
/*                             </div>*/
/*                             <span>5184 **** **** 4334</span>*/
/*                             <div class="bank-opt-cont">*/
/*                                 <strong>*/
/*                                 <input type="number" id="fname" name="fname" placeholder="CVV"><i class="fa fa-question-circle"></i>*/
/*                                 </strong>*/
/*                                 <a href="" class="btn btn-checkout btn-opt-confirm">Continue</a>*/
/*                                 <p>This card is recommend for You <i class="fa fa-question-circle"></i></p>*/
/*                             </div>*/
/*                         </li>*/
/*                     </ul>*/
/*                 </div>*/
/*                 <div class="other-payment">*/
/*                       <h3><i class="fa fa-credit-card"></i>Other Payment Methods</h3>*/
/*                       <div class="payment-tabs">*/
/*                           <div class="col-md-4 pr-0 pl-0">*/
/*                       <ul>*/
/*                           <li> <button class="tablinks" onclick="openCity(event, 'credit')">Credit/Debit</button></li>*/
/*                           <li><button class="tablinks" onclick="openCity(event, 'netbanking')">Net Banking</button></li>*/
/*                           <li><button class="tablinks" onclick="openCity(event, 'cashcard')">Cash Card</button></li>*/
/*                           <li><button class="tablinks" onclick="openCity(event, 'wallet')">Wallet</button><span>Offers Available on PayTM Payzapp</span></li>*/
/*                           <li><button class="tablinks" onclick="openCity(event, 'emi')">EMI</button><span>Get EMI on HDFC Bank</span></li>*/
/*                       </ul>*/
/*                       </div>*/
/*                       <div class="col-md-8 pr-0 pl-0">*/
/*                       <div class="payment-card tabcontent" id="credit">*/
/*                          <div class="input-block">*/
/*                           <label>Number*</label>*/
/*                           <span><input type="number" id="fname" name="fname"></span> */
/*                          </div>*/
/*                          <div class="input-block">*/
/*                            <label>Name Of the Card*</label>*/
/*                           <span><input type="text" id="fname" name="fname"></span> */
/*                          </div>*/
/*                          <div class="col-md-6 pr-0 pl-0">*/
/*                           <div class="select-box">*/
/*                            <label>Expiry Month*</label>*/
/*                          <select name="type" class="custom-select">*/
/*                                       <option value="">Month</option>*/
/*                                       <option value="">Jan</option>*/
/*                                       <option value="">Feb</option>*/
/*                                      </select>*/
/*                          </div>*/
/*                          </div>*/
/*                          <div class="col-md-6 pr-0 pl-0">*/
/*                          <div class="select-box">*/
/*                            <label>Expiry Year*</label>*/
/*                          <select name="type" class="custom-select">*/
/*                                       <option value="">Year</option>*/
/*                                       <option value="">2023</option>*/
/*                                        <option value="">2024</option>*/
/*                                      </select>*/
/*                                      </div>*/
/*                                      </div>*/
/*                                        <div class="col-md-3">*/
/*                          <div class="input-block1">*/
/*                                 <label>CVV*</label>*/
/*                                <input type="number" id="fname" name="fname" placeholder="CVV">*/
/*                         </div>*/
/*                         </div>*/
/*                         <div class="col-md-9">*/
/*                         <div class="input-para">*/
/*                               <p>The last three digits printed on the back of the card</p>*/
/*                           </div>*/
/*                           </div>*/
/*                       </div>*/
/*                        <div class="payment-card tabcontent tab-hide" id="netbanking">*/
/*                          <div class="input-block">*/
/*                           <label>Card Number*</label>*/
/*                           <span><input type="number" id="fname" name="fname"></span> */
/*                          </div>*/
/*                          <div class="input-block">*/
/*                            <label>Name Of the Card*</label>*/
/*                           <span><input type="text" id="fname" name="fname"></span> */
/*                          </div>*/
/*                          <div class="col-md-6 pr-0 pl-0">*/
/*                           <div class="select-box">*/
/*                            <label>Expiry Month*</label>*/
/*                          <select name="type" class="custom-select">*/
/*                                       <option value="">Month</option>*/
/*                                       <option value="">Jan</option>*/
/*                                       <option value="">Feb</option>*/
/*                                      </select>*/
/*                          </div>*/
/*                          </div>*/
/*                          <div class="col-md-6 pr-0 pl-0">*/
/*                          <div class="select-box">*/
/*                            <label>Expiry Year*</label>*/
/*                          <select name="type" class="custom-select">*/
/*                                       <option value="">Year</option>*/
/*                                       <option value="">2023</option>*/
/*                                        <option value="">2024</option>*/
/*                                      </select>*/
/*                                      </div>*/
/*                                      </div>*/
/*                                        <div class="col-md-3">*/
/*                          <div class="input-block1">*/
/*                                 <label>CVV*</label>*/
/*                                <input type="number" id="fname" name="fname" placeholder="CVV">*/
/*                         </div>*/
/*                         </div>*/
/*                         <div class="col-md-9">*/
/*                         <div class="input-para">*/
/*                               <p>The last three digits printed on the back of the card</p>*/
/*                           </div>*/
/*                           </div>*/
/*                       </div>*/
/*                       </div>*/
/*                       </div>*/
/*                 </div>*/
/*             </div>*/
/*             */
/*         </div>*/
/*         <div class="col-md-4">*/
/*             <div class="cart-block summary payment">*/
/*             <h3>Order Confirmation<a href="">Edit Cart</a></h3>*/
/*             <ul class="none-boxshadow">*/
/*                */
/*                      <li>*/
/*                     <div class="payment-img">*/
/*                         <img src="http://poorvikabeta.webindia.com/image/cache/catalog/demo/imac_1-90x120.jpg">*/
/*                     </div>*/
/*                     <div class="payment-cont">*/
/*                         <span>Dell Laptop<small>8GB RAM</small></span>*/
/*                         <strong>&#8377;1,06,000</strong>*/
/*                     </div>*/
/*                 </li>*/
/*                   <li>*/
/*                     <div class="payment-img">*/
/*                         <img src="http://poorvikabeta.webindia.com/image/cache/catalog/demo/imac_1-90x120.jpg">*/
/*                     </div>*/
/*                     <div class="payment-cont">*/
/*                         <span>Dell Laptop<small>8GB RAM</small></span>*/
/*                         <strong>&#8377;1,06,000</strong>*/
/*                     </div>*/
/*                 </li>*/
/*                 */
/*             </ul>*/
/*             <ul>*/
/*                */
/*                */
/*                  <li>Delivery Fee<span class="green-color">Free</span></li>*/
/*                   <li>Coupon Discount<span><a href="">Apply Coupon</a></span></li>*/
/*                   <li class="total-amount">Total Amount<span>&#8377;1,06,000</span></li>*/
/*                   <li class="save-offer"><i class="fa fa-percent"></i>You will save &#8377;6,000 on this Order</li>*/
/*             </ul>*/
/*             </div>*/
/*            */
/*         </div>*/
/*     </div>*/
/*     </div>*/
/* </div>*/
/* <script>*/
/* function openCity(evt, cityName) {*/
/*   var i, tabcontent, tablinks;*/
/*   tabcontent = document.getElementsByClassName("tabcontent");*/
/*   for (i = 0; i < tabcontent.length; i++) {*/
/*     tabcontent[i].style.display = "none";*/
/*   }*/
/*   tablinks = document.getElementsByClassName("tablinks");*/
/*   for (i = 0; i < tablinks.length; i++) {*/
/*     tablinks[i].className = tablinks[i].className.replace(" active", "");*/
/*   }*/
/*   document.getElementById(cityName).style.display = "block";*/
/*   evt.currentTarget.className += " active";*/
/* }*/
/* </script>*/
/* {{ footer }} */
