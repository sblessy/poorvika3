<?php

/* extension/module/so_call_for_price.twig */
class __TwigTemplate_b2052ff47e13c67ecfb4b4008b8ec65f1a9f05e2668e25b1a3ba43ef6dcb27fc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "
<div id=\"content\">
\t<div class=\"page-header\">
    \t<div class=\"container-fluid\">
      \t\t<div class=\"pull-right\">
        \t\t<button type=\"submit\" form=\"form-so-call-for-price\" data-toggle=\"tooltip\" title=\"";
        // line 6
        echo (isset($context["button_save"]) ? $context["button_save"] : null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i> ";
        echo (isset($context["button_save"]) ? $context["button_save"] : null);
        echo "</button>
        \t\t<a class=\"btn btn-success1\" onclick=\"\$('#action').val('save_edit');\$('#form-so-call-for-price').submit();\" data-toggle=\"tooltip\" title=\"";
        // line 7
        echo (isset($context["button_savestay"]) ? $context["button_savestay"] : null);
        echo "\" ><i class=\"fa fa-pencil-square-o\"></i> ";
        echo (isset($context["button_savestay"]) ? $context["button_savestay"] : null);
        echo "</a>
\t\t\t\t<a href=\"";
        // line 8
        echo (isset($context["cancel"]) ? $context["cancel"] : null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo (isset($context["button_cancel"]) ? $context["button_cancel"] : null);
        echo "\" class=\"btn btn-danger\"><i class=\"fa fa-reply\"></i> ";
        echo (isset($context["button_cancel"]) ? $context["button_cancel"] : null);
        echo "</a>
        \t</div>
\t\t\t<h1>";
        // line 10
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
      \t\t<ul class=\"breadcrumb\">
        \t\t";
        // line 12
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 13
            echo "        \t\t\t<li><a href=\"";
            echo $this->getAttribute($context["breadcrumb"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array());
            echo "</a></li>
        \t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 15
        echo "      \t\t</ul>
    \t</div>
  \t</div>
  \t<div class=\"container-fluid\">
  \t\t";
        // line 19
        if ((isset($context["error_warning"]) ? $context["error_warning"] : null)) {
            // line 20
            echo "    \t\t<div class=\"alert alert-danger\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo (isset($context["error_warning"]) ? $context["error_warning"] : null);
            echo "
      \t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    \t\t</div>
    \t";
        }
        // line 24
        echo "    \t";
        if ((isset($context["success"]) ? $context["success"] : null)) {
            // line 25
            echo "    \t\t<div class=\"alert alert-success\"><i class=\"fa fa-check-circle\"></i> ";
            echo (isset($context["success"]) ? $context["success"] : null);
            echo "
      \t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    \t\t</div>
    \t";
        }
        // line 29
        echo "    \t<div class=\"panel panel-default\">
      \t\t<div class=\"panel-heading\">
        \t\t<h3 class=\"panel-title\"><i class=\"fa fa-pencil\"></i> ";
        // line 31
        echo (isset($context["text_edit"]) ? $context["text_edit"] : null);
        echo "</h3>
      \t\t</div>
      \t\t<div class=\"panel-body\">
      \t\t\t<form action=\"";
        // line 34
        echo (isset($context["action"]) ? $context["action"] : null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-so-call-for-price\" class=\"form-horizontal\">
      \t\t\t\t<input type=\"hidden\" name=\"action\" id=\"action\" value=\"\"/>
                    <div class=\"form-group\">
                        <label class=\"col-sm-4 control-label\" for=\"input-status\">";
        // line 37
        echo (isset($context["entry_status"]) ? $context["entry_status"] : null);
        echo "</label>
                        <div class=\"col-sm-8\">
                            <select name=\"module_so_call_for_price_status\" id=\"input-status\" class=\"form-control\">
                                ";
        // line 40
        if ((isset($context["status"]) ? $context["status"] : null)) {
            // line 41
            echo "                                    <option value=\"1\" selected=\"selected\">";
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "</option>
                                    <option value=\"0\">";
            // line 42
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "</option>
                                ";
        } else {
            // line 44
            echo "                                    <option value=\"1\">";
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "</option>
                                    <option value=\"0\" selected=\"selected\">";
            // line 45
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "</option>
                                ";
        }
        // line 47
        echo "                            </select>
                        </div>
                    </div>
                    <div class=\"form-group\">
                        <label class=\"col-sm-4 control-label\" for=\"input-hide_cart\">";
        // line 51
        echo (isset($context["entry_hide_cart"]) ? $context["entry_hide_cart"] : null);
        echo "</label>
                        <div class=\"col-sm-8\">
                            <select name=\"module_so_call_for_price_hide_cart\" id=\"input-hide_cart\" class=\"form-control\">
                                ";
        // line 54
        if ((isset($context["hide_cart"]) ? $context["hide_cart"] : null)) {
            // line 55
            echo "                                    <option value=\"1\" selected=\"selected\">";
            echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
            echo "</option>
                                    <option value=\"0\">";
            // line 56
            echo (isset($context["text_no"]) ? $context["text_no"] : null);
            echo "</option>
                                ";
        } else {
            // line 58
            echo "                                    <option value=\"1\">";
            echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
            echo "</option>
                                    <option value=\"0\" selected=\"selected\">";
            // line 59
            echo (isset($context["text_no"]) ? $context["text_no"] : null);
            echo "</option>
                                ";
        }
        // line 61
        echo "                            </select>
                            <div class=\"help-block\">";
        // line 62
        echo (isset($context["entry_hide_cart_desc"]) ? $context["entry_hide_cart_desc"] : null);
        echo "</div>
                        </div>
                    </div>
                    <div class=\"form-group\">
                        <label class=\"col-sm-4 control-label\" for=\"input-replace_cart\">";
        // line 66
        echo (isset($context["entry_replace_cart"]) ? $context["entry_replace_cart"] : null);
        echo "</label>
                        <div class=\"col-sm-8\">
                            <select name=\"module_so_call_for_price_replace_cart\" id=\"input-replace_cart\" class=\"form-control\">
                                ";
        // line 69
        if ((isset($context["replace_cart"]) ? $context["replace_cart"] : null)) {
            // line 70
            echo "                                    <option value=\"1\" selected=\"selected\">";
            echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
            echo "</option>
                                    <option value=\"0\">";
            // line 71
            echo (isset($context["text_no"]) ? $context["text_no"] : null);
            echo "</option>
                                ";
        } else {
            // line 73
            echo "                                    <option value=\"1\">";
            echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
            echo "</option>
                                    <option value=\"0\" selected=\"selected\">";
            // line 74
            echo (isset($context["text_no"]) ? $context["text_no"] : null);
            echo "</option>
                                ";
        }
        // line 76
        echo "                            </select>
                            <div class=\"help-block\">";
        // line 77
        echo (isset($context["entry_replace_cart_desc"]) ? $context["entry_replace_cart_desc"] : null);
        echo "</div>
                        </div>
                    </div>
                    <div class=\"form-group\">
                        <label class=\"col-sm-4 control-label\" for=\"input-sendmailto\">";
        // line 81
        echo (isset($context["entry_send_mail_to"]) ? $context["entry_send_mail_to"] : null);
        echo "</label>
                        <div class=\"col-sm-8\">
                            <input type=\"text\" name=\"module_so_call_for_price_send_mail_to\" value=\"";
        // line 83
        echo (isset($context["send_mail_to"]) ? $context["send_mail_to"] : null);
        echo "\" id=\"input-sendmailto\" class=\"form-control\" />
                            <div class=\"help-block\">";
        // line 84
        echo (isset($context["entry_send_mail_to_desc"]) ? $context["entry_send_mail_to_desc"] : null);
        echo "</div>
                        </div>
                    </div>
                    <div class=\"form-group\">
                        <label class=\"col-sm-4 control-label\" for=\"input-send_mail_customer\">";
        // line 88
        echo (isset($context["entry_send_mail_customer"]) ? $context["entry_send_mail_customer"] : null);
        echo "</label>
                        <div class=\"col-sm-8\">
                            <select name=\"module_so_call_for_price_send_mail_customer\" id=\"input-send_mail_customer\" class=\"form-control\">
                                ";
        // line 91
        if ((isset($context["send_mail_customer"]) ? $context["send_mail_customer"] : null)) {
            // line 92
            echo "                                    <option value=\"1\" selected=\"selected\">";
            echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
            echo "</option>
                                    <option value=\"0\">";
            // line 93
            echo (isset($context["text_no"]) ? $context["text_no"] : null);
            echo "</option>
                                ";
        } else {
            // line 95
            echo "                                    <option value=\"1\">";
            echo (isset($context["text_yes"]) ? $context["text_yes"] : null);
            echo "</option>
                                    <option value=\"0\" selected=\"selected\">";
            // line 96
            echo (isset($context["text_no"]) ? $context["text_no"] : null);
            echo "</option>
                                ";
        }
        // line 98
        echo "                            </select>
                            <div class=\"help-block\">";
        // line 99
        echo (isset($context["entry_send_mail_customer_desc"]) ? $context["entry_send_mail_customer_desc"] : null);
        echo "</div>
                        </div>
                    </div>
      \t\t\t</form>
      \t\t</div>
      \t</div>
  \t</div>
</div>
<style>
.btn-success {
\tbackground-color: #fff;
\tcolor: #000;
\tborder-color: #ddd;
}
.btn-success1 {
    background-color: #8fbb6c;
    border-color: #7aae50;
    color: #fff;
}
.btn-success1:hover {
\tbackground-color: #648e42;
    border-color: #3d5728;
    color: #fff;
}
</style>
<script type=\"text/javascript\">
    /*language*/
    \$('#language').change(function(){
        var that = \$(this), opt_select = \$('option:selected', that).val() , _input = \$('#input-head-name-'+opt_select);
        \$('[id^=\"input-head-name-\"]').addClass('hide');
        _input.removeClass('hide');
    });

    \$('.first-name').change(function(){
        \$('input[name=\"head-name\"]').val(\$(this).val());
    });
</script>
";
        // line 136
        echo (isset($context["footer"]) ? $context["footer"] : null);
    }

    public function getTemplateName()
    {
        return "extension/module/so_call_for_price.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  309 => 136,  269 => 99,  266 => 98,  261 => 96,  256 => 95,  251 => 93,  246 => 92,  244 => 91,  238 => 88,  231 => 84,  227 => 83,  222 => 81,  215 => 77,  212 => 76,  207 => 74,  202 => 73,  197 => 71,  192 => 70,  190 => 69,  184 => 66,  177 => 62,  174 => 61,  169 => 59,  164 => 58,  159 => 56,  154 => 55,  152 => 54,  146 => 51,  140 => 47,  135 => 45,  130 => 44,  125 => 42,  120 => 41,  118 => 40,  112 => 37,  106 => 34,  100 => 31,  96 => 29,  88 => 25,  85 => 24,  77 => 20,  75 => 19,  69 => 15,  58 => 13,  54 => 12,  49 => 10,  40 => 8,  34 => 7,  28 => 6,  19 => 1,);
    }
}
/* {{ header }}{{ column_left }}*/
/* <div id="content">*/
/* 	<div class="page-header">*/
/*     	<div class="container-fluid">*/
/*       		<div class="pull-right">*/
/*         		<button type="submit" form="form-so-call-for-price" data-toggle="tooltip" title="{{ button_save }}" class="btn btn-primary"><i class="fa fa-save"></i> {{ button_save }}</button>*/
/*         		<a class="btn btn-success1" onclick="$('#action').val('save_edit');$('#form-so-call-for-price').submit();" data-toggle="tooltip" title="{{ button_savestay }}" ><i class="fa fa-pencil-square-o"></i> {{ button_savestay }}</a>*/
/* 				<a href="{{ cancel }}" data-toggle="tooltip" title="{{ button_cancel }}" class="btn btn-danger"><i class="fa fa-reply"></i> {{ button_cancel }}</a>*/
/*         	</div>*/
/* 			<h1>{{ heading_title }}</h1>*/
/*       		<ul class="breadcrumb">*/
/*         		{% for breadcrumb in breadcrumbs %}*/
/*         			<li><a href="{{ breadcrumb.href }}">{{ breadcrumb.text }}</a></li>*/
/*         		{% endfor %}*/
/*       		</ul>*/
/*     	</div>*/
/*   	</div>*/
/*   	<div class="container-fluid">*/
/*   		{% if error_warning %}*/
/*     		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> {{ error_warning }}*/
/*       			<button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*     		</div>*/
/*     	{% endif %}*/
/*     	{% if success %}*/
/*     		<div class="alert alert-success"><i class="fa fa-check-circle"></i> {{ success }}*/
/*       			<button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*     		</div>*/
/*     	{% endif %}*/
/*     	<div class="panel panel-default">*/
/*       		<div class="panel-heading">*/
/*         		<h3 class="panel-title"><i class="fa fa-pencil"></i> {{ text_edit }}</h3>*/
/*       		</div>*/
/*       		<div class="panel-body">*/
/*       			<form action="{{ action }}" method="post" enctype="multipart/form-data" id="form-so-call-for-price" class="form-horizontal">*/
/*       				<input type="hidden" name="action" id="action" value=""/>*/
/*                     <div class="form-group">*/
/*                         <label class="col-sm-4 control-label" for="input-status">{{ entry_status }}</label>*/
/*                         <div class="col-sm-8">*/
/*                             <select name="module_so_call_for_price_status" id="input-status" class="form-control">*/
/*                                 {% if status %}*/
/*                                     <option value="1" selected="selected">{{ text_enabled }}</option>*/
/*                                     <option value="0">{{ text_disabled }}</option>*/
/*                                 {% else %}*/
/*                                     <option value="1">{{ text_enabled }}</option>*/
/*                                     <option value="0" selected="selected">{{ text_disabled }}</option>*/
/*                                 {% endif %}*/
/*                             </select>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="form-group">*/
/*                         <label class="col-sm-4 control-label" for="input-hide_cart">{{ entry_hide_cart }}</label>*/
/*                         <div class="col-sm-8">*/
/*                             <select name="module_so_call_for_price_hide_cart" id="input-hide_cart" class="form-control">*/
/*                                 {% if hide_cart %}*/
/*                                     <option value="1" selected="selected">{{ text_yes }}</option>*/
/*                                     <option value="0">{{ text_no }}</option>*/
/*                                 {% else %}*/
/*                                     <option value="1">{{ text_yes }}</option>*/
/*                                     <option value="0" selected="selected">{{ text_no }}</option>*/
/*                                 {% endif %}*/
/*                             </select>*/
/*                             <div class="help-block">{{ entry_hide_cart_desc }}</div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="form-group">*/
/*                         <label class="col-sm-4 control-label" for="input-replace_cart">{{ entry_replace_cart }}</label>*/
/*                         <div class="col-sm-8">*/
/*                             <select name="module_so_call_for_price_replace_cart" id="input-replace_cart" class="form-control">*/
/*                                 {% if replace_cart %}*/
/*                                     <option value="1" selected="selected">{{ text_yes }}</option>*/
/*                                     <option value="0">{{ text_no }}</option>*/
/*                                 {% else %}*/
/*                                     <option value="1">{{ text_yes }}</option>*/
/*                                     <option value="0" selected="selected">{{ text_no }}</option>*/
/*                                 {% endif %}*/
/*                             </select>*/
/*                             <div class="help-block">{{ entry_replace_cart_desc }}</div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="form-group">*/
/*                         <label class="col-sm-4 control-label" for="input-sendmailto">{{ entry_send_mail_to }}</label>*/
/*                         <div class="col-sm-8">*/
/*                             <input type="text" name="module_so_call_for_price_send_mail_to" value="{{ send_mail_to }}" id="input-sendmailto" class="form-control" />*/
/*                             <div class="help-block">{{ entry_send_mail_to_desc }}</div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="form-group">*/
/*                         <label class="col-sm-4 control-label" for="input-send_mail_customer">{{ entry_send_mail_customer }}</label>*/
/*                         <div class="col-sm-8">*/
/*                             <select name="module_so_call_for_price_send_mail_customer" id="input-send_mail_customer" class="form-control">*/
/*                                 {% if send_mail_customer %}*/
/*                                     <option value="1" selected="selected">{{ text_yes }}</option>*/
/*                                     <option value="0">{{ text_no }}</option>*/
/*                                 {% else %}*/
/*                                     <option value="1">{{ text_yes }}</option>*/
/*                                     <option value="0" selected="selected">{{ text_no }}</option>*/
/*                                 {% endif %}*/
/*                             </select>*/
/*                             <div class="help-block">{{ entry_send_mail_customer_desc }}</div>*/
/*                         </div>*/
/*                     </div>*/
/*       			</form>*/
/*       		</div>*/
/*       	</div>*/
/*   	</div>*/
/* </div>*/
/* <style>*/
/* .btn-success {*/
/* 	background-color: #fff;*/
/* 	color: #000;*/
/* 	border-color: #ddd;*/
/* }*/
/* .btn-success1 {*/
/*     background-color: #8fbb6c;*/
/*     border-color: #7aae50;*/
/*     color: #fff;*/
/* }*/
/* .btn-success1:hover {*/
/* 	background-color: #648e42;*/
/*     border-color: #3d5728;*/
/*     color: #fff;*/
/* }*/
/* </style>*/
/* <script type="text/javascript">*/
/*     /*language*//* */
/*     $('#language').change(function(){*/
/*         var that = $(this), opt_select = $('option:selected', that).val() , _input = $('#input-head-name-'+opt_select);*/
/*         $('[id^="input-head-name-"]').addClass('hide');*/
/*         _input.removeClass('hide');*/
/*     });*/
/* */
/*     $('.first-name').change(function(){*/
/*         $('input[name="head-name"]').val($(this).val());*/
/*     });*/
/* </script>*/
/* {{ footer }}*/
