<?php

/* default/template/checkout/cart.twig */
class __TwigTemplate_7f5e7f2aab8a003c1d6f914b3c674ca692f78b3ce37e49f987b939b25b8c9f79 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo "

 <div class=\"row\">
        <div class=\"col-md-8\">
            <div class=\"cart-block\">
                <h3><i class=\"fa fa-cart\"></i>My cart</h3>
                <ul>
                    <li>backkk</li>
                </ul>
            </div>
        </div>
    </div>

";
        // line 14
        echo (isset($context["footer"]) ? $context["footer"] : null);
        echo " ";
    }

    public function getTemplateName()
    {
        return "default/template/checkout/cart.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 14,  19 => 1,);
    }
}
/* {{ header }}*/
/* */
/*  <div class="row">*/
/*         <div class="col-md-8">*/
/*             <div class="cart-block">*/
/*                 <h3><i class="fa fa-cart"></i>My cart</h3>*/
/*                 <ul>*/
/*                     <li>backkk</li>*/
/*                 </ul>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* */
/* {{ footer }} */
