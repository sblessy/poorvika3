<?php
class ControllerCheckoutSuccess extends Controller {
	public function index() {
		$this->load->language('checkout/success');

		if (isset($this->session->data['order_id'])) {

            $customer_id = $this->customer->getId();
        if($customer_id){
            $wk_order_id = $this->session->data['order_id'];
            $this->load->model('extension/module/wk_preorder_pro');
            if(isset($this->session->data['wk_preorder']) && $this->session->data['wk_preorder'] == 'update'){
                $wk_order_id = $this->session->data['wk_order_id'];
				$wk_product_id = $this->session->data['wk_product_id'];
				$products = $this->model_extension_module_wk_preorder_pro->getOrderProducts($wk_order_id);
				foreach ($products as $key => $value) {
					$isPreorder = $this->model_extension_module_wk_preorder_pro->isPreorder($value['product_id']);
					if(isset($isPreorder['product_id']) && $isPreorder['deduction_type'] == 'pc' ){
						$price = $isPreorder['rem_price'];
						if (isset($isPreorder['discount']) && $isPreorder['discount']) {
							$price  = $price - ($price * $isPreorder['discount'] ) / 100;
						}
                        $details = array(
                            'order_id' => $wk_order_id,
                            'product_id' => $isPreorder['product_id'],
                            'preorder_product_id' => $isPreorder['id'],
                            'customer_id' => $customer_id,
                            'status' => 0,
							'quantity' => $value['quantity'],
							'price'	=>	$price,
                        );
                        $this->model_extension_module_wk_preorder_pro->finalizePreOrder($details,$this->config->get('module_wk_preorder_pro_order_status'));
                         $this->model_extension_module_wk_preorder_pro->sendNotification($details['order_id']);
                    }

				}

                unset($this->session->data['wk_confirm_order']);
                unset($this->session->data['wk_preorder_id']);
                unset($this->session->data['module_wk_preorder_product_id']);
                unset($this->session->data['wk_order_id']);
                unset($this->session->data['wk_product_id']);
                unset($this->session->data['wk_preorder']);
               
            }else{
                $products = $this->model_extension_module_wk_preorder_pro->getOrderProducts($wk_order_id);
                foreach ($products as $key => $value) {
					$isPreorder = $this->model_extension_module_wk_preorder_pro->isPreorder($value['product_id']);
                    if(isset($isPreorder['product_id']) && $isPreorder['deduction_type'] == 'pc' ){
						$price = $isPreorder['base_price'] - $isPreorder['rem_price'];
						if (isset($isPreorder['discount']) && $isPreorder['discount']) {
							$price  = $price - ($price * $isPreorder['discount'] ) / 100;
						}
                        $details = array(
                            'order_id' => $wk_order_id,
                            'product_id' => $isPreorder['product_id'],
                            'preorder_product_id' => $isPreorder['id'],
                            'customer_id' => $customer_id,
                            'status' => 0,
							'quantity' => $value['quantity'],
							'price'	=>	$price,
                        );
                        $this->model_extension_module_wk_preorder_pro->addPreorderOrder($details);
                    } else if( isset($isPreorder['product_id']) && $isPreorder['deduction_type'] == 'fp'){
						$price = $isPreorder['base_price'];
						if (isset($isPreorder['discount']) && $isPreorder['discount']) {
							$price  = $price - ($price * $isPreorder['discount'] ) / 100;
						}
                        $details = array(
                            'order_id' => $wk_order_id,
                            'product_id' => $isPreorder['product_id'],
                            'preorder_product_id' => $isPreorder['id'],
                            'customer_id' => $customer_id,
                            'status' => 1,
							'quantity' => $value['quantity'],
							'price'	=>	$price,
                        );
                        $this->model_extension_module_wk_preorder_pro->sendNotification($details['order_id']);
                        $this->model_extension_module_wk_preorder_pro->addPreorderOrder($details, $this->config->get('module_wk_preorder_pro_order_status'));
                    }
                }
            }
        }
        // end here
                            
			$this->cart->clear();

			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['payment_method']);
			unset($this->session->data['payment_methods']);
			unset($this->session->data['guest']);
			unset($this->session->data['comment']);
			unset($this->session->data['order_id']);
			unset($this->session->data['coupon']);
			unset($this->session->data['reward']);
			unset($this->session->data['voucher']);
			unset($this->session->data['vouchers']);
			unset($this->session->data['totals']);
		}

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_basket'),
			'href' => $this->url->link('checkout/cart')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_checkout'),
			'href' => $this->url->link('checkout/checkout', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_success'),
			'href' => $this->url->link('checkout/success')
		);

		if ($this->customer->isLogged()) {
			$data['text_message'] = sprintf($this->language->get('text_customer'), $this->url->link('account/account', '', true), $this->url->link('account/order', '', true), $this->url->link('account/download', '', true), $this->url->link('information/contact'));
		} else {
			$data['text_message'] = sprintf($this->language->get('text_guest'), $this->url->link('information/contact'));
		}

		$data['continue'] = $this->url->link('common/home');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('common/success', $data));
	}
}