<?php
class Controlleraccountwkpreorderlist extends Controller {

    public function index() {

      if(!$this->config->get('module_wk_preorder_pro_status')){
        $this->response->redirect($this->url->link('account/account', '', true));
      }

      if (!$this->customer->isLogged()) {
  			$this->session->data['redirect'] = $this->url->link('account/account', '', true);
  			$this->response->redirect($this->url->link('account/login', '', true));
  		}
		// $this->load->model('extension/module/wk_preorder_pro');
		// $this->model_extension_module_wk_preorder_pro->finalizePreOrder(92,48,1);
      $data = array();
      $data = array_merge($data, $this->language->load('account/wk_preorder'));

  		$this->document->setTitle($this->language->get('heading_title'));
  		$data['breadcrumbs'] = array();
  		$data['breadcrumbs'][] = array(
  			'text' => $this->language->get('text_home'),
  			'href' => $this->url->link('common/home')
  		);

    	$data['breadcrumbs'][] = array(
    		'text' => $this->language->get('text_account'),
    		'href' => $this->url->link('account/account', '', true)
    	);

      $data['breadcrumbs'][] = array(
    		'text' => $this->language->get('heading_title'),
    		'href' => $this->url->link('account/wk_preorder_list', '', true)
    	);

      $this->load->model('account/wk_pre_order');
	  $preorders = $this->model_account_wk_pre_order->getPreOrder($this->customer->getId());
	
      $preorders_total = $this->model_account_wk_pre_order->getPreOrderTotal($this->customer->getId());
      $data['preorders'] = array();
      $data['module_wk_preorder_pro_notification_status'] = $this->config->get('module_wk_preorder_pro_notification_status');

      if($preorders) {
        foreach ($preorders as $key => $preorder) {

          $order_total = $this->model_account_wk_pre_order->getOrderTotal($preorder['order_id']);
      		$product_option = $this->model_account_wk_pre_order->getOrderProductOptions(11);
      		$totals = array();
      		$other = 0;
      		$data['currency_change_url'] = $this->url->link('account/wk_preorder_list', '&currency_change=dtsfa', true);
      		if(isset($this->request->get['currency_change']) && $this->request->get['currency_change'] == 'dtsfa') {
      			$data['currency_change_url'] = $this->url->link('account/wk_preorder_list', '', true);
      			$currency_code = $this->session->data['currency'];
      		} else {
      			$currency_code = $preorder['currency_code'];
      		}

    			if(!$currency_code) {
    			$currency_code = $this->session->data['currency'];
    			}
    			if(isset($order_total) && $order_total) {
    				foreach ($order_total as $key => $value) {
    					$totals[] = array(
    						'value' => $value['value'],
    						'text' => $this->currency->format($value['value'], $currency_code),
    						'title' => $value['title'],
    					);
    					$other = $other + $value['value'];
    				}
    			}

          $data['preorders'][] = array(

              'id'	=> $preorder['id'],
              'order_id' => $preorder['order_id'],
    					'name'	=> $preorder['firstname'].' '.$preorder['lastname'],
    					'product_id' => $preorder['product_id'],
              'product_name' => $preorder['name'],
    					'stock_status_id' => $preorder['stock_status_id'],
    					'quantity' => $preorder['quantity'],
    					'total'	=> $this->currency->format( ($preorder['total']), $currency_code),
    					'options' => $this->model_account_wk_pre_order->getOrderProductOptions($preorder['order_id']),
    					'date_added' => $preorder['date_added'],
    					'status' => $preorder['status'],
    					'base_price' => $this->currency->format( ($preorder['base_price'] * $preorder['quantity'] ), $this->session->data['currency']),
    					'rem_price' => $this->currency->format($preorder['status'] ? 0 : ( $preorder['rem_price'] * $preorder['quantity']), $this->session->data['currency']),
    					'extra_total' => $totals,
    					'product_price' => $this->currency->format( ( $preorder['paid_amount']), $currency_code),
    					'view'	=> $this->url->link('account/wk_preorder_list/view', '&id='. $preorder['id'], true),
              );
        }
      }

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$pagination = new Pagination();
		$pagination->total = $preorders_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('account/wk_preorder_list', '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($preorders_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($preorders_total - $this->config->get('config_limit_admin'))) ? $preorders_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $preorders_total, ceil($preorders_total / $this->config->get('config_limit_admin')));

    $data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('account/wk_preorder_list', $data));
    }

}

?>
