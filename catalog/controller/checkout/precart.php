<?php
################################################################################################
#  Preorder Pro Opencart 2.x.x.x From Webkul  http://webkul.com 	#
################################################################################################
?><?php

class ControllerCheckoutPrecart extends Controller {

	private $error = array();

	public function index() {

		$this->load->language('checkout/precart');

		// Update
		if (!empty($this->request->post['quantity'])) {

			foreach ($this->request->post['quantity'] as $key => $value) {
				$this->update($key, $value);
			}

			$this->response->redirect($this->url->link('checkout/precart'));
		}

		// Remove
		if (isset($this->request->get['remove'])) {
			$this->remove($this->request->get['remove']);

			$this->session->data['success'] = $this->language->get('text_remove');

			$this->response->redirect($this->url->link('checkout/precart'));
		}

		$this->document->setTitle($this->language->get('heading_title'));

      	$data['breadcrumbs'] = array();

      	$data['breadcrumbs'][] = array(
        	'href'      => $this->url->link('common/home'),
        	'text'      => $this->language->get('text_home'),
        	'separator' => false
      	);

      	$data['breadcrumbs'][] = array(
        	'href'      => $this->url->link('checkout/precart'),
        	'text'      => $this->language->get('heading_title'),
        	'separator' => $this->language->get('text_separator')
      	);


    	if ($this->hasProducts()) {

      		$data['heading_title'] = $this->language->get('heading_title');

			$data['column_image'] = $this->language->get('column_image');
      		$data['column_name'] = $this->language->get('column_name');
      		$data['column_model'] = $this->language->get('column_model');
      		$data['column_quantity'] = $this->language->get('column_quantity');
			$data['text_name'] = $this->language->get('text_name');
      		$data['text_email'] = $this->language->get('text_email');
			$data['text_enquiry'] = $this->language->get('text_enquiry');
			$data['text_code'] = $this->language->get('text_code');
			$data['text_next'] = $this->language->get('text_next');
			$data['text_wait'] = $this->language->get('text_wait');
			$data['button_update'] = $this->language->get('button_update');
			$data['button_remove'] = $this->language->get('button_remove');


      		$data['button_checkout'] = $this->language->get('text_on_enquiry');

			if (isset($this->error['warning'])) {
				$data['error_warning'] = $this->error['warning'];
			} else {
				$data['error_warning'] = '';
			}

			if (isset($this->session->data['success'])) {
				$data['success'] = $this->session->data['success'];
				unset($this->session->data['success']);
			} else {
				$data['success'] = '';
			}

			if (!$this->customer->isLogged()) {
				$data['attention'] = sprintf($this->language->get('text_login'), $this->url->link('account/login'), $this->url->link('account/register'));
			} else {
				$data['attention'] = '';
			}

			$data['action'] = $this->url->link('checkout/precart');

			$this->load->model('tool/image');

      		$data['products'] = array();

			$products = $this->getProducts();

			if($products){

	      		foreach ($products as $product) {

					$product_total = 0;
					foreach ($products as $product_2) {
						if ($product_2['product_id'] == $product['product_id']) {
							$product_total += $product_2['quantity'];
						}
					}

					if ($product['image']) {

						$image = $this->model_tool_image->resize($product['image'], $this->config->get($this->config->get('config_theme') . '_image_cart_width'), $this->config->get($this->config->get('config_theme') . '_image_cart_height'));
					} else {
						$image = '';
					}

					$option_data = array();

	        		foreach ($product['option'] as $option) {
						if ($option['type'] != 'file') {
							$value = $option['option_value'];
						} else {
							$filename = $this->encryption->decrypt($option['option_value']);

							$value = utf8_substr($filename, 0, utf8_strrpos($filename, '.'));
						}

						$option_data[] = array(
							'name'  => $option['name'],
							'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
						);
	        		}

	        		$data['products'][] = array(
	          			'key'      => $product['key'],
	          			'thumb'    => $image,
						'name'     => $product['name'],
	          			'model'    => $product['model'],
	          			'option'   => $option_data,
	          			'quantity' => $product['quantity'],
	          			'stock'    => $product['stock'] ? true : !(!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning')),
						'href'     => $this->url->link('product/product', 'product_id=' . $product['product_id']),
						'remove'   => $this->url->link('checkout/precart', 'remove=' . $product['key'])
					);
	      		}
	      	}

			$data['continue'] = $this->url->link('common/home');

			$data['checkout'] = $this->url->link('checkout/checkout', '', 'SSL');

			$data['header'] = $this->load->Controller('common/header');
			$data['footer'] = $this->load->Controller('common/footer');
			$data['column_left'] = $this->load->Controller('common/column_left');
			$data['column_right'] = $this->load->Controller('common/column_right');
			$data['content_bottom'] = $this->load->Controller('common/content_bottom');
			$data['content_top'] = $this->load->Controller('common/content_top');

		    $this->response->setOutput($this->load->view('checkout/precart', $data));
    	} else {
      		$data['heading_title'] = $this->language->get('heading_title');

      		$data['text_error'] = $this->language->get('text_empty');

      		$data['button_continue'] = $this->language->get('button_continue');

      		$data['continue'] = $this->url->link('common/home');

			unset($this->session->data['success']);

			$data['header'] = $this->load->Controller('common/header');
			$data['footer'] = $this->load->Controller('common/footer');
			$data['column_left'] = $this->load->Controller('common/column_left');
			$data['column_right'] = $this->load->Controller('common/column_right');
			$data['content_top'] = $this->load->Controller('common/content_top');
			$data['content_bottom'] = $this->load->Controller('common/content_bottom');

			$this->response->setOutput($this->load->view('error/not_found', $data));
    	}
  	}

	public function generateEnquiry() {
		$json = array();
		$this->load->language('checkout/precart');
  		if($this->request->server['REQUEST_METHOD'] == 'POST' && $this->request->post) {
			if(isset($this->request->post['g-recaptcha-response']) || !$this->request->post['g-recaptcha-response']) {
				$secret = $this->config->get('module_wk_preorder_pro_secret_key');
				$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$this->request->post['g-recaptcha-response']);
				$responseData = json_decode($verifyResponse);
				if(!$responseData->success) {
					$json['success'] = false;
					$json['error_captcha'] = $this->language->get('error_captcha');
				}
			} else {
				$json['success'] = false;
				$json['error_captcha'] = $this->language->get('error_captcha');
			}
			if(!isset($this->request->post['preorder_name']) || !$this->request->post['preorder_name'] || strlen(trim($this->request->post['preorder_name'])) < 2) {
				$json['success'] = false;
				$json['error_name'] = $this->language->get('error_name');
			}
			if(!isset($this->request->post['preorder_email']) || !$this->request->post['preorder_email'] || !filter_var($this->request->post['preorder_email'], FILTER_VALIDATE_EMAIL)) {
				$json['success'] = false;
				$json['error_email'] = $this->language->get('error_email');
			}
			if(!isset($this->request->post['preorder_subject']) || !$this->request->post['preorder_subject'] || strlen(trim($this->request->post['preorder_subject'])) < 5 || strlen(trim($this->request->post['preorder_subject'])) > 50) {
				$json['success'] = false;
				$json['error_subject'] = $this->language->get('error_subject');
			}
			if(!isset($this->request->post['preorder_query']) || !$this->request->post['preorder_query'] || strlen(trim($this->request->post['preorder_query'])) < 10 || strlen(trim($this->request->post['preorder_query'])) > 1000) {
				$json['success'] = false;
				$json['error_query'] = $this->language->get('error_query');
			}

			if(!$json) {
				$json['success'] = true;
				$this->load->model('extension/module/wk_preorder_pro');
				$this->model_extension_module_wk_preorder_pro->addPreOrderEnquiry($this->request->post);
				$json['success_msg'] = $this->language->get('success_msg_enquiry');
			}
			$this->response->setOutput(json_encode($json));
		}
	}

	public function customer_preordered()
	{
		$json = array();
		$this->load->language('checkout/precart');

  		if($this->request->server['REQUEST_METHOD'] == 'POST') {
			$product_id = 0;
			if(isset($this->request->post['product_id']) && $this->request->post['product_id']) {
				$product_id = $this->request->post['product_id'];
			}

				$this->load->model("extension/module/wk_preorder_pro");
				$isPreOrder = $this->model_extension_module_wk_preorder_pro->isPreorder($product_id);

				if($isPreOrder) {
					if($this->customer->isLogged()) {
						$isEligible = $this->model_extension_module_wk_preorder_pro->checkCustomerEligibility($isPreOrder, $this->customer->getId());
						if($isEligible['success']) {
							$result = true;
							$cart_products = $this->cart->getProducts();
							if($cart_products) {
								foreach($cart_products as $product) {
									if (!$this->model_extension_module_wk_preorder_pro->isPreorder($product['product_id'])) {
										$result = false;
										$json['msg'] = $this->language->get('error_product_in_cart');
									}
								}
								
							}
						} else {
							$result = false;
							$json['msg'] = $this->language->get('error_'.$isEligible['type']);
						}
					} else {
						$result = false;
						$json['msg'] = $this->language->get('error_login_error');
					}
				} else {
					$cart_has_preorder_product = false;
					if($this->cart->getProducts()) {
						$this->load->model('extension/module/wk_preorder_pro');
						foreach ($this->cart->getProducts() as $key => $cart_product) {
							if($this->model_extension_module_wk_preorder_pro->isPreorder($cart_product['product_id'])) {
								$cart_has_preorder_product = true;
							}
						}
					}
					if($cart_has_preorder_product) {
						$result = false;
						$json['msg'] = $this->language->get('error_preorder_cart');
					} else {
						$result = true;
						$json['msg'] = $this->language->get('error_not_preorder_product');
					}
				}


				if($result) {
					$json['success'] = true;
				} else {
					$json['success'] = false;
				}
  		}
		$this->response->setOutput(json_encode($json));
  	}

  	public function set_session_to_confirm_order() {
		$this->load->language('checkout/precart');
		$json = array();
  		if($this->request->server['REQUEST_METHOD'] == 'POST') {
			if(isset($this->request->get['order_id']) && $this->request->get['order_id']) {
				$this->load->model('extension/module/wk_preorder_pro');
				$result = $this->model_extension_module_wk_preorder_pro->getPreorderDetailsByOrderId($this->request->get['order_id']);
				if($result) {
					$json['success'] = true;
					$json['msg'] = $this->language->get('success_preorder_session_enabled');
					$this->session->data['wk_preorder_id'] = $result['id'];
					$this->session->data['module_wk_preorder_product_id'] = $result['preorder_product_id'];
					$this->session->data['wk_confirm_order'] = true;
				} else {
					$json['success'] = false;
					$json['msg'] = $this->language->get('error_preorder_not_auth');
				}
			} else {
				$json['success'] = false;
				$json['msg'] = $this->language->get('error_general');
			}
  		} else {
  			$json['success'] = false;
			$json['msg'] = $this->language->get('error_general');
  		}
		$this->response->setOutput(json_encode($json));
  	}

  	public function cart_has_preorder_product(){
  		$products = $this->cart->getProducts();
  		$this->load->model('extension/module/wk_preorder_pro');
  		if($products){
	  		foreach ($products as $key => $product) {
	  			$result = $this->model_extension_module_wk_preorder_pro->isPreorder($product['product_id']);
	  			if(isset($result['product_id'])){
	  				echo "preorder";
	  				break;
	  			}
	  		}
	  	}
  	}

  	public function empty_cart(){
  		if($this->request->server['REQUEST_METHOD'] == 'POST'){
  			$this->cart->clear();
  			echo "success";
  		}else{
  			echo "warning";
  		}
  	}

  	public function write() {

		$this->load->language('checkout/precart');

		$this->load->model('extension/module/wk_preorder_pro');

		$json = array();

		if ($this->request->server['REQUEST_METHOD'] == 'POST') {

			if (empty($this->session->data['captcha']) || ($this->session->data['captcha'] != $this->request->post['captcha'])) {
				$json['error'] = $this->language->get('error_captcha');
				$json['name'] = 'pre_code';
				$json['input'] = 'input';
			}

			if ((utf8_strlen($this->request->post['text']) < 25) || (utf8_strlen($this->request->post['text']) > 1000)) {
				$json['error'] = $this->language->get('error_enquiry');
				$json['name'] = 'pre_enquiry';
				$json['input'] = 'textarea';
			}

			if ((utf8_strlen($this->request->post['email']) > 96) || !preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->post['email'])) {
				$json['error'] = $this->language->get('error_email');
				$json['name'] = 'pre_email';
				$json['input'] = 'input';
			}

			if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 25)) {
				$json['error'] = $this->language->get('error_name');
				$json['name'] = 'pre_name';
				$json['input'] = 'input';
			}

			if (!isset($json['error'])) {

				$c_id = $this->model_extension_module_wk_preorder_pro->addPreorderOrderCustomer($this->request->post);
				$products = $this->getProducts();
				if($products){
		      		foreach ($products as $product) {
						$this->model_extension_module_wk_preorder_pro->addPreorderOrderprecart($product,$c_id);
					}
				}

				$this->model_extension_module_wk_preorder_pro->SendPrecartMail($products,$this->request->post);

			 	$this->clear();
				$this->session->data['success'] = $this->language->get('text_success_query');
			 	$json['success'] = $this->language->get('text_success_query');
			}
		}

		$this->response->setOutput(json_encode($json));
	}

 //  	public function captcha() {

 //  		$captcha = $this->load->Controller('tool/captcha');
 //  		echo $captcha;
	// 	$this->load->library('captcha');

	// 	$captcha = new Captcha();

	// 	echo $this->session->data['captcha'] = $captcha->getCode();

	// 	$captcha->showImage();
	// }

	public function addprecart() {

		$this->load->language('checkout/precart');

		$json = array();

		if (isset($this->request->post['product_id'])) {
			$product_id = $this->request->post['product_id'];
		} else {
			$product_id = 0;
		}

		$this->load->model('catalog/product');

		$product_info = $this->model_catalog_product->getProduct($product_id);

		if ($product_info) {
			if (isset($this->request->post['quantity'])) {
				$quantity = $this->request->post['quantity'];
			} else {
				$quantity = 1;
			}

			if (isset($this->request->post['option'])) {
				$option = array_filter($this->request->post['option']);
			} else {
				$option = array();
			}

			$product_options = $this->model_catalog_product->getProductOptions($this->request->post['product_id']);

			foreach ($product_options as $product_option) {
				if ($product_option['required'] && empty($option[$product_option['product_option_id']])) {
					$json['error']['option'][$product_option['product_option_id']] = sprintf($this->language->get('error_required'), $product_option['name']);
				}
			}

			if (!$json) {
				$this->add($this->request->post['product_id'], $quantity, $option);

				$json['success'] = sprintf($this->language->get('text_success_precart'), $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']), $product_info['name'], $this->url->link('checkout/precart'));
			} else {
				$json['redirect'] = str_replace('&amp;', '&', $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']));
			}
		}

		$this->response->setOutput(json_encode($json));
	}


  	public function add($product_id, $qty = 1, $option = array()) {
    	if (!$option) {
      		$key = (int)$product_id;
    	} else {
      		$key = (int)$product_id . ':' . base64_encode(serialize($option));
    	}

		if ((int)$qty && ((int)$qty > 0)) {
    		if (!isset($this->session->data['precart'][$key])) {
      			$this->session->data['precart'][$key] = (int)$qty;
    		} else {
      			$this->session->data['precart'][$key] += (int)$qty;
    		}
		}

		$data = array();
  	}

  	public function update($key, $qty) {
    	if ((int)$qty && ((int)$qty > 0)) {
      		$this->session->data['precart'][$key] = (int)$qty;
    	} else {
	  		$this->remove($key);
		}

		$data = array();
  	}

  	public function remove($key) {
		if (isset($this->session->data['precart'][$key])) {
     		unset($this->session->data['precart'][$key]);
  		}

		$data = array();
	}

	public function hasProducts() {
    	return count($this->session->data['precart']);
  	}

  	public function clear() {
		$this->session->data['precart'] = array();
		$data = array();
  	}

  	public function countProducts() {
		$product_total = 0;

		$products = $this->getProducts();

		foreach ($products as $product) {
			$product_total += $product['quantity'];
		}

		return $product_total;
	}

  	public function hasStock() {
		$stock = true;

		foreach ($this->getProducts() as $product) {
			if (!isset($product['stock']) || !$product['stock']) {
	    		$stock = false;
			}
		}

    	return $stock;
  	}

	public function clearPreOrderSessionTrigger() {
		unset($this->session->data['wk_confirm_order']);
		unset($this->session->data['wk_preorder_id']);
		unset($this->session->data['module_wk_preorder_product_id']);
		unset($this->session->data['wk_order_id']);
		unset($this->session->data['wk_product_id']);
		unset($this->session->data['wk_preorder']);
	}

}

?>
