<?php
##################################################################
#  Preorder Pro Opencart 2.x.x.x From Webkul  http://webkul.com  #
##################################################################
// Heading
$_['heading_title']          = 'Pre Order Cart';

// Text
$_['text_success_precart']           = 'Success: You have added <a href="%s">%s</a> to your <a href="%s">pre order cart</a>!';
$_['text_success_query']     = 'Success: You have send your Pre Order details to Admin !';
$_['text_remove']            = 'Success: You have modified your pre order cart!';
$_['text_login']             = 'Attention: You must <a href="%s">login</a> or <a href="%s">create an account</a> to view prices!';
$_['text_login_button']      = 'Login';
$_['text_items']             = '%s item(s) - %s';
$_['text_next']              = 'Send Mail To Us About Your Requirement';
$_['text_empty']             = 'Your pre order cart is empty!';
$_['text_enquiry_heading']   = 'Enquiry for preordering ';
$_['text_name']              = 'Your Name: ';
$_['text_email']             = 'Your Email: ';
$_['text_subject']           = 'Subject: ';
$_['text_enquiry']           = 'Your Query: ';
$_['text_captcha']           = 'Confirm Captcha:';
$_['text_on_enquiry']        = 'Enquiry ';
$_['text_close']             = 'Close';
$_['text_submit']            = 'Submit';
$_['text_wait']       		 = 'Please Wait ..';
$_['text_preorder']       		 = ' ( Pre-Order )';
$_['text_out0fstock']       		 = 'Out of stock';
$_['text_error']       		 = 'Error!';
$_['text_complete_preorder']        = 'Complete Preorder ';
$_['text_preorder_completed']       		 = 'Preorder Completed';
$_['text_rem_amount']       		 = 'Remaining Amount';
$_['text_preorder_status']       		 = 'Preorder Status';
$_['text_add_permission']       		 = 'Adding preorder to cart will remove all product(s)! Are you sure?';
$_['text_product_preorder']       		 = 'You can pre-order';
$_['text_preorder_price_fixed']    		 = 'Pre order price ';
$_['text_preorder_price_percentage']     = 'Pre order price - %s before completion of the order ';
$_['text_left_days']                    = 'Only %d more day(s) left to pre-order, Hurry up!!';
// $_['text_product_preorder']       		 = 'You can pre-order';
$_['text_msg']                      ='Order placed for pre order product';
$_['text_discount']                = 'Pre Order Discount applied of %f';
$_['text_discount_fixed']          = 'Pre Order Discount applied of %s';
$_['text_last_day']                = 'Only Last day left to pre-order, Hurry up!!'   ;  

// Column
$_['column_image']           = 'Image';
$_['column_name']            = 'Product Name';
$_['column_model']           = 'Model';
$_['column_quantity']        = 'Quantity';

// Entry
$_['entry_coupon']           = 'Enter your coupon here:';
$_['entry_voucher']          = 'Enter your gift voucher code here:';
$_['entry_reward']           = 'Points to use (Max %s):';
$_['entry_country']          = 'Country:';
$_['entry_zone']             = 'Region / State:';
$_['entry_postcode']         = 'Post Code:';

// success
$_['success_msg_enquiry']    = 'Success: Your query has been succesfully submitted to us and soon we will revert you back.';
$_['success_preorder_session_enabled']    = 'Success: Pre-Order session is enabled, go for checkout.';
// Error
$_['error_general']            = ' Warning: There is some issue, please try again later!';
$_['error_preorder_not_auth']            = ' Warning: You are not authorized to complete this preorder!';
$_['error_name']            = 'Name must be between 3 to 25 characters!';
$_['error_email']           = 'Please enter a valid email address!';
$_['error_subject']         = 'Subject must be between 5 to 50 characters!';
$_['error_query']           = 'Query must be between 10 to 1000 characters!';
$_['error_captcha']         = 'Please verify captcha!';
$_['error_required']        = '%s required!';
$_['error_not_preorder_product']        = 'Warning: Not a preorder product!';
$_['error_order_quantity']        = 'You can not buy this much of quantity, maximum %d quantity is allowed!';
$_['error_login_error']     = ' Warning: Please Login to pre-order this product!';
$_['error_one_ip_one_order'] = ' Warning: We have already received order from this ip address and for this product same IP Address can order once only!';
$_['error_order_per_customer']            = 'Total number of product is exceeded to order by one account!';
$_['error_no_more_preorder']            = 'We are out of stock for perorders, please click on enquiry and ask if you have any query!';
$_['error_already_preordered']         = ' Warning: You have pre-order already, please complete that order and then order again!';
$_['error_preorder_cart']         = ' Warning: Cart has preorder product, so please first checkout with that and then do other order!';
$_['error_product_in_cart']         = ' Warning: Cart has product(s), so please first checkout with that and then you can do preorder or clear your cart!';
$_['error_preorder_quantity']         = ' Warning: Quantity is not as, as in pre-order. Please make it as per pre-order that is %d!';
$_['error_preorder_not_found']         = ' Warning: You can not order this product because order not found';
$_['error_preorder_other']	= ' Warning: Your order has other products please remove them to complete the preorder.';
