<?php

class Modelaccountwkpreorder extends Model {

  public function getProductEditStatus($product_id , $customer_id){
    $result = $this->db->query("SELECT * FROM ".DB_PREFIX."preordred_product_detail WHERE product_id = ".$product_id." AND customer_id = ".$customer_id." AND `status` = 0");
    if($result->num_rows > 0){
      return true;
    }else{
      return false;
    }
  }

    public function getProOrderProducts() {
        $sql = "SELECT pd.name,pp.product_id FROM ".DB_PREFIX."preorder_products pp LEFT JOIN ".DB_PREFIX."product_description pd ON (pd.product_id=pp.product_id) WHERE pd.language_id = ".(int)$this->config->get('config_language_id')." AND pp.status = '1' ";
        $result = $this->db->query($sql)->rows;
        if($result) {
            return $result;
        } else {
            return false;
        }
    }

    public function getPreOrder($customer_id) {
        $sql = "SELECT DISTINCT ppd.id,p.stock_status_id, ppd.quantity, ppd.product_id, pp.base_price, o.order_id, o.currency_code, c.firstname,c.lastname,o.total, o.date_added, ppd.status, ppd.paid_amount, pp.rem_price , pd.name FROM ".DB_PREFIX."preordred_product_detail ppd LEFT JOIN ".DB_PREFIX."order o ON (o.order_id=ppd.order_id) LEFT JOIN ".DB_PREFIX."product p ON (p.product_id=ppd.product_id) LEFT JOIN ".DB_PREFIX."customer c ON (c.customer_id=o.customer_id) LEFT JOIN ".DB_PREFIX."preorder_products pp ON (pp.product_id=ppd.product_id) LEFT JOIN `".DB_PREFIX."product_description` pd ON pd.product_id=ppd.product_id WHERE ppd.customer_id = '".(int)$customer_id."' ORDER BY o.date_added DESC ";

        $result = $this->db->query($sql)->rows;
        if($result) {
            return $result;
        } else {
            return false;
        }
    }

    public function getPreOrderTotal($customer_id) {
        $sql = "SELECT ppd.id, o.order_id, c.firstname,c.lastname,o.total, o.date_added, ppd.status, pp.rem_price FROM ".DB_PREFIX."preordred_product_detail ppd LEFT JOIN ".DB_PREFIX."order o ON (o.order_id=ppd.order_id) LEFT JOIN ".DB_PREFIX."customer c ON (c.customer_id=o.customer_id) LEFT JOIN ".DB_PREFIX."preorder_products pp ON (pp.product_id=ppd.product_id) WHERE ppd.customer_id = '".(int)$customer_id."' ";

        $result = $this->db->query($sql)->rows;
        if($result) {
            return count($result);
        } else {
            return false;
        }
    }

    public function getOrderTotal($order_id) {
        $sql = "SELECT * FROM ".DB_PREFIX."order_total ot WHERE ot.order_id = '".(int)$order_id."' AND code != 'total' AND code != 'sub_total' ";
        $result = $this->db->query($sql)->rows;
        if($result) {
            return $result;
        } else {
            return false;
        }
    }

    public function getOrderProductOptions($order_id) {
        $sql = "SELECT oo.product_option_id, oo.product_option_value_id, oo.type,oo.value,pov.price, pov.price_prefix FROM ".DB_PREFIX."order_option oo LEFT JOIN ".DB_PREFIX."order_product op ON (oo.order_product_id=op.order_product_id) LEFT JOIN ".DB_PREFIX."product_option_value pov ON (pov.product_option_value_id=oo.product_option_value_id) WHERE op.order_id = '".(int)$order_id."' ";
        $result = $this->db->query($sql)->rows;
        if($result) {
            return $result;
        } else {
            return false;
        }
    }

    public function getEnquiry($enquiry_id) {
        $result = $this->db->query("SELECT * FROM ".DB_PREFIX."preorder_enquiry WHERE enquiry_id = '".(int)$enquiry_id."' AND customer_id = '".(int)$this->customer->getId()."' ")->row;
        if($result) {
            return $result;
        } else {
            return false;
        }
    }

    public function getEnquiryThreads($enquiry_id) {
        $result = $this->db->query("SELECT * FROM ".DB_PREFIX."preorder_enquiry_threads WHERE enquiry_id = '".(int)$enquiry_id."' ")->rows;
        if($result) {
            return $result;
        } else {
            return false;
        }
    }

    public function getEnquiries($filter_data) {
        $sql = "SELECT pe.enquiry_id,pe.product_id,pe.email, pe.subject,pd.name, pe.customer_name, pe.status, (SELECT COUNT(thread_id) FROM ".DB_PREFIX."preorder_enquiry_threads pet WHERE pet.enquiry_id = pe.enquiry_id ) as total_thread FROM ".DB_PREFIX."preorder_enquiry pe LEFT JOIN ".DB_PREFIX."product_description pd ON (pd.product_id=pe.product_id) LEFT JOIN ".DB_PREFIX."customer c ON (c.customer_id=pe.customer_id) WHERE pd.language_id = '".(int)$this->config->get('config_language_id')."' AND pe.customer_id = '".(int)$filter_data['customer_id']."' ";

        if(isset($filter_data['filter_enquiry_id']) && $filter_data['filter_enquiry_id']) {
			$sql .= " AND pe.enquiry_id = '".(int)$filter_data['filter_enquiry_id']."' ";
		}

        if(isset($filter_data['filter_customer_name']) && $filter_data['filter_customer_name']) {
			$sql .= " AND pe.customer_name like '".$this->db->escape($filter_data['filter_customer_name'])."%' ";
		}

        if(isset($filter_data['filter_email']) && $filter_data['filter_email']) {
			$sql .= " AND pe.email like '".$this->db->escape($filter_data['filter_email'])."%' ";
		}

        if(isset($filter_data['filter_product_name']) && $filter_data['filter_product_name']) {
			$sql .= " AND pd.name like '".$this->db->escape($filter_data['filter_product_name'])."%' ";
		}

        if(isset($filter_data['filter_subject']) && $filter_data['filter_subject']) {
			$sql .= " AND pe.subject like '".$this->db->escape($filter_data['filter_subject'])."%' ";
		}

        if(isset($filter_data['filter_status']) && $filter_data['filter_status'] != '' ) {
			$sql .= " AND pe.status = '".(int)$filter_data['filter_status']."' ";
		}

        $sort_array = array(
            'pe.enquiry_id',
            'pe.customer_name',
            'pe.email',
            'pd.name',
            'pe.subject',
            'pe.status'
        );

		if (isset($filter_data['sort'])) {
			$sql .= " ORDER BY " . $filter_data['sort'];
		} else {
			$sql .= " ORDER BY pe.enquiry_id";
		}

		if (isset($filter_data['order']) && ($filter_data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($filter_data['start']) || isset($filter_data['limit'])) {
			if ($filter_data['start'] < 0) {
				$filter_data['start'] = 0;
			}

			if ($filter_data['limit'] < 1) {
				$filter_data['limit'] = 20;
			}
			$sql .= " LIMIT " . (int)$filter_data['start'] . "," . (int)$filter_data['limit'];

		}

        $result = $this->db->query($sql)->rows;
        if($result) {
            return $result;
        } else {
            return false;
        }
    }

    public function getEnquiriesTotal($filter_data) {
        $sql = "SELECT pe.enquiry_id,pe.product_id,pe.email, pe.subject,pd.name, pe.customer_name, pe.status, (SELECT COUNT(thread_id) FROM ".DB_PREFIX."preorder_enquiry_threads pet WHERE pet.enquiry_id = pe.enquiry_id ) as total_thread FROM ".DB_PREFIX."preorder_enquiry pe LEFT JOIN ".DB_PREFIX."product_description pd ON (pd.product_id=pe.product_id) LEFT JOIN ".DB_PREFIX."customer c ON (c.customer_id=pe.customer_id) WHERE pd.language_id = '".(int)$this->config->get('config_language_id')."' AND pe.customer_id = '".(int)$filter_data['customer_id']."' ";

        if(isset($filter_data['filter_enquiry_id']) && $filter_data['filter_enquiry_id']) {
			$sql .= " AND pe.enquiry_id = '".(int)$filter_data['filter_enquiry_id']."' ";
		}

        if(isset($filter_data['filter_customer_name']) && $filter_data['filter_customer_name']) {
			$sql .= " AND pe.customer_name like '".$this->db->escape($filter_data['filter_customer_name'])."%' ";
		}

        if(isset($filter_data['filter_email']) && $filter_data['filter_email']) {
			$sql .= " AND pe.email like '".$this->db->escape($filter_data['filter_email'])."%' ";
		}

        if(isset($filter_data['filter_product_name']) && $filter_data['filter_product_name']) {
			$sql .= " AND pd.name like '".$this->db->escape($filter_data['filter_product_name'])."%' ";
		}

        if(isset($filter_data['filter_subject']) && $filter_data['filter_subject']) {
			$sql .= " AND pe.subject like '".$this->db->escape($filter_data['filter_subject'])."%' ";
		}

        if(isset($filter_data['filter_status']) && $filter_data['filter_status'] != '' ) {
			$sql .= " AND pe.status = '".(int)$filter_data['filter_status']."' ";
		}

        $result = $this->db->query($sql)->rows;
        if($result) {
            return count($result);
        } else {
            return false;
        }
    }

}

?>
