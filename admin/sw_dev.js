

self.addEventListener('install', function(e) {
    console.log('Installing Service Worker');
  });

  self.addEventListener('activate', function(e) {
    console.log('Activating Service Worker');
    return self.clients.claim();
  });
  
