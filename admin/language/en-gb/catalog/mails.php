<?php
$_['heading_title']      	= 'Pre-orders Mails';

// Text
$_['text_module']         = 'Modules';
$_['text_success']       	= 'Success - Successfully Saved Mail.';
$_['text_update']        	= 'Success - Successfully Updated Mail.';
$_['text_success_del']   	= 'Success - Successfully Deleted.';
$_['text_edit']           = 'Edit';
$_['text_confirm']        = 'Are you sure';

// Button
$_['button_save']			    = 'Save';
$_['button_back']			    = 'Back';
$_['button_cancel']			  = 'Cancel';
$_['button_insert']			  = 'Add';
$_['button_delete']			  = 'Delete';
$_['button_filter']			  = 'Filter';
$_['button_clrfilter']		= 'Clear Filter';

// Entry
$_['entry_id']        		= 'Id';
$_['entry_name']      		= 'Name';
$_['entry_name_info']     = 'Name which will display at time of selection.';
$_['entry_message']       = 'Message';
$_['entry_message_info']  = 'Add Mail Message';
$_['entry_subject']      	= 'Subject';
$_['entry_subject_info']  = 'Add Mail Subject';
$_['entry_no_records']    = 'No Records Found !';
$_['entry_action']     		= 'Action';

// Error
$_['error_name']          = 'Warning - Please add Name for this mail message !!';
$_['error_subject']    		= 'Warning - Please add Subject for mail, character must between 5 to 1000 !!';
$_['error_message']    		= 'Warning - Please add Message for mail, character must between 25 to 5000 !!';

// Info
$_['info_mail']			      = ' You can read Info for more details.';
$_['info_mail_add']			  = 'Here about pre-order mail';
$_['tab_info']      	 	  = 'Info';
$_['tab_general']         = 'Add Message';
$_['entry_for']           = 'For';
$_['entry_code']      	  = 'Keyword';


$_['error_permission']    = 'Warning: You do not have permission to modify mails!';
