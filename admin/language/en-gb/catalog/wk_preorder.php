<?php
#####################################################################
#  Preorder Pro Opencart 2.x.x.x From Webkul  http://webkul.com 	#
#####################################################################

// Heading
$_['heading_title']      	                = 'Pre-orders list';
$_['heading_title_enquiry_list']            = 'Pre-orders enquiry list';
$_['heading_title_order'] 	                = 'Order - PreOrder';
$_['heading_title_productlist']             = 'Preorder Product list';
$_['heading_title_productadd']              = 'Add Preorder Product';
$_['heading_title_productedit']             = 'Edit Preorder Product';
$_['heading_title_enquiry']                 = 'Enquiry Details';

// Text
$_['text_action']    		                = 'Action';
$_['text_ordered']    		                = 'Ordered';
$_['text_preordered']    	                = 'Pre Order Enquiry';
$_['text_name']      	                    = 'Customer Name';
$_['text_email']     	 	                = 'Email';
$_['text_subject']     	 	                = 'Subject';
$_['text_total_thread']   	                = 'Total Thread(s)';
$_['text_product_name'] 	                = 'Product Name';
$_['text_query']   	  		                = 'Query';
$_['text_action']   	                    = 'Action';
$_['text_products']			                = 'Products';
$_['text_notifyc']   	  	                = 'Notify Customer';
$_['text_cus_email']   	  	                = 'Customer Email';
$_['text_notified']   	  	                = 'Notified';
$_['button_clrfilter']   	                = 'Clear Filter';
$_['button_convert']                        = 'Convert to Pre-Order';
$_['text_quantity']   	  	                = 'Quantity';
$_['text_pname']   	 		                = 'Product Name';
$_['text_model']   	  	                    = 'Model';
$_['text_current_status']                   = 'Current status';
$_['text_status']    		                = 'Status';
$_['text_open']    		                    = '<span class="text-danger"><b>Open</b></span>';
$_['text_resolved']    		                = '<span class="text-success"><b>Resolved</b></span>';
$_['text_preorder_price']                   = 'Pre order price';
$_['text_deduction_type']                   = 'Deduction type';
$_['text_percentage']                       = 'Percentage';
$_['text_fixed']                            = 'Fixed';
$_['error_permission']  	                = ' Warning: You do not have permission to modify module Preorder Pro Module !!';

$_['text_pre_ordered']                      = '  Pre-ordered';
$_['text_full_paid']                        = '  Full paid';
$_['text_paid_amount']                      = 'Paid Amount';
$_['text_not_notified']                     = ' Not notified';
$_['text_notify']                           = ' Notify';
$_['text_notify_help']                      = 'Click to notifiy selected customers';
$_['text_order_id'] 	                    = 'Order Id';
$_['text_date']        		                = 'Order Date';
$_['text_total']      		                = 'Order Total Amount';
$_['text_view']      		                = 'View Order';
$_['text_update_info']    	                = 'If Preorder Send then Update Here.';

$_['text_notification_status']              = 'Notification Status';
$_['text_add_preorder_product']             = 'Add product';
$_['text_add_preorder_product_info']        = 'Select products which you want to become pre-order.';
$_['text_deduction_method'] 	            = 'Select Pre-order mode';
$_['text_deduction_method_info']            = 'To set what kind of pre-order will be for current products';

$_['text_fixed_rate']                       = 'Full';
$_['text_partial_rate']                     = 'Partial';

$_['text_product_price'] 	                = 'Add Price';
$_['text_product_price_info']               = 'Entered percentage of price of product will be taken initially and remaining when pre-order will be completed.';

$_['text_preorder_quantity'] 	            = 'Pre-Order Quantity';
$_['text_quantity_per_order'] 	            = 'Quantity per order';
$_['text_order_per_customer'] 	            = 'Order per customer';

$_['text_preorder_quantity_info'] 	        = 'How many total quantity of selected product will be on for pre-order';

$_['text_quantity_per_order_info'] 	        = 'How many product quantity can contain single order';

$_['text_order_per_customer_info'] 	        = 'How many order(s) a customer can place';

$_['text_one_ip_one_order'] 	            = 'One Ip one order';
$_['text_one_ip_one_order_info']            = 'This will enable ip tracking means if enabled then same ip address can not place multiple preorder';

$_['text_preorder_start_date']   	        = 'Start date for Pre Order';
$_['text_preorder_start_date_info']         = 'Please add date for all products when accepting preorder(s) will be start for customer.';
$_['text_preorder_date']   	                = 'Date for Pre Order';
$_['text_preorder_pro_available_date_info'] = 'Please add date for all products when they will available to customer.';
$_['text_no_thread']    	 	            = 'No thread found for this enquiry';
$_['text_confirmation']    	 	            = 'Are you sure?';
$_['text_discount']                         = 'Pre-Order Discount';
$_['text_discount_info']                    = 'Discount will be applied on preorder products only';

// Success or Error
$_['text_success']    	 	                = 'Success: You have modified and updated PreOrder !';
$_['text_success_deleteproduct']            = 'Success: You have successfully deleted selected product(s) from preorder product list.';
$_['text_success_delete_enquiry']           = 'Success: You have successfully deleted selected enquiry.';
$_['text_success_convert']                  = 'Success: You have successfully converted product(s) to preorder';
$_['text_delete_warning']    	 	        = 'Warning: Please select any option !';

$_['error_query_length']                    = 'Query must be between 10 to 500 character!';
$_['error_no_enquiry_id']                   = 'Enquiry id is missing, please try again later!';
$_['error_in_general']                      = 'Warning: There is some issue, please try again later!';
$_['error_no_preorder_product']             = 'Warning: No preorder product exist with given id, please try again later!';
$_['error_general']       	 	            = 'There is some issue, please try again later';
$_['error_warning']       	 	            = 'Warning: Please check the form, there must be some error!';
$_['error_no_product']    	 	            = 'Please select product(s)';
$_['error_no_percentage']    	 	        = 'In partial type of deduction, please enter some percentage for partial payment to done';
$_['error_start_date']         	 	        = 'Please mention start date';
$_['error_available_date']    	 	        = 'Please mention available date';
$_['text_no_record']    	 	            = 'No record found!';
$_['error_product_exist']                   = '%s - Product(s) already in pre-order list!';
$_['error_deleteproduct']                   = 'Warning: No product is selected to delete!';
$_['error_delete_enquiry']                  = 'Warning: No enquiry is selected to delete!';
$_['error_select']                          = 'Warning: Please select atleast one to delete!';
$_['error_status']                          = 'Warning: Please select the valid option!';
$_['error_till_date']                       = 'Warning: Till date should be grater than from date!';
$_['error_no_discount']                     = 'Warning: Please enter the valid amount!';
$_['error_no_discount_fixed']               = 'Warning: Please enter the amount till 10000!';



$_['quantity_err']    	 	                = 'Warning: Pre-Order Quantity is required and must be between 1 and 500';
$_['quantity_per_oerr']    	 	            = 'Warning: Quantity per order is required and must be between 1 and 500';
$_['order_les_error']    	 	            = 'Quantity per order must be less than pre-order quantity';
$_['per_cust_error']    	 	            = 'Warning: Order per customer is required and must be between 1 and 500';

$_['text_success_update']                   = 'Success: You have updated PreOrder !';
$_['text_success_delete']                   = 'Success: You have successfully deleted customer Pre order !';
$_['text_success_notify']                   = 'Success: You have successfully notified customer about Pre order !';

$_['success_thread_delete']                 = 'Success: Thread to the enquiry is deleted successfully.';
$_['success_thread_added']                  = 'Success: Thread to the enquiry is added successfully.';
$_['success_product_update']                = 'Success: Product is successfully updated.';
$_['success_added']    				        = 'Success: %s is successfully added for pre-order.';
$_['error_already_added']                   = 'Warning: %s is already added for pre-order!';
$_['error_failure']    				        = 'Error: %s can not be added for pre-order, please try again!';

//user
$_['star']    				                = '<span style="color:red;"> * </span>';
$_['dept_name']    			  	            = 'Department Name ';
$_['no_staff']    			 	            = 'No. of staff in Department ';
$_['text_update']    			            = 'Enable';
$_['text_disabled']    			            = 'Disable';

// Entry
$_['button_filter']    	                    = 'Filter';
$_['button_clear_filter']                   = 'Clear filter';
$_['button_update']    	                    = 'Update Order';
$_['button_save']    	                    = 'Save';
$_['button_cancel']    	                    = 'Cancel';
$_['button_delete']    	                    = 'Delete';
$_['text_edit']    	          	            = 'Edit';
