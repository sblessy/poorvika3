<?php
class ControllerExtensionModuleWhatsapp extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('extension/module/whatsapp');
    
	 	$this->document->setTitle($this->language->get('heading_title'));
	 

		$this->load->model('extension/module');
		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			if (!isset($this->request->get['module_id'])) {
				$this->model_extension_module->addModule('whatsapp', $this->request->post);
			} else {
				$this->model_extension_module->editModule($this->request->get['module_id'], $this->request->post);
			}
			
			if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
				$this->model_setting_setting->editSetting('whatsapp', $this->request->post);

				$this->session->data['success'] = $this->language->get('text_success');

				$this->response->redirect($this->url->link('extension/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
			}


			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
		}


		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_share'] = $this->language->get('entry_share');
		$data['entry_Chat'] = $this->language->get('entry_Chat');
		$data['entry_cphone'] = $this->language->get('entry_cphone');
		$data['entry_chatmsg'] = $this->language->get('entry_chatmsg');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_order'] = $this->language->get('entry_order');
		$data['entry_ophone'] = $this->language->get('entry_ophone');
		$data['entry_ordermsg'] = $this->language->get('entry_ordermsg');
		
		
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('extension/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
		);

		if (!isset($this->request->get['module_id'])) {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('extension/module/whatsapp', 'user_token=' . $this->session->data['user_token'], true)
			);
		} else {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('extension/module/whatsapp', 'user_token=' . $this->session->data['user_token'] . '&module_id=' . $this->request->get['module_id'], true)
			);
		}

		if (!isset($this->request->get['module_id'])) {
			$data['action'] = $this->url->link('extension/module/whatsapp', 'user_token=' . $this->session->data['user_token'], true);
		} else {
			$data['action'] = $this->url->link('extension/module/whatsapp', 'user_token=' . $this->session->data['user_token'] . '&module_id=' . $this->request->get['module_id'], true);
		}

		$data['cancel'] = $this->url->link('extension/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);

		if (isset($this->request->get['module_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$module_info = $this->model_extension_module->getModule($this->request->get['module_id']);
		}

		$data['user_token'] = $this->session->data['user_token'];

		if (isset($this->request->post['name'])) {
			$data['name'] = $this->request->post['name'];
		} elseif (!empty($module_info)) {
			$data['name'] = $module_info['name'];
		} else {
			$data['name'] = '';
		}
	
		if (isset($this->request->post['whatsapp_share'])) {
			$data['whatsapp_share'] = $this->request->post['whatsapp_share'];
		} else {
			$data['whatsapp_share'] = $this->config->get('whatsapp_share');
		} 
		if (isset($this->request->post['whatsapp_chat'])) {
			$data['whatsapp_chat'] = $this->request->post['whatsapp_chat'];
		} else {
			$data['whatsapp_chat'] = $this->config->get('whatsapp_chat');
		} 
		if (isset($this->request->post['whatsapp_cphone'])) {
			$data['whatsapp_cphone'] = $this->request->post['whatsapp_cphone'];
		} else {
			$data['whatsapp_cphone'] = $this->config->get('whatsapp_cphone');
		} 
		if (isset($this->request->post['whatsapp_cmsg'])) {
			$data['whatsapp_cmsg'] = $this->request->post['whatsapp_cmsg'];
		} else {
			$data['whatsapp_cmsg'] = $this->config->get('whatsapp_cmsg');
		}
		if (isset($this->request->post['whatsapp_order'])) {
			$data['whatsapp_order'] = $this->request->post['whatsapp_order'];
		} else {
			$data['whatsapp_order'] = $this->config->get('whatsapp_order');
		} 
		if (isset($this->request->post['whatsapp_ophone'])) {
			$data['whatsapp_ophone'] = $this->request->post['whatsapp_ophone'];
		} else {
			$data['whatsapp_ophone'] = $this->config->get('whatsapp_ophone');
		} 
		if (isset($this->request->post['whatsapp_omsg'])) {
			$data['whatsapp_omsg'] = $this->request->post['whatsapp_omsg'];
		} else {
			$data['whatsapp_omsg'] = $this->config->get('whatsapp_omsg');
		} 
		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($module_info)) {
			$data['status'] = $module_info['status'];
		} else {
			$data['status'] = '';
		}
		

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/whatsapp', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/whatsapp')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 64)) {
			$this->error['name'] = $this->language->get('error_name');
		}

		return !$this->error;
	}
}
