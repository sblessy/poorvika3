<?php
################################################################################################
#  Preorder Pro Opencart 2.x.x.x From Webkul  http://webkul.com   #
################################################################################################

class ControllerCatalogWkPreorderedlist extends Controller
{

  private $error = array();
	private $data = array();

	public function __construct($registry) {
		parent::__construct($registry);
		$registry->get('document')->addScript('view/javascript/jscolor/js/colorpicker.js');
		$registry->get('document')->addScript('view/javascript/jscolor/css/colorpicker.css');

    $this->data = array_merge($this->data, $this->load->language('catalog/wk_preorder'));
    $this->load->model('extension/module/wk_preorder_pro');
    $this->load->model('catalog/product');
	}
  public function index(){

      if(!$this->config->get('module_wk_preorder_pro_status')) {
        $this->response->redirect($this->url->link('common/dashboard','&user_token=' . $this->session->data['user_token'],true));
      }

      $this->document->setTitle($this->language->get('heading_title'));

      $this->data['user_token'] = $this->session->data['user_token'];

      if(isset($this->request->get['sort'])) {
        $this->data['sort'] = $this->request->get['sort'];
      } else {
        $this->data['sort'] = 'ppd.order_id';
      }

      if(isset($this->request->get['order'])) {
        $this->data['order'] = $this->request->get['order'];
      } else {
        $this->data['order'] = 'ASC';

      }

      if(isset($this->request->get['pname'])) {
        $this->data['pname'] = $this->request->get['pname'];
      } else {
        $this->data['pname'] = null;
      }

      if(isset($this->request->get['name'])) {
        $this->data['name'] = $this->request->get['name'];
      } else {
        $this->data['name'] = null;
      }

      if(isset($this->request->get['customer_mail'])) {
        $this->data['customer_mail'] = $this->request->get['customer_mail'];
      } else {
        $this->data['customer_mail'] = null;
      }

      if(isset($this->request->get['notified'])) {
        if($this->request->get['notified']){
          $this->data['notified'] = $notified = 'yes';
        }else{
          $this->data['notified'] = $notified = 'no';
        }
      } else {
        $this->data['notified'] = null;
      }

      if(isset($this->request->get['status'])) {
        if($this->request->get['status']){
          $this->data['status'] = $status = 'yes';
        }else{
          $this->data['status'] = $status = 'no';
        }
      } else {
        $status = null;
      }

      if (isset($this->request->get['page'])) {
        $this->data['page'] = $page = $this->request->get['page'];
      } else {
        $page = 1;
      }
      $url = '';
      $url = $this->setRequestgetVar('');
      $filter_data = array(
        'sort'          => $this->data['sort'],
        'order'         => $this->data['order'],
        'name'          => $this->data['name'],
        'pname'         => $this->data['pname'],
        'notified'      => $this->data['notified'],
        'customer_mail' => $this->data['customer_mail'],
        'status'        => $status,
        'start'         => ($page - 1) * $this->config->get('config_limit_admin'),
        'limit'         => $this->config->get('config_limit_admin')
      );

      $this->data['breadcrumbs'] = array();

      $this->data['breadcrumbs'][] = array(
          'text'      => $this->language->get('text_home'),
      'href'      => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true),
      );

      $this->data['breadcrumbs'][] = array(
          'text'      => $this->language->get('heading_title'),
      'href'      => $this->url->link('catalog/wk_preordered_list', 'user_token=' . $this->session->data['user_token'] . $url , true),
      );

      $this->data['notify'] = $this->url->link('catalog/wk_preordered_list/notify_preorder_product', 'user_token=' . $this->session->data['user_token'] . $url, true);

      $this->data['delete'] = $this->url->link('catalog/wk_preordered_list/delete_preorder_order' , 'user_token=' . $this->session->data['user_token'] . $url,true);

      $url = '';
      $url = $this->setRequestgetVar('sort');

      $this->data['sort_orderid'] = $this->url->link('catalog/wk_preordered_list', 'user_token=' . $this->session->data['user_token'] . '&sort=ppd.order_id' . $url, true);

      $this->data['sort_pname'] = $this->url->link('catalog/wk_preordered_list', 'user_token=' . $this->session->data['user_token'] . '&sort=pd.name' . $url, true);

      $this->data['sort_cname'] = $this->url->link('catalog/wk_preordered_list', 'user_token=' . $this->session->data['user_token'] . '&sort=c.firstname' . $url, true);

      $this->data['sort_cus_mail'] = $this->url->link('catalog/wk_preordered_list', 'user_token=' . $this->session->data['user_token'] . '&sort=c.email' . $url, true);

      $this->data['sort_notify'] = $this->url->link('catalog/wk_preordered_list', 'user_token=' . $this->session->data['user_token'] . '&sort=ppd.notified' . $url, true);

      $this->data['sort_status'] = $this->url->link('catalog/wk_preordered_list', 'user_token=' . $this->session->data['user_token'] . '&sort=ppd.status' . $url, true);

      $this->data['sort_paid_amount'] = $this->url->link('catalog/wk_preordered_list', 'user_token=' . $this->session->data['user_token'] . '&sort=oo.total' . $url, true);

      if (isset($this->error['warning'])) {
        $this->data['error_warning'] = $this->error['warning'];
      } else if (isset($this->session->data['error_warning'])) {
        $this->data['error_warning'] = $this->session->data['error_warning'];
        unset($this->session->data['error_warning']);
      } else {
        $this->data['error_warning'] = '';
      }


    if (isset($this->session->data['success'])) {
      $this->data['success'] = $this->session->data['success'];
      unset($this->session->data['success']);
    } else {
      $this->data['success'] = '';
    }

      $product_total = $this->model_extension_module_wk_preorder_pro->get_total_productcount($filter_data);
      $orders = $this->model_extension_module_wk_preorder_pro->get_Preordered_list($filter_data);

      if($orders){
        foreach ($orders as $key => $order) {
          $this->data['orders'][] = array(
            'id'  => $order['order_id'],
            'order_id'  => $order['order_id'],
            'product_id' => $order['product_id'],
            'product_name' => $order['name'],
            'customer_name' => $order['firstname']." ".$order['lastname'],
            'email' => $order['email'],
            'notified' => $order['notified'],
            'status' => $order['status'],
            'preorder_price' => $this->currency->format($order['total'], $this->config->get('config_currency')),
          );
        }
      }

      if($this->config->get('module_wk_preorder_pro_notification_mode') && $this->config->get('module_wk_preorder_pro_notification_mode') == 1){
        $this->data['notification_status'] = false;
      }else{
        $this->data['notification_status'] = true;
      }

      $this->data['clear_filter'] = $this->url->link('catalog/wk_preordered_list', 'user_token='.$this->session->data['user_token'], true);

      $url = '';
		  $url = $this->setRequestgetVar('page');
      $pagination = new Pagination();
      $pagination->total = $product_total;
      $pagination->page = $page;
      $pagination->limit = $this->config->get('config_limit_admin');
      $pagination->url = $this->url->link('catalog/wk_preordered_list', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

      $this->data['pagination'] = $pagination->render();

      $this->data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($product_total - $this->config->get('config_limit_admin'))) ? $product_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $product_total, ceil($product_total / $this->config->get('config_limit_admin')));

      $this->data['header'] = $this->load->Controller('common/header');
      $this->data['footer'] = $this->load->Controller('common/footer');
      $this->data['column_left'] = $this->load->Controller('common/column_left');

      $this->response->setOutput($this->load->view('catalog/wk_preordered_list',$this->data));
    }

    public function delete_preorder_order(){
      $url = '';
      $url = $this->setRequestgetVar('');
      if($this->validateForm()) {
        if (isset($this->request->post['selected'])) {
          foreach ($this->request->post['selected'] as $id) {
            $this->model_extension_module_wk_preorder_pro->deleteOrder($id);
            }
          $this->session->data['success'] = $this->language->get('text_success_delete');
        }else{
          $this->session->data['error_warning'] = $this->language->get('text_delete_warning');

        }
      } else {
        $this->session->data['error_warning'] = $this->error['warning'];
      }
     
      $this->response->redirect($this->url->link('catalog/wk_preordered_list', 'user_token=' . $this->session->data['user_token'] . $url, true));
    }

    public function notify_preorder_product() {
      $url = '';
      $url = $this->setRequestgetVar('');
      if($this->validateForm()) {
        if (isset($this->request->post['selected'])) {
          foreach ($this->request->post['selected'] as $id) {
            $this->model_extension_module_wk_preorder_pro->emailToCustomer($id,'order');
          }

          $this->session->data['success'] = $this->language->get('text_success_notify');
        }else{
          $this->session->data['error_warning'] = $this->language->get('text_delete_warning');
        }
      } else {
        $this->session->data['error_warning'] = $this->error['warning'];
      }
      $this->response->redirect($this->url->link('catalog/wk_preordered_list', 'user_token=' . $this->session->data['user_token'] . $url, true));
    }

    private function validateForm() {
      if (!$this->user->hasPermission('modify', 'catalog/wk_preordered_list')) {
        $this->error['warning'] = $this->language->get('error_permission');
      }
      return !$this->error;
    }

    public function autocomplete() {
      $this->load->model('catalog/product');

			if (isset($this->request->get['filter_name'])) {
				$filter_name = $this->request->get['filter_name'];
			} else {
				$filter_name = '';
			}

			if (isset($this->request->get['filter_model'])) {
				$filter_model = $this->request->get['filter_model'];
			} else {
				$filter_model = '';
			}

			if (isset($this->request->get['limit'])) {
				$limit = $this->request->get['limit'];
			} else {
				$limit = 5;
			}

			$filter_data = array(
				'filter_name'  => $filter_name,
				'filter_model' => $filter_model,
				'start'        => 0,
				'limit'        => $limit
			);

			$results = $this->model_catalog_product->getProducts($filter_data);

			foreach ($results as $result) {
        if (!$this->model_extension_module_wk_preorder_pro->getPreOrderProductExists($result['product_id'])) {
          $json[] = array(
            'product_id' => $result['product_id'],
            'name'       => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
            'model'      => $result['model'],
            'price'      => $result['price']
          );
        }

      }
		  $this->response->addHeader('Content-Type: application/json');
		  $this->response->setOutput(json_encode($json));
    }
    public function autocompleteFilter() {
      $this->load->model('catalog/product');

			if (isset($this->request->get['filter_pname'])) {
				$filter_pname = $this->request->get['filter_pname'];
			} else {
				$filter_pname = '';
			}

			if (isset($this->request->get['filter_name'])) {
				$filter_name = $this->request->get['filter_name'];
			} else {
				$filter_name = '';
      }

      if (isset($this->request->get['filter_customer_mail'])) {
				$filter_customer_mail = $this->request->get['filter_customer_mail'];
			} else {
				$filter_customer_mail = '';
      }

			if (isset($this->request->get['limit'])) {
				$limit = $this->request->get['limit'];
			} else {
				$limit = 5;
			}

			$filter_data = array(
				'pname'  => $filter_pname,
        'name' => $filter_name,
        'customer_mail' => $filter_customer_mail,
				'start'        => 0,
				'limit'        => $limit
			);

			$results = $this->model_extension_module_wk_preorder_pro->get_Preordered_list($filter_data);

			foreach ($results as $result) {
          $json[] = array(
            'product_id' => $result['product_id'],
            'pname'       => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
            'name'        => strip_tags(html_entity_decode($result['firstname'] . ' ' . $result['firstname'], ENT_QUOTES, 'UTF-8')),
            'email'      => $result['email'],
          );
      }
		  $this->response->addHeader('Content-Type: application/json');
		  $this->response->setOutput(json_encode($json));
    }

    public function setRequestgetVar($type = '') {

      $order = 'ASC';

      if(isset($this->request->get['order']) && $this->request->get['order'])
        $order = $this->request->get['order'];

      //setting get variable in URL code starts here
      $url = '';

      $uri_component = array(
        'name',
        'pname',
        'customer_mail',
        'notified',
        'status',
        'sort',
        'page',
      );

      switch ($type) {
        case 'page':
        foreach ($uri_component as $key => $value) {
          if($value != 'page')
          $url .=  $this->ocutilities->_setStringURLs($value);
        }
        break;

        case 'sort':
          foreach ($uri_component as $key => $value) {
          if($value != 'sort')
          $url .=  $this->ocutilities->_setStringURLs($value);
          }
          if ($order == 'ASC') {
          $order = 'DESC';
          } else {
          $order = 'ASC';
          }
        break;

        default:
        foreach ($uri_component as $key => $value) {
          $url .=  $this->ocutilities->_setStringURLs($value);
        }
        break;
      }

      $url .= '&order=' . $order;

      //setting get variable in URL code ends here

      return $url;
    }
}
