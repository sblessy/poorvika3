<?php
class ControllerCatalogMails extends Controller{
  private $error = array();
  private $data = array();

  public function index(){
    $this->getlist();
  }

  public function getlist(){
    if(!$this->config->get('module_wk_preorder_pro_status')) {
      $this->response->redirect($this->url->link('common/dashboard','&user_token=' . $this->session->data['user_token'],true));
    }
    $this->load->language('catalog/mails');
	$this->document->setTitle($this->language->get('heading_title'));
    $data['button_insert'] = $this->language->get('button_insert');
    $data['button_delete'] = $this->language->get('button_delete');
    $data['button_filter'] = $this->language->get('button_filter');
    $data['button_clrfilter'] = $this->language->get('button_clrfilter');
    $data['heading_title'] = $this->language->get('heading_title');
    $data['entry_id']      = $this->language->get('entry_id');
    $data['entry_name']    = $this->language->get('entry_name');
    $data['entry_subject'] = $this->language->get('entry_subject');
    $data['entry_message'] = $this->language->get('entry_message');
    $data['entry_action'] = $this->language->get('entry_action');
    $data['text_edit'] = $this->language->get('text_edit');
    $data['entry_no_records'] = $this->language->get('entry_no_records');
    $data['text_confirm'] = $this->language->get('text_confirm');

    $this->load->model('catalog/mail');


    $filter_array = array(
							  'filter_id',
							  'filter_name',
							  'filter_message',
							  'filter_subject',
							  'page',
							  'sort',
							  'order',
							  'start',
							  'limit',
							  );

		$url = '';

		foreach ($filter_array as $unsetKey => $key) {

			if (isset($this->request->get[$key])) {
				$filter_array[$key] = $this->request->get[$key];
			} else {
				if ($key=='page')
					$filter_array[$key] = 1;
				elseif($key=='sort')
					$filter_array[$key] = 'cc.id';
				elseif($key=='order')
					$filter_array[$key] = 'ASC';
				elseif($key=='start')
					$filter_array[$key] = ($filter_array['page'] - 1) * $this->config->get('config_limit_admin');
				elseif($key=='limit')
					$filter_array[$key] = $this->config->get('config_limit_admin');
				else
					$filter_array[$key] = null;
			}
			unset($filter_array[$unsetKey]);

			if(isset($this->request->get[$key])){
				if ($key=='filter_name' || $key=='filter_message' || $key=='filter_subject')
					$url .= '&'.$key.'=' . urlencode(html_entity_decode($filter_array[$key], ENT_QUOTES, 'UTF-8'));
				else
					$url .= '&'.$key.'='. $filter_array[$key];
			}
		}

    $data['user_token'] = $this->session->data['user_token'];
 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}
    $data['breadcrumbs'] = array();
    $data['breadcrumbs'][] = array(
      'text' => 'Home',
      'href' => $this->url->link('common/dashboard' , 'user_token='.$this->session->data['user_token'].$url , true)
    );
    $data['breadcrumbs'][] = array(
      'text' => 'Mails',
      'href' => $this->url->link('catalog/mails' , 'user_token='.$this->session->data['user_token'].$url , true)
    );
    $data['delete'] = $this->url->link('catalog/mails/delete', 'user_token=' . $this->session->data['user_token'] , 'true');
    $data['insert'] = $this->url->link('catalog/mails/addmail', 'user_token=' . $this->session->data['user_token'] , 'true');


    $results = $this->model_catalog_mail->viewtotal($filter_array);
		$product_total = $this->model_catalog_mail->viewtotalentry($filter_array);

		$data['mails'] = array();
	    foreach ($results as $result) {
	      $data['mails'][] = array(
				'selected'=>False,
				'id' => $result['id'],
				'name' => $result['name'],
				'message' => html_entity_decode($result['message']),
				'subject' => $result['subject'],
				'action' => $this->url->link('catalog/mails/addMail', 'user_token=' . $this->session->data['user_token'] . '&mail_id=' . $result['id'], 'true'),
			);
		}

    $url = '';

		foreach ($filter_array as $key => $value) {
			if(isset($this->request->get[$key])){
				if(!isset($this->request->get['order']) AND isset($this->request->get['sort']))
					$url .= '&order=DESC';
				if ($key=='filter_name' || $key=='filter_message' || $key=='filter_subject')
					$url .= '&'.$key.'=' . urlencode(html_entity_decode($filter_array[$key], ENT_QUOTES, 'UTF-8'));
				elseif($key=='order')
					$url .= $value=='ASC' ? '&order=DESC' : '&order=ASC';
				elseif($key!='start' AND $key!='limit' AND $key!='sort')
					$url .= '&'.$key.'='. $filter_array[$key];
			}
		}

		$data['sort_name'] = $this->url->link('catalog/mails', 'user_token=' . $this->session->data['user_token'] . '&sort=name' . $url, 'true');
		$data['sort_id'] = $this->url->link('catalog/mails', 'user_token=' . $this->session->data['user_token'] . '&sort=id' . $url, 'true');
		$data['sort_message'] = $this->url->link('catalog/mails', 'user_token=' . $this->session->data['user_token'] . '&sort=message' . $url, 'true');
		$data['sort_subject'] = $this->url->link('catalog/mails', 'user_token=' . $this->session->data['user_token'] . '&sort=subject' . $url, 'true');

		$url = '';

		foreach ($filter_array as $key => $value) {
			if(isset($this->request->get[$key])){
				if(!isset($this->request->get['order']) AND isset($this->request->get['sort']))
					$url .= '&order=DESC';
				if ($key=='filter_name' || $key=='filter_message' || $key=='filter_subject')
					$url .= '&'.$key.'=' . urlencode(html_entity_decode($filter_array[$key], ENT_QUOTES, 'UTF-8'));
				elseif($key!='page')
					$url .= '&'.$key.'='. $filter_array[$key];
			}
		}


    $pagination = new Pagination();
		$pagination->total = $product_total;
		$pagination->page = $filter_array['page'];
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('catalog/mails', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', 'true');

		$data['pagination'] = $pagination->render();
		$data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($filter_array['page'] - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($filter_array['page'] - 1) * $this->config->get('config_limit_admin')) > ($product_total - $this->config->get('config_limit_admin'))) ? $product_total : ((($filter_array['page'] - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $product_total, ceil($product_total / $this->config->get('config_limit_admin')));

		foreach ($filter_array as $key => $value) {
			if($key!='start' AND $key!='end')
				$data[$key] = $value;
		}


    $data['header'] = $this->load->Controller('common/header');
		$data['column_left'] = $this->load->Controller('common/column_left');
		$data['footer'] = $this->load->Controller('common/footer');
    $this->response->setOutput($this->load->view('catalog/mail_list' , $data));
  }

  public function addmail(){

    if(!$this->config->get('module_wk_preorder_pro_status')) {
      $this->response->redirect($this->url->link('common/dashboard','&user_token=' . $this->session->data['user_token'],true));
    }

    $this->load->language('catalog/mails');
    $this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('catalog/mail');
    $lang_array = array('heading_title',
							'entry_message',
							'entry_message_info',
							'entry_name',
							'entry_name_info',
							'entry_subject',
							'entry_subject_info',
							'entry_for',
							'entry_code',
							'info_mail',
							'info_mail_add',
							'button_back',
							'button_save',
							'button_cancel',
							'button_insert',
							'button_delete',
							'button_filter',
							'tab_general',
							'tab_info',
							);

		foreach($lang_array as $language){
			$data[$language] = $this->language->get($language);
		}

		$post_data = array(
							'mail_id',
							'name',
							'message',
							'subject',
							);

		foreach($post_data as $post){
			if(isset($this->request->post[$post])){
				$data[$post] = $this->request->post[$post];
			}else{
				$data[$post] = '';
			}
		}

		if(isset($this->request->get['mail_id'])){
    		$result = $this->model_catalog_mail->getMailData($this->request->get['mail_id']);
    		if($result)
    			foreach($result as $key => $value){
    				$data[$key] = $value;
    			}
    		$data['mail_id'] = $this->request->get['mail_id'];
		}


		$data['user_token'] = $this->session->data['user_token'];

 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$url = '';

		$data['breadcrumbs'] = array();
 		$data['breadcrumbs'][] = array(
     		'text'      => $this->language->get('text_home'),
				'href'      => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], 'true'),
    		'separator' => false
 		);

 	  $data['breadcrumbs'][] = array(
      'text'      => $this->language->get('heading_title'),
  		'href'      => $this->url->link('catalog/mails', 'user_token=' . $this->session->data['user_token'] . $url, 'true'),
      'separator' => ' :: '
 		);

    $data['cancel'] = $this->url->link('catalog/mails', 'user_token=' . $this->session->data['user_token'] . $url, 'true');
		$data['save'] = $this->url->link('catalog/mails/mailSave', 'user_token=' . $this->session->data['user_token'] . $url, 'true');

		$mailValues = str_replace('<br />', ',', nl2br($this->config->get('module_wk_preorder_pro_mail_keywords')));
		$mailValues = explode(',', $mailValues);
		$find = array();
		foreach ($mailValues as $key => $value) {
			$find[] = trim($value);
		}

		$data['mail_help'] = $find;
		$data['header'] = $this->load->controller('common/header');
		$data['footer'] = $this->load->controller('common/footer');
		$data['column_left'] = $this->load->controller('common/column_left');
    $this->response->setOutput($this->load->view('catalog/mail_form' , $data));
  }

  public function mailSave() {
    if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

      $this->load->language('catalog/mails');
      $this->load->model('catalog/mail');

      if ((utf8_strlen(trim($this->request->post['message'])) < 25) || (utf8_strlen(trim($this->request->post['message'])) > 5000)) {
        $this->error['warning'] = $this->language->get('error_message');
      }

      if ((utf8_strlen(trim($this->request->post['subject'])) < 5) || (utf8_strlen(trim($this->request->post['subject'])) > 1000)) {
        $this->error['warning'] = $this->language->get('error_subject');
      }

      if ((utf8_strlen(trim($this->request->post['name'])) < 1)) {
        $this->error['warning'] = $this->language->get('error_name');
      }

      if(!isset($this->error['warning'])){
        $this->model_catalog_mail->addMail($this->request->post);
        if($this->request->post['mail_id'])
        $this->session->data['success'] = $this->language->get('text_update');
        else
        $this->session->data['success'] = $this->language->get('text_success');
        $this->response->redirect($this->url->link('catalog/mails', 'user_token=' . $this->session->data['user_token'], 'true'));
      }
    }
    $this->addmail();
  }

	public function delete() {

    	$this->language->load('catalog/mails');
    	$this->document->setTitle($this->language->get('heading_title'));
		  $this->load->model('catalog/mail');

		  if ($this->validate()) {
			if (isset($this->request->post['selected'])) {
				foreach ($this->request->post['selected'] as $id) {
					$this->model_catalog_mail->deleteentry($id);
			  	}
				$this->session->data['success'] = $this->language->get('text_success_del');
				$url='';
				if (isset($this->request->get['page'])) {
					$url .= '&page=' . $this->request->get['page'];
				}
  
				$this->response->redirect($this->url->link('catalog/mails', 'user_token=' . $this->session->data['user_token'] . $url, 'true'));
			}
		  } else {
			$this->session->data['error_warning'] = $this->language->get('error_permission');
		  }
  		
        $this->index();
    	}

  private function validate() {
	$this->load->language('catalog/mails');
  		if (!$this->user->hasPermission('modify', 'catalog/mails')) {
  			$this->error['warning'] = $this->language->get('error_permission');
  		}
  		return !$this->error;
	}
}
