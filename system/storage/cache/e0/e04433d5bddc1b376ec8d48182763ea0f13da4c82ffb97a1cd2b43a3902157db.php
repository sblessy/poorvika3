<?php

/* so-destino/template/header/header5.twig */
class __TwigTemplate_22326e4b4df9d90e7ff6e4f4d98588c9ce7c937e8b3f523868164fb14320fe68 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $context["hidden_headercenter"] = ((($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "toppanel_type"), "method") == "2")) ? ("hidden-compact") : (""));
        // line 3
        $context["hidden_headerbottom"] = ((($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "toppanel_type"), "method") == "1")) ? ("hidden-compact") : (""));
        // line 4
        echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/css/owl.carousel.min.css\" />
<link rel=\"stylesheet\" type=\"text/css\" href=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/css/owl.theme.default.min.css\" />
<header id=\"header scrollHeader\" class=\"sticky variant typeheader-";
        // line 6
        echo (((isset($context["typeheader"]) ? $context["typeheader"] : null)) ? ((isset($context["typeheader"]) ? $context["typeheader"] : null)) : ("1"));
        echo "\">
\t<!-- HEADER TOP -->
\t<div class=\"header-top compact-hidden custom\">
\t    <style>
\t        
\t        #so-groups{
\t            display:none;
\t        }
\t        
\t        a.dropdown-toggle.account{
\t            color:#fff;
\t        }
\t        #acc_div{
\t            height: 30px;
                border-radius: 50% !important;
                width: 40px;
                padding-top: 5px;
\t        }
\t        #accimg{
\t            height: 30px;
                border-radius: 50%;
\t        }
\t        .sticky {
  position: fixed;
  top: 0;
  width: 100%;
  z-index:3;
}
\t    </style>
\t    ";
        // line 35
        if ((isset($context["logged"]) ? $context["logged"] : null)) {
            echo " 
\t        <style>
\t        .typeheader-5 ul.top-link > li.account::after{
\t            display:none;
\t        }
\t        </style>
\t    ";
        }
        // line 42
        echo "\t<a href=\"";
        echo (isset($context["Session"]) ? $context["Session"] : null);
        echo "\"></a>
\t     <!-- HEADER TOP TITLE BAR Antony-->
  <!-- <div class=\"header-top-title-bar\">
\t<div class=\"row top-row\">
\t<div class=\"col-12 col-sm-12 col-md-4 col-xs-4\">
\t\t<i class=\"fa fa-pie-chart\" aria-hidden=\"true\" class=\"chart\"></i>

\t\t<i class=\"fa fa-comment-o\" aria-hidden=\"true\" class=\"comment\"></i>

        </div>
        <div class=\"col-12 col-sm-12 col-md-4 col-xs-4\">
        \t<p class=\"title-bar\">New Website Layout</p>
        </div>
        <div class=\"col-12 col-sm-12 col-md-4 col-xs-4 button-section\">
        \t<i class=\"fa fa-question\" aria-hidden=\"true\"></i>
        \t<button class=\"btn btn-info question\" type=\"submit\"><span>Login or Create Account </span></button>

        </div>
    </div>
    </div> -->
      <!-- HEADER TOP TITLE BAR Antony-->
\t\t<div class=\"inner\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"header-top-left col-lg-1 col-md-1 col-sm-1 col-xs-6\">
\t\t\t\t\t<!-- LANGUAGE CURENCY -->
\t\t\t\t<!--\t";
        // line 67
        if ($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "lang_status"), "method")) {
            // line 68
            echo "\t\t\t\t\t<ul class=\"top-link list-inline lang-curr\">
\t\t\t\t\t\t";
            // line 69
            if ((isset($context["currency"]) ? $context["currency"] : null)) {
                echo "<li class=\"currency\"> ";
                echo (isset($context["currency"]) ? $context["currency"] : null);
                echo "  </li> ";
            }
            // line 70
            echo "\t\t\t\t\t\t";
            if ((isset($context["language"]) ? $context["language"] : null)) {
                echo " <li class=\"language\">";
                echo (isset($context["language"]) ? $context["language"] : null);
                echo " </li>\t";
            }
            echo "\t\t\t
\t\t\t\t\t</ul>\t\t\t\t
\t\t\t\t\t";
        }
        // line 72
        echo " 
\t\t\t\t\t
\t\t\t\t\t";
        // line 74
        if (($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "phone_status"), "method") && $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "contact_number"), "method"))) {
            // line 75
            echo "\t\t\t\t\t<div class=\"telephone hidden-xs hidden-sm hidden-md\" >
\t\t\t\t\t\t";
            // line 76
            echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "decode_entities", array(0 => $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "contact_number"), "method")), "method");
            echo "
\t\t\t\t\t</div>
\t\t\t\t\t";
        }
        // line 78
        echo "-->
\t\t\t\t\t<div id=\"wrapper\">
        <div class=\"overlay\"></div>
    
        <!-- Sidebar -->
        <nav class=\"navbar navbar-inverse navbar-fixed-top\" id=\"sidebar-wrapper\" role=\"navigation\">
            <ul class=\"nav sidebar-nav\">
               
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t";
        // line 87
        if ((isset($context["logged"]) ? $context["logged"] : null)) {
            echo " 
\t\t\t\t\t\t\t  <li class=\"sidebar-brand\">
                    <a  style=\"font-size: 15px; vertical-align: text-bottom; font-weight:500;\" href=\"";
            // line 89
            echo (isset($context["logout"]) ? $context["logout"] : null);
            echo "\">
                       Sign Out
                    </a>
                </li> ";
        } else {
            // line 93
            echo "\t\t\t\t\t\t\t <li class=\"sidebar-brand\">
                    <a data-toggle=\"modal\" id=\"main-login\" style=\"font-size: 15px; vertical-align: text-bottom; font-weight:500;\" data-target=\"#modalLoginForm\" href=\"#\">
                       Sign In & Sign Up
                    </a>
                </li>
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t";
        }
        // line 100
        echo "\t\t\t\t
\t\t\t\t
\t\t\t\t
\t\t\t\t
                <li>
                    <a href=\"#\" >Home</a>
                </li>
                <li>Categories</li>
                <li class=\"dropdown\">
                  <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Mobiles & Tablets<span class=\"caret\"></span></a>
                  <ul class=\"dropdown-menu\" role=\"menu\">
                    <li><a href=\"#\">Smartphones</a></li>
                    <li><a href=\"#\">Wearable Tech</a></li>
                    <li><a href=\"#\">Mobile, Tablet Accessories</a></li>
                    <li><a href=\"#\">Headphones & Headsets</a></li>
                    <li><a href=\"#\">Tablets & eReaders</a></li>
                    <li><a href=\"#\">Power Banks</a></li>
                    <li><a href=\"#\">Mobile Cases & Protectors</a></li>
                  </ul>
                </li>
                <li>
                    <a href=\"#\">Computers</a>
                </li>
                <li>
                    <a href=\"#\">Cameras</a>
                </li>
                <li>
                    <a href=\"#\">Television & Audio</a>
                </li>
                <li>
                    <a href=\"#\">Accessories</a>
                </li>
            </ul>
        </nav>
        <!-- /#sidebar-wrapper -->
        <!-- Page Content -->
        <div id=\"page-content-wrapper\">
            <button type=\"button\" class=\"hamburger is-closed\" data-toggle=\"offcanvas\">
                <span class=\"hamb-top\"></span>
          <span class=\"hamb-middle\"></span>
        <span class=\"hamb-bottom\"></span>
            </button>
        </div>
        <!-- /#page-content-wrapper -->


    </div>
    <!-- /#wrapper -->
\t\t\t\t\t
\t\t\t\t</div>
\t\t\t\t<div class=\"navbar-logo col-lg-2 col-md-2  col-sm-4 col-xs-12\">
\t\t\t\t\t<div class=\"logo\">
\t\t\t\t   \t\t";
        // line 152
        echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_logo", array(), "method");
        echo " 
\t\t\t\t   \t<!--<a href=\"http://poorvikabeta.webindia.com/index.php?route=common/home\">
\t\t\t\t   \t    <img class=\"lazyautosizes lazyloaded\" data-sizes=\"auto\" src=\"https://s1.poorvikamobile.com/image/data/poorvika_mobile_five/poorvika-4-logo.png\" data-src=\"https://s1.poorvikamobile.com/image/data/poorvika_mobile_five/poorvika-4-logo.png\" title=\"Poorvika\" alt=\"Poorvika\" sizes=\"194px\">
\t\t\t\t   \t    </a>-->
\t\t\t\t   \t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-lg-4 col-md-4  col-sm-3 col-xs-12 cata-parent\">
\t\t\t\t   
\t\t\t\t\t<div class=\"search-header-w cata-child\">
\t\t\t\t\t    <div class=\"choose-cata\">
\t\t\t\t\t<select class=\"form-control option cata-child-opn\" id=\"top_search1\">
\t\t\t\t\t    <option value=\"\">";
        // line 163
        echo (isset($context["text_category_all"]) ? $context["text_category_all"] : null);
        echo " </option>
\t\t\t\t\t";
        // line 164
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            echo " 
\t\t\t\t\t\t
\t\t\t\t\t\t";
            // line 166
            if (($this->getAttribute($context["category"], "category_id", array()) == (isset($context["category_id"]) ? $context["category_id"] : null))) {
                echo " 
\t\t\t\t\t\t\t<option value=\"";
                // line 167
                echo $this->getAttribute($context["category"], "href", array());
                echo " \" selected=\"selected\">";
                echo $this->getAttribute($context["category"], "name", array());
                echo " </option>
\t\t\t\t\t\t";
            } else {
                // line 168
                echo "   
\t\t\t\t\t\t\t<option value=\"";
                // line 169
                echo $this->getAttribute($context["category"], "href", array());
                echo " \">";
                echo $this->getAttribute($context["category"], "name", array());
                echo " </option>
\t\t\t\t\t\t";
            }
            // line 170
            echo " 
\t\t\t\t\t\t
\t\t\t\t\t\t";
            // line 172
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["category"], "children", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["category_lv2"]) {
                echo " 
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t";
                // line 174
                if (($this->getAttribute($context["category_lv2"], "category_id", array()) == (isset($context["category_id"]) ? $context["category_id"] : null))) {
                    echo " 
\t\t\t\t\t\t\t\t<option value=\"";
                    // line 175
                    echo $this->getAttribute($context["category_lv2"], "href", array());
                    echo " \" selected=\"selected\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ";
                    echo $this->getAttribute($context["category_lv2"], "name", array());
                    echo " </option>
\t\t\t\t\t\t\t";
                } else {
                    // line 176
                    echo "   
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<option value=\"";
                    // line 178
                    echo $this->getAttribute($context["category_lv2"], "href", array());
                    echo "\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ";
                    echo $this->getAttribute($context["category_lv2"], "name", array());
                    echo " </option>
\t\t\t\t\t\t\t";
                }
                // line 179
                echo " 
\t\t\t\t\t\t\t
\t\t\t\t\t\t\t";
                // line 181
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["category_lv2"], "children", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["category_lv3"]) {
                    echo " 
\t\t\t\t\t\t\t\t";
                    // line 182
                    if (($this->getAttribute($context["category_lv3"], "category_id", array()) == (isset($context["category_id"]) ? $context["category_id"] : null))) {
                        echo " 
\t\t\t\t\t\t\t\t\t<option value=\"";
                        // line 183
                        echo $this->getAttribute($context["category_lv3"], "href", array());
                        echo " \" selected=\"selected\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                        echo $this->getAttribute($context["category_lv3"], "name", array());
                        echo " </option>
\t\t\t\t\t\t\t\t";
                    } else {
                        // line 184
                        echo "   
\t\t\t\t\t\t\t\t\t<option value=\"";
                        // line 185
                        echo $this->getAttribute($context["category_lv3"], "href", array());
                        echo " \">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                        echo $this->getAttribute($context["category_lv3"], "name", array());
                        echo " </option>
\t\t\t\t\t\t\t\t";
                    }
                    // line 186
                    echo " 
\t\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category_lv3'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 188
                echo "\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category_lv2'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 189
            echo "\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 190
        echo "\t\t\t\t\t
\t\t\t\t\t  
\t\t\t\t\t  
\t\t\t\t\t</select>
\t\t\t\t\t</div>\t
\t\t\t\t\t";
        // line 195
        echo (isset($context["search_block"]) ? $context["search_block"] : null);
        echo "
\t\t\t\t    </div>
\t\t\t\t</div>
\t\t\t\t<div class=\"header-top-right collapsed-block col-lg-5 col-md-5  col-sm-4 col-xs-6 \">
\t\t\t\t\t<ul class=\"top-link list-inline\">
\t\t\t\t\t\t";
        // line 200
        if ($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "welcome_message_status"), "method")) {
            // line 201
            echo "\t\t\t\t\t\t\t<li class=\"hidden-sm hidden-xs welcome-msg\">
\t\t\t\t\t\t\t\t";
            // line 202
            if ( !twig_test_empty($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "welcome_message"), "method"))) {
                // line 203
                echo "\t\t\t\t\t\t\t\t\t";
                echo $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "decode_entities", array(0 => $this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "welcome_message"), "method")), "method");
                echo "
\t\t\t\t\t\t\t\t";
            }
            // line 204
            echo " 
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t";
        }
        // line 207
        echo "\t\t\t\t\t\t
\t\t\t\t\t\t<li class=\"cart-bag col-lg-4 col-md-4  col-sm-4 col-xs-12\">
    \t\t\t\t\t\t<div class=\"shopping_cart\">\t\t\t\t\t\t\t
            \t\t\t\t \t";
        // line 210
        echo (isset($context["cart"]) ? $context["cart"] : null);
        echo "
            \t\t\t\t</div>
        \t\t\t\t</li>
        \t\t\t\t<!-- <li><a class=\"stores\">Stores</a></li> -->
                        <li class=\"dropdown locate col-lg-4 col-md-4  col-sm-4 col-xs-12\">
          <a href=\"#\" class=\"dropdown-toggle stores\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\"> Locate <br><h5>Stores</h5><span class=\"caret\"></span></a>
          <ul class=\"dropdown-menu locator\">
           
            <li class=\"pincode\">
                        <span>
                                    <input type=\"text\" placeholder=\"Enter Pincode, City or State\" value=\"\" class=\"form-control\" id=\"pincode\" name=\"txtemail\" size=\"55\">
                               
                                 <button class=\"check\" type=\"submit\" name=\"submit\">Check</button></span>
                             </li>
                             <li class=\"search-bar\">
                               
                                    <select class=\"form-control option\">
                         <option value=\"\" disabled selected>Select State</option>
                      <option value=\"Andhra Pradesh\">Andhra Pradesh</option>
<option value=\"Andaman and Nicobar Islands\">Andaman and Nicobar Islands</option>
<option value=\"Arunachal Pradesh\">Arunachal Pradesh</option>
<option value=\"Assam\">Assam</option>
<option value=\"Bihar\">Bihar</option>
<option value=\"Chandigarh\">Chandigarh</option>
<option value=\"Chhattisgarh\">Chhattisgarh</option>
<option value=\"Dadar and Nagar Haveli\">Dadar and Nagar Haveli</option>
<option value=\"Daman and Diu\">Daman and Diu</option>
<option value=\"Delhi\">Delhi</option>
<option value=\"Lakshadweep\">Lakshadweep</option>
<option value=\"Puducherry\">Puducherry</option>
<option value=\"Goa\">Goa</option>
<option value=\"Gujarat\">Gujarat</option>
<option value=\"Haryana\">Haryana</option>
<option value=\"Himachal Pradesh\">Himachal Pradesh</option>
<option value=\"Jammu and Kashmir\">Jammu and Kashmir</option>
<option value=\"Jharkhand\">Jharkhand</option>
<option value=\"Karnataka\">Karnataka</option>
<option value=\"Kerala\">Kerala</option>
<option value=\"Madhya Pradesh\">Madhya Pradesh</option>
<option value=\"Maharashtra\">Maharashtra</option>
<option value=\"Manipur\">Manipur</option>
<option value=\"Meghalaya\">Meghalaya</option>
<option value=\"Mizoram\">Mizoram</option>
<option value=\"Nagaland\">Nagaland</option>
<option value=\"Odisha\">Odisha</option>
<option value=\"Punjab\">Punjab</option>
<option value=\"Rajasthan\">Rajasthan</option>
<option value=\"Sikkim\">Sikkim</option>
<option value=\"Tamil Nadu\">Tamil Nadu</option>
<option value=\"Telangana\">Telangana</option>
<option value=\"Tripura\">Tripura</option>
<option value=\"Uttar Pradesh\">Uttar Pradesh</option>
<option value=\"Uttarakhand\">Uttarakhand</option>
<option value=\"West Bengal\">West Bengal</option>
                     
                    </select>
                      </li>
                             <li>
                            
                                    <select class=\"form-control option\">
                         <option value=\"\" disabled selected>Select City</option>
                      <option value=\"Andhra Pradesh\">Chennai</option>
                                        </select>

                                 </li>
                             <li>
                                        <select class=\"form-control option\">
                         <option value=\"\" disabled selected>Select Area</option>
                      <option value=\"Andhra Pradesh\">Anna Nagar</option>
                                        </select>
                                          </li>
                             
                               
                        </li>
          </ul>
        </li>
        <!-- <li class=\"dropdown account col-lg-4 col-md-4  col-sm-4 col-xs-12\">
          <a href=\"index.php?route=account/account\" class=\"dropdown-toggle account\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">";
        // line 287
        if ((isset($context["logged"]) ? $context["logged"] : null)) {
            echo " ";
            echo (isset($context["customername"]) ? $context["customername"] : null);
            echo " ";
        }
        echo " 's<br>Account <span class=\"caret\"></span></a>
          <ul class=\"dropdown-menu my-account\">
            <li><a href=\"index.php?route=account/order\" class=\"order\">My Order</a></li>
            <li><a href=\"index.php?route=account/wishlist\" class=\"wishlist\">Wishlist & Saved</a></li>
            <li><a href=\"index.php?route=account/address\" class=\"address\">Saved Address</a></li>
            <li><a href=\"#\" class=\"gift\">Gift Card</a></li>
            <li class=\"border\"><a href=\"#\" class=\"coupon\">Coupons</a></li>
            <li><a href=\"#\" class=\"contact\">Contact Us</a></li>
            <li><a href=\"index.php?route=account/account\" class=\"profile\">My Profile</a></li>
            <li><a href=\"#\" class=\"notification\">Notification</a></li>
            ";
        // line 297
        if ((isset($context["logged"]) ? $context["logged"] : null)) {
            echo " 
\t\t\t\t <li><a href=\"";
            // line 298
            echo (isset($context["logout"]) ? $context["logout"] : null);
            echo "\" class=\"logout\">";
            echo (isset($context["text_logout"]) ? $context["text_logout"] : null);
            echo "</a></li>\t\t\t\t\t\t\t
\t\t\t\t  ";
        } else {
            // line 300
            echo "\t\t\t\t<li><a href=\"";
            echo (isset($context["login"]) ? $context["login"] : null);
            echo "\" class=\"logout\">";
            echo (isset($context["text_login"]) ? $context["text_login"] : null);
            echo "</a></li>
\t\t\t\t<li><a href=\"";
            // line 301
            echo (isset($context["register"]) ? $context["register"] : null);
            echo "\" class=\"logout\">";
            echo (isset($context["text_register"]) ? $context["text_register"] : null);
            echo "</a></li> \t\t\t\t\t\t\t
\t\t\t";
        }
        // line 303
        echo "          </ul>
        </li> -->
        <li class=\"dropdown account col-lg-4 col-md-4  col-sm-4 col-xs-12\">
            ";
        // line 306
        if ((isset($context["logged"]) ? $context["logged"] : null)) {
            // line 307
            echo "                <div class=\"col-lg-8 col-md-8  col-sm-8 col-xs-8\">
            ";
        }
        // line 309
        echo "          <a href=\"index.php?route=account/account\" class=\"dropdown-toggle account\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">";
        if ((isset($context["logged"]) ? $context["logged"] : null)) {
            echo " ";
            echo (isset($context["customername"]) ? $context["customername"] : null);
            echo " 's <br> ";
        }
        echo " Account <span class=\"caret\"></span></a>
          
          <ul class=\"dropdown-menu my-account\">
            <li><a href=\"index.php?route=account/order\" class=\"order\">My Order</a></li>
            <li><a href=\"index.php?route=account/wishlist\" class=\"wishlist\">Wishlist & Saved</a></li>
            <li><a href=\"index.php?route=account/address\" class=\"address\">Saved Address</a></li>
            <li><a href=\"#\" class=\"gift\">Gift Card</a></li>
            <li class=\"border\"><a href=\"#\" class=\"coupon\">Coupons</a></li>
            <li><a href=\"#\" class=\"contact\">Contact Us</a></li>
            <li><a href=\"index.php?route=account/account\" class=\"profile\">My Profile</a></li>
            <li><a href=\"#\" class=\"notification\">Notification</a></li>
            ";
        // line 320
        if ((isset($context["logged"]) ? $context["logged"] : null)) {
            echo " 
\t\t\t\t <li><a href=\"";
            // line 321
            echo (isset($context["logout"]) ? $context["logout"] : null);
            echo "\" class=\"logout\">";
            echo (isset($context["text_logout"]) ? $context["text_logout"] : null);
            echo "</a></li>\t\t\t\t\t\t\t
\t\t\t\t  ";
        } else {
            // line 323
            echo "\t\t\t\t<li id=\"new-loginbutton\"><a class=\"logout\">";
            echo (isset($context["text_login"]) ? $context["text_login"] : null);
            echo "</a></li>
\t\t\t\t<li id=\"new-regbutton\"><a class=\"logout\">";
            // line 324
            echo (isset($context["text_register"]) ? $context["text_register"] : null);
            echo "</a></li> \t\t\t\t\t\t\t
\t\t\t";
        }
        // line 326
        echo "          </ul>
          
          ";
        // line 328
        if ((isset($context["logged"]) ? $context["logged"] : null)) {
            // line 329
            echo "          </div>
          <div class=\"col-lg-4 col-md-4  col-sm-4 col-xs-4\">
              <div id=\"acc_div\">
                  <img id=\"accimg\" data-sizes=\"auto\" src=\"";
            // line 332
            echo (isset($context["cust_image"]) ? $context["cust_image"] : null);
            echo "\" data-src=\"";
            echo (isset($context["cust_image"]) ? $context["cust_image"] : null);
            echo "\" sizes=\"134px\">
              </div>
          </div>
          ";
        }
        // line 336
        echo "        </li>
    </div>
                         \t\t
                         \t
                         \t
                         \t</div>
                         \t
                         \t
                         </div>

        \t\t\t\t
\t\t\t\t\t\t<!--";
        // line 347
        if ((isset($context["logged"]) ? $context["logged"] : null)) {
            echo " 
\t\t\t\t\t\t\t <li><a href=\"";
            // line 348
            echo (isset($context["logout"]) ? $context["logout"] : null);
            echo "\">";
            echo (isset($context["text_logout"]) ? $context["text_logout"] : null);
            echo "</a></li>\t\t\t\t\t\t\t
\t\t\t\t\t\t\t  ";
        } else {
            // line 350
            echo "\t\t\t\t\t\t\t<li><a href=\"";
            echo (isset($context["login"]) ? $context["login"] : null);
            echo "\">";
            echo (isset($context["text_login"]) ? $context["text_login"] : null);
            echo "</a></li>
\t\t\t\t\t\t\t<li><a href=\"";
            // line 351
            echo (isset($context["register"]) ? $context["register"] : null);
            echo "\">";
            echo (isset($context["text_register"]) ? $context["text_register"] : null);
            echo "</a></li> \t\t\t\t\t\t\t
\t\t\t\t\t\t";
        }
        // line 352
        echo "-->

\t\t\t\t\t\t<!-- WISHLIST  -->
\t\t\t\t\t\t";
        // line 355
        if ((isset($context["wishlist_status"]) ? $context["wishlist_status"] : null)) {
            // line 356
            echo "\t\t\t\t\t\t\t<li class=\"wishlist\"><a id=\"wishlist-total\" class=\"btn-link\" href=\"";
            echo (isset($context["wishlist"]) ? $context["wishlist"] : null);
            echo "\"  title=\"";
            echo (isset($context["text_wishlist"]) ? $context["text_wishlist"] : null);
            echo "\">";
            echo (isset($context["text_wishlist"]) ? $context["text_wishlist"] : null);
            echo "</a></li>
\t\t\t\t\t\t";
        }
        // line 357
        echo "\t
\t\t\t\t\t\t<!-- checkout -->
\t\t\t\t\t\t";
        // line 359
        if ($this->getAttribute((isset($context["soconfig"]) ? $context["soconfig"] : null), "get_settings", array(0 => "checkout_status"), "method")) {
            // line 360
            echo "\t\t\t\t\t\t\t<li class=\"checkout hidden-xs\"><a href=\"";
            echo (isset($context["checkout"]) ? $context["checkout"] : null);
            echo " \" class=\"btn-link\" title=\"";
            echo (isset($context["text_checkout"]) ? $context["text_checkout"] : null);
            echo " \"><span >";
            echo (isset($context["text_checkout"]) ? $context["text_checkout"] : null);
            echo " </span></a></li>
\t\t\t\t\t\t";
        }
        // line 361
        echo " \t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t</ul>
\t\t\t\t</div>
\t\t\t\t\t<!-- HEADER CENTER -->
\t<div class=\"header-center ";
        // line 365
        echo (isset($context["hidden_headercenter"]) ? $context["hidden_headercenter"] : null);
        echo "\">
\t\t<div class=\"container\">

\t\t\t<div class=\"main-menu-w\">
\t\t\t<!-- Main menu -->\t\t\t\t
\t\t\t  ";
        // line 370
        echo (isset($context["content_menu1"]) ? $context["content_menu1"] : null);
        echo "
\t\t\t</div>
\t\t\t<div class=\"cart-search\">
\t\t\t\t

\t\t\t
\t\t\t</div>
\t\t</div>
\t</div>
\t\t\t\t
\t\t\t</div>
\t\t\t
\t\t</div>
\t\t
\t</div>

\t\t<script type=\"text/javascript\">
  \$(document).ready(function () {
  var trigger = \$('.hamburger'),
      overlay = \$('.overlay'),
     isClosed = false;

    trigger.click(function () {
      hamburger_cross();      
    });

    function hamburger_cross() {

      if (isClosed == true) {          
        overlay.hide();
        trigger.removeClass('is-open');
        trigger.addClass('is-closed');
        isClosed = false;
      } else {   
        overlay.show();
        trigger.removeClass('is-closed');
        trigger.addClass('is-open');
        isClosed = true;
      }
  }
  
  \$('[data-toggle=\"offcanvas\"]').click(function () {
        \$('#wrapper').toggleClass('toggled');
  });  
});
</script>

</header>



<!-- LOGIN POPUP-->
\t<!-- HEADER TOP -->
\t<input type=\"hidden\" name=\"signupotp\" id=\"signupotp\">
\t<input type=\"hidden\" name=\"msgotp\" id=\"msgotp\">
\t<input type=\"hidden\" name=\"forgetotp\" id=\"forgetotp\">
\t<!-- HEADER CENTER -->\t\t\t
\t<!--LoginPopup-->
\t<div class=\"modal fade\" id=\"modalLoginForm\" tabindex=\"-1\" data-backdrop=\"static\" data-keyboard=\"false\" role=\"dialog\" aria-labelledby=\"myModalLabel\"
    style=\"cursor: context-menu;\">
 
  <div class=\"modal-dialog modal-login\" role=\"document\">
    <div class=\"modal-content\" >
    \t <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\" style=\"color: white; margin-top: -13px;\">
<span aria-hidden=\"true\" onclick=\"resetForm();\"style=\"position: absolute;\">×</span>
</button>
      <div class=\"col-md-5 col-xs-5 loginleft\">
         <img src=\"https://demo1.gyso.in/apx/image/data/pvk-logo/poovika_new_mobile.png\" class=\"img-responsive\" style=\"margin-bottom: 40px!important;\">   
                         <!--  <h2 style=\"margin-bottom: 30px;\">Login</h2>    --> 
                  <img src=\"https://s1.poorvikamobile.com/image/data/poorvika_mobile_five/oppo-side-banner.jpg\" class=\"img-responsive\"> 

      </div>
      <!-- <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
          <span aria-hidden=\"true\">&times;</span>
        </button> -->

      <div class=\"modal-body col-md-7 col-xs-7\">
       
\t\t<!--<div class=\"\" style=\"display:none;\">SIGN UP</div>-->
        <!--Main Page(Sign IN)-->
        <div id=\"hide\">
        <div class=\"signin\">SIGN IN</div>
     <form id=\"contact\" method=\"post\" action=\"\" name=\"loginform\">
        <div class=\"floating-label\" data-validate=\"Name is required\" name=\"myform\">
           <input class=\"floating-input firstemailid\" id=\"emailid\"  type=\"text\" onkeyup=\"lookupemail(this);\" name=\"emailid\" autofocus placeholder=\"\">
            <span class=\"emailerror\" style=\"color:red;\"></span>
            <label>Enter Email/Mobile Number</label> 
            \t<span class=\"countrycode\">+91</span>
            <!-- <span class=\"countrycode\">+91</span> -->
          </div>
          <div class=\"floating-label\" data-validate=\"Name is required\" name=\"myform\">
          
            <input class=\"floating-input\" type=\"password\" onkeyup=\"lookup(this);\" style=\"\"height:45px; name=\"password\" id=\"password\" placeholder=\"\">
           <!-- <span class=\"highlight\"></span>-->
            <span class=\"highlight\" style=\"color:red; z-index:-1;\"></span>
            <label style=\"margin-top: 7px;\">Enter Password</label>  
          </div>
                       <!-- Forget Password Field-->
           <!--<div class=\"forgetpsw\"><b id=\"fp\"><a href=\"JavaScript:Void(0);\">Forget Password?</a></b></div>-->
            <div class=\"forgetpsw\"><b id=\"fp\"><a>Forgot Password?</a></b></div>
           
            <!--End-->
            <div id=\"popupfooter\">
      <div class=\"modal-footer d-flex justify-content-center\">
      <div id=\"contact_submit\">\t\t\t
        <button type=\"button\" class=\"btn btn-lg loginbutton\" onclick=\"MyLogin()\">Login</button>  
     </div>    
\t 

\t 
\t\t<div class=\"or\">OR</div>
        <button type=\"button\" class=\"btn btn-lg\"  onclick=\"myFunction()\" id=\"otp-field\">Request OTP</button>
        
      </div> 

  </form>

         
 <div class=\"loginbutton1\">New to Poorvika? Create an account</div>
    </div>
                   </form>
                   </div>
           <!-- End-->
\t\t\t\t   <!--Sign up Field-->
\t\t\t\t   
\t <div id=\"signupcontent\" style=\"display:none;\">
          <div class=\"floating-label\" data-validate=\"Name is required\" name=\"myform\">
            <input class=\"floating-input firstemailid numbersOnly\" type=\"number\" onclick=\"lookup(this)\" name=\"continuephonenumber\" id=\"continuephonenumber\" pattern=\"/^-?\\d+\\.?\\d*\$/\" onKeyPress=\"if(this.value.length==10) return false;\"  placeholder=\"\" style=\"padding-left: 37px!important;\">
            <span class=\"continue_error redcolor\"></span>
            <label>Enter Mobile Number</label>
            <span class=\"countrycode1\">+91</span>
          </div>
          <div style=\"text-align: left;\">By continuing, you agree to Poorvika <span style=\"color: #ff5722;\"><a href=\"https://www.poorvikamobile.com/terms-and-conditions\" style=\"color: #ff5721;\">Terms of Use</a></span> and <span style=\"color: #ff5722;\">Privacy Policy.</span></div>
          <button class=\"btn btn-lg\" id=\"continuebutton\">Continue</button>
          <button class=\"btn btn-lg Existing-loginbutton\" style=\"font-size:15px; color: #ff5722; font-weight: 500; background: transparent;\">Existing User?Log in</button>
          </div>
           <!--End-->\t\t\t\t   
\t\t\t\t   <!--Forget Password-->
            <div style=\"display: none;\" id=\"show\">
             <div class=\"floating-label\" data-validate=\"Name is required\" name=\"myform\">
            <input class=\"floating-input firstemailid\" type=\"text\" name=\"forgetphonenumber\" id=\"forgetphonenumber\" class=\"forgetphonenumber\" placeholder=\"\"><span class=\"resendotp\"><b id=\"change\"><a href=\"JavaScript:Void(0);\">Change?</a></b></span>
            <span class=\"forgetphonr_error\"></span>
            <label>Mobile Number</label>
            <span class=\"countrycode\">+91</span>
          </div>
                       <!-- <div class=\"wrap-input2 validate-input\" data-validate=\"Name is required\">
            <input class=\"input2\" type=\"text\" name=\"name\"><span class=\"resendotp\"><b id=\"change\"><a href=\"JavaScript:Void(0);\">Change?</a></b></span>
            <span class=\"focus-input2\" data-placeholder=\"Enter Your Phone Number\"></span>
          </div> -->
          <div class=\"floating-label\" data-validate=\"Name is required\" name=\"myform\">
            <input class=\"floating-input numbersOnly\"  maxlength=\"6\" name=\"forgetotptext\"  onkeyup=\"forgetotplogin()\" id=\"forgetotptext\" placeholder=\"\"><span class=\"resendotp\" id=\"forgotresend\"><a href=\"JavaScript:Void(0);\">Resend?</a></span>
            <span class=\"forgetotptext redcolor\"></span>
            <label style=\"top: -18px!important;\">Enter OTP</label>
          </div>
          <!-- <div class=\"wrap-input2 validate-input\" data-validate=\"Name is required\">
            <input class=\"input2\" type=\"number\" name=\"otp\" min=\"1\" max=\"4\"><span class=\"resendotp\"><a href=\"JavaScript:Void(0);\">Resend?</a></span>
            <span class=\"focus-input2\" data-placeholder=\"Enter Your OTP\"></span>
          </div> -->
          <div class=\"floating-label\" data-validate=\"Name is required\" name=\"myform\">
            <input class=\"floating-input\" type=\"password\" name=\"forgetpassword\"  id=\"forgetpassword\" placeholder=\"\">
            <span class=\"forgetpass_error redcolor\"></span>
            <label style=\"margin-top:6px;\">Set Password</label>
          </div>
          <!-- <div class=\"wrap-input2 validate-input\" data-validate=\"Name is required\">
            <input class=\"input2\" type=\"Password\" name=\"Password\">
            <span class=\"focus-input2\" data-placeholder=\"Set New Password\"></span>
          </div> -->
          <button class=\"btn btn-lg forgetloginbutton\" id=\"forgrtloginbutton\" style=\"margin-bottom: 20px;\">Login</button> 
          <button class=\"btn btn-lg newtopoor\">New to Poorvika ? Sign Up</button>
          </div>
          <!--End-->
\t<!--Otp Field--> 
            <div class=\"signup-newcus\" style=\"display:none;\">
\t\t\t
\t\t\t<form id=\"signin-newcustomer\">
\t\t\t<div class=\"floating-label\" data-validate=\"Name is required\" name=\"myform\">
            <input class=\"floating-input numbersOnly\" maxlength=\"6\" name=\"regotp\" onkeyup=\"signinotp()\" id=\"regotp\" placeholder=\"\" style=\"border: none; border:none; text-align: left; font-size: 14px;\"><span class=\"resendotp\" id=\"resendotpsignup\"><a href=\"JavaScript:Void(0);\">Resend?</a></span>
            <span class=\"regotperror_msg redcolor\" style=\"color:red;\"></span>
            <label style=\"top:-18px!important;\">Enter OTP</label>
            </div> 
\t\t\t<div style=\"margin-bottom: 29px;\"class=\"floating-label\" data-validate=\"Name is required\" name=\"myform\">
            <input class=\"floating-input\" type=\"text\" name=\"regname\" onkeyup=\"signinotp()\" id=\"regname\"  placeholder=\"\">
            <span class=\"regname_error redcolor\"></span>
            <label>Name</label>
            </div>
\t\t\t
\t\t\t<!--<div class=\"floating-label\" data-validate=\"gender is required\" name=\"myform\">
\t\t\t<input type=\"radio\" value=\"Male\" name=\"reggender\" style=\"margin-top: 10px; margin-left: 116px;\"><span style=\"margin-left: 6px;
    margin-right: 41px;
    font-size: 15px;
    color: #FF5722;
    font-weight: 600;\">Male</span>
\t\t\t<input type=\"radio\" value=\"FeMale\" name=\"reggender\" style=\"margin-top: 10px;\"><span style=\"margin-left: 6px;
    margin-right: 41px;
    font-size: 14px;
    color: #FF5722;
    font-weight: 600;\">Female</span>
            <label>Gender</label>
            </div>-->
\t\t\t
\t\t\t
            <div class=\"floating-label\" data-validate=\"Name is required\" name=\"myform\">
            <input class=\"floating-input\" type=\"number\" name=\"regphonenumber\" id=\"regphonenumber\" placeholder=\"\" style=\"border:none; text-align: left; font-size: 14px; padding-left: 37px!important;\"><span class=\"resendotp\"><b id=\"change1\"><a href=\"JavaScript:Void(0);\">Change?</a></b></span>
            <span class=\"regphone_error redcolor\"></span>
            <label>Mobile Number</label>
             <span class=\"countrycode1\">+91</span>

            </div>
\t\t\t
\t\t\t
\t\t\t <div class=\"floating-label\" data-validate=\"Name is required\" name=\"myform\">
\t\t\t<input class=\"floating-input\" type=\"email\" onkeyup=\"signinotp()\" name=\"regemail\" id=\"regemail\" required placeholder=\"\" style=\"border:none; text-align: left; font-size: 14px;\">
\t\t\t<span class=\"regemail_error redcolor\"></span>
\t\t\t<label class=\"regemaillabel\">Email</label>
\t\t\t</div>
\t\t\t
\t\t\t
\t\t\t
                <!-- <div class=\"wrap-input2 validate-input\" data-validate=\"Name is required\">
            <input class=\"input2\" type=\"text\" name=\"name\"><span class=\"resendotp\"><b id=\"change\"><a href=\"JavaScript:Void(0);\">Change?</a></b></span>
            <span class=\"focus-input2\" data-placeholder=\"Enter Your Phone Number\"></span>
          </div> -->
         
          <!-- <div class=\"wrap-input2 validate-input\" data-validate=\"Name is required\">
            <input class=\"input2\" type=\"number\" name=\"otp\" min=\"1\" max=\"4\"><span class=\"resendotp\"><a href=\"JavaScript:Void(0);\">Resend?</a></span>
            <span class=\"focus-input2\" data-placeholder=\"Enter Your OTP\"></span>
          </div> -->
          <div class=\"floating-label\" data-validate=\"Name is required\" name=\"myform\">
            <input class=\"floating-input\" type=\"password\" onkeyup=\"signinotp()\" name=\"regpassword\" id=\"regpassword\" placeholder=\"\">
            <span class=\"regpass_error redcolor\"></span>
            <label>Set Password</label>
          </div>
          <button type=\"button\" class=\"btn btn-lg signupbutton\" id=\"signupbutton\" style=\"margin-bottom: 20px;\">Sign Up</button> 
          
</form>
</div>
  <!--SignUp New Customer-->
  <form id=\"otphead123\">
        <div style=\"display: none;\" id=\"otp-head\">
\t\t\t  <div>Please enter the OTP sent to</div> <div><p class=\"otp_password\"></p>
\t\t\t  <span id=\"otpchange\" style=\"color:#f68600; font-weight:500;\"><p id=\"currentphnumber\"></p>Change?</span></div>
\t\t\t  <!--<div style=\"display:flex; margin-left: auto; margin-right: auto; display: block; margin-top: 50px;\">
\t\t\t    <div id=\"divOuter\">
\t\t\t\t\t<div id=\"divInner\">
\t\t\t\t\t\t<input id=\"partitioned\" type=\"text\" maxlength=\"6\" onkeyup=\"verifyotp()\" onKeyPress=\"if(this.value.length==6) return false;\"/>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t
\t\t\t  </div>-->
             <div class=\"header-otp1\">
    <input id=\"sendotpemail-first1\" class=\"form-control1\" type=\"number\" onkeyup=\"verifyotp()\" onKeyPress=\"if(this.value.length==1) return false;\"/>
    <input id=\"sendotpemail-first2\" class=\"form-control1\" type=\"number\" onkeyup=\"verifyotp()\" onKeyPress=\"if(this.value.length==1) return false;\"/>
    <input id=\"sendotpemail-first3\" class=\"form-control1\" type=\"number\" onkeyup=\"verifyotp()\" onKeyPress=\"if(this.value.length==1) return false;\"/>
    <input id=\"sendotpemail-first4\" class=\"form-control1\" type=\"number\" onkeyup=\"verifyotp()\" onKeyPress=\"if(this.value.length==1) return false;\"/>
    <input id=\"sendotpemail-first5\" class=\"form-control1\" type=\"number\" onkeyup=\"verifyotp()\"  onKeyPress=\"if(this.value.length==1) return false;\"/>
    <input id=\"sendotpemail-first6\" class=\"form-control1\" type=\"number\" onkeyup=\"verifyotp()\"  onKeyPress=\"if(this.value.length==1) return false;\"/>
             </div>   
            <!-- <div style=\"text-align: center; margin-top: 20px;\">Try to Auto Capture</div>-->
\t\t\t<span id=\"emailotp_error\" class=\"red-color\"></span>
             <div class=\"otp-verify\"> 
              <button type=\"button\" id=\"email_verifyotp\" onclick=\"emailotpverify()\" class=\"otp-verify-button\">Verify</button>
             </div>

\t\t\t  <!--<div class=\"btn btn-lg verifybutton\" onclick=\"verifyotp()\">Verify</div> -->
\t\t\t  <p style=\"margin-top:25px; font-size:14px;\">Not received your code? <span class=\"resendcode\" onclick=\"myFunction()\">Resend Code</span></p>
        </div>
        </div>
          </form>
              <!--End-->
        
<!-- Button Field-->
    
    </div>
  </div>
  

</div>

 </div>
\t<!--End-->
\t\t\t\t\t
\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t
\t\t\t\t\t
\t\t\t\t</div>
\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t</div>
\t\t</div>
    <div id=\"error-msg\"></div>

\t</div>
  
\t<!-- HEADER BOTTOM -->
\t
\t
\t<!-- Navbar switcher -->
\t<?php if (!isset(\$toppanel_status) || \$toppanel_status != 0) : ?>
\t<?php if (!isset(\$toppanel_type) || \$toppanel_type != 2 ) :  ?>
\t<div class=\"navbar-switcher-container\" style=\"display:none;\">
\t\t<div class=\"navbar-switcher\">
\t\t\t<span class=\"i-inactive\">
\t\t\t\t<i class=\"fa fa-caret-down\"></i>
\t\t\t</span>
\t\t\t <span class=\"i-active fa fa-times\"></span>
\t\t</div>
\t</div>
\t<?php endif; ?>
\t<?php endif; ?>
</header>



<script>
function validateEmail(email) { 
 var mailFormat = /^([a-zA-Z0-9_\\.\\-])+\\@(([a-zA-Z0-9\\-])+\\.)+([a-zA-Z0-9]{2,4})|([0-9]{10})+\$/;
    var emailRegex = /^([a-zA-Z0-9_\\.\\-])+\\@(([a-zA-Z0-9\\-])+\\.)+([a-zA-Z0-9]{2,4})+\$/;
\tif (emailRegex.test(email)&&mailFormat.test(email)) {
    return emailRegex.test(email);
\t}
}
function validatePhone(phone) { 
    var pattern = /^[0]?[6789]\\d{9}\$/;
    var phoneRegex = /^(\\+91-|\\+91|0)?\\d{10}\$/; 
\tif (pattern.test(phone)&&phoneRegex.test(phone)) {
    return phoneRegex.test(phone);
\t}
}


function myFunction() {   
   
\t\tvar email=document.loginform.emailid.value;
\t\t\$(\"#otp-field\").button('loading');
\t\t
\t\t
\tif (validateEmail(document.loginform.emailid.value)||validatePhone(document.loginform.emailid.value)){
\t
\t\t 
         \$.ajax({
\t\t url: 'index.php?route=login/login',
\t\t type: 'POST', 
\t\t data:{email:email},
\t\t datatype:'json',
\t\t success: function (data) {
\t\t\t// console.log(data);
\t\t\tvar data_array = jQuery.parseJSON(data);
\t\t\tif(data_array.status=='1')
\t\t\t{
\t\t\t\t\$(\"#otp-field\").button('reset');
\t\t\t    \$(\"#hide\").hide();
                \$(\"#signupcontent\").show();
                var mobile=\$(\"#emailid\").val();
\t\t\t    \$(\"#continuephonenumber\").empty();
\t\t\t    \$(\"#continuephonenumber\").val(mobile);  
\t\t\t   
\t\t\t}else{  
\t\t\t   \$(\"#otp-field\").button('reset');
\t\t\t   \$('#error-msg').empty();
\t           \$('#error-msg').fadeIn();
               \$('#error-msg').append('<p id=\"err-msg-verification\" style=\"width: 440px; background: #093143; height: 43px; padding-top: 10px;margin-top: -10px;\"><img src=\"data:image/svg+xml;base64,PHN2ZyB3aWR0aD0nMjAnIGhlaWdodD0nMjAnIHZpZXdCb3g9JzAgMCAyMCAyMCcgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJz4KCTxnPgoJCTxwYXRoIGQ9J00tMi0yaDI0djI0SC0yeicgZmlsbD0nbm9uZScvPgoJCTxwYXRoIGQ9J00xMCAwQzQuNDggMCAwIDQuNDggMCAxMHM0LjQ4IDEwIDEwIDEwIDEwLTQuNDggMTAtMTBTMTUuNTIgMCAxMCAwem0xIDE1SDl2LTJoMnYyem0wLTRIOVY1aDJ2NnonIGZpbGw9JyNGRjQzNDMnLz4KCTwvZz4KPC9zdmc+Cg==\">Verification Code Send To '+email+'</p>').fadeOut(6000);
   
\t\t\t\t\$(\"#hide\").hide();
\t\t\t\t\$('#msgotp').val(\"\");
\t\t\t\t\$('#currentphnumber').empty();
\t\t\t\t
\t\t\t\t\$('#currentphnumber').append(\$(\"#emailid\").val());
\t\t\t\t
\t\t\t\t\$('#msgotp').val(btoa(btoa(data_array.otp)));    
\t\t\t\t\$(\"#otp-head\").show();
\t\t\t\t\$(\".otp_password\").empty();\t
\t\t\t\t\$('#sendotpemail-first1').focus();
\t\t\t\t
\t\t\t\t}
\t\t }
\t\t});
\t\t }else{
\t\t\t \$(\"#otp-field\").button('reset');
\t\t\t\$('.emailerror').empty();
\t\t    \$('.emailerror').append('Please enter valid Email ID/Mobile number');
\t\t }
}

function MyLogin()
{
\t    
\tif(\$('#emailid').val()==\"\"){
\t\t \$('.emailerror').empty();
\t\t    \$('.emailerror').append('Please enter valid Email ID/Mobile number');
\t\t
\t}else if(\$('#password').val()==\"\"){
\t\t \$('.highlight').empty();
\t\t    \$('.highlight').append('Please enter Password');
\t\t   
\t}else{\t 
\t\$('.loginbutton').button('loading');
              // \$.preloader.start({ modal: true, src : 'image/loadspinner/sprites323.png' });
                            // setTimeout(function(){ \$.preloader.stop(); }, 3000);  \t
\tif(validateEmail(document.loginform.emailid.value)||validatePhone(document.loginform.emailid.value)){
\t\t\t\t\t\$.ajax({
\t\t\t\t\t\t url: 'index.php?route=login/login/checkloginuser',
\t\t\t\t\t\t type: 'POST', 
\t\t\t\t\t\t data:{email:\$('#emailid').val(),password:\$('#password').val()},
\t\t\t\t\t\t datatype:'json',
\t\t\t\t\t\t success: function (data){
\t\t\t\t\t\t\tvar data_array = jQuery.parseJSON(data);
\t\t\t\t\t\t  if(data_array.status=='1')
\t\t\t\t\t\t  {
\t\t\t\t\t\t\t  
\t\t\t\t\t\t\t  return loginfunction(); 
                           \$('.loginbutton').button('reset'); \t\t\t\t\t\t\t  
\t\t\t\t\t\t  }else{
\t\t\t\t\t\t\t  \$('.loginbutton').button('reset'); 
\t\t\t\t\t\t\t  \$('.emailerror').empty();
\t\t                       \$('.emailerror').append('Your username or password is incorrect');
\t\t\t\t\t\t  }
\t\t\t\t\t\t }
\t\t\t\t\t\t});
\t}else
{
\$('.loginbutton').button('reset');
\$('.emailerror').empty();
\$('.emailerror').append('Please enter valid Email ID/Mobile number');
}
\t}
\t
}


function forgetpasswordemail()
{
\t\$('#fp').html('<div id=\"dots5\"><span></span><span></span><span></span><span></span></div>');
var email=document.loginform.emailid.value;
         \$.ajax({
\t\t url: 'index.php?route=login/login/forgetpasswordmail',
\t\t type: 'POST', 
\t\t data:{email:email},
\t\t datatype:'json',
\t\t success: function (data) {
\t\t\t //console.log(data);
\t\t\tvar data_array = jQuery.parseJSON(data);
\t\t\tif(data_array.status=='0')
\t\t\t{
\t\t\t\t\$('#fp').html('<div class=\"forgetpsw\"><b id=\"fp\"><a>Forgot Password?</a></b></div>');
\t\t\t \$(\".emailerror\").empty();\t
\t\t\t \$(\".emailerror\").append('You are not registered with us. Please sign up.');
\t\t 
\t\t\t}else{
\t\t\t\t\$('#fp').html('<div class=\"forgetpsw\"><b id=\"fp\"><a>Forgot Password?</a></b></div>');
\t\t\t\t\$('#error-msg').empty();
\t\t\t\t\$('#error-msg').fadeIn();
          \$('#error-msg').append('<p id=\"err-msg-verification\"><img src=\"data:image/svg+xml;base64,PHN2ZyB3aWR0aD0nMjAnIGhlaWdodD0nMjAnIHZpZXdCb3g9JzAgMCAyMCAyMCcgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJz4KCTxnPgoJCTxwYXRoIGQ9J00tMi0yaDI0djI0SC0yeicgZmlsbD0nbm9uZScvPgoJCTxwYXRoIGQ9J00xMCAwQzQuNDggMCAwIDQuNDggMCAxMHM0LjQ4IDEwIDEwIDEwIDEwLTQuNDggMTAtMTBTMTUuNTIgMCAxMCAwem0xIDE1SDl2LTJoMnYyem0wLTRIOVY1aDJ2NnonIGZpbGw9JyNGRjQzNDMnLz4KCTwvZz4KPC9zdmc+Cg==\">Verification Code Send To '+email+'</p>').fadeOut(7000);
  
\t\t\t\$(\"#forgetotp\").empty();
\t\t\t\$(\"#forgetotp\").val(btoa(btoa(data_array.otpnumber)));
\t\t\t\$(\"#forgetphonenumber\").empty();
\t\t\t\$(\"#forgetphonenumber\").val(email);
\t\t\t \$(\"#forgetphonenumber\").prop(\"readonly\", true);
\t\t\t\$(\"#hide, #fp, #requestotp, .loginbutton1, #otp-field\").hide();
\t\t\t\$(\"#show, .loginbutton\").show();
\t\t\t\$(\".loginleft\").css(\"height\", \"515px\");
\t\t\t\$(\".modal-body\").css(\"padding\", \"0px 0px\");
\t\t\t\$(\".modal-content\").addClass(\"test\");
\t\t\t\$(\"#forgetotptext\").focus();
\t\t\t
\t\t\t}
\t\t   }
\t});
}


function forgetpassworphone()
{
\t\$('#fp').button('loading');
\tvar email=document.loginform.emailid.value;
\t
\t
         \$.ajax({
\t\t url: 'index.php?route=login/login/forgetpasswordphone',
\t\t type: 'POST', 
\t\t data:{phone:email},
\t\t datatype:'json',
\t\t success: function (data) {
\t\t\t// console.log(data);
\t\t\tvar data_array = jQuery.parseJSON(data);
\t\t\tif(data_array.status=='0')
\t\t\t{
\t\t\t\t\$('#fp').button('reset');
\t\t\t \$(\".emailerror\").empty();\t
\t\t\t \$(\".emailerror\").append('You are not registered with us. Please sign up.');
\t\t 
\t\t\t}else{
\t\t\t\$('#error-msg').empty();
\t\t\t\$('#error-msg').fadeIn();
            \$('#error-msg').append('<p id=\"err-msg-verification\" style=\"width: 461px;background: #093143;height: 43px;padding-top: 10px;margin-top: -10px;\"><img src=\"data:image/svg+xml;base64,PHN2ZyB3aWR0aD0nMjAnIGhlaWdodD0nMjAnIHZpZXdCb3g9JzAgMCAyMCAyMCcgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJz4KCTxnPgoJCTxwYXRoIGQ9J00tMi0yaDI0djI0SC0yeicgZmlsbD0nbm9uZScvPgoJCTxwYXRoIGQ9J00xMCAwQzQuNDggMCAwIDQuNDggMCAxMHM0LjQ4IDEwIDEwIDEwIDEwLTQuNDggMTAtMTBTMTUuNTIgMCAxMCAwem0xIDE1SDl2LTJoMnYyem0wLTRIOVY1aDJ2NnonIGZpbGw9JyNGRjQzNDMnLz4KCTwvZz4KPC9zdmc+Cg==\">Verification Code Send To '+email+'</p>').fadeOut(7000);
\t\t\t\$('#fp').button('reset');
\t\t\t\$(\"#forgetotp\").empty();
\t\t\t\$(\"#forgetotp\").val(btoa(btoa(data_array.otpnumber)));
\t\t\t\$(\"#forgetphonenumber\").empty();
\t\t\t\$(\"#forgetphonenumber\").val(email);
\t\t\t \$(\"#forgetphonenumber\").prop(\"readonly\", true);
\t\t\t\$(\"#hide, #fp, #requestotp, .loginbutton1, #otp-field\").hide();
\t\t\t\$(\"#show, .loginbutton\").show();
\t\t\t\$(\".loginleft\").css(\"height\", \"515px\");
\t\t\t\$(\".modal-body\").css(\"padding\", \"0px 0px\");
\t\t\t\$(\".modal-content\").addClass(\"test\");
\t\t\t\$(\"#forgetotptext\").focus();
\t\t\t}
\t\t   }
\t});
}
 
function loginfunction()
{
\t\t\$.ajax({
\t\t\t// url: 'index.php?route=login/login/otplogin',
\t\t\t url: 'index.php?route=login/login/userlogin',
\t\t\t type: 'POST', 
\t\t\t data:{email:\$('#emailid').val(),password:\$('#password').val()},
\t\t\t//data:{email:'8976876543',password:'palani@12345'},
\t\t\t datatype:'json',
\t\t\t success: function (data){
\t\t\t\t var url      = window.location.href;  
\t\t\t\t if(url==\"http://poorvikabeta.webindia.com/index.php?route=account/logout\"){
\t\t\t\t\t \$('#modalLoginForm').modal('hide');
            localStorage.setItem('popState', 'shown');
\t\t\t\t\t location.replace(\"http://poorvikabeta.webindia.com/\");
\t\t\t\t }else{
\t\t\t\t\t \$('#modalLoginForm').modal('hide');
            localStorage.setItem('popState', 'shown');
\t\t\t\t\t window.location.reload();
\t\t\t\t }
\t\t\t\t 
\t\t\t }
\t\t\t});
}

function verifyotp() {  
var otplogin=\"otplogin\";
var verifyotpdesk=\$('#sendotpemail-first1').val()+\$('#sendotpemail-first2').val()+\$('#sendotpemail-first3').val()+\$('#sendotpemail-first4').val()+\$('#sendotpemail-first5').val()+\$('#sendotpemail-first6').val();
if(verifyotpdesk.length=='6') {
if(\$('#msgotp').val()==btoa(btoa(verifyotpdesk)))
{
   //\$.preloader.start({ modal: true, src : '../image/loadspinner/sprites323.png' });
    // setTimeout(function(){ \$.preloader.stop(); }, 3000); 
\t\t\t\t\t\$.ajax({
\t\t\t\t\t\t url: 'index.php?route=login/login/otplogin',
\t\t\t\t\t\t type: 'POST', 
\t\t\t\t\t\t data:{email:\$('#emailid').val(),password:\$('#password').val(),otplogin:otplogin},
\t\t\t\t\t\t datatype:'json',
\t\t\t\t\t\t success: function(data){\t
\t\t\t\t\t\t  var url      = window.location.href;  
\t\t\t\t if(url==\"http://poorvikabeta.webindia.com/index.php?route=account/logout\"){
\t\t\t\t\t \$('#modalLoginForm').modal('hide');
            localStorage.setItem('popState', 'shown');
\t\t\t\t\t location.replace(\"http://poorvikabeta.webindia.com/\");
\t\t\t\t }else{
\t\t\t\t\t \$('#modalLoginForm').modal('hide');
            localStorage.setItem('popState', 'shown');
\t\t\t\t\t window.location.reload();
\t\t\t\t }\t
\t\t\t\t\t\t }
\t\t\t\t\t});
\t
}else
{
\$('#error-msg').empty();
\$('#error-msg').fadeIn();
\$('#error-msg').append('<p id=\"err-msg\"><img src=\"data:image/svg+xml;base64,PHN2ZyB3aWR0aD0nMjAnIGhlaWdodD0nMjAnIHZpZXdCb3g9JzAgMCAyMCAyMCcgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJz4KCTxnPgoJCTxwYXRoIGQ9J00tMi0yaDI0djI0SC0yeicgZmlsbD0nbm9uZScvPgoJCTxwYXRoIGQ9J00xMCAwQzQuNDggMCAwIDQuNDggMCAxMHM0LjQ4IDEwIDEwIDEwIDEwLTQuNDggMTAtMTBTMTUuNTIgMCAxMCAwem0xIDE1SDl2LTJoMnYyem0wLTRIOVY1aDJ2NnonIGZpbGw9JyNGRjQzNDMnLz4KCTwvZz4KPC9zdmc+Cg==\">OTP is incorrect</p>').fadeOut(6000);
 

 }
\t}
}
function lookupemail(arg)
{
\tvar dInput = arg.value[0];
\tif(\$.isNumeric(dInput)==true)
\t{
\t\t\$('.countrycode').show();
\t\t\$('.firstemailid').css('cssText', 'padding-left: 37px !important');

\t}else{
\t\t\$('.countrycode').hide();
\t\t\$('.firstemailid').css('cssText', 'padding-left: 0px !important');
\t\t
\t\t
\t}
\t //alert(dInput);
 \$('.emailerror').empty();
 \$('.highlight').empty();
 \$('.continue_error').empty();
}

function lookup(arg){
\t
\t //alert(dInput);
 \$('.emailerror').empty();
 \$('.highlight').empty();
 \$('.continue_error').empty();
}
function continuefunction()
{
\$('#continuebutton').button('loading');
\t\$.ajax({
\t\t\t url: 'index.php?route=login/login/continuelogin',
\t\t\t type: 'POST',  
\t\t\t data:{phonenumber:\$('#continuephonenumber').val()},
\t\t\t datatype:'json',
\t\t\t success: function (data){\t
\t\t\t\t var data_array = jQuery.parseJSON(data);
\t\t\t\t if(data_array.continue_otp=='alreadytaken')
\t\t\t\t {
\t\t\t\t\t \$('#continuebutton').button('reset'); 
\t\t\t\t\t \$('#signupcontent').hide();
\t\t\t\t\t \$('#hide').show();
\t\t\t\t\t \$(\"#emailid\").val(\$('#continuephonenumber').val()); 
\t\t\t\t\t \$(\"#emailid\").focus(); 
\t\t\t\t\t \$('.signin').show();
\t\t\t\t\t \$('#popupfooter').show();
\t\t\t\t  \$('.continue_error').empty();
\t\t\t\t  
\t\t\t\t    \$('#error-msg').empty();
                    \$('#error-msg').fadeIn();
                    \$('#error-msg').append('<p id=\"err-msg-already\"><img src=\"data:image/svg+xml;base64,PHN2ZyB3aWR0aD0nMjAnIGhlaWdodD0nMjAnIHZpZXdCb3g9JzAgMCAyMCAyMCcgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJz4KCTxnPgoJCTxwYXRoIGQ9J00tMi0yaDI0djI0SC0yeicgZmlsbD0nbm9uZScvPgoJCTxwYXRoIGQ9J00xMCAwQzQuNDggMCAwIDQuNDggMCAxMHM0LjQ4IDEwIDEwIDEwIDEwLTQuNDggMTAtMTBTMTUuNTIgMCAxMCAwem0xIDE1SDl2LTJoMnYyem0wLTRIOVY1aDJ2NnonIGZpbGw9JyNGRjQzNDMnLz4KCTwvZz4KPC9zdmc+Cg==\">You are already registered. Please login.</p>').fadeOut(3500);
 
\t\t\t\t }
\t\t\t\t else{
\t\t\t\t\t  \$('#continuebutton').button('reset'); 
\t\t\t\t\t \$('#error-msg').empty();
                    \$('#error-msg').fadeIn();
                    \$('#error-msg').append('<p id=\"err-msg\"><img src=\"data:image/svg+xml;base64,PHN2ZyB3aWR0aD0nMjAnIGhlaWdodD0nMjAnIHZpZXdCb3g9JzAgMCAyMCAyMCcgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJz4KCTxnPgoJCTxwYXRoIGQ9J00tMi0yaDI0djI0SC0yeicgZmlsbD0nbm9uZScvPgoJCTxwYXRoIGQ9J00xMCAwQzQuNDggMCAwIDQuNDggMCAxMHM0LjQ4IDEwIDEwIDEwIDEwLTQuNDggMTAtMTBTMTUuNTIgMCAxMCAwem0xIDE1SDl2LTJoMnYyem0wLTRIOVY1aDJ2NnonIGZpbGw9JyNGRjQzNDMnLz4KCTwvZz4KPC9zdmc+Cg==\">Verification code Send to '+\$('#continuephonenumber').val()+'</p>').fadeOut(3500);
 
\t\t\t\t\t  \$('#signupotp').empty();
\t\t\t\t\t  \$('#signupotp').val(btoa(btoa(data_array.continue_otp)));
\t\t\t\t\t  var mobilenumber=\$(\"#continuephonenumber\").val();  
\t\t\t\t\t \$(\"#regphonenumber\").empty();
\t\t\t\t\t \$(\"#regphonenumber\").val(mobilenumber);  
\t\t\t\t\t \$(\"#regphonenumber\").prop(\"readonly\", true);
\t\t\t\t\t\t\$(\"#signupcontent\").hide();
\t\t\t\t\t\t\$(\".signup-newcus\").show();
\t\t\t\t\t\t\$(\"#regotp\").focus(); 
\t\t\t\t }
\t\t\t\t
\t\t\t }
\t});
\t
}

function signinotp()
{
\t\$('.regotperror_msg').empty();
\tif (\$('#regotp').val().length=='6') {
\t\tif(\$('#signupotp').val()==btoa(btoa(\$('#regotp').val()))){
\t\t\$('.regotperror_msg').empty();
\t\t\$('.regname_error').empty();
\t\t\$('.regphone_error').empty();
\t\t\$('.regemail_error').empty();
\t\t\$('.regpass_error').empty();
\t\t document.getElementById(\"signupbutton\").disabled = false;
\t}else
\t{
\t\t\$('.regotperror_msg').empty();
\t\t\$('.regotperror_msg').append('OTP is incorrect');
\t\tdocument.getElementById(\"signupbutton\").disabled = true;
\t}
\t}
}


jQuery('.numbersOnly').keyup(function () { 
    this.value = this.value.replace(/[^0-9\\.]/g,'');
});

function forgetotplogin()
{
\t\$('.forgetotptext').empty();
\tif (\$('#forgetotptext').val().length=='6') {
\t\tif(btoa(btoa(\$('#forgetotptext').val()))==\$('#forgetotp').val()){
\t\t\$('.forgetotptext').empty();
\t\t document.getElementById(\"forgrtloginbutton\").disabled = false;
\t\t// document.getElementById('forgetotptext').setAttribute('readonly', 'readonly'); 
\t}else{
\t\t
\t\t\$('.forgetotptext').empty();
\t\t\$('.forgetotptext').append('OTP is incorrect');
\t\tdocument.getElementById(\"forgrtloginbutton\").disabled = true;
\t}
\t}
}


\$(window).load(function() {
\t\$('.my_account_new').hover(function(){
\t\t\$('.dropdown_myacc').toggleClass('acc_active');
\t});
});
</script>
<script>
\$(document).ready(function(){
\$(':input').keyup(function(e) {
    if (e.which == 8 || e.which == 46) {
        \$(this).prev('input').focus();
    }
    else {
        \$(this).next('input').focus();
    }
});
});
\$(document).ready(function(){
\t\$('.countrycode').css('display','none');


\t
  \$('#autofocusemail :input:enabled:visible:first').focus();

\$('.menu-button').click(function(e){
    e.stopPropagation();
     \$('#hide-menu').toggleClass('so-megamenu-active');
\t \$('body').addClass('menu-overlay');
});
\$('#hide-menu').click(function(e){
    e.stopPropagation();
});
\$('#remove-megamenu').click(function() {
\t\$('.megamenu-wrapper').removeClass('so-megamenu-active');
\t\$('body').removeClass('menu-overlay');
\treturn false;
});
\$('body,html').click(function(e){
       \$('#hide-menu').removeClass('so-megamenu-active');
\t   \$('body').removeClass('menu-overlay');
});
});
function wishlistload() {
    var wishlist = \$('#wishlist-total').text();
    if(wishlist == 'Wish List'){
        \$('#wishlist-total').html('<i class=\"fa fa-heart\"></i>');
    }
}
setInterval(wishlistload,20);
</script>
<script>
\tfunction init() {
\tvar vidDefer = document.getElementsByTagName('iframe');
\tfor (var i=0; i<vidDefer.length; i++) {
\tif(vidDefer[i].getAttribute('data-src')) {
\tvidDefer[i].setAttribute('src',vidDefer[i].getAttribute('data-src'));
\t} } }
\twindow.onload = init;


    var image  = \$(\".image-logo img\").attr('src');
    var image1 = image.replace(\"www\",\"s1\");
    \$(\".image-logo img\").attr('src',image1);

    // added by aadhi subash
      document.addEventListener(\"DOMContentLoaded\",()=>{
        var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function(){
                if (this.readyState == 4 && this.status == 200) {
                    var data = this.responseText;
                    var arr = JSON.parse(data);
                   

                    var inp = document.getElementById(\"myInput\");
                    inp.addEventListener('input',function(e){
                        var a, b, i, val = this.value;
                            \$(\".autocomplete-items\").addClass('item_list');
                            let filterdata = arr.filter(it => new RegExp(this.value, \"i\").test(it.name));
                            currentFocus = -1;
                            var b = \"\";
                            
                            for(var i=0;i<filterdata.length;i++){                               
                                if(val.length <= 1){                     
                                    if(filterdata[i].name.substr(0, val.length) == val.toUpperCase()){
                                       b += '<li class=\"liremove\"><a href=\"'+filterdata[i].keyword+'\">'+ filterdata[i].name.toLowerCase() +'</a></li>'; 
                                    }
                                }else{
                                    b += '<li class=\"liremove\"><a href=\"'+filterdata[i].keyword+'\">'+ filterdata[i].name.toLowerCase() +'</a></li>';    
                                }
                                
                            }
                            if(val.length === 0){
                                \$(\".autocomplete-items\").removeClass('item_list');
                                \$(\"#myInputautocomplete-list\").html('');
                            }else{
                                \$(\"#myInputautocomplete-list\").html(b); 
                            }
                            
                       
                    });

                    inp.addEventListener('keydown',function(e){
                        var x = document.getElementById(this.id + \"autocomplete-list\");
                          if (x) x = x.getElementsByTagName(\"li\");
                      
                          if (e.keyCode == 40) {
                            
                            currentFocus++;
                            
                            addActive(x);
                          } else if (e.keyCode == 38) { 
                            
                            currentFocus--;
                            
                            addActive(x);
                        } else if (e.keyCode == 13) {
                            e.preventDefault();
                            var t = document.getElementsByClassName(\"autocomplete-active\")[0];
                        
                            if(typeof t === 'undefined'){
                                var enterinp = document.getElementById(\"myInput\");
                                    var redirect = \"search?name='\" + encodeURIComponent(enterinp.value) +\"'\";
                                    window.location.href = redirect;    
                            }else{
                                var link = t.getElementsByTagName(\"a\")[0].getAttribute(\"href\");
                                window.location.href=link;  
                                
                            }
                           
                        }
                        \$(\"#myInputautocomplete-list\").scrollTop(0);
                        \$(\"#myInputautocomplete-list\").scrollTop(\$('.autocomplete-active:first').offset().top-\$(\"#myInputautocomplete-list\").height());
                    });
                    function addActive(x) {
                        if (!x) return false;

                        removeActive(x);
                        if (currentFocus >= x.length) currentFocus = 0;
                        if (currentFocus < 0) currentFocus = (x.length - 1);
                        
                        x[currentFocus].classList.add(\"autocomplete-active\");
                    }

                    function removeActive(x) {
                        for (var i = 0; i < x.length; i++) {
                          x[i].classList.remove(\"autocomplete-active\");
                        }
                    }
                    document.addEventListener(\"click\", function (e) {
                        \$('.liremove').remove();
                        \$(\".autocomplete-items\").removeClass('item_list');
                    });

                    \$('#submit_search').on('click',function(){
                        var t = document.getElementsByClassName(\"autocomplete-active\")[0];
                        
                            if(typeof t === 'undefined'){
                                var enterinp = document.getElementById(\"myInput\");
                                    var redirect = \"search?name='\" + encodeURIComponent(enterinp.value) +\"'\";
                                    window.location.href = redirect;    
                            }
                    });
                 
                }
            };
            xhttp.open(\"GET\", \"index.php?route=product/search/searchautocomplete\", true);
            xhttp.send();
    });
    // end here
</script>


<script>



\$(\"#main-login\").click(function() {
\t
   \$('#continuephonenumber').val(\"\");
   \$('#emailid').focus();
\t\$(\".emailerror,.highlight\").empty();
});  
\$(\".toggle-password\").click(function() {

  \$(this).toggleClass(\"fa-eye fa-eye-slash\");
  var input = \$(\$(this).attr(\"toggle\"));
  if (input.attr(\"type\") == \"password\") {
    input.attr(\"type\", \"text\");
  } else {
    input.attr(\"type\", \"password\");
  }
});


\$(document).ready(function(){
  \$(\"#fp,#forgotresend\").click(function(){
\t  if(validateEmail(document.loginform.emailid.value))
\t{\t 
     forgetpasswordemail();
\t}else if(validatePhone(document.loginform.emailid.value))
\t{
\tforgetpassworphone();
     }
\telse{
\t\t    \$('.emailerror').empty();
\t\t    \$('.emailerror').append('Please enter valid Email ID/Mobile number');
\t   }
\t
  });
  
  
 /*\$(\"#otp-field\").click(function()
  {
\tif (validateEmail(document.loginform.emailid.value)||validatePhone(document.loginform.emailid.value)){
    \$(\"#hide\").hide();
   \$(\"#otp-head\").show();
    \$(\".otp_password\").empty();
   \$(\".otp_password\").append(document.loginform.emailid.value);
\t}else{
\t}
  });*/
  
  \$(\".Existing-loginbutton\").click(function()
  {
\t  \$(\"#continuephonenumber\").val(\"\");
\t  \$(\"#emailid\").val(\"\");
    \$(\"#signupcontent\").hide();
   \$(\"#hide, .signin, #popupfooter, #otp-field, #fp, .loginbutton1\").show();
  });
  \$(\"#otpchange\").click(function()
  {
    \$(\"#otp-head\").hide();
   \$(\"#hide\").show();
  });
  \$(\".newtopoor\").click(function()
  {
    \$(\"#show\").hide();
   \$(\".signin1, #signupcontent\").show();
  });

  \$(\"#change\").click(function()
  {
    \$(\"#hide, .signin, #popupfooter, #otp-field, #fp, .loginbutton1\").show();
   \$(\"#show\").hide();
  });

  \$(\"#change1\").click(function()
  {
    \$(\"#signupcontent\").show();
   \$(\".signup-newcus\").hide();
  });
  \$(\".loginbutton1\").click(function()
  {
   \$(\"#hide, .signin, #popupfooter\").hide();
   \$(\"#signupcontent, .signin1\").show();
  });
  \$(\"#new-loginbutton\").click(function()
  {
\t \$(\"#modalLoginForm\").modal('show');
  });
  \$(\"#new-regbutton\").click(function()
  {
\t \$(\"#modalLoginForm\").modal('show');
  });
  });
  
  
    

    if (localStorage.getItem('popState') != 'shown') {
          
          \$(\"#modalLoginForm\").modal('show');
        }

        \$('.close').click(function (e) {
            \$('#modalLoginForm').modal('hide');
            localStorage.setItem('popState', 'shown');
        });
        
    

\$(document).ready(function(){
  \$(\"#input22\").change(function()
  {
   \$(\"#result\").show();
  });
  });
  \$(document).ready(function(){
  \$(\"#continuebutton,#resendotpsignup\").click(function()
  { 
 
\t if(validatePhone(document.getElementById(\"continuephonenumber\").value)){
        continuefunction();
\t\t }
\t\t else{
\t\t\t\$('.continue_error').empty();
\t\t    \$('.continue_error').append('Please enter valid Mobile number');
\t\t } 
\t\t
  });
  });

  \$(document).ready(function(){
\$(\".close\").click(function(){ 

\$('.signup-newcus').hide();
  \$(\"#hide, .signin, #popupfooter, #fp, #otp-field, .loginbutton1\").show();
   \$(\"#show, #signupcontent, #otp-head\").hide();
\$('form#contact,form#signin-newcustomer,form#otphead123').trigger(\"reset\");
});
});
  
\$(document).ready(function(){
\$(\"#signupbutton\").click(function()
  {
\t
\t  var regemail=\$(\"#regemail\").val();
\t  var regname=\$(\"#regname\").val();
\t  
\t if(\$(\"#regphonenumber\").val()==''){\$(\".regphone_error\").empty();\$(\".regphone_error\").append(\"PhoneNumber is Required\");return false;}else if(\$(\"#regotp\").val().length!='6'){\$(\".regotperror_msg\").empty();\$(\".regotperror_msg\").append(\"Please enter valid OTP\");return false;}else if(\$(\"#regotp\").val()==''){\$(\".regotperror_msg\").empty();\$(\".regotperror_msg\").append(\"OTP is Required\");return false;}else if(\$(\"#regname\").val()==\"\"){\$(\".regname_error\").empty();\$(\".regname_error\").append(\"Name is Required\");return false;}else if(\$(\"#regemail\").val()==\"\"){\$(\".regemail_error\").empty();\$(\".regemail_error\").append(\"Email is Required\");return false;}else if(!validateEmail(\$(\"#regemail\").val())){\$(\".regemail_error\").empty();\$(\".regemail_error\").append(\"Please enter valid Email ID\");return false;}else if(\$(\"#regpassword\").val()==''){\$(\".regpass_error\").empty();\$(\".regpass_error\").append(\"Password is Required\");return false;}
\t  
\t  \$(\"#signupbutton\").button('loading');
\t  
\t  if(\$(\"#regpassword\").val().length>=4){
\t       //\$.preloader.start({ modal: true, src : 'image/loadspinner/sprites323.png' });
                           //  setTimeout(function(){ \$.preloader.stop(); }, 3000); 
\t\t   \$.ajax({
\t\t\t\t\t\t url: 'index.php?route=login/login/signin',
\t\t\t\t\t\t type: 'POST', 
\t\t\t\t\t\t data:{phnumber:\$(\"#regphonenumber\").val(),otp:\$(\"#regotp\").val(),password:\$(\"#regpassword\").val(),firstname:\$('#regname').val(),email:\$(\"#regemail\").val()}, 
\t\t\t\t\t\t datatype:'json',
\t\t\t\t\t\t success: function (data){
\t\t\t\t\t\t\t// console.log(data);
\t\t\t\t\t\t\t \tvar data_array = jQuery.parseJSON(data);
\t\t\t\t\t\t\t\tif(data_array.status=='insert')
\t\t\t\t\t\t\t\t{
\t\t\t\t\t\t\t\t\t\$(\"#signupbutton\").button('reset');
\t\t\t\t\t\t\t\t\tnewuserlogin(data_array.mobilenumber,data_array.password);\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t\t\t\$(\"#signupbutton\").button('reset');
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t }
\t\t\t\t\t});
\t\t\t\t\t}else{ 
\t\t\t\t\t\$(\"#signupbutton\").button('reset');
\t\t\t\t\t\t\$(\".regpass_error\").empty();
\t\t\t\t\t\t\$(\".regpass_error\").append(\"Password should be minimum 4 characters\");

\t\t\t\t\t}
\t
  });
  });
  
  

  
  
 \$(document).ready(function(){
\$(\".forgetloginbutton\").click(function()
  {
\t  if(validateEmail(\$(\"#forgetphonenumber\").val())&&\$(\"#forgetotptext\").val().length=='6')
\t{\t 
     updatepasswordemail();
\t}else if(validatePhone(\$(\"#forgetphonenumber\").val())&&\$(\"#forgetotptext\").val().length=='6')
\t{
\tupdatepassworphone();
     }else{
\t\t \$('.forgetotptext').empty();
\t\t\$('.forgetotptext').append('Please enter valid OTP');
\t\tdocument.getElementById(\"forgrtloginbutton\").disabled = true;
\t }
\t
  });
  });
  
function updatepassworphone()
  {
\t   //\$.preloader.start({ modal: true, src : 'image/loadspinner/sprites323.png' });
                           //  setTimeout(function(){ \$.preloader.stop(); }, 3000); 
\t \$(\".forgetloginbutton\").button('loading');
\t  if(\$(\"#forgetpassword\").val().length>=4){
\t if(\$(\"#forgetphonenumber\").val()==''){\$(\".forgetphonr_error\").empty();\$(\".forgetphonr_error\").append(\"field is Required\");return false;}else if(\$(\"#forgetotp\").val()==''){\$(\".forgetotptext\").empty();\$(\".forgetotptext\").append(\"OTP is Required\");return false;}else if(\$(\"#forgetpassword\").val()==''){\$(\".forgetpass_error\").empty();\$(\".forgetpass_error\").append(\"Password is Required\");return false;}
\t         \$.ajax({
\t\t\t\t\t\t url: 'index.php?route=login/login/updatephonelogin',
\t\t\t\t\t\t type: 'POST', 
\t\t\t\t\t\t data:{phnumber:\$(\"#forgetphonenumber\").val(),otp:\$(\"#forgetotp\").val(),password:\$(\"#forgetpassword\").val()},
\t\t\t\t\t\t datatype:'json',
\t\t\t\t\t\t success: function (data){
\t\t\t\t\t\t\t \tvar data_array = jQuery.parseJSON(data);
\t\t\t\t\t\t\t\tif(data_array.status=='update')
\t\t\t\t\t\t\t\t{
\t\t\t\t\t\t\t\t\t\$(\".forgetloginbutton\").button('reset');
\t\t\t\t\t\t\t\t\tnewuserlogin(data_array.mobilenumber,data_array.password);
\t\t\t\t\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t\t\t\t\t\$(\".forgetloginbutton\").button('reset');
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t }
\t\t\t\t\t});
\t  }else{
\t\t  \$(\".forgetloginbutton\").button('reset');
\t\t                \$(\".forgetpass_error\").empty();
\t\t\t\t\t\t\$(\".forgetpass_error\").append(\"Password should be minimum 4 characters\");

\t  }
  }

\$(\"input#forgetpassword,input#regpassword,input#password,input#emailid\").on({
  keydown: function(e) { 
    if (e.which === 32)
      return false;
  },
  change: function() {
    this.value = this.value.replace(/\\s/g, \"\");
  }
});

  function updatepasswordemail()
  {
\t  // \$.preloader.start({ modal: true, src : 'image/loadspinner/sprites323.png' });
                           //  setTimeout(function(){ \$.preloader.stop(); }, 3000); 
\t\t\t\t\t\t   \$(\".forgetloginbutton\").button('loading');
\t\t\t\t\t\t   
\t  if(\$(\"#forgetpassword\").val().length>=4){
\t if(\$(\"#forgetphonenumber\").val()==''){\$(\".forgetphonr_error\").empty();\$(\".forgetphonr_error\").append(\"field is Required\");return false;}else if(\$(\"#forgetotp\").val()==''){\$(\".forgetotptext\").empty();\$(\".forgetotptext\").append(\"OTP is Required\");return false;}else if(\$(\"#forgetpassword\").val()==''){\$(\".forgetpass_error\").empty();\$(\".forgetpass_error\").append(\"Password is Required\");return false;}
\t         \$.ajax({
\t\t\t\t\t\t url: 'index.php?route=login/login/updateemaillogin',
\t\t\t\t\t\t type: 'POST', 
\t\t\t\t\t\t data:{email:\$(\"#forgetphonenumber\").val(),otp:\$(\"#forgetotp\").val(),password:\$(\"#forgetpassword\").val()},
\t\t\t\t\t\t datatype:'json',
\t\t\t\t\t\t success: function (data){
\t\t\t\t\t\t\t \tvar data_array = jQuery.parseJSON(data);
\t\t\t\t\t\t\t\tif(data_array.status=='update')
\t\t\t\t\t\t\t\t{
\t\t\t\t\t\t\t\t\t\$(\".forgetloginbutton\").button('reset');
\t\t\t\t\t\t\t\t\tnewuserlogin(data_array.email,data_array.password);
\t\t\t\t\t\t\t\t\t\t}else{
\t\t\t\t\t\t\t\t\t\t\t\$(\".forgetloginbutton\").button('reset');
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t }
\t\t\t\t\t});
\t  }else{
\t\t  \$(\".forgetloginbutton\").button('reset');
\t\t    \$(\".forgetpass_error\").empty();
\t\t\t\$(\".forgetpass_error\").append(\"Password should be minimum 4 characters\");
\t  }
  }
  
  
  
   
 function newuserlogin(mobilenumber,password) { 
\t\t\t\t\t\$.ajax({
\t\t\t\t\t\t url: 'index.php?route=login/login/otplogin',
\t\t\t\t\t\t type: 'POST', 
\t\t\t\t\t\t data:{email:mobilenumber,password:password},
\t\t\t\t\t\t datatype:'json',
\t\t\t\t\t\t success: function(data){\t
                            var url      = window.location.href;  
\t\t\t\t if(url==\"http://poorvikabeta.webindia.com/index.php?route=account/logout\"){
\t\t\t\t\t \$('#modalLoginForm').modal('hide');
            localStorage.setItem('popState', 'shown');
\t\t\t\t\t location.replace(\"http://poorvikabeta.webindia.com/\");
\t\t\t\t }else{
\t\t\t\t\t \$('#modalLoginForm').modal('hide');
            localStorage.setItem('popState', 'shown');
\t\t\t\t\t window.location.reload();
\t\t\t\t }\t\t
\t\t\t\t\t\t }
\t\t\t\t\t});
                 }
 
</script>
 <script type=\"text/javascript\">

\t\$(\"#contact_submit button\").click(function(event){

\t\t\t\tvar form_data=\$(\"#contact\").serializeArray();
\t\t\t\tvar error_free=true;
\t\t\t\tfor (var input in form_data){
\t\t\t\t\tvar element=\$(\"#contact_\"+form_data[input]['name']);
\t\t\t\t\tvar valid=element.hasClass(\"valid\");
\t\t\t\t\tvar error_element=\$(\"span\", element.parent());
\t\t\t\t\tif (!valid){error_element.removeClass(\"error\").addClass(\"error_show\"); error_free=false;}
\t\t\t\t\telse{error_element.removeClass(\"error_show\").addClass(\"error\");}
\t\t\t\t}
\t\t\t\tif (!error_free){
\t\t\t\t\tevent.preventDefault(); 
\t\t\t\t}
\t\t\t\telse{
\t\t\t\t\talert('No errors: Form will be submitted');
\t\t\t\t}
\t\t\t});
\t\t\t







function validateForm() {
  var x = document.forms[\"myForm\"][\"name\"].value;
  if (x == \"\") {
    alert(\"Name must be filled out\");
    return false;
  }
}
  // Email-Address Validation
  
function emailfunction(){
  var email = document.getElementById(\"input22\");
  var filter = /^([a-zA-Z0-9_\\.\\-])+\\@(([a-zA-Z0-9\\-])+\\.)+([a-zA-Z0-9]{2,4})+\$/;
  if(!filter.test(email.value)){
    document.getElementById(\"result\").innerHTML=\"Enter ur Correct Email\"
    email.focus;
    return false
  }
}

</script>
<script>
      var obj = document.getElementById('partitioned');
      obj.addEventListener('keydown', stopCarret); 
      obj.addEventListener('keyup', stopCarret); 

      function stopCarret() {
        if (obj.value.length > 5){
          setCaretPosition(obj, 5);
        }
      }

      function setCaretPosition(elem, caretPos) {
          if(elem != null) {
              if(elem.createTextRange) {
                  var range = elem.createTextRange();
                  range.move('character', caretPos);
                  range.select();
              }
              else {
                  if(elem.selectionStart) {
                      elem.focus();
                      elem.setSelectionRange(caretPos, caretPos);
                  }
                  else
                      elem.focus();
              }
          }
      }
 </script>
 <style>

/**===== dots5 =====*/
#dots5 {
  display: block;
  position: absolute;
  left: 80%;
  height: 50px;
  width: 50px;
  margin: -25px 0 0 0px;
}

#dots5 span {
  position: absolute;
  width: 10px;
  height: 10px;
  background: rgba(0, 0, 0, 0.25);
  border-radius: 50%;
  -webkit-animation: dots5 1s infinite ease-in-out;
          animation: dots5 1s infinite ease-in-out;
}

#dots5 span:nth-child(1) {
  left: 0px;
  -webkit-animation-delay: 0.2s;
          animation-delay: 0.2s;
}

#dots5 span:nth-child(2) {
  left: 15px;
  -webkit-animation-delay: 0.3s;
          animation-delay: 0.3s;
}

#dots5 span:nth-child(3) {
  left: 30px;
  -webkit-animation-delay: 0.4s;
          animation-delay: 0.4s;
}

#dots5 span:nth-child(4) {
  left: 45px;
  -webkit-animation-delay: 0.5s;
          animation-delay: 0.5s;
}

@keyframes dots5 {
  0% {
    -webkit-transform: translateY(0px);
            transform: translateY(0px);
    -webkit-transform: translateY(0px);
            transform: translateY(0px);
    background: #d62d20;
  }
  25% {
    -webkit-transform: translateY(10px);
            transform: translateY(10px);
    -webkit-transform: translateY(10px);
            transform: translateY(10px);
    background: #ffa700;
  }
  50% {
    -webkit-transform: translateY(10px);
            transform: translateY(10px);
    -webkit-transform: translateY(10px);
            transform: translateY(10px);
    background: #008744;
  }
  100% {
    -webkit-transform: translateY(0px);
            transform: translateY(0px);
    -webkit-transform: translateY(0px);
            transform: translateY(0px);
    background: #0057e7;
  }
}
@-webkit-keyframes dots5 {
  0% {
    -webkit-transform: translateY(0px);
            transform: translateY(0px);
    background: #d62d20;
  }
  25% {
    -webkit-transform: translateY(10px);
            transform: translateY(10px);
    background: #ffa700;
  }
  50% {
    -webkit-transform: translateY(10px);
            transform: translateY(10px);
    background: #008744;
  }
  100% {
    -webkit-transform: translateY(0px);
            transform: translateY(0px);
    background: #0057e7;
  }
}
/** END of dots5 */


input[type=number]:focus
{
  background-color: transparent;
  border-bottom: 1px solid #de490a;
}
.otp-verify-button
{
      width: 325px;
    height: 49px;
    background: #f5641b;
    box-shadow: 0 5px 5px 0 rgba(0,0,0,.2);
    border: none;
    margin-top: 40px;
    color: white;
}
\t.container-megamenu.horizontal ul.megamenu .sub-menu .content .hover-menu .menu ul a.main-menu
\t{
\t\tcolor: black!important;
\t}
  .Existing-loginbutton:hover
  {
    background-color: white!important;
  }
  .header-otp{
    display: flex;
    width: 100%;
    margin-top: 15px;
  }
  .header-otp1 input
  {
    border: 0;
    outline: 0;
    background: transparent;
    border-bottom: 1px solid black;
    width: 12%;
    text-align: center;
    padding: 5px;
    margin-left: 10px;
    border-radius: inherit;
  }
  .regemaillabel
  {
    top: -15px!important;
  }
  input[type=password]
  { 
    transition: none!important;
  }
  input[type=\"number\"]:hover
  {
    background-color:white!important;
  }
\t.close:hover
\t{
\t\topacity: 1!important;
\t}
\t.close
\t{
\t\topacity: 1!important;
\t}
  #error-msg
  {
    
    left: 0;
    right: 0;
    bottom: 100px;
    margin: auto;
    display:none;
    position: absolute;
    color: white;
    height: 43px;
    width: 374px;
    z-index: 999999;
    background: #083044;
    box-shadow: 0 10px 6px -6px #777;
    border-radius: 3px;
    font-size: 16px;
    text-align: center;
    padding-top: 10px;
    margin-top: -10px;
  }
  #err-msg-verification img
  {
        margin-right:8px;

  }
  #err-msg
  {
    color: white;
    height: 43px;
    width: 200px;
    background: #083044;
    box-shadow: 0 10px 6px -6px #777;
    border-radius: 3px;
    font-size: 16px;
    margin: auto;
    padding-top: 10px;
    margin-top: -10px;
  }
  #err-msg-already
  {
  \t color: white;
    height: 43px;
    width: 348px;
    background: #083044;
    box-shadow: 0 10px 6px -6px #777;
    border-radius: 3px;
    font-size: 16px;
    margin: auto;
    padding-top: 10px;
    margin-top: -10px;
  }
  #err-msg img
  {
    margin-right:8px;
  }
  .countrycode
  {
    left: 0;
    padding-right: 5px;
    border-right: 1px solid #c2c2c2;
    bottom: 7px;
    font-size: 14px;
    position: absolute;
    display: inline-block;
  }
   .countrycode1
  {
    left: 0;
    padding-right: 5px;
    border-right: 1px solid #c2c2c2;
    bottom: 7px;
    font-size: 14px;
    position: absolute;
    display: inline-block;
  }
  .signup-newcus
  {
    margin-top:19px;
  }
  .signupbutton
  {
    margin-bottom: 20px;
    width: 325px;
    height: 49px;
    background: #fb641b;
    box-shadow: 0 5px 5px 0 rgba(0,0,0,.2);
    border: none;
    margin-top: 13px;
  }
  .signupbutton:hover
  {
  \t    background: #fb641b!important;

  }
.redcolor
{
\tcolor:red;
  float: left;
}
span.emailerror
{
  float:left;
}
span.regotperror_msg.redcolor
{
  float:left;
}
button.btn.btn-lg.forgetloginbutton
{
    width: 325px;
    height: 49px;
    background: #fb641b;
    box-shadow: 0 1px 2px 0 rgba(0,0,0,.2);
    border: none;
    color: #fff;
    margin-top: 13px;
}
#signupcontent
{
  margin-top:35px;
}
.countrycode
{
\tbottom: 9px;
\tposition: absolute;
    top: 6px;
    font-size: 14px;
    left: 0;
    padding-right: 5px;
    border-right: 1px solid #c2c2c2;
}
.form-control1 {
    border: 0;
    outline: 0;
    background: transparent;
    border-bottom: 1px solid black;
    width: 20%;
    text-align: center;
    padding: 5px;
    margin-left: 10px;
}
#partitioned {
    padding-left: 20px;
    letter-spacing: 38px;
    background-image: linear-gradient(to left, #94918f 75%, rgba(255, 255, 255, 0) 0%);
    background-position: bottom;
    background-size: 47px 1px;
    background-repeat: repeat-x;
    background-position-x: 90px;
    width: 305px;
    min-width: 305px;
\tborder: none !important;
}
#partitioned:focus {
  outline: 0;
}
#divInner{
  left: 0;
  position: sticky;
}

#divOuter{
  width: 290px; 
  overflow: hidden;
  margin: auto;
}
input.floating-input
{
\tborder-bottom:1px solid #0000002e!important;
    border-radius: 0px;
    background:white!important;
    padding-left:0px!important;
}
input.floating-input:focus
{
  background:white!important;
  padding-left:0px!important;
}

.modal-login
{
\t  margin-top:70px!important;
\t  background: inherit!important;

}
input[type=\"text\"]
{
  background-color:white!important;
  border-bottom:1px solid #0000002e!important;
  border-radius:0px!important;
}
input[type=\"text\"]:focus, input[type=\"text\"]:hover
{
  background-color:white;
}
.floating-form {
  width:320px;
}

/****  floating-Lable style start ****/
.floating-label { 
  position:relative; 
  margin-bottom:30px; 
}
.floating-input , .floating-select {
  font-size:14px;
  padding:4px 4px;
  display:block;
  width:100%;
  height:36px;
  background-color: transparent;
  border:none;
  border-bottom:1px solid #757575;
}

.floating-input:focus , .floating-select:focus {
     outline:none!important;
     border-bottom:1px solid #f67100!important; 
}

.floating-label label {
  color:#999; 
  font-size:14px;
  font-weight:normal;
  position:absolute;
  pointer-events:none;
  left:0px;
  top:-18px;
  transition:0.2s ease all; 
  -moz-transition:0.2s ease all; 
  -webkit-transition:0.2s ease all;
}
.floating-input:focus ~ label{
  top:-18px;
  font-size:12px;
  color:#999;
}


.floating-input:focus ~ label , .floating-input:not([value=\"\"]):valid ~ label {
  top:-18px;
  font-size:14px;
}

/* active state */
.floating-input:focus ~ .bar:before, .floating-input:focus ~ .bar:after, .floating-select:focus ~ .bar:before, .floating-select:focus ~ .bar:after {
  width:50%;
}

*, *:before, *:after {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

.floating-textarea {
   min-height: 30px;
   max-height: 260px; 
   overflow:hidden;
  overflow-x: hidden; 
}

/* highlighter */
.highlight {
  position: absolute;
    height: 50%;
    width: 100%;
    top: 105%;
    left: -100px;
    pointer-events: none;
}

/* active state */
.floating-input:focus ~ .highlight , .floating-select:focus ~ .highlight {
  -webkit-animation:inputHighlighter 0.3s ease;
  -moz-animation:inputHighlighter 0.3s ease;
  animation:inputHighlighter 0.3s ease;
      display:none;

}

/* animation */
@-webkit-keyframes inputHighlighter {
\tfrom { background:#5264AE; }
  to \t{ width:0; background:transparent; }
}
@-moz-keyframes inputHighlighter {
\tfrom { background:#5264AE; }
  to \t{ width:0; background:transparent; }
}
@keyframes inputHighlighter {
\tfrom { background:#5264AE; }
  to \t{ width:0; background:transparent; }
}

/****  floating-Lable style end ***

/***   Body style start  ***/

html {
    font-family: \"Helvetica Neue\", Helvetica, \"Noto Sans\", sans-serif, Arial, sans-serif;
    font-size: 12px;
    line-height: 1.42857143;
    color: #949494;
    background-color: #ffffff;
}
/***   Body style end  ***/


/***   daniel - Fork me friend - style   ***/
.floating-credit { position:fixed; bottom:10px;right:10px; color:#aaa; font-size:13px;font-family:arial,sans-serif; }
.floating-credit a { text-decoration:none; color:#000000; font-weight:bold; }
.floating-credit a:hover { border-bottom:1px dotted #f8f8f8; }
.floating-heading { position:fixed; color:#aaa; font-size:20px; font-family:arial,sans-serif; }
/***  daniel - Fork me friend - style  ***/

.newtopoor
{
  background:white!important;
  border-color:white!important;
  color:#fb641b!important;
  font-weight:500;
}
#secondheader{
\tposition:inherit!important;
}
.resendcode
{
  color:#f79000!important;
  font-weight:500;
}
.otp-field1
{
  border:none!important;
  border-bottom: 1px solid #e0e0e0!important;
width:28px;
text-align:center;
border-radius:0px!important;
background-color:white!important;
padding:0px!important;
margin-right:10px;
}
#otp-head
{
 padding-top: 30px;
    font-size: 16px;
    color: black;
    text-align: center;
}
#show
{
  margin-top:30px;
}
  <!--Popup Design-->
input[type=\"text\"]
{
background-color:white!important;
}
.loginbutton1{
\tposition: absolute;
margin-top:70px;
    left: 0;
    right: 0;
    text-align: center;
    font-weight: 500;
    font-size: 14px;
    color:#2874f0;
    cursor: pointer;
}
.verifybutton
{
  width: 260px;
    height: 49px; 
    background-color:#FF5722!important;
    box-shadow: 0 1px 2px 0 rgba(0,0,0,.2);
    border: none;
    color: #fff;
    margin-top:40px;
}
.verifybutton:hover
{
  background: #FF5722;
}
#otp-field:hover
{
\tbackground:none!important;
\tbackground-color:none!important;
}
.modal-content
  {
    width: 670px!important;
    height: 520px!important;
    border:none!important;
    border-radius: inherit;
    margin-top: 70px!important;
    margin: auto;
  }
  .otp-field:hover
  {
    background-color: #f5641b!important;
  }
  .loginbutton:hover
  {
    background-color: rgb(245 100 27)!important;
  }
 .loginbutton
  {
    width: 325px;
    height: 49px;
    background:#ff531f;
    box-shadow: 0 5px 5px 0 rgba(0,0,0,.2);
    border: none;
    margin-top:13px;
    
  }
  .Existing-loginbutton
  {
    width: 325px;
    height: 49px;
    background: #fb641b;
    box-shadow: 0 5px 5px 0 rgba(0,0,0,.2);
    border: none;
    margin-top:13px;
  }
  #continuebutton
  {
    width: 325px;
    height: 49px; 
    background:#FF5722;
    box-shadow: 0 1px 2px 0 rgba(0,0,0,.2);
    border: none;
    color: #fff;
    margin-bottom:15px;
  }
  #otp-field
  {
    width: 325px;
    height: 49px;
    background: transparent;
    box-shadow: 0 5px 2px 0 rgba(0,0,0,.2);
    border: none;
    font-size:15px;
    color: #f55b00;
    font-weight:500;
  }
  .newtopoor
  {
\t  width: 325px;
    height: 49px;
    background: transparent;
    box-shadow: 0 1px 2px 0 rgba(0,0,0,.2);
    border: none;
\tfont-size:16px;
    color: #000;
  }
 .btn:focus
 {
  color: white!important;
  outline: none!important;
 }
  .loginleft
  {
    height: 520px;
    background: #ff531f;
    padding: 40px 35px;
    font-size: 16px;
    color: white;
  }
.modal-footer
{
  text-align: center!important;
  border:none!important;
    padding-bottom: 0px!important;
    padding-top:30px;
    padding:0px;
}
.wrap-input2 {
  width: 100%;
  position: relative;
border-bottom: 2px solid #3c3c3c;
  margin-bottom: 37px;
}
.input2 
{
  display: block;
  width: 100%;
  font-size: 15px;
  color: #555555;
  line-height: 1.2;
}
.focus-input2 {
  position: absolute;
  display: block;
  width: 100%;
  height: 100%;
  top: 0;
  text-align:left!important;
  left: 0;
  pointer-events: none;
}
.focus-input2::before {
  content: \"\";
  display: block;
  position: absolute;
  bottom: -2px;
  left: 0;
  width: 0;
  height: 2px;
}

.focus-input2::after {
  content: attr(data-placeholder);
  display: block;
  width: 100%;
  position: absolute;
  top: -13px!important;
  left: 0;
  font-family: Poppins-Regular;
  font-size: 13px;
  color: #999999;
  line-height: 1.2;
}

input.input2 {
  height: 40px;
  border: none;
  background:white!important;
}

input.input2 + .focus-input2::after {
  top: 16px;
  left: 0;
}

textarea.input2 {
  min-height: 115px;
  padding-top: 13px;
  padding-bottom: 13px;
}

textarea.input2 + .focus-input2::after {
  top: 16px;
  left: 0;
}

.input2:focus + .focus-input2::after {
  top: -25px;
  
}

.input2:focus + .focus-input2::before {
  width: 100%;
}

.has-val.input2 + .focus-input2::after {
  top: -13px;
}

.has-val.input2 + .focus-input2::before {
  width: 100%;
}
.modal-body
{
  width: 320px;
    padding: 33px 0px!important;
    margin-left: 33px;
}
.modal-content p
{
  font-size: 20px;
}
.input2:focus
{
  outline: none!important;
}
.forgetpsw
{
 font-size: 14px;
 text-align: right; 
  font-weight: 500;
}
.forgetpsw a
{
color: #2874f0;
font-weight:500px;
}
button.close
{
  font-size: 50px!important;
    color: white;
    margin-top: -12px;
    font-weight: 100;
}
.or
{
  padding: 13px;
    font-weight: 600;
}
.signin
{
    font-size: 25px;
    text-align: center;
    color: #000;
    font-weight: 700;
    margin-bottom:40px;
}
.signin1
{
  font-size: 24px;
    text-align: center;
    padding-bottom: 45px;
color: #3c3c3c;
    font-weight: 700;
}
.fa-fw
{
  float: right!important;
    position: relative!important;
    margin-top: -20px!important;
}
.fa-eye:before
{
  font-weight: 500;
    color: #7d7d7d;
}
.fa-fw
{
  color:#7d7d7d; 
}
.resendotp 
{
  position: relative;
    font-weight: 600;
    float: right;
    margin-top: -25px;
}
.resendotp a
{
   color: #f55100;
   font-size:14px;
}
.test
{
  height: 515px!important;
}
input[type=number]::-webkit-inner-spin-button, 
input[type=number]::-webkit-outer-spin-button
 { 
  -webkit-appearance: none; 
  margin: 0; 
}
#result
{
  margin-top: -37px;
    margin-bottom: 36px;
}
@media only screen and (max-width: 900px) {
  .modal-body {
    width: 50%;
    padding: 0px 0px;
    margin-left: 24px;
  }
  .loginbutton {
    width: 100%;
    height: 48px;
  }

    .loginbutton1 {
    width: 100%;
    height: 42px;
  }
  .modal-content
  {
    width: 100%!important;
  }
}
<!--End-->

/* Begin Saerch bar style */
.container-megamenu.horizontal ul.megamenu > li {
    margin-right: 0 !important;
}
.typeheader-2 .search-header-w {
    margin: 12px 10px 10px 32px;
}
.typeheader-2 .search-header-w .input-group{
    display: block;
    width: 100%;
    float: left;
}
.search-header-w .input-group .button-search {
    right: 0px;
    background-image: linear-gradient(45deg,#dc4300 ,#f57000a3);
        width: 80px !important;
    line-height: 34px;
    position: absolute;
    top: 0px;
    text-align: center;
    padding: 0;
}.input-group .form-control:first-child, .input-group-addon:first-child, .input-group-btn:first-child>.btn, .input-group-btn:first-child>.btn-group>.btn, .input-group-btn:first-child>.dropdown-toggle, .input-group-btn:last-child>.btn-group:not(:last-child)>.btn, .input-group-btn:last-child>.btn:not(:last-child):not(.dropdown-toggle)
{
    border-radius:inherit;
    border:none!important;
}
.typeheader-2 .search-header-w input {
    height: 35px !important;
    padding-left: 22px;
    background: #dcd1d180 !important;
    border: 1px solid #c5c5c5 !important;
    font-size: 14px;
    width:525px;
}
#myInput::-webkit-input-placeholder { 
  color: #808080;
  font-size: 12px;
  letter-spacing: 0.5px;
}
.autocomplete-items.dropdown-menu{
    padding: 0;
    margin: 0;
    min-width: 350px;
    overflow: hidden scroll;
    border: 0;
    max-height: 450px;
    left: 0;
}
.autocomplete {
    position: relative;
    display: inline-block;
  }

.autocomplete-items.dropdown-menu li {
    padding: 6px;
    margin: 0;
    border-bottom: 1px solid #e5e5e5;
}
li.liremove a {
    text-transform: capitalize;
    color: #000;
    font-weight: 500;
}
li.liremove:hover, li.liremove.autocomplete-active{
      background: #f1f1f1;
}
li.liremove.autocomplete-active a{
      color: #fda30e;
}

.typeheader-2 .search-header-w input:hover{
    border-color: #fd6500 !important;
}
.item_list{
    display: block !important;
    opacity: 1 !important;
    visibility: visible !important;
}

/* End Saerch bar style */

#widget-global-52a2ipbiyj .lc-8cxdov {
   color: #0e0d0d !important;
}
.phone_price {
    background: url(https://s1.poorvikamobile.com/image/data/Back%20Case/price-back.jpg) no-repeat;
    background-position: center;
    background-size: cover;
    padding-top:25px;
\tmargin-top: 25px
}
.price_head {
    display: flex;
    justify-content: center;
    padding-left: 5px;
}
ul.price_head li {
    padding: 7px 21px;
    border: 3px solid #fff;
    border-radius: 20px;
\tmargin-right: 15px;
}
ul.price_head li a {
    color: #fff;
    font-size: 15px;
\tfont-weight: 600;
}
.tab-content{
\tborder:none;
\tpadding-bottom:20px;
\tmargin:0;
}
ul.price_head li.active, ul.price_head li:hover {
    background: #ff6600;
\tborder: 3px solid #f60;
}
.budget_list ul li {
    display: inline-block;
    padding: 5px 10px;
    background: #fff;
    border-radius: 3px;
\ttext-align: center;
}
.budget_list ul{
\ttext-align: center;
}

.budget_list ul a {
    color: #000;
    font-size: 12px;
    font-weight: 600;
}
.budget_list.camera_price{
    width: 80%;
    margin: 0 auto;
}
.budget_list.camera_price li {
    width: 18%;
}
.budget_list ul li:hover {
    background: #18ab97;
}
.budget_list ul li a:hover, .budget_list ul li:hover a {
    color: #fff;
}
.dealsproduct_pos span {
    width: 50px;
    height: 50px;
    color: #fff;
    background: #04a211;
    font-weight: 600;
    display: block;
    line-height: 50px;
    text-align: center;
    font-size: 18px;
    margin: 12px auto 10px;
    border-radius: 50%;
}
.product_deals_page{
\tpadding: 35px 0 50px;
}
.dealsproduct_pos img{
\tborder:none;
}
.dealsproduct_grd h5{
\ttext-align: center;
    font-weight: 500;
    color: #ea732b;
    font-size: 15px;
}
.product_deals_page h1 {
    color: #000;
    font-size: 25px;
}
.common-deals ul.breadcrumb{
\t    margin-bottom: 10px;
}
.product_deals_page ul {
\ttext-align: center;
\tmargin-bottom: 30px;
}
.product_deals_page ul li{
\tdisplay: inline-block;
}
.product_deals_page ul li a{
    padding: 8px 12px;
    color: #fff;
    background: #000;
    display: block;
    text-transform: uppercase;
    font-size: 14px;
    letter-spacing: 1px;
    border-radius: 4px;
}
.product_deals_page ul li a.active{
\tbackground: #fda30e;
}
.my_deals_offer_new, .my_deals_offer{
    padding: 18px 0;
    margin-bottom: 20px;
    border-radius: 5px;
\tbackground: #e0e0e0;
    box-shadow: 0px 0px 5px #828282;
}
.my_deals_offer_new{
\theight: 350px;
}
.deals_title_four{
\tbackground: #5b9426;
}
.deals_title_five{
\tbackground: #313f88;
}
.deals_title_six{
\tbackground: #e87800;
}

.round_effect .round_btm div a {
    display: block;
    position: relative;
    overflow: hidden;
}
.round_effect .round_btm div a:hover:before, .round_effect .round_btm div a:hover:after {
    border: 0 solid rgba(255, 255, 255, 0.7);
    opacity: 0;
    filter: alpha(opacity=0);
}
.round_effect .round_btm div a:before, .round_effect .round_btm div a:after {
    border: 50px solid transparent;
    border-top-right-radius: 50px;
    border-top-left-radius: 50px;
    border-bottom-right-radius: 50px;
    border-bottom-left-radius: 50px;
    box-sizing: border-box;
    cursor: pointer;
    display: inline-block;
    left: 0;
    right: 0;
    bottom: 0;
    margin: auto;
    position: absolute;
    top: 0;
    content: \"\";
    opacity: 1;
    filter: alpha(opacity=100);
    width: 100px;
    height: 100px;
    -webkit-transform: scale(7);
    -moz-transform: scale(7);
    -ms-transform: scale(7);
    -o-transform: scale(7);
    transform: scale(7);
    transition: all 0.4s ease-in-out;
    -moz-transition: all 0.4s ease-in-out;
    -webkit-transition: all 0.4s ease-in-out;
    visibility: visible;
    z-index: 1;
}
#so-groups3 {
    top: 55%;
    position: fixed;
    right: 0;
    z-index: 999;
    /*background: #f60;*/
    width: 50px;
}
.whatsapp {
    float: left;
    width: 100%;
    display: block;
    cursor: help;
    text-align: center;
    color: #fff;
    border-bottom: 1px solid rgba(255,255,255,.5);
    padding: 10px;
    height: 43px;
    position: relative;
}
.whatsapp a{
    font-size: 26px;
    color: #fff;
}
#so-groups > div > i {
    font-size: 25px;
}
.left_groups >div span {
    left: 100% !important;
    right: inherit !important;
}
#so-groups > div:hover {
    background-color: #ec3a01;
}
#so-groups > div:hover span{
    opacity: 1;
    transition: all 0.2s ease-in-out 0s;
    visibility: visible;
    width: auto;
}

#so-groups1 {
    top: 65%;
    position: fixed;
    right: 0;
    z-index: 999;
    background: #ccc;
    width: 42px;
}
#so-groups1 .whatsapp {
    float: left;
    width: 100%;
    display: block;
    cursor: help;
    text-align: center;
    color: #fff;
    border-bottom: 1px solid rgba(255,255,255,.5);
    padding: 5px;
    height: inherit !important;
    position: relative;
}
#so-groups1 .whatsapp a{
    font-size: 26px;
    color: #fff;
}
#so-groups1 > div span {
background: #0089fe;
    color: #ffffff;
    display: inline-block;
    font-size: 18px;
    line-height: 22px;
    opacity: 0;
    padding: 10px;
    position: absolute;
    right: 100%;
    text-align: center;
    text-transform: capitalize;
    top: 0;
    transition: all 0.2s ease-in-out 0s;
    visibility: hidden;
    white-space: nowrap;
    width: auto;
}
#so-groups1 > div > i {
    font-size: 25px;
}


#so-groups1 > div:hover span{
    opacity: 1;
    transition: all 0.2s ease-in-out 0s;
    visibility: visible;
    width: auto;
}

#header .container {
    max-width: 1650px;
    padding: 0;
    width: 85%!important;
}
.typeheader-2 .header-bottom
{
    background-color:transparent!important;
}

.my_account_new .dropdown_myacc
{
    display: none;
    background: white;
    width: 210px;
    color:black;
    z-index:999;
}
.acc
{
    color: black!important;
    font-size: 14px!important;
    padding-right: 8px;
}
.typeheader-2 ul.top-link li
{
    padding-top: 8px;
    font-size: 13px;
}
hr
{
    margin-top:0px!important;
    border-top: 1px solid #f5690145;
}
.shopcart
{
    color:#FF9800!important;
}
.typeheader-2 .shopping_cart .btn-shopping-cart .total-shopping-cart .items_cart
{
    background-image: linear-gradient(45deg,#dc4300 ,#f57000a3);
    }
.typeheader-2 .container-megamenu.horizontal ul.megamenu.menu__list li a
{
    padding: 18px 20px !important;
    color:white!important;
    font-size: 14px!important;
}

@media (max-width:1080px) {
\t.typeheader-2 .container-megamenu.vertical:hover .vertical-wrapper{
\t\ttop: 48px !important;
\t}
\tul.price_head li {
\t\tpadding: 7px 15px;
\t}
}
@media (max-width:991px) {
\t.product_deals_page ul li a {
\t\tmargin-bottom: 8px;
\t}
\t.dealsproduct_grd {
\t\tmargin: 8px 0;
\t}
\t.my_deals_offer_new {
\t\theight: inherit;
\t}
\tul.price_head li {
\t\tpadding: 7px 5px;
\t}
\tul.price_head li a {
\t\tfont-size: 13px;
\t}
\t.budget_list.camera_price{
\t\twidth: 100%;
\t}
}
@media (max-width:767px) {
\tul.price_head li a {
\t\tfont-size: 12px;
\t}
\tul.price_head li{
\t\tmargin-right: 7px;
\t\tborder-radius: 10px;
\t}
\t.budget_list.camera_price li {
    width: 20%;
}
}
@media (max-width:720px) {
  .budget_list ul li a {
      padding: 12px;
  }
  ul.price_head li {
    padding: 5px 5px !important;
  }
  ul.price_head li a {
    font-size: 10px !important;
  }
}
@media (max-width:585px) {
\tul.price_head li a {
\t\tfont-size: 11px !important;
\t}
\tul.price_head li{
\t\ttext-align: center;
\t}
\t.budget_list.camera_price li {
\t\twidth: 25%;
\t}
}

</style>
 <script src=\"image/loadspinner/src/jquery.preloaders.js\"></script>
 <script src=\"http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/js/owl.carousel.min.js\"></script> 
<script>
 \$(document).ready(function(){
  \$('.home-video-carousel1').owlCarousel({
    loop:true,
   autoplay:true,
    autoplayTimeout:3000,
    autoplayHoverPause:true,
    nav:true,
\tdots:false,
    responsive:{
        0:{
            items:1
        },
\t\t320:{
            items:1
        },
\t\t480:{
            items:1
        },
        600:{
            items:2
        },
\t\t767:{
            items:2
        },
\t\t991:{
            items:3
        },
        1200:{
            items:4
        }
    }
})
\$( \".owl-prev\").html('<i class=\"fa fa-lg fa-arrow-left\"></i>');
 \$( \".owl-next\").html('<i class=\"fa fa-lg fa-arrow-right\"></i>');
});\t

</script>
<script>
 \$(document).ready(function(){
  \$('.brand-logo-carousel').owlCarousel({
    loop:true,
   autoplay:true,
    autoplayTimeout:3000,
    autoplayHoverPause:true,
    nav:false,
\tdots:false,
    responsive:{
        0:{
            items:1
        },
\t\t320:{
            items:2
        },
\t\t480:{
            items:3
        },
        600:{
            items:4
        },
\t\t767:{
            items:5
        },
\t\t991:{
            items:7
        },
        1200:{
            items:10
        }
    }
})
\$( \".owl-prev\").html('<i class=\"fa fa-lg fa-angle-left\"></i>');
 \$( \".owl-next\").html('<i class=\"fa fa-lg fa-angle-right\"></i>');
});\t

</script>
<script>
    \$(function(){
      // bind change event to select
      \$('#top_search1').on('change', function () {
          var url = \$(this).val(); // get selected value
          if (url) { // require a URL
              window.location = url; // redirect
          }
          return false;
      });
    });
</script>
<script>
window.onscroll = function() {myFunction()};

var header = document.getElementById(\"scrollHeader\");
var sticky = header.offsetTop;

function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add(\"sticky\");
  } else {
    header.classList.remove(\"sticky\");
  }
}
</script>";
    }

    public function getTemplateName()
    {
        return "so-destino/template/header/header5.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  666 => 370,  658 => 365,  652 => 361,  642 => 360,  640 => 359,  636 => 357,  626 => 356,  624 => 355,  619 => 352,  612 => 351,  605 => 350,  598 => 348,  594 => 347,  581 => 336,  572 => 332,  567 => 329,  565 => 328,  561 => 326,  556 => 324,  551 => 323,  544 => 321,  540 => 320,  521 => 309,  517 => 307,  515 => 306,  510 => 303,  503 => 301,  496 => 300,  489 => 298,  485 => 297,  468 => 287,  388 => 210,  383 => 207,  378 => 204,  372 => 203,  370 => 202,  367 => 201,  365 => 200,  357 => 195,  350 => 190,  344 => 189,  338 => 188,  331 => 186,  324 => 185,  321 => 184,  314 => 183,  310 => 182,  304 => 181,  300 => 179,  293 => 178,  289 => 176,  282 => 175,  278 => 174,  271 => 172,  267 => 170,  260 => 169,  257 => 168,  250 => 167,  246 => 166,  239 => 164,  235 => 163,  221 => 152,  167 => 100,  158 => 93,  151 => 89,  146 => 87,  135 => 78,  129 => 76,  126 => 75,  124 => 74,  120 => 72,  109 => 70,  103 => 69,  100 => 68,  98 => 67,  69 => 42,  59 => 35,  27 => 6,  23 => 4,  21 => 3,  19 => 2,);
    }
}
/* {#=====Get variable : Config Select Block on header=====#}*/
/* {% set hidden_headercenter = soconfig.get_settings('toppanel_type') =='2'? 'hidden-compact' : '' %}*/
/* {% set hidden_headerbottom = soconfig.get_settings('toppanel_type') =='1'? 'hidden-compact' : '' %}*/
/* <link rel="stylesheet" type="text/css" href="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/css/owl.carousel.min.css" />*/
/* <link rel="stylesheet" type="text/css" href="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/css/owl.theme.default.min.css" />*/
/* <header id="header scrollHeader" class="sticky variant typeheader-{{ typeheader ? typeheader : '1'}}">*/
/* 	<!-- HEADER TOP -->*/
/* 	<div class="header-top compact-hidden custom">*/
/* 	    <style>*/
/* 	        */
/* 	        #so-groups{*/
/* 	            display:none;*/
/* 	        }*/
/* 	        */
/* 	        a.dropdown-toggle.account{*/
/* 	            color:#fff;*/
/* 	        }*/
/* 	        #acc_div{*/
/* 	            height: 30px;*/
/*                 border-radius: 50% !important;*/
/*                 width: 40px;*/
/*                 padding-top: 5px;*/
/* 	        }*/
/* 	        #accimg{*/
/* 	            height: 30px;*/
/*                 border-radius: 50%;*/
/* 	        }*/
/* 	        .sticky {*/
/*   position: fixed;*/
/*   top: 0;*/
/*   width: 100%;*/
/*   z-index:3;*/
/* }*/
/* 	    </style>*/
/* 	    {% if logged %} */
/* 	        <style>*/
/* 	        .typeheader-5 ul.top-link > li.account::after{*/
/* 	            display:none;*/
/* 	        }*/
/* 	        </style>*/
/* 	    {% endif %}*/
/* 	<a href="{{Session}}"></a>*/
/* 	     <!-- HEADER TOP TITLE BAR Antony-->*/
/*   <!-- <div class="header-top-title-bar">*/
/* 	<div class="row top-row">*/
/* 	<div class="col-12 col-sm-12 col-md-4 col-xs-4">*/
/* 		<i class="fa fa-pie-chart" aria-hidden="true" class="chart"></i>*/
/* */
/* 		<i class="fa fa-comment-o" aria-hidden="true" class="comment"></i>*/
/* */
/*         </div>*/
/*         <div class="col-12 col-sm-12 col-md-4 col-xs-4">*/
/*         	<p class="title-bar">New Website Layout</p>*/
/*         </div>*/
/*         <div class="col-12 col-sm-12 col-md-4 col-xs-4 button-section">*/
/*         	<i class="fa fa-question" aria-hidden="true"></i>*/
/*         	<button class="btn btn-info question" type="submit"><span>Login or Create Account </span></button>*/
/* */
/*         </div>*/
/*     </div>*/
/*     </div> -->*/
/*       <!-- HEADER TOP TITLE BAR Antony-->*/
/* 		<div class="inner">*/
/* 			<div class="row">*/
/* 				<div class="header-top-left col-lg-1 col-md-1 col-sm-1 col-xs-6">*/
/* 					<!-- LANGUAGE CURENCY -->*/
/* 				<!--	{% if soconfig.get_settings('lang_status') %}*/
/* 					<ul class="top-link list-inline lang-curr">*/
/* 						{% if currency %}<li class="currency"> {{ currency }}  </li> {% endif %}*/
/* 						{% if language %} <li class="language">{{ language }} </li>	{% endif %}			*/
/* 					</ul>				*/
/* 					{% endif %} */
/* 					*/
/* 					{% if soconfig.get_settings('phone_status') and soconfig.get_settings('contact_number') %}*/
/* 					<div class="telephone hidden-xs hidden-sm hidden-md" >*/
/* 						{{ soconfig.decode_entities( soconfig.get_settings('contact_number') ) }}*/
/* 					</div>*/
/* 					{% endif %}-->*/
/* 					<div id="wrapper">*/
/*         <div class="overlay"></div>*/
/*     */
/*         <!-- Sidebar -->*/
/*         <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">*/
/*             <ul class="nav sidebar-nav">*/
/*                */
/* 							*/
/* 							{% if logged %} */
/* 							  <li class="sidebar-brand">*/
/*                     <a  style="font-size: 15px; vertical-align: text-bottom; font-weight:500;" href="{{logout}}">*/
/*                        Sign Out*/
/*                     </a>*/
/*                 </li> {% else %}*/
/* 							 <li class="sidebar-brand">*/
/*                     <a data-toggle="modal" id="main-login" style="font-size: 15px; vertical-align: text-bottom; font-weight:500;" data-target="#modalLoginForm" href="#">*/
/*                        Sign In & Sign Up*/
/*                     </a>*/
/*                 </li>*/
/* 										*/
/* 						{% endif %}*/
/* 				*/
/* 				*/
/* 				*/
/* 				*/
/*                 <li>*/
/*                     <a href="#" >Home</a>*/
/*                 </li>*/
/*                 <li>Categories</li>*/
/*                 <li class="dropdown">*/
/*                   <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mobiles & Tablets<span class="caret"></span></a>*/
/*                   <ul class="dropdown-menu" role="menu">*/
/*                     <li><a href="#">Smartphones</a></li>*/
/*                     <li><a href="#">Wearable Tech</a></li>*/
/*                     <li><a href="#">Mobile, Tablet Accessories</a></li>*/
/*                     <li><a href="#">Headphones & Headsets</a></li>*/
/*                     <li><a href="#">Tablets & eReaders</a></li>*/
/*                     <li><a href="#">Power Banks</a></li>*/
/*                     <li><a href="#">Mobile Cases & Protectors</a></li>*/
/*                   </ul>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="#">Computers</a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="#">Cameras</a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="#">Television & Audio</a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="#">Accessories</a>*/
/*                 </li>*/
/*             </ul>*/
/*         </nav>*/
/*         <!-- /#sidebar-wrapper -->*/
/*         <!-- Page Content -->*/
/*         <div id="page-content-wrapper">*/
/*             <button type="button" class="hamburger is-closed" data-toggle="offcanvas">*/
/*                 <span class="hamb-top"></span>*/
/*           <span class="hamb-middle"></span>*/
/*         <span class="hamb-bottom"></span>*/
/*             </button>*/
/*         </div>*/
/*         <!-- /#page-content-wrapper -->*/
/* */
/* */
/*     </div>*/
/*     <!-- /#wrapper -->*/
/* 					*/
/* 				</div>*/
/* 				<div class="navbar-logo col-lg-2 col-md-2  col-sm-4 col-xs-12">*/
/* 					<div class="logo">*/
/* 				   		{{soconfig.get_logo()}} */
/* 				   	<!--<a href="http://poorvikabeta.webindia.com/index.php?route=common/home">*/
/* 				   	    <img class="lazyautosizes lazyloaded" data-sizes="auto" src="https://s1.poorvikamobile.com/image/data/poorvika_mobile_five/poorvika-4-logo.png" data-src="https://s1.poorvikamobile.com/image/data/poorvika_mobile_five/poorvika-4-logo.png" title="Poorvika" alt="Poorvika" sizes="194px">*/
/* 				   	    </a>-->*/
/* 				   	</div>*/
/* 				</div>*/
/* 				<div class="col-lg-4 col-md-4  col-sm-3 col-xs-12 cata-parent">*/
/* 				   */
/* 					<div class="search-header-w cata-child">*/
/* 					    <div class="choose-cata">*/
/* 					<select class="form-control option cata-child-opn" id="top_search1">*/
/* 					    <option value="">{{ text_category_all  }} </option>*/
/* 					{% for category in categories %} */
/* 						*/
/* 						{% if category.category_id  ==  category_id %} */
/* 							<option value="{{ category.href }} " selected="selected">{{ category.name }} </option>*/
/* 						{% else %}   */
/* 							<option value="{{ category.href }} ">{{ category.name }} </option>*/
/* 						{% endif %} */
/* 						*/
/* 						{% for category_lv2 in category.children %} */
/* 							*/
/* 							{% if category_lv2.category_id  ==  category_id %} */
/* 								<option value="{{ category_lv2.href }} " selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{ category_lv2.name }} </option>*/
/* 							{% else %}   */
/* 								*/
/* 								<option value="{{ category_lv2.href }}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{category_lv2.name}} </option>*/
/* 							{% endif %} */
/* 							*/
/* 							{% for category_lv3 in category_lv2.children %} */
/* 								{% if category_lv3.category_id  ==  category_id %} */
/* 									<option value="{{ category_lv3.href }} " selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ category_lv3.name }} </option>*/
/* 								{% else %}   */
/* 									<option value="{{ category_lv3.href }} ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ category_lv3.name }} </option>*/
/* 								{% endif %} */
/* 							{% endfor %}*/
/* 						{% endfor %}*/
/* 					{% endfor %}*/
/* 					*/
/* 					  */
/* 					  */
/* 					</select>*/
/* 					</div>	*/
/* 					{{ search_block }}*/
/* 				    </div>*/
/* 				</div>*/
/* 				<div class="header-top-right collapsed-block col-lg-5 col-md-5  col-sm-4 col-xs-6 ">*/
/* 					<ul class="top-link list-inline">*/
/* 						{% if soconfig.get_settings('welcome_message_status') %}*/
/* 							<li class="hidden-sm hidden-xs welcome-msg">*/
/* 								{% if soconfig.get_settings('welcome_message') is not empty %}*/
/* 									{{ soconfig.decode_entities( soconfig.get_settings('welcome_message') ) }}*/
/* 								{% endif %} */
/* 							</li>*/
/* 						{% endif %}*/
/* 						*/
/* 						<li class="cart-bag col-lg-4 col-md-4  col-sm-4 col-xs-12">*/
/*     						<div class="shopping_cart">							*/
/*             				 	{{ cart }}*/
/*             				</div>*/
/*         				</li>*/
/*         				<!-- <li><a class="stores">Stores</a></li> -->*/
/*                         <li class="dropdown locate col-lg-4 col-md-4  col-sm-4 col-xs-12">*/
/*           <a href="#" class="dropdown-toggle stores" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Locate <br><h5>Stores</h5><span class="caret"></span></a>*/
/*           <ul class="dropdown-menu locator">*/
/*            */
/*             <li class="pincode">*/
/*                         <span>*/
/*                                     <input type="text" placeholder="Enter Pincode, City or State" value="" class="form-control" id="pincode" name="txtemail" size="55">*/
/*                                */
/*                                  <button class="check" type="submit" name="submit">Check</button></span>*/
/*                              </li>*/
/*                              <li class="search-bar">*/
/*                                */
/*                                     <select class="form-control option">*/
/*                          <option value="" disabled selected>Select State</option>*/
/*                       <option value="Andhra Pradesh">Andhra Pradesh</option>*/
/* <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>*/
/* <option value="Arunachal Pradesh">Arunachal Pradesh</option>*/
/* <option value="Assam">Assam</option>*/
/* <option value="Bihar">Bihar</option>*/
/* <option value="Chandigarh">Chandigarh</option>*/
/* <option value="Chhattisgarh">Chhattisgarh</option>*/
/* <option value="Dadar and Nagar Haveli">Dadar and Nagar Haveli</option>*/
/* <option value="Daman and Diu">Daman and Diu</option>*/
/* <option value="Delhi">Delhi</option>*/
/* <option value="Lakshadweep">Lakshadweep</option>*/
/* <option value="Puducherry">Puducherry</option>*/
/* <option value="Goa">Goa</option>*/
/* <option value="Gujarat">Gujarat</option>*/
/* <option value="Haryana">Haryana</option>*/
/* <option value="Himachal Pradesh">Himachal Pradesh</option>*/
/* <option value="Jammu and Kashmir">Jammu and Kashmir</option>*/
/* <option value="Jharkhand">Jharkhand</option>*/
/* <option value="Karnataka">Karnataka</option>*/
/* <option value="Kerala">Kerala</option>*/
/* <option value="Madhya Pradesh">Madhya Pradesh</option>*/
/* <option value="Maharashtra">Maharashtra</option>*/
/* <option value="Manipur">Manipur</option>*/
/* <option value="Meghalaya">Meghalaya</option>*/
/* <option value="Mizoram">Mizoram</option>*/
/* <option value="Nagaland">Nagaland</option>*/
/* <option value="Odisha">Odisha</option>*/
/* <option value="Punjab">Punjab</option>*/
/* <option value="Rajasthan">Rajasthan</option>*/
/* <option value="Sikkim">Sikkim</option>*/
/* <option value="Tamil Nadu">Tamil Nadu</option>*/
/* <option value="Telangana">Telangana</option>*/
/* <option value="Tripura">Tripura</option>*/
/* <option value="Uttar Pradesh">Uttar Pradesh</option>*/
/* <option value="Uttarakhand">Uttarakhand</option>*/
/* <option value="West Bengal">West Bengal</option>*/
/*                      */
/*                     </select>*/
/*                       </li>*/
/*                              <li>*/
/*                             */
/*                                     <select class="form-control option">*/
/*                          <option value="" disabled selected>Select City</option>*/
/*                       <option value="Andhra Pradesh">Chennai</option>*/
/*                                         </select>*/
/* */
/*                                  </li>*/
/*                              <li>*/
/*                                         <select class="form-control option">*/
/*                          <option value="" disabled selected>Select Area</option>*/
/*                       <option value="Andhra Pradesh">Anna Nagar</option>*/
/*                                         </select>*/
/*                                           </li>*/
/*                              */
/*                                */
/*                         </li>*/
/*           </ul>*/
/*         </li>*/
/*         <!-- <li class="dropdown account col-lg-4 col-md-4  col-sm-4 col-xs-12">*/
/*           <a href="index.php?route=account/account" class="dropdown-toggle account" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{% if logged %} {{ customername }} {% endif %} 's<br>Account <span class="caret"></span></a>*/
/*           <ul class="dropdown-menu my-account">*/
/*             <li><a href="index.php?route=account/order" class="order">My Order</a></li>*/
/*             <li><a href="index.php?route=account/wishlist" class="wishlist">Wishlist & Saved</a></li>*/
/*             <li><a href="index.php?route=account/address" class="address">Saved Address</a></li>*/
/*             <li><a href="#" class="gift">Gift Card</a></li>*/
/*             <li class="border"><a href="#" class="coupon">Coupons</a></li>*/
/*             <li><a href="#" class="contact">Contact Us</a></li>*/
/*             <li><a href="index.php?route=account/account" class="profile">My Profile</a></li>*/
/*             <li><a href="#" class="notification">Notification</a></li>*/
/*             {% if logged %} */
/* 				 <li><a href="{{ logout }}" class="logout">{{ text_logout }}</a></li>							*/
/* 				  {% else %}*/
/* 				<li><a href="{{ login }}" class="logout">{{ text_login }}</a></li>*/
/* 				<li><a href="{{ register }}" class="logout">{{ text_register }}</a></li> 							*/
/* 			{% endif %}*/
/*           </ul>*/
/*         </li> -->*/
/*         <li class="dropdown account col-lg-4 col-md-4  col-sm-4 col-xs-12">*/
/*             {% if logged %}*/
/*                 <div class="col-lg-8 col-md-8  col-sm-8 col-xs-8">*/
/*             {% endif %}*/
/*           <a href="index.php?route=account/account" class="dropdown-toggle account" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{% if logged %} {{ customername }} 's <br> {% endif %} Account <span class="caret"></span></a>*/
/*           */
/*           <ul class="dropdown-menu my-account">*/
/*             <li><a href="index.php?route=account/order" class="order">My Order</a></li>*/
/*             <li><a href="index.php?route=account/wishlist" class="wishlist">Wishlist & Saved</a></li>*/
/*             <li><a href="index.php?route=account/address" class="address">Saved Address</a></li>*/
/*             <li><a href="#" class="gift">Gift Card</a></li>*/
/*             <li class="border"><a href="#" class="coupon">Coupons</a></li>*/
/*             <li><a href="#" class="contact">Contact Us</a></li>*/
/*             <li><a href="index.php?route=account/account" class="profile">My Profile</a></li>*/
/*             <li><a href="#" class="notification">Notification</a></li>*/
/*             {% if logged %} */
/* 				 <li><a href="{{ logout }}" class="logout">{{ text_logout }}</a></li>							*/
/* 				  {% else %}*/
/* 				<li id="new-loginbutton"><a class="logout">{{ text_login }}</a></li>*/
/* 				<li id="new-regbutton"><a class="logout">{{ text_register }}</a></li> 							*/
/* 			{% endif %}*/
/*           </ul>*/
/*           */
/*           {% if logged %}*/
/*           </div>*/
/*           <div class="col-lg-4 col-md-4  col-sm-4 col-xs-4">*/
/*               <div id="acc_div">*/
/*                   <img id="accimg" data-sizes="auto" src="{{ cust_image }}" data-src="{{ cust_image }}" sizes="134px">*/
/*               </div>*/
/*           </div>*/
/*           {% endif %}*/
/*         </li>*/
/*     </div>*/
/*                          		*/
/*                          	*/
/*                          	*/
/*                          	</div>*/
/*                          	*/
/*                          	*/
/*                          </div>*/
/* */
/*         				*/
/* 						<!--{% if logged %} */
/* 							 <li><a href="{{ logout }}">{{ text_logout }}</a></li>							*/
/* 							  {% else %}*/
/* 							<li><a href="{{ login }}">{{ text_login }}</a></li>*/
/* 							<li><a href="{{ register }}">{{ text_register }}</a></li> 							*/
/* 						{% endif %}-->*/
/* */
/* 						<!-- WISHLIST  -->*/
/* 						{% if wishlist_status %}*/
/* 							<li class="wishlist"><a id="wishlist-total" class="btn-link" href="{{ wishlist }}"  title="{{ text_wishlist }}">{{ text_wishlist }}</a></li>*/
/* 						{% endif %}	*/
/* 						<!-- checkout -->*/
/* 						{% if soconfig.get_settings('checkout_status') %}*/
/* 							<li class="checkout hidden-xs"><a href="{{ checkout }} " class="btn-link" title="{{ text_checkout }} "><span >{{ text_checkout }} </span></a></li>*/
/* 						{% endif %} 											*/
/* 					</ul>*/
/* 				</div>*/
/* 					<!-- HEADER CENTER -->*/
/* 	<div class="header-center {{hidden_headercenter}}">*/
/* 		<div class="container">*/
/* */
/* 			<div class="main-menu-w">*/
/* 			<!-- Main menu -->				*/
/* 			  {{ content_menu1 }}*/
/* 			</div>*/
/* 			<div class="cart-search">*/
/* 				*/
/* */
/* 			*/
/* 			</div>*/
/* 		</div>*/
/* 	</div>*/
/* 				*/
/* 			</div>*/
/* 			*/
/* 		</div>*/
/* 		*/
/* 	</div>*/
/* */
/* 		<script type="text/javascript">*/
/*   $(document).ready(function () {*/
/*   var trigger = $('.hamburger'),*/
/*       overlay = $('.overlay'),*/
/*      isClosed = false;*/
/* */
/*     trigger.click(function () {*/
/*       hamburger_cross();      */
/*     });*/
/* */
/*     function hamburger_cross() {*/
/* */
/*       if (isClosed == true) {          */
/*         overlay.hide();*/
/*         trigger.removeClass('is-open');*/
/*         trigger.addClass('is-closed');*/
/*         isClosed = false;*/
/*       } else {   */
/*         overlay.show();*/
/*         trigger.removeClass('is-closed');*/
/*         trigger.addClass('is-open');*/
/*         isClosed = true;*/
/*       }*/
/*   }*/
/*   */
/*   $('[data-toggle="offcanvas"]').click(function () {*/
/*         $('#wrapper').toggleClass('toggled');*/
/*   });  */
/* });*/
/* </script>*/
/* */
/* </header>*/
/* */
/* */
/* */
/* <!-- LOGIN POPUP-->*/
/* 	<!-- HEADER TOP -->*/
/* 	<input type="hidden" name="signupotp" id="signupotp">*/
/* 	<input type="hidden" name="msgotp" id="msgotp">*/
/* 	<input type="hidden" name="forgetotp" id="forgetotp">*/
/* 	<!-- HEADER CENTER -->			*/
/* 	<!--LoginPopup-->*/
/* 	<div class="modal fade" id="modalLoginForm" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel"*/
/*     style="cursor: context-menu;">*/
/*  */
/*   <div class="modal-dialog modal-login" role="document">*/
/*     <div class="modal-content" >*/
/*     	 <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: white; margin-top: -13px;">*/
/* <span aria-hidden="true" onclick="resetForm();"style="position: absolute;">×</span>*/
/* </button>*/
/*       <div class="col-md-5 col-xs-5 loginleft">*/
/*          <img src="https://demo1.gyso.in/apx/image/data/pvk-logo/poovika_new_mobile.png" class="img-responsive" style="margin-bottom: 40px!important;">   */
/*                          <!--  <h2 style="margin-bottom: 30px;">Login</h2>    --> */
/*                   <img src="https://s1.poorvikamobile.com/image/data/poorvika_mobile_five/oppo-side-banner.jpg" class="img-responsive"> */
/* */
/*       </div>*/
/*       <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">*/
/*           <span aria-hidden="true">&times;</span>*/
/*         </button> -->*/
/* */
/*       <div class="modal-body col-md-7 col-xs-7">*/
/*        */
/* 		<!--<div class="" style="display:none;">SIGN UP</div>-->*/
/*         <!--Main Page(Sign IN)-->*/
/*         <div id="hide">*/
/*         <div class="signin">SIGN IN</div>*/
/*      <form id="contact" method="post" action="" name="loginform">*/
/*         <div class="floating-label" data-validate="Name is required" name="myform">*/
/*            <input class="floating-input firstemailid" id="emailid"  type="text" onkeyup="lookupemail(this);" name="emailid" autofocus placeholder="">*/
/*             <span class="emailerror" style="color:red;"></span>*/
/*             <label>Enter Email/Mobile Number</label> */
/*             	<span class="countrycode">+91</span>*/
/*             <!-- <span class="countrycode">+91</span> -->*/
/*           </div>*/
/*           <div class="floating-label" data-validate="Name is required" name="myform">*/
/*           */
/*             <input class="floating-input" type="password" onkeyup="lookup(this);" style=""height:45px; name="password" id="password" placeholder="">*/
/*            <!-- <span class="highlight"></span>-->*/
/*             <span class="highlight" style="color:red; z-index:-1;"></span>*/
/*             <label style="margin-top: 7px;">Enter Password</label>  */
/*           </div>*/
/*                        <!-- Forget Password Field-->*/
/*            <!--<div class="forgetpsw"><b id="fp"><a href="JavaScript:Void(0);">Forget Password?</a></b></div>-->*/
/*             <div class="forgetpsw"><b id="fp"><a>Forgot Password?</a></b></div>*/
/*            */
/*             <!--End-->*/
/*             <div id="popupfooter">*/
/*       <div class="modal-footer d-flex justify-content-center">*/
/*       <div id="contact_submit">			*/
/*         <button type="button" class="btn btn-lg loginbutton" onclick="MyLogin()">Login</button>  */
/*      </div>    */
/* 	 */
/* */
/* 	 */
/* 		<div class="or">OR</div>*/
/*         <button type="button" class="btn btn-lg"  onclick="myFunction()" id="otp-field">Request OTP</button>*/
/*         */
/*       </div> */
/* */
/*   </form>*/
/* */
/*          */
/*  <div class="loginbutton1">New to Poorvika? Create an account</div>*/
/*     </div>*/
/*                    </form>*/
/*                    </div>*/
/*            <!-- End-->*/
/* 				   <!--Sign up Field-->*/
/* 				   */
/* 	 <div id="signupcontent" style="display:none;">*/
/*           <div class="floating-label" data-validate="Name is required" name="myform">*/
/*             <input class="floating-input firstemailid numbersOnly" type="number" onclick="lookup(this)" name="continuephonenumber" id="continuephonenumber" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==10) return false;"  placeholder="" style="padding-left: 37px!important;">*/
/*             <span class="continue_error redcolor"></span>*/
/*             <label>Enter Mobile Number</label>*/
/*             <span class="countrycode1">+91</span>*/
/*           </div>*/
/*           <div style="text-align: left;">By continuing, you agree to Poorvika <span style="color: #ff5722;"><a href="https://www.poorvikamobile.com/terms-and-conditions" style="color: #ff5721;">Terms of Use</a></span> and <span style="color: #ff5722;">Privacy Policy.</span></div>*/
/*           <button class="btn btn-lg" id="continuebutton">Continue</button>*/
/*           <button class="btn btn-lg Existing-loginbutton" style="font-size:15px; color: #ff5722; font-weight: 500; background: transparent;">Existing User?Log in</button>*/
/*           </div>*/
/*            <!--End-->				   */
/* 				   <!--Forget Password-->*/
/*             <div style="display: none;" id="show">*/
/*              <div class="floating-label" data-validate="Name is required" name="myform">*/
/*             <input class="floating-input firstemailid" type="text" name="forgetphonenumber" id="forgetphonenumber" class="forgetphonenumber" placeholder=""><span class="resendotp"><b id="change"><a href="JavaScript:Void(0);">Change?</a></b></span>*/
/*             <span class="forgetphonr_error"></span>*/
/*             <label>Mobile Number</label>*/
/*             <span class="countrycode">+91</span>*/
/*           </div>*/
/*                        <!-- <div class="wrap-input2 validate-input" data-validate="Name is required">*/
/*             <input class="input2" type="text" name="name"><span class="resendotp"><b id="change"><a href="JavaScript:Void(0);">Change?</a></b></span>*/
/*             <span class="focus-input2" data-placeholder="Enter Your Phone Number"></span>*/
/*           </div> -->*/
/*           <div class="floating-label" data-validate="Name is required" name="myform">*/
/*             <input class="floating-input numbersOnly"  maxlength="6" name="forgetotptext"  onkeyup="forgetotplogin()" id="forgetotptext" placeholder=""><span class="resendotp" id="forgotresend"><a href="JavaScript:Void(0);">Resend?</a></span>*/
/*             <span class="forgetotptext redcolor"></span>*/
/*             <label style="top: -18px!important;">Enter OTP</label>*/
/*           </div>*/
/*           <!-- <div class="wrap-input2 validate-input" data-validate="Name is required">*/
/*             <input class="input2" type="number" name="otp" min="1" max="4"><span class="resendotp"><a href="JavaScript:Void(0);">Resend?</a></span>*/
/*             <span class="focus-input2" data-placeholder="Enter Your OTP"></span>*/
/*           </div> -->*/
/*           <div class="floating-label" data-validate="Name is required" name="myform">*/
/*             <input class="floating-input" type="password" name="forgetpassword"  id="forgetpassword" placeholder="">*/
/*             <span class="forgetpass_error redcolor"></span>*/
/*             <label style="margin-top:6px;">Set Password</label>*/
/*           </div>*/
/*           <!-- <div class="wrap-input2 validate-input" data-validate="Name is required">*/
/*             <input class="input2" type="Password" name="Password">*/
/*             <span class="focus-input2" data-placeholder="Set New Password"></span>*/
/*           </div> -->*/
/*           <button class="btn btn-lg forgetloginbutton" id="forgrtloginbutton" style="margin-bottom: 20px;">Login</button> */
/*           <button class="btn btn-lg newtopoor">New to Poorvika ? Sign Up</button>*/
/*           </div>*/
/*           <!--End-->*/
/* 	<!--Otp Field--> */
/*             <div class="signup-newcus" style="display:none;">*/
/* 			*/
/* 			<form id="signin-newcustomer">*/
/* 			<div class="floating-label" data-validate="Name is required" name="myform">*/
/*             <input class="floating-input numbersOnly" maxlength="6" name="regotp" onkeyup="signinotp()" id="regotp" placeholder="" style="border: none; border:none; text-align: left; font-size: 14px;"><span class="resendotp" id="resendotpsignup"><a href="JavaScript:Void(0);">Resend?</a></span>*/
/*             <span class="regotperror_msg redcolor" style="color:red;"></span>*/
/*             <label style="top:-18px!important;">Enter OTP</label>*/
/*             </div> */
/* 			<div style="margin-bottom: 29px;"class="floating-label" data-validate="Name is required" name="myform">*/
/*             <input class="floating-input" type="text" name="regname" onkeyup="signinotp()" id="regname"  placeholder="">*/
/*             <span class="regname_error redcolor"></span>*/
/*             <label>Name</label>*/
/*             </div>*/
/* 			*/
/* 			<!--<div class="floating-label" data-validate="gender is required" name="myform">*/
/* 			<input type="radio" value="Male" name="reggender" style="margin-top: 10px; margin-left: 116px;"><span style="margin-left: 6px;*/
/*     margin-right: 41px;*/
/*     font-size: 15px;*/
/*     color: #FF5722;*/
/*     font-weight: 600;">Male</span>*/
/* 			<input type="radio" value="FeMale" name="reggender" style="margin-top: 10px;"><span style="margin-left: 6px;*/
/*     margin-right: 41px;*/
/*     font-size: 14px;*/
/*     color: #FF5722;*/
/*     font-weight: 600;">Female</span>*/
/*             <label>Gender</label>*/
/*             </div>-->*/
/* 			*/
/* 			*/
/*             <div class="floating-label" data-validate="Name is required" name="myform">*/
/*             <input class="floating-input" type="number" name="regphonenumber" id="regphonenumber" placeholder="" style="border:none; text-align: left; font-size: 14px; padding-left: 37px!important;"><span class="resendotp"><b id="change1"><a href="JavaScript:Void(0);">Change?</a></b></span>*/
/*             <span class="regphone_error redcolor"></span>*/
/*             <label>Mobile Number</label>*/
/*              <span class="countrycode1">+91</span>*/
/* */
/*             </div>*/
/* 			*/
/* 			*/
/* 			 <div class="floating-label" data-validate="Name is required" name="myform">*/
/* 			<input class="floating-input" type="email" onkeyup="signinotp()" name="regemail" id="regemail" required placeholder="" style="border:none; text-align: left; font-size: 14px;">*/
/* 			<span class="regemail_error redcolor"></span>*/
/* 			<label class="regemaillabel">Email</label>*/
/* 			</div>*/
/* 			*/
/* 			*/
/* 			*/
/*                 <!-- <div class="wrap-input2 validate-input" data-validate="Name is required">*/
/*             <input class="input2" type="text" name="name"><span class="resendotp"><b id="change"><a href="JavaScript:Void(0);">Change?</a></b></span>*/
/*             <span class="focus-input2" data-placeholder="Enter Your Phone Number"></span>*/
/*           </div> -->*/
/*          */
/*           <!-- <div class="wrap-input2 validate-input" data-validate="Name is required">*/
/*             <input class="input2" type="number" name="otp" min="1" max="4"><span class="resendotp"><a href="JavaScript:Void(0);">Resend?</a></span>*/
/*             <span class="focus-input2" data-placeholder="Enter Your OTP"></span>*/
/*           </div> -->*/
/*           <div class="floating-label" data-validate="Name is required" name="myform">*/
/*             <input class="floating-input" type="password" onkeyup="signinotp()" name="regpassword" id="regpassword" placeholder="">*/
/*             <span class="regpass_error redcolor"></span>*/
/*             <label>Set Password</label>*/
/*           </div>*/
/*           <button type="button" class="btn btn-lg signupbutton" id="signupbutton" style="margin-bottom: 20px;">Sign Up</button> */
/*           */
/* </form>*/
/* </div>*/
/*   <!--SignUp New Customer-->*/
/*   <form id="otphead123">*/
/*         <div style="display: none;" id="otp-head">*/
/* 			  <div>Please enter the OTP sent to</div> <div><p class="otp_password"></p>*/
/* 			  <span id="otpchange" style="color:#f68600; font-weight:500;"><p id="currentphnumber"></p>Change?</span></div>*/
/* 			  <!--<div style="display:flex; margin-left: auto; margin-right: auto; display: block; margin-top: 50px;">*/
/* 			    <div id="divOuter">*/
/* 					<div id="divInner">*/
/* 						<input id="partitioned" type="text" maxlength="6" onkeyup="verifyotp()" onKeyPress="if(this.value.length==6) return false;"/>*/
/* 					</div>*/
/* 				</div>*/
/* 	*/
/* 			  </div>-->*/
/*              <div class="header-otp1">*/
/*     <input id="sendotpemail-first1" class="form-control1" type="number" onkeyup="verifyotp()" onKeyPress="if(this.value.length==1) return false;"/>*/
/*     <input id="sendotpemail-first2" class="form-control1" type="number" onkeyup="verifyotp()" onKeyPress="if(this.value.length==1) return false;"/>*/
/*     <input id="sendotpemail-first3" class="form-control1" type="number" onkeyup="verifyotp()" onKeyPress="if(this.value.length==1) return false;"/>*/
/*     <input id="sendotpemail-first4" class="form-control1" type="number" onkeyup="verifyotp()" onKeyPress="if(this.value.length==1) return false;"/>*/
/*     <input id="sendotpemail-first5" class="form-control1" type="number" onkeyup="verifyotp()"  onKeyPress="if(this.value.length==1) return false;"/>*/
/*     <input id="sendotpemail-first6" class="form-control1" type="number" onkeyup="verifyotp()"  onKeyPress="if(this.value.length==1) return false;"/>*/
/*              </div>   */
/*             <!-- <div style="text-align: center; margin-top: 20px;">Try to Auto Capture</div>-->*/
/* 			<span id="emailotp_error" class="red-color"></span>*/
/*              <div class="otp-verify"> */
/*               <button type="button" id="email_verifyotp" onclick="emailotpverify()" class="otp-verify-button">Verify</button>*/
/*              </div>*/
/* */
/* 			  <!--<div class="btn btn-lg verifybutton" onclick="verifyotp()">Verify</div> -->*/
/* 			  <p style="margin-top:25px; font-size:14px;">Not received your code? <span class="resendcode" onclick="myFunction()">Resend Code</span></p>*/
/*         </div>*/
/*         </div>*/
/*           </form>*/
/*               <!--End-->*/
/*         */
/* <!-- Button Field-->*/
/*     */
/*     </div>*/
/*   </div>*/
/*   */
/* */
/* </div>*/
/* */
/*  </div>*/
/* 	<!--End-->*/
/* 					*/
/* 					*/
/* 						</div>*/
/* 			*/
/* 					*/
/* 				</div>*/
/* 				<div class="clearfix"></div>*/
/* 			</div>*/
/* 		</div>*/
/*     <div id="error-msg"></div>*/
/* */
/* 	</div>*/
/*   */
/* 	<!-- HEADER BOTTOM -->*/
/* 	*/
/* 	*/
/* 	<!-- Navbar switcher -->*/
/* 	<?php if (!isset($toppanel_status) || $toppanel_status != 0) : ?>*/
/* 	<?php if (!isset($toppanel_type) || $toppanel_type != 2 ) :  ?>*/
/* 	<div class="navbar-switcher-container" style="display:none;">*/
/* 		<div class="navbar-switcher">*/
/* 			<span class="i-inactive">*/
/* 				<i class="fa fa-caret-down"></i>*/
/* 			</span>*/
/* 			 <span class="i-active fa fa-times"></span>*/
/* 		</div>*/
/* 	</div>*/
/* 	<?php endif; ?>*/
/* 	<?php endif; ?>*/
/* </header>*/
/* */
/* */
/* */
/* <script>*/
/* function validateEmail(email) { */
/*  var mailFormat = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})|([0-9]{10})+$/;*/
/*     var emailRegex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;*/
/* 	if (emailRegex.test(email)&&mailFormat.test(email)) {*/
/*     return emailRegex.test(email);*/
/* 	}*/
/* }*/
/* function validatePhone(phone) { */
/*     var pattern = /^[0]?[6789]\d{9}$/;*/
/*     var phoneRegex = /^(\+91-|\+91|0)?\d{10}$/; */
/* 	if (pattern.test(phone)&&phoneRegex.test(phone)) {*/
/*     return phoneRegex.test(phone);*/
/* 	}*/
/* }*/
/* */
/* */
/* function myFunction() {   */
/*    */
/* 		var email=document.loginform.emailid.value;*/
/* 		$("#otp-field").button('loading');*/
/* 		*/
/* 		*/
/* 	if (validateEmail(document.loginform.emailid.value)||validatePhone(document.loginform.emailid.value)){*/
/* 	*/
/* 		 */
/*          $.ajax({*/
/* 		 url: 'index.php?route=login/login',*/
/* 		 type: 'POST', */
/* 		 data:{email:email},*/
/* 		 datatype:'json',*/
/* 		 success: function (data) {*/
/* 			// console.log(data);*/
/* 			var data_array = jQuery.parseJSON(data);*/
/* 			if(data_array.status=='1')*/
/* 			{*/
/* 				$("#otp-field").button('reset');*/
/* 			    $("#hide").hide();*/
/*                 $("#signupcontent").show();*/
/*                 var mobile=$("#emailid").val();*/
/* 			    $("#continuephonenumber").empty();*/
/* 			    $("#continuephonenumber").val(mobile);  */
/* 			   */
/* 			}else{  */
/* 			   $("#otp-field").button('reset');*/
/* 			   $('#error-msg').empty();*/
/* 	           $('#error-msg').fadeIn();*/
/*                $('#error-msg').append('<p id="err-msg-verification" style="width: 440px; background: #093143; height: 43px; padding-top: 10px;margin-top: -10px;"><img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0nMjAnIGhlaWdodD0nMjAnIHZpZXdCb3g9JzAgMCAyMCAyMCcgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJz4KCTxnPgoJCTxwYXRoIGQ9J00tMi0yaDI0djI0SC0yeicgZmlsbD0nbm9uZScvPgoJCTxwYXRoIGQ9J00xMCAwQzQuNDggMCAwIDQuNDggMCAxMHM0LjQ4IDEwIDEwIDEwIDEwLTQuNDggMTAtMTBTMTUuNTIgMCAxMCAwem0xIDE1SDl2LTJoMnYyem0wLTRIOVY1aDJ2NnonIGZpbGw9JyNGRjQzNDMnLz4KCTwvZz4KPC9zdmc+Cg==">Verification Code Send To '+email+'</p>').fadeOut(6000);*/
/*    */
/* 				$("#hide").hide();*/
/* 				$('#msgotp').val("");*/
/* 				$('#currentphnumber').empty();*/
/* 				*/
/* 				$('#currentphnumber').append($("#emailid").val());*/
/* 				*/
/* 				$('#msgotp').val(btoa(btoa(data_array.otp)));    */
/* 				$("#otp-head").show();*/
/* 				$(".otp_password").empty();	*/
/* 				$('#sendotpemail-first1').focus();*/
/* 				*/
/* 				}*/
/* 		 }*/
/* 		});*/
/* 		 }else{*/
/* 			 $("#otp-field").button('reset');*/
/* 			$('.emailerror').empty();*/
/* 		    $('.emailerror').append('Please enter valid Email ID/Mobile number');*/
/* 		 }*/
/* }*/
/* */
/* function MyLogin()*/
/* {*/
/* 	    */
/* 	if($('#emailid').val()==""){*/
/* 		 $('.emailerror').empty();*/
/* 		    $('.emailerror').append('Please enter valid Email ID/Mobile number');*/
/* 		*/
/* 	}else if($('#password').val()==""){*/
/* 		 $('.highlight').empty();*/
/* 		    $('.highlight').append('Please enter Password');*/
/* 		   */
/* 	}else{	 */
/* 	$('.loginbutton').button('loading');*/
/*               // $.preloader.start({ modal: true, src : 'image/loadspinner/sprites323.png' });*/
/*                             // setTimeout(function(){ $.preloader.stop(); }, 3000);  	*/
/* 	if(validateEmail(document.loginform.emailid.value)||validatePhone(document.loginform.emailid.value)){*/
/* 					$.ajax({*/
/* 						 url: 'index.php?route=login/login/checkloginuser',*/
/* 						 type: 'POST', */
/* 						 data:{email:$('#emailid').val(),password:$('#password').val()},*/
/* 						 datatype:'json',*/
/* 						 success: function (data){*/
/* 							var data_array = jQuery.parseJSON(data);*/
/* 						  if(data_array.status=='1')*/
/* 						  {*/
/* 							  */
/* 							  return loginfunction(); */
/*                            $('.loginbutton').button('reset'); 							  */
/* 						  }else{*/
/* 							  $('.loginbutton').button('reset'); */
/* 							  $('.emailerror').empty();*/
/* 		                       $('.emailerror').append('Your username or password is incorrect');*/
/* 						  }*/
/* 						 }*/
/* 						});*/
/* 	}else*/
/* {*/
/* $('.loginbutton').button('reset');*/
/* $('.emailerror').empty();*/
/* $('.emailerror').append('Please enter valid Email ID/Mobile number');*/
/* }*/
/* 	}*/
/* 	*/
/* }*/
/* */
/* */
/* function forgetpasswordemail()*/
/* {*/
/* 	$('#fp').html('<div id="dots5"><span></span><span></span><span></span><span></span></div>');*/
/* var email=document.loginform.emailid.value;*/
/*          $.ajax({*/
/* 		 url: 'index.php?route=login/login/forgetpasswordmail',*/
/* 		 type: 'POST', */
/* 		 data:{email:email},*/
/* 		 datatype:'json',*/
/* 		 success: function (data) {*/
/* 			 //console.log(data);*/
/* 			var data_array = jQuery.parseJSON(data);*/
/* 			if(data_array.status=='0')*/
/* 			{*/
/* 				$('#fp').html('<div class="forgetpsw"><b id="fp"><a>Forgot Password?</a></b></div>');*/
/* 			 $(".emailerror").empty();	*/
/* 			 $(".emailerror").append('You are not registered with us. Please sign up.');*/
/* 		 */
/* 			}else{*/
/* 				$('#fp').html('<div class="forgetpsw"><b id="fp"><a>Forgot Password?</a></b></div>');*/
/* 				$('#error-msg').empty();*/
/* 				$('#error-msg').fadeIn();*/
/*           $('#error-msg').append('<p id="err-msg-verification"><img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0nMjAnIGhlaWdodD0nMjAnIHZpZXdCb3g9JzAgMCAyMCAyMCcgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJz4KCTxnPgoJCTxwYXRoIGQ9J00tMi0yaDI0djI0SC0yeicgZmlsbD0nbm9uZScvPgoJCTxwYXRoIGQ9J00xMCAwQzQuNDggMCAwIDQuNDggMCAxMHM0LjQ4IDEwIDEwIDEwIDEwLTQuNDggMTAtMTBTMTUuNTIgMCAxMCAwem0xIDE1SDl2LTJoMnYyem0wLTRIOVY1aDJ2NnonIGZpbGw9JyNGRjQzNDMnLz4KCTwvZz4KPC9zdmc+Cg==">Verification Code Send To '+email+'</p>').fadeOut(7000);*/
/*   */
/* 			$("#forgetotp").empty();*/
/* 			$("#forgetotp").val(btoa(btoa(data_array.otpnumber)));*/
/* 			$("#forgetphonenumber").empty();*/
/* 			$("#forgetphonenumber").val(email);*/
/* 			 $("#forgetphonenumber").prop("readonly", true);*/
/* 			$("#hide, #fp, #requestotp, .loginbutton1, #otp-field").hide();*/
/* 			$("#show, .loginbutton").show();*/
/* 			$(".loginleft").css("height", "515px");*/
/* 			$(".modal-body").css("padding", "0px 0px");*/
/* 			$(".modal-content").addClass("test");*/
/* 			$("#forgetotptext").focus();*/
/* 			*/
/* 			}*/
/* 		   }*/
/* 	});*/
/* }*/
/* */
/* */
/* function forgetpassworphone()*/
/* {*/
/* 	$('#fp').button('loading');*/
/* 	var email=document.loginform.emailid.value;*/
/* 	*/
/* 	*/
/*          $.ajax({*/
/* 		 url: 'index.php?route=login/login/forgetpasswordphone',*/
/* 		 type: 'POST', */
/* 		 data:{phone:email},*/
/* 		 datatype:'json',*/
/* 		 success: function (data) {*/
/* 			// console.log(data);*/
/* 			var data_array = jQuery.parseJSON(data);*/
/* 			if(data_array.status=='0')*/
/* 			{*/
/* 				$('#fp').button('reset');*/
/* 			 $(".emailerror").empty();	*/
/* 			 $(".emailerror").append('You are not registered with us. Please sign up.');*/
/* 		 */
/* 			}else{*/
/* 			$('#error-msg').empty();*/
/* 			$('#error-msg').fadeIn();*/
/*             $('#error-msg').append('<p id="err-msg-verification" style="width: 461px;background: #093143;height: 43px;padding-top: 10px;margin-top: -10px;"><img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0nMjAnIGhlaWdodD0nMjAnIHZpZXdCb3g9JzAgMCAyMCAyMCcgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJz4KCTxnPgoJCTxwYXRoIGQ9J00tMi0yaDI0djI0SC0yeicgZmlsbD0nbm9uZScvPgoJCTxwYXRoIGQ9J00xMCAwQzQuNDggMCAwIDQuNDggMCAxMHM0LjQ4IDEwIDEwIDEwIDEwLTQuNDggMTAtMTBTMTUuNTIgMCAxMCAwem0xIDE1SDl2LTJoMnYyem0wLTRIOVY1aDJ2NnonIGZpbGw9JyNGRjQzNDMnLz4KCTwvZz4KPC9zdmc+Cg==">Verification Code Send To '+email+'</p>').fadeOut(7000);*/
/* 			$('#fp').button('reset');*/
/* 			$("#forgetotp").empty();*/
/* 			$("#forgetotp").val(btoa(btoa(data_array.otpnumber)));*/
/* 			$("#forgetphonenumber").empty();*/
/* 			$("#forgetphonenumber").val(email);*/
/* 			 $("#forgetphonenumber").prop("readonly", true);*/
/* 			$("#hide, #fp, #requestotp, .loginbutton1, #otp-field").hide();*/
/* 			$("#show, .loginbutton").show();*/
/* 			$(".loginleft").css("height", "515px");*/
/* 			$(".modal-body").css("padding", "0px 0px");*/
/* 			$(".modal-content").addClass("test");*/
/* 			$("#forgetotptext").focus();*/
/* 			}*/
/* 		   }*/
/* 	});*/
/* }*/
/*  */
/* function loginfunction()*/
/* {*/
/* 		$.ajax({*/
/* 			// url: 'index.php?route=login/login/otplogin',*/
/* 			 url: 'index.php?route=login/login/userlogin',*/
/* 			 type: 'POST', */
/* 			 data:{email:$('#emailid').val(),password:$('#password').val()},*/
/* 			//data:{email:'8976876543',password:'palani@12345'},*/
/* 			 datatype:'json',*/
/* 			 success: function (data){*/
/* 				 var url      = window.location.href;  */
/* 				 if(url=="http://poorvikabeta.webindia.com/index.php?route=account/logout"){*/
/* 					 $('#modalLoginForm').modal('hide');*/
/*             localStorage.setItem('popState', 'shown');*/
/* 					 location.replace("http://poorvikabeta.webindia.com/");*/
/* 				 }else{*/
/* 					 $('#modalLoginForm').modal('hide');*/
/*             localStorage.setItem('popState', 'shown');*/
/* 					 window.location.reload();*/
/* 				 }*/
/* 				 */
/* 			 }*/
/* 			});*/
/* }*/
/* */
/* function verifyotp() {  */
/* var otplogin="otplogin";*/
/* var verifyotpdesk=$('#sendotpemail-first1').val()+$('#sendotpemail-first2').val()+$('#sendotpemail-first3').val()+$('#sendotpemail-first4').val()+$('#sendotpemail-first5').val()+$('#sendotpemail-first6').val();*/
/* if(verifyotpdesk.length=='6') {*/
/* if($('#msgotp').val()==btoa(btoa(verifyotpdesk)))*/
/* {*/
/*    //$.preloader.start({ modal: true, src : '../image/loadspinner/sprites323.png' });*/
/*     // setTimeout(function(){ $.preloader.stop(); }, 3000); */
/* 					$.ajax({*/
/* 						 url: 'index.php?route=login/login/otplogin',*/
/* 						 type: 'POST', */
/* 						 data:{email:$('#emailid').val(),password:$('#password').val(),otplogin:otplogin},*/
/* 						 datatype:'json',*/
/* 						 success: function(data){	*/
/* 						  var url      = window.location.href;  */
/* 				 if(url=="http://poorvikabeta.webindia.com/index.php?route=account/logout"){*/
/* 					 $('#modalLoginForm').modal('hide');*/
/*             localStorage.setItem('popState', 'shown');*/
/* 					 location.replace("http://poorvikabeta.webindia.com/");*/
/* 				 }else{*/
/* 					 $('#modalLoginForm').modal('hide');*/
/*             localStorage.setItem('popState', 'shown');*/
/* 					 window.location.reload();*/
/* 				 }	*/
/* 						 }*/
/* 					});*/
/* 	*/
/* }else*/
/* {*/
/* $('#error-msg').empty();*/
/* $('#error-msg').fadeIn();*/
/* $('#error-msg').append('<p id="err-msg"><img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0nMjAnIGhlaWdodD0nMjAnIHZpZXdCb3g9JzAgMCAyMCAyMCcgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJz4KCTxnPgoJCTxwYXRoIGQ9J00tMi0yaDI0djI0SC0yeicgZmlsbD0nbm9uZScvPgoJCTxwYXRoIGQ9J00xMCAwQzQuNDggMCAwIDQuNDggMCAxMHM0LjQ4IDEwIDEwIDEwIDEwLTQuNDggMTAtMTBTMTUuNTIgMCAxMCAwem0xIDE1SDl2LTJoMnYyem0wLTRIOVY1aDJ2NnonIGZpbGw9JyNGRjQzNDMnLz4KCTwvZz4KPC9zdmc+Cg==">OTP is incorrect</p>').fadeOut(6000);*/
/*  */
/* */
/*  }*/
/* 	}*/
/* }*/
/* function lookupemail(arg)*/
/* {*/
/* 	var dInput = arg.value[0];*/
/* 	if($.isNumeric(dInput)==true)*/
/* 	{*/
/* 		$('.countrycode').show();*/
/* 		$('.firstemailid').css('cssText', 'padding-left: 37px !important');*/
/* */
/* 	}else{*/
/* 		$('.countrycode').hide();*/
/* 		$('.firstemailid').css('cssText', 'padding-left: 0px !important');*/
/* 		*/
/* 		*/
/* 	}*/
/* 	 //alert(dInput);*/
/*  $('.emailerror').empty();*/
/*  $('.highlight').empty();*/
/*  $('.continue_error').empty();*/
/* }*/
/* */
/* function lookup(arg){*/
/* 	*/
/* 	 //alert(dInput);*/
/*  $('.emailerror').empty();*/
/*  $('.highlight').empty();*/
/*  $('.continue_error').empty();*/
/* }*/
/* function continuefunction()*/
/* {*/
/* $('#continuebutton').button('loading');*/
/* 	$.ajax({*/
/* 			 url: 'index.php?route=login/login/continuelogin',*/
/* 			 type: 'POST',  */
/* 			 data:{phonenumber:$('#continuephonenumber').val()},*/
/* 			 datatype:'json',*/
/* 			 success: function (data){	*/
/* 				 var data_array = jQuery.parseJSON(data);*/
/* 				 if(data_array.continue_otp=='alreadytaken')*/
/* 				 {*/
/* 					 $('#continuebutton').button('reset'); */
/* 					 $('#signupcontent').hide();*/
/* 					 $('#hide').show();*/
/* 					 $("#emailid").val($('#continuephonenumber').val()); */
/* 					 $("#emailid").focus(); */
/* 					 $('.signin').show();*/
/* 					 $('#popupfooter').show();*/
/* 				  $('.continue_error').empty();*/
/* 				  */
/* 				    $('#error-msg').empty();*/
/*                     $('#error-msg').fadeIn();*/
/*                     $('#error-msg').append('<p id="err-msg-already"><img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0nMjAnIGhlaWdodD0nMjAnIHZpZXdCb3g9JzAgMCAyMCAyMCcgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJz4KCTxnPgoJCTxwYXRoIGQ9J00tMi0yaDI0djI0SC0yeicgZmlsbD0nbm9uZScvPgoJCTxwYXRoIGQ9J00xMCAwQzQuNDggMCAwIDQuNDggMCAxMHM0LjQ4IDEwIDEwIDEwIDEwLTQuNDggMTAtMTBTMTUuNTIgMCAxMCAwem0xIDE1SDl2LTJoMnYyem0wLTRIOVY1aDJ2NnonIGZpbGw9JyNGRjQzNDMnLz4KCTwvZz4KPC9zdmc+Cg==">You are already registered. Please login.</p>').fadeOut(3500);*/
/*  */
/* 				 }*/
/* 				 else{*/
/* 					  $('#continuebutton').button('reset'); */
/* 					 $('#error-msg').empty();*/
/*                     $('#error-msg').fadeIn();*/
/*                     $('#error-msg').append('<p id="err-msg"><img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0nMjAnIGhlaWdodD0nMjAnIHZpZXdCb3g9JzAgMCAyMCAyMCcgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJz4KCTxnPgoJCTxwYXRoIGQ9J00tMi0yaDI0djI0SC0yeicgZmlsbD0nbm9uZScvPgoJCTxwYXRoIGQ9J00xMCAwQzQuNDggMCAwIDQuNDggMCAxMHM0LjQ4IDEwIDEwIDEwIDEwLTQuNDggMTAtMTBTMTUuNTIgMCAxMCAwem0xIDE1SDl2LTJoMnYyem0wLTRIOVY1aDJ2NnonIGZpbGw9JyNGRjQzNDMnLz4KCTwvZz4KPC9zdmc+Cg==">Verification code Send to '+$('#continuephonenumber').val()+'</p>').fadeOut(3500);*/
/*  */
/* 					  $('#signupotp').empty();*/
/* 					  $('#signupotp').val(btoa(btoa(data_array.continue_otp)));*/
/* 					  var mobilenumber=$("#continuephonenumber").val();  */
/* 					 $("#regphonenumber").empty();*/
/* 					 $("#regphonenumber").val(mobilenumber);  */
/* 					 $("#regphonenumber").prop("readonly", true);*/
/* 						$("#signupcontent").hide();*/
/* 						$(".signup-newcus").show();*/
/* 						$("#regotp").focus(); */
/* 				 }*/
/* 				*/
/* 			 }*/
/* 	});*/
/* 	*/
/* }*/
/* */
/* function signinotp()*/
/* {*/
/* 	$('.regotperror_msg').empty();*/
/* 	if ($('#regotp').val().length=='6') {*/
/* 		if($('#signupotp').val()==btoa(btoa($('#regotp').val()))){*/
/* 		$('.regotperror_msg').empty();*/
/* 		$('.regname_error').empty();*/
/* 		$('.regphone_error').empty();*/
/* 		$('.regemail_error').empty();*/
/* 		$('.regpass_error').empty();*/
/* 		 document.getElementById("signupbutton").disabled = false;*/
/* 	}else*/
/* 	{*/
/* 		$('.regotperror_msg').empty();*/
/* 		$('.regotperror_msg').append('OTP is incorrect');*/
/* 		document.getElementById("signupbutton").disabled = true;*/
/* 	}*/
/* 	}*/
/* }*/
/* */
/* */
/* jQuery('.numbersOnly').keyup(function () { */
/*     this.value = this.value.replace(/[^0-9\.]/g,'');*/
/* });*/
/* */
/* function forgetotplogin()*/
/* {*/
/* 	$('.forgetotptext').empty();*/
/* 	if ($('#forgetotptext').val().length=='6') {*/
/* 		if(btoa(btoa($('#forgetotptext').val()))==$('#forgetotp').val()){*/
/* 		$('.forgetotptext').empty();*/
/* 		 document.getElementById("forgrtloginbutton").disabled = false;*/
/* 		// document.getElementById('forgetotptext').setAttribute('readonly', 'readonly'); */
/* 	}else{*/
/* 		*/
/* 		$('.forgetotptext').empty();*/
/* 		$('.forgetotptext').append('OTP is incorrect');*/
/* 		document.getElementById("forgrtloginbutton").disabled = true;*/
/* 	}*/
/* 	}*/
/* }*/
/* */
/* */
/* $(window).load(function() {*/
/* 	$('.my_account_new').hover(function(){*/
/* 		$('.dropdown_myacc').toggleClass('acc_active');*/
/* 	});*/
/* });*/
/* </script>*/
/* <script>*/
/* $(document).ready(function(){*/
/* $(':input').keyup(function(e) {*/
/*     if (e.which == 8 || e.which == 46) {*/
/*         $(this).prev('input').focus();*/
/*     }*/
/*     else {*/
/*         $(this).next('input').focus();*/
/*     }*/
/* });*/
/* });*/
/* $(document).ready(function(){*/
/* 	$('.countrycode').css('display','none');*/
/* */
/* */
/* 	*/
/*   $('#autofocusemail :input:enabled:visible:first').focus();*/
/* */
/* $('.menu-button').click(function(e){*/
/*     e.stopPropagation();*/
/*      $('#hide-menu').toggleClass('so-megamenu-active');*/
/* 	 $('body').addClass('menu-overlay');*/
/* });*/
/* $('#hide-menu').click(function(e){*/
/*     e.stopPropagation();*/
/* });*/
/* $('#remove-megamenu').click(function() {*/
/* 	$('.megamenu-wrapper').removeClass('so-megamenu-active');*/
/* 	$('body').removeClass('menu-overlay');*/
/* 	return false;*/
/* });*/
/* $('body,html').click(function(e){*/
/*        $('#hide-menu').removeClass('so-megamenu-active');*/
/* 	   $('body').removeClass('menu-overlay');*/
/* });*/
/* });*/
/* function wishlistload() {*/
/*     var wishlist = $('#wishlist-total').text();*/
/*     if(wishlist == 'Wish List'){*/
/*         $('#wishlist-total').html('<i class="fa fa-heart"></i>');*/
/*     }*/
/* }*/
/* setInterval(wishlistload,20);*/
/* </script>*/
/* <script>*/
/* 	function init() {*/
/* 	var vidDefer = document.getElementsByTagName('iframe');*/
/* 	for (var i=0; i<vidDefer.length; i++) {*/
/* 	if(vidDefer[i].getAttribute('data-src')) {*/
/* 	vidDefer[i].setAttribute('src',vidDefer[i].getAttribute('data-src'));*/
/* 	} } }*/
/* 	window.onload = init;*/
/* */
/* */
/*     var image  = $(".image-logo img").attr('src');*/
/*     var image1 = image.replace("www","s1");*/
/*     $(".image-logo img").attr('src',image1);*/
/* */
/*     // added by aadhi subash*/
/*       document.addEventListener("DOMContentLoaded",()=>{*/
/*         var xhttp = new XMLHttpRequest();*/
/*             xhttp.onreadystatechange = function(){*/
/*                 if (this.readyState == 4 && this.status == 200) {*/
/*                     var data = this.responseText;*/
/*                     var arr = JSON.parse(data);*/
/*                    */
/* */
/*                     var inp = document.getElementById("myInput");*/
/*                     inp.addEventListener('input',function(e){*/
/*                         var a, b, i, val = this.value;*/
/*                             $(".autocomplete-items").addClass('item_list');*/
/*                             let filterdata = arr.filter(it => new RegExp(this.value, "i").test(it.name));*/
/*                             currentFocus = -1;*/
/*                             var b = "";*/
/*                             */
/*                             for(var i=0;i<filterdata.length;i++){                               */
/*                                 if(val.length <= 1){                     */
/*                                     if(filterdata[i].name.substr(0, val.length) == val.toUpperCase()){*/
/*                                        b += '<li class="liremove"><a href="'+filterdata[i].keyword+'">'+ filterdata[i].name.toLowerCase() +'</a></li>'; */
/*                                     }*/
/*                                 }else{*/
/*                                     b += '<li class="liremove"><a href="'+filterdata[i].keyword+'">'+ filterdata[i].name.toLowerCase() +'</a></li>';    */
/*                                 }*/
/*                                 */
/*                             }*/
/*                             if(val.length === 0){*/
/*                                 $(".autocomplete-items").removeClass('item_list');*/
/*                                 $("#myInputautocomplete-list").html('');*/
/*                             }else{*/
/*                                 $("#myInputautocomplete-list").html(b); */
/*                             }*/
/*                             */
/*                        */
/*                     });*/
/* */
/*                     inp.addEventListener('keydown',function(e){*/
/*                         var x = document.getElementById(this.id + "autocomplete-list");*/
/*                           if (x) x = x.getElementsByTagName("li");*/
/*                       */
/*                           if (e.keyCode == 40) {*/
/*                             */
/*                             currentFocus++;*/
/*                             */
/*                             addActive(x);*/
/*                           } else if (e.keyCode == 38) { */
/*                             */
/*                             currentFocus--;*/
/*                             */
/*                             addActive(x);*/
/*                         } else if (e.keyCode == 13) {*/
/*                             e.preventDefault();*/
/*                             var t = document.getElementsByClassName("autocomplete-active")[0];*/
/*                         */
/*                             if(typeof t === 'undefined'){*/
/*                                 var enterinp = document.getElementById("myInput");*/
/*                                     var redirect = "search?name='" + encodeURIComponent(enterinp.value) +"'";*/
/*                                     window.location.href = redirect;    */
/*                             }else{*/
/*                                 var link = t.getElementsByTagName("a")[0].getAttribute("href");*/
/*                                 window.location.href=link;  */
/*                                 */
/*                             }*/
/*                            */
/*                         }*/
/*                         $("#myInputautocomplete-list").scrollTop(0);*/
/*                         $("#myInputautocomplete-list").scrollTop($('.autocomplete-active:first').offset().top-$("#myInputautocomplete-list").height());*/
/*                     });*/
/*                     function addActive(x) {*/
/*                         if (!x) return false;*/
/* */
/*                         removeActive(x);*/
/*                         if (currentFocus >= x.length) currentFocus = 0;*/
/*                         if (currentFocus < 0) currentFocus = (x.length - 1);*/
/*                         */
/*                         x[currentFocus].classList.add("autocomplete-active");*/
/*                     }*/
/* */
/*                     function removeActive(x) {*/
/*                         for (var i = 0; i < x.length; i++) {*/
/*                           x[i].classList.remove("autocomplete-active");*/
/*                         }*/
/*                     }*/
/*                     document.addEventListener("click", function (e) {*/
/*                         $('.liremove').remove();*/
/*                         $(".autocomplete-items").removeClass('item_list');*/
/*                     });*/
/* */
/*                     $('#submit_search').on('click',function(){*/
/*                         var t = document.getElementsByClassName("autocomplete-active")[0];*/
/*                         */
/*                             if(typeof t === 'undefined'){*/
/*                                 var enterinp = document.getElementById("myInput");*/
/*                                     var redirect = "search?name='" + encodeURIComponent(enterinp.value) +"'";*/
/*                                     window.location.href = redirect;    */
/*                             }*/
/*                     });*/
/*                  */
/*                 }*/
/*             };*/
/*             xhttp.open("GET", "index.php?route=product/search/searchautocomplete", true);*/
/*             xhttp.send();*/
/*     });*/
/*     // end here*/
/* </script>*/
/* */
/* */
/* <script>*/
/* */
/* */
/* */
/* $("#main-login").click(function() {*/
/* 	*/
/*    $('#continuephonenumber').val("");*/
/*    $('#emailid').focus();*/
/* 	$(".emailerror,.highlight").empty();*/
/* });  */
/* $(".toggle-password").click(function() {*/
/* */
/*   $(this).toggleClass("fa-eye fa-eye-slash");*/
/*   var input = $($(this).attr("toggle"));*/
/*   if (input.attr("type") == "password") {*/
/*     input.attr("type", "text");*/
/*   } else {*/
/*     input.attr("type", "password");*/
/*   }*/
/* });*/
/* */
/* */
/* $(document).ready(function(){*/
/*   $("#fp,#forgotresend").click(function(){*/
/* 	  if(validateEmail(document.loginform.emailid.value))*/
/* 	{	 */
/*      forgetpasswordemail();*/
/* 	}else if(validatePhone(document.loginform.emailid.value))*/
/* 	{*/
/* 	forgetpassworphone();*/
/*      }*/
/* 	else{*/
/* 		    $('.emailerror').empty();*/
/* 		    $('.emailerror').append('Please enter valid Email ID/Mobile number');*/
/* 	   }*/
/* 	*/
/*   });*/
/*   */
/*   */
/*  /*$("#otp-field").click(function()*/
/*   {*/
/* 	if (validateEmail(document.loginform.emailid.value)||validatePhone(document.loginform.emailid.value)){*/
/*     $("#hide").hide();*/
/*    $("#otp-head").show();*/
/*     $(".otp_password").empty();*/
/*    $(".otp_password").append(document.loginform.emailid.value);*/
/* 	}else{*/
/* 	}*/
/*   });*//* */
/*   */
/*   $(".Existing-loginbutton").click(function()*/
/*   {*/
/* 	  $("#continuephonenumber").val("");*/
/* 	  $("#emailid").val("");*/
/*     $("#signupcontent").hide();*/
/*    $("#hide, .signin, #popupfooter, #otp-field, #fp, .loginbutton1").show();*/
/*   });*/
/*   $("#otpchange").click(function()*/
/*   {*/
/*     $("#otp-head").hide();*/
/*    $("#hide").show();*/
/*   });*/
/*   $(".newtopoor").click(function()*/
/*   {*/
/*     $("#show").hide();*/
/*    $(".signin1, #signupcontent").show();*/
/*   });*/
/* */
/*   $("#change").click(function()*/
/*   {*/
/*     $("#hide, .signin, #popupfooter, #otp-field, #fp, .loginbutton1").show();*/
/*    $("#show").hide();*/
/*   });*/
/* */
/*   $("#change1").click(function()*/
/*   {*/
/*     $("#signupcontent").show();*/
/*    $(".signup-newcus").hide();*/
/*   });*/
/*   $(".loginbutton1").click(function()*/
/*   {*/
/*    $("#hide, .signin, #popupfooter").hide();*/
/*    $("#signupcontent, .signin1").show();*/
/*   });*/
/*   $("#new-loginbutton").click(function()*/
/*   {*/
/* 	 $("#modalLoginForm").modal('show');*/
/*   });*/
/*   $("#new-regbutton").click(function()*/
/*   {*/
/* 	 $("#modalLoginForm").modal('show');*/
/*   });*/
/*   });*/
/*   */
/*   */
/*     */
/* */
/*     if (localStorage.getItem('popState') != 'shown') {*/
/*           */
/*           $("#modalLoginForm").modal('show');*/
/*         }*/
/* */
/*         $('.close').click(function (e) {*/
/*             $('#modalLoginForm').modal('hide');*/
/*             localStorage.setItem('popState', 'shown');*/
/*         });*/
/*         */
/*     */
/* */
/* $(document).ready(function(){*/
/*   $("#input22").change(function()*/
/*   {*/
/*    $("#result").show();*/
/*   });*/
/*   });*/
/*   $(document).ready(function(){*/
/*   $("#continuebutton,#resendotpsignup").click(function()*/
/*   { */
/*  */
/* 	 if(validatePhone(document.getElementById("continuephonenumber").value)){*/
/*         continuefunction();*/
/* 		 }*/
/* 		 else{*/
/* 			$('.continue_error').empty();*/
/* 		    $('.continue_error').append('Please enter valid Mobile number');*/
/* 		 } */
/* 		*/
/*   });*/
/*   });*/
/* */
/*   $(document).ready(function(){*/
/* $(".close").click(function(){ */
/* */
/* $('.signup-newcus').hide();*/
/*   $("#hide, .signin, #popupfooter, #fp, #otp-field, .loginbutton1").show();*/
/*    $("#show, #signupcontent, #otp-head").hide();*/
/* $('form#contact,form#signin-newcustomer,form#otphead123').trigger("reset");*/
/* });*/
/* });*/
/*   */
/* $(document).ready(function(){*/
/* $("#signupbutton").click(function()*/
/*   {*/
/* 	*/
/* 	  var regemail=$("#regemail").val();*/
/* 	  var regname=$("#regname").val();*/
/* 	  */
/* 	 if($("#regphonenumber").val()==''){$(".regphone_error").empty();$(".regphone_error").append("PhoneNumber is Required");return false;}else if($("#regotp").val().length!='6'){$(".regotperror_msg").empty();$(".regotperror_msg").append("Please enter valid OTP");return false;}else if($("#regotp").val()==''){$(".regotperror_msg").empty();$(".regotperror_msg").append("OTP is Required");return false;}else if($("#regname").val()==""){$(".regname_error").empty();$(".regname_error").append("Name is Required");return false;}else if($("#regemail").val()==""){$(".regemail_error").empty();$(".regemail_error").append("Email is Required");return false;}else if(!validateEmail($("#regemail").val())){$(".regemail_error").empty();$(".regemail_error").append("Please enter valid Email ID");return false;}else if($("#regpassword").val()==''){$(".regpass_error").empty();$(".regpass_error").append("Password is Required");return false;}*/
/* 	  */
/* 	  $("#signupbutton").button('loading');*/
/* 	  */
/* 	  if($("#regpassword").val().length>=4){*/
/* 	       //$.preloader.start({ modal: true, src : 'image/loadspinner/sprites323.png' });*/
/*                            //  setTimeout(function(){ $.preloader.stop(); }, 3000); */
/* 		   $.ajax({*/
/* 						 url: 'index.php?route=login/login/signin',*/
/* 						 type: 'POST', */
/* 						 data:{phnumber:$("#regphonenumber").val(),otp:$("#regotp").val(),password:$("#regpassword").val(),firstname:$('#regname').val(),email:$("#regemail").val()}, */
/* 						 datatype:'json',*/
/* 						 success: function (data){*/
/* 							// console.log(data);*/
/* 							 	var data_array = jQuery.parseJSON(data);*/
/* 								if(data_array.status=='insert')*/
/* 								{*/
/* 									$("#signupbutton").button('reset');*/
/* 									newuserlogin(data_array.mobilenumber,data_array.password);										*/
/* 								}else{*/
/* 									$("#signupbutton").button('reset');*/
/* 								}*/
/* 								 }*/
/* 					});*/
/* 					}else{ */
/* 					$("#signupbutton").button('reset');*/
/* 						$(".regpass_error").empty();*/
/* 						$(".regpass_error").append("Password should be minimum 4 characters");*/
/* */
/* 					}*/
/* 	*/
/*   });*/
/*   });*/
/*   */
/*   */
/* */
/*   */
/*   */
/*  $(document).ready(function(){*/
/* $(".forgetloginbutton").click(function()*/
/*   {*/
/* 	  if(validateEmail($("#forgetphonenumber").val())&&$("#forgetotptext").val().length=='6')*/
/* 	{	 */
/*      updatepasswordemail();*/
/* 	}else if(validatePhone($("#forgetphonenumber").val())&&$("#forgetotptext").val().length=='6')*/
/* 	{*/
/* 	updatepassworphone();*/
/*      }else{*/
/* 		 $('.forgetotptext').empty();*/
/* 		$('.forgetotptext').append('Please enter valid OTP');*/
/* 		document.getElementById("forgrtloginbutton").disabled = true;*/
/* 	 }*/
/* 	*/
/*   });*/
/*   });*/
/*   */
/* function updatepassworphone()*/
/*   {*/
/* 	   //$.preloader.start({ modal: true, src : 'image/loadspinner/sprites323.png' });*/
/*                            //  setTimeout(function(){ $.preloader.stop(); }, 3000); */
/* 	 $(".forgetloginbutton").button('loading');*/
/* 	  if($("#forgetpassword").val().length>=4){*/
/* 	 if($("#forgetphonenumber").val()==''){$(".forgetphonr_error").empty();$(".forgetphonr_error").append("field is Required");return false;}else if($("#forgetotp").val()==''){$(".forgetotptext").empty();$(".forgetotptext").append("OTP is Required");return false;}else if($("#forgetpassword").val()==''){$(".forgetpass_error").empty();$(".forgetpass_error").append("Password is Required");return false;}*/
/* 	         $.ajax({*/
/* 						 url: 'index.php?route=login/login/updatephonelogin',*/
/* 						 type: 'POST', */
/* 						 data:{phnumber:$("#forgetphonenumber").val(),otp:$("#forgetotp").val(),password:$("#forgetpassword").val()},*/
/* 						 datatype:'json',*/
/* 						 success: function (data){*/
/* 							 	var data_array = jQuery.parseJSON(data);*/
/* 								if(data_array.status=='update')*/
/* 								{*/
/* 									$(".forgetloginbutton").button('reset');*/
/* 									newuserlogin(data_array.mobilenumber,data_array.password);*/
/* 										}else{*/
/* 											$(".forgetloginbutton").button('reset');*/
/* 								}*/
/* 								 }*/
/* 					});*/
/* 	  }else{*/
/* 		  $(".forgetloginbutton").button('reset');*/
/* 		                $(".forgetpass_error").empty();*/
/* 						$(".forgetpass_error").append("Password should be minimum 4 characters");*/
/* */
/* 	  }*/
/*   }*/
/* */
/* $("input#forgetpassword,input#regpassword,input#password,input#emailid").on({*/
/*   keydown: function(e) { */
/*     if (e.which === 32)*/
/*       return false;*/
/*   },*/
/*   change: function() {*/
/*     this.value = this.value.replace(/\s/g, "");*/
/*   }*/
/* });*/
/* */
/*   function updatepasswordemail()*/
/*   {*/
/* 	  // $.preloader.start({ modal: true, src : 'image/loadspinner/sprites323.png' });*/
/*                            //  setTimeout(function(){ $.preloader.stop(); }, 3000); */
/* 						   $(".forgetloginbutton").button('loading');*/
/* 						   */
/* 	  if($("#forgetpassword").val().length>=4){*/
/* 	 if($("#forgetphonenumber").val()==''){$(".forgetphonr_error").empty();$(".forgetphonr_error").append("field is Required");return false;}else if($("#forgetotp").val()==''){$(".forgetotptext").empty();$(".forgetotptext").append("OTP is Required");return false;}else if($("#forgetpassword").val()==''){$(".forgetpass_error").empty();$(".forgetpass_error").append("Password is Required");return false;}*/
/* 	         $.ajax({*/
/* 						 url: 'index.php?route=login/login/updateemaillogin',*/
/* 						 type: 'POST', */
/* 						 data:{email:$("#forgetphonenumber").val(),otp:$("#forgetotp").val(),password:$("#forgetpassword").val()},*/
/* 						 datatype:'json',*/
/* 						 success: function (data){*/
/* 							 	var data_array = jQuery.parseJSON(data);*/
/* 								if(data_array.status=='update')*/
/* 								{*/
/* 									$(".forgetloginbutton").button('reset');*/
/* 									newuserlogin(data_array.email,data_array.password);*/
/* 										}else{*/
/* 											$(".forgetloginbutton").button('reset');*/
/* 								}*/
/* 								 }*/
/* 					});*/
/* 	  }else{*/
/* 		  $(".forgetloginbutton").button('reset');*/
/* 		    $(".forgetpass_error").empty();*/
/* 			$(".forgetpass_error").append("Password should be minimum 4 characters");*/
/* 	  }*/
/*   }*/
/*   */
/*   */
/*   */
/*    */
/*  function newuserlogin(mobilenumber,password) { */
/* 					$.ajax({*/
/* 						 url: 'index.php?route=login/login/otplogin',*/
/* 						 type: 'POST', */
/* 						 data:{email:mobilenumber,password:password},*/
/* 						 datatype:'json',*/
/* 						 success: function(data){	*/
/*                             var url      = window.location.href;  */
/* 				 if(url=="http://poorvikabeta.webindia.com/index.php?route=account/logout"){*/
/* 					 $('#modalLoginForm').modal('hide');*/
/*             localStorage.setItem('popState', 'shown');*/
/* 					 location.replace("http://poorvikabeta.webindia.com/");*/
/* 				 }else{*/
/* 					 $('#modalLoginForm').modal('hide');*/
/*             localStorage.setItem('popState', 'shown');*/
/* 					 window.location.reload();*/
/* 				 }		*/
/* 						 }*/
/* 					});*/
/*                  }*/
/*  */
/* </script>*/
/*  <script type="text/javascript">*/
/* */
/* 	$("#contact_submit button").click(function(event){*/
/* */
/* 				var form_data=$("#contact").serializeArray();*/
/* 				var error_free=true;*/
/* 				for (var input in form_data){*/
/* 					var element=$("#contact_"+form_data[input]['name']);*/
/* 					var valid=element.hasClass("valid");*/
/* 					var error_element=$("span", element.parent());*/
/* 					if (!valid){error_element.removeClass("error").addClass("error_show"); error_free=false;}*/
/* 					else{error_element.removeClass("error_show").addClass("error");}*/
/* 				}*/
/* 				if (!error_free){*/
/* 					event.preventDefault(); */
/* 				}*/
/* 				else{*/
/* 					alert('No errors: Form will be submitted');*/
/* 				}*/
/* 			});*/
/* 			*/
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* function validateForm() {*/
/*   var x = document.forms["myForm"]["name"].value;*/
/*   if (x == "") {*/
/*     alert("Name must be filled out");*/
/*     return false;*/
/*   }*/
/* }*/
/*   // Email-Address Validation*/
/*   */
/* function emailfunction(){*/
/*   var email = document.getElementById("input22");*/
/*   var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;*/
/*   if(!filter.test(email.value)){*/
/*     document.getElementById("result").innerHTML="Enter ur Correct Email"*/
/*     email.focus;*/
/*     return false*/
/*   }*/
/* }*/
/* */
/* </script>*/
/* <script>*/
/*       var obj = document.getElementById('partitioned');*/
/*       obj.addEventListener('keydown', stopCarret); */
/*       obj.addEventListener('keyup', stopCarret); */
/* */
/*       function stopCarret() {*/
/*         if (obj.value.length > 5){*/
/*           setCaretPosition(obj, 5);*/
/*         }*/
/*       }*/
/* */
/*       function setCaretPosition(elem, caretPos) {*/
/*           if(elem != null) {*/
/*               if(elem.createTextRange) {*/
/*                   var range = elem.createTextRange();*/
/*                   range.move('character', caretPos);*/
/*                   range.select();*/
/*               }*/
/*               else {*/
/*                   if(elem.selectionStart) {*/
/*                       elem.focus();*/
/*                       elem.setSelectionRange(caretPos, caretPos);*/
/*                   }*/
/*                   else*/
/*                       elem.focus();*/
/*               }*/
/*           }*/
/*       }*/
/*  </script>*/
/*  <style>*/
/* */
/* /**===== dots5 =====*//* */
/* #dots5 {*/
/*   display: block;*/
/*   position: absolute;*/
/*   left: 80%;*/
/*   height: 50px;*/
/*   width: 50px;*/
/*   margin: -25px 0 0 0px;*/
/* }*/
/* */
/* #dots5 span {*/
/*   position: absolute;*/
/*   width: 10px;*/
/*   height: 10px;*/
/*   background: rgba(0, 0, 0, 0.25);*/
/*   border-radius: 50%;*/
/*   -webkit-animation: dots5 1s infinite ease-in-out;*/
/*           animation: dots5 1s infinite ease-in-out;*/
/* }*/
/* */
/* #dots5 span:nth-child(1) {*/
/*   left: 0px;*/
/*   -webkit-animation-delay: 0.2s;*/
/*           animation-delay: 0.2s;*/
/* }*/
/* */
/* #dots5 span:nth-child(2) {*/
/*   left: 15px;*/
/*   -webkit-animation-delay: 0.3s;*/
/*           animation-delay: 0.3s;*/
/* }*/
/* */
/* #dots5 span:nth-child(3) {*/
/*   left: 30px;*/
/*   -webkit-animation-delay: 0.4s;*/
/*           animation-delay: 0.4s;*/
/* }*/
/* */
/* #dots5 span:nth-child(4) {*/
/*   left: 45px;*/
/*   -webkit-animation-delay: 0.5s;*/
/*           animation-delay: 0.5s;*/
/* }*/
/* */
/* @keyframes dots5 {*/
/*   0% {*/
/*     -webkit-transform: translateY(0px);*/
/*             transform: translateY(0px);*/
/*     -webkit-transform: translateY(0px);*/
/*             transform: translateY(0px);*/
/*     background: #d62d20;*/
/*   }*/
/*   25% {*/
/*     -webkit-transform: translateY(10px);*/
/*             transform: translateY(10px);*/
/*     -webkit-transform: translateY(10px);*/
/*             transform: translateY(10px);*/
/*     background: #ffa700;*/
/*   }*/
/*   50% {*/
/*     -webkit-transform: translateY(10px);*/
/*             transform: translateY(10px);*/
/*     -webkit-transform: translateY(10px);*/
/*             transform: translateY(10px);*/
/*     background: #008744;*/
/*   }*/
/*   100% {*/
/*     -webkit-transform: translateY(0px);*/
/*             transform: translateY(0px);*/
/*     -webkit-transform: translateY(0px);*/
/*             transform: translateY(0px);*/
/*     background: #0057e7;*/
/*   }*/
/* }*/
/* @-webkit-keyframes dots5 {*/
/*   0% {*/
/*     -webkit-transform: translateY(0px);*/
/*             transform: translateY(0px);*/
/*     background: #d62d20;*/
/*   }*/
/*   25% {*/
/*     -webkit-transform: translateY(10px);*/
/*             transform: translateY(10px);*/
/*     background: #ffa700;*/
/*   }*/
/*   50% {*/
/*     -webkit-transform: translateY(10px);*/
/*             transform: translateY(10px);*/
/*     background: #008744;*/
/*   }*/
/*   100% {*/
/*     -webkit-transform: translateY(0px);*/
/*             transform: translateY(0px);*/
/*     background: #0057e7;*/
/*   }*/
/* }*/
/* /** END of dots5 *//* */
/* */
/* */
/* input[type=number]:focus*/
/* {*/
/*   background-color: transparent;*/
/*   border-bottom: 1px solid #de490a;*/
/* }*/
/* .otp-verify-button*/
/* {*/
/*       width: 325px;*/
/*     height: 49px;*/
/*     background: #f5641b;*/
/*     box-shadow: 0 5px 5px 0 rgba(0,0,0,.2);*/
/*     border: none;*/
/*     margin-top: 40px;*/
/*     color: white;*/
/* }*/
/* 	.container-megamenu.horizontal ul.megamenu .sub-menu .content .hover-menu .menu ul a.main-menu*/
/* 	{*/
/* 		color: black!important;*/
/* 	}*/
/*   .Existing-loginbutton:hover*/
/*   {*/
/*     background-color: white!important;*/
/*   }*/
/*   .header-otp{*/
/*     display: flex;*/
/*     width: 100%;*/
/*     margin-top: 15px;*/
/*   }*/
/*   .header-otp1 input*/
/*   {*/
/*     border: 0;*/
/*     outline: 0;*/
/*     background: transparent;*/
/*     border-bottom: 1px solid black;*/
/*     width: 12%;*/
/*     text-align: center;*/
/*     padding: 5px;*/
/*     margin-left: 10px;*/
/*     border-radius: inherit;*/
/*   }*/
/*   .regemaillabel*/
/*   {*/
/*     top: -15px!important;*/
/*   }*/
/*   input[type=password]*/
/*   { */
/*     transition: none!important;*/
/*   }*/
/*   input[type="number"]:hover*/
/*   {*/
/*     background-color:white!important;*/
/*   }*/
/* 	.close:hover*/
/* 	{*/
/* 		opacity: 1!important;*/
/* 	}*/
/* 	.close*/
/* 	{*/
/* 		opacity: 1!important;*/
/* 	}*/
/*   #error-msg*/
/*   {*/
/*     */
/*     left: 0;*/
/*     right: 0;*/
/*     bottom: 100px;*/
/*     margin: auto;*/
/*     display:none;*/
/*     position: absolute;*/
/*     color: white;*/
/*     height: 43px;*/
/*     width: 374px;*/
/*     z-index: 999999;*/
/*     background: #083044;*/
/*     box-shadow: 0 10px 6px -6px #777;*/
/*     border-radius: 3px;*/
/*     font-size: 16px;*/
/*     text-align: center;*/
/*     padding-top: 10px;*/
/*     margin-top: -10px;*/
/*   }*/
/*   #err-msg-verification img*/
/*   {*/
/*         margin-right:8px;*/
/* */
/*   }*/
/*   #err-msg*/
/*   {*/
/*     color: white;*/
/*     height: 43px;*/
/*     width: 200px;*/
/*     background: #083044;*/
/*     box-shadow: 0 10px 6px -6px #777;*/
/*     border-radius: 3px;*/
/*     font-size: 16px;*/
/*     margin: auto;*/
/*     padding-top: 10px;*/
/*     margin-top: -10px;*/
/*   }*/
/*   #err-msg-already*/
/*   {*/
/*   	 color: white;*/
/*     height: 43px;*/
/*     width: 348px;*/
/*     background: #083044;*/
/*     box-shadow: 0 10px 6px -6px #777;*/
/*     border-radius: 3px;*/
/*     font-size: 16px;*/
/*     margin: auto;*/
/*     padding-top: 10px;*/
/*     margin-top: -10px;*/
/*   }*/
/*   #err-msg img*/
/*   {*/
/*     margin-right:8px;*/
/*   }*/
/*   .countrycode*/
/*   {*/
/*     left: 0;*/
/*     padding-right: 5px;*/
/*     border-right: 1px solid #c2c2c2;*/
/*     bottom: 7px;*/
/*     font-size: 14px;*/
/*     position: absolute;*/
/*     display: inline-block;*/
/*   }*/
/*    .countrycode1*/
/*   {*/
/*     left: 0;*/
/*     padding-right: 5px;*/
/*     border-right: 1px solid #c2c2c2;*/
/*     bottom: 7px;*/
/*     font-size: 14px;*/
/*     position: absolute;*/
/*     display: inline-block;*/
/*   }*/
/*   .signup-newcus*/
/*   {*/
/*     margin-top:19px;*/
/*   }*/
/*   .signupbutton*/
/*   {*/
/*     margin-bottom: 20px;*/
/*     width: 325px;*/
/*     height: 49px;*/
/*     background: #fb641b;*/
/*     box-shadow: 0 5px 5px 0 rgba(0,0,0,.2);*/
/*     border: none;*/
/*     margin-top: 13px;*/
/*   }*/
/*   .signupbutton:hover*/
/*   {*/
/*   	    background: #fb641b!important;*/
/* */
/*   }*/
/* .redcolor*/
/* {*/
/* 	color:red;*/
/*   float: left;*/
/* }*/
/* span.emailerror*/
/* {*/
/*   float:left;*/
/* }*/
/* span.regotperror_msg.redcolor*/
/* {*/
/*   float:left;*/
/* }*/
/* button.btn.btn-lg.forgetloginbutton*/
/* {*/
/*     width: 325px;*/
/*     height: 49px;*/
/*     background: #fb641b;*/
/*     box-shadow: 0 1px 2px 0 rgba(0,0,0,.2);*/
/*     border: none;*/
/*     color: #fff;*/
/*     margin-top: 13px;*/
/* }*/
/* #signupcontent*/
/* {*/
/*   margin-top:35px;*/
/* }*/
/* .countrycode*/
/* {*/
/* 	bottom: 9px;*/
/* 	position: absolute;*/
/*     top: 6px;*/
/*     font-size: 14px;*/
/*     left: 0;*/
/*     padding-right: 5px;*/
/*     border-right: 1px solid #c2c2c2;*/
/* }*/
/* .form-control1 {*/
/*     border: 0;*/
/*     outline: 0;*/
/*     background: transparent;*/
/*     border-bottom: 1px solid black;*/
/*     width: 20%;*/
/*     text-align: center;*/
/*     padding: 5px;*/
/*     margin-left: 10px;*/
/* }*/
/* #partitioned {*/
/*     padding-left: 20px;*/
/*     letter-spacing: 38px;*/
/*     background-image: linear-gradient(to left, #94918f 75%, rgba(255, 255, 255, 0) 0%);*/
/*     background-position: bottom;*/
/*     background-size: 47px 1px;*/
/*     background-repeat: repeat-x;*/
/*     background-position-x: 90px;*/
/*     width: 305px;*/
/*     min-width: 305px;*/
/* 	border: none !important;*/
/* }*/
/* #partitioned:focus {*/
/*   outline: 0;*/
/* }*/
/* #divInner{*/
/*   left: 0;*/
/*   position: sticky;*/
/* }*/
/* */
/* #divOuter{*/
/*   width: 290px; */
/*   overflow: hidden;*/
/*   margin: auto;*/
/* }*/
/* input.floating-input*/
/* {*/
/* 	border-bottom:1px solid #0000002e!important;*/
/*     border-radius: 0px;*/
/*     background:white!important;*/
/*     padding-left:0px!important;*/
/* }*/
/* input.floating-input:focus*/
/* {*/
/*   background:white!important;*/
/*   padding-left:0px!important;*/
/* }*/
/* */
/* .modal-login*/
/* {*/
/* 	  margin-top:70px!important;*/
/* 	  background: inherit!important;*/
/* */
/* }*/
/* input[type="text"]*/
/* {*/
/*   background-color:white!important;*/
/*   border-bottom:1px solid #0000002e!important;*/
/*   border-radius:0px!important;*/
/* }*/
/* input[type="text"]:focus, input[type="text"]:hover*/
/* {*/
/*   background-color:white;*/
/* }*/
/* .floating-form {*/
/*   width:320px;*/
/* }*/
/* */
/* /****  floating-Lable style start ****//* */
/* .floating-label { */
/*   position:relative; */
/*   margin-bottom:30px; */
/* }*/
/* .floating-input , .floating-select {*/
/*   font-size:14px;*/
/*   padding:4px 4px;*/
/*   display:block;*/
/*   width:100%;*/
/*   height:36px;*/
/*   background-color: transparent;*/
/*   border:none;*/
/*   border-bottom:1px solid #757575;*/
/* }*/
/* */
/* .floating-input:focus , .floating-select:focus {*/
/*      outline:none!important;*/
/*      border-bottom:1px solid #f67100!important; */
/* }*/
/* */
/* .floating-label label {*/
/*   color:#999; */
/*   font-size:14px;*/
/*   font-weight:normal;*/
/*   position:absolute;*/
/*   pointer-events:none;*/
/*   left:0px;*/
/*   top:-18px;*/
/*   transition:0.2s ease all; */
/*   -moz-transition:0.2s ease all; */
/*   -webkit-transition:0.2s ease all;*/
/* }*/
/* .floating-input:focus ~ label{*/
/*   top:-18px;*/
/*   font-size:12px;*/
/*   color:#999;*/
/* }*/
/* */
/* */
/* .floating-input:focus ~ label , .floating-input:not([value=""]):valid ~ label {*/
/*   top:-18px;*/
/*   font-size:14px;*/
/* }*/
/* */
/* /* active state *//* */
/* .floating-input:focus ~ .bar:before, .floating-input:focus ~ .bar:after, .floating-select:focus ~ .bar:before, .floating-select:focus ~ .bar:after {*/
/*   width:50%;*/
/* }*/
/* */
/* *, *:before, *:after {*/
/*     -webkit-box-sizing: border-box;*/
/*     -moz-box-sizing: border-box;*/
/*     box-sizing: border-box;*/
/* }*/
/* */
/* .floating-textarea {*/
/*    min-height: 30px;*/
/*    max-height: 260px; */
/*    overflow:hidden;*/
/*   overflow-x: hidden; */
/* }*/
/* */
/* /* highlighter *//* */
/* .highlight {*/
/*   position: absolute;*/
/*     height: 50%;*/
/*     width: 100%;*/
/*     top: 105%;*/
/*     left: -100px;*/
/*     pointer-events: none;*/
/* }*/
/* */
/* /* active state *//* */
/* .floating-input:focus ~ .highlight , .floating-select:focus ~ .highlight {*/
/*   -webkit-animation:inputHighlighter 0.3s ease;*/
/*   -moz-animation:inputHighlighter 0.3s ease;*/
/*   animation:inputHighlighter 0.3s ease;*/
/*       display:none;*/
/* */
/* }*/
/* */
/* /* animation *//* */
/* @-webkit-keyframes inputHighlighter {*/
/* 	from { background:#5264AE; }*/
/*   to 	{ width:0; background:transparent; }*/
/* }*/
/* @-moz-keyframes inputHighlighter {*/
/* 	from { background:#5264AE; }*/
/*   to 	{ width:0; background:transparent; }*/
/* }*/
/* @keyframes inputHighlighter {*/
/* 	from { background:#5264AE; }*/
/*   to 	{ width:0; background:transparent; }*/
/* }*/
/* */
/* /****  floating-Lable style end ****/
/* */
/* /***   Body style start  ***//* */
/* */
/* html {*/
/*     font-family: "Helvetica Neue", Helvetica, "Noto Sans", sans-serif, Arial, sans-serif;*/
/*     font-size: 12px;*/
/*     line-height: 1.42857143;*/
/*     color: #949494;*/
/*     background-color: #ffffff;*/
/* }*/
/* /***   Body style end  ***//* */
/* */
/* */
/* /***   daniel - Fork me friend - style   ***//* */
/* .floating-credit { position:fixed; bottom:10px;right:10px; color:#aaa; font-size:13px;font-family:arial,sans-serif; }*/
/* .floating-credit a { text-decoration:none; color:#000000; font-weight:bold; }*/
/* .floating-credit a:hover { border-bottom:1px dotted #f8f8f8; }*/
/* .floating-heading { position:fixed; color:#aaa; font-size:20px; font-family:arial,sans-serif; }*/
/* /***  daniel - Fork me friend - style  ***//* */
/* */
/* .newtopoor*/
/* {*/
/*   background:white!important;*/
/*   border-color:white!important;*/
/*   color:#fb641b!important;*/
/*   font-weight:500;*/
/* }*/
/* #secondheader{*/
/* 	position:inherit!important;*/
/* }*/
/* .resendcode*/
/* {*/
/*   color:#f79000!important;*/
/*   font-weight:500;*/
/* }*/
/* .otp-field1*/
/* {*/
/*   border:none!important;*/
/*   border-bottom: 1px solid #e0e0e0!important;*/
/* width:28px;*/
/* text-align:center;*/
/* border-radius:0px!important;*/
/* background-color:white!important;*/
/* padding:0px!important;*/
/* margin-right:10px;*/
/* }*/
/* #otp-head*/
/* {*/
/*  padding-top: 30px;*/
/*     font-size: 16px;*/
/*     color: black;*/
/*     text-align: center;*/
/* }*/
/* #show*/
/* {*/
/*   margin-top:30px;*/
/* }*/
/*   <!--Popup Design-->*/
/* input[type="text"]*/
/* {*/
/* background-color:white!important;*/
/* }*/
/* .loginbutton1{*/
/* 	position: absolute;*/
/* margin-top:70px;*/
/*     left: 0;*/
/*     right: 0;*/
/*     text-align: center;*/
/*     font-weight: 500;*/
/*     font-size: 14px;*/
/*     color:#2874f0;*/
/*     cursor: pointer;*/
/* }*/
/* .verifybutton*/
/* {*/
/*   width: 260px;*/
/*     height: 49px; */
/*     background-color:#FF5722!important;*/
/*     box-shadow: 0 1px 2px 0 rgba(0,0,0,.2);*/
/*     border: none;*/
/*     color: #fff;*/
/*     margin-top:40px;*/
/* }*/
/* .verifybutton:hover*/
/* {*/
/*   background: #FF5722;*/
/* }*/
/* #otp-field:hover*/
/* {*/
/* 	background:none!important;*/
/* 	background-color:none!important;*/
/* }*/
/* .modal-content*/
/*   {*/
/*     width: 670px!important;*/
/*     height: 520px!important;*/
/*     border:none!important;*/
/*     border-radius: inherit;*/
/*     margin-top: 70px!important;*/
/*     margin: auto;*/
/*   }*/
/*   .otp-field:hover*/
/*   {*/
/*     background-color: #f5641b!important;*/
/*   }*/
/*   .loginbutton:hover*/
/*   {*/
/*     background-color: rgb(245 100 27)!important;*/
/*   }*/
/*  .loginbutton*/
/*   {*/
/*     width: 325px;*/
/*     height: 49px;*/
/*     background:#ff531f;*/
/*     box-shadow: 0 5px 5px 0 rgba(0,0,0,.2);*/
/*     border: none;*/
/*     margin-top:13px;*/
/*     */
/*   }*/
/*   .Existing-loginbutton*/
/*   {*/
/*     width: 325px;*/
/*     height: 49px;*/
/*     background: #fb641b;*/
/*     box-shadow: 0 5px 5px 0 rgba(0,0,0,.2);*/
/*     border: none;*/
/*     margin-top:13px;*/
/*   }*/
/*   #continuebutton*/
/*   {*/
/*     width: 325px;*/
/*     height: 49px; */
/*     background:#FF5722;*/
/*     box-shadow: 0 1px 2px 0 rgba(0,0,0,.2);*/
/*     border: none;*/
/*     color: #fff;*/
/*     margin-bottom:15px;*/
/*   }*/
/*   #otp-field*/
/*   {*/
/*     width: 325px;*/
/*     height: 49px;*/
/*     background: transparent;*/
/*     box-shadow: 0 5px 2px 0 rgba(0,0,0,.2);*/
/*     border: none;*/
/*     font-size:15px;*/
/*     color: #f55b00;*/
/*     font-weight:500;*/
/*   }*/
/*   .newtopoor*/
/*   {*/
/* 	  width: 325px;*/
/*     height: 49px;*/
/*     background: transparent;*/
/*     box-shadow: 0 1px 2px 0 rgba(0,0,0,.2);*/
/*     border: none;*/
/* 	font-size:16px;*/
/*     color: #000;*/
/*   }*/
/*  .btn:focus*/
/*  {*/
/*   color: white!important;*/
/*   outline: none!important;*/
/*  }*/
/*   .loginleft*/
/*   {*/
/*     height: 520px;*/
/*     background: #ff531f;*/
/*     padding: 40px 35px;*/
/*     font-size: 16px;*/
/*     color: white;*/
/*   }*/
/* .modal-footer*/
/* {*/
/*   text-align: center!important;*/
/*   border:none!important;*/
/*     padding-bottom: 0px!important;*/
/*     padding-top:30px;*/
/*     padding:0px;*/
/* }*/
/* .wrap-input2 {*/
/*   width: 100%;*/
/*   position: relative;*/
/* border-bottom: 2px solid #3c3c3c;*/
/*   margin-bottom: 37px;*/
/* }*/
/* .input2 */
/* {*/
/*   display: block;*/
/*   width: 100%;*/
/*   font-size: 15px;*/
/*   color: #555555;*/
/*   line-height: 1.2;*/
/* }*/
/* .focus-input2 {*/
/*   position: absolute;*/
/*   display: block;*/
/*   width: 100%;*/
/*   height: 100%;*/
/*   top: 0;*/
/*   text-align:left!important;*/
/*   left: 0;*/
/*   pointer-events: none;*/
/* }*/
/* .focus-input2::before {*/
/*   content: "";*/
/*   display: block;*/
/*   position: absolute;*/
/*   bottom: -2px;*/
/*   left: 0;*/
/*   width: 0;*/
/*   height: 2px;*/
/* }*/
/* */
/* .focus-input2::after {*/
/*   content: attr(data-placeholder);*/
/*   display: block;*/
/*   width: 100%;*/
/*   position: absolute;*/
/*   top: -13px!important;*/
/*   left: 0;*/
/*   font-family: Poppins-Regular;*/
/*   font-size: 13px;*/
/*   color: #999999;*/
/*   line-height: 1.2;*/
/* }*/
/* */
/* input.input2 {*/
/*   height: 40px;*/
/*   border: none;*/
/*   background:white!important;*/
/* }*/
/* */
/* input.input2 + .focus-input2::after {*/
/*   top: 16px;*/
/*   left: 0;*/
/* }*/
/* */
/* textarea.input2 {*/
/*   min-height: 115px;*/
/*   padding-top: 13px;*/
/*   padding-bottom: 13px;*/
/* }*/
/* */
/* textarea.input2 + .focus-input2::after {*/
/*   top: 16px;*/
/*   left: 0;*/
/* }*/
/* */
/* .input2:focus + .focus-input2::after {*/
/*   top: -25px;*/
/*   */
/* }*/
/* */
/* .input2:focus + .focus-input2::before {*/
/*   width: 100%;*/
/* }*/
/* */
/* .has-val.input2 + .focus-input2::after {*/
/*   top: -13px;*/
/* }*/
/* */
/* .has-val.input2 + .focus-input2::before {*/
/*   width: 100%;*/
/* }*/
/* .modal-body*/
/* {*/
/*   width: 320px;*/
/*     padding: 33px 0px!important;*/
/*     margin-left: 33px;*/
/* }*/
/* .modal-content p*/
/* {*/
/*   font-size: 20px;*/
/* }*/
/* .input2:focus*/
/* {*/
/*   outline: none!important;*/
/* }*/
/* .forgetpsw*/
/* {*/
/*  font-size: 14px;*/
/*  text-align: right; */
/*   font-weight: 500;*/
/* }*/
/* .forgetpsw a*/
/* {*/
/* color: #2874f0;*/
/* font-weight:500px;*/
/* }*/
/* button.close*/
/* {*/
/*   font-size: 50px!important;*/
/*     color: white;*/
/*     margin-top: -12px;*/
/*     font-weight: 100;*/
/* }*/
/* .or*/
/* {*/
/*   padding: 13px;*/
/*     font-weight: 600;*/
/* }*/
/* .signin*/
/* {*/
/*     font-size: 25px;*/
/*     text-align: center;*/
/*     color: #000;*/
/*     font-weight: 700;*/
/*     margin-bottom:40px;*/
/* }*/
/* .signin1*/
/* {*/
/*   font-size: 24px;*/
/*     text-align: center;*/
/*     padding-bottom: 45px;*/
/* color: #3c3c3c;*/
/*     font-weight: 700;*/
/* }*/
/* .fa-fw*/
/* {*/
/*   float: right!important;*/
/*     position: relative!important;*/
/*     margin-top: -20px!important;*/
/* }*/
/* .fa-eye:before*/
/* {*/
/*   font-weight: 500;*/
/*     color: #7d7d7d;*/
/* }*/
/* .fa-fw*/
/* {*/
/*   color:#7d7d7d; */
/* }*/
/* .resendotp */
/* {*/
/*   position: relative;*/
/*     font-weight: 600;*/
/*     float: right;*/
/*     margin-top: -25px;*/
/* }*/
/* .resendotp a*/
/* {*/
/*    color: #f55100;*/
/*    font-size:14px;*/
/* }*/
/* .test*/
/* {*/
/*   height: 515px!important;*/
/* }*/
/* input[type=number]::-webkit-inner-spin-button, */
/* input[type=number]::-webkit-outer-spin-button*/
/*  { */
/*   -webkit-appearance: none; */
/*   margin: 0; */
/* }*/
/* #result*/
/* {*/
/*   margin-top: -37px;*/
/*     margin-bottom: 36px;*/
/* }*/
/* @media only screen and (max-width: 900px) {*/
/*   .modal-body {*/
/*     width: 50%;*/
/*     padding: 0px 0px;*/
/*     margin-left: 24px;*/
/*   }*/
/*   .loginbutton {*/
/*     width: 100%;*/
/*     height: 48px;*/
/*   }*/
/* */
/*     .loginbutton1 {*/
/*     width: 100%;*/
/*     height: 42px;*/
/*   }*/
/*   .modal-content*/
/*   {*/
/*     width: 100%!important;*/
/*   }*/
/* }*/
/* <!--End-->*/
/* */
/* /* Begin Saerch bar style *//* */
/* .container-megamenu.horizontal ul.megamenu > li {*/
/*     margin-right: 0 !important;*/
/* }*/
/* .typeheader-2 .search-header-w {*/
/*     margin: 12px 10px 10px 32px;*/
/* }*/
/* .typeheader-2 .search-header-w .input-group{*/
/*     display: block;*/
/*     width: 100%;*/
/*     float: left;*/
/* }*/
/* .search-header-w .input-group .button-search {*/
/*     right: 0px;*/
/*     background-image: linear-gradient(45deg,#dc4300 ,#f57000a3);*/
/*         width: 80px !important;*/
/*     line-height: 34px;*/
/*     position: absolute;*/
/*     top: 0px;*/
/*     text-align: center;*/
/*     padding: 0;*/
/* }.input-group .form-control:first-child, .input-group-addon:first-child, .input-group-btn:first-child>.btn, .input-group-btn:first-child>.btn-group>.btn, .input-group-btn:first-child>.dropdown-toggle, .input-group-btn:last-child>.btn-group:not(:last-child)>.btn, .input-group-btn:last-child>.btn:not(:last-child):not(.dropdown-toggle)*/
/* {*/
/*     border-radius:inherit;*/
/*     border:none!important;*/
/* }*/
/* .typeheader-2 .search-header-w input {*/
/*     height: 35px !important;*/
/*     padding-left: 22px;*/
/*     background: #dcd1d180 !important;*/
/*     border: 1px solid #c5c5c5 !important;*/
/*     font-size: 14px;*/
/*     width:525px;*/
/* }*/
/* #myInput::-webkit-input-placeholder { */
/*   color: #808080;*/
/*   font-size: 12px;*/
/*   letter-spacing: 0.5px;*/
/* }*/
/* .autocomplete-items.dropdown-menu{*/
/*     padding: 0;*/
/*     margin: 0;*/
/*     min-width: 350px;*/
/*     overflow: hidden scroll;*/
/*     border: 0;*/
/*     max-height: 450px;*/
/*     left: 0;*/
/* }*/
/* .autocomplete {*/
/*     position: relative;*/
/*     display: inline-block;*/
/*   }*/
/* */
/* .autocomplete-items.dropdown-menu li {*/
/*     padding: 6px;*/
/*     margin: 0;*/
/*     border-bottom: 1px solid #e5e5e5;*/
/* }*/
/* li.liremove a {*/
/*     text-transform: capitalize;*/
/*     color: #000;*/
/*     font-weight: 500;*/
/* }*/
/* li.liremove:hover, li.liremove.autocomplete-active{*/
/*       background: #f1f1f1;*/
/* }*/
/* li.liremove.autocomplete-active a{*/
/*       color: #fda30e;*/
/* }*/
/* */
/* .typeheader-2 .search-header-w input:hover{*/
/*     border-color: #fd6500 !important;*/
/* }*/
/* .item_list{*/
/*     display: block !important;*/
/*     opacity: 1 !important;*/
/*     visibility: visible !important;*/
/* }*/
/* */
/* /* End Saerch bar style *//* */
/* */
/* #widget-global-52a2ipbiyj .lc-8cxdov {*/
/*    color: #0e0d0d !important;*/
/* }*/
/* .phone_price {*/
/*     background: url(https://s1.poorvikamobile.com/image/data/Back%20Case/price-back.jpg) no-repeat;*/
/*     background-position: center;*/
/*     background-size: cover;*/
/*     padding-top:25px;*/
/* 	margin-top: 25px*/
/* }*/
/* .price_head {*/
/*     display: flex;*/
/*     justify-content: center;*/
/*     padding-left: 5px;*/
/* }*/
/* ul.price_head li {*/
/*     padding: 7px 21px;*/
/*     border: 3px solid #fff;*/
/*     border-radius: 20px;*/
/* 	margin-right: 15px;*/
/* }*/
/* ul.price_head li a {*/
/*     color: #fff;*/
/*     font-size: 15px;*/
/* 	font-weight: 600;*/
/* }*/
/* .tab-content{*/
/* 	border:none;*/
/* 	padding-bottom:20px;*/
/* 	margin:0;*/
/* }*/
/* ul.price_head li.active, ul.price_head li:hover {*/
/*     background: #ff6600;*/
/* 	border: 3px solid #f60;*/
/* }*/
/* .budget_list ul li {*/
/*     display: inline-block;*/
/*     padding: 5px 10px;*/
/*     background: #fff;*/
/*     border-radius: 3px;*/
/* 	text-align: center;*/
/* }*/
/* .budget_list ul{*/
/* 	text-align: center;*/
/* }*/
/* */
/* .budget_list ul a {*/
/*     color: #000;*/
/*     font-size: 12px;*/
/*     font-weight: 600;*/
/* }*/
/* .budget_list.camera_price{*/
/*     width: 80%;*/
/*     margin: 0 auto;*/
/* }*/
/* .budget_list.camera_price li {*/
/*     width: 18%;*/
/* }*/
/* .budget_list ul li:hover {*/
/*     background: #18ab97;*/
/* }*/
/* .budget_list ul li a:hover, .budget_list ul li:hover a {*/
/*     color: #fff;*/
/* }*/
/* .dealsproduct_pos span {*/
/*     width: 50px;*/
/*     height: 50px;*/
/*     color: #fff;*/
/*     background: #04a211;*/
/*     font-weight: 600;*/
/*     display: block;*/
/*     line-height: 50px;*/
/*     text-align: center;*/
/*     font-size: 18px;*/
/*     margin: 12px auto 10px;*/
/*     border-radius: 50%;*/
/* }*/
/* .product_deals_page{*/
/* 	padding: 35px 0 50px;*/
/* }*/
/* .dealsproduct_pos img{*/
/* 	border:none;*/
/* }*/
/* .dealsproduct_grd h5{*/
/* 	text-align: center;*/
/*     font-weight: 500;*/
/*     color: #ea732b;*/
/*     font-size: 15px;*/
/* }*/
/* .product_deals_page h1 {*/
/*     color: #000;*/
/*     font-size: 25px;*/
/* }*/
/* .common-deals ul.breadcrumb{*/
/* 	    margin-bottom: 10px;*/
/* }*/
/* .product_deals_page ul {*/
/* 	text-align: center;*/
/* 	margin-bottom: 30px;*/
/* }*/
/* .product_deals_page ul li{*/
/* 	display: inline-block;*/
/* }*/
/* .product_deals_page ul li a{*/
/*     padding: 8px 12px;*/
/*     color: #fff;*/
/*     background: #000;*/
/*     display: block;*/
/*     text-transform: uppercase;*/
/*     font-size: 14px;*/
/*     letter-spacing: 1px;*/
/*     border-radius: 4px;*/
/* }*/
/* .product_deals_page ul li a.active{*/
/* 	background: #fda30e;*/
/* }*/
/* .my_deals_offer_new, .my_deals_offer{*/
/*     padding: 18px 0;*/
/*     margin-bottom: 20px;*/
/*     border-radius: 5px;*/
/* 	background: #e0e0e0;*/
/*     box-shadow: 0px 0px 5px #828282;*/
/* }*/
/* .my_deals_offer_new{*/
/* 	height: 350px;*/
/* }*/
/* .deals_title_four{*/
/* 	background: #5b9426;*/
/* }*/
/* .deals_title_five{*/
/* 	background: #313f88;*/
/* }*/
/* .deals_title_six{*/
/* 	background: #e87800;*/
/* }*/
/* */
/* .round_effect .round_btm div a {*/
/*     display: block;*/
/*     position: relative;*/
/*     overflow: hidden;*/
/* }*/
/* .round_effect .round_btm div a:hover:before, .round_effect .round_btm div a:hover:after {*/
/*     border: 0 solid rgba(255, 255, 255, 0.7);*/
/*     opacity: 0;*/
/*     filter: alpha(opacity=0);*/
/* }*/
/* .round_effect .round_btm div a:before, .round_effect .round_btm div a:after {*/
/*     border: 50px solid transparent;*/
/*     border-top-right-radius: 50px;*/
/*     border-top-left-radius: 50px;*/
/*     border-bottom-right-radius: 50px;*/
/*     border-bottom-left-radius: 50px;*/
/*     box-sizing: border-box;*/
/*     cursor: pointer;*/
/*     display: inline-block;*/
/*     left: 0;*/
/*     right: 0;*/
/*     bottom: 0;*/
/*     margin: auto;*/
/*     position: absolute;*/
/*     top: 0;*/
/*     content: "";*/
/*     opacity: 1;*/
/*     filter: alpha(opacity=100);*/
/*     width: 100px;*/
/*     height: 100px;*/
/*     -webkit-transform: scale(7);*/
/*     -moz-transform: scale(7);*/
/*     -ms-transform: scale(7);*/
/*     -o-transform: scale(7);*/
/*     transform: scale(7);*/
/*     transition: all 0.4s ease-in-out;*/
/*     -moz-transition: all 0.4s ease-in-out;*/
/*     -webkit-transition: all 0.4s ease-in-out;*/
/*     visibility: visible;*/
/*     z-index: 1;*/
/* }*/
/* #so-groups3 {*/
/*     top: 55%;*/
/*     position: fixed;*/
/*     right: 0;*/
/*     z-index: 999;*/
/*     /*background: #f60;*//* */
/*     width: 50px;*/
/* }*/
/* .whatsapp {*/
/*     float: left;*/
/*     width: 100%;*/
/*     display: block;*/
/*     cursor: help;*/
/*     text-align: center;*/
/*     color: #fff;*/
/*     border-bottom: 1px solid rgba(255,255,255,.5);*/
/*     padding: 10px;*/
/*     height: 43px;*/
/*     position: relative;*/
/* }*/
/* .whatsapp a{*/
/*     font-size: 26px;*/
/*     color: #fff;*/
/* }*/
/* #so-groups > div > i {*/
/*     font-size: 25px;*/
/* }*/
/* .left_groups >div span {*/
/*     left: 100% !important;*/
/*     right: inherit !important;*/
/* }*/
/* #so-groups > div:hover {*/
/*     background-color: #ec3a01;*/
/* }*/
/* #so-groups > div:hover span{*/
/*     opacity: 1;*/
/*     transition: all 0.2s ease-in-out 0s;*/
/*     visibility: visible;*/
/*     width: auto;*/
/* }*/
/* */
/* #so-groups1 {*/
/*     top: 65%;*/
/*     position: fixed;*/
/*     right: 0;*/
/*     z-index: 999;*/
/*     background: #ccc;*/
/*     width: 42px;*/
/* }*/
/* #so-groups1 .whatsapp {*/
/*     float: left;*/
/*     width: 100%;*/
/*     display: block;*/
/*     cursor: help;*/
/*     text-align: center;*/
/*     color: #fff;*/
/*     border-bottom: 1px solid rgba(255,255,255,.5);*/
/*     padding: 5px;*/
/*     height: inherit !important;*/
/*     position: relative;*/
/* }*/
/* #so-groups1 .whatsapp a{*/
/*     font-size: 26px;*/
/*     color: #fff;*/
/* }*/
/* #so-groups1 > div span {*/
/* background: #0089fe;*/
/*     color: #ffffff;*/
/*     display: inline-block;*/
/*     font-size: 18px;*/
/*     line-height: 22px;*/
/*     opacity: 0;*/
/*     padding: 10px;*/
/*     position: absolute;*/
/*     right: 100%;*/
/*     text-align: center;*/
/*     text-transform: capitalize;*/
/*     top: 0;*/
/*     transition: all 0.2s ease-in-out 0s;*/
/*     visibility: hidden;*/
/*     white-space: nowrap;*/
/*     width: auto;*/
/* }*/
/* #so-groups1 > div > i {*/
/*     font-size: 25px;*/
/* }*/
/* */
/* */
/* #so-groups1 > div:hover span{*/
/*     opacity: 1;*/
/*     transition: all 0.2s ease-in-out 0s;*/
/*     visibility: visible;*/
/*     width: auto;*/
/* }*/
/* */
/* #header .container {*/
/*     max-width: 1650px;*/
/*     padding: 0;*/
/*     width: 85%!important;*/
/* }*/
/* .typeheader-2 .header-bottom*/
/* {*/
/*     background-color:transparent!important;*/
/* }*/
/* */
/* .my_account_new .dropdown_myacc*/
/* {*/
/*     display: none;*/
/*     background: white;*/
/*     width: 210px;*/
/*     color:black;*/
/*     z-index:999;*/
/* }*/
/* .acc*/
/* {*/
/*     color: black!important;*/
/*     font-size: 14px!important;*/
/*     padding-right: 8px;*/
/* }*/
/* .typeheader-2 ul.top-link li*/
/* {*/
/*     padding-top: 8px;*/
/*     font-size: 13px;*/
/* }*/
/* hr*/
/* {*/
/*     margin-top:0px!important;*/
/*     border-top: 1px solid #f5690145;*/
/* }*/
/* .shopcart*/
/* {*/
/*     color:#FF9800!important;*/
/* }*/
/* .typeheader-2 .shopping_cart .btn-shopping-cart .total-shopping-cart .items_cart*/
/* {*/
/*     background-image: linear-gradient(45deg,#dc4300 ,#f57000a3);*/
/*     }*/
/* .typeheader-2 .container-megamenu.horizontal ul.megamenu.menu__list li a*/
/* {*/
/*     padding: 18px 20px !important;*/
/*     color:white!important;*/
/*     font-size: 14px!important;*/
/* }*/
/* */
/* @media (max-width:1080px) {*/
/* 	.typeheader-2 .container-megamenu.vertical:hover .vertical-wrapper{*/
/* 		top: 48px !important;*/
/* 	}*/
/* 	ul.price_head li {*/
/* 		padding: 7px 15px;*/
/* 	}*/
/* }*/
/* @media (max-width:991px) {*/
/* 	.product_deals_page ul li a {*/
/* 		margin-bottom: 8px;*/
/* 	}*/
/* 	.dealsproduct_grd {*/
/* 		margin: 8px 0;*/
/* 	}*/
/* 	.my_deals_offer_new {*/
/* 		height: inherit;*/
/* 	}*/
/* 	ul.price_head li {*/
/* 		padding: 7px 5px;*/
/* 	}*/
/* 	ul.price_head li a {*/
/* 		font-size: 13px;*/
/* 	}*/
/* 	.budget_list.camera_price{*/
/* 		width: 100%;*/
/* 	}*/
/* }*/
/* @media (max-width:767px) {*/
/* 	ul.price_head li a {*/
/* 		font-size: 12px;*/
/* 	}*/
/* 	ul.price_head li{*/
/* 		margin-right: 7px;*/
/* 		border-radius: 10px;*/
/* 	}*/
/* 	.budget_list.camera_price li {*/
/*     width: 20%;*/
/* }*/
/* }*/
/* @media (max-width:720px) {*/
/*   .budget_list ul li a {*/
/*       padding: 12px;*/
/*   }*/
/*   ul.price_head li {*/
/*     padding: 5px 5px !important;*/
/*   }*/
/*   ul.price_head li a {*/
/*     font-size: 10px !important;*/
/*   }*/
/* }*/
/* @media (max-width:585px) {*/
/* 	ul.price_head li a {*/
/* 		font-size: 11px !important;*/
/* 	}*/
/* 	ul.price_head li{*/
/* 		text-align: center;*/
/* 	}*/
/* 	.budget_list.camera_price li {*/
/* 		width: 25%;*/
/* 	}*/
/* }*/
/* */
/* </style>*/
/*  <script src="image/loadspinner/src/jquery.preloaders.js"></script>*/
/*  <script src="http://poorvikabeta.webindia.com/catalog/view/theme/so-destino/js/owl.carousel.min.js"></script> */
/* <script>*/
/*  $(document).ready(function(){*/
/*   $('.home-video-carousel1').owlCarousel({*/
/*     loop:true,*/
/*    autoplay:true,*/
/*     autoplayTimeout:3000,*/
/*     autoplayHoverPause:true,*/
/*     nav:true,*/
/* 	dots:false,*/
/*     responsive:{*/
/*         0:{*/
/*             items:1*/
/*         },*/
/* 		320:{*/
/*             items:1*/
/*         },*/
/* 		480:{*/
/*             items:1*/
/*         },*/
/*         600:{*/
/*             items:2*/
/*         },*/
/* 		767:{*/
/*             items:2*/
/*         },*/
/* 		991:{*/
/*             items:3*/
/*         },*/
/*         1200:{*/
/*             items:4*/
/*         }*/
/*     }*/
/* })*/
/* $( ".owl-prev").html('<i class="fa fa-lg fa-arrow-left"></i>');*/
/*  $( ".owl-next").html('<i class="fa fa-lg fa-arrow-right"></i>');*/
/* });	*/
/* */
/* </script>*/
/* <script>*/
/*  $(document).ready(function(){*/
/*   $('.brand-logo-carousel').owlCarousel({*/
/*     loop:true,*/
/*    autoplay:true,*/
/*     autoplayTimeout:3000,*/
/*     autoplayHoverPause:true,*/
/*     nav:false,*/
/* 	dots:false,*/
/*     responsive:{*/
/*         0:{*/
/*             items:1*/
/*         },*/
/* 		320:{*/
/*             items:2*/
/*         },*/
/* 		480:{*/
/*             items:3*/
/*         },*/
/*         600:{*/
/*             items:4*/
/*         },*/
/* 		767:{*/
/*             items:5*/
/*         },*/
/* 		991:{*/
/*             items:7*/
/*         },*/
/*         1200:{*/
/*             items:10*/
/*         }*/
/*     }*/
/* })*/
/* $( ".owl-prev").html('<i class="fa fa-lg fa-angle-left"></i>');*/
/*  $( ".owl-next").html('<i class="fa fa-lg fa-angle-right"></i>');*/
/* });	*/
/* */
/* </script>*/
/* <script>*/
/*     $(function(){*/
/*       // bind change event to select*/
/*       $('#top_search1').on('change', function () {*/
/*           var url = $(this).val(); // get selected value*/
/*           if (url) { // require a URL*/
/*               window.location = url; // redirect*/
/*           }*/
/*           return false;*/
/*       });*/
/*     });*/
/* </script>*/
/* <script>*/
/* window.onscroll = function() {myFunction()};*/
/* */
/* var header = document.getElementById("scrollHeader");*/
/* var sticky = header.offsetTop;*/
/* */
/* function myFunction() {*/
/*   if (window.pageYOffset > sticky) {*/
/*     header.classList.add("sticky");*/
/*   } else {*/
/*     header.classList.remove("sticky");*/
/*   }*/
/* }*/
/* </script>*/
