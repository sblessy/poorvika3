<?php

/* default/template/extension/module/featured.twig */
class __TwigTemplate_e673102012b28bc65e2f9f320ca62d20b8f4549039634a2115118544d0b76779 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<h3>";
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h3>
<div class=\"row\">
 ";
        // line 3
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 4
            echo "  <div class=\"product-layout col-lg-3 col-md-3 col-sm-6 col-xs-12\">
    <div class=\"product-thumb transition\">
      <div class=\"image\"><a href=\"";
            // line 6
            echo $this->getAttribute($context["product"], "href", array());
            echo "\"> 
 <img src=\"";
            // line 7
            echo $this->getAttribute($context["product"], "thumb", array());
            echo "\" alt=\"";
            echo $this->getAttribute($context["product"], "name", array());
            echo "\" title=\"";
            echo $this->getAttribute($context["product"], "name", array());
            echo "\" class=\"img-responsive\" id=\"product-image-";
            echo $this->getAttribute($context["product"], "product_id", array());
            echo "\" /> 
 </a></div>
      <div class=\"caption\">
        <h4><a href=\"";
            // line 10
            echo $this->getAttribute($context["product"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["product"], "name", array());
            echo "</a></h4>
 
 ";
            // line 12
            if ($this->getAttribute($context["product"], "option", array())) {
                echo " 
 <ul class=\"so-productlist-colorswatch\" id=\"colorswatch-";
                // line 13
                echo $this->getAttribute($context["product"], "product_id", array());
                echo "\"> 
 ";
                // line 14
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["product"], "option", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["option"]) {
                    echo " 
 ";
                    // line 15
                    if (($this->getAttribute($context["option"], "type", array()) == "select")) {
                        echo " 
 ";
                        // line 16
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["option"], "product_option_value", array()));
                        foreach ($context['_seq'] as $context["_key"] => $context["option_value"]) {
                            echo " 
 <li class=\"colorswatch";
                            // line 17
                            echo (($this->getAttribute($context["option_value"], "product_option_value_id", array()) . $this->getAttribute($context["option_value"], "option_value_id", array())) . $this->getAttribute($context["product"], "product_id", array()));
                            echo "\"> 
 <a href=\"javascript:void(0);\" title=\"";
                            // line 18
                            echo $this->getAttribute($context["option_value"], "name", array());
                            echo "\" style=\"width: ";
                            echo (isset($context["width_product_list"]) ? $context["width_product_list"] : null);
                            echo "px; height: ";
                            echo (isset($context["height_product_list"]) ? $context["height_product_list"] : null);
                            echo "px; background-image: url('";
                            echo $this->getAttribute($context["option_value"], "image", array());
                            echo "')\"></a> 
 <script type=\"text/javascript\"> 
 jQuery(document).ready(function(\$) { 
 var \$window_width = \$(window).width(); 
 ";
                            // line 22
                            if (((isset($context["colorswatch_type"]) ? $context["colorswatch_type"] : null) == "click")) {
                                echo " 
 \$(document).on('click', '.so-productlist-colorswatch li.colorswatch";
                                // line 23
                                echo (($this->getAttribute($context["option_value"], "product_option_value_id", array()) . $this->getAttribute($context["option_value"], "option_value_id", array())) . $this->getAttribute($context["product"], "product_id", array()));
                                echo "', function(e) { 
 e.preventDefault(); 
 ";
                                // line 25
                                if ($this->getAttribute($context["option_value"], "color_image", array())) {
                                    echo " 
 jQuery('img#product-image-";
                                    // line 26
                                    echo $this->getAttribute($context["product"], "product_id", array());
                                    echo "').attr('src', '";
                                    echo $this->getAttribute($context["option_value"], "color_image", array());
                                    echo "'); 
 ";
                                }
                                // line 27
                                echo " 
 
 \$('.so-productlist-colorswatch li').removeClass('actived'); 
 \$(this).removeClass('actived').addClass('actived'); 
 }); 
 ";
                            } else {
                                // line 32
                                echo " 
 if (\$window_width > 1199) { 
 \$('.so-productlist-colorswatch li.colorswatch";
                                // line 34
                                echo (($this->getAttribute($context["option_value"], "product_option_value_id", array()) . $this->getAttribute($context["option_value"], "option_value_id", array())) . $this->getAttribute($context["product"], "product_id", array()));
                                echo "').hover(function(e) { 
 e.preventDefault(); 
 ";
                                // line 36
                                if ($this->getAttribute($context["option_value"], "color_image", array())) {
                                    echo " 
 jQuery('img#product-image-";
                                    // line 37
                                    echo $this->getAttribute($context["product"], "product_id", array());
                                    echo "').attr('src', '";
                                    echo $this->getAttribute($context["option_value"], "color_image", array());
                                    echo "'); 
 ";
                                }
                                // line 38
                                echo " 
 
 \$('.so-productlist-colorswatch li').removeClass('actived'); 
 \$(this).removeClass('actived').addClass('actived'); 
 }); 
 } 
 else { 
 \$(document).on('click', '.so-productlist-colorswatch li.colorswatch";
                                // line 45
                                echo (($this->getAttribute($context["option_value"], "product_option_value_id", array()) . $this->getAttribute($context["option_value"], "option_value_id", array())) . $this->getAttribute($context["product"], "product_id", array()));
                                echo "', function(e) { 
 e.preventDefault(); 
 ";
                                // line 47
                                if ($this->getAttribute($context["option_value"], "color_image", array())) {
                                    echo " 
 jQuery('img#product-image-";
                                    // line 48
                                    echo $this->getAttribute($context["product"], "product_id", array());
                                    echo "').attr('src', '";
                                    echo $this->getAttribute($context["option_value"], "color_image", array());
                                    echo "'); 
 ";
                                }
                                // line 49
                                echo " 
 
 \$('.so-productlist-colorswatch li').removeClass('actived'); 
 \$(this).removeClass('actived').addClass('actived'); 
 }); 
 } 
 ";
                            }
                            // line 55
                            echo " 
 }) 
 </script> 
 </li> 
 ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option_value'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 59
                        echo " 
 ";
                    }
                    // line 60
                    echo " 
 ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 61
                echo " 
 </ul> 
 ";
            }
            // line 63
            echo " 
 
        <p>";
            // line 65
            echo $this->getAttribute($context["product"], "description", array());
            echo "</p>
        ";
            // line 66
            if ($this->getAttribute($context["product"], "rating", array())) {
                // line 67
                echo "        <div class=\"rating\">
          ";
                // line 68
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(5);
                foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                    // line 69
                    echo "          ";
                    if (($this->getAttribute($context["product"], "rating", array()) < $context["i"])) {
                        // line 70
                        echo "          <span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-2x\"></i></span>
          ";
                    } else {
                        // line 72
                        echo "          <span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-2x\"></i><i class=\"fa fa-star-o fa-stack-2x\"></i></span>
          ";
                    }
                    // line 74
                    echo "          ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 75
                echo "        </div>
        ";
            }
            // line 77
            echo "        ";
            if ($this->getAttribute($context["product"], "price", array())) {
                // line 78
                echo "        <p class=\"price\">
          ";
                // line 79
                if ( !$this->getAttribute($context["product"], "special", array())) {
                    // line 80
                    echo "           
 ";
                    // line 81
                    if (($this->getAttribute((isset($context["cfp_setting"]) ? $context["cfp_setting"] : null), "module_so_call_for_price_status", array()) && ($this->getAttribute($context["product"], "price_0", array()) <= 0))) {
                        echo " 
 ";
                        // line 82
                        if (($this->getAttribute((isset($context["cfp_setting"]) ? $context["cfp_setting"] : null), "module_so_call_for_price_replace_cart", array(), "any", true, true) && ($this->getAttribute((isset($context["cfp_setting"]) ? $context["cfp_setting"] : null), "module_so_call_for_price_replace_cart", array()) == "0"))) {
                            echo " 
 <a data-fancybox data-type=\"ajax\" data-src=\"";
                            // line 83
                            echo (isset($context["base"]) ? $context["base"] : null);
                            echo "index.php?route=extension/module/so_call_for_price&product_id=";
                            echo $this->getAttribute($context["product"], "product_id", array());
                            echo "\" href=\"javascript:;\" class=\"callforprice\" style=\"color: #ff0000; font-weight: bold;\"><i class=\"fa fa-phone\"></i> ";
                            echo (isset($context["text_price_0"]) ? $context["text_price_0"] : null);
                            echo "</a> 
 ";
                        }
                        // line 84
                        echo " 
 ";
                    } else {
                        // line 85
                        echo " 
 ";
                        // line 86
                        echo $this->getAttribute($context["product"], "price", array());
                        echo " 
 ";
                    }
                    // line 87
                    echo " 
 
          ";
                } else {
                    // line 90
                    echo "          <span class=\"price-new\">";
                    echo $this->getAttribute($context["product"], "special", array());
                    echo "</span> <span class=\"price-old\">";
                    echo $this->getAttribute($context["product"], "price", array());
                    echo "</span>
          ";
                }
                // line 92
                echo "           
 ";
                // line 93
                if ((($this->getAttribute($context["product"], "tax", array()) && $this->getAttribute((isset($context["cfp_setting"]) ? $context["cfp_setting"] : null), "module_so_call_for_price_status", array())) && ($this->getAttribute($context["product"], "price_0", array()) > 0))) {
                    echo " 
 
          <span class=\"price-tax\">";
                    // line 95
                    echo (isset($context["text_tax"]) ? $context["text_tax"] : null);
                    echo " ";
                    echo $this->getAttribute($context["product"], "tax", array());
                    echo "</span>
          ";
                }
                // line 97
                echo "        </p>
        ";
            }
            // line 99
            echo "      </div>
      <div class=\"button-group\">
         
 ";
            // line 102
            if (($this->getAttribute((isset($context["cfp_setting"]) ? $context["cfp_setting"] : null), "module_so_call_for_price_status", array()) && ($this->getAttribute($context["product"], "price_0", array()) <= 0))) {
                echo " 
 ";
                // line 103
                if (($this->getAttribute((isset($context["cfp_setting"]) ? $context["cfp_setting"] : null), "module_so_call_for_price_hide_cart", array(), "any", true, true) && ($this->getAttribute((isset($context["cfp_setting"]) ? $context["cfp_setting"] : null), "module_so_call_for_price_hide_cart", array()) == "0"))) {
                    echo " 
 ";
                    // line 104
                    if (($this->getAttribute((isset($context["cfp_setting"]) ? $context["cfp_setting"] : null), "module_so_call_for_price_replace_cart", array(), "any", true, true) && ($this->getAttribute((isset($context["cfp_setting"]) ? $context["cfp_setting"] : null), "module_so_call_for_price_replace_cart", array()) == "1"))) {
                        echo " 
 <button type=\"button\" data-fancybox data-type=\"ajax\" data-src=\"";
                        // line 105
                        echo (isset($context["base"]) ? $context["base"] : null);
                        echo "index.php?route=extension/module/so_call_for_price&product_id=";
                        echo $this->getAttribute($context["product"], "product_id", array());
                        echo "\">";
                        echo (isset($context["text_price_0"]) ? $context["text_price_0"] : null);
                        echo "</button> 
 ";
                    } else {
                        // line 106
                        echo " 
 <button type=\"button\" style=\"cursor: default; background-color: #eee; color: #ccc;\"><i class=\"fa fa-shopping-cart\"></i> <span class=\"hidden-xs hidden-sm hidden-md\">";
                        // line 107
                        echo (isset($context["button_cart"]) ? $context["button_cart"] : null);
                        echo "</span></button> 
 ";
                    }
                    // line 108
                    echo " 
 ";
                } else {
                    // line 109
                    echo " 
 ";
                    // line 110
                    if (($this->getAttribute((isset($context["cfp_setting"]) ? $context["cfp_setting"] : null), "module_so_call_for_price_replace_cart", array(), "any", true, true) && ($this->getAttribute((isset($context["cfp_setting"]) ? $context["cfp_setting"] : null), "module_so_call_for_price_replace_cart", array()) == "1"))) {
                        echo " 
 <button type=\"button\" data-fancybox data-type=\"ajax\" data-src=\"";
                        // line 111
                        echo (isset($context["base"]) ? $context["base"] : null);
                        echo "index.php?route=extension/module/so_call_for_price&product_id=";
                        echo $this->getAttribute($context["product"], "product_id", array());
                        echo "\">";
                        echo (isset($context["text_price_0"]) ? $context["text_price_0"] : null);
                        echo "</button> 
 ";
                    }
                    // line 112
                    echo " 
 ";
                }
                // line 113
                echo " 
 ";
            } else {
                // line 114
                echo " 
 <button type=\"button\" onclick=\"cart.add('";
                // line 115
                echo $this->getAttribute($context["product"], "product_id", array());
                echo "');\"><i class=\"fa fa-shopping-cart\"></i> <span class=\"hidden-xs hidden-sm hidden-md\">";
                echo (isset($context["button_cart"]) ? $context["button_cart"] : null);
                echo "</span></button> 
 ";
            }
            // line 116
            echo " 
 
        <button type=\"button\" data-toggle=\"tooltip\" title=\"";
            // line 118
            echo (isset($context["button_wishlist"]) ? $context["button_wishlist"] : null);
            echo "\" onclick=\"wishlist.add('";
            echo $this->getAttribute($context["product"], "product_id", array());
            echo "');\"><i class=\"fa fa-heart\"></i></button>
        <button type=\"button\" data-toggle=\"tooltip\" title=\"";
            // line 119
            echo (isset($context["button_compare"]) ? $context["button_compare"] : null);
            echo "\" onclick=\"compare.add('";
            echo $this->getAttribute($context["product"], "product_id", array());
            echo "');\"><i class=\"fa fa-exchange\"></i></button>
      </div>
    </div>
  </div>
  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 124
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "default/template/extension/module/featured.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  410 => 124,  397 => 119,  391 => 118,  387 => 116,  380 => 115,  377 => 114,  373 => 113,  369 => 112,  360 => 111,  356 => 110,  353 => 109,  349 => 108,  344 => 107,  341 => 106,  332 => 105,  328 => 104,  324 => 103,  320 => 102,  315 => 99,  311 => 97,  304 => 95,  299 => 93,  296 => 92,  288 => 90,  283 => 87,  278 => 86,  275 => 85,  271 => 84,  262 => 83,  258 => 82,  254 => 81,  251 => 80,  249 => 79,  246 => 78,  243 => 77,  239 => 75,  233 => 74,  229 => 72,  225 => 70,  222 => 69,  218 => 68,  215 => 67,  213 => 66,  209 => 65,  205 => 63,  200 => 61,  193 => 60,  189 => 59,  179 => 55,  170 => 49,  163 => 48,  159 => 47,  154 => 45,  145 => 38,  138 => 37,  134 => 36,  129 => 34,  125 => 32,  117 => 27,  110 => 26,  106 => 25,  101 => 23,  97 => 22,  84 => 18,  80 => 17,  74 => 16,  70 => 15,  64 => 14,  60 => 13,  56 => 12,  49 => 10,  37 => 7,  33 => 6,  29 => 4,  25 => 3,  19 => 1,);
    }
}
/* <h3>{{ heading_title }}</h3>*/
/* <div class="row">*/
/*  {% for product in products %}*/
/*   <div class="product-layout col-lg-3 col-md-3 col-sm-6 col-xs-12">*/
/*     <div class="product-thumb transition">*/
/*       <div class="image"><a href="{{ product.href }}"> */
/*  <img src="{{ product.thumb }}" alt="{{ product.name }}" title="{{ product.name }}" class="img-responsive" id="product-image-{{ product.product_id }}" /> */
/*  </a></div>*/
/*       <div class="caption">*/
/*         <h4><a href="{{ product.href }}">{{ product.name }}</a></h4>*/
/*  */
/*  {% if product.option %} */
/*  <ul class="so-productlist-colorswatch" id="colorswatch-{{ product.product_id }}"> */
/*  {% for option in product.option %} */
/*  {% if option.type == 'select' %} */
/*  {% for option_value in option.product_option_value %} */
/*  <li class="colorswatch{{option_value.product_option_value_id~option_value.option_value_id~product.product_id }}"> */
/*  <a href="javascript:void(0);" title="{{ option_value.name }}" style="width: {{ width_product_list }}px; height: {{ height_product_list }}px; background-image: url('{{ option_value.image }}')"></a> */
/*  <script type="text/javascript"> */
/*  jQuery(document).ready(function($) { */
/*  var $window_width = $(window).width(); */
/*  {% if colorswatch_type == 'click' %} */
/*  $(document).on('click', '.so-productlist-colorswatch li.colorswatch{{ option_value.product_option_value_id~option_value.option_value_id~product.product_id }}', function(e) { */
/*  e.preventDefault(); */
/*  {% if option_value.color_image %} */
/*  jQuery('img#product-image-{{product.product_id }}').attr('src', '{{ option_value.color_image }}'); */
/*  {% endif %} */
/*  */
/*  $('.so-productlist-colorswatch li').removeClass('actived'); */
/*  $(this).removeClass('actived').addClass('actived'); */
/*  }); */
/*  {% else %} */
/*  if ($window_width > 1199) { */
/*  $('.so-productlist-colorswatch li.colorswatch{{ option_value.product_option_value_id~option_value.option_value_id~product.product_id }}').hover(function(e) { */
/*  e.preventDefault(); */
/*  {% if option_value.color_image %} */
/*  jQuery('img#product-image-{{product.product_id }}').attr('src', '{{ option_value.color_image }}'); */
/*  {% endif %} */
/*  */
/*  $('.so-productlist-colorswatch li').removeClass('actived'); */
/*  $(this).removeClass('actived').addClass('actived'); */
/*  }); */
/*  } */
/*  else { */
/*  $(document).on('click', '.so-productlist-colorswatch li.colorswatch{{ option_value.product_option_value_id~option_value.option_value_id~product.product_id }}', function(e) { */
/*  e.preventDefault(); */
/*  {% if option_value.color_image %} */
/*  jQuery('img#product-image-{{product.product_id }}').attr('src', '{{ option_value.color_image }}'); */
/*  {% endif %} */
/*  */
/*  $('.so-productlist-colorswatch li').removeClass('actived'); */
/*  $(this).removeClass('actived').addClass('actived'); */
/*  }); */
/*  } */
/*  {% endif %} */
/*  }) */
/*  </script> */
/*  </li> */
/*  {% endfor %} */
/*  {% endif %} */
/*  {% endfor %} */
/*  </ul> */
/*  {% endif %} */
/*  */
/*         <p>{{ product.description }}</p>*/
/*         {% if product.rating %}*/
/*         <div class="rating">*/
/*           {% for i in 5 %}*/
/*           {% if product.rating < i %}*/
/*           <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>*/
/*           {% else %}*/
/*           <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>*/
/*           {% endif %}*/
/*           {% endfor %}*/
/*         </div>*/
/*         {% endif %}*/
/*         {% if product.price %}*/
/*         <p class="price">*/
/*           {% if not product.special %}*/
/*            */
/*  {% if (cfp_setting.module_so_call_for_price_status and product.price_0 <= 0) %} */
/*  {% if cfp_setting.module_so_call_for_price_replace_cart is defined and cfp_setting.module_so_call_for_price_replace_cart == '0' %} */
/*  <a data-fancybox data-type="ajax" data-src="{{ base }}index.php?route=extension/module/so_call_for_price&product_id={{ product.product_id }}" href="javascript:;" class="callforprice" style="color: #ff0000; font-weight: bold;"><i class="fa fa-phone"></i> {{ text_price_0 }}</a> */
/*  {% endif %} */
/*  {% else %} */
/*  {{ product.price }} */
/*  {% endif %} */
/*  */
/*           {% else %}*/
/*           <span class="price-new">{{ product.special }}</span> <span class="price-old">{{ product.price }}</span>*/
/*           {% endif %}*/
/*            */
/*  {% if ((product.tax) and (cfp_setting.module_so_call_for_price_status) and (product.price_0 > 0)) %} */
/*  */
/*           <span class="price-tax">{{ text_tax }} {{ product.tax }}</span>*/
/*           {% endif %}*/
/*         </p>*/
/*         {% endif %}*/
/*       </div>*/
/*       <div class="button-group">*/
/*          */
/*  {% if (cfp_setting.module_so_call_for_price_status and product.price_0 <= 0) %} */
/*  {% if cfp_setting.module_so_call_for_price_hide_cart is defined and cfp_setting.module_so_call_for_price_hide_cart == '0' %} */
/*  {% if cfp_setting.module_so_call_for_price_replace_cart is defined and cfp_setting.module_so_call_for_price_replace_cart == '1' %} */
/*  <button type="button" data-fancybox data-type="ajax" data-src="{{ base }}index.php?route=extension/module/so_call_for_price&product_id={{ product.product_id }}">{{ text_price_0 }}</button> */
/*  {% else %} */
/*  <button type="button" style="cursor: default; background-color: #eee; color: #ccc;"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md">{{ button_cart }}</span></button> */
/*  {% endif %} */
/*  {% else %} */
/*  {% if cfp_setting.module_so_call_for_price_replace_cart is defined and cfp_setting.module_so_call_for_price_replace_cart == '1' %} */
/*  <button type="button" data-fancybox data-type="ajax" data-src="{{ base }}index.php?route=extension/module/so_call_for_price&product_id={{ product.product_id }}">{{ text_price_0 }}</button> */
/*  {% endif %} */
/*  {% endif %} */
/*  {% else %} */
/*  <button type="button" onclick="cart.add('{{ product.product_id }}');"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md">{{ button_cart }}</span></button> */
/*  {% endif %} */
/*  */
/*         <button type="button" data-toggle="tooltip" title="{{ button_wishlist }}" onclick="wishlist.add('{{ product.product_id }}');"><i class="fa fa-heart"></i></button>*/
/*         <button type="button" data-toggle="tooltip" title="{{ button_compare }}" onclick="compare.add('{{ product.product_id }}');"><i class="fa fa-exchange"></i></button>*/
/*       </div>*/
/*     </div>*/
/*   </div>*/
/*   {% endfor %}*/
/* </div>*/
/* */
