<?php

/* extension/module/simple_blog/notification.twig */
class __TwigTemplate_7cd43eaaef8498395ec4ffedf3f088e25609c12c1612c8fee308a9dba81e8b7f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "    
    <div id=\"content\">
        <div class=\"page-header\">
            <div class=\"container-fluid\">
                <h1>";
        // line 5
        echo (isset($context["error_database"]) ? $context["error_database"] : null);
        echo "</h1>
                <ul class=\"breadcrumb\">
                    ";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 8
            echo "                        <li><a href=\"";
            echo $this->getAttribute($context["breadcrumb"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array());
            echo "</a></li>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 10
        echo "                </ul>
            </div>
        </div>
        
        <div class=\"container-fluid\">
            <div class=\"alert alert-danger\"><i class=\"fa fa-exclamation-circle\"></i>
                ";
        // line 16
        echo (isset($context["text_install_message"]) ? $context["text_install_message"] : null);
        echo " <a href=\"";
        echo (isset($context["install_database"]) ? $context["install_database"] : null);
        echo "\" class=\"btn btn-info\">";
        echo (isset($context["text_upgread"]) ? $context["text_upgread"] : null);
        echo "</a>                
            </div>
        </div>        
    </div>    
";
        // line 20
        echo (isset($context["footer"]) ? $context["footer"] : null);
    }

    public function getTemplateName()
    {
        return "extension/module/simple_blog/notification.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  66 => 20,  55 => 16,  47 => 10,  36 => 8,  32 => 7,  27 => 5,  19 => 1,);
    }
}
/* {{ header }}{{ column_left }}    */
/*     <div id="content">*/
/*         <div class="page-header">*/
/*             <div class="container-fluid">*/
/*                 <h1>{{ error_database }}</h1>*/
/*                 <ul class="breadcrumb">*/
/*                     {% for breadcrumb in breadcrumbs %}*/
/*                         <li><a href="{{ breadcrumb.href }}">{{ breadcrumb.text }}</a></li>*/
/*                     {% endfor %}*/
/*                 </ul>*/
/*             </div>*/
/*         </div>*/
/*         */
/*         <div class="container-fluid">*/
/*             <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i>*/
/*                 {{ text_install_message }} <a href="{{ install_database }}" class="btn btn-info">{{ text_upgread }}</a>                */
/*             </div>*/
/*         </div>        */
/*     </div>    */
/* {{ footer }}*/
