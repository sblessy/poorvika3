<?php

/* default/template/extension/module/account_picture.twig */
class __TwigTemplate_31c1e1785ea9932cf9752356e397c4b54f24edfec73297e2139c0966fd404d39 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((isset($context["logged"]) ? $context["logged"] : null)) {
            // line 2
            echo "<style>
.fileUpload {
\tposition: absolute;
\toverflow: hidden;
\tmargin: 0 auto;
\tbackground: none;
\tborder:none;
\twidth:100px;
\tpadding-top:5px;
\tpadding-bottom: 5px;
\tmargin-bottom: 5px;
\tborder-radius: 4px;
}
.fileUpload input.upload {
\tposition: absolute;
\ttop: 0;
\tright: 0;
\tmargin: 0;
\tpadding: 0;
\tfont-size: 20px;
\tcursor: pointer;
\topacity: 0;
\tfilter: alpha(opacity=0);
}
.avimage {
\tmargin: 10px 0;
}
.imagediv {
\tposition: relative;
}
.updatepic {
\tbackground-color: black;
\tcolor: white;
\topacity: 0.5;
\twidth: 120px;
\tposition: absolute;
\tbottom: 0px;
\tleft: 71px;
\tmargin: 10px 0;
\tfont-size: medium;
\tcursor: pointer;
\tdisplay: none;
}
</style>
<h4 class=\"text-center\">";
            // line 46
            echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
            echo "</h4>
<div class=\"text-center\">
   <form action=\"";
            // line 48
            echo (isset($context["action"]) ? $context["action"] : null);
            echo "\" method=\"post\" enctype=\"multipart/form-data\">
\t";
            // line 49
            if ((isset($context["extension"]) ? $context["extension"] : null)) {
                // line 50
                echo "\t<div class=\"warning\">";
                echo (isset($context["extension"]) ? $context["extension"] : null);
                echo "</div>
\t";
            }
            // line 52
            echo "\t<div class=\"fileUpload\">
\t\t<input type=\"file\" name=\"account_picture\" class=\"upload\" />
\t</div>
\t<div class=\"imagediv\">
\t\t";
            // line 56
            if ((isset($context["customer_ext"]) ? $context["customer_ext"] : null)) {
                // line 57
                echo "\t\t<img src=\"";
                echo (isset($context["image"]) ? $context["image"] : null);
                echo "\" width=\"";
                echo (isset($context["width"]) ? $context["width"] : null);
                echo "\" height=\"";
                echo (isset($context["height"]) ? $context["height"] : null);
                echo "\" class=\"avimage\" />
\t\t";
            } else {
                // line 59
                echo "\t\t<img src=\"image/no_image.png\" width=\"100\" height=\"100\" class=\"avimage\" />
\t\t";
            }
            // line 61
            echo "\t\t<div class=\"updatepic\">";
            echo (isset($context["text_change"]) ? $context["text_change"] : null);
            echo "</div>
\t</div>
\t<input type=\"submit\" name=\"change\" value=\"Update photo\" class=\"submitpic hide\" />
</form>
</div>\t

<script type=\"text/javascript\">
\t\$('.updatepic, .avimage').on('click', function () {
\t\t\$('.upload').trigger('click');
\t});
\t\$('.imagediv').on('mouseenter', function () {
\t\t\$('.updatepic').fadeIn();
\t});
\t\$('.imagediv').on('mouseleave', function () {
\t\t\$('.updatepic').fadeOut();
\t});
\t\$('.upload').on('change', function () {
\t\t\$('.submitpic').trigger('click');
\t})
</script>
";
        }
    }

    public function getTemplateName()
    {
        return "default/template/extension/module/account_picture.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  106 => 61,  102 => 59,  92 => 57,  90 => 56,  84 => 52,  78 => 50,  76 => 49,  72 => 48,  67 => 46,  21 => 2,  19 => 1,);
    }
}
/* {% if logged %}*/
/* <style>*/
/* .fileUpload {*/
/* 	position: absolute;*/
/* 	overflow: hidden;*/
/* 	margin: 0 auto;*/
/* 	background: none;*/
/* 	border:none;*/
/* 	width:100px;*/
/* 	padding-top:5px;*/
/* 	padding-bottom: 5px;*/
/* 	margin-bottom: 5px;*/
/* 	border-radius: 4px;*/
/* }*/
/* .fileUpload input.upload {*/
/* 	position: absolute;*/
/* 	top: 0;*/
/* 	right: 0;*/
/* 	margin: 0;*/
/* 	padding: 0;*/
/* 	font-size: 20px;*/
/* 	cursor: pointer;*/
/* 	opacity: 0;*/
/* 	filter: alpha(opacity=0);*/
/* }*/
/* .avimage {*/
/* 	margin: 10px 0;*/
/* }*/
/* .imagediv {*/
/* 	position: relative;*/
/* }*/
/* .updatepic {*/
/* 	background-color: black;*/
/* 	color: white;*/
/* 	opacity: 0.5;*/
/* 	width: 120px;*/
/* 	position: absolute;*/
/* 	bottom: 0px;*/
/* 	left: 71px;*/
/* 	margin: 10px 0;*/
/* 	font-size: medium;*/
/* 	cursor: pointer;*/
/* 	display: none;*/
/* }*/
/* </style>*/
/* <h4 class="text-center">{{ heading_title }}</h4>*/
/* <div class="text-center">*/
/*    <form action="{{ action }}" method="post" enctype="multipart/form-data">*/
/* 	{% if extension %}*/
/* 	<div class="warning">{{ extension }}</div>*/
/* 	{% endif %}*/
/* 	<div class="fileUpload">*/
/* 		<input type="file" name="account_picture" class="upload" />*/
/* 	</div>*/
/* 	<div class="imagediv">*/
/* 		{% if customer_ext %}*/
/* 		<img src="{{ image }}" width="{{ width }}" height="{{ height }}" class="avimage" />*/
/* 		{% else %}*/
/* 		<img src="image/no_image.png" width="100" height="100" class="avimage" />*/
/* 		{% endif %}*/
/* 		<div class="updatepic">{{ text_change }}</div>*/
/* 	</div>*/
/* 	<input type="submit" name="change" value="Update photo" class="submitpic hide" />*/
/* </form>*/
/* </div>	*/
/* */
/* <script type="text/javascript">*/
/* 	$('.updatepic, .avimage').on('click', function () {*/
/* 		$('.upload').trigger('click');*/
/* 	});*/
/* 	$('.imagediv').on('mouseenter', function () {*/
/* 		$('.updatepic').fadeIn();*/
/* 	});*/
/* 	$('.imagediv').on('mouseleave', function () {*/
/* 		$('.updatepic').fadeOut();*/
/* 	});*/
/* 	$('.upload').on('change', function () {*/
/* 		$('.submitpic').trigger('click');*/
/* 	})*/
/* </script>*/
/* {% endif %}*/
