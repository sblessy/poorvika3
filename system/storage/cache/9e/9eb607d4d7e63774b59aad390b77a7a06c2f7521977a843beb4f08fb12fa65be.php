<?php

/* so-mobile/template/extension/module/so_deals/default_carousel.twig */
class __TwigTemplate_45d88cc3c2cad570079a3109cf9d30163754f76b103ce6eb1d597af59de9c046 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"";
        echo (isset($context["tag_id"]) ? $context["tag_id"] : null);
        echo "\" class=\"so-deal modcontent clearfix ";
        echo (isset($context["class_respl"]) ? $context["class_respl"] : null);
        echo " ";
        if (((isset($context["button_page"]) ? $context["button_page"] : null) == "top")) {
            echo " ";
            echo "button-type1";
            echo " ";
        } else {
            echo " ";
            echo "button-type2";
            echo " ";
        }
        echo " style2\">
\t";
        // line 2
        if ((isset($context["display_feature"]) ? $context["display_feature"] : null)) {
            // line 3
            echo "\t\t<div class=\"product-feature\">
\t\t";
            // line 4
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["product_features"]) ? $context["product_features"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 5
                echo "\t\t\t<div class=\"item\">
\t\t\t\t<div class=\"product-thumb transition\">
\t\t\t\t\t<div class=\"image\">
\t\t\t\t\t\t";
                // line 8
                if (($this->getAttribute($context["product"], "special", array()) && (isset($context["display_sale"]) ? $context["display_sale"] : null))) {
                    // line 9
                    echo "\t\t\t\t\t\t\t<span class=\"label label-sale\">";
                    echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "text_sale"), "method");
                    echo "</span>
\t\t\t\t\t\t";
                }
                // line 11
                echo "\t\t\t\t\t\t";
                if (($this->getAttribute($context["product"], "productNew", array()) && (isset($context["display_new"]) ? $context["display_new"] : null))) {
                    // line 12
                    echo "\t\t\t\t\t\t\t<span class=\"label label-new\">";
                    echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "text_new"), "method");
                    echo "</span>
\t\t\t\t\t\t";
                }
                // line 14
                echo "\t\t\t\t\t\t";
                if ((isset($context["product_image"]) ? $context["product_image"] : null)) {
                    // line 15
                    echo "\t\t\t\t\t\t\t<a href=\"";
                    echo $this->getAttribute($context["product"], "href", array());
                    echo "\" target=\"";
                    echo (isset($context["item_link_target"]) ? $context["item_link_target"] : null);
                    echo "\">
\t\t\t\t\t\t\t\t";
                    // line 16
                    if (((isset($context["product_image_num"]) ? $context["product_image_num"] : null) == 2)) {
                        // line 17
                        echo "\t\t\t\t\t\t\t\t\t<img data-sizes=\"auto\" src=\"data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==\" data-src=\"";
                        echo $this->getAttribute($context["product"], "thumb", array());
                        echo "\" class=\"img-thumb1 lazyload\" alt=\"";
                        echo $this->getAttribute($context["product"], "name", array());
                        echo "\">
\t\t\t\t\t\t\t\t\t<img data-sizes=\"auto\" src=\"data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==\" data-src=\"";
                        // line 18
                        echo $this->getAttribute($context["product"], "thumb2", array());
                        echo "\" class=\"img-thumb2 lazyload\" alt=\"";
                        echo $this->getAttribute($context["product"], "name", array());
                        echo "\">
\t\t\t\t\t\t\t\t";
                    } else {
                        // line 20
                        echo "\t\t\t\t\t\t\t\t\t<img class=\"lazyload\"   data-sizes=\"auto\" src=\"data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==\" data-src=\"";
                        echo $this->getAttribute($context["product"], "thumb", array());
                        echo "\" alt=\"";
                        echo $this->getAttribute($context["product"], "name", array());
                        echo "\">
\t\t\t\t\t\t\t\t";
                    }
                    // line 22
                    echo "\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t";
                }
                // line 24
                echo "\t\t\t\t\t\t
\t\t\t\t\t\t";
                // line 25
                if ((((isset($context["display_addtocart"]) ? $context["display_addtocart"] : null) || (isset($context["display_wishlist"]) ? $context["display_wishlist"] : null)) || (isset($context["display_compare"]) ? $context["display_compare"] : null))) {
                    // line 26
                    echo "\t\t\t\t\t\t\t<div class=\"button-group\">
\t\t\t\t\t\t\t\t";
                    // line 27
                    if ((isset($context["display_addtocart"]) ? $context["display_addtocart"] : null)) {
                        // line 28
                        echo "\t\t\t\t\t\t\t\t\t<button type=\"button\" onclick=\"cart.add('";
                        echo $this->getAttribute($context["product"], "product_id", array());
                        echo "');\"><i class=\"fa fa-shopping-cart\"></i> <span class=\"hidden-xs hidden-sm hidden-md\">";
                        echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "button_cart"), "method");
                        echo "</span></button>
\t\t\t\t\t\t\t\t";
                    }
                    // line 30
                    echo "\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t";
                    // line 31
                    if ((isset($context["display_wishlist"]) ? $context["display_wishlist"] : null)) {
                        // line 32
                        echo "\t\t\t\t\t\t\t\t\t<button type=\"button\" title=\"";
                        echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "button_wishlist"), "method");
                        echo "\" onclick=\"wishlist.add('";
                        echo $this->getAttribute($context["product"], "product_id", array());
                        echo "');\"><i class=\"fa fa-heart\"></i></button>
\t\t\t\t\t\t\t\t";
                    }
                    // line 34
                    echo "
\t\t\t\t\t\t\t\t";
                    // line 35
                    if ((isset($context["display_compare"]) ? $context["display_compare"] : null)) {
                        // line 36
                        echo "\t\t\t\t\t\t\t\t\t<button type=\"button\" title=\"";
                        echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "button_compare"), "method");
                        echo "\" onclick=\"compare.add('";
                        echo $this->getAttribute($context["product"], "product_id", array());
                        echo "');\"><i class=\"fa fa-exchange\"></i></button>
\t\t\t\t\t\t\t\t";
                    }
                    // line 38
                    echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
                }
                // line 40
                echo "\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"caption\">
\t\t\t\t\t\t";
                // line 43
                if (((isset($context["display_title"]) ? $context["display_title"] : null) == 1)) {
                    // line 44
                    echo "\t\t\t\t\t\t\t<h4><a href=\"";
                    echo $this->getAttribute($context["product"], "href", array());
                    echo "\" target=\"";
                    echo (isset($context["item_link_target"]) ? $context["item_link_target"] : null);
                    echo "\" title=\"";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "\" >";
                    echo $this->getAttribute($context["product"], "name_maxlength", array());
                    echo "</a></h4>
\t\t\t\t\t\t";
                }
                // line 46
                echo "\t\t\t\t\t\t
\t\t\t\t\t\t";
                // line 47
                if ((isset($context["display_description"]) ? $context["display_description"] : null)) {
                    // line 48
                    echo "\t\t\t\t\t\t\t<p>";
                    echo $this->getAttribute($context["product"], "description_maxlength", array());
                    echo "</p>
\t\t\t\t\t\t";
                }
                // line 50
                echo "\t\t\t\t\t\t
\t\t\t\t\t\t";
                // line 51
                if ((isset($context["display_rating"]) ? $context["display_rating"] : null)) {
                    // line 52
                    echo "\t\t\t\t\t\t\t<div class=\"rating\">
\t\t\t\t\t\t\t\t";
                    // line 53
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(range(1, 5));
                    foreach ($context['_seq'] as $context["_key"] => $context["j"]) {
                        // line 54
                        echo "\t\t\t\t\t\t\t\t\t";
                        if (($this->getAttribute($context["product"], "rating", array()) < $context["j"])) {
                            // line 55
                            echo "\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-2x\"></i></span>
\t\t\t\t\t\t\t\t\t";
                        } else {
                            // line 57
                            echo "\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-2x\"></i><i class=\"fa fa-star-o fa-stack-2x\"></i></span>
\t\t\t\t\t\t\t\t\t";
                        }
                        // line 59
                        echo "\t\t\t\t\t\t\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['j'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 60
                    echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
                }
                // line 62
                echo "
\t\t\t\t\t\t";
                // line 63
                if (($this->getAttribute($context["product"], "price", array()) && (isset($context["display_price"]) ? $context["display_price"] : null))) {
                    // line 64
                    echo "\t\t\t\t\t\t\t<p class=\"price\">
\t\t\t\t\t\t\t\t";
                    // line 65
                    if ( !$this->getAttribute($context["product"], "special", array())) {
                        // line 66
                        echo "\t\t\t\t\t\t\t\t\t";
                        echo $this->getAttribute($context["product"], "price", array());
                        echo "
\t\t\t\t\t\t\t\t";
                    } else {
                        // line 68
                        echo "\t\t\t\t\t\t\t\t\t<span class=\"price-new\">";
                        echo $this->getAttribute($context["product"], "special", array());
                        echo "</span>
\t\t\t\t\t\t\t\t\t<span class=\"price-old\">";
                        // line 69
                        echo $this->getAttribute($context["product"], "price", array());
                        echo "</span>
\t\t\t\t\t\t\t\t";
                    }
                    // line 71
                    echo "
\t\t\t\t\t\t\t\t";
                    // line 72
                    if ($this->getAttribute($context["product"], "tax", array())) {
                        // line 73
                        echo "\t\t\t\t\t\t\t\t<span class=\"price-tax\">";
                        echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "text_tax"), "method");
                        echo " ";
                        echo $this->getAttribute($context["product"], "tax", array());
                        echo "</span>
\t\t\t\t\t\t\t\t";
                    }
                    // line 75
                    echo "\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t";
                }
                // line 77
                echo "\t\t\t\t\t\t<div class=\"item-time\">
\t\t\t\t\t\t\t<div class=\"item-timer product_time_";
                // line 78
                echo $this->getAttribute($context["product"], "product_id", array());
                echo "\"></div>
\t\t\t\t\t\t\t<script type=\"text/javascript\">
\t\t\t\t\t\t\t\t//<![CDATA[
\t\t\t\t\t\t\t\tlistdeal";
                // line 81
                echo (isset($context["module"]) ? $context["module"] : null);
                echo ".push('product_time_";
                echo $this->getAttribute($context["product"], "product_id", array());
                echo "|";
                echo $this->getAttribute($context["product"], "specialPriceToDate", array());
                echo "')
\t\t\t\t\t\t\t\t//]]>
\t\t\t\t\t\t\t</script>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 89
            echo "\t\t</div>
\t";
        }
        // line 91
        echo "\t<div class=\"products-list extraslider-inner\" data-effect=\"";
        echo (isset($context["effect"]) ? $context["effect"] : null);
        echo "\">
\t\t";
        // line 92
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["list"]) ? $context["list"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            if (!twig_in_filter((isset($context["product_feature_ids"]) ? $context["product_feature_ids"] : null), $this->getAttribute($context["product"], "product_id", array()))) {
                // line 93
                echo "\t\t\t";
                $context["i"] = ((isset($context["i"]) ? $context["i"] : null) + 1);
                // line 94
                echo "\t\t\t";
                if (((((isset($context["i"]) ? $context["i"] : null) % (isset($context["nb_rows"]) ? $context["nb_rows"] : null)) == 1) || ((isset($context["nb_rows"]) ? $context["nb_rows"] : null) == 1))) {
                    // line 95
                    echo "\t\t\t<div class=\"item product-layout product-grid\">
\t\t\t\t";
                }
                // line 97
                echo "\t\t\t\t<div class=\"product-item-container\">
\t\t\t\t\t<div class=\"left-block\">
\t\t\t\t\t\t<div class=\"product-image-container\">
\t\t\t\t\t\t\t<div class=\"image\">
\t\t\t\t\t\t\t\t";
                // line 101
                if ((isset($context["product_image"]) ? $context["product_image"] : null)) {
                    // line 102
                    echo "\t\t\t\t\t\t\t\t\t<a href=\"";
                    echo $this->getAttribute($context["product"], "href", array());
                    echo "\" target=\"";
                    echo (isset($context["item_link_target"]) ? $context["item_link_target"] : null);
                    echo "\">
\t\t\t\t\t\t\t\t\t\t";
                    // line 103
                    if (((isset($context["product_image_num"]) ? $context["product_image_num"] : null) == 2)) {
                        // line 104
                        echo "\t\t\t\t\t\t\t\t\t\t\t<img data-sizes=\"auto\" src=\"data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==\" data-src=\"";
                        echo $this->getAttribute($context["product"], "thumb", array());
                        echo "\" class=\"img-thumb1 lazyload\" alt=\"";
                        echo $this->getAttribute($context["product"], "name", array());
                        echo "\">
\t\t\t\t\t\t\t\t\t\t\t<img data-sizes=\"auto\" src=\"data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==\" data-src=\"";
                        // line 105
                        echo $this->getAttribute($context["product"], "thumb2", array());
                        echo "\" class=\"img-thumb2 lazyload\" alt=\"";
                        echo $this->getAttribute($context["product"], "name", array());
                        echo "\">
\t\t\t\t\t\t\t\t\t\t";
                    } else {
                        // line 107
                        echo "\t\t\t\t\t\t\t\t\t\t\t<img class=\"lazyload\"   data-sizes=\"auto\" src=\"data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==\" data-src=\"";
                        echo $this->getAttribute($context["product"], "thumb", array());
                        echo "\" alt=\"";
                        echo $this->getAttribute($context["product"], "name", array());
                        echo "\">
\t\t\t\t\t\t\t\t\t\t";
                    }
                    // line 109
                    echo "\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t";
                }
                // line 110
                echo "\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"box-label\">
\t\t\t\t\t\t\t\t";
                // line 113
                if (($this->getAttribute($context["product"], "special", array()) && (isset($context["display_sale"]) ? $context["display_sale"] : null))) {
                    // line 114
                    echo "\t\t\t\t\t\t\t\t\t<span class=\"label-product label-sale\">
\t\t\t\t\t\t\t\t\t\t";
                    // line 115
                    echo $this->getAttribute($context["product"], "discount", array());
                    echo "
\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t";
                }
                // line 118
                echo "\t\t\t\t\t\t\t\t";
                if (($this->getAttribute($context["product"], "productNew", array()) && (isset($context["display_new"]) ? $context["display_new"] : null))) {
                    // line 119
                    echo "\t\t\t\t\t\t\t\t\t<span class=\"label-product label-new\">";
                    echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "text_new"), "method");
                    echo "</span>
\t\t\t\t\t\t\t\t";
                }
                // line 121
                echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"button-group text-center\">
\t\t\t\t\t\t\t";
                // line 124
                if ((isset($context["display_addtocart"]) ? $context["display_addtocart"] : null)) {
                    // line 125
                    echo "\t\t\t\t\t\t\t\t<button class=\"btn-button addToCart\" type=\"button\" onclick=\"cart.add('";
                    echo $this->getAttribute($context["product"], "product_id", array());
                    echo "');\"><i class=\"fa fa-shopping-cart\"></i> <span class=\"hidden-xs hidden-sm hidden-md\">";
                    echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "button_cart"), "method");
                    echo "</span></button>
\t\t\t\t\t\t\t";
                }
                // line 127
                echo "
\t\t\t\t\t\t\t";
                // line 128
                if ((isset($context["display_wishlist"]) ? $context["display_wishlist"] : null)) {
                    // line 129
                    echo "\t\t\t\t\t\t\t\t<button class=\"btn-button wishlist\" type=\"button\" title=\"";
                    echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "button_wishlist"), "method");
                    echo "\" onclick=\"wishlist.add('";
                    echo $this->getAttribute($context["product"], "product_id", array());
                    echo "');\"><i class=\"fa fa-heart-o\"></i></button>
\t\t\t\t\t\t\t";
                }
                // line 131
                echo "
\t\t\t\t\t\t\t";
                // line 132
                if ((isset($context["display_compare"]) ? $context["display_compare"] : null)) {
                    // line 133
                    echo "\t\t\t\t\t\t\t\t<button class=\"btn-button compare\" type=\"button\" title=\"";
                    echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "button_compare"), "method");
                    echo "\" onclick=\"compare.add('";
                    echo $this->getAttribute($context["product"], "product_id", array());
                    echo "');\"><i class=\"fa fa-exchange\"></i></button>
\t\t\t\t\t\t\t";
                }
                // line 135
                echo "\t\t\t\t\t\t\t<a class=\"hidden\" data-product='";
                echo $this->getAttribute($context["product"], "product_id", array());
                echo "' href=\"";
                echo $this->getAttribute($context["product"], "href", array());
                echo "\" target=\"";
                echo (isset($context["item_link_target"]) ? $context["item_link_target"] : null);
                echo "\" title=\"";
                echo $this->getAttribute($context["product"], "nameFull", array());
                echo " \" ></a>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"right-block\">
\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"caption\">
\t\t\t\t\t\t\t";
                // line 142
                if ((isset($context["display_rating"]) ? $context["display_rating"] : null)) {
                    // line 143
                    echo "\t\t\t\t\t\t\t\t<div class=\"rating\">
\t\t\t\t\t\t\t\t\t";
                    // line 144
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(range(1, 5));
                    foreach ($context['_seq'] as $context["_key"] => $context["j"]) {
                        // line 145
                        echo "\t\t\t\t\t\t\t\t\t\t";
                        if (($this->getAttribute($context["product"], "rating", array()) < $context["j"])) {
                            // line 146
                            echo "\t\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-2x\"></i></span>
\t\t\t\t\t\t\t\t\t\t";
                        } else {
                            // line 148
                            echo "\t\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-2x\"></i><i class=\"fa fa-star-o fa-stack-2x\"></i></span>
\t\t\t\t\t\t\t\t\t\t";
                        }
                        // line 150
                        echo "\t\t\t\t\t\t\t\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['j'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 151
                    echo "\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t";
                }
                // line 153
                echo "
\t\t\t\t\t\t\t";
                // line 154
                if (((isset($context["display_title"]) ? $context["display_title"] : null) == 1)) {
                    // line 155
                    echo "\t\t\t\t\t\t\t\t<h4><a href=\"";
                    echo $this->getAttribute($context["product"], "href", array());
                    echo "\" target=\"";
                    echo (isset($context["item_link_target"]) ? $context["item_link_target"] : null);
                    echo "\" title=\"";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "\">";
                    echo $this->getAttribute($context["product"], "name_maxlength", array());
                    echo "</a></h4>
\t\t\t\t\t\t\t";
                }
                // line 157
                echo "\t\t\t\t\t\t\t";
                if ((isset($context["display_description"]) ? $context["display_description"] : null)) {
                    // line 158
                    echo "\t\t\t\t\t\t\t\t<p>";
                    echo $this->getAttribute($context["product"], "description_maxlength", array());
                    echo "</p>
\t\t\t\t\t\t\t";
                }
                // line 160
                echo "\t\t\t\t\t\t\t";
                if (($this->getAttribute($context["product"], "price", array()) && (isset($context["display_price"]) ? $context["display_price"] : null))) {
                    // line 161
                    echo "\t\t\t\t\t\t\t<p class=\"price\">
\t\t\t\t\t\t\t\t";
                    // line 162
                    if ( !$this->getAttribute($context["product"], "special", array())) {
                        // line 163
                        echo "\t\t\t\t\t\t\t\t\t";
                        echo $this->getAttribute($context["product"], "price", array());
                        echo "
\t\t\t\t\t\t\t\t";
                    } else {
                        // line 165
                        echo "\t\t\t\t\t\t\t\t\t<span class=\"price-new\">";
                        echo $this->getAttribute($context["product"], "special", array());
                        echo "</span>
\t\t\t\t\t\t\t\t\t<span class=\"price-old\">";
                        // line 166
                        echo $this->getAttribute($context["product"], "price", array());
                        echo "</span>
\t\t\t\t\t\t\t\t";
                    }
                    // line 168
                    echo "
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t";
                }
                // line 172
                echo "\t\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t</div>
\t\t\t\t";
                // line 177
                if (((((isset($context["i"]) ? $context["i"] : null) % (isset($context["nb_rows"]) ? $context["nb_rows"] : null)) == 0) || ((isset($context["i"]) ? $context["i"] : null) == (isset($context["count_item"]) ? $context["count_item"] : null)))) {
                    // line 178
                    echo "\t\t\t</div>
\t\t\t";
                }
                // line 180
                echo "\t\t";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 181
        echo "\t</div>
</div>
<script type=\"text/javascript\">
//<![CDATA[
jQuery(document).ready(function (\$) {  ;
(function (element) {
\tvar \$element = \$(element),
\t\t\t\$extraslider = \$('.extraslider-inner', \$element),
\t\t\t\$featureslider = \$('.product-feature', \$element),
\t\t\t_delay = ";
        // line 190
        echo (isset($context["delay"]) ? $context["delay"] : null);
        echo ",
\t\t\t_duration = ";
        // line 191
        echo (isset($context["duration"]) ? $context["duration"] : null);
        echo ",
\t\t\t_effect = '";
        // line 192
        echo (isset($context["effect"]) ? $context["effect"] : null);
        echo "';

\t\$extraslider.on('initialized.owl.carousel2', function () {
\t\tvar \$item_active = \$('.extraslider-inner .owl2-item.active', \$element);
\t\tif (\$item_active.length > 1 && _effect != 'none') {
\t\t\t_getAnimate(\$item_active);
\t\t}
\t\telse {
\t\t\tvar \$item = \$('.extraslider-inner .owl2-item', \$element);
\t\t\t\$item.css({'opacity': 1, 'filter': 'alpha(opacity = 100)'});
\t\t}
\t\t/*";
        // line 203
        if (((isset($context["button_page"]) ? $context["button_page"] : null) == "top")) {
            // line 204
            echo "\t\t\t\$('.extraslider-inner .owl2-dots', \$element).insertAfter(\$('.extraslider-inner .owl2-prev', \$element));
\t\t\t\$('.extraslider-inner .owl2-controls', \$element).insertBefore(\$extraslider).addClass('extraslider');
\t\t";
        } else {
            // line 207
            echo "\t\t\t\$('.extraslider-inner .owl2-nav', \$element).insertBefore(\$extraslider);
\t\t\t\$('.extraslider-inner .owl2-controls', \$element).insertAfter(\$extraslider).addClass('extraslider');
\t\t";
        }
        // line 209
        echo "*/
\t});

\t\$extraslider.owlCarousel2({
\t\trtl: ";
        // line 213
        echo (isset($context["direction"]) ? $context["direction"] : null);
        echo ",
\t\tmargin: ";
        // line 214
        echo (isset($context["margin"]) ? $context["margin"] : null);
        echo ",
\t\tslideBy: ";
        // line 215
        echo (isset($context["slideBy"]) ? $context["slideBy"] : null);
        echo ",
\t\tautoplay: ";
        // line 216
        echo (isset($context["autoplay"]) ? $context["autoplay"] : null);
        echo ",
\t\tautoplayHoverPause: ";
        // line 217
        echo (isset($context["autoplayHoverPause"]) ? $context["autoplayHoverPause"] : null);
        echo ",
\t\tautoplayTimeout: ";
        // line 218
        echo (isset($context["autoplayTimeout"]) ? $context["autoplayTimeout"] : null);
        echo ",
\t\tautoplaySpeed: ";
        // line 219
        echo (isset($context["autoplaySpeed"]) ? $context["autoplaySpeed"] : null);
        echo ",
\t\tstartPosition: ";
        // line 220
        echo (isset($context["startPosition"]) ? $context["startPosition"] : null);
        echo ",
\t\tmouseDrag: ";
        // line 221
        echo (isset($context["mouseDrag"]) ? $context["mouseDrag"] : null);
        echo ",
\t\ttouchDrag: ";
        // line 222
        echo (isset($context["touchDrag"]) ? $context["touchDrag"] : null);
        echo ",
\t\tresponsive: {
\t\t\t0: \t{ items: ";
        // line 224
        echo (isset($context["nb_column4"]) ? $context["nb_column4"] : null);
        echo " } ,
\t\t\t480: { items: ";
        // line 225
        echo (isset($context["nb_column3"]) ? $context["nb_column3"] : null);
        echo " },
\t\t\t768: { items: ";
        // line 226
        echo (isset($context["nb_column2"]) ? $context["nb_column2"] : null);
        echo " },
\t\t\t992: { items: ";
        // line 227
        echo (isset($context["nb_column1"]) ? $context["nb_column1"] : null);
        echo " },
\t\t\t1200: {items: ";
        // line 228
        echo (isset($context["nb_column0"]) ? $context["nb_column0"] : null);
        echo " }
\t\t},
\t\tdotClass: 'owl2-dot',
\t\tdotsClass: 'owl2-dots',
\t\tdots: ";
        // line 232
        echo (isset($context["dots"]) ? $context["dots"] : null);
        echo ",
\t\tdotsSpeed: ";
        // line 233
        echo (isset($context["dotsSpeed"]) ? $context["dotsSpeed"] : null);
        echo ",
\t\tnav: ";
        // line 234
        echo (isset($context["navs"]) ? $context["navs"] : null);
        echo ",
\t\tloop: ";
        // line 235
        echo (isset($context["loop"]) ? $context["loop"] : null);
        echo ",
\t\tnavSpeed: ";
        // line 236
        echo (isset($context["navSpeed"]) ? $context["navSpeed"] : null);
        echo ",
\t\tnavText: ['<i class=\"fa fa-chevron-left\"></i>', '<i class=\"fa fa-chevron-right\"></i>'],
\t\tnavClass: ['owl2-prev', 'owl2-next']
\t});

\t\$extraslider.on('translated.owl.carousel2', function (e) {
\t\tvar \$item_active = \$('.extraslider-inner .owl2-item.active', \$element);
\t\tvar \$item = \$('.extraslider-inner .owl2-item', \$element);

\t\t_UngetAnimate(\$item);

\t\tif (\$item_active.length > 1 && _effect != 'none') {
\t\t\t_getAnimate(\$item_active);
\t\t} else {
\t\t\t\$item.css({'opacity': 1, 'filter': 'alpha(opacity = 100)'});
\t\t}
\t});
\t/*feature product*/
\t\$featureslider.on('initialized.owl.carousel2', function () {
\t\tvar \$item_active = \$('.product-feature .owl2-item.active', \$element);
\t\tif (\$item_active.length > 1 && _effect != 'none') {
\t\t\t_getAnimate(\$item_active);
\t\t}
\t\telse {
\t\t\tvar \$item = \$('.owl2-item', \$element);
\t\t\t\$item.css({'opacity': 1, 'filter': 'alpha(opacity = 100)'});
\t\t}
\t\t";
        // line 263
        if (((isset($context["button_page"]) ? $context["button_page"] : null) == "top")) {
            // line 264
            echo "\t\t\t\$('.product-feature .owl2-dots', \$element).insertAfter(\$('.product-feature .owl2-prev', \$element));
\t\t\t\$('.product-feature .owl2-controls', \$element).insertBefore(\$featureslider).addClass('featureslider');\t
\t\t";
        } else {
            // line 267
            echo "\t\t\t\$('.product-feature .owl2-nav', \$element).insertBefore(\$featureslider);
\t\t\t\$('.product-feature .owl2-controls', \$element).insertAfter(\$featureslider).addClass('featureslider');;
\t\t";
        }
        // line 270
        echo "\t});

\t\$featureslider.owlCarousel2({
\t\trtl: ";
        // line 273
        echo (isset($context["direction"]) ? $context["direction"] : null);
        echo ",
\t\tmargin: ";
        // line 274
        echo (isset($context["margin"]) ? $context["margin"] : null);
        echo ",
\t\tslideBy: ";
        // line 275
        echo (isset($context["slideBy"]) ? $context["slideBy"] : null);
        echo ",
\t\tautoplay: ";
        // line 276
        echo (isset($context["autoplay"]) ? $context["autoplay"] : null);
        echo ",
\t\tautoplayHoverPause: ";
        // line 277
        echo (isset($context["autoplayHoverPause"]) ? $context["autoplayHoverPause"] : null);
        echo ",
\t\tautoplayTimeout: ";
        // line 278
        echo (isset($context["autoplayTimeout"]) ? $context["autoplayTimeout"] : null);
        echo ",
\t\tautoplaySpeed: ";
        // line 279
        echo (isset($context["autoplaySpeed"]) ? $context["autoplaySpeed"] : null);
        echo ",
\t\tstartPosition: ";
        // line 280
        echo (isset($context["startPosition"]) ? $context["startPosition"] : null);
        echo ",
\t\tmouseDrag: ";
        // line 281
        echo (isset($context["mouseDrag"]) ? $context["mouseDrag"] : null);
        echo ",
\t\ttouchDrag: ";
        // line 282
        echo (isset($context["touchDrag"]) ? $context["touchDrag"] : null);
        echo ",
\t\tresponsive: {
\t\t\t0: \t{ items: 1 } ,
\t\t\t480: { items: 1 },
\t\t\t768: { items: 1 },
\t\t\t992: { items: 1 },
\t\t\t1200: {items: 1}
\t\t},
\t\tdotClass: 'owl2-dot',
\t\t\tdotsClass: 'owl2-dots',
\t\tdots: ";
        // line 292
        echo (isset($context["dots"]) ? $context["dots"] : null);
        echo ",
\t\tdotsSpeed: ";
        // line 293
        echo (isset($context["dotsSpeed"]) ? $context["dotsSpeed"] : null);
        echo ",
\t\tnav: ";
        // line 294
        echo (isset($context["navs"]) ? $context["navs"] : null);
        echo ",
\t\tloop: ";
        // line 295
        echo (isset($context["loop"]) ? $context["loop"] : null);
        echo ",
\t\tnavSpeed: ";
        // line 296
        echo (isset($context["navSpeed"]) ? $context["navSpeed"] : null);
        echo ",
\t\tnavText: ['&#171;', '&#187;'],
\t\tnavClass: ['owl2-prev', 'owl2-next']
\t});

\t\$featureslider.on('translated.owl.carousel2', function (e) {
\t\tvar \$item_active = \$('.product-feature .owl2-item.active', \$element);
\t\tvar \$item = \$('.product-feature .owl2-item', \$element);

\t\t_UngetAnimate(\$item);

\t\tif (\$item_active.length > 1 && _effect != 'none') {
\t\t\t_getAnimate(\$item_active);
\t\t} else {
\t\t\t\$item.css({'opacity': 1, 'filter': 'alpha(opacity = 100)'});
\t\t}
\t});
\t
\tfunction _getAnimate(\$el) {
\t\tif (_effect == 'none') return;
\t\t\$extraslider.removeClass('extra-animate');
\t\t\$el.each(function (i) {
\t\t\tvar \$_el = \$(this);
\t\t\t\$(this).css({
\t\t\t\t'-webkit-animation': _effect + ' ' + _duration + \"ms ease both\",
\t\t\t\t'-moz-animation': _effect + ' ' + _duration + \"ms ease both\",
\t\t\t\t'-o-animation': _effect + ' ' + _duration + \"ms ease both\",
\t\t\t\t'animation': _effect + ' ' + _duration + \"ms ease both\",
\t\t\t\t'-webkit-animation-delay': +i * _delay + 'ms',
\t\t\t\t'-moz-animation-delay': +i * _delay + 'ms',
\t\t\t\t'-o-animation-delay': +i * _delay + 'ms',
\t\t\t\t'animation-delay': +i * _delay + 'ms',
\t\t\t\t'opacity': 1
\t\t\t}).animate({
\t\t\t\topacity: 1
\t\t\t});

\t\t\tif (i == \$el.size() - 1) {
\t\t\t\t\$extraslider.addClass(\"extra-animate\");
\t\t\t}
\t\t});
\t}

\tfunction _UngetAnimate(\$el) {
\t\t\$el.each(function (i) {
\t\t\t\$(this).css({
\t\t\t\t'animation': '',
\t\t\t\t'-webkit-animation': '',
\t\t\t\t'-moz-animation': '',
\t\t\t\t'-o-animation': '',
\t\t\t\t'opacity': 1
\t\t\t});
\t\t});
\t}
\tdata = new Date(2013, 10, 26, 12, 00, 00);
\tfunction CountDown(date, id) {
\t\tdateNow = new Date();
\t\tamount = date.getTime() - dateNow.getTime();
\t\tif (amount < 0 && \$('#' + id).length) {
\t\t\t\$('.' + id).html(\"Now!\");
\t\t} else {
\t\t\tdays = 0;
\t\t\thours = 0;
\t\t\tmins = 0;
\t\t\tsecs = 0;
\t\t\tout = \"\";
\t\t\tamount = Math.floor(amount / 1000);
\t\t\tdays = Math.floor(amount / 86400);
\t\t\tamount = amount % 86400;
\t\t\thours = Math.floor(amount / 3600);
\t\t\tamount = amount % 3600;
\t\t\tmins = Math.floor(amount / 60);
\t\t\tamount = amount % 60;
\t\t\tsecs = Math.floor(amount);
\t\t\tif (days != 0) {
\t\t\t\tout += \"<div class='time-item time-day'>\" + \"<div class='num-time'>\" + days + \"</div>\" + \" <div class='name-time'>\" + ((days == 1) ? \"";
        // line 371
        echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "text_Day"), "method");
        echo "\" : \"";
        echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "text_Days"), "method");
        echo "\") + \"</div>\" + \"</div> \";
\t\t\t}
\t\t\tif(days == 0 && hours != 0)
\t\t\t{
\t\t\t\t out += \"<div class='time-item time-hour' style='width:33.33%'>\" + \"<div class='num-time'>\" + hours + \"</div>\" + \" <div class='name-time'>\" + ((hours == 1) ? \"";
        // line 375
        echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "text_Hour"), "method");
        echo "\" : \"";
        echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "text_Hours"), "method");
        echo "\") + \"</div>\" + \"</div> \";
\t\t\t}else if (hours != 0) {
\t\t\t\tout += \"<div class='time-item time-hour'>\" + \"<div class='num-time'>\" + hours + \"</div>\" + \" <div class='name-time'>\" + ((hours == 1) ? \"";
        // line 377
        echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "text_Hour"), "method");
        echo "\" : \"";
        echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "text_Hours"), "method");
        echo "\") + \"</div>\" + \"</div> \";
\t\t\t}
\t\t\tif(days == 0 && hours != 0)
\t\t\t{
\t\t\t\tout += \"<div class='time-item time-min' style='width:33.33%'>\" + \"<div class='num-time'>\" + mins + \"</div>\" + \" <div class='name-time'>\" + ((mins == 1) ? \"";
        // line 381
        echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "text_Min"), "method");
        echo "\" : \"";
        echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "text_Mins"), "method");
        echo "\") + \"</div>\" + \"</div> \";
\t\t\t\tout += \"<div class='time-item time-sec' style='width:33.33%'>\" + \"<div class='num-time'>\" + secs + \"</div>\" + \" <div class='name-time'>\" + ((secs == 1) ? \"";
        // line 382
        echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "text_Sec"), "method");
        echo "\" : \"";
        echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "text_Secs"), "method");
        echo "\") + \"</div>\" + \"</div> \";
\t\t\t\tout = out.substr(0, out.length - 2);
\t\t\t}else if(days == 0 && hours == 0)
\t\t\t{
\t\t\t\tout += \"<div class='time-item time-min' style='width:50%'>\" + \"<div class='num-time'>\" + mins + \"</div>\" + \" <div class='name-time'>\" + ((mins == 1) ? \"";
        // line 386
        echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "text_Min"), "method");
        echo "\" : \"";
        echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "text_Mins"), "method");
        echo "\") + \"</div>\" + \"</div> \";
\t\t\t\tout += \"<div class='time-item time-sec' style='width:50%'>\" + \"<div class='num-time'>\" + secs + \"</div>\" + \" <div class='name-time'>\" + ((secs == 1) ? \"";
        // line 387
        echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "text_Sec"), "method");
        echo "\" : \"";
        echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "text_Secs"), "method");
        echo "\") + \"</div>\" + \"</div> \";
\t\t\t\tout = out.substr(0, out.length - 2);
\t\t\t}else{
\t\t\t\tout += \"<div class='time-item time-min'>\" + \"<div class='num-time'>\" + mins + \"</div>\" + \" <div class='name-time'>\" + ((mins == 1) ? \"";
        // line 390
        echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "text_Min"), "method");
        echo "\" : \"";
        echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "text_Mins"), "method");
        echo "\") + \"</div>\" + \"</div> \";
\t\t\t\tout += \"<div class='time-item time-sec'>\" + \"<div class='num-time'>\" + secs + \"</div>\" + \" <div class='name-time'>\" + ((secs == 1) ? \"";
        // line 391
        echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "text_Sec"), "method");
        echo "\" : \"";
        echo $this->getAttribute((isset($context["objlang"]) ? $context["objlang"] : null), "get", array(0 => "text_Secs"), "method");
        echo "\") + \"</div>\" + \"</div> \";
\t\t\t\tout = out.substr(0, out.length - 2);
\t\t\t}


\t\t\t\$('.' + id).html(out);

\t\t\tsetTimeout(function () {
\t\t\t\tCountDown(date, id);
\t\t\t}, 1000);
\t\t}
\t}
\tif (listdeal";
        // line 403
        echo (isset($context["module"]) ? $context["module"] : null);
        echo ".length > 0) {
\t\tfor (var i = 0; i < listdeal";
        // line 404
        echo (isset($context["module"]) ? $context["module"] : null);
        echo ".length; i++) {
\t\t\tvar arr = listdeal";
        // line 405
        echo (isset($context["module"]) ? $context["module"] : null);
        echo "[i].split(\"|\");
\t\t\tif (arr[1].length) {
\t\t\t\tvar data = new Date(arr[1]);
\t\t\t\tCountDown(data, arr[0]);
\t\t\t}
\t\t}
\t}
\t})('#";
        // line 412
        echo (isset($context["tag_id"]) ? $context["tag_id"] : null);
        echo "');
});
//]]>
</script>";
    }

    public function getTemplateName()
    {
        return "so-mobile/template/extension/module/so_deals/default_carousel.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  958 => 412,  948 => 405,  944 => 404,  940 => 403,  923 => 391,  917 => 390,  909 => 387,  903 => 386,  894 => 382,  888 => 381,  879 => 377,  872 => 375,  863 => 371,  785 => 296,  781 => 295,  777 => 294,  773 => 293,  769 => 292,  756 => 282,  752 => 281,  748 => 280,  744 => 279,  740 => 278,  736 => 277,  732 => 276,  728 => 275,  724 => 274,  720 => 273,  715 => 270,  710 => 267,  705 => 264,  703 => 263,  673 => 236,  669 => 235,  665 => 234,  661 => 233,  657 => 232,  650 => 228,  646 => 227,  642 => 226,  638 => 225,  634 => 224,  629 => 222,  625 => 221,  621 => 220,  617 => 219,  613 => 218,  609 => 217,  605 => 216,  601 => 215,  597 => 214,  593 => 213,  587 => 209,  582 => 207,  577 => 204,  575 => 203,  561 => 192,  557 => 191,  553 => 190,  542 => 181,  535 => 180,  531 => 178,  529 => 177,  522 => 172,  516 => 168,  511 => 166,  506 => 165,  500 => 163,  498 => 162,  495 => 161,  492 => 160,  486 => 158,  483 => 157,  471 => 155,  469 => 154,  466 => 153,  462 => 151,  456 => 150,  452 => 148,  448 => 146,  445 => 145,  441 => 144,  438 => 143,  436 => 142,  419 => 135,  411 => 133,  409 => 132,  406 => 131,  398 => 129,  396 => 128,  393 => 127,  385 => 125,  383 => 124,  378 => 121,  372 => 119,  369 => 118,  363 => 115,  360 => 114,  358 => 113,  353 => 110,  349 => 109,  341 => 107,  334 => 105,  327 => 104,  325 => 103,  318 => 102,  316 => 101,  310 => 97,  306 => 95,  303 => 94,  300 => 93,  295 => 92,  290 => 91,  286 => 89,  268 => 81,  262 => 78,  259 => 77,  255 => 75,  247 => 73,  245 => 72,  242 => 71,  237 => 69,  232 => 68,  226 => 66,  224 => 65,  221 => 64,  219 => 63,  216 => 62,  212 => 60,  206 => 59,  202 => 57,  198 => 55,  195 => 54,  191 => 53,  188 => 52,  186 => 51,  183 => 50,  177 => 48,  175 => 47,  172 => 46,  160 => 44,  158 => 43,  153 => 40,  149 => 38,  141 => 36,  139 => 35,  136 => 34,  128 => 32,  126 => 31,  123 => 30,  115 => 28,  113 => 27,  110 => 26,  108 => 25,  105 => 24,  101 => 22,  93 => 20,  86 => 18,  79 => 17,  77 => 16,  70 => 15,  67 => 14,  61 => 12,  58 => 11,  52 => 9,  50 => 8,  45 => 5,  41 => 4,  38 => 3,  36 => 2,  19 => 1,);
    }
}
/* <div id="{{ tag_id }}" class="so-deal modcontent clearfix {{ class_respl }} {% if button_page == 'top' %} {{ 'button-type1' }} {% else %} {{ 'button-type2' }} {% endif %} style2">*/
/* 	{% if display_feature %}*/
/* 		<div class="product-feature">*/
/* 		{% for product in product_features %}*/
/* 			<div class="item">*/
/* 				<div class="product-thumb transition">*/
/* 					<div class="image">*/
/* 						{% if product.special and display_sale %}*/
/* 							<span class="label label-sale">{{ objlang.get('text_sale') }}</span>*/
/* 						{% endif %}*/
/* 						{% if product.productNew and display_new %}*/
/* 							<span class="label label-new">{{ objlang.get('text_new') }}</span>*/
/* 						{% endif %}*/
/* 						{% if product_image %}*/
/* 							<a href="{{ product.href }}" target="{{ item_link_target }}">*/
/* 								{% if product_image_num ==2 %}*/
/* 									<img data-sizes="auto" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{ product.thumb }}" class="img-thumb1 lazyload" alt="{{ product.name }}">*/
/* 									<img data-sizes="auto" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{ product.thumb2 }}" class="img-thumb2 lazyload" alt="{{ product.name }}">*/
/* 								{% else %}*/
/* 									<img class="lazyload"   data-sizes="auto" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{ product.thumb }}" alt="{{ product.name }}">*/
/* 								{% endif %}*/
/* 							</a>*/
/* 						{% endif %}*/
/* 						*/
/* 						{% if display_addtocart or display_wishlist or display_compare %}*/
/* 							<div class="button-group">*/
/* 								{% if display_addtocart %}*/
/* 									<button type="button" onclick="cart.add('{{ product.product_id }}');"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md">{{ objlang.get('button_cart') }}</span></button>*/
/* 								{% endif %}*/
/* 								*/
/* 								{% if display_wishlist %}*/
/* 									<button type="button" title="{{ objlang.get('button_wishlist') }}" onclick="wishlist.add('{{ product.product_id }}');"><i class="fa fa-heart"></i></button>*/
/* 								{% endif %}*/
/* */
/* 								{% if display_compare %}*/
/* 									<button type="button" title="{{ objlang.get('button_compare') }}" onclick="compare.add('{{ product.product_id }}');"><i class="fa fa-exchange"></i></button>*/
/* 								{% endif %}*/
/* 							</div>*/
/* 						{% endif %}*/
/* 					</div>*/
/* */
/* 					<div class="caption">*/
/* 						{% if display_title == 1 %}*/
/* 							<h4><a href="{{ product.href }}" target="{{ item_link_target }}" title="{{ product.name }}" >{{ product.name_maxlength }}</a></h4>*/
/* 						{% endif %}*/
/* 						*/
/* 						{% if display_description %}*/
/* 							<p>{{ product.description_maxlength|raw }}</p>*/
/* 						{% endif %}*/
/* 						*/
/* 						{% if display_rating %}*/
/* 							<div class="rating">*/
/* 								{% for j in 1..5 %}*/
/* 									{% if product.rating < j %}*/
/* 										<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>*/
/* 									{% else %}*/
/* 										<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>*/
/* 									{% endif %}*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						{% endif %}*/
/* */
/* 						{% if product.price and display_price %}*/
/* 							<p class="price">*/
/* 								{% if not product.special %}*/
/* 									{{ product.price }}*/
/* 								{% else %}*/
/* 									<span class="price-new">{{ product.special }}</span>*/
/* 									<span class="price-old">{{ product.price }}</span>*/
/* 								{% endif %}*/
/* */
/* 								{% if product.tax %}*/
/* 								<span class="price-tax">{{ objlang.get('text_tax') }} {{ product.tax }}</span>*/
/* 								{% endif %}*/
/* 							</p>*/
/* 						{% endif %}*/
/* 						<div class="item-time">*/
/* 							<div class="item-timer product_time_{{ product.product_id }}"></div>*/
/* 							<script type="text/javascript">*/
/* 								//<![CDATA[*/
/* 								listdeal{{ module }}.push('product_time_{{ product.product_id }}|{{ product.specialPriceToDate }}')*/
/* 								//]]>*/
/* 							</script>*/
/* 						</div>*/
/* 					</div>*/
/* 				</div>*/
/* 			</div>*/
/* 		{% endfor %}*/
/* 		</div>*/
/* 	{% endif %}*/
/* 	<div class="products-list extraslider-inner" data-effect="{{ effect }}">*/
/* 		{% for product in list if product_feature_ids not in product.product_id %}*/
/* 			{% set i = i + 1 %}*/
/* 			{% if i % nb_rows == 1 or nb_rows == 1 %}*/
/* 			<div class="item product-layout product-grid">*/
/* 				{% endif %}*/
/* 				<div class="product-item-container">*/
/* 					<div class="left-block">*/
/* 						<div class="product-image-container">*/
/* 							<div class="image">*/
/* 								{% if product_image %}*/
/* 									<a href="{{ product.href }}" target="{{ item_link_target }}">*/
/* 										{% if product_image_num ==2 %}*/
/* 											<img data-sizes="auto" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{ product.thumb }}" class="img-thumb1 lazyload" alt="{{ product.name }}">*/
/* 											<img data-sizes="auto" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{ product.thumb2 }}" class="img-thumb2 lazyload" alt="{{ product.name }}">*/
/* 										{% else %}*/
/* 											<img class="lazyload"   data-sizes="auto" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{ product.thumb }}" alt="{{ product.name }}">*/
/* 										{% endif %}*/
/* 									</a>*/
/* 								{% endif %}							*/
/* 							</div>*/
/* 							<div class="box-label">*/
/* 								{% if product.special and display_sale %}*/
/* 									<span class="label-product label-sale">*/
/* 										{{ product.discount}}*/
/* 									</span>*/
/* 								{% endif %}*/
/* 								{% if product.productNew and display_new %}*/
/* 									<span class="label-product label-new">{{ objlang.get('text_new') }}</span>*/
/* 								{% endif %}*/
/* 							</div>*/
/* 						</div>*/
/* 						<div class="button-group text-center">*/
/* 							{% if display_addtocart %}*/
/* 								<button class="btn-button addToCart" type="button" onclick="cart.add('{{ product.product_id }}');"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md">{{ objlang.get('button_cart') }}</span></button>*/
/* 							{% endif %}*/
/* */
/* 							{% if display_wishlist %}*/
/* 								<button class="btn-button wishlist" type="button" title="{{ objlang.get('button_wishlist') }}" onclick="wishlist.add('{{ product.product_id }}');"><i class="fa fa-heart-o"></i></button>*/
/* 							{% endif %}*/
/* */
/* 							{% if display_compare %}*/
/* 								<button class="btn-button compare" type="button" title="{{ objlang.get('button_compare') }}" onclick="compare.add('{{ product.product_id }}');"><i class="fa fa-exchange"></i></button>*/
/* 							{% endif %}*/
/* 							<a class="hidden" data-product='{{ product.product_id }}' href="{{ product.href }}" target="{{ item_link_target }}" title="{{ product.nameFull }} " ></a>*/
/* 						</div>*/
/* 						*/
/* 					</div>*/
/* 					<div class="right-block">*/
/* 						*/
/* 						<div class="caption">*/
/* 							{% if display_rating %}*/
/* 								<div class="rating">*/
/* 									{% for j in 1..5 %}*/
/* 										{% if product.rating < j %}*/
/* 											<span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>*/
/* 										{% else %}*/
/* 											<span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>*/
/* 										{% endif %}*/
/* 									{% endfor %}*/
/* 								</div>*/
/* 							{% endif %}*/
/* */
/* 							{% if display_title == 1 %}*/
/* 								<h4><a href="{{ product.href }}" target="{{ item_link_target }}" title="{{ product.name }}">{{ product.name_maxlength }}</a></h4>*/
/* 							{% endif %}*/
/* 							{% if display_description %}*/
/* 								<p>{{ product.description_maxlength|raw }}</p>*/
/* 							{% endif %}*/
/* 							{% if product.price and display_price %}*/
/* 							<p class="price">*/
/* 								{% if not product.special %}*/
/* 									{{ product.price }}*/
/* 								{% else %}*/
/* 									<span class="price-new">{{ product.special }}</span>*/
/* 									<span class="price-old">{{ product.price }}</span>*/
/* 								{% endif %}*/
/* */
/* 								*/
/* 								</p>*/
/* 							{% endif %}*/
/* 						*/
/* 						</div>*/
/* 					</div>*/
/* 					*/
/* 				</div>*/
/* 				{% if i % nb_rows == 0 or i == count_item %}*/
/* 			</div>*/
/* 			{% endif %}*/
/* 		{% endfor %}*/
/* 	</div>*/
/* </div>*/
/* <script type="text/javascript">*/
/* //<![CDATA[*/
/* jQuery(document).ready(function ($) {  ;*/
/* (function (element) {*/
/* 	var $element = $(element),*/
/* 			$extraslider = $('.extraslider-inner', $element),*/
/* 			$featureslider = $('.product-feature', $element),*/
/* 			_delay = {{ delay }},*/
/* 			_duration = {{ duration }},*/
/* 			_effect = '{{ effect }}';*/
/* */
/* 	$extraslider.on('initialized.owl.carousel2', function () {*/
/* 		var $item_active = $('.extraslider-inner .owl2-item.active', $element);*/
/* 		if ($item_active.length > 1 && _effect != 'none') {*/
/* 			_getAnimate($item_active);*/
/* 		}*/
/* 		else {*/
/* 			var $item = $('.extraslider-inner .owl2-item', $element);*/
/* 			$item.css({'opacity': 1, 'filter': 'alpha(opacity = 100)'});*/
/* 		}*/
/* 		/*{% if button_page == "top" %}*/
/* 			$('.extraslider-inner .owl2-dots', $element).insertAfter($('.extraslider-inner .owl2-prev', $element));*/
/* 			$('.extraslider-inner .owl2-controls', $element).insertBefore($extraslider).addClass('extraslider');*/
/* 		{% else %}*/
/* 			$('.extraslider-inner .owl2-nav', $element).insertBefore($extraslider);*/
/* 			$('.extraslider-inner .owl2-controls', $element).insertAfter($extraslider).addClass('extraslider');*/
/* 		{% endif %}*//* */
/* 	});*/
/* */
/* 	$extraslider.owlCarousel2({*/
/* 		rtl: {{ direction }},*/
/* 		margin: {{ margin }},*/
/* 		slideBy: {{ slideBy }},*/
/* 		autoplay: {{ autoplay }},*/
/* 		autoplayHoverPause: {{ autoplayHoverPause }},*/
/* 		autoplayTimeout: {{ autoplayTimeout }},*/
/* 		autoplaySpeed: {{ autoplaySpeed }},*/
/* 		startPosition: {{ startPosition }},*/
/* 		mouseDrag: {{ mouseDrag }},*/
/* 		touchDrag: {{ touchDrag }},*/
/* 		responsive: {*/
/* 			0: 	{ items: {{ nb_column4 }} } ,*/
/* 			480: { items: {{ nb_column3 }} },*/
/* 			768: { items: {{ nb_column2 }} },*/
/* 			992: { items: {{ nb_column1 }} },*/
/* 			1200: {items: {{ nb_column0 }} }*/
/* 		},*/
/* 		dotClass: 'owl2-dot',*/
/* 		dotsClass: 'owl2-dots',*/
/* 		dots: {{ dots }},*/
/* 		dotsSpeed: {{ dotsSpeed }},*/
/* 		nav: {{ navs }},*/
/* 		loop: {{ loop }},*/
/* 		navSpeed: {{ navSpeed }},*/
/* 		navText: ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>'],*/
/* 		navClass: ['owl2-prev', 'owl2-next']*/
/* 	});*/
/* */
/* 	$extraslider.on('translated.owl.carousel2', function (e) {*/
/* 		var $item_active = $('.extraslider-inner .owl2-item.active', $element);*/
/* 		var $item = $('.extraslider-inner .owl2-item', $element);*/
/* */
/* 		_UngetAnimate($item);*/
/* */
/* 		if ($item_active.length > 1 && _effect != 'none') {*/
/* 			_getAnimate($item_active);*/
/* 		} else {*/
/* 			$item.css({'opacity': 1, 'filter': 'alpha(opacity = 100)'});*/
/* 		}*/
/* 	});*/
/* 	/*feature product*//* */
/* 	$featureslider.on('initialized.owl.carousel2', function () {*/
/* 		var $item_active = $('.product-feature .owl2-item.active', $element);*/
/* 		if ($item_active.length > 1 && _effect != 'none') {*/
/* 			_getAnimate($item_active);*/
/* 		}*/
/* 		else {*/
/* 			var $item = $('.owl2-item', $element);*/
/* 			$item.css({'opacity': 1, 'filter': 'alpha(opacity = 100)'});*/
/* 		}*/
/* 		{% if button_page == "top" %}*/
/* 			$('.product-feature .owl2-dots', $element).insertAfter($('.product-feature .owl2-prev', $element));*/
/* 			$('.product-feature .owl2-controls', $element).insertBefore($featureslider).addClass('featureslider');	*/
/* 		{% else %}*/
/* 			$('.product-feature .owl2-nav', $element).insertBefore($featureslider);*/
/* 			$('.product-feature .owl2-controls', $element).insertAfter($featureslider).addClass('featureslider');;*/
/* 		{% endif %}*/
/* 	});*/
/* */
/* 	$featureslider.owlCarousel2({*/
/* 		rtl: {{ direction }},*/
/* 		margin: {{ margin }},*/
/* 		slideBy: {{ slideBy }},*/
/* 		autoplay: {{ autoplay }},*/
/* 		autoplayHoverPause: {{ autoplayHoverPause }},*/
/* 		autoplayTimeout: {{ autoplayTimeout }},*/
/* 		autoplaySpeed: {{ autoplaySpeed }},*/
/* 		startPosition: {{ startPosition }},*/
/* 		mouseDrag: {{ mouseDrag }},*/
/* 		touchDrag: {{ touchDrag }},*/
/* 		responsive: {*/
/* 			0: 	{ items: 1 } ,*/
/* 			480: { items: 1 },*/
/* 			768: { items: 1 },*/
/* 			992: { items: 1 },*/
/* 			1200: {items: 1}*/
/* 		},*/
/* 		dotClass: 'owl2-dot',*/
/* 			dotsClass: 'owl2-dots',*/
/* 		dots: {{ dots }},*/
/* 		dotsSpeed: {{ dotsSpeed }},*/
/* 		nav: {{ navs }},*/
/* 		loop: {{ loop }},*/
/* 		navSpeed: {{ navSpeed }},*/
/* 		navText: ['&#171;', '&#187;'],*/
/* 		navClass: ['owl2-prev', 'owl2-next']*/
/* 	});*/
/* */
/* 	$featureslider.on('translated.owl.carousel2', function (e) {*/
/* 		var $item_active = $('.product-feature .owl2-item.active', $element);*/
/* 		var $item = $('.product-feature .owl2-item', $element);*/
/* */
/* 		_UngetAnimate($item);*/
/* */
/* 		if ($item_active.length > 1 && _effect != 'none') {*/
/* 			_getAnimate($item_active);*/
/* 		} else {*/
/* 			$item.css({'opacity': 1, 'filter': 'alpha(opacity = 100)'});*/
/* 		}*/
/* 	});*/
/* 	*/
/* 	function _getAnimate($el) {*/
/* 		if (_effect == 'none') return;*/
/* 		$extraslider.removeClass('extra-animate');*/
/* 		$el.each(function (i) {*/
/* 			var $_el = $(this);*/
/* 			$(this).css({*/
/* 				'-webkit-animation': _effect + ' ' + _duration + "ms ease both",*/
/* 				'-moz-animation': _effect + ' ' + _duration + "ms ease both",*/
/* 				'-o-animation': _effect + ' ' + _duration + "ms ease both",*/
/* 				'animation': _effect + ' ' + _duration + "ms ease both",*/
/* 				'-webkit-animation-delay': +i * _delay + 'ms',*/
/* 				'-moz-animation-delay': +i * _delay + 'ms',*/
/* 				'-o-animation-delay': +i * _delay + 'ms',*/
/* 				'animation-delay': +i * _delay + 'ms',*/
/* 				'opacity': 1*/
/* 			}).animate({*/
/* 				opacity: 1*/
/* 			});*/
/* */
/* 			if (i == $el.size() - 1) {*/
/* 				$extraslider.addClass("extra-animate");*/
/* 			}*/
/* 		});*/
/* 	}*/
/* */
/* 	function _UngetAnimate($el) {*/
/* 		$el.each(function (i) {*/
/* 			$(this).css({*/
/* 				'animation': '',*/
/* 				'-webkit-animation': '',*/
/* 				'-moz-animation': '',*/
/* 				'-o-animation': '',*/
/* 				'opacity': 1*/
/* 			});*/
/* 		});*/
/* 	}*/
/* 	data = new Date(2013, 10, 26, 12, 00, 00);*/
/* 	function CountDown(date, id) {*/
/* 		dateNow = new Date();*/
/* 		amount = date.getTime() - dateNow.getTime();*/
/* 		if (amount < 0 && $('#' + id).length) {*/
/* 			$('.' + id).html("Now!");*/
/* 		} else {*/
/* 			days = 0;*/
/* 			hours = 0;*/
/* 			mins = 0;*/
/* 			secs = 0;*/
/* 			out = "";*/
/* 			amount = Math.floor(amount / 1000);*/
/* 			days = Math.floor(amount / 86400);*/
/* 			amount = amount % 86400;*/
/* 			hours = Math.floor(amount / 3600);*/
/* 			amount = amount % 3600;*/
/* 			mins = Math.floor(amount / 60);*/
/* 			amount = amount % 60;*/
/* 			secs = Math.floor(amount);*/
/* 			if (days != 0) {*/
/* 				out += "<div class='time-item time-day'>" + "<div class='num-time'>" + days + "</div>" + " <div class='name-time'>" + ((days == 1) ? "{{ objlang.get('text_Day') }}" : "{{ objlang.get('text_Days') }}") + "</div>" + "</div> ";*/
/* 			}*/
/* 			if(days == 0 && hours != 0)*/
/* 			{*/
/* 				 out += "<div class='time-item time-hour' style='width:33.33%'>" + "<div class='num-time'>" + hours + "</div>" + " <div class='name-time'>" + ((hours == 1) ? "{{ objlang.get('text_Hour') }}" : "{{ objlang.get('text_Hours') }}") + "</div>" + "</div> ";*/
/* 			}else if (hours != 0) {*/
/* 				out += "<div class='time-item time-hour'>" + "<div class='num-time'>" + hours + "</div>" + " <div class='name-time'>" + ((hours == 1) ? "{{ objlang.get('text_Hour') }}" : "{{ objlang.get('text_Hours') }}") + "</div>" + "</div> ";*/
/* 			}*/
/* 			if(days == 0 && hours != 0)*/
/* 			{*/
/* 				out += "<div class='time-item time-min' style='width:33.33%'>" + "<div class='num-time'>" + mins + "</div>" + " <div class='name-time'>" + ((mins == 1) ? "{{ objlang.get('text_Min') }}" : "{{ objlang.get('text_Mins') }}") + "</div>" + "</div> ";*/
/* 				out += "<div class='time-item time-sec' style='width:33.33%'>" + "<div class='num-time'>" + secs + "</div>" + " <div class='name-time'>" + ((secs == 1) ? "{{ objlang.get('text_Sec') }}" : "{{ objlang.get('text_Secs') }}") + "</div>" + "</div> ";*/
/* 				out = out.substr(0, out.length - 2);*/
/* 			}else if(days == 0 && hours == 0)*/
/* 			{*/
/* 				out += "<div class='time-item time-min' style='width:50%'>" + "<div class='num-time'>" + mins + "</div>" + " <div class='name-time'>" + ((mins == 1) ? "{{ objlang.get('text_Min') }}" : "{{ objlang.get('text_Mins') }}") + "</div>" + "</div> ";*/
/* 				out += "<div class='time-item time-sec' style='width:50%'>" + "<div class='num-time'>" + secs + "</div>" + " <div class='name-time'>" + ((secs == 1) ? "{{ objlang.get('text_Sec') }}" : "{{ objlang.get('text_Secs') }}") + "</div>" + "</div> ";*/
/* 				out = out.substr(0, out.length - 2);*/
/* 			}else{*/
/* 				out += "<div class='time-item time-min'>" + "<div class='num-time'>" + mins + "</div>" + " <div class='name-time'>" + ((mins == 1) ? "{{ objlang.get('text_Min') }}" : "{{ objlang.get('text_Mins') }}") + "</div>" + "</div> ";*/
/* 				out += "<div class='time-item time-sec'>" + "<div class='num-time'>" + secs + "</div>" + " <div class='name-time'>" + ((secs == 1) ? "{{ objlang.get('text_Sec') }}" : "{{ objlang.get('text_Secs') }}") + "</div>" + "</div> ";*/
/* 				out = out.substr(0, out.length - 2);*/
/* 			}*/
/* */
/* */
/* 			$('.' + id).html(out);*/
/* */
/* 			setTimeout(function () {*/
/* 				CountDown(date, id);*/
/* 			}, 1000);*/
/* 		}*/
/* 	}*/
/* 	if (listdeal{{ module }}.length > 0) {*/
/* 		for (var i = 0; i < listdeal{{ module }}.length; i++) {*/
/* 			var arr = listdeal{{ module }}[i].split("|");*/
/* 			if (arr[1].length) {*/
/* 				var data = new Date(arr[1]);*/
/* 				CountDown(data, arr[0]);*/
/* 			}*/
/* 		}*/
/* 	}*/
/* 	})('#{{ tag_id }}');*/
/* });*/
/* //]]>*/
/* </script>*/
